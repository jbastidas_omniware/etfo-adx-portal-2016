﻿using System;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Site.Helpers;
using Site.Library;
using Xrm;
using Newtonsoft.Json;
using Microsoft.Xrm.Sdk;

namespace Site.Services
{
    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AjaxHelper 
    {
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool isContactExist(string EMailAddress1)
        {
            var context = XrmHelper.GetContext();
            return (from c in context.CreateQuery<Contact>() where c.Adx_username == EMailAddress1 select c).AsEnumerable().Count() > 0;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public AccountResponse getMemberAccount(string oems_ETFOMemberId)
        {
            var context = XrmHelper.GetContext();
            Contact contact = (from a in context.CreateQuery<Contact>() where a.mbr_ETFOID == oems_ETFOMemberId select a).FirstOrDefault();

            if (ReferenceEquals(contact, null))
                return new AccountResponse { result = false.ToString()};

            return new AccountResponse
            {
                result = true.ToString(),
                name = contact.FirstName + " " + contact.LastName,
                email = contact.EMailAddress1 ?? string.Empty,
                contactid = contact.ContactId.ToString(),
                //sincheck = contact.oems_SINCheck, // Clean
                eftomemberid = contact.mbr_ETFOID,
                memberonleaveflag = (contact.mbr_OnLeave.HasValue) ? contact.mbr_OnLeave.Value.ToString() : false.ToString(),
                adx_username = (ReferenceEquals(contact, null) ? string.Empty : contact.Adx_username),
                adx_resetpasswordanswer = (ReferenceEquals(contact, null) ? string.Empty : contact.adx_resetpasswordanswer),
                adx_passwordquestion = (ReferenceEquals(contact, null) ? string.Empty : contact.Adx_passwordquestion),
                goodstandingflag = contact.mbr_holdReason != null && contact.mbr_holdReason.Name.ToUpper() == "DISCIPL" ? false.ToString() : true.ToString()
            };
        }

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool CreateMemberProfile(string EMailAddress1, string password, string sequrityQuestion, string sequrityAnswer)
        {
            var context = XrmHelper.GetContext();
            Guid contactid = Guid.Empty;
            string fName = string.Empty;
            string lName = string.Empty;

            if (ProfileHelper.CreateProfileSecurity(EMailAddress1, password, sequrityQuestion, sequrityAnswer))
            {
                contactid = (from c in context.CreateQuery<Contact>() where c.StateCode == 0 && c.EMailAddress1 == EMailAddress1 select c.Id).FirstOrDefault();
            }
            else
            {
                return false;
            }

            ProfileHelper.UpdateProfileContact(contactid, fName, lName, EMailAddress1);

            return true;
        }

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public string getSchoolByBoard(string schoolboardid)
        {
            Guid boardid;
            bool x = Guid.TryParse(schoolboardid, out boardid);
            if (!x)
                return string.Empty;

            return ProfileHelper.getSchoolHtml(boardid);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public EventJasonResponce createRegistration(string accountid, string eventid, string oems_EventInvitedGuestId, int statusCode)
        {
            Guid Accountid;
            bool x = Guid.TryParse(accountid, out Accountid);
            Guid Eventid;
            bool y = Guid.TryParse(eventid, out Eventid);
            Guid invitedGuestId;
            bool z = Guid.TryParse(oems_EventInvitedGuestId, out invitedGuestId);


            if (!x || !y || !z) return new EventJasonResponce
            {
                result = "false"
            };

            return RegistrationHelper.CreateInvitation(Accountid, Eventid, invitedGuestId, statusCode);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool updateInvitedGuest(string oems_EventInvitedGuestId, int statusCode)
        {
            if (statusCode == 0)
                statusCode = 347780002;

            Guid invitedGuestId;
            bool x = Guid.TryParse(oems_EventInvitedGuestId, out invitedGuestId);

            if (!x)
                return false;

            return RegistrationHelper.updateInvitedGuest(invitedGuestId, statusCode);
        }

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //public bool updateRegistrationForm(string oems_EventRegistrationId, string oems_UpdateEventRegistrationData, string oems_UpdatePersonalAccommodationData, int statusCode)
        public int updateDynamicRegistrationForm(string oems_EventRegistrationId, string oems_UpdateEventRegistrationData, string documents, int statusCode)
        {
            //return true;
            Guid EventRegistrationId;
            bool x = Guid.TryParse(oems_EventRegistrationId, out EventRegistrationId);

            if (!x)
                return 0;

            return RegistrationHelper.updateDynamicRegistrationForm(EventRegistrationId, oems_UpdateEventRegistrationData, documents, statusCode);
        }

		[OperationContract]
		[WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		public string GetSchoolData()
		{
			var context = XrmHelper.GetContext();
			var schools = context.mbr_schoolSet;

			var schoolData = schools.Select(school => 
            new
			{
				schoolid = school.Id.ToString(),
				schoolboardid = school.mbr_SchoolBoard.Id.ToString()
			});

			return JsonConvert.SerializeObject(schoolData);
		}
    }
}
