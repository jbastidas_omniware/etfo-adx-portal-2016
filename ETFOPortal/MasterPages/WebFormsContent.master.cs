﻿using System;
using System.Linq;
using System.Web.UI;

namespace Site.MasterPages
{
    public partial class WebFormsContent : PortalMasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var page = XrmContext.Adx_webpageSet.FirstOrDefault(p => p.Adx_webpageId == Portal.Entity.Id);
                if (page != null)
                {
                    CustomEntity.DataItem = page;
                }
            }
        }
    }
}