﻿<%@ Master Language="C#" AutoEventWireup="true" ViewStateMode="Disabled" CodeBehind="Default.master.cs" Inherits="Site.MasterPages.Default" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<!DOCTYPE html>
<html lang="<%: Html.Setting("Html/LanguageCode", "en") %>">
	<head runat="server">
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<title><asp:ContentPlaceHolder ID="Title" runat="server"><%: Html.AttributeLiteral("adx_title") ?? Html.AttributeLiteral("adx_name") %></asp:ContentPlaceHolder><%= Html.SnippetLiteral("Browser Title Suffix") %></title>
		<asp:ContentPlaceHolder ID="MetaTags" runat="server">
			<% var metaDescription = Html.Entity().GetAttribute("adx_meta_description"); %>
			<% if (metaDescription != null && metaDescription.Value != null) { %>
				<meta name="description" content="<%: Html.AttributeLiteral(metaDescription) %>" />
			<% } %>
		</asp:ContentPlaceHolder>
		<script type="text/javascript">
			// Fix for incorrect viewport width setting in IE 10 on Windows Phone 8.
			if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
				var msViewportStyle = document.createElement("style");
				msViewportStyle.appendChild(document.createTextNode("@-ms-viewport{width:auto!important}"));
				document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
			}
		</script>
		<asp:ContentPlaceHolder ID="Styles" runat="server">
			<%= Html.SnippetLiteral("Head/Fonts") %>
			<%= Html.SnippetLiteral("Head/Bootstrap", Html.ContentStyles(only: new Dictionary<string, string>
				{
					{"bootstrap.min.css", Url.Content("~/css/bootstrap.min.css")}
				})) %>
			<%: System.Web.Optimization.Styles.Render(Html.Setting("Head/Icons", "~/css/glyphicons-font-awesome-migrate.min.css")) %>
			<%: System.Web.Optimization.Styles.Render("~/css/default.bundle.css") %>
			<%-- HTML5 shim, for IE6-8 support of HTML elements --%>
			<!--[if lt IE 9]>
				<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->
			<%-- HTML5 polyfill, for IE6-8 support of HTML FormData allowing to post upload file async --%>
			<!--[if lt IE 10]>
				<script src="~/js/formdata.js"></script>
			<![endif]-->
            
			<asp:ContentPlaceHolder ID="Header" runat="server">
			<% Html.RenderPartialFromSetting("Head/Template"); %>
			</asp:ContentPlaceHolder>
			<asp:ContentPlaceHolder ID="Head" runat="server"/>
			<%: Html.ContentStyles(except: new [] { "bootstrap.min.css" }) %>
			<%: Html.StyleAttribute("adx_customcss") %>
			<%= Html.SnippetLiteral("Head/Bottom") %>
		</asp:ContentPlaceHolder>
	</head>
	<body data-sitemap-state="<%: Html.SiteMapState() %>"
		data-dateformat="<%: Html.Setting("DateTime/DateFormat", "M/d/yyyy") %>"
		data-timeformat="<%: Html.Setting("DateTime/TimeFormat", "h:mm tt") %>"
		data-datetimeformat="<%: Html.Setting("DateTime/DateTimeFormat") %>"
		data-app-path="<%: Url.Content("~/") %>"
		data-ckeditor-basepath="<%: Url.Content("~/js/ckeditor/") %>"
		data-case-deflection-url="<%: Html.GetPortalScopedRouteUrlByName("PortalSearch") %>"
		<% if (Html.BooleanSetting("Parature/Enabled").GetValueOrDefault(false))
		   { %>data-parature-case-deflection-url="<%: Html.GetPortalScopedRouteUrlByName("ParatureSearchArticles") %>"<% } %>>

		<%: System.Web.Optimization.Scripts.Render("~/js/default.preform.bundle.js") %>
        <% Html.RenderPartialFromSetting("Header/Template", "HeaderNavbar"); %>
        
		<asp:ContentPlaceHolder ID="ContentContainer" runat="server">
			<asp:ContentPlaceHolder ID="ContentHeader" runat="server"/>
			<div id="content-container" class="container">
				<div id="content" >
					<asp:ContentPlaceHolder ID="MainContent" runat="server"/>
				</div>
			</div>
		</asp:ContentPlaceHolder>

        <% Html.RenderPartialFromSetting("Footer/Template", "FooterMenuWithCopy"); %>
		<%: Html.EntityEditingMetadata() %>
		<%: Html.EditingStyles(new []
			{
				"~/xrm-adx/css/yui-skin-sam-2.9.0/skin.css"//,
				//"~/js/select2/select2.css"
			}) %>
		<%: Html.EditingScripts(dependencyScriptPaths: new []
			{
				"~/xrm-adx/js/jquery-ui-1.10.0.min.js"
			}, extensionScriptPaths: new string[] {}) %>
		<%: System.Web.Optimization.Scripts.Render("~/js/default.bundle.js") %>

		<asp:ContentPlaceHolder ID="Scripts" runat="server"/>
        <script>
            jQuery(document).ready(function () {
                App.init();
            });
        </script>
		<%: Html.ScriptAttribute("adx_customjavascript") %>
		<%= Html.SnippetLiteral("Tracking Code") %>

        <!-- Modal -->
        <div id="informationModal" class="modal fade" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                        <h4 id="modalTitle" class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <p id="modalMessage"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="modalGoTo" type="button" class="btn btn-primary" style="display: none;"></button>
                    </div>
                </div>
            </div>
        </div>
 
        <!-- SessionModal -->
        <div id="sessionModal" class="modal fade" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><%= Html.SnippetLiteral("SessionTimeout/Warning/Title", "Session Timeout Warning") %></h4>
                    </div>
                    <div class="modal-body">
                        <p id="sessionExpireWarning"></p>
                    </div>
                    <div class="modal-footer">
                        <button id="extendSession" type="button" class="btn btn-default"><%= Html.SnippetLiteral("SessionTimeout/Warning/ExtendSession", "Continue") %></button>
                        <button id="cancelSession" type="button" class="btn btn-primary"><%= Html.SnippetLiteral("SessionTimeout/Warning/Logout", "Cancel") %></button>
                    </div>
                </div>
            </div>
        </div>
 
        <!-- Session Management -->
        <script type="text/javascript">
            var sessionVariables =
            {
                sessionTimeoutSeconds: "<%= Session.Timeout %>" * 60, // Session timeout is 20 minutes
                countdownSeconds: '<%= Html.Setting("SessionTimeout/Warning/Seconds", "60") %>', // Number of seconds to count down
                promptSeconds: '<%= Html.Setting("SessionTimeout/Warning/Seconds", "60") %>', // Number of seconds to show the prompt
                extendSessionUrl: '<%= Url.Action("ExtendSession", "Account", new { area = "Account" }) %>', // URL to call when extending session
                expireSessionUrl: '<%= Url.Action("SignOut", "Account", new { area = "Account" }) %>', // URL to call when expiring session
                currentUrl: '<%= Request.Url.AbsolutePath %>',
                registrationUrl: '<%= Html.SiteMarkerUrl("Registration Page") %>',
                sessionMessage: '<%= Html.SnippetLiteral("SessionTimeout/Warning/Message", "Your session will expire in {0}. Press Continue to remain logged in, or Cancel to log off.") %>'
            };

            $(function () {
                // Session Modal
                $('#sessionModal').modal
                ({
                    keyboard: false,
                    backdrop: true,
                    show: false
                });

                var isAuthenticated = "<%= HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated%>".toLowerCase();
                if (isAuthenticated === 'true') {
                    sessionManagement.startManager(sessionVariables);
                }
            });
        </script>

	</body>
</html>
<!-- Generated at <%: DateTime.UtcNow %> -->
<!-- Page OK -->
