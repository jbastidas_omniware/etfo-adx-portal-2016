﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Newtonsoft.Json;
using Site.Areas.ETFO.Library;
using Site.Helpers;
using Xrm;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Portal.Configuration;
using Site.Library;
using System.Web.UI.WebControls;
using Microsoft.Crm.Sdk.Messages;

namespace Site
{
    public class DelegationController : ApiController
    {
        // GET api/<controller>
        [AjaxAuthorizeAttribute]
        public HttpResponseMessage GetEventRegistrationInfo(Guid id)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                oems_EventRegistration eventRegistration = serviceContext.oems_EventRegistrationSet.Where(a => a.oems_EventRegistrationId == id).Single();
                var eventId = eventRegistration.oems_Event.Id;
                var contact = GetContact();

                var now = DateTime.Now.Date;
                var orgContact =
                (
                    from a in serviceContext.mbr_organizationcontactSet
                    where a.mbr_ContactId != null && a.mbr_ContactId.Id == contact.Id
                        && a.mbr_LocalId != null && a.mbr_LocalId.Id == eventRegistration.oems_Local.Id
                        && a.mbr_EffectiveStartDate != null && a.mbr_EffectiveStartDate.Value <= now
                        && a.mbr_EffectiveEndDate != null && a.mbr_EffectiveEndDate.Value >= now
                        && a.mbr_Role != null && a.statecode == 0
                    select a
                ).FirstOrDefault();

                var isPresident = orgContact != null && orgContact.mbr_Role.Name.ToLower().Trim() == "president";
                if (!isPresident)
                {
                    string noPresident = XrmHelper.GetSnippetValueOrDefault("ManageLocalDelegation/ErrorMsg/NoPresident", "You are not authorized to view this page");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, noPresident);
                }

                var delegation =
                (
                    from d in serviceContext.oems_EventLocalSet
                    where (d.oems_Event.Id == eventId && d.oems_local.Id == eventRegistration.oems_Local.Id)
                    select new
                    {
                        Id = d.oems_EventLocalId.Value,
                        Local = d.oems_local.Id,
                        LocalName = d.oems_EventLocalName,
                        Event = d.oems_Event.Id,
                        EventName = d.oems_Event.Name,
                        MaxDelegates = d.oems_LocalDelegationLimit,
                        Status = d.statuscode,
                        LocalOffice = d.oems_local.Name
                    }
                ).FirstOrDefault();

                if (delegation != null)
                {
                    if (delegation.MaxDelegates == null || delegation.MaxDelegates <= 0)
                    {
                        string noLocalLimit = string.Format(XrmHelper.GetSnippetValueOrDefault("ManageLocalDelegation/ErrorMsg/MaxDelegationError", "Delegation limit has not been defined for this local: {0}"), delegation.LocalName);
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, noLocalLimit);
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        delegation.Id,
                        delegation.Local,
                        delegation.Event,
                        delegation.EventName,
                        delegation.MaxDelegates,
                        StatusId = delegation.Status,
                        Status = EntityOptionSet.GetOptionSetLabel("oems_eventlocal", "statuscode", delegation.Status),
                        delegation.LocalOffice
                    });
                }
                else
                {
                    string noDelegation = XrmHelper.GetSnippetValueOrDefault("ManageLocalDelegation/ErrorMsg/NoDelegation", "You are not authorized to view this page");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, noDelegation);
                }
            }
        }

        [AjaxAuthorizeAttribute]
        public HttpResponseMessage GetEventRegistrants(Guid eventRegistrationId)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var eventReg = (from a in serviceContext.oems_EventRegistrationSet where a.oems_EventRegistrationId == eventRegistrationId select a).Single();
                var results =
                (
                    from reg in serviceContext.oems_EventRegistrationSet
                    join evtLocal in serviceContext.oems_EventLocalSet on reg.oems_EventLocal.Id equals evtLocal.Id
                    where evtLocal.oems_local.Id == eventReg.oems_Local.Id && reg.oems_Event.Id == eventReg.oems_Event.Id
                    where reg.statuscode != RegistrationHelper.EventRegistration.Initial &&
                        (reg["oems_registrationtype"] == null || ((OptionSetValue)reg["oems_registrationtype"]).Value == RegistrationHelper.EventRegistrationType.Attendee)
                    select new Registrant
                    {
                        DelgationID = reg.oems_EventLocal.Id,
                        RegistrationID = reg.oems_EventRegistrationId,
                        ContactID = reg.oems_Contact.Id,
                        EventName = reg.oems_Event.Name,
                        LocalName = reg.oems_EventLocal.Name,
                        RegistrantFirstName = reg.oems_FirstName,
                        RegistrantLastName = reg.oems_LastName,
                        RegistrationStateCode = reg.statecode,
                        RegistrationStatusId = reg.statuscode,
                        RegistrationStatusText = EntityOptionSet.GetOptionSetLabel("oems_eventregistration", "statuscode", reg.statuscode),
                        InDelegationList = reg.oems_LocalDelegationFlag.Value,
                        AttendingAsOther = reg.oems_AttendingAsOther,
                        AttendingAsJSON = reg.GetAttributeValue<string>("oems_updateattendingasdata")
                    }
                ).ToList();

                var now = DateTime.Now.Date;
                var finalResults = new List<Registrant>();
                foreach (var res in results)
                {
                    var validMember = true;
                    var executivePresidents =
                    (
                        from a in serviceContext.mbr_organizationcontactSet
                        where a.mbr_ContactId != null && a.mbr_ContactId.Id == res.ContactID
                            && a.mbr_LocalId != null && a.mbr_Role != null && a.statecode == 0
                        select a
                    ).ToList();

                    executivePresidents = executivePresidents.Where(a => a.GetAttributeValue<EntityReference>("mbr_localid").Name.Trim() == "ETFO - Provincial Office").ToList();

                    if (executivePresidents.Count > 0)
                    {
                        validMember = executivePresidents.Any(m => m.mbr_EffectiveStartDate != null && m.mbr_EffectiveStartDate.Value <= now
                                                                && m.mbr_EffectiveEndDate != null && m.mbr_EffectiveEndDate.Value >= now);
                    }

                    if (validMember)
                    {
                        if (!string.IsNullOrWhiteSpace(res.AttendingAsJSON))
                        {
                            var jsonAttendingAs = JsonConvert.DeserializeObject<AttendingAs>(res.AttendingAsJSON);
                            jsonAttendingAs.attendingAsList.ForEach(x => x.otherInput = res.AttendingAsOther);

                            res.AttendingAsJSON = JsonConvert.SerializeObject(jsonAttendingAs);
                        }

                        finalResults.Add(res);
                    }
                }

                if (finalResults.Any())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, finalResults);
                }
                else
                {
                    string noRegistrants = XrmHelper.GetSnippetValueOrDefault("ManageLocalDelegation/ErrorMsg/NoRegistrants", "No users have registered for this event.");
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, noRegistrants);
                }
            }

            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }

        [HttpPost]
        public HttpResponseMessage SaveDelegationRegistrants(object data)
        {
            var items = JsonConvert.DeserializeObject<List<Tuple<Guid, bool, string>>>(data.ToString());

            using (var serviceContext = new XrmServiceContext())
            {
                foreach (var val in items)
                {
                    var currentReg = new Entity("oems_eventregistration");
                    currentReg.Id = val.Item1;
                    currentReg["oems_localdelegationflag"] = val.Item2;
                    currentReg["oems_updateattendingasdata"] = val.Item3;

                    serviceContext.Attach(currentReg);
                    serviceContext.UpdateObject(currentReg);
                }

                serviceContext.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        [HttpPost]
        public HttpResponseMessage SubmitDelegation(Guid id, object data)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var delegation = serviceContext.oems_EventLocalSet.FirstOrDefault(el => el.Id == id);
                if (delegation != null)
                {
                    //All data
                    var items = data != null ? JsonConvert.DeserializeObject<List<Tuple<Guid, bool, string, string>>>(data.ToString()) : null;
                    if (items != null && items.Count > 0)
                    {
                        //Email variables
                        var subject = XrmHelper.GetSnippetValueOrDefault("Omniware/ApproveDelegationEmailSubject", "Local Delegation Change");
                        var fromUser = serviceContext.SystemUserSet.FirstOrDefault(u => u.FullName == XrmHelper.GetSiteSettingValueOrDefault("SystemEmail/Defaults/EmailFrom", "Admin User"));
                        List<Email> delegationEmails = new List<Email>();

                        //Modified Items
                        var modifiedItems = (from modifiedItem in items where modifiedItem.Item4 != "normal" select modifiedItem).ToList();
                        foreach (var val in modifiedItems)
                        {
                            //Update event registration
                            var currentReg = new Entity("oems_eventregistration");
                            currentReg.Id = val.Item1;
                            currentReg["oems_localdelegationflag"] = val.Item2;
                            currentReg["oems_updateattendingasdata"] = val.Item3;
                            currentReg["oems_updateattendingasdatalastapproval"] = val.Item3;

                            serviceContext.Attach(currentReg);
                            serviceContext.UpdateObject(currentReg);

                            //Create notifications emails
                            var email = GetVotingEmail(fromUser, subject, val.Item1, val.Item3);
                            if (email != null)
                            {
                                delegationEmails.Add(email);
                            }
                        }

                        //Other Items
                        var otherItems = (from savedItem in items where savedItem.Item4 == "normal" select savedItem).ToList();
                        foreach (var val in otherItems)
                        {
                            //Update event registration
                            var currentReg = new Entity("oems_eventregistration");
                            currentReg.Id = val.Item1;
                            currentReg["oems_updateattendingasdatalastapproval"] = val.Item3;

                            serviceContext.Attach(currentReg);
                            serviceContext.UpdateObject(currentReg);

                            //Create notifications emails
                            var email = GetVotingEmail(fromUser, subject, val.Item1, val.Item3);
                            if (email != null)
                            {
                                delegationEmails.Add(email);
                            }
                        }

                        delegation.statuscode = 347780000;
                        serviceContext.UpdateObject(delegation);
                        serviceContext.SaveChanges();

                        //Send the emails
                        foreach (var email in delegationEmails)
                        {
                            try
                            {
                                var mailContext = XrmHelper.GetContext();
                                mailContext.AddObject(email);
                                mailContext.SaveChanges();

                                var trackingTokenEmailRequest = new GetTrackingTokenEmailRequest();
                                var trackingTokenEmailResponse =
                                    (GetTrackingTokenEmailResponse)serviceContext.Execute(trackingTokenEmailRequest);

                                var sendEmailreq = new SendEmailRequest
                                {
                                    EmailId = email.Id,
                                    TrackingToken = trackingTokenEmailResponse.TrackingToken,
                                    IssueSend = true
                                };

                                var sendEmailresp = (SendEmailResponse)serviceContext.Execute(sendEmailreq);
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        private Email GetVotingEmail(SystemUser fromUser, string subject, Guid registrationId, string attendingAs)
        {
            var originalContext = XrmHelper.GetContext();
            var originalReg = (from r in originalContext.CreateQuery("oems_eventregistration")
                               join evt in originalContext.CreateQuery("oems_event") on r.GetAttributeValue<EntityReference>("oems_event").Id equals evt.GetAttributeValue<Guid>("oems_eventid")
                               join con in originalContext.CreateQuery("contact") on r.GetAttributeValue<EntityReference>("oems_contact").Id equals con.GetAttributeValue<Guid>("contactid")
                               join loc in originalContext.CreateQuery("oems_location") on evt.GetAttributeValue<EntityReference>("oems_eventlocation").Id equals loc.GetAttributeValue<Guid>("oems_locationid")
                               where r.GetAttributeValue<Guid>("oems_eventregistrationid") == registrationId
                               select new
                               {
                                   Reg = r,
                                   Evt = evt,
                                   Con = con,
                                   Loc = loc
                               }).FirstOrDefault();

            if (fromUser != null && originalReg != null && originalReg.Reg.GetAttributeValue<string>("oems_updateattendingasdatalastapproval") != attendingAs)
            {
                //Values for Subject
                var attendingNew = JsonConvert.DeserializeObject<LocalDelegationUpdate>(attendingAs);

                //Revisar que si no es voting no hace nada
                var newVotingRole = (from selected in attendingNew.attendingAsList where selected.isVotingRole && selected.isSelected select selected.name).FirstOrDefault();
                if (newVotingRole != null)
                {
                    //Body
                    var bodyDefaultValue =
                        "<p>Dear <b>{0}</b>:</p>" +
                        "<p>Your status has been changed and you are now attending as a <b>{1}</b> for the following ETFO Event:</p>" +
                        "<p><b>Event Name</b>: {2}</p>" +
                        "<p><b>Event Date</b>: {3} to {4}</p>" +
                        "<p><b>Event Location</b>: {5}</p>";

                    var userName = originalReg.Reg.GetAttributeValue<EntityReference>("oems_contact").Name;
                    var eventName = originalReg.Evt.GetAttributeValue<string>("oems_eventname");
                    var startDate = originalReg.Evt.GetAttributeValue<DateTime>("oems_startdate").ToString("dddd, MMMM dd, yyyy");
                    var endDate = originalReg.Evt.GetAttributeValue<DateTime>("oems_enddate").ToString("dddd, MMMM dd, yyyy");
                    var location = originalReg.Loc.GetAttributeValue<string>("oems_locationname");

                    var body = XrmHelper.GetSnippetValueOrDefault("Omniware/ApproveDelegationEmailBody", bodyDefaultValue);
                    body = string.Format(body, userName, newVotingRole, eventName, startDate, endDate, location);

                    //Email info
                    originalReg.Con.Id = originalReg.Con.GetAttributeValue<Guid>("contactid");
                    var emailTo = new ActivityParty { PartyId = originalReg.Con.ToEntityReference() };
                    var emailFrom = new ActivityParty { PartyId = fromUser.ToEntityReference() };
                    var htmlBody = new StringBuilder();
                    htmlBody.Append(body);

                    var email = new Email
                    {
                        To = new[] { emailTo },
                        From = new[] { emailFrom },
                        Subject = subject,
                        Description = htmlBody.ToString()
                    };

                    return email;
                }
            }

            return null;
        }

        public HttpResponseMessage GetSnippetValue(string id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, XrmHelper.GetSnippetValueOrDefault(id));
        }

        private bool GetVotingStatus(Guid Registration)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var votingRoles = serviceContext.oems_eventregistration_attending_asSet
                            .Join(serviceContext.oems_attendingasoptionSet, aaEr => aaEr.oems_attendingasoptionid, aa => aa.oems_attendingasoptionId, (aaEr, aa) => new { AttendingAsRegistration = aaEr, AttendingAsOption = aa })
                            .Where(aos => aos.AttendingAsRegistration.oems_eventregistrationid == Registration)
                            .Where(aos => aos.AttendingAsOption.oems_VotingRoleFlag.Value == true)
                            .Select(x => x.AttendingAsOption.oems_attendingasoptionId).ToList();
                return votingRoles.Any();
            }
        }

        private Entity GetContact()
        {
            var ctx = XrmHelper.GetContext();
            var portal = PortalCrmConfigurationManager.CreatePortalContext();
            //var account = ctx.Retrieve(Xrm.Account.EntityLogicalName, (portal.User as Contact).ParentCustomerId.Id, new ColumnSet(true)).ToEntity<Xrm.Account>();

            return portal.User as Contact;
        }
    }
}