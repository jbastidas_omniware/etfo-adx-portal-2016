﻿using System;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Collections.Generic;
using Adxstudio.Xrm.Web.Mvc;
using Microsoft.Xrm.Portal;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Portal.IdentityModel.Configuration;
using Microsoft.Xrm.Sdk;
using Xrm;

namespace Site.Controls
{
    public class PortalUserControl : PortalViewUserControl
    {
        private readonly Lazy<XrmServiceContext> _xrmContext;

        public PortalUserControl()
        {
            _xrmContext = new Lazy<XrmServiceContext>(CreateXrmServiceContext);
        }

        public XrmServiceContext XrmContext
        {
            get { return _xrmContext.Value; }
        }

        public IPortalContext Portal
        {
            get { return PortalCrmConfigurationManager.CreatePortalContext(PortalName); }
        }

        public XrmServiceContext ServiceContext
        {
            get { return Portal.ServiceContext as XrmServiceContext; }
        }

        public Adx_website Website
        {
            get { return Portal.Website as Adx_website; }
        }

        public Contact Contact
        {
            get { return Portal.User as Contact; }
        }

        public Entity Entity
        {
            get { return Portal.Entity; }
        }

        protected void RedirectToLoginIfAnonymous()
        {
            if (!Request.IsAuthenticated)
            {
                var loginPage = ServiceContext.GetPageBySiteMarkerName(Website, "Login");

                var settings = FederationCrmConfigurationManager.GetUserRegistrationSettings();
                var returnUrlKey = settings.ReturnUrlKey ?? "returnurl";
                Response.Redirect(ServiceContext.GetUrl(loginPage).AppendQueryString(returnUrlKey, Request.Url.PathAndQuery));
            }
        }

        private XrmServiceContext CreateXrmServiceContext()
        {
            return PortalCrmConfigurationManager.CreateServiceContext(PortalName) as XrmServiceContext;
        }
    }
}