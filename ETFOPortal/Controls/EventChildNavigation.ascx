﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventChildNavigation.ascx.cs" Inherits="Site.Controls.EventChildNavigation" %>

<div class="event-page-children">
    <% if (ShowChildren && Children.Any())
       {%>
    <%--<h4>
			<i class="icon-folder-open"></i>
			<crm:Snippet SnippetName="Page Children Heading" DefaultText="In This Section" EditType="text" runat="server"/>
		</h4>--%>
    <ul class="etfo-event-nav nav nav-tabs nav-stacked">
        <% foreach (var node in Children)
           { %>
        <% if (node.Title == "Announcements")
           { %>
        <li class="item-orange">
            <% }
           else if (node.Title == "Blog")
           { %>
        <li class="item-lightpurple">
            <% }
           else if (node.Title == "Contact Us")
           { %>
        <li class="item-darkblue">
            <% }
           else if (node.Title == "Documents")
           { %>
        <li class="item-pink">
            <% }
           else if (node.Title == "Features")
           { %>
        <li class="item-darkteal">
            <% }
           else if (node.Title == "Media Room")
           { %>
        <li class="item-teal">
            <% }
           else if (node.Title == "Photos")
           { %>
        <li class="item-purple">
            <% }
           else if (node.Title == "Videos")
           { %>
        <li class="item-green">
            <% }
           else
           { %>
        <li class="item-grey">
            <% } %>
            <a href="<%= node.Url %>">
                <%= node.Title %>
            </a>
        </li>
        <% } %>
    </ul>
    <% } %>
    <% if (ShowShortcuts && Shortcuts.Any())
       {%>
    <h4>
        <i class="icon-share"></i>
        <crm:Snippet SnippetName="Page Related Heading" DefaultText="Quick Links" EditType="text" runat="server" />
    </h4>
    <ul class="etfo-event-nav nav nav-tabs nav-stacked">
        <% foreach (var node in Shortcuts)
           { %>
        <li class="item-grey">
            <a href="<%= node.Url %>">
                <%= node.Title %>
            </a>
        </li>
        <% } %>
    </ul>
    <% } %>
</div>
<%--<div class="event-sidebar-content">
    <asp:Literal runat="server" ID="EventSideBar"/>
</div>--%>
