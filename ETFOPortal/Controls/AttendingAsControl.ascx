﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttendingAsControl.ascx.cs" Inherits="Site.Controls.AttendingAsControl" %>
<link type="text/css" href="~/css/AttendingAsControl.css" rel="stylesheet" />

<asp:HiddenField runat="server" ID="attendingAsValues"  Visible="true" OnValueChanged="AttendingAsValues_TextChanged"/>
<asp:HiddenField runat="server" ID="registrationEventId" Visible="true" />


<div id="attendingAsContainer" runat="server" class="AttendingAsContainer">

    <div id="attendingAsBoxes" class="AttendingAsBoxes" runat="server">
    </div>
    <div class="AttendingAsEdit">
        <a style="text-decoration: none; cursor: pointer" id="editattending" class="icon-pencil-square-o icon-large greyiconcolor"></a>
    </div>
</div>

<script>
    $("#<%= this.attendingAsContainer.ClientID %>").ready(function () {
        $("#<%= this.attendingAsContainer.ClientID %> #editattending")
        .data("model", JSON.parse($("#<%= this.attendingAsValues.ClientID%>").val()))
        .click(function () {

            ShowCustomModalAjax(function () {
                return $.ajax({
                    url: "~/Controls/AttendingAs.htm"
                });

            }, BindModel, SaveModel, CancelEdit);

            function CancelEdit() {
                $("#<%= this.attendingAsContainer.ClientID %> #editattending").data("model", JSON.parse($("#<%= this.attendingAsValues.ClientID%>").val()))
                UnbindModel();
            }

            function MyViewModel() {
                var self = this;
                self.ShowHideOtherTextBox = function (element, e) {
                    var textbox = $(".OtherTextBox", $(e.currentTarget).parent());
                    if (element.isSelected) {
                        textbox.css("display", "inline");
                        textbox.focus();
                    }
                    else {
                        textbox.css("display", "none");
                        textbox.val("");
                        element.otherInput = null;
                    }
                    return true;
                };
                self.ValidateOtherContent = function (element, e) {
                    var checkbox = $("input[type='checkbox']", $(e.currentTarget).parent());
                    if (element.otherInput == null || element.otherInput == "") {
                        element.isSelected = false;
                        checkbox.click();
                    }
                    return true;
                };
                self.CustomClick = function (element, e) {
                    var myModel = $("#<%= this.attendingAsContainer.ClientID %> #editattending").data("model");
                    var checkbox = $(e.currentTarget);
                    for (var i = 0; i < myModel.attendingAsList.length; i++) {
                        var mValue = myModel.attendingAsList[i];
                        if (mValue.isVotingRole) {
                            if (mValue.id == checkbox.val()) {
                                mValue.isSelected = true;
                                checkbox.checked = true;
                            }
                            else {
                                var chk = document.getElementById(mValue.id);
                                mValue.isSelected = false;
                                chk.checked = false;
                            }
                        }
                    }
                    return true;
                }
                this.main = $("#<%= this.attendingAsContainer.ClientID %> #editattending").data("model");
                ko.observable(this.main);
            }

            function UnbindModel() {
                ko.cleanNode($("#customModal")[0]);
            }

            function BindModel() {c
                var viewModel = new MyViewModel();

                try {
                    ko.applyBindings(viewModel, $("#customModal")[0]);
                } catch (e) {
                    //Check json for nulls
                }
            }

            function SaveModel() {
                UnbindModel();
                var model = $("#<%= this.attendingAsContainer.ClientID %> #editattending").data("model");

                $("#<%= this.attendingAsValues.ClientID %>")
                    .val(JSON.stringify(model))
                    .change();

                var boxesContainer = $("#<%=this.attendingAsBoxes.ClientID%>");

                boxesContainer.html("");

                for (var ii = 0; ii < model.attendingAsList.length; ii++) {
                    var item = model.attendingAsList[ii];
                    if (item.isSelected) {
                        var name = item.name;
                        if (item.isOther) {
                            name = item.name + ": " + item.otherInput;
                        }
                        var label = $("<span class=\"btn-primary AttendingAsBox\" style=\"opacity:0;\" >" + name + "</span>");
                        boxesContainer.append(label);
                        label.css({
                            backgroundImage: "none",
                            backgroundColor: item.colour ? "#" + item.colour : ""
                        }).animate({
                            opacity: 1
                        }, {
                            duration: 500
                        });
                    }
                }
            }
        })
    });
</script>
