﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Comments.ascx.cs" Inherits="Site.Controls.Comments" %>
<%@ Import Namespace="Adxstudio.Xrm.Cms" %>
<%@ Import Namespace="Site.Helpers" %>
<%@ Register src="~/Controls/CommentCreator.ascx" tagname="CommentCreator" tagprefix="crm" %>
<%@ Register src="~/Controls/MultiRatingControl.ascx" tagname="MultiRatingControl" tagprefix="adx" %>

<asp:ObjectDataSource ID="CommentDataSource" TypeName="Adxstudio.Xrm.Cms.ICommentDataAdapter" OnObjectCreating="CreateCommentDataAdapter" SelectMethod="SelectComments" SelectCountMethod="SelectCommentCount" runat="server" />
<asp:ListView ID="CommentsView" DataSourceID="CommentDataSource" runat="server">
	<LayoutTemplate>
		<div class="comments">
			<legend>
				<asp:Literal Text='<%$ Snippet: Comments Heading, Comments %>' runat="server"></asp:Literal>
			</legend>
			<ul class="list-unstyled">
				<li id="itemPlaceholder" runat="server" />
			</ul>
		</div>
	</LayoutTemplate>
	<ItemTemplate>
		<li id="Li1" class='<%# ((bool)Eval("IsApproved")) ? "approved" : "unapproved" %>' runat="server">
			<asp:Panel ID="Panel1" CssClass="alert alert-block alert-info" Visible='<%# !(bool)Eval("IsApproved") %>' runat="server" >
				<asp:Label ID="Label1"  Text='<%$ Snippet: Unapproved Comment Message, This comment is not yet approved. It is only visible to you. %>' runat="server"></asp:Label>
			</asp:Panel>
			<asp:Panel ID="Panel2"  Visible='<%# Eval("Editable") ?? false %>' CssClass='<%# Eval("Entity.LogicalName", "post-comment-controls xrm-entity xrm-editable-{0}")%>' runat="server">
				<div class="btn-group">
					<a class="btn xrm-edit"><i class="fa fa-cog" aria-hidden="true"></i></a>
					<a class="btn dropdown-toggle" data-toggle="dropdown">
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a href="#" class="xrm-edit"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a>
						</li>
						<li>
							<a href="#" class="xrm-delete"><i class="fa fa-remove" aria-hidden="true"></i> Delete</a>
						</li>
					</ul>
				</div>
				<asp:HyperLink ID="HyperLink1" NavigateUrl='<%# Eval("EditPath.AbsolutePath") %>' CssClass="xrm-entity-ref" style="display:none;" runat="server"/>
				<asp:HyperLink ID="HyperLink2" NavigateUrl='<%# Eval("DeletePath.AbsolutePath") %>' CssClass="xrm-entity-delete-ref" style="display:none;" runat="server"/>
			</asp:Panel>
			<div class="pull-right">
				<adx:MultiRatingControl ID="Rating"
					EnableRatings='<%# Eval("RatingEnabled") %>'
					SourceID='<%# Eval("Entity.Id") %>' 
					LogicalName='<%# Eval("Entity.LogicalName") %>' 
					RatingInfo='<%# Bind("RatingInfo") %>' 
					RatingType="rating" 
					InitEventName="OnDataBinding"
					runat="server" />
			</div>
			<h4>
				<i class="fa fa-comment" aria-hidden="true"></i>
				<asp:HyperLink ID="HyperLink3" rel="nofollow" NavigateUrl="<%# Url.AuthorUrl(Container.DataItem as IComment) %>" Text='<%# HttpUtility.HtmlEncode(Eval("Author.DisplayName") ?? "") %>' runat="server"></asp:HyperLink>
				<small>
					&ndash;
					<abbr class="timeago"><%# Eval("Date", "{0:r}") %></abbr>
				</small>
			</h4>
			<div><%# Eval("Content") %></div>
		</li>	</ItemTemplate>
</asp:ListView>

<asp:Panel ID="NewCommentPanel" runat="server">
	<crm:CommentCreator ID="NewCommentCreator" runat="server" />
</asp:Panel>
