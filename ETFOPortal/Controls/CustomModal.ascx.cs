﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Site.Controls
{
    public partial class CustomModal : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string jsname = "ModalJS";
            string jsurl = "~/js/CustomModal.js";

            var cs = Page.ClientScript;

            if (!cs.IsClientScriptIncludeRegistered(this.GetType(), jsname))
                cs.RegisterClientScriptInclude(this.GetType(), jsname, ResolveClientUrl(jsurl));
        }
    }
}