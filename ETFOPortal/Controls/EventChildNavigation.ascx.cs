﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Portal;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Portal.Web;
using Site.Helpers;

namespace Site.Controls
{
	public partial class EventChildNavigation : PortalUserControl
	{
        private readonly Lazy<IPortalContext> _portal = new Lazy<IPortalContext>(() => PortalCrmConfigurationManager.CreatePortalContext());
        
        private readonly Lazy<Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>> _childNodes;

		public EventChildNavigation()
		{
			_childNodes = new Lazy<Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>>(GetChildNodes, LazyThreadSafetyMode.None);
			ShowChildren = true;
			ShowShortcuts = true;
		}

		public string Exclude { get; set; }

		public bool ShowChildren { get; set; }

		public bool ShowShortcuts { get; set; }

		protected IEnumerable<SiteMapNode> Children
		{
			get { return _childNodes.Value.Item1; }
		}

		protected IEnumerable<SiteMapNode> Shortcuts
		{
			get { return _childNodes.Value.Item2; }
		}

		private Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>> GetChildNodes()
		{
		    var context = XrmHelper.GetContext();

            var webPage = context.Adx_webpageSet.FirstOrDefault(wp => wp.Adx_webpageId == _portal.Value.Entity.Id);
            if (webPage == null)
                return new Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>(new SiteMapNode[] { }, new SiteMapNode[] { });

            if (webPage.oems_EventId == null)
                return new Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>(new SiteMapNode[] { }, new SiteMapNode[] { });

            var @event = context.oems_EventSet.FirstOrDefault(ev => ev.oems_EventId == webPage.oems_EventId.Id);
            if (@event == null)
                return new Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>(new SiteMapNode[] { }, new SiteMapNode[] { });

		    //EventSideBar.Text = @event.oems_EventSidebar;

            var eventParentPage = context.Adx_webpageSet.FirstOrDefault(wp => wp.Adx_webpageId == @event.oems_WebPage.Id);
            if (eventParentPage == null) return new Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>(new SiteMapNode[] { }, new SiteMapNode[] { });

			var currentNode = SiteMap.Provider.FindSiteMapNodeFromKey(ServiceContext.GetUrl(eventParentPage));

			if (currentNode == null)
			{
				return new Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>(new SiteMapNode[] {}, new SiteMapNode[] {});
			}

			var excludeLogicalNames = string.IsNullOrEmpty(Exclude)
				? new HashSet<string>(StringComparer.InvariantCultureIgnoreCase)
				: new HashSet<string>(Exclude.Split(',').Select(name => name.Trim()), StringComparer.InvariantCultureIgnoreCase);

            var blogNodes = new List<SiteMapNode>();
			var shortcutNodes = new List<SiteMapNode>();
			var otherNodes = new List<SiteMapNode>();

			foreach (SiteMapNode childNode in currentNode.ChildNodes)
			{
				var entityNode = childNode as CrmSiteMapNode;

				if (entityNode != null && excludeLogicalNames.Any(entityNode.HasCrmEntityName))
				{
					continue;
				}

                if (entityNode != null && entityNode.Title == "Carousel")
			        continue;


                if (entityNode != null && entityNode.HasCrmEntityName("adx_shortcut"))
                {
                    shortcutNodes.Add(childNode);

                    continue;
                }

				if (entityNode != null && entityNode.HasCrmEntityName("adx_blog"))
				{
                    blogNodes.Add(childNode);

					continue;
				}
				
				otherNodes.Add(childNode);
			}

            blogNodes.AddRange(otherNodes);
            otherNodes = blogNodes;

			return new Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>(otherNodes, shortcutNodes);
		}
	}
}