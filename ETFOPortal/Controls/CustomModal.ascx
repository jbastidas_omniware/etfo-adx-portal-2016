﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomModal.ascx.cs" Inherits="Site.Controls.CustomModal" %>
<link type="text/css" href="~/css/CustomModal.css" rel="stylesheet" />

<div id="customModal">
    <div class="CustomModalBackground"></div>
    <div class="CustomModal">
        <div id="modalContent">
        </div>
        <div id="modalButtons">
            <div class="ButtonsContainer">
                <input id="modalCancelButton" type="button" value="Cancel" class="btn btn-primary" />
                <input id="modalAcceptButton" type="button" value="Accept" class="btn btn-primary" />
            </div>
        </div>
    </div>
</div>
