﻿using Microsoft.Xrm.Sdk.Query;
using Site.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Xrm.Portal;
using Microsoft.Xrm.Client;
using Xrm;
using Newtonsoft.Json;

namespace Site.Controls
{
    public partial class AttendingAsControl : PortalUserControl
    {
        public String AttendingAsValues
        {
            get
            {
                return this.attendingAsValues.Value;
            }
            set
            {
                this.attendingAsValues.Value = value;
                this.CreateAttendingBoxes();
            }
        }

        public Guid RegistrationEventId
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.registrationEventId.Value) ? Guid.Empty : Guid.Parse(this.registrationEventId.Value);
            }
            set
            {
                this.registrationEventId.Value = value.ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void AttendingAsValues_TextChanged(object sender, EventArgs e)
        {
            oems_EventRegistration current = new oems_EventRegistration
            {
                oems_EventRegistrationId = RegistrationEventId,
                oems_UpdateAttendingAsData = AttendingAsValues
            };

            ServiceContext.Attach(current);
            ServiceContext.UpdateObject(current);
            ServiceContext.SaveChanges();
        }

        private void CreateAttendingBoxes()
        {
            this.attendingAsBoxes.Controls.Clear();

            dynamic jsonObj = JsonConvert.DeserializeObject(this.attendingAsValues.Value);
            if (jsonObj != null && jsonObj.attendingAsList != null)
            {
                foreach (var item in jsonObj.attendingAsList)
                {
                    if (item.isSelected)
                    {
                        var label = new Label()
                        {
                            ID = string.Format("{0}_{1}", this.RegistrationEventId, item.id),
                            Text = item.isOther ? string.Format("Other: {0}", item.otherInput ?? string.Empty) : item.name,
                            CssClass = "btn-primary AttendingAsBox"
                        };
                        if (item.colour != null)
                        {
                            label.Style.Add(HtmlTextWriterStyle.BackgroundColor, string.Format("#{0}", item.colour));
                            label.Style.Add(HtmlTextWriterStyle.BackgroundImage, "url('')");
                        }
                        this.attendingAsBoxes.Controls.Add(label);
                    }
                }
            }
        }

        private string GetCRMAttendingAsValue()
        {
            //return "{\"sectionMessage\":\"I am attending Representative Council as:\",\"attendingAsList\":[{\"isSelected\":true,\"id\":\"7c7570b0-5c63-e311-af5c-22000ab9871c\",\"selected\":null,\"name\":\"President\",\"isVotingRole\":true,\"isGuestRole\":false,\"isOther\":false,\"otherInput\":null,\"colour\":\"6283F0\"},{\"isSelected\":true,\"id\":\"7e7570b0-5c63-e311-af5c-22000ab9871c\",\"selected\":null,\"name\":\"Additional Local Representative\",\"isVotingRole\":true,\"isGuestRole\":false,\"isOther\":false,\"otherInput\":null,\"colour\":\"6283F0\"},{\"isSelected\":true,\"id\":\"807570b0-5c63-e311-af5c-22000ab9871c\",\"selected\":null,\"name\":\"DECE President\",\"isVotingRole\":true,\"isGuestRole\":false,\"isOther\":false,\"otherInput\":null,\"colour\":\"6283F0\"},{\"isSelected\":true,\"id\":\"827570b0-5c63-e311-af5c-22000ab9871c\",\"selected\":null,\"name\":\"Local Observer\",\"isVotingRole\":false,\"isGuestRole\":false,\"isOther\":false,\"otherInput\":null,\"colour\":\"6283F0\"},{\"isSelected\":true,\"id\":\"847570b0-5c63-e311-af5c-22000ab9871c\",\"selected\":null,\"name\":\"Individual Observer\",\"isVotingRole\":false,\"isGuestRole\":false,\"isOther\":false,\"otherInput\":null,\"colour\":\"6283F0\"},{\"isSelected\":false,\"id\":\"867570b0-5c63-e311-af5c-22000ab9871c\",\"selected\":null,\"name\":\"Member of  Designated Group\",\"isVotingRole\":false,\"isGuestRole\":false,\"isOther\":false,\"otherInput\":null,\"colour\":\"6283F0\"}]}";

            var value = string.Empty;

            var eventRegistration = ServiceContext.oems_EventRegistrationSet.FirstOrDefault(e => e.Id.Equals(this.RegistrationEventId));

            if (eventRegistration != null)
            {
                value = eventRegistration.oems_RetrieveAttendingAsData;

                if (value == null)
                    value = "{\"sectionMessage\":\"\",\"attendingAsList\":[]}";
            }

            return value;
        }
    }
}