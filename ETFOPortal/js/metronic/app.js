﻿/**
Core script to handle the entire theme and core functions
**/
var App = function () {

    //* BEGIN:CORE HANDLERS *//

    // Handles the horizontal menu
    var handleHorizontalMenu = function() {
        //handle hor menu search form toggler click
        $('header').on('click', '.nav-container .hor-menu-search-form-toggler', function (e) {
            if ($(this).hasClass('off')) {
                $(this).removeClass('off');
                $('header .nav-container .search-form').hide();
            } else {
                $(this).addClass('off');
                $('header .nav-container .search-form').show();
            }
            e.preventDefault();
        });

        //handle hor menu search button click
        $('header').on('click', '.nav-container .search-form .btn', function (e) {
            $('.form-search').submit();
            e.preventDefault();
        });

        //handle hor menu search form on enter press
        $('header').on('keypress', '.nav-container .search-form input', function (e) {
            if (e.which == 13) {
                $('.form-search').submit();
                return false;
            }
        });
    };

    //* END:CORE HANDLERS *//

    return {

        //main function to initiate the theme
        init: function () {

            handleHorizontalMenu(); // handles horizontal menu
        }

    };

}();