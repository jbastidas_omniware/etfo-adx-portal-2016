﻿var viewModel;
var model;
var urlfile;
var controlId;

var loaded = false;

$(document).ready(function ()
{
    ensureTemplates(urlfile, controlId);
});

ko.bindingHandlers.CustomLocation =
{
    init: function (element, valueAccessor, allBindingsAccessor)
    {
        var $el = $(element);
        var value = ko.utils.unwrapObservable(valueAccessor());

        $el.append(value);
    }
}

ko.bindingHandlers.CustomEmail =
{
    init: function (element, valueAccessor, allBindingsAccessor)
    {
        var $el = $(element);
        var value = ko.utils.unwrapObservable(valueAccessor());

        if (value != null)
        {
            if (value != "")
            {
                $el.append(' (E) ');
                $el.append(value);
            }
        }
    }
}

ko.bindingHandlers.CustomTelephone =
{
    init: function (element, valueAccessor, allBindingsAccessor)
    {
        var $el = $(element);
        var value = ko.utils.unwrapObservable(valueAccessor());

        if (value != null)
        {
            if (value != "")
            {
                $el.append(' (T) ');
                $el.append(value);
            }
        }
    }
}

ko.bindingHandlers.CustomFax =
{
    init: function (element, valueAccessor, allBindingsAccessor)
    {
        var $el = $(element);
        var value = ko.utils.unwrapObservable(valueAccessor());

        if (value != null)
        {
            if (value != "") {
                $el.append(' (F) ');
                $el.append(value);
            }
        }
    }
}

function ensureTemplates(filename, controlName)
{
    jQuery.ajax(
    {
        url: filename,
        success: function (html)
        {
            $(controlName).append(html);
            ForceToApplyBinding(model);
        },
        async: false
    });
}

function MyViewModel(attendingasModel)
{
    var self = this;
    self.main = attendingasModel;
    ko.observable(attendingasModel);

    self.updatemodel = function ()
    {
        UpdateCRMValue();
    }

    self.openPopup = function ()
    {
        ShowDialog();
    };
}

//var viewModel = new MyViewModel();
//ko.applyBindings(viewModel);

function ForceToApplyBinding(amodel)
{
    viewModel = new MyViewModel(amodel);
    ko.applyBindings(viewModel);
}