﻿var ajaxCalls = [];

$("#customModal")
    .ready(function () {
        //$(this)
        //    .click(function () {
        //        HideCustomModal();
        //    });
        $("#modalAcceptButton, #modalCancelButton")
            .on("click.DefaultActions", function (e) {
                e.stopPropagation();
                e.preventDefault();
                HideCustomModal();
            });
    });

function ShowCustomModal(content, top, okCallback, cancelCallback) {
    if (!top) {
        top = "40%";
    }

    var customModalContainer = $("#customModal");
    var customModal = $(".CustomModal");
    var customModalBackground = $(".CustomModalBackground");
    if (customModalContainer && customModal && customModalBackground) {
        $("body").css("overflow", "hidden");
        customModalContainer
            .css("display", "block");
        customModalBackground
            .animate({
                opacity: .6
            }, {
                duration: 200,
                complete: function () {
                    FillCustomModal(content, false, null, okCallback, cancelCallback);
                    customModal
                        .animate({
                            opacity: 1,
                            top: top
                        }, {
                            duration: 500,
                            complete: function () {
                                CreateAndAppendClose($(this));
                            }
                        });
                }
            });
    }
}

function FillCustomModal(content, reposition, readyCallback, okCallback, cancelCallback) {
    var customModal = $(".CustomModal");
    var modalContent = $("#modalContent", customModal);
    var buttonsHeight = 0;
    if (okCallback || cancelCallback) {
        buttonsHeight = $("#modalButtons").height() - 20;

        if (okCallback) {
            $("#modalAcceptButton")
                .off("click.CustomEvents")
                .on("click.CustomEvents", okCallback);
        }
        if (cancelCallback) {
            $("#modalCancelButton")
                .off("click.CustomEvents")
                .on("click.CustomEvents", cancelCallback);
        }
    }

    if (customModal) {
        content.css({
            display: "none",
            opacity: 0
        });
        modalContent.html(content);
        if (readyCallback) {
            readyCallback();
        }
        var top = ($(window).height() / 2) - ((content.height() + buttonsHeight) / 2);
        if (top < 0) {
            top = 0;
            customModal.css("margin", "40px auto")
        }
        else {
            customModal.css("margin", "")
        }
        if (reposition) {
            customModal.animate({
                top: top
            }, 200);
        }
        customModal.animate({
            width: content.width(),
            height: content.height() + buttonsHeight,
        }, {
            duration: 400,
            complete: function () {
                content
                    .css("display", "block")
                    .animate({
                        opacity: 1
                    }, {
                        duration: 200
                    });
                if (okCallback || cancelCallback) {
                    $("#modalButtons")
                        .css("display", "block");
                }
            }
        });
        customModal.children().click(function (e) {
            e.stopPropagation();
            //e.preventDefault();
        });
    }
}


function ShowCustomModalAjax(ajaxCall, readyCallback, okCallback, cancelCallback) {
    ShowCustomModal($("<div>").addClass("LoadingModal"));
    if (ajaxCall) {
        var call = ajaxCall();
        ajaxCalls.push(call);
        call
            .done(function (data) {
                var html = $.parseHTML(data, null, true);
                data = $("<div>").html(html);
                setTimeout(function () { FillCustomModal(data, true, readyCallback, okCallback, cancelCallback) }, 500);
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                if (errorThrown != "abort") {
                    setTimeout(function () { FillCustomModal(BuildErrorMessage(), true) }, 500);
                }
            });
    }
}

function HideCustomModal() {
    $.each(ajaxCalls, function (index, data) {
        data.abort();
    });
    $(".CustomModal")
        .stop(true)
        .animate({
            opacity: 0
        }, {
            duration: 200
        });
    $(".CustomModalBackground")
        .stop(true)
        .animate({
            opacity: 0
        }, {
            duration: 200,
            complete: function () {
                $("#customModal").hide();
                $("body").css("overflow", "auto");
                ResetModal();
            }
        });
}

function ResetModal() {
    var iframes = $(".CustomModal iframe");
    if (iframes.size() > 0) {
        iframes.attr("src", "about:blank");
        setTimeout(function () { $(".CustomModal").html(""); }, 200);
    }
    else {
        $(".CustomModal #modalContent").html("");
    }
    $("#modalButtons")
        .css("display", "none");
    $(".CustomModal")
         .css({
             top: "30%",
             opacity: 0
         })
}

function CreateAndAppendClose(modal) {
    var divContainer = $("<div class='FakeContainerRight'></div>")
    var close = $("<div class='CustomModalClose' title='Close'>X</div>");
    divContainer.append(close);
    modal.prepend(divContainer);
    close
        .animate({
            opacity: 1
        }, {
            duration: 400
        })
        .click(function (e) {
            $("#modalCancelButton").click();
            //e.stopPropagation();
            //e.preventDefault();
            //HideCustomModal();
        });
}

function BuildErrorMessage(message) {
    if (!message) {
        message = "Something went wrong :(. Please, wait a moment and try again.";
    }
    var message = $("<div><label>" + message + "</label></div>");
    message.addClass("CustomModalErrorMessage");
    var messageContainer = $("<div>");
    messageContainer
        .append(message)
        .addClass("CustomModalErrorMessageContainer");
    return messageContainer;
}