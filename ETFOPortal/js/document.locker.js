﻿(function (documentlocker, $, _, ko, undefined) {
    var model;
    var options;
    var validator;
    var bytesInMb = 1024000;

    String.prototype.format = function ()
    {
        var str = this;
        for (var i = 0; i < arguments.length; i++)
        {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            str = str.replace(reg, arguments[i]);
        }
        return str;
    };
    
    $(document).ajaxStop(function () {
        AjaxUpdateProgressHide();
    });
    
    function documentLockerModel(data)
    {
        ko.bindingHandlers.documentSize =
        {
            init: function (element, valueAccessor, allBindingsAccessor)
            {
                var $el = $(element);
                var value = ko.utils.unwrapObservable(valueAccessor());

                if (value != null)
                {
                    if (value !== "")
                    {
                        var size = parseInt(value) / bytesInMb;
                        $el.text(size.toFixed(2));
                    }
                }
            }
        };
        
        model = this;
        model.noData = data != null && data.length > 0 ? ko.observable(false) : ko.observable(true);
        model.data = ko.observableArray(data);
        model.getTotalText = ko.computed
        (
            function ()
            {
                var total = model.data().length;
                var size = 0;
                var all = model.data();
                for (var i = 0; i < all.length; i++)
                {
                    size += all[i].FileSize;
                }
                size = size / bytesInMb;

                var txt = options.texts.totalFiles + ': ' + total + ' - ' + options.texts.totalSizeMb + ': ' + size.toFixed(2);

                return txt;
            }
        );
        
        model.isUploadEnabled = ko.computed
        (
            function ()
            {
                return options.maxDocuments > model.data().length;
            }
        );
        
        model.downloadDocument = function (item)
        {
            AjaxUpdateProgressShow();
            $.fileDownload
            (
                options.downloadUrl + '/' + item.AnnotationId,
                {
                    httpMethod: 'POST',
                    cookieName: "OMWFileDownload",
                    successCallback: function (url)
                    {
                        //etfo.info(options.texts.modalTitle, "Your document has been successfully downloaded", null, null);
                        AjaxUpdateProgressHide();
                    },
                    failCallback: function (html, url)
                    {
                        etfo.info(options.texts.modalTitle, options.texts.errorDownloading, null, null);
                        AjaxUpdateProgressHide();
                    }
                }
            );
        };

        model.deleteDocument = function (item)
        {
            var fn = function()
            {
                $.ajax
                ({
                    url: options.deleteUrl,
                    type: "POST",
                    data:
                    {
                        id: item.AnnotationId
                    },
                    beforeSend: function ()
                    {
                        AjaxUpdateProgressShow();
                    },
                    success: function (response)
                    {
                        if (response.Success)
                        {
                            //etfo.info(options.texts.modalTitle, "Your document has been successfully deleted", null, null);
                            model.data.remove(item);
                        }
                        else if (response.Refresh)
                        {
                            window.location.reload();
                        }
                        else
                        {
                            etfo.info(options.texts.modalTitle, options.texts.errorDeleteting, null, null);
                        }
                    }
                });
            };
            
            etfo.confirm(options.texts.areYouSureFile, fn, null, options.texts.yes, options.texts.no, false);
        };
    }

    // PUBLIC FUNCTIONS
    documentlocker.load = function (opts)
    {
		var defaults =
        {
            maxDocuments: 0,
            documents: [],
            uploadUrl: null,
            deleteUrl: null,
            downloadUrl: null,
			maxSize: null,
			maxSizeMb: null,
            texts: null
        };

        options = _.extend(defaults, opts);
        options.maxSizeMb = (parseInt(options.maxSize) / bytesInMb).toFixed(2);
        
        ko.applyBindings(new documentLockerModel(options.documents));
        
        $("#document-locker-upload-form").removeData("validator");
        $("#document-locker-upload-form").removeData("unobtrusiveValidation");
        
        validator = $("#document-locker-upload-form").validate
        ({
            debug: true,
            rules:
            {
                description:
                {
                    required: true
                }
            }
        });

        $("input[name='file']").fileupload
        ({
            url: options.uploadUrl,
            dataType: 'json',
            autoUpload: false,
            //acceptFileTypes: /(\.|\/)(gif|jpe?g|png|doc?x|ppt?x|xls?x|pdf|txt|rtf)$/i,
            done: function (e, data)
            {
                if (data.result.Success)
                {
                    //etfo.info(options.texts.modalTitle, "Your document has been successfully uploaded", null, null);
                    data.files.splice(0, 1);
                    model.data.push(data.result.Document);
                    $(".selected-file").hide();
                    $("input[name='description']").val('');
                }
                else
                {
                    if(data.result.Message)
                    {
                        etfo.info(options.texts.modalTitle, data.result.Message, null, null);
                    }
                    if (data.result.NoFileSent)
                    {
                        $(".selected-file").hide();
                        data.files.splice(1, 1);
                    }
                    if (data.result.Refresh)
                    {
                        $(".selected-file").hide();
                        window.location.reload();
                    }
                }
                
                $('#dlprogress .progress-bar').css
                (
                    'width',
                    '0%'
                );
            },
            progressall: function (e, data)
            {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#dlprogress .progress-bar').css
                (
                    'width',
                    progress + '%'
                );
            },
            add: function (e, data)
            {
                $(".selected-file").text(data.files[0].name).show();
                $("#upload-tg").unbind("click");
                $("#upload-tg").click
                (
                    function ()
                    {
                        var valid = $("#document-locker-upload-form").valid();
                        if (valid)
                        {
                            if (data.files && data.files.length > 0)
                            {
                                if (data.files[0].size === 0)
                                {
                                    $(".selected-file").hide();
                                    etfo.info(options.texts.modalTitle, options.texts.documentZeroSize, null, null);
                                }
                                else if (data.files[0].size > options.maxSize)
                                {
                                    $(".selected-file").hide();
                                    etfo.info(options.texts.modalTitle, options.texts.documentTooBig.format(options.maxSizeMb), null, null);
                                }
                                else
                                {
                                    AjaxUpdateProgressShow();
                                    data.submit();
                                }
                            }
                            else
                            {
                                $(".selected-file").hide();
                                etfo.info(options.texts.modalTitle, options.texts.selectDocument, null, null);
                            }
                        }
                    }
                );
            }
        }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
        
        $("#document-locker-upload-form").bind
        (
            'fileuploadsubmit',
            function (e, data)
            {
                var valid = $("#document-locker-upload-form").valid();
                if (!valid)
                {
                    validator.focusInvalid();
                    data.form.find('button').prop('disabled', false);
                    return false;
                }
                
                data.formData = { description: $("input[name='description']").val() };
                return true;
            }
        );
    };
}(window.documentlocker = window.documentlocker || {}, jQuery, _, ko));