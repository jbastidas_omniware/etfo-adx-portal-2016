﻿(function (etfo, $, undefined) {
    // PUBLIC FUNCTIONS
    etfo.confirm = function (question, okCallback, cancelCallback, okText, cancelText, hideX) {
        var confirmModal = $('#confirmModal');
        var confirmOkCallback;
        var confirmCancelCallback;

        confirmModal.find('.okButton').unbind("click");
        confirmModal.find('.okButton').click(function (event) {
            if (confirmOkCallback) confirmOkCallback.call();
            confirmModal.modal('hide');
        });

        confirmModal.find('.cancelButton').unbind("click");
        confirmModal.find('.cancelButton').click(function (event) {
            if (confirmCancelCallback) confirmCancelCallback.call();
            confirmModal.modal('hide');
        });

        confirmModal.find('.confirmText').html(question);

        if (okText) {
            confirmModal.find('.okButton').html(okText);
        }
        
        if (cancelText) {
            confirmModal.find('.cancelButton').html(cancelText);
        }

        if (okCallback) {
            confirmOkCallback = okCallback;
            confirmCancelCallback = cancelCallback;
            confirmModal.find('.cancelButton').show();
        } else {
            confirmModal.find('.cancelButton').hide();
        }

        if (hideX)
        {
            confirmModal.find('.close').hide();
        }

        confirmModal.modal('show');
    };

    etfo.info = function (modalTitle, modalText, modalGoToText, modalGoToUrl, closeCallback)
    {
        var infoModal = $('#infoModal');
        var infoCloseCallback = closeCallback;
        infoModal.find("#modalTitle").html(modalTitle);
        infoModal.find("#modalMessage").html(modalText);
        infoModal.find("#modalGoTo").html(modalGoToText);

        infoModal.find("#modalGoTo").hide();
        if (modalGoToUrl)
        {
            infoModal.find("#modalGoTo").show();
            infoModal.find("#modalGoTo").click
            (
                function ()
                {
                    window.location.href = modalGoToUrl;
                }
            );
        }

        if (infoCloseCallback)
        {
            infoModal.find(".close-modal").click
            (
                function ()
                {
                    infoCloseCallback();
                }
            );
        }

        infoModal.modal('show');
    };
    
    etfo.errorAlert = function (text, container)
    {
        var html = '';
        html += '<div class="alert alert-danger alert-dismissible fade in" role="alert">';
        html += '   <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        html += '   <span>' + text + '</span>';
        html += '</div>';

        $(container).html(html);
    };
    
    etfo.successAlert = function (text, container)
    {
        var html = '';
        html += '<div class="alert alert-success alert-dismissible fade in" role="alert">';
        html += '   <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        html += '   <span>' + text + '</span>';
        html += '</div>';
        
        $(container).html(html);
    };

}(window.etfo = window.etfo || {}, jQuery));