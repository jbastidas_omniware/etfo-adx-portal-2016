﻿
var grid_data;

var functionCallBack;

function SetSelected(id) {
    if (id != null) {
        model = FindObjectInTopic(id);


        if (model.isSelected) {
            model.isSelected = false;
        }
        else {
            model.isSelected = true;
        }

        //Update model
        //mData.personalAccommodationList = grid_data;
        functionCallBack(grid_data);

    }
}

function EventSetSelected(id) {
    if (id != null) {
        model = FindObjectInTopic(id);


        if (model.isEventSelected) {
            model.isEventSelected = false;
        }
        else {
            model.isEventSelected = true;
        }

        //Update model
        //mData.personalAccommodationList = grid_data;
        functionCallBack(grid_data);

    }
}

function HotelSetSelected(id) {
    if (id != null) {
        model = FindObjectInTopic(id);


        if (model.isHotelSelected) {
            model.isHotelSelected = false;
        }
        else {
            model.isHotelSelected = true;
        }

        //Update model
        //mData.personalAccommodationList = grid_data;
        functionCallBack(grid_data);
    }
}

function SaveDetailsValue(element, id) {

    model = FindObjectInTopic(id);
    var updated = element.value;
    model.userSpecifiedDetails = updated;
    //mData.personalAccommodationList = grid_data;
    functionCallBack(grid_data);
}






function FindObjectInTopic(id) {

    for (var i = 0; i < grid_data.length; i++) {

        if (grid_data[i].id == id) {

            return grid_data[i];
        }
    }
}


function LoadJQGrid(grid_dataIn, tableName,fnCallBack) {

    jQuery(function ($) {
        var grid_selector = "#" + tableName;

        grid_data = grid_dataIn;
        functionCallBack = fnCallBack;

        jQuery(grid_selector).jqGrid({
            //direction: "rtl",

            //data: grid_data,
            datastr: grid_data,
            datatype: "jsonstring",
            //width: 100,
            height: 500,
            loadui: "disable",
            //colNames:[' ', 'ID','Last Sales','Name', 'Stock', 'Ship via','Notes'],
            colNames: ['id', '', 'Additional Details', 'Event', 'Hotel'],
            colModel: [
             { name: "id", width: 1, hidden: true, key: true },
            { name: "name", width: 35, resizable: true },
              { name: "detail", width: 45, resizable: true },
            { name: "event", width: 10, resizable: true },
            { name: "hotel", width: 10, resizable: true }
            ],

            //viewrecords : true,
            treeGrid: true,
            treeGridModel: "adjacency",
            cellEdit: true,

            ExpandColumn: "name",

            recreateForm: true,

            gridComplete: function () {

                var grid = jQuery("#" + tableName);


                var ids = grid.jqGrid('getDataIDs');

                for (var i = 0; i < ids.length; i++) {


                    var rowId = ids[i];
                    var checkOut = "<input  " + "type='checkbox'   />";
                    var nothing = "";

                    var rowData = $('#' +tableName).jqGrid('getRowData', rowId);



                    var modelValue = FindObjectInTopic(rowId);



                    if (modelValue.isLeaf != null) {

                        var originalName = rowData.name;

                        if (modelValue.isLeaf == false) {

                            rowData.name =  rowData.name ;
                            grid.jqGrid('setRowData', rowId, rowData);
                        }
                        else {
                            var everyElement = rowData.name;
                            var eventChecked = "<input type='checkbox' style=' margin-left:50%'  onClick=\"EventSetSelected('" + rowData.id + "\')\" checked='checked' /> ";
                            var eventNotChecked = "<input type='checkbox'  style=' margin-left:50%' onClick=\"EventSetSelected('" + rowData.id + "\')\" /> ";
                            var hotelChecked = "<input type='checkbox'  style=' margin-left:50%'  onClick=\"HotelSetSelected('" + rowData.id + "\')\" checked='checked' /> ";
                            var hotelNotChecked = "<input type='checkbox'  style=' margin-left:50%' onClick=\"HotelSetSelected('" + rowData.id + "\')\" /> ";

                            if (modelValue.showUserSpecifiedDetails == true) {
                                if (modelValue.userSpecifiedDetails != null) {
                                    rowData.detail = " <textarea  style='width:80%; margin-left:10%' onchange=\"SaveDetailsValue(this,'" + rowData.id + "\')\"  cols='5' rows='2' >" + modelValue.userSpecifiedDetails + "\</textarea>";
                                }
                                else {
                                    rowData.detail = " <textarea  style='width:80%; margin-left:10%' onchange=\"SaveDetailsValue(this,'" + rowData.id + "\')\"  cols='5' rows='2' ></textarea>";
                                }
                            }

                            //Type 2 EVENT + HOTEL
                            if (modelValue.optionType.value == 2) {
                                //Event
                                if (modelValue.isEventSelected != null) {
                                    if (modelValue.isEventSelected == true) {
                                        rowData.event = eventChecked;
                                    }
                                    else {
                                        rowData.event = eventNotChecked;
                                    }
                                }
                                else {
                                    rowData.event = eventNotChecked;
                                }

                                //HOTEL
                                if (modelValue.isHotelSelected != null) {
                                    if (modelValue.isHotelSelected == true) {
                                        rowData.hotel = hotelChecked;
                                    }
                                    else {
                                        rowData.hotel = hotelNotChecked;
                                    }
                                }
                                else {
                                    rowData.hotel = hotelNotChecked;
                                }
                            }


                            //Type 3 EVENT
                            if (modelValue.optionType.value == 3) {

                                if (modelValue.isEventSelected != null) {
                                    if (modelValue.isEventSelected == true) {
                                        rowData.event = eventChecked;
                                    }
                                    else {
                                        rowData.event = eventNotChecked;
                                    }
                                }
                                else {
                                    rowData.event = eventNotChecked;
                                }
                            }


                            //Type 4 Hotel
                            if (modelValue.optionType.value == 4) {
                                if (modelValue.isHotelSelected != null) {
                                    if (modelValue.isHotelSelected == true) {
                                        rowData.hotel = hotelChecked;
                                    }
                                    else {
                                        rowData.hotel = hotelNotChecked;
                                    }
                                }
                                else {
                                    rowData.hotel = hotelNotChecked;
                                }
                            }

                            rowData.name = everyElement;

                            grid.jqGrid('setRowData', rowId, rowData);
                        }
                    }

                }

            },


            autowidth: true,
            jsonReader: {
                repeatitems: false
            }

        });



    });
}

