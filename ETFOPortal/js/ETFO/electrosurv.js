﻿$( function() {

    var infoContainer$ = $('#infoReviewContainer');
    var surveyContainer$ = $('#surveyContainer');

    //todo temporary
    //    infoContainer$.addClass('hidden');
    //    surveyContainer$.removeClass('hidden');

    $('#continueButton').click(function ()
    {
        infoContainer$.addClass('hidden');
        surveyContainer$.removeClass('hidden');
        
        //date dropdownlist birthday
        $("#birthDate").dateDropdowns
        ({
            submitFormat: "mm/dd/yyyy",
            monthFormat: "short",
            dropdownClass: "select-date",
            required: true,
            submitFieldName: "dateofbirth",
            minYear: 1900
        });
    });
    

    $('#saveButton').click(function () {
        surveyContainer$.find(".alert").hide(0);
        console.log('Form values');
        var values = gatherValues();
        console.log(values);
        $.post('/electronicsurvey/save',
            values,
            function(data) {
                if (data.message) {
                    surveyContainer$.find('.ow_saved').show();
                } else {
                    surveyContainer$.find('.ow_expired').show();
                }
            }
        );
        return true;
    });
    
    $('#submitButton').click(function () {
        surveyContainer$.find(".alert").hide(0);
        if (!validate()) return false;
        console.log('Form values');
        var values = gatherValues();
        console.log(values);
        $.post('/electronicsurvey/submit',
            values,
            function (data) {
                if (data.message) {
                    surveyContainer$.find('.ow_submitted').show();
                } else {
                    surveyContainer$.find('.ow_expired').show();
                }
            }
        );        
        return true;
    });

    $(document).ajaxError(function (event, jqxhr, settings, thrownError) {
        surveyContainer$.find('.ow_system_error').show();
        console.log("Ajax error "+thrownError);        
    });

    // clicking clearing input unchecks all other checkboxes
    $("input[type='checkbox'].ow_clearing_cb").change(function () {
        if (this.checked) {
            var group$ = $(this).closest(".control-group");
            group$.find("input").not(this).attr('checked', false);
        }
    });

    //clicking non-clearing checkbox unchecks clearing checkbox
    $("input[type='checkbox']").not(".ow_clearing_cb").change(function () {
        if (this.checked) {
            var group$ = $(this).closest(".control-group");
            group$.find("input.ow_clearing_cb").not(this).attr('checked', false);
        }
    });
    
    $("input[type='number']").change(function() {
        var parsed = parseFloat(this.value);
        if (isNaN(parsed) || parsed < 0) {
            this.value = 0;
        } else {
            this.value = parsed.toFixed(2);
        }

    });
    
    // Check if the check identify option set ir required
    $("input[type='checkbox']").change(function () {
        var group$ = $(this).closest(".control-group");
        if (group$.attr('id') === "cb-selfidentify")
        {
            $("#rb-selfidentify").removeClass("ow_notrequired");
            var childs = group$.find("input[type='checkbox']:checked");
            if (childs.length === 0)
            {
                $("#rb-selfidentify").addClass("ow_notrequired");
                $("#rb-selfidentify").removeClass("error");
            }
        }
    });

    $("input[name=selfidentify]").change(function () {
        $("#cb-selfidentify").removeClass("ow_notrequired");
    });

    // Check if other value for the first language is typed
    $('input[name=otherfirstlanguage]').keyup(function () {
        if ($(this).val() != "") {
            var $radios = $("input[name=firstlanguage]");
            $radios.filter('[value=3]').prop('checked', true);
        }
    });

    $("input[name=firstlanguage]").change(function () {
        if ($('input[name=firstlanguage]:checked').val() != 3) {
            $('input[name=otherfirstlanguage]').val('');
        }
    });

    // collect values from the input elements
    function gatherValues() {
        var values = {};
        surveyContainer$.find('input').each(function () {
            //if (!values[this.name]) values[this.name] = [];
            if (this.name == '__RequestVerificationToken') {
              //skip it  
            } else if (this.type == 'checkbox'  ) {
                values[this.name] = this.checked;
                console.log(this.name + '->' + this.checked);
            } else if( this.type =='radio' ){
                if (this.checked) {
                    values[this.name] = this.value;
                    console.log(this.name + '->' + this.value);
                }
            } else {
                values[this.name] = this.value;
                console.log(this.name + '->' + this.value);
            }
        });
        return values;
    }

    //validate form
    function validate() {
        var controlGroups = surveyContainer$.find('.control-group').not('.ow_notrequired');
        controlGroups.removeClass('error');

        var formComplete = true;
        controlGroups.each(function () {
            group$ = $(this);
            var hasCheckedChbx = group$.find("input[type=checkbox]:checked").val();
            var hasRadioWithVal = group$.find("input[type=radio]:checked").val(); 
            var hasNumberInput = group$.find("input[type=number]").val();
            var hasBirthDate = group$.find("input[name=dateofbirth]").val();

            if (hasCheckedChbx ) return; // any checkbox will do
            if( hasRadioWithVal) return; 
            if (hasNumberInput ) return;
            if (hasBirthDate ) return;

            group$.addClass('error');
            surveyContainer$.find('.ow_errors').show();
            formComplete = false;
        });
        console.log("validation,  formComplete? " + formComplete);
        return formComplete;
    }
})