﻿var offsetMin = -1;
var offSetMax = 3;

function decreaseFontSize()
{
	var fontOffset = parseInt($.cookie("fontOffset"));
	
	if (fontOffset <= offsetMin)
	{
		return;
	}

	$(".font-slider").slider("value", fontOffset-1);
}

function increaseFontSize()
{
	var fontOffset = parseInt($.cookie("fontOffset"));

	if (fontOffset >= offSetMax)
	{
		return;
	}

	$(".font-slider").slider("value", fontOffset + 1);
}

function setFontSize(offset)
{
	var accessibilityControls = $(".accessibility-controls").add($(".accessibility-controls").find("*"));
	var containers = $("div, span, p, a, h1, h2, h3, legend, label, li").not(accessibilityControls);

	containers.css(
    {
		"font-size" : "",
		"line-height" : ""
	});
	
	$.each(containers, function (index)
	{
		resizeElement($(this), offset);
	});

	$.each(accessibilityControls, function (index)
	{
		resizeElement($(this), 0);
	});
}

function resizeElement(element, offset)
{
	var size = parseInt(element.css('font-size'));
	var lineheight = parseInt(element.css("line-height"));
	element.css(
    {
		"font-size": (size + offset) + "px",
		"line-height" : (lineheight + offset) + "px"
	});
}

function showAccessibilityPanel() {
	$(".accessibility-controls").show();
}

function hideAccessibilityPanel() {
	$(".accessibility-controls").hide();
}

function isBrowserCompatible()
{
    var ua = navigator.userAgent, tem,
		M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    var browserName = '';
    var browserVersion = '';

    if (/trident/i.test(M[1]))
    {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        browserName = 'msie';
        browserVersion = (tem[1] || '');
    }
    else if (M[1] === 'Chrome' && (tem = ua.match(/\bOPR\/(\d+)/)))
    {
        browserName = 'opera';
        browserVersion = tem[1];
    }
    else
    {
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) != null)
        {
            M.splice(1, 1, tem[1]);
        }
        browserName = M[0];
        browserVersion = M[1];
    }

    switch (browserName.toLowerCase())
    { // Last update: 1/20/2015
        case 'opera':
            return browserVersion >= 24;
        case 'chrome':
            return browserVersion >= 37;
        case 'safari':
            return browserVersion >= 6;
        case 'firefox':
            return browserVersion >= 33;
        case 'msie':
            return browserVersion >= 9;
        default:
            return false;
    }
}

function checkBrowserCompatibility()
{
    var dialogHtml = '<div id="compatibilityWarning" title="Warning">' +
                     '<p>Your browser is old and may cause compatibility issues with this page.</p>' +
                     '<p>Please update it or use a different browser</p></div>';

    if (!isBrowserCompatible())
    {
        $(dialogHtml).dialog({ modal: true });
    }
}

function appendLinkFromHeader(titleLookup, destination)
{
    var titles = $(titleLookup);

    for (var i = 0; i < titles.length; i++)
    {
        var linkName = $(titles[i]).text().replace(/\s+/g, '');
        if (!($(titles[i]).attr('id')))
            $(titles[i]).attr('id', linkName);

        $(destination).append('<a href=\"#' + linkName + '\">' + $(titles[i]).text().trim() + '</a>');
    }
}

function showInfoModal(modalTitle, modalText, modalGoToText, modalGoToUrl) {
    var wrapper = $("#informationModal");
    wrapper.find("#modalTitle").html(modalTitle);
    wrapper.find("#modalMessage").html(modalText);
    wrapper.find("#modalGoTo").html(modalGoToText);

    wrapper.find("#modalGoTo").hide();
    if (modalGoToUrl) {
        wrapper.find("#modalGoTo").show();
        wrapper.find("#modalGoTo").click
        (
            function () {
                window.location.href = modalGoToUrl;
            }
        );
        $('#informationModal').modal('show');
    }
}

$(document).ready(function ()
{
    checkBrowserCompatibility();

	var fontOffset = parseInt($.cookie("fontOffset"));
	
	if (isNaN(fontOffset)) {
		fontOffset = 0;
		$.cookie("fontOffset", 0, { path: "/" });
	}

	$(".font-slider").slider({
		value: fontOffset,
		min: offsetMin,
		max: offSetMax,
		step: 1,
		change: function (event, ui) {
			setFontSize(ui.value);
			$.cookie("fontOffset", ui.value, {path: "/"});
		}
	});

	setFontSize(fontOffset);

	$(".icon-etfo-accessibility").click(function () { $(".accessibility-controls").show(); });
	$(".accessibility-close").click(function () { $(".accessibility-controls").hide(); });

	$(".increase-font").click(increaseFontSize);
	$(".decrease-font").click(decreaseFontSize);

	appendLinkFromHeader('div[id*=ContentContainer_MainContent_MainContent] h2', '.auto-links');
});