﻿<%@ Page Language="C#" ClientTarget="uplevel" ContentType="text/javascript" AutoEventWireup="true" %>
<%@ OutputCache CacheProfile="JS" %>
var portal = portal || {};

portal.carouselSettings = {
	interval: <asp:Literal runat="server" Text="<%$ SiteSetting: /carousel/interval, 5000 %>" />,
	pause: <asp:Literal runat="server" Text="<%$ SiteSetting: /carousel/pause, 'hover' %>" />,
	wrap: <asp:Literal runat="server" Text="<%$ SiteSetting: /carousel/wrap, true %>" />
}
