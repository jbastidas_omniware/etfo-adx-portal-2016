﻿(function(sessionManagement, $, _, undefined) {
    var lastInteractionTime = 0; // Last time an event triggered a session extension
    var extensionRefreshRateSeconds = 60; // Number of seconds between session extensions

    var secondsBeforePrompt = 0; //// Time until prompt pops up
    var sessionVariables =
    {
        sessionTimeoutSeconds: 900, // Session timeout 15 minutes
        countdownSeconds: 60, // Number of seconds to count down
        promptSeconds: 60,
        extendSessionUrl: '/', // URL to call when extending session
        expireSessionUrl: '/', // URL to call when expiring session
        currentUrl: '/',
        registrationUrl: '/',
        sessionMessage: 'Your session will expire in {0}. Press Continue to remain logged in, or Cancel to log off.'
    };

    var sessionManager = function ()
    {
        // Private Variables
        var displayCountdownIntervalId,                                                     // setInterval id, for clean up
            promptToExtendSessionTimeoutId,                                                 // setTimeout id, for clean up
            count = sessionVariables.countdownSeconds;                                                       // total count down;

        var endSession = function ()
        {
            $('#sessionModal').modal('hide');                                               // Close the jQuery Dialog
            location.href = sessionVariables.expireSessionUrl;                                               // Redirect to the expiration URL
        };

        var displayCountdown = function ()
        {
            var countdown = function ()
            {
                var cd = new Date(count * 1000);                                            // Returns milliseconds since 01/01/70
                var minutes = cd.getUTCMinutes();                                           // Grab the minutes
                var seconds = cd.getUTCSeconds().pad(2);                                    // Grab the seconds

                var message = sessionVariables.sessionMessage.format(minutes + ':' + seconds);
                $('#sessionExpireWarning').html(message);                                   // Update the countdown display
                if (count === 0)
                {                                                                           // If we reached zero,
                    endSession();                                                           // and end the session
                }
                count--;
            };
            countdown();                                                                    // Call the function once
            displayCountdownIntervalId = window.setInterval(countdown, 1000);               // Call the function every second
        };

        var promptToExtendSession = function () {
            $('#sessionModal').modal('show');
            count = sessionVariables.promptSeconds;                                                          // Set our counter
            displayCountdown();                                                             // Show that dialog!
        };

        // Just a private implementation to actually start our countdown before popup
        var startSessionManager = function ()
        {   
            promptToExtendSessionTimeoutId = window.setTimeout(promptToExtendSession, secondsBeforePrompt * 1000);
            $('#sessionModal').modal('hide');
        };

        var refreshSession = function () {
            window.clearInterval(displayCountdownIntervalId);                               // Stop calling countdown so we can start a new timer
            $.ajax(sessionVariables.extendSessionUrl);
            window.clearTimeout(promptToExtendSessionTimeoutId);                            // Clear the timeout so we can...
            startSessionManager();                                                          // ... start it all over!
        };

        $(document).off("click.extend-session").on("click.extend-session", "#extendSession", function (e) {
            $('#sessionModal').modal('hide');
            refreshSession();                                                               // Refresh the session
        });

        $(document).off("click.cancel-session").on("click.cancel-session", "#cancelSession", function (e) {
            $('#sessionModal').modal('hide');
            endSession();                                                               // Refresh the session
        });

        // Public Functions
        return {
            start: function () {
                startSessionManager();
            },

            extend: function () {
                refreshSession();
            }
        };
    }();

    Number.prototype.pad = function (size) {
        var s = String(this);
        if (typeof (size) !== "number") { size = 2; }

        while (s.length < size) { s = "0" + s; }
        return s;
    }

    String.prototype.format = function () {
        var str = this;
        for (var i = 0; i < arguments.length; i++) {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            str = str.replace(reg, arguments[i]);
        }
        return str;
    }

    // PUBLIC FUNCTIONS
    sessionManagement.startManager = function (vars)
    {
        sessionVariables = _.extend(sessionVariables, vars);
        secondsBeforePrompt = sessionVariables.sessionTimeoutSeconds - sessionVariables.countdownSeconds;
        sessionManager.start();

        // Whenever a click or keyup event fires, extend the session,
        // since we know the user is interacting with the site.
        $('body > :not(#sessionModal)').bind('click keyup', function(e) {
            var time = (new Date()).getTime();
            if ((time - lastInteractionTime) > (extensionRefreshRateSeconds * 1000)) {
                lastInteractionTime = time;
                sessionManager.extend();
            }
        });

    }
}(window.sessionManagement = window.sessionManagement || {}, jQuery, _));
