﻿using System.Web.Optimization;

namespace Site
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/css/default.bundle.css").Include(
                "~/css/font-awesome.min.css",
                "~/js/google-code-prettify/prettify.css",
                //"~/css/bootstrap-modal.css", 
                "~/css/bootstrap-datetimepicker.css",
                "~/css/portal.css",
                "~/css/comments.css",
                "~/css/ratings.css",
                "~/css/polls.css",
                "~/css/map.css",
                "~/css/webforms.css",
                "~/css/ui.jqgrid.css",
                "~/css/jquery-ui/1.10.3/themes/smoothness/jquery-ui.css",
                "~/css/style-metronic.css",
                "~/css/bootstrap-upgrade.css",
                "~/css/ETFO/etfo.css",
                "~/css/ETFO/dynamic.css.aspx"));

            bundles.Add(new ScriptBundle("~/js/default.preform.bundle.js").Include(
                "~/js/jquery-1.11.1.min.js",
                "~/js/jquery-ui/1.10.3/jquery-ui.js",
                "~/js/jquery.cookie.js",
                "~/js/jquery.jqGrid.min.js",
                "~/js/DateFormat.js",
                "~/js/moment-with-locales.min.js",
                "~/js/jquery-migrate-{version}.js",
                "~/js/respond.min.js",
                "~/js/underscore-min.js",
                "~/js/URI.min.js",
                "~/js/bootstrap-datetimepicker.min.js",
                "~/js/ckeditor-basepath.js",
                "~/js/ckeditor/ckeditor.js",
                "~/js/antiforgerytoken.js"));

            bundles.Add(new ScriptBundle("~/js/default.bundle.js").Include(
                "~/js/bootstrap.min.js",
                "~/js/date.js",
                "~/js/handlebars.js",
                "~/js/jquery.timeago.js",
                "~/js/google-code-prettify/prettify.js",
                "~/js/metronic/app.js",
                "~/js/ETFO/sessionManagement.js",
                "~/js/portal.js",
                "~/js/ETFO/etfo-portal.js"));

            //"~/js/eventListener.polyfill.js",
            //"~/js/jquery.bootstrap-pagination.js",
            //"~/js/jquery.blockUI.js",
            //"~/js/jquery.form.min.js",
            //"~/js/entity-notes.js",
            //"~/js/entity-form.js",
            //"~/js/entity-grid.js",
            //"~/js/entity-associate.js",
            //"~/js/entity-lookup.js",
            //"~/js/quickform.js",
            //"~/js/serialized-query.js",
            //"~/Areas/Cms/js/ads.js",
            //"~/Areas/Cms/js/polls.js",
            //"~/Areas/CaseManagement/js/case-deflection.js",
            //"~/js/badges.js",
            //"~/js/sharepoint-grid.js",
        }
    }
}
