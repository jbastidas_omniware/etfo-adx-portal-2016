﻿<%@ Page Language="C#" ClientTarget="uplevel" ContentType="text/css" AutoEventWireup="true" %>
<%@ OutputCache CacheProfile="CSS" %>

legend.signup-addition, .page-header.signup-addition {
	border-bottom: 1px solid <asp:Literal runat="server" Text="<%$ SiteSetting: /css/shaded-line, #DDDDDD %>" />;
}

.shaded-darker {
	background-color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/shaded-darker-background, #DDDDDD %>" />;
}

.shaded {
	background-color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/shaded-background, #EFEFEF %>" />;
}


.increase-font, .decrease-font {
	font-color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/resizer-font-color, #000000 %>" />
}
