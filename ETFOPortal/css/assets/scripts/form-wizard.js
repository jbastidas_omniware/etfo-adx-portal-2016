var FormWizard = function () {


    return {


        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }

            function format(state) {
                if (!state.id) return state.text; // optgroup
                return "<img class='flag' src='../../../css/assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
            }

            $("#country_list").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });

            var form = $('#submit_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //account
                    etfomemberid: {
                        minlength: 1,
                        maxlength: 10,
                        required: true
                    },
                    sanswerconfirm:
                         {
                             minlength: 5,
                             required: true
                         },
                    squestion:
                       {
                           minlength: 5,
                           required: true
                       },
                    sanswer:
                       {
                           minlength: 5,
                           required: true
                       },
                    agreed:
                        {
                            required: true
                        },
                    username: {
                        required: true,
                        email: true
                    },
                    password: {
                        minlength: 6,
                        required: true
                    },
                    rpassword: {
                        minlength: 6,
                        required: true,
                        equalTo: "#submit_form_password"
                    },
                    //profile
                    fname: {
                        required: true
                    },
                    lname: {
                        required: true
                    },

                    email: {
                        required: true,
                        email: true
                    },
                    oct: {
                        minlength: 1,
                        maxlength: 10,
                        required: true
                    },
                    phone: {
                        required: true
                    },
                    gender: {
                        required: true
                    },
                    address: {
                        required: true
                    },
                    city: {
                        required: true
                    },
                    nonmembertype: {
                        required: true
                    },
                    sboard: {
                        required: true
                    },
                    school: {
                        required: true
                    },
                    state: {
                        required: true
                    },
                    zip: {
                        minlength: 6,
                        required: true
                    }
                    //,
                    //payment
                    //card_name: {
                    //    required: true
                    //},
                    //card_number: {
                    //    minlength: 16,
                    //    maxlength: 16,
                    //    required: true
                    //},
                    //card_cvc: {
                    //    digits: true,
                    //    required: true,
                    //    minlength: 3,
                    //    maxlength: 4
                    //},
                    //card_expiry_date: {
                    //    required: true
                    //},
                    //'payment[]': {
                    //    required: true,
                    //    minlength: 1
                    //}
                },

                messages: { // custom messages for radio buttons and checkboxes
                    'payment[]': {
                        required: "Please select at least one option",
                        minlength: jQuery.format("Please select at least one option")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else if (element.attr("name") == "agreed") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form-agreed-error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    //success.show();
                    //error.hide();

                    if (SubmitCurrentMembershipType()) success.show();
                    else error.hide();

                    //alert('Submit handler');

                }

            });

            //var displayConfirm = function() {
            //    $('#tab4 .form-control-static', form).each(function(){
            //        var input = $('[name="'+$(this).attr("data-display")+'"]', form);
            //        if (input.is(":text") || input.is("textarea")) {
            //            $(this).html(input.val());
            //        } else if (input.is("select")) {
            //            $(this).html(input.find('option:selected').text());
            //        } else if (input.is(":radio") && input.is(":checked")) {
            //            $(this).html(input.attr("data-title"));
            //        } else if ($(this).attr("data-display") == 'payment') {
            //            var payment = [];
            //            $('[name="payment[]"]').each(function(){
            //                payment.push($(this).attr('data-title'));
            //            });
            //            $(this).html(payment.join("<br>"));
            //        }
            //    });
            //}

            var displayConfirm = function (tabName) {


                $('#' + tabName + ' .form-control-static', form).each(function () {
                    var input = $('[name="' + $(this).attr("data-display") + '"]', form);
                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") == 'payment') {
                        var payment = [];
                        $('[name="payment[]"]').each(function () {
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }
                });
            }

            // default form wizard
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index) {
                    //alert('on tab click disabled');
                    return false;
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();
                    Custom.hideAlert();

                    if (form.valid() == false) {
                        return false;
                    }

                    var total = navigation.find('li').length;
                    var current = index + 1;
                    // set wizard title
                    $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                    // set done steps
                    jQuery('li', $('#form_wizard_1')).removeClass("done");
                    var li_list = navigation.find('li');
                    for (var i = 0; i < index; i++) {
                        jQuery(li_list[i]).addClass("done");
                    }

                    if (current == 1) {
                        $('#form_wizard_1').find('.button-previous').hide();
                    } else {
                        $('#form_wizard_1').find('.button-previous').show();
                    }

                    if (current >= total) {

                        $('#form_wizard_1').find('.button-next').hide();
                        $('#form_wizard_1').find('.button-submit').show();
                        displayConfirm('tab4');
                    } else {

                        $('#form_wizard_1').find('.button-next').show();
                        $('#form_wizard_1').find('.button-submit').hide();

                    }
                    if (current == 3) {
                        displayConfirm('tab3');
                    }
                    if (current == 2) {
                        displayConfirm('tab2');
                    }


                    App.scrollTo($('.page-title'));
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    var total = navigation.find('li').length;
                    var current = index + 1;
                    // set wizard title
                    $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                    // set done steps
                    jQuery('li', $('#form_wizard_1')).removeClass("done");
                    var li_list = navigation.find('li');
                    for (var i = 0; i < index; i++) {
                        jQuery(li_list[i]).addClass("done");
                    }

                    if (current == 1) {
                        $('#form_wizard_1').find('.button-previous').hide();
                    } else {
                        $('#form_wizard_1').find('.button-previous').show();
                    }

                    if (current >= total) {
                        $('#form_wizard_1').find('.button-next').hide();
                        $('#form_wizard_1').find('.button-submit').show();
                    } else {
                        $('#form_wizard_1').find('.button-next').show();
                        $('#form_wizard_1').find('.button-submit').hide();
                    }

                    App.scrollTo($('.page-title'));
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function () {
                //debugger;

                form.submit();
                // alert('Finished! Hope you like it :)');
            }).hide();
        }

    };

}();