/**
Custom module for you to write your own javascript functions
**/
var Custom = function () {


    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');

    var AlertWindowHtml = '<div id="messageDiv" class="alert" style="color:black;width:30%;border-color:red;font-size:small;margin: auto auto;">'
                   + '<table style="width: 100%;">  <tr> <td colspan="2">  <p class="form-control-static" id="message">{0}'
                   + '</p>  </td> </tr> <tr><td><i class="icon-phone" style="width:30%;flex-item-align:center"></i></td>'
                   + ' <td>  <p class="form-control-static" style="width:70%;text-align:left;">  1-800-555-1212  </p>  </td>  </tr>'
                   + '<tr> <td><i class="icon-envelope" style="width:30%;flex-item-align:center"></i></td> <td>'
                   + ' <p class="form-control-static" style="width:70%;text-align:left;" > support@efto.ca </p>  </td>'
                   + '<td colspan="2">  <input id="okbutton" class="btn default" type="button" value="OK" onclick="javascript:Custom.hideAlertWindow;" /> <td>  </tr> </table></div>';

    var AlertHtml = '<div id="messageDiv" class="alert" style="color:black;width:30%;border-color:red;font-size:small;margin: 0 auto;">'
                   + '<table style="width: 100%;">  <tr> <td colspan="2">  <p class="form-control-static" id="message"> {0}'
                   + '</p>  </td> </tr> <tr><td><i class="icon-phone" style="width:30%;flex-item-align:center"></i></td>'
                   + ' <td>  <p class="form-control-static" style="width:70%;text-align:left;">  1-800-555-1212  </p>  </td>  </tr>'
                   + '<tr> <td><i class="icon-envelope" style="width:30%;flex-item-align:center"></i></td> <td>'
                   + ' <p class="form-control-static" style="width:70%;text-align:left;" > support@efto.ca </p>  </td></tr> </table></div>';

    return {
        showPleaseWait: function () {
            pleaseWaitDiv.modal();
        },
        hidePleaseWait: function () {
            pleaseWaitDiv.modal('hide');
        },

        showAlert: function (insertText, insertTo) {

            $('#' + insertTo).html(AlertHtml.replace('{0}', insertText));
            //document.getElementById(insertTo).innerHTML = AlertHtml;
        },
        hideAlert: function () {

            //document.getElementById('messageDiv').style.display = 'none';
            if ($('#messageDiv'))
                $('#messageDiv').hide();
        },
        showAlertWindowByTimeout: function (insertText, timeOut) {

            var AlertWindow = $(AlertWindow.replace('{0}', insertText));

            AlertWindow.modal('show');
            setTimeout("$('#messageDiv').modal('hide');", timeOut);
        },
        showAlertWindow: function (insertText) {

            var AlertWindow = $(AlertWindowHtml.replace('{0}', insertText));

            AlertWindow.modal('show');

        },
        hideAlertWindow: function (insertText) {

            if ($('#messageDiv'))
                $('#messageDiv').modal('hide');
        },
    };
}();



/***
Usage
***/
//Custom.init();
//Custom.doSomeStuff();