﻿using System;
using System.Threading;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Blogs;
using Adxstudio.Xrm.Web.Mvc;
using Microsoft.Xrm.Portal;
using Microsoft.Xrm.Portal.Configuration;
using Site.Pages;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Site.Areas.ETFO.DataAdapters;
using Site.Helpers;
using System.Linq;
using System.Text.RegularExpressions;

namespace Site.Areas.Blogs.Pages
{
    public partial class BlogPost : PortalPage
    {
        private readonly Lazy<IPortalContext> _portal = new Lazy<IPortalContext>(() => PortalCrmConfigurationManager.CreatePortalContext(), LazyThreadSafetyMode.None);
        private readonly Xrm.XrmServiceContext _context = XrmHelper.GetContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                Post.DataBind();
            }
        }

        protected void CreateBlogPostDataAdapter(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = new BlogPostDataAdapter(_portal.Value.Entity, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
        }

        protected string GetThumbnailUrl(object entityObject)
        {
            var entity = entityObject as Entity;

            if (entity == null) return null;

            var blogItem = _context.adx_blogpostSet.FirstOrDefault(bp => bp.adx_blogpostId == entity.Id);

            if (blogItem == null || blogItem.oems_Image == null)
            {
                return @"http://placehold.it/1280x720";
            }

            var webfile = ServiceContext.CreateQuery<Xrm.Adx_webfile>().FirstOrDefault(wf => wf.Id == blogItem.oems_Image.Id);

            return webfile == null ? @"http://placehold.it/1280x720" : new UrlBuilder(ServiceContext.GetUrl(webfile)).Path;
        }
    }
}