﻿<%@ Page Language="C#" MasterPageFile="../MasterPages/Blogs.master" AutoEventWireup="true" CodeBehind="BlogPost.aspx.cs" ValidateRequest="false" Inherits="Site.Areas.Blogs.Pages.BlogPost" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Blogs" %>
<%@ Import Namespace="Adxstudio.Xrm.Web" %>
<%@ Import Namespace="Site.Helpers" %>
<%@ Register src="~/Controls/Comments.ascx" tagname="Comments" tagprefix="adx" %>
<%@ Register src="~/Controls/MultiRatingControl.ascx" tagname="MultiRatingControl" tagprefix="adx" %>

<asp:Content ContentPlaceHolderID="PageHeader" runat="server">
	<asp:ObjectDataSource ID="PostHeaderDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogPostDataAdapter" OnObjectCreating="CreateBlogPostDataAdapter" SelectMethod="Select" runat="server" />
	<asp:ListView ID="PostHeader" DataSourceID="PostHeaderDataSource" runat="server">
		<LayoutTemplate>
			<asp:PlaceHolder ID="itemPlaceholder" runat="server" />
		</LayoutTemplate>
		<ItemTemplate>
			<crm:CrmEntityDataSource ID="Post" DataItem='<%# Eval("Entity") %>' runat="server" />
			<div class="page-header blog-post-heading">
				<asp:HyperLink CssClass="user-avatar" NavigateUrl='<%# Url.AuthorUrl(Eval("Author") as IBlogAuthor) %>' ImageUrl='<%# Url.UserImageUrl(Eval("Author.EmailAddress")) %>' ToolTip='<%# HttpUtility.HtmlEncode(Eval("Author.Name") ?? "") %>' runat="server"/>
				<h1><adx:Property DataSourceID="Post" PropertyName="adx_name" EditType="text" runat="server" /></h1>
			</div>
		</ItemTemplate>
	</asp:ListView>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<asp:ObjectDataSource ID="PostDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogPostDataAdapter" OnObjectCreating="CreateBlogPostDataAdapter" SelectMethod="Select" runat="server" />
	<asp:ListView ID="Post" DataSourceID="PostDataSource" runat="server">
		<LayoutTemplate>
			<asp:PlaceHolder ID="itemPlaceholder" runat="server" />
		</LayoutTemplate>
		<ItemTemplate>
			<div class='<%# "blog-post" + ((bool)Eval("IsPublished") ? "" : " unpublished") %>' runat="server">
                <div class="blog-image">
                    <asp:Image ID="Image1" ImageUrl='<%# GetThumbnailUrl(Eval("Entity")) %>' runat="server" />
                    <div class="blog-image-title">
                        <%# Eval("Title") %>
                        <span class="blog-title-fade"></span>
                    </div>
                </div>
				<asp:Panel CssClass="alert alert-block alert-info" Visible='<%# !(bool)Eval("IsPublished") %>' runat="server" >
					<asp:Label ID="Label1" Text='<%$ Snippet: Unpublished Post Message, This post is not published. It is only visible to you. %>' runat="server"></asp:Label>
				</asp:Panel>
				<crm:CrmEntityDataSource ID="Post" DataItem='<%# Eval("Entity") %>' runat="server" />
				<div class="metadata">
					<asp:HyperLink NavigateUrl='<%# Url.AuthorUrl(Eval("Author") as IBlogAuthor) %>' Text='<%# Eval("Author.Name") %>' runat="server" />
					&ndash;
					<abbr class="timeago"><%# Eval("PublishDate", "{0:r}") %></abbr>
					<asp:Label Visible='<%# ((BlogCommentPolicy)Eval("CommentPolicy")) != BlogCommentPolicy.None %>' runat="server">
						&ndash;
						<a href="#comments">
							<span class="fa fa-comment" aria-hidden="true"></span> <%# Eval("CommentCount") %>
						</a>
					</asp:Label>
					<adx:MultiRatingControl ID="Rating" InitEventName="OnLoad" RatingType="rating" runat="server" />
				</div>
				<div>
					<asp:Panel Visible='<%# Eval("HasExcerpt") %>' runat="server">
						<adx:Property DataSourceID="Post" PropertyName="adx_summary" EditType="html" runat="server" />
					</asp:Panel>
					<a class="anchor" name="extended"></a>
					<adx:Property DataSourceID="Post" PropertyName="adx_copy" EditType="html" CssClass="page-copy" runat="server" />
				</div>
				<div>
					<asp:ListView runat="server" DataSource='<%# Eval("Tags") %>'>
						<LayoutTemplate>
							<ul class="tags">
                                <li><i class="fa fa-tags"></i></li>
								<li id="itemPlaceholder" runat="server" />
							</ul>
						</LayoutTemplate>
						<ItemTemplate>
							<li runat="server">
								<asp:HyperLink CssClass="btn btn-default btn-xs" NavigateUrl='<%# Eval("ApplicationPath.AppRelativePath") %>' runat="server">
									<span class="fa fa-tag" aria-hidden="true"></span>
									<%# Eval("Name") %>
								</asp:HyperLink>
							</li>
						</ItemTemplate>
					</asp:ListView>
				</div>
			</div>
		</ItemTemplate>
	</asp:ListView>
	<adx:Comments EnableRatings="true" runat="server" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="Scripts" runat="server">
	<script src="<%: Url.Content("~/xrm-adx/js/tiny_mce/tiny_mce.js") %>"></script>
</asp:Content>