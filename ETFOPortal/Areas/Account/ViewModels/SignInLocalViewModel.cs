﻿namespace Site.Areas.Account.ViewModels
{
	public class SignInLocalViewModel
	{
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public bool PendingEmailNotification { get; set; }
        public bool InactiveUser { get; set; }
	}
}
