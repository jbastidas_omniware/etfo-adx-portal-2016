﻿using System.Web;

namespace Site.Areas.Account.ViewModels
{
	public class SharePointAppContext
	{
		public string SPAppToken { get; set; }
		public string SPSiteUrl { get; set; }
		public string SPSiteTitle { get; set; }
		public string SPSiteLogoUrl { get; set; }
		public string SPSiteLanguage { get; set; }
		public string SPSiteCulture { get; set; }
		public string SPRedirectMessage { get; set; }
		public string SPErrorCorrelationId { get; set; }
		public string SPErrorInfo { get; set; }

		public string SPHostUrl { get; set; }
		public string SPHostTitle { get; set; }
		public string SPAppWebUrl { get; set; }
		public string SPLanguage { get; set; }
		public int SPClientTag { get; set; }
		public string SPProductNumber { get; set; }
		public string WPID { get; set; }
		public int EditMode { get; set; }
		public string WPQ { get; set; }
		public int WebLocaleId { get; set; }
		public string AppPath { get; set; }
		public string SenderId { get; set; }

		public string AbsoluteAppPath
		{
			get { return VirtualPathUtility.ToAbsolute(AppPath ?? "~/"); }
		}
	}
}