﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Site.Areas.Account.ViewModels.SignInLocalViewModel>" %>
<%@ Import namespace="Adxstudio.Xrm.Collections.Generic" %>

<% using (Html.BeginForm("SignInLocal", "Account", ViewBag.ReturnUrlRouteValues as RouteValueDictionary)) { %>
	<%= Html.AntiForgeryToken() %>
	<div id="login-form" class="form-horizontal">
		<fieldset>
			<legend><%: Html.SnippetLiteral("ETFO/SignIn/SignInETFO", "Sign in with your ETFO account") %></legend>
			<%= Html.ValidationSummary(string.Empty, new {@class = "alert alert-warning alert-block alert-danger"}) %>
			<div class="form-group row">
				<label class="col-sm-2 control-label required" for="Email"><%: Html.SnippetLiteral("Account/SignIn/UserNameLabel", "Username") %></label>
				<div class="col-sm-5">
					<%= Html.TextBoxFor(model => model.UserName, new { @class = "form-control", placeholder = "Email" }) %>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 control-label required" for="Password"><%: Html.SnippetLiteral("Account/SignIn/PasswordLabel", "Password") %></label>
				<div class="col-sm-5">
					<%= Html.PasswordFor(model => model.Password, new { @class = "form-control", placeholder = "Password" }) %>
				</div>
			</div>
			<div class="form-actions signin-addition-actions form-group" style="background-color:transparent;border-top-style:none;">
				<input id="submit-signin-local" type="submit" class="btn btn-primary btn-signin" value="<%: Html.SnippetLiteral("Account/SignIn/LocalSignInFormButtonText", "Sign in") %>"/>	
                <br/>
                <% if(!ReferenceEquals(Model.UserName, null)){
                    var membershipUser = Membership.GetUser(Model.UserName);
                    if (membershipUser != null && (membershipUser.IsLockedOut || Model.PendingEmailNotification)){%>
                        <br/>
                        <a  class="help-link" href="<%= Html.SiteMarkerUrl("Send Email") %>"><%: Html.SnippetLiteral("Account/SignIn/SendEmail", "Resend Verification Email") %></a><br>
                <%} }%>
                <% if (Membership.EnablePasswordReset) { %>
					<a class="help-link" href="<%= Html.SiteMarkerUrl("Login") %>PasswordRecovery?<%: (ViewBag.PasswordRecoveryQueryString as NameValueCollection).ToQueryString() %>"><%: Html.SnippetLiteral("Account/SignIn/PasswordRecovery", "Forgot your username or password?") %></a>
				<% } %>
                <br />
			</div>
		</fieldset>
	</div>
<% } %>
<script src="<%: Url.Content("~/js/jquery.placeholder.min.js") %>"></script>
<script>
    $(function () {
        $("#UserName").focus();
        $('input, textarea').placeholder();
    });
</script>
