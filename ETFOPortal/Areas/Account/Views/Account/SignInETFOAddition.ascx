﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<% using (Html.BeginForm( ViewBag.ReturnUrlRouteValues as RouteValueDictionary)) { %>
	<%= Html.AntiForgeryToken() %>
    <div class="form-horizontal">
	    <fieldset>
		    <legend class="signup-addition"><%: Html.SnippetLiteral("ETFO/SignUp/WhyRegister", "Why Register?") %></legend>
            <div class="validation-summary"></div>
            <div class="form-group signup-registertext">
		    <%: Html.Snippet("ETFO/SignUp/WhyRegisterText") %>
            </div>
            <div class="form-actions signin-actions shaded-darker" >
				    Sign up for a <a href="<%= Html.SiteMarkerUrl("Member Login") %>">Member</a> 
		    <% if (ViewBag.OpenRegistrationEnabled) { %>
			    or <a href="<%= Html.SiteMarkerUrl("NonMember Login") %>">Non-Member</a> account.
		    <% } %>
		    </div>
	    </fieldset>
    </div>
<% } %>

