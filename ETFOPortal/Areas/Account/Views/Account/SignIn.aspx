﻿<%@ Page Title="" Language="C#" MasterPageFile="../Shared/Account.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHeader" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="row signin-container col-md-12">
        <% if (ViewBag.MembershipProviderEnabled) { %>
			<div class="col-md-6 signin-addition">
				<div class="page-header">
					<h1><%: Html.TextAttribute("adx_name") %></h1>
				</div>
				<% Html.RenderPartial("SignInLocal", ViewData["local"]); %>
			</div>
		<% } %>

		<div class="col-md-6 shaded signup-addition-container">
			<div class="page-header signup-addition">
				<h1><crm:Snippet ID="Snippet1" runat="server" SnippetName="SignIn/SignUpAdditionHeader" DefaultText="SignIn/SignUpAdditionHeader"/></h1>
			</div>
			<% Html.RenderPartial("SignInETFOAddition"); %>
		</div>
	</div>
</asp:Content>
