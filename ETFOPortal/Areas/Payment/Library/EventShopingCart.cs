﻿using System;

namespace Site.Areas.Payment.Library
{
    [Serializable]
    public class EventShopingCart
    {
        public Guid Id { get; set; }

        public Guid? EventRegistrationId { get; set; }

        public string EventName { get; set; }

        public bool? PayLater { get; set; }        

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool IsCourse { get; set; }

        public string CourseCode { get; set; }

        public Guid? CourseTermId { get; set; }
    }
}