﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client.Configuration;
using Microsoft.Xrm.Sdk;
using Site.Helpers;
using Xrm;
using log4net;
using Microsoft.Xrm.Sdk.Query;
using Moneris;
using Site.Areas.Payment.Models;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.tool.xml;

namespace Site.Areas.Payment.Library
{
    public static class BillingService
    {
        public static string StoreId = ConfigurationManager.AppSettings["Moneris.Store.Id"];
        public static string HppKey = ConfigurationManager.AppSettings["Moneris.Hpp.Key"];
        public static string MonerisBaseUrl = ConfigurationManager.AppSettings["Moneris.Base.Url"];
        public static string MonerisVerifyPath = ConfigurationManager.AppSettings["Moneris.Verify.Path"];
        public static string MonerisTransactionPath = ConfigurationManager.AppSettings["Moneris.Transaction.Path"];

        private static readonly ILog Log = LogManager.GetLogger(typeof(BillingService));
        
        public static class PaymentStatus
        {
            public static int Unpaid = 347780000;
            public static int ChequePending = 347780001;
            public static int CreditCardPending = 347780002;
            public static int Paid = 347780003;
            public static int Refunded = 347780004;
            public static int Partial = 347780005;
            public static int ChequeRefundPending = 347780006;
            public static int PayLater = 347780007;
            public static int Cancelled = 347780008;
            public static int Failed = 347780009;
            public static int RefundFailed = 347780010;
            public static int LocalToPay = 347780011;
        }

        public static class CreditCartTransactionType
        {
            public const int REFUND_TRANSACTION = 347780000;
            public const int PAYMENT_TRANSACTION = 347780001;
        }

        public static class PaymentMethod
        {
            public static int Cheque = 347780000;
            public static int CreditCard = 347780001;
            public static int ZeroPayment = 347780002;
            public static int Deferral = 347780003;
            public static int PayLater = 347780004;
            public static int LocalToPay = 347780005;
        }

        // fields retruned on moneris payment response
        public static class MonerisResponse
        {
            public static string ResponseOrderId = "response_order_id";
            public static string DateStamp = "date_stamp";
            public static string TimeStamp = "time_stamp";
            public static string BankTransactionId = "bank_transaction_id";
            public static string ChargeTotal = "charge_total";
            public static string BankApprovalCode = "bank_approval_code";
            public static string ResponseCode = "response_code";
            public static string IsoCode = "iso_code";
            public static string Message = "message";
            public static string TransName = "trans_name";
            public static string Cardholder = "cardholder";
            public static string CardNumber = "f4l4";
            public static string CardNumberAsync = "card_num";
            public static string Card = "card";
            public static string ExpiryDate = "expiry_date";
            public static string TxnNum = "txn_num";
            public static string Result = "result";
            public static string TransactionKey = "transactionKey";
        }

        //fields returned on verification response
        public static class MonerisVerification
        {
            public static string OrderId = "order_id";
            public static string Amount = "amount";
            public static string TxnNum = "txn_num";
            public static string TransactionKey = "transactionKey";
            public static string Status = "status";
        }

        public static class MonerisPreloadResponse
        {
            public static string HppId = "hpp_id";
            public static string Ticket = "ticket";
            public static string OrderId = "order_id";
            public static string ResponseCode = "response_code";
        }

        public static class BillingStatus
        {
            public static int None = 347780000;
            public static int RefundFailed = 347780001;
        }

        public static class MonerisVerificationStatus
        {
            // The transaction was approved and successfully validated 
            public static string ValidApproved = "Valid-Approved";

            //The transaction was declined and successfully validated
            public static string ValidDeclined = "Valid-Declined";

            // No reference to this transactionKey, validation failed
            public static string Invalid = "Invalid";

            // An attempt has already been made with this transaction key,
            public static string InvalidReConfirmed = "Invalid-ReConfirmed";
        }

        [Serializable]
        public class PaymentInfo
        {
            public Guid BillingRequestId { get; set; }
            public Guid RegistrationId { get; set; }

            //Sequencial Request Id visible on UI serves as order Id
            public string OrderId { get; set; }

            public string EventName { get; set; }

            public int PaymentStatus { get; set; }

            public bool IsChequeAllowed{ get; set;}
            public bool PayLater { get; set; }
            public bool PayLocal { get; set; }
            public DateTime ChequeDueDate { get; set; }
            public Guid? EventPriceId { get; set; }
            public string LocalName { get; set; }

            //Event & disccounts
            public decimal EventCost { get; set; }
            public decimal WorkshopsDiscount { get; set; }
            public decimal EventTax { get; set; }

            //Fees
            public decimal AdditionalFeeForMaterials { get; set; }
            public decimal RegistrationFee { get; set; }

            //Accomodation
            public int AccommodationDays { get; set; }
            public decimal AccommodationCost { get; set; }
            public decimal AccommodationTax { get; set; }

            //Aditional tickets
            public int AdditionalTickets { get; set; }
            public decimal TicketCost { get; set; }
            public decimal TicketTax { get; set; }

            //Deferal
            public decimal Deferral { get; set; }

            // Amounts and taxes
            public decimal AmountDueBeforeTax
            {
                get
                {
                    return EventCost + AdditionalFeeForMaterials + RegistrationFee + WorkshopsDiscount - Deferral + AccommodationCost + TicketCost;
                }
            }
            public decimal Tax
            {
                get
                {
                    return EventTax + AccommodationTax + TicketTax;
                }
            }
            public decimal AmountDueWithTax { get; set; }
            public override string ToString()
            {
                return
                    string.Format("BillingRequestId: {0}, IsPaid: {1}, ChequeDueDate: {2}, AccommodationDays: {3}, AccommodationCost: {4}, AdditionalTickets: {5}, TicketCost: {6}, EventCost: {7}, Total: {8}, Tax: {9}, AmountDueWithTax: {10}",
                        BillingRequestId, PaymentStatus, ChequeDueDate, AccommodationDays, AccommodationCost, AdditionalTickets,
                        TicketCost, EventCost, AmountDueBeforeTax, Tax, AmountDueWithTax);
            }
        }
        
        public class PaymentPreloadInfo
        {
            public string HppId { get; set; }
            public string Ticket { get; set; }
            public string OrderId { get; set; }
            public int ResponseCode { get; set; }
        }

        // retrieve payment status from CRM, including allowed payment options and the amount due
        public static PaymentInfo GetPaymentInformation(Guid registrationId)
        {
            var ctx =  CrmConfigurationManager.CreateContext("XrmNoCache") as XrmServiceContext;

            var fullRegistration = (
                     from br in ctx.CreateQuery("oems_billingrequest")
                     join reg in ctx.CreateQuery("oems_eventregistration") on ((EntityReference)br["oems_eventregistration"]).Id equals reg["oems_eventregistrationid"]
                     join ev in ctx.CreateQuery("oems_event") on ((EntityReference)br["oems_event"]).Id equals ev["oems_eventid"]
                     where (Guid)br["oems_eventregistration"] == registrationId
                     select new
                     {
                         BillingRequest = br,
                         LocalName = reg.GetAttributeValue<EntityReference>("oems_local") != null ? reg.GetAttributeValue<EntityReference>("oems_local").Name : string.Empty,
                         Evnt = new
                         {
                             PayLater = ev.GetAttributeValue<bool>("oems_billingpaylater"),
                             AcceptCheque = ev.GetAttributeValue<bool>("oems_billingacceptcheque"),
                             ChequeDueDate = ev.GetAttributeValue<DateTime>("oems_billingchequeduedate"),
                             PayLocal = ev.GetAttributeValue<bool>("oems_chargetolocal")
                         }
                     }).FirstOrDefault();

            if (fullRegistration == null)
            {
                // no billing request for this registration id
                return null;
            }

            var billingRequest = fullRegistration.BillingRequest;
            var theEvent = fullRegistration.Evnt;

            //Get the payment Transaction if any
            var paymentTransaction =
                billingRequest.Contains("oems_paymenttransaction") ?
                (
                    from tx in ctx.CreateQuery("oems_cctransaction")
                    where (Guid)tx["oems_cctransactionid"] == ((EntityReference)billingRequest["oems_paymenttransaction"]).Id
                    select tx
                ).FirstOrDefault() : 
                null;

            return PaymentInformation(billingRequest, paymentTransaction, theEvent.PayLater, theEvent.AcceptCheque, theEvent.ChequeDueDate, theEvent.PayLocal, fullRegistration.LocalName);
        }

        // retrieve payment status from CRM, including allowed payment options and the amount due
        public static ShoppingCart GetPaymentInformation(string orderId, out Entity paymentTransaction)
        {
            paymentTransaction = null;

            //strip order prefix if any
            var orderIdPrefix = ConfigurationManager.AppSettings["Moneris.OrderId.Prefix"] ?? "";
            orderId = orderId.Substring(orderIdPrefix.Length);

            var ctx = CrmConfigurationManager.CreateContext("XrmNoCache") as XrmServiceContext;

            var entities = (
                     from br in ctx.CreateQuery("oems_billingrequest")
                     join reg in ctx.CreateQuery("oems_eventregistration") on ((EntityReference)br["oems_eventregistration"]).Id equals reg["oems_eventregistrationid"]
                     join ev in ctx.CreateQuery("oems_event") on ((EntityReference)br["oems_event"]).Id equals ev["oems_eventid"]
                     join tx in ctx.CreateQuery("oems_cctransaction") on ((EntityReference)br["oems_paymenttransaction"]).Id equals tx["oems_cctransactionid"]
                     where tx["oems_orderid"] == orderId
                     select new
                     {
                         BillingRequest = br,
                         LocalName = reg.GetAttributeValue<EntityReference>("oems_local") != null ? reg.GetAttributeValue<EntityReference>("oems_local").Name : string.Empty,
                         Evnt = new
                         {
                             Id = ev.GetAttributeValue<global::System.Guid>("oems_eventid"),
                             Name = ev.GetAttributeValue<string>("oems_eventname"),
                             PayLater = ev.GetAttributeValue<bool>("oems_billingpaylater"),
                             AcceptCheque = ev.GetAttributeValue<bool>("oems_billingacceptcheque"),
                             ChequeDueDate = ev.GetAttributeValue<global::System.DateTime>("oems_billingchequeduedate"),
                             PayLocal = ev.GetAttributeValue<bool>("oems_chargetolocal"),
                             LocalName = reg.GetAttributeValue<global::Microsoft.Xrm.Sdk.EntityReference>("oems_local") != null ? reg.GetAttributeValue<global::Microsoft.Xrm.Sdk.EntityReference>("oems_local").Name : string.Empty,
                             StartDate = ev.GetAttributeValue<global::System.DateTime?>("oems_startdate"),
                             EndDate = ev.GetAttributeValue<global::System.DateTime?>("oems_enddate"),
                             IsCourse = ev.GetAttributeValue<bool>("oems_courseflag"),
                             CourseCode = ev.GetAttributeValue<string>("oems_coursecode"),
                             EventRegistrationId = br.GetAttributeValue<global::Microsoft.Xrm.Sdk.EntityReference>("oems_eventregistration").Id,
                             CourseTermId = ev.GetAttributeValue<global::Microsoft.Xrm.Sdk.EntityReference>("oems_courseterm") != null ? ev.GetAttributeValue<global::Microsoft.Xrm.Sdk.EntityReference>("oems_courseterm").Id : (global::System.Guid?)null
                         },
                         PaymentTransaction = tx
                     }).ToList();


            if (entities.Count == 0)
            {
                // no billing requests for this order id
                Log.Warn("Can't find payment transactions with order id " + orderId);
                return null;
            }

            var subtotal = (decimal)0;
            var totalTax = (decimal)0;
            var totalDeferral = (decimal)0;
            var shoppingCart = new ShoppingCart();
            foreach (var info in entities)
            {
                var billingRequest = info.BillingRequest;
                var theEvent = info.Evnt;
                paymentTransaction = info.PaymentTransaction;

                var paymentInfo = PaymentInformation(billingRequest, paymentTransaction, theEvent.PayLater, theEvent.AcceptCheque, theEvent.ChequeDueDate, theEvent.PayLocal, info.LocalName);
                paymentInfo.EventName = theEvent.Name;

                var detail = new ShoppingCartDetail()
                {
                    Event =
                        new EventShopingCart()
                        {
                            Id = theEvent.Id,
                            EventRegistrationId = theEvent.EventRegistrationId,
                            EventName = theEvent.Name,
                            PayLater = theEvent.PayLater,
                            StartDate = theEvent.StartDate,
                            EndDate = theEvent.EndDate,
                            IsCourse = theEvent.IsCourse,
                            CourseCode = theEvent.CourseCode,
                            CourseTermId = theEvent.CourseTermId
                        },
                    PaymentInfo = paymentInfo
                };

                subtotal += paymentInfo.EventCost + paymentInfo.AdditionalFeeForMaterials + paymentInfo.RegistrationFee + paymentInfo.WorkshopsDiscount + paymentInfo.AccommodationCost + paymentInfo.TicketCost;
                totalTax += paymentInfo.EventTax + paymentInfo.AccommodationTax + paymentInfo.TicketTax;
                totalDeferral += paymentInfo.Deferral;

                shoppingCart.Details.Add(paymentInfo.RegistrationId, detail);
            }

            shoppingCart.SubTotal = subtotal;
            shoppingCart.TotalTax = totalTax;
            shoppingCart.Due = subtotal + totalTax;
            shoppingCart.Deferral = -totalDeferral;
            shoppingCart.TaxPending = totalTax;
            if (totalDeferral > 0)
            {
                var realHst = totalTax / shoppingCart.Due;
                var deferralTax = Math.Round(totalDeferral * realHst);

                shoppingCart.TaxPending = totalTax - deferralTax;
            }
            shoppingCart.TotalToPay = shoppingCart.Due - totalDeferral;

            return shoppingCart;
        }

        private static PaymentInfo PaymentInformation(Entity billingRequest, Entity paymentTransaction, bool payLater, bool acceptCheque, DateTime chequeDueDate, bool payLocal, string localName)
        {
            var orderIdPrefix = ConfigurationManager.AppSettings["Moneris.OrderId.Prefix"] ?? "";
            
            // separate from linq for ease of debugging
            var info = new PaymentInfo
            {
                BillingRequestId = billingRequest.GetAttributeValue<Guid>("oems_billingrequestid"),
                RegistrationId = billingRequest.GetAttributeValue<EntityReference>("oems_eventregistration").Id,
                OrderId = paymentTransaction != null ? orderIdPrefix + paymentTransaction.GetAttributeValue<string>("oems_orderid") : null,
                EventPriceId = billingRequest.GetAttributeValue<EntityReference>("oems_eventpricing").Id,
                PaymentStatus = ((OptionSetValue)billingRequest["oems_paymentstatus"]).Value,
                PayLater = payLater,
                PayLocal = payLocal,
                IsChequeAllowed = acceptCheque,
                ChequeDueDate = chequeDueDate,
                LocalName = localName,
                EventCost = billingRequest.GetAttributeValue<decimal>("oems_eventcost"),
                WorkshopsDiscount = billingRequest.GetAttributeValue<decimal>("oems_numberofworkshopsdiscountamount"),
                RegistrationFee = billingRequest.GetAttributeValue<decimal>("oems_registrationfeeamount"),
                AdditionalFeeForMaterials = billingRequest.GetAttributeValue<decimal>("oems_additionalfeeforeventmaterials"),
                EventTax = billingRequest.GetAttributeValue<decimal>("oems_eventtax"),
                AccommodationDays = Convert.ToInt32(billingRequest.GetAttributeValue<decimal>("oems_accommodationdays")),
                AccommodationCost = billingRequest.GetAttributeValue<decimal>("oems_accommodationcost"),
                AccommodationTax = billingRequest.GetAttributeValue<decimal>("oems_accommodationtax"),
                AdditionalTickets = Convert.ToInt32(billingRequest.GetAttributeValue<decimal>("oems_additionaltickets")),
                TicketCost = billingRequest.GetAttributeValue<decimal>("oems_ticketcost"),
                TicketTax = billingRequest.GetAttributeValue<decimal>("oems_tickettax"),
                AmountDueWithTax = billingRequest.GetAttributeValue<decimal>("oems_totalamountdue"),
                Deferral = billingRequest.GetAttributeValue<decimal>("oems_deferralamount")
            };

            return info;
        }

        // upload transaction data to Moneris, get ticket back
        public static PaymentPreloadInfo PreloadMonerisPayment(ShoppingCart shoppingCart, Guid paymenttransactionId)
        {
            List<ShoppingCartDetail> paymentInfos = shoppingCart.Details.Values.ToList();
            string eventName = paymentInfos.Count == 1 ? paymentInfos.First().Event.EventName : "Payment Details";

            using (var client = new WebClient())
            {
                var note = eventName;
                if (note.Length > 50)
                {
                    note = note.Substring(0, 50);
                }

                // Creates the transaction
                var context = XrmHelper.GetContext();
                var paymenttransaction = context.Retrieve("oems_cctransaction", paymenttransactionId, new ColumnSet("oems_orderid"));
                var orderId = paymenttransaction.GetAttributeValue<string>("oems_orderid");

                var amountDueWithTax = shoppingCart.TotalToPay;
                var deferral = shoppingCart.Deferral;
                var tax = shoppingCart.TaxPending;

                var values = new NameValueCollection
                {
                    {"ps_store_id", StoreId},
                    {"hpp_key", HppKey},
                    {"hpp_preload", ""},
                    {"charge_total", amountDueWithTax.ToString("F")},
                    {"note", note},
                    {"order_id", orderId}, 
                    {"hst", tax.ToString("F")}
                };

                int eventsIndex = 0;
                for (int i = 0; i < paymentInfos.Count; ++i)
                {
                    paymentInfos[i].PaymentInfo.OrderId = orderId;
                    var paymentInfo = paymentInfos[i].PaymentInfo;

                    Log.Debug("Preloading Moneris transaction " + paymentInfo);

                    if (paymentInfo.EventCost != 0)
                    {
                        var evtName = paymentInfo.EventName;
                        if (evtName.Length > 15)
                        {
                            evtName = evtName.Substring(0, 15);
                        }

                        //Event
                        values.Add("description" + (i + 1 + eventsIndex).ToString(), evtName);
                        values.Add("quantity" + (i + 1 + eventsIndex).ToString(), "1");
                        values.Add("price" + (i + 1 + eventsIndex).ToString(), paymentInfo.EventCost.ToString("F"));
                    }

                    if (paymentInfo.RegistrationFee != 0)
                    {
                        values.Add("description" + (i + 2 + eventsIndex).ToString(), "Registration Fee");
                        values.Add("quantity" + (i + 2 + eventsIndex).ToString(), "1");
                        values.Add("price" + (i + 2 + eventsIndex).ToString(), paymentInfo.RegistrationFee.ToString("F"));
                    }

                    if (paymentInfo.AdditionalFeeForMaterials != 0)
                    {
                        values.Add("description" + (i + 3 + eventsIndex).ToString(), "Materials Fee");
                        values.Add("quantity" + (i + 3 + eventsIndex).ToString(), "1");
                        values.Add("price" + (i + 3 + eventsIndex).ToString(), paymentInfo.AdditionalFeeForMaterials.ToString("F"));
                    }

                    if (paymentInfo.WorkshopsDiscount != 0)
                    {
                        values.Add("description" + (i + 5 + eventsIndex).ToString(), "Workshop Discount");
                        values.Add("quantity" + (i + 5 + eventsIndex).ToString(), "1");
                        values.Add("price" + (i + 5 + eventsIndex).ToString(), paymentInfo.WorkshopsDiscount.ToString("F"));
                    }

                    if (paymentInfo.AccommodationCost != 0)
                    {
                        values.Add("description" + (i + 6 + eventsIndex).ToString(), "Accommodation");
                        values.Add("quantity" + (i + 6 + eventsIndex).ToString(), paymentInfo.AccommodationDays.ToString("#"));
                        values.Add("price" + (i + 6 + eventsIndex).ToString(), paymentInfo.AccommodationCost.ToString("F"));
                    }

                    if (paymentInfo.TicketCost != 0)
                    {
                        values.Add("description" + (i + 7 + eventsIndex).ToString(), "Additional Tickets");
                        values.Add("quantity" + (i + 7 + eventsIndex).ToString(), paymentInfo.AdditionalTickets.ToString("#"));
                        values.Add("price" + (i + 7 + eventsIndex).ToString(), paymentInfo.TicketCost.ToString("F"));
                    }

                    //Blank
                    values.Add("description" + (i + 8 + eventsIndex).ToString(), "-");
                    values.Add("quantity" + (i + 8 + eventsIndex).ToString(), "-");
                    values.Add("price" + (i + 8 + eventsIndex).ToString(), "-");

                    eventsIndex += 7;
                }
                
                if (deferral != 0)
                {
                    values.Add("description" + (8 + eventsIndex).ToString(), "Deferral Credit");
                    values.Add("quantity" + (8 + eventsIndex).ToString(), "1");
                    values.Add("price" + (8 + eventsIndex).ToString(), deferral.ToString("F"));
                }
                
                //client
                var raw = client.UploadValues
                (
                    MonerisBaseUrl + MonerisTransactionPath,
                    values
                );

                var xmlResponse = Encoding.UTF8.GetString(raw);
                var response = XDocument.Parse(xmlResponse).Element("response");
                var preloadResponse = response.Descendants().ToDictionary(element => element.Name.LocalName, element => element.Value);

                Log.Info("Moneris PRELOAD response " + ToLogString(preloadResponse));

                var pr = new PaymentPreloadInfo()
                {
                    HppId = preloadResponse[MonerisPreloadResponse.HppId],
                    Ticket = preloadResponse[MonerisPreloadResponse.Ticket],
                    OrderId = preloadResponse[MonerisPreloadResponse.OrderId],
                    ResponseCode = Convert.ToInt32(preloadResponse[MonerisPreloadResponse.ResponseCode]),
                };

                return pr;
            }
        }

        /// <summary>
        /// Process a moneris response, sample response:
        /// response_order_id: mhp14291123236p88
        /// date_stamp: 2014-10-19
        /// time_stamp: 12:34:30 
        /// bank_transaction_id: 660021780014413460
        /// charge_total: 1.00 
        /// bank_approval_code: 824682 
        /// response_code: 027
        /// iso_code: 01
        /// message:APPROVED *=
        /// trans_name: purchase
        /// cardholder:michael test
        /// CardNumber(f4l4): 4532***4435
        /// card: V
        /// expiry_date: 1703
        /// result: 1
        /// transactionKey: AO6CTlOR4EUHwCtnjmR5Vly5b3f8UL
        /// </summary>
        public static ShoppingCart ProcessMonerisResponse(Dictionary<string, string> paymentResp, bool isAsyncResponse, out bool isVoid, IReceiptProcessor receiptProcessor)
        {
            isVoid = false;
            string originMethod = isAsyncResponse ? "Async Response" : "Normal Response";
            Log.Info("(" + originMethod + "): " + ToLogString(paymentResp));

            // If transaction is declined return immediatelly as calling verification for declined transaction will result in verification status Invalid
            if (paymentResp.ContainsKey(MonerisResponse.Result) && paymentResp[MonerisResponse.Result] == "0")
            {
                Log.Debug("Payment Declined. Result = 0"); 
                return null;
            }

            //Fixed response orderid
            var monerisOrderId = paymentResp[MonerisResponse.ResponseOrderId]; ;
            var orderRespId = monerisOrderId;
            var indexRetryResp = orderRespId.IndexOf("-");
            if (indexRetryResp > 0)
            {
                orderRespId = orderRespId.Substring(0, indexRetryResp);
            }

            //Check payment status
            Entity paymentTransaction;
            var shoppingCart = GetPaymentInformation(orderRespId, out paymentTransaction);
            shoppingCart.IsAsyncRequest = isAsyncResponse;
            
            //Check if the tx is already payed
            var bankTransactionId = paymentTransaction.GetAttributeValue<string>("oems_banktransactionid");
            Log.Info("(" + originMethod + ") Bank TransactionId: " + bankTransactionId);
            if (!string.IsNullOrWhiteSpace(bankTransactionId) && bankTransactionId == paymentResp[BillingService.MonerisResponse.BankTransactionId])
            {
                Log.Info("(" + originMethod + ") Transaction was already processed");
                return shoppingCart;
            }
            else
            {
                Log.Info("(" + originMethod + ") Transaction waiting for be processed");
            }

            // Call Moneris for transaction verification
            // This should not normally happen, it really is an assertion of the assumption that Moneris API works
            var verifiedResp = VerifyMonerisTransaction(paymentResp[MonerisResponse.TransactionKey], isAsyncResponse);
            
            //Fixed verified orderid
            var orderVerId = verifiedResp[MonerisVerification.OrderId];
            var indexRetryVer = orderVerId.IndexOf("-");
            if (indexRetryVer > 0)
            {
                orderVerId = orderVerId.Substring(0, indexRetryVer);
            }

            Log.Info("(" + originMethod + ") Validate Moneris Verification Response");
            if (verifiedResp[MonerisVerification.TransactionKey] != paymentResp[MonerisResponse.TransactionKey])
            {
                throw new ArgumentException("Transaction key mismatch: " + verifiedResp[MonerisVerification.TransactionKey]);
            }

            // Check  verified transaction status,
            if (verifiedResp[MonerisVerification.Status] == MonerisVerificationStatus.ValidDeclined)
            {
                Log.Info("Transaction declined " + ToLogString(verifiedResp));
                return null;
            }
            else if (verifiedResp[MonerisVerification.Status] != MonerisVerificationStatus.ValidApproved)
            {
                // Invalid and Invalid-Reconfirmed statuses should not occur in normal flows
                throw new ArgumentException("Transaction verification returened illegal transaction status: " + ToLogString(verifiedResp));
            }

            //Verify amount and order id (Only applies on normal payment flow)
            if (Decimal.Parse(verifiedResp[MonerisVerification.Amount]) != shoppingCart.TotalToPay
                || orderVerId != orderRespId
                || verifiedResp[MonerisVerification.TxnNum] != paymentResp[MonerisResponse.TxnNum]
                )
            {
                throw new ArgumentException("Amount, order Id or tx_num mismatch BETWEEN: " + ToLogString(paymentResp) + " AND: " + ToLogString(verifiedResp) + " (" + originMethod + ")");
            }

            //Validate if billing requests are CC pending
            var isCreditCardPendingOrPaid = shoppingCart.Details.Values.ToList().TrueForAll(i => i.PaymentInfo.PaymentStatus == PaymentStatus.CreditCardPending || i.PaymentInfo.PaymentStatus == PaymentStatus.Paid);
            if (isCreditCardPendingOrPaid)
            {
                //Update billing requests and CC Transaction
                UpdateRegistrationAsPaid(paymentTransaction, monerisOrderId, shoppingCart, paymentResp, verifiedResp, isAsyncResponse, receiptProcessor);
            }
            else
            {
                //Void the transaction
                isVoid = true;
                VoidMonerisTransaction(orderVerId, verifiedResp[MonerisVerification.TxnNum]);
            }

            return shoppingCart;
        }

        public static bool VoidMonerisTransaction(string orderId, string transactionNumber)
        {
            var context = XrmHelper.GetContext();

            //var host = "esqa.moneris.com";
            //var storeId = "store2";
            //var apiToken = "yesguy";
            //var crypt = "7";
            var configuration =
               (
                   from c in context.CreateQuery("oems_configuration")
                   select c
               ).FirstOrDefault();
            var host = configuration["oems_monerishost"].ToString();
            var storeId = configuration["oems_monerisstoreid"].ToString();
            var apiToken = configuration["oems_monerisapitoken"].ToString();
            var crypt = configuration["oems_moneriscrypt"].ToString();
            var processing_country_code = configuration["oems_moneriscountrycode"].ToString();
            var descriptor = String.Format("Billing Requests for Order Id {0} are not Credit Card Pending", orderId);

            var orderIdPrefix = configuration.GetAttributeValue<string>("oems_monerisorderidprefix");
            if (String.IsNullOrEmpty(orderIdPrefix))
            {
                orderIdPrefix = "";
            }

            var testMode = true;
            var testModeConfig = configuration["oems_moneristestmode"].ToString();
            if (!String.IsNullOrEmpty(testModeConfig))
            {
                testMode = Convert.ToBoolean(testModeConfig);
            }
            var pc = new PurchaseCorrection(orderId, transactionNumber, crypt);
            pc.SetDynamicDescriptor(descriptor); 

            try
            {
                //var mpgReq = new HttpsPostRequest(host, storeId, apiToken, pc);
                var mpgReq = new HttpsPostRequest();
                mpgReq.SetProcCountryCode(processing_country_code);
                mpgReq.SetTestMode(testMode);
                mpgReq.SetStoreId(storeId);
                mpgReq.SetApiToken(apiToken);
                mpgReq.SetTransaction(pc);

                mpgReq.Send();

                var receipt = mpgReq.GetReceipt();
                var success = Convert.ToBoolean(receipt.GetComplete());

                //TODO: Save any info on CRM?
                //CRM ORDER ID - Billing Request Id
                //var orderIdPrefix = ConfigurationManager.AppSettings["Moneris.OrderId.Prefix"] ?? "";
                //var requestId = orderId.Substring(orderIdPrefix.Length);

                //Console.WriteLine("CardType = " + receipt.GetCardType());
                //Console.WriteLine("TransAmount = " + receipt.GetTransAmount());
                //Console.WriteLine("TxnNumber = " + receipt.GetTxnNumber());
                //Console.WriteLine("ReceiptId = " + receipt.GetReceiptId());
                //Console.WriteLine("TransType = " + receipt.GetTransType());
                //Console.WriteLine("ReferenceNum = " + receipt.GetReferenceNum());
                //Console.WriteLine("ResponseCode = " + receipt.GetResponseCode());
                //Console.WriteLine("ISO = " + receipt.GetISO());
                //Console.WriteLine("BankTotals = " + receipt.GetBankTotals());
                //Console.WriteLine("Message = " + receipt.GetMessage());
                //Console.WriteLine("AuthCode = " + receipt.GetAuthCode());
                //Console.WriteLine("Complete = " + receipt.GetComplete());
                //Console.WriteLine("TransDate = " + receipt.GetTransDate());
                //Console.WriteLine("TransTime = " + receipt.GetTransTime());
                //Console.WriteLine("Ticket = " + receipt.GetTicket());
                //Console.WriteLine("TimedOut = " + receipt.GetTimedOut());

                return success;
            }

            catch (Exception e)
            {
                return false;
            }
        }

        // ask moneris to verify transaction based on transactionKey
        // sample response:     <?xml version="1.0" standalone="yes"?><response><order_id></order_id><response_code>null</response_code><amount>null</amount><txn_num></txn_num><status>Invalid</status><transactionKey>AO6CTlOR4EUHwCtnjmR5Vly5b3f8UL</transactionKey></response>
        static Dictionary<string, string> VerifyMonerisTransaction(string transactionKey, bool isAsyncResponse)
        {
            string originMethod = isAsyncResponse ? "(Async Response)" : "(Normal Response)";

            using (var client = new WebClient())
            {
                var values = new NameValueCollection
                {
                    {"ps_store_id", StoreId},
                    {"hpp_key", HppKey},
                    {"transactionKey", transactionKey}
                };

                var raw = client.UploadValues
                (
                    MonerisBaseUrl + MonerisTransactionPath,
                    values
                );

                var xmlResponse = Encoding.UTF8.GetString(raw);
                var response = XDocument.Parse(xmlResponse).Element("response");
               
                //todo use name value collection for consistency ?
                var verifiedResponse = response.Descendants().ToDictionary(element => element.Name.LocalName, element => element.Value);

                Log.Info(originMethod + " Moneris VERIFICATION response " + ToLogString(verifiedResponse));
                return verifiedResponse;
            }
        }

        private static void UpdateRegistrationAsPaid(Entity paymentTransaction, string orderId, ShoppingCart shoppingCart, Dictionary<string, string> paymentResp,
            Dictionary<string, string> verifiedResp, bool isAsyncResponse, IReceiptProcessor receiptProcessor)
        {
            string originMethod = isAsyncResponse ? "(Async Response)" : "(Normal Response)";
            Log.Info(originMethod + " Updating Registrations as Paid " + shoppingCart);

            var context = XrmHelper.GetContext();
            List<Guid> approvedCoursesRegistrations = new List<Guid>();
            foreach (var detail in shoppingCart.Details.Values)
            {
                var billingRequest = new Entity("oems_billingrequest");
                billingRequest.Id = detail.PaymentInfo.BillingRequestId;
                billingRequest["oems_billingrequestid"] = detail.PaymentInfo.BillingRequestId;
                billingRequest["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.Paid);

                context.Attach(billingRequest);
                context.UpdateObject(billingRequest);

                var registration = new Entity("oems_eventregistration");
                registration.Id = detail.PaymentInfo.RegistrationId;
                registration["oems_eventregistrationid"] = detail.PaymentInfo.RegistrationId;
                registration["statuscode"] = new OptionSetValue(RegistrationHelper.EventRegistration.Sumbitted);

                context.Attach(registration);
                context.UpdateObject(registration);

                //Update form to submitted
                var autoApprove = IsAutoApproved(context, detail.PaymentInfo.RegistrationId);
                if (autoApprove)
                {
                    approvedCoursesRegistrations.Add(registration.Id);
                }
            }

            //Gets the user
            var eventRegistrationId = shoppingCart.Details.Values.First().PaymentInfo.RegistrationId;
            EntityReference receiptAttachmentRef = null;
            string documentStr = string.Empty;
            try
            {
                Log.Info(originMethod + " Generating Receipt");
                string html = receiptProcessor.CreateReceipt(shoppingCart);

                //Init the attachment variables
                Document receiptDoc = new Document(PageSize.A4, 25, 25, 25, 25);
                MemoryStream memStream = new MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(receiptDoc, memStream);
                receiptDoc.Open();
                HTMLWorker htmlWorker = new HTMLWorker(receiptDoc);
                StringReader sr = new StringReader(html);
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, receiptDoc, sr);
                receiptDoc.Close();

                var byteArray = memStream.ToArray();

                //Creates the receipt for the transaction
                var receiptAttachment = new Entity("oems_eventattachment");
                receiptAttachment["oems_name"] = "Payment Receipt";
                var evtAttachmentId = context.Create(receiptAttachment);
                receiptAttachment.Id = evtAttachmentId;
                receiptAttachmentRef = new EntityReference(receiptAttachment.LogicalName, receiptAttachment.Id);

                //Creates the annotation
                documentStr = Convert.ToBase64String(byteArray);
                var annotation = new Xrm.Annotation()
                {
                    FileName = "Payment Receipt.pdf",
                    IsDocument = true,
                    DocumentBody = documentStr,
                    MimeType = "application/pdf"
                };
                AnnotationsHelper.CreateAnnotation(annotation, receiptAttachment);

                Log.Info(originMethod + " Receipt generated");
            }
            catch (Exception ex)
            {
                Log.Info(originMethod + " There was an error generaing the receipt: " + ex.Message);
            }

            //Details of payment transaction
            var paymentTx = new oems_cctransaction()
            {
                Id = paymentTransaction.GetAttributeValue<Guid>("oems_cctransactionid"),
                oems_orderid = orderId,
                oems_paymentmethod = PaymentMethod.CreditCard,
                oems_paymentdate = DateTime.Now.ToUniversalTime(),
                oems_paymentstatus = PaymentStatus.Paid,
                oems_transactionnumber = verifiedResp[MonerisVerification.TxnNum],
                oems_transactionname = paymentResp[BillingService.MonerisResponse.TransName],
                oems_datestamp = paymentResp[BillingService.MonerisResponse.DateStamp],
                oems_timestamp = paymentResp[BillingService.MonerisResponse.TimeStamp],
                oems_bankapprovalcode = paymentResp[BillingService.MonerisResponse.BankApprovalCode],
                oems_responsecode = paymentResp[BillingService.MonerisResponse.ResponseCode],
                oems_isocode = paymentResp[BillingService.MonerisResponse.IsoCode],
                oems_message = paymentResp[BillingService.MonerisResponse.Message],
                oems_banktransactionid = paymentResp[BillingService.MonerisResponse.BankTransactionId],
                oems_cardholder = paymentResp[BillingService.MonerisResponse.Cardholder],
                oems_cardtype = paymentResp[MonerisResponse.Card],
                oems_transactionamount = Decimal.Parse(verifiedResp[MonerisVerification.Amount]), // read only amount exacly as returned by Moneris
                oems_cardnumber = isAsyncResponse ? paymentResp[BillingService.MonerisResponse.CardNumberAsync] : paymentResp[BillingService.MonerisResponse.CardNumber],
                oems_type = CreditCartTransactionType.PAYMENT_TRANSACTION,
                oems_receipt = receiptAttachmentRef
            };

            context.Attach(paymentTx);
            context.UpdateObject(paymentTx);

            context.SaveChanges();

            Log.Info(originMethod + " Payment transaction created");
            try
            {
                if (!string.IsNullOrWhiteSpace(documentStr))
                {
                    var query = new QueryExpression("oems_eventregistration");
                    query.ColumnSet = new ColumnSet(true);
                    query.Criteria.AddCondition(new ConditionExpression("oems_eventregistrationid", ConditionOperator.Equal, eventRegistrationId));
                    var registration = context.RetrieveMultiple(query).Entities.Single();

                    var user = (from con in context.CreateQuery("contact")
                                where con.GetAttributeValue<Guid>("contactid") == registration.GetAttributeValue<EntityReference>("oems_contact").Id
                                select con).Single();

                    SendReceiptEmail(user, paymentTx, documentStr);
                }

                Log.Info(originMethod + " Receipt Mail Sent");
            }
            catch (Exception ex)
            {
                Log.Info(originMethod + " There was an error sending the email");
            }

            //Update the approved courses
            foreach (var courseRegistrationId in approvedCoursesRegistrations)
            {
                //// set status to cancelled
                XrmHelper.SetStatus
                (
                    oems_EventRegistration.EntityLogicalName,
                    courseRegistrationId,
                    RegistrationHelper.EventRegistration.Approved,
                    RegistrationHelper.State.Active
                );
            }

            Log.Debug(originMethod + " Update completed " + shoppingCart);
        }

        public static void UpdateDeferralPayment(ShoppingCart shoppingCart, Entity user)
        {
            var context = XrmHelper.GetContext();
            var deferralUsed = shoppingCart.Details.Values.ToList().Sum(x => x.PaymentInfo.Deferral);

            //Transaction
            var paymentTransaction = new Entity("oems_cctransaction");
            paymentTransaction["oems_paymentmethod"] = new OptionSetValue(PaymentMethod.Deferral);
            paymentTransaction["oems_deferralamount"] = deferralUsed;
            paymentTransaction["oems_paymentdate"] = DateTime.Now;
            paymentTransaction["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.Paid);
            var paymentTransactionId = context.Create(paymentTransaction);

            //BR
            foreach (var detail in shoppingCart.Details.Values)
            {
                var paymentInfo = detail.PaymentInfo;

                Log.Info("Updating Deferral payment." + paymentInfo);

                var billingRequest = new Entity("oems_billingrequest");
                billingRequest.Id = paymentInfo.BillingRequestId;
                billingRequest["oems_paymenttransaction"] = new EntityReference(paymentTransaction.LogicalName, paymentTransactionId);
                billingRequest["oems_billingrequestid"] = paymentInfo.BillingRequestId;
                billingRequest["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.Paid);
                billingRequest["oems_deferralamount"] = paymentInfo.Deferral;
                billingRequest["oems_paymentamount"] = (decimal)0;

                context.Attach(billingRequest);
                context.UpdateObject(billingRequest);

                //Update form to submitted
                var registration = new Entity("oems_eventregistration");
                registration.Id = paymentInfo.RegistrationId;
                registration["oems_eventregistrationid"] = paymentInfo.RegistrationId;
                registration["statuscode"] = new OptionSetValue(RegistrationHelper.EventRegistration.Sumbitted);

                context.Attach(registration);
                context.UpdateObject(registration);
            }

            //Update user
            var newDeferral = user.GetAttributeValue<decimal>("oems_deferralcreditamount");
            newDeferral -= deferralUsed;
            user["oems_deferralcreditamount"] = newDeferral;
            context.Attach(user);
            context.UpdateObject(user);

            context.SaveChanges();
        }

        public static void UpdateZeroPayment(PaymentInfo paymentInfo)
        {
            Log.Info("Updating Zero payment." + paymentInfo);
            var context = XrmHelper.GetContext();

            //Transaction
            var paymentTransaction = new Entity("oems_cctransaction");
            paymentTransaction["oems_paymentmethod"] = new OptionSetValue(PaymentMethod.ZeroPayment);
            paymentTransaction["oems_paymentdate"] = DateTime.Now;
            paymentTransaction["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.Paid);
            var paymentTransactionId = context.Create(paymentTransaction);

            //BR
            var billingRequest = new Entity("oems_billingrequest");
            billingRequest.Id = paymentInfo.BillingRequestId;
            billingRequest["oems_paymenttransaction"] = new EntityReference(paymentTransaction.LogicalName, paymentTransactionId);
            billingRequest["oems_billingrequestid"] = paymentInfo.BillingRequestId;
            billingRequest["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.Paid);
            billingRequest["oems_deferralamount"] = (decimal)0;
            billingRequest["oems_paymentamount"] = (decimal)0;

            context.Attach(billingRequest);
            context.UpdateObject(billingRequest);

            //Update form to submitted
            var registration = new Entity("oems_eventregistration");
            registration.Id = paymentInfo.RegistrationId;
            registration["oems_eventregistrationid"] = paymentInfo.RegistrationId;
            registration["statuscode"] = new OptionSetValue(RegistrationHelper.EventRegistration.Sumbitted);

            context.Attach(registration);
            context.UpdateObject(registration);

            context.SaveChanges();
        }

        public static void UpdatePayLater(ShoppingCart shoppingCart)
        {
            var context = XrmHelper.GetContext();

            //Totals
            var totalCart = shoppingCart.Details.Values.ToList().Sum(x => x.PaymentInfo.AmountDueWithTax);

            //Transaction
            var paymentTransaction = new Entity("oems_cctransaction");
            paymentTransaction["oems_paymentmethod"] = new OptionSetValue(PaymentMethod.PayLater);
            paymentTransaction["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.PayLater);

            var paymentTransactionId = context.Create(paymentTransaction);

            //BR
            foreach (var detail in shoppingCart.Details.Values)
            {
                var paymentInfo = detail.PaymentInfo;

                Log.Info("Updating Pay Later Option." + paymentInfo);
                
                var billingRequest = new Entity("oems_billingrequest");
                billingRequest.Id = paymentInfo.BillingRequestId;
                billingRequest["oems_paymenttransaction"] = new EntityReference(paymentTransaction.LogicalName, paymentTransactionId);
                billingRequest["oems_billingrequestid"] = paymentInfo.BillingRequestId;
                billingRequest["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.PayLater);
                billingRequest["oems_paymentamount"] = paymentInfo.AmountDueBeforeTax;

                context.Attach(billingRequest);
                context.UpdateObject(billingRequest);

                //Update form to submitted
                var registration = new Entity("oems_eventregistration");
                registration.Id = paymentInfo.RegistrationId;
                registration["oems_eventregistrationid"] = paymentInfo.RegistrationId;
                registration["statuscode"] = new OptionSetValue(RegistrationHelper.EventRegistration.Sumbitted);

                context.Attach(registration);
                context.UpdateObject(registration);
            }

            context.SaveChanges();
        }

        public static void UpdatePayLocal(ShoppingCart shoppingCart)
        {
            var context = XrmHelper.GetContext();

            //Totals
            var totalCart = shoppingCart.Details.Values.ToList().Sum(x => x.PaymentInfo.AmountDueWithTax);

            //Transaction
            var paymentTransaction = new Entity("oems_cctransaction");
            paymentTransaction["oems_paymentmethod"] = new OptionSetValue(PaymentMethod.LocalToPay);
            paymentTransaction["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.LocalToPay);
            var paymentTransactionId = context.Create(paymentTransaction);

            //BR
            foreach (var detail in shoppingCart.Details.Values)
            {
                var paymentInfo = detail.PaymentInfo;
                Log.Info("Updating Charge to Local Option." + paymentInfo);
                
                var billingRequest = new Entity("oems_billingrequest");
                billingRequest.Id = paymentInfo.BillingRequestId;
                billingRequest["oems_paymenttransaction"] = new EntityReference(paymentTransaction.LogicalName, paymentTransactionId);
                billingRequest["oems_billingrequestid"] = paymentInfo.BillingRequestId;
                billingRequest["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.LocalToPay);
                billingRequest["oems_paymentamount"] = paymentInfo.AmountDueBeforeTax;

                context.Attach(billingRequest);
                context.UpdateObject(billingRequest);

                //Update form to submitted
                var registration = new Entity("oems_eventregistration");
                registration.Id = paymentInfo.RegistrationId;
                registration["oems_eventregistrationid"] = paymentInfo.RegistrationId;
                registration["statuscode"] = new OptionSetValue(RegistrationHelper.EventRegistration.Sumbitted);

                context.Attach(registration);
                context.UpdateObject(registration);
            }
            context.SaveChanges();
        }

        public static void UpdateChequePending(ShoppingCart shoppingCart, Entity user)
        {
            var context = XrmHelper.GetContext();
            var saveUser = false;

            //Totals
            var totalCart = shoppingCart.Details.Values.ToList().Sum(x => x.PaymentInfo.AmountDueWithTax);
            var totalDeferral = shoppingCart.Details.Values.ToList().Sum(x => x.PaymentInfo.Deferral);

            //Transaction
            var paymentTransaction = new Entity("oems_cctransaction");
            paymentTransaction["oems_paymentmethod"] = new OptionSetValue(PaymentMethod.Cheque);
            paymentTransaction["oems_deferralamount"] = totalDeferral;

            var transactionPaid = totalCart == totalDeferral;
            if (transactionPaid)
            {
                paymentTransaction["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.Paid);
                paymentTransaction["oems_paymentdate"] = DateTime.Now;
            }
            else
            {
                paymentTransaction["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.ChequePending);
            }
            var paymentTransactionId = context.Create(paymentTransaction);

            //BR
            foreach (var detail in shoppingCart.Details.Values)
            {
                var paymentInfo = detail.PaymentInfo;
 
                Log.Info("Updating Cheque payment as pending." + paymentInfo);

                var isDeferral = paymentInfo.Deferral != 0;
                var isDeferralPayed = isDeferral && paymentInfo.AmountDueWithTax == 0;

                var billingRequest = new Entity("oems_billingrequest");
                billingRequest.Id = paymentInfo.BillingRequestId;
                billingRequest["oems_paymenttransaction"] = new EntityReference(paymentTransaction.LogicalName, paymentTransactionId);
                billingRequest["oems_billingrequestid"] = paymentInfo.BillingRequestId;
                billingRequest["oems_paymentstatus"] = new OptionSetValue(isDeferralPayed || paymentInfo.AmountDueWithTax == 0 ? PaymentStatus.Paid : PaymentStatus.ChequePending);
                billingRequest["oems_deferralamount"] = paymentInfo.Deferral;
                billingRequest["oems_paymentamount"] = isDeferralPayed || paymentInfo.AmountDueWithTax == 0 ? (decimal)0 : paymentInfo.AmountDueBeforeTax;

                context.Attach(billingRequest);
                context.UpdateObject(billingRequest);

                //Update contact if deferral
                if (isDeferral)
                {
                    var newDeferral = user.GetAttributeValue<decimal>("oems_deferralcreditamount");
                    newDeferral -= paymentInfo.Deferral;
                    user["oems_deferralcreditamount"] = newDeferral;
                    saveUser = true;
                }

                //Update form to submitted
                var registration = new Entity("oems_eventregistration");
                registration.Id = paymentInfo.RegistrationId; 
                registration["oems_eventregistrationid"] = paymentInfo.RegistrationId;
                registration["statuscode"] = new OptionSetValue(RegistrationHelper.EventRegistration.Sumbitted);

                context.Attach(registration);
                context.UpdateObject(registration);
            }

            //Save user
            if (saveUser)
            {
                //user.Id = user.GetAttributeValue<bool>("oems_applyhst");
                context.Attach(user);
                context.UpdateObject(user);
            }

            context.SaveChanges();
        }

        public static PaymentPreloadInfo UpdateCreditCardPending(ShoppingCart shoppingCart, Entity user, bool zeroPayment)
        {
            var context = XrmHelper.GetContext();
            var saveUser = false;

            //Totals
            var totalCart = shoppingCart.Details.Values.ToList().Sum(x => x.PaymentInfo.AmountDueWithTax);
            var totalDeferral = shoppingCart.Details.Values.ToList().Sum(x => x.PaymentInfo.Deferral);

            //Transaction
            var paymentTransaction = new Entity("oems_cctransaction");
            paymentTransaction["oems_paymentmethod"] = new OptionSetValue(PaymentMethod.CreditCard);
            paymentTransaction["oems_deferralamount"] = totalDeferral;

            var transactionPaid = totalCart == totalDeferral;
            if (transactionPaid)
            {
                paymentTransaction["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.Paid);
                paymentTransaction["oems_paymentdate"] = DateTime.Now;
            }
            else
            {
                paymentTransaction["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.CreditCardPending);
            }
            var paymentTransactionId = context.Create(paymentTransaction);
            
            //BR
            List<Guid> approvedCoursesRegistrations = new List<Guid>();
            foreach (var detail in shoppingCart.Details.Values)
            {
                var paymentInfo = detail.PaymentInfo;

                Log.Info("Updating CC payment as pending." + paymentInfo);

                var isDeferral = paymentInfo.Deferral != 0;
                var isDeferralPayed = isDeferral && paymentInfo.AmountDueWithTax == 0;

                var billingRequest = new Entity("oems_billingrequest");
                billingRequest.Id = paymentInfo.BillingRequestId;
                billingRequest["oems_paymenttransaction"] = new EntityReference(paymentTransaction.LogicalName, paymentTransactionId);
                billingRequest["oems_billingrequestid"] = paymentInfo.BillingRequestId;
                billingRequest["oems_paymentstatus"] = new OptionSetValue(isDeferralPayed || paymentInfo.AmountDueWithTax == 0 ? PaymentStatus.Paid : PaymentStatus.CreditCardPending);
                billingRequest["oems_deferralamount"] = paymentInfo.Deferral;
                billingRequest["oems_paymentamount"] = isDeferralPayed || paymentInfo.AmountDueWithTax == 0 ? (decimal)0 : paymentInfo.AmountDueBeforeTax;

                context.Attach(billingRequest);
                context.UpdateObject(billingRequest);

                //Update contact if deferral
                if (isDeferral)
                {
                    var newDeferral = (decimal) user["oems_deferralcreditamount"];
                    newDeferral -= paymentInfo.Deferral;
                    user["oems_deferralcreditamount"] = newDeferral;
                    saveUser = true;
                }

                if (isDeferralPayed)
                {
                    var registration = new Entity("oems_eventregistration");
                    registration.Id = paymentInfo.RegistrationId;
                    registration["oems_eventregistrationid"] = paymentInfo.RegistrationId;
                    registration["statuscode"] = new OptionSetValue(RegistrationHelper.EventRegistration.Sumbitted);

                    context.Attach(registration); 
                    context.UpdateObject(registration);

                    var autoApprove = IsAutoApproved(context, detail.PaymentInfo.RegistrationId);
                    if (autoApprove)
                    {
                        approvedCoursesRegistrations.Add(registration.Id);
                    }
                }
            }

            //Save user
            if (saveUser)
            {
                context.Attach(user);
                context.UpdateObject(user);
            }

            //Preload moneris Payment
            PaymentPreloadInfo monerisInfo = null;
            if (!zeroPayment)
            {
                monerisInfo = BillingService.PreloadMonerisPayment(shoppingCart, paymentTransactionId);                
            }

            context.SaveChanges();

            //Update the approved courses
            foreach (var courseRegistrationId in approvedCoursesRegistrations)
            {
                //// set status to cancelled
                XrmHelper.SetStatus
                (
                    oems_EventRegistration.EntityLogicalName,
                    courseRegistrationId,
                    RegistrationHelper.EventRegistration.Approved,
                    RegistrationHelper.State.Active
                );
            }


            return monerisInfo;
        }

        public static void UndoMarkingAsCreditCardPending(ShoppingCart shoppingCart, Entity user)
        {
            var context = XrmHelper.GetContext();

            decimal newDeferral = (decimal)0;
            foreach (var detail in shoppingCart.Details.Values)
            {
                if (detail.PaymentInfo.PaymentStatus != PaymentStatus.CreditCardPending && detail.PaymentInfo.PaymentStatus != PaymentStatus.Paid) 
                    continue;

                Log.Debug("Erasing CC payment pending status" + detail);

                var billingRequest = new Entity("oems_billingrequest");
                billingRequest.Id = detail.PaymentInfo.BillingRequestId;
                billingRequest["oems_billingrequestid"] = detail.PaymentInfo.BillingRequestId;
                billingRequest["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.Unpaid);
                billingRequest["oems_deferralamount"] = null;
                billingRequest["oems_paymentamount"] = null;
                billingRequest["oems_transactionid"] = null;
                billingRequest["oems_paymenttransaction"] = null;

                if (detail.PaymentInfo.Deferral > 0)
                {
                    newDeferral += detail.PaymentInfo.Deferral;
                }

                context.Attach(billingRequest);
                context.UpdateObject(billingRequest);
            }

            if (newDeferral > 0)
            {
                newDeferral += user.GetAttributeValue<decimal>("oems_deferralcreditamount");
                user["oems_deferralcreditamount"] = newDeferral;

                context.Attach(user);
                context.UpdateObject(user);
            }

            context.SaveChanges();
            Log.Debug("Mark creditCard as unpaid" );
        }

        public static void SendReceiptEmail(Entity user, Entity paymentTransaction, string documentStr)
        {
            string mailTo = user.GetAttributeValue<string>("emailaddress1");
            string userName = user.GetAttributeValue<string>("fullname");
            var context = XrmHelper.GetContext();
             
            //Basic info
            var fromUser = context.SystemUserSet.FirstOrDefault(u => u.FullName == XrmHelper.GetSiteSettingValueOrDefault("SystemEmail/Defaults/EmailFrom", "Admin User"));
            if (fromUser == null)
                throw new Exception("Error retrieving system user: " + XrmHelper.GetSiteSettingValueOrDefault("SystemEmail/Defaults/EmailFrom", "Admin User") + " from Site Setting: SystemEmail/Defaults/EmailFrom");

            var emailId = Guid.NewGuid();
            var emailFrom = new ActivityParty { PartyId = fromUser.ToEntityReference() };
            var emailTo = new ActivityParty { PartyId = user.ToEntityReference() };

            var htmlBody = new StringBuilder();
            htmlBody.Append(XrmHelper.GetSnippetValueOrDefault("Payment/Receipt/Email/Body",
                "<br />Dear " + user.GetAttributeValue<string>("fullname") + ",<br /><br /><br />" +
                "Your credit card was charged the amount of $" + paymentTransaction.GetAttributeValue<decimal>("oems_transactionamount") + "<br /><br /><br />" +
                //"<b>Event Information</b><br /><br /><br />" + 
                //"Event Name: billing event - DO NOT DELETE <br />" + "Event Date:  Thursday, July 2, 2015 9:00:00 AM  to  Thursday, July 2, 2015 3:30:00 PM<br /><br /><br />" +
                "<b>Payment Information</b><br /><br /><br />" +
                "Event Registration: " + paymentTransaction.GetAttributeValue<string>("oems_orderid") + "<br /><br />" +
                "Transaction Type: " + paymentTransaction.GetAttributeValue<string>("oems_transactionname") + "<br /><br />" +
                "Date and Time: " + paymentTransaction.GetAttributeValue<DateTime>("oems_paymentdate").ToString("MM/dd/yyyy") + " " + paymentTransaction.GetAttributeValue<string>("oems_timestamp") + "<br /><br />" +
                "Payment Amount: $" + paymentTransaction.GetAttributeValue<decimal>("oems_transactionamount") + "<br /><br />" +
                "Authorization Code: " + paymentTransaction.GetAttributeValue<string>("oems_bankapprovalcode") + "<br /><br />" +
                "Response Code / ISO Code: " + paymentTransaction.GetAttributeValue<string>("oems_responsecode") + " / " + paymentTransaction.GetAttributeValue<string>("oems_isocode") + "<br /><br />" +
                "Message: " + paymentTransaction.GetAttributeValue<string>("oems_message") + "<br /><br />" +
                "Reference Number: " + paymentTransaction.GetAttributeValue<string>("oems_banktransactionid") + "<br /><br />" +
                "Name on the Card: " + paymentTransaction.GetAttributeValue<string>("oems_cardholder") + "<br /><br />" +
                "Credit Card Number: " + paymentTransaction.GetAttributeValue<string>("oems_cardnumber") + "<br /><br /><br />" +
                "Thank you,<br /><br />" +
                "ETFO EMS Team."
            ));

            //Creates the receipt attachment
            List<ActivityMimeAttachment> attachments = new List<ActivityMimeAttachment>();
            attachments.Add(new ActivityMimeAttachment()
            {
                ObjectId = new EntityReference(Email.EntityLogicalName, emailId),
                ObjectTypeCode = Email.EntityLogicalName,
                FileName = "Payment Receipt.pdf",
                MimeType = "application/pdf",
                Body = documentStr,
            });

            //Creates the email
            var email = new Email
            {
                Id = emailId,
                To = new[] { emailTo },
                From = new[] { emailFrom },
                Subject = XrmHelper.GetSnippetValueOrDefault("Payment/Receipt/Email/Subject", "Payment Confirmation"),
                Description = htmlBody.ToString(),
                email_activity_mime_attachment = attachments
            };

            context.Create(email);
            context.SaveChanges();

            var trackingTokenEmailRequest = new GetTrackingTokenEmailRequest();
            var trackingTokenEmailResponse = (GetTrackingTokenEmailResponse)context.Execute(trackingTokenEmailRequest);

            var sendEmailreq = new SendEmailRequest
            {
                EmailId = email.Id,
                TrackingToken = trackingTokenEmailResponse.TrackingToken,
                IssueSend = true
            };

            var sendEmailresp = (SendEmailResponse)context.Execute(sendEmailreq);
        }

        // convert dictionary into easy to log string
        public static string ToLogString(Dictionary<string, string> response)
        {
            if (response == null) return "null";
            return String.Join(", ", response.Keys.Select(param => param + ':' + response[param]).ToArray());
        }

        private static bool IsAutoApproved(XrmServiceContext serviceContext, Guid eventRegistrationId)
        {
            var isAutoApproved = false;
            var term =
            (
                from reg in serviceContext.CreateQuery("oems_eventregistration")
                join evt in serviceContext.CreateQuery("oems_event") on ((EntityReference)reg["oems_event"]).Id equals evt["oems_eventid"]
                join trm in serviceContext.CreateQuery("oems_event") on ((EntityReference)evt["oems_courseterm"]).Id equals trm["oems_eventid"]
                where reg.GetAttributeValue<Guid>("oems_eventregistrationid") == eventRegistrationId
                where evt.GetAttributeValue<EntityReference>("oems_courseterm") != null
                select trm
            ).FirstOrDefault();

            if (term != null)
            {
                isAutoApproved = term.GetAttributeValue<bool>("oems_autoapproveflag");
            }

            return isAutoApproved;
        }

    }
}