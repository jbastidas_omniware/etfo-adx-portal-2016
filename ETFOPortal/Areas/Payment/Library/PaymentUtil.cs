﻿using System;
using System.Collections.Generic;
using System.Web;
using Site.Areas.Payment.Models;
using Site.Library;

namespace Site.Areas.Payment.Library
{
    public static class PaymentUtil
    {
        public static ShoppingCart ShoppingCart
        {
            get
            {
                return HttpContext.Current != null && HttpContext.Current.Session["ShoppingCart"] != null ? ObjectCopier.Clone(((ShoppingCart)HttpContext.Current.Session["ShoppingCart"])) : new ShoppingCart();
            }
            set
            {
                if(HttpContext.Current != null)
                    HttpContext.Current.Session["ShoppingCart"] = value; 
            }
        }

        public static ShoppingCart ShoppingCartCopy
        {
            get
            {
                return HttpContext.Current != null && HttpContext.Current.Session["ShoppingCartCopy"] != null ? (ShoppingCart)HttpContext.Current.Session["ShoppingCartCopy"] : new ShoppingCart();
            }
            set
            {

                if (HttpContext.Current != null)
                    HttpContext.Current.Session["ShoppingCartCopy"] = value;
            }
        }

        public static bool IsUserPayingShoppingCart
        {
            get
            {
                return HttpContext.Current != null && HttpContext.Current.Session["IsUserPayingShoppingCart"] != null && (bool)HttpContext.Current.Session["IsUserPayingShoppingCart"];
            }
            set
            {
                if (HttpContext.Current != null)
                {
                    if (value)
                    {
                        HttpContext.Current.Session["IsUserPayingShoppingCart"] = true;
                    }
                    else
                    {
                        HttpContext.Current.Session.Remove("IsUserPayingShoppingCart");
                    }
                }
            }
        }

        public static void AddShoppingCartDetail(Guid key, ShoppingCartDetail detail)
        {
            var shoppingCart = ShoppingCart;
            shoppingCart.Details[key] = detail;
            ShoppingCart = shoppingCart;
        }

        public static void DeleteShoppingCartDetail(Guid key)
        {
            var shoppingCart = ShoppingCart;

            if (!shoppingCart.Details.ContainsKey(key)) 
                return;

            shoppingCart.Details.Remove(key);
            ShoppingCart = shoppingCart;
        }

        public static void Clean()
        {
            var shoppingCart = ShoppingCart;
            shoppingCart.OrderId = string.Empty;
            shoppingCart.SubTotal = 0;
            shoppingCart.TotalTax = 0;
            shoppingCart.Deferral = 0;
            shoppingCart.Due = 0;
            shoppingCart.TaxPending = 0;
            shoppingCart.TotalToPay = 0;
            shoppingCart.Details = new Dictionary<Guid, ShoppingCartDetail>();

            ShoppingCart = shoppingCart;
            IsUserPayingShoppingCart = false;
        }

        public static void CleanCopy()
        {
            var shoppingCart = ShoppingCartCopy;
            shoppingCart.OrderId = string.Empty;
            shoppingCart.SubTotal = 0;
            shoppingCart.TotalTax = 0;
            shoppingCart.Deferral = 0;
            shoppingCart.Due = 0;
            shoppingCart.TaxPending = 0;
            shoppingCart.TotalToPay = 0;
            shoppingCart.Details = new Dictionary<Guid, ShoppingCartDetail>();

            ShoppingCartCopy = shoppingCart;
        }

        public static void ClearDetails()
        {
            var shoppingCart = ShoppingCart;
            shoppingCart.Details = new Dictionary<Guid, ShoppingCartDetail>();

            ShoppingCart = shoppingCart;
        }
    }
}