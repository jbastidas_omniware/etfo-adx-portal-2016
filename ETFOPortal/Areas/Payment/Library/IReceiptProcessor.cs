﻿using Site.Areas.Payment.Models;

namespace Site.Areas.Payment.Library
{
    public interface IReceiptProcessor
    {
        string CreateReceipt(ShoppingCart model);
    }
}
