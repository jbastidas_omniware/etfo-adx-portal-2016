﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotNetOpenAuth.Messaging;
using Microsoft.Xrm.Sdk;
using Site.Areas.Payment.Library;
//using Xrm;

namespace Site.Areas.Payment.Models
{
    [Serializable]
    public class ShoppingCart
    {
        public string OrderId { get; set; }
        public decimal SubTotal { get; set; }
        public decimal TotalTax { get; set; }
        public decimal Deferral { get; set; }
        public decimal Due { get; set; }
        public decimal TaxPending { get; set; }
        public decimal TotalToPay { get; set; }
        public bool IsAsyncRequest { get; set; }
        
        public CreditCardInfo CreditCard { get; set; }
        public Dictionary<Guid, ShoppingCartDetail> Details { get; set; }

        public ShoppingCart()
        {
            Details = new Dictionary<Guid, ShoppingCartDetail>();
        }
    }

    [Serializable]
    public class ShoppingCartDetail
    {
        public EventShopingCart Event;
        public BillingService.PaymentInfo PaymentInfo;
    }

    [Serializable]
    public class CreditCardInfo
    {
        public string TransactionID { get; set; }
        public string TransactionType { get; set; }
        public string DateandTime { get; set; }
        public string PaymentAmount { get; set; }
        public string AuthorizationCode { get; set; }
        public string ResponseCode { get; set; }
        public string ISOCode { get; set; }
        public string Message { get; set; }
        public string ReferenceNumber { get; set; }
        public string NameOnTheCard { get; set; }
        public string CreditCardNumber { get; set; }
        public string ExpiryDate { get; set; }
    }
}