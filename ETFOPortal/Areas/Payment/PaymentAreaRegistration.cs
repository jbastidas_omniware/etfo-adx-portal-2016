﻿using System.Web.Http;
using System.Web.Mvc;
using Adxstudio.Xrm.Web.Mvc;

namespace Site.Areas.Payment
{
    public class PaymentRegistration : AreaRegistration
	{
		public override string AreaName
		{
            get { return "Payment"; }
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapSiteMarkerRoute(
                "Payment",
                "Payment",
				"{action}",
                new { controller = "Payment", action = "Index" }, new { action = @"^Payment.*" });

            context.MapRoute("PaymentDefault", 
                "payment/{action}/{id}", 
                new { controller = "Payment", action = "Index", id = RouteParameter.Optional, area = "Payment" });
		}
	}
}
