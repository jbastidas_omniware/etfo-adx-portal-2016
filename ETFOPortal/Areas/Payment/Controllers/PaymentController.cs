﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Adxstudio.Xrm.Web.Mvc;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Portal.Web;
using Newtonsoft.Json.Linq;
using Site.Library;
using Site.Areas.Payment.Library;
using Site.Helpers;
using Xrm;
using log4net;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Configuration;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Site.Areas.Payment.Models;
using Site.Areas.ETFO.Library;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using System.IO;
using iTextSharp.tool.xml;
using System.Xml.Linq;
using System.Threading;

namespace Site.Areas.Payment.Controllers
{
    [PortalView]
    [AjaxAuthorizeAttribute]
    public class PaymentController : BaseController, IReceiptProcessor
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(PaymentController));
        private enum EventRegistrationResult
        {
            Default = 0,
            OverlappingRegistrationCourses = 1,
            OverlappingShoppingCartCourses = 2,
            WaitingListFull = 3,
            WaitingListOffer = 4,
            SucessPayment = 5
        }

        string IReceiptProcessor.CreateReceipt(ShoppingCart model)
        {
            return this.RenderPartialViewToString("PaymentReceipt", model);
        }

        [HttpPost]
        public JsonResult ResetBackUrl()
        {
            Session["BackUrl"] = null;
            return Json(new { Success = true });
        }

        [HttpPost]
        public JsonResult AcceptWaitingListOffer(Guid eventRegistrationId, Guid eventId)
        {
            var context = XrmHelper.GetContext();
            var request = new Entity { LogicalName = "oems_waitinglistrequest" };
            request.SetAttributeValue("oems_event", new EntityReference("oems_event", eventId));
            request.SetAttributeValue("oems_eventregistration", new EntityReference("oems_eventregistration", eventRegistrationId));
            var id = context.Create(request);

            return Json(new { Success = true });
        }

        [HttpPost]
        public JsonResult UpdateEventRegistrationWithPayment(Guid eventRegistrationId, Guid eventId, string updateEventRegistrationData, string documents, string attachments, bool? returningPresenter, bool isSubmit)
        {
            try
            {
                var context = XrmHelper.GetContext();
                var result = EventRegistrationResult.Default;
                List<EventListing> overlappingRegistrationCourses = null;
                List<EventListing> overlappingShoppingCartCourses = null;
                BillingService.PaymentInfo info = null;


                Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Start");
                var user = this.GetContact();
                var eventColumnSet = new ColumnSet(new String[] 
                {
                    "oems_waitinglistcomponentflag", "oems_eventfullflag", "oems_eventname", "oems_billingpaylater", "oems_startdate",
                    "oems_enddate", "oems_courseflag", "oems_courseterm", "oems_courseflag", "oems_coursecode", "oems_coursedateslot"
                });
                var oemsEvent = (oems_Event)context.Retrieve("oems_event", eventId, eventColumnSet);
                Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Event: " + oemsEvent.oems_EventName + " / Contact: " + user.FullName);

                #region Save

                var eventRegistration = new oems_EventRegistration
                {
                    oems_EventRegistrationId = eventRegistrationId,
                    oems_dynamicregformdata = updateEventRegistrationData
                };

                //Returning Presenter
                if (returningPresenter.HasValue)
                {
                    Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Returning presenter: " + returningPresenter.Value.ToString());
                    eventRegistration["oems_returningpresenter"] = returningPresenter.Value;
                }

                //Saves the event registration
                try
                {
                    if (!context.IsAttached(eventRegistration))
                    {
                        Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Attaching event registration");
                        context.Attach(eventRegistration);
                    }

                    Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Before saving regisration");

                    context.UpdateObject(eventRegistration);
                    context.SaveChanges();

                    Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Registration saved");
                }
                catch (Exception ex)
                {
                    Log.Error("Error saving the registration (" + eventRegistrationId.ToString() + "): " + ex.Message);
                    if (ex.InnerException != null)
                    {
                        Log.Error("Error saving the registration (Inner Ex Message): " + ex.InnerException.Message);
                    }

                    if (ex.InnerException != null)
                    {
                        return Json(new { Success = false, Message = ex.InnerException.Message });
                    }
                    else
                    {
                        return Json(new { Success = false, Message = ex.Message });
                    }
                }

                #endregion

                #region Submit

                //Registration type
                var ercs = new ColumnSet(new String[] { "oems_registrationtype" });
                var er = (oems_EventRegistration)context.Retrieve("oems_eventregistration", eventRegistrationId, ercs);
                var registrationType = er.GetAttributeValue<OptionSetValue>("oems_registrationtype") != null ? er.GetAttributeValue<OptionSetValue>("oems_registrationtype").Value : RegistrationHelper.EventRegistrationType.Attendee;
                if (isSubmit)
                {
                    Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Check overlapping");

                    #region Check overlapping
                    var isCourse = oemsEvent.GetAttributeValue<bool>("oems_courseflag");
                    var startDate = oemsEvent.GetAttributeValue<DateTime>("oems_startdate");
                    var endDate = oemsEvent.GetAttributeValue<DateTime>("oems_enddate");
                    if (isCourse)
                    {
                        Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Is Course");

                        var courseDates = (from cd in context.CreateQuery("oems_coursedateslots")
                                           where cd.GetAttributeValue<Guid>("oems_coursedateslotsid") == oemsEvent.GetAttributeValue<EntityReference>("oems_coursedateslot").Id
                                           select cd).FirstOrDefault();

                        startDate = courseDates.GetAttributeValue<DateTime>("oems_startdate");
                        endDate = courseDates.GetAttributeValue<DateTime>("oems_enddate");
                        var termId = oemsEvent.GetAttributeValue<EntityReference>("oems_courseterm").Id;

                        overlappingRegistrationCourses = this.OverlapingRegistrationCourses(eventId, eventRegistrationId, termId, startDate, endDate);
                        if (overlappingRegistrationCourses.Count > 0)
                        {
                            result = EventRegistrationResult.OverlappingRegistrationCourses;
                            goto Finish;
                        }

                        overlappingShoppingCartCourses = this.OverlapingShoppingCartCourses(termId, startDate, endDate);
                        if (overlappingShoppingCartCourses.Count > 0)
                        {
                            result = EventRegistrationResult.OverlappingShoppingCartCourses;
                            goto Finish;
                        }
                    }
                    #endregion

                    Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Check WL");

                    #region Waiting list flags
                    // + We dont offer waiting list spots to presenters
                    var waitingListComponentFlag = oemsEvent.GetAttributeValue<bool>("oems_waitinglistcomponentflag");
                    if (waitingListComponentFlag && registrationType == RegistrationHelper.EventRegistrationType.Attendee)
                    {
                        Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): WL Attendee");

                        //Registration has waiting list request in status Offered
                        var request = RegistrationHelper.GetRegistrationWithWaitingListRequest(eventId, eventRegistrationId, context);
                        if (request != null && request.GetAttributeValue<OptionSetValue>("statuscode").Value == RegistrationHelper.WaitingListStatus.Offered)
                        {
                            Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): WL offered");

                            request.SetAttributeValue("oems_accepteddate", DateTime.UtcNow);
                            if (!context.IsAttached(eventRegistration))
                            {
                                context.Attach(eventRegistration);
                            }
                            context.UpdateObject(eventRegistration);
                            context.SaveChanges();

                            XrmHelper.SetStatus
                            (
                                "oems_waitinglistrequest",
                                request.GetAttributeValue<Guid>("oems_waitinglistrequestid"),
                                RegistrationHelper.WaitingListStatus.Accepted,
                                RegistrationHelper.State.Active
                            );
                        }
                        else
                        {
                            //Requery event again maybe data has changed
                            var contextNoCache = (XrmServiceContext)CrmConfigurationManager.CreateContext("XrmNoCache");
                            oemsEvent = (oems_Event)contextNoCache.Retrieve("oems_event", eventId, eventColumnSet);
                            var waitingListFullFlag = oemsEvent.GetAttributeValue<bool>("oems_waitinglistfullflag");
                            var eventFullFlag = oemsEvent.GetAttributeValue<bool>("oems_eventfullflag");
                            if (waitingListFullFlag)
                            {
                                Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): WL FULL");
                                result = EventRegistrationResult.WaitingListFull;
                                goto Finish;
                            }

                            if (eventFullFlag)
                            {
                                Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): WL FULL Offered");
                                result = EventRegistrationResult.WaitingListOffer;
                                goto Finish;
                            }
                        }
                    }
                    #endregion

                    Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Check payment requirements");

                    #region Check if the event requires payment
                    info = BillingService.GetPaymentInformation(eventRegistrationId);
                    if (info == null)
                    {
                        Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): No payment info");

                        eventRegistration.statuscode = RegistrationHelper.EventRegistration.Sumbitted;
                        if (!context.IsAttached(eventRegistration))
                        {
                            Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Attaching event registration");
                            context.Attach(eventRegistration);
                        }
                        context.UpdateObject(eventRegistration);
                        context.SaveChanges();

                        Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Event registration saved");
                    }
                    else if (info.AmountDueWithTax == 0)
                    {
                        Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Zero payment started");

                        BillingService.UpdateZeroPayment(info);
                        info = null;

                        Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Zero pyment saved ");

                        eventRegistration.statuscode = RegistrationHelper.EventRegistration.Sumbitted;
                        if (!context.IsAttached(eventRegistration))
                        {
                            Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Attaching event registration");
                            context.Attach(eventRegistration);
                        }
                        context.UpdateObject(eventRegistration);
                        context.SaveChanges();

                        Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Event registration saved");
                    }
                    else
                    {
                        Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Payment event");

                        //Get event for displaying purposes
                        var scEvent = new EventShopingCart()
                        {
                            Id = oemsEvent.Id,
                            EventRegistrationId = eventRegistrationId,
                            EventName = oemsEvent.GetAttributeValue<string>("oems_eventname"),
                            PayLater = oemsEvent.GetAttributeValue<bool>("oems_billingpaylater"),
                            StartDate = startDate,
                            EndDate = endDate,
                            IsCourse = isCourse,
                            CourseCode = oemsEvent.GetAttributeValue<string>("oems_coursecode"),
                            CourseTermId = oemsEvent.GetAttributeValue<EntityReference>("oems_courseterm") != null ? oemsEvent.GetAttributeValue<EntityReference>("oems_courseterm").Id : (Guid?)null
                        };

                        info.EventName = scEvent.EventName;
                        var detail = new ShoppingCartDetail() { Event = scEvent, PaymentInfo = info };

                        PaymentUtil.AddShoppingCartDetail(eventRegistrationId, detail);

                        Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Added payment to shopping cart");
                    }

                    #endregion

                    Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Payment finished");

                    // do not send all the data to client in case someone adds some sensitive info to the object
                    result = EventRegistrationResult.SucessPayment;
                }

                #endregion

                Finish: Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "):  Result: " + result);

                #region Save attachments

                if (!String.IsNullOrEmpty(attachments))
                {
                    Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Saving attachments");

                    dynamic oattachments = JArray.Parse(attachments);
                    foreach (var att in oattachments)
                    {
                        var attr = "";
                        var documentTitle = "";
                        if (att.attachmentType == RegistrationHelper.AttachmentTypes.CourseOutiline)
                        {
                            attr = "oems_courseoutline1";
                            documentTitle = "Course Outline";
                        }
                        else if (att.attachmentType == RegistrationHelper.AttachmentTypes.Resume)
                        {
                            attr = "oems_resume1";
                            documentTitle = "Resume";
                        }
                        else if (att.attachmentType == RegistrationHelper.AttachmentTypes.LetterReference1)
                        {
                            attr = "oems_letterofreference1";
                            documentTitle = "Letter of Reference 1";
                        }
                        else if (att.attachmentType == RegistrationHelper.AttachmentTypes.LetterReference2)
                        {
                            attr = "oems_letterofreference2";
                            documentTitle = "Letter of Reference 2";
                        }

                        //Creates the event document related
                        var evtAttachment = new Entity("oems_eventattachment");
                        evtAttachment["oems_name"] = documentTitle;
                        var evtAttachmentId = context.Create(evtAttachment);
                        evtAttachment.Id = evtAttachmentId;
                        var evtAttachmentRef = new EntityReference(evtAttachment.LogicalName, evtAttachment.Id);

                        //Creates the annotation
                        var annotation = new Xrm.Annotation()
                        {
                            FileName = att.fileName,
                            IsDocument = true,
                            DocumentBody = att.body,
                            MimeType = att.mimeType,
                            Subject = att.subject
                        };
                        AnnotationsHelper.CreateAnnotation(annotation, evtAttachment, context);

                        //Assign the event document to the event registration
                        eventRegistration[attr] = evtAttachmentRef;
                    }
                }

                #endregion

                #region Save documents

                if (!String.IsNullOrEmpty(documents))
                {
                    Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Saving documents");

                    dynamic odocuments = JArray.Parse(documents);
                    foreach (var d in odocuments)
                    {
                        var annotation = new Xrm.Annotation()
                        {
                            FileName = d.fileName,
                            //FileSize = d.fileSize,
                            IsDocument = true,
                            DocumentBody = d.body,
                            MimeType = d.mimeType,
                            Subject = d.subject
                        };
                        AnnotationsHelper.CreateAnnotation(annotation, eventRegistration, context);
                    }
                }

                #endregion

                Log.Info("Saving Registration (" + eventRegistrationId.ToString() + "): Before result");

                switch (result)
                {
                    case EventRegistrationResult.OverlappingRegistrationCourses:
                        return new JsonNetResult() { Data = new { Success = false, RegistrationOverlapping = true, OverlappingSchedules = overlappingRegistrationCourses } };

                    case EventRegistrationResult.OverlappingShoppingCartCourses:
                        return new JsonNetResult() { Data = new { Success = false, ShoppingCartOverlapping = true, OverlappingSchedules = overlappingShoppingCartCourses } };

                    case EventRegistrationResult.WaitingListFull:
                        return Json(new { Success = true, RequirePayment = false, WaitingListFull = true });

                    case EventRegistrationResult.WaitingListOffer:
                        return Json(new { Success = true, RequirePayment = false, DoWaitingListOffer = true });

                    case EventRegistrationResult.SucessPayment:
                        return Json(new { Success = true, RequirePayment = info != null, IsApplication = registrationType == RegistrationHelper.EventRegistrationType.Presenter });

                    default:
                        return Json(new { Success = true, RequirePayment = false, IsApplication = registrationType == RegistrationHelper.EventRegistrationType.Presenter });
                }
            }
            catch (Exception ex)
            {
                Log.Error("Saving Registration: General error (" + eventRegistrationId.ToString() + "): " + ex.Message);
                if (ex.InnerException != null)
                {
                    Log.Error("Saving Registration: General error (Inner Ex Message): " + ex.InnerException.Message);
                }

                return new JsonNetResult() { Data = new { Success = false } };
            }
        }

        [HttpPost]
        public JsonResult CancelRegistration(Guid eventRegistrationId)
        {
            var context = XrmHelper.GetContext();
            var entity = context.Retrieve("oems_eventregistration", eventRegistrationId, new ColumnSet("statuscode"));
            if (RegistrationHelper.EventRegistration.Initial == ((OptionSetValue)entity["statuscode"]).Value)
            {
                context.Delete(entity.LogicalName, eventRegistrationId);
                context.SaveChanges();
            }
            return Json(new { Success = true });
        }

        [HttpPost]
        public JsonResult UpdateEventRegistrationAttachments(Guid eventRegistrationId, Guid eventId, string attachments)
        {
            //Save attachments
            if (!String.IsNullOrEmpty(attachments))
            {
                var context = XrmHelper.GetContext();
                var eventRegistration = new oems_EventRegistration()
                {
                    oems_EventRegistrationId = eventRegistrationId
                };

                dynamic oattachments = JArray.Parse(attachments);
                foreach (var att in oattachments)
                {
                    var attr = "";
                    var documentTitle = "";
                    if (att.attachmentType == RegistrationHelper.AttachmentTypes.IndividualCourseParticipationLetter)
                    {
                        attr = "oems_individualcourseparticipationletter1";
                        documentTitle = "Individual Course Participation Letter";
                    }
                    else if (att.attachmentType == RegistrationHelper.AttachmentTypes.InstructorCourseAgreementForm)
                    {
                        attr = "oems_instructorcourseagreementform1";
                        documentTitle = "Instructor Course Agreement Form";
                    }
                    else if (att.attachmentType == RegistrationHelper.AttachmentTypes.InstructorPayrollEnrollmentForm)
                    {
                        attr = "oems_instructorpayrollenrollmentform1";
                        documentTitle = "Instructor Payroll Enrollment Form";
                    }

                    //Creates the event document related
                    var evtAttachment = new Entity("oems_eventattachment");
                    evtAttachment["oems_name"] = documentTitle;
                    var evtAttachmentId = context.Create(evtAttachment);
                    evtAttachment.Id = evtAttachmentId;
                    var evtAttachmentRef = new EntityReference(evtAttachment.LogicalName, evtAttachment.Id);

                    //Creates the annotation
                    var annotation = new Xrm.Annotation()
                    {
                        FileName = att.fileName,
                        IsDocument = true,
                        DocumentBody = att.body,
                        MimeType = att.mimeType,
                        Subject = att.subject
                    };
                    AnnotationsHelper.CreateAnnotation(annotation, evtAttachment, context);

                    //Assign the event document to the event registration
                    eventRegistration[attr] = evtAttachmentRef;
                }

                //Saves the event registration
                context.Attach(eventRegistration);
                context.UpdateObject(eventRegistration);
                context.SaveChanges();
            }

            return Json(new { Success = true });
        }

        [HttpPost]
        public JsonResult ValidateLocalCode(Guid eventRegistrationId, string code)
        {
            var context = XrmHelper.GetContext();

            var eventRegistration = (from reg in context.CreateQuery("oems_eventregistration")
                                     where reg.GetAttributeValue<Guid>("oems_eventregistrationid") == eventRegistrationId
                                     select reg).FirstOrDefault();

            var local = eventRegistration.GetAttributeValue<EntityReference>("oems_local");
            if (local != null)
            {
                var eventLocal = (from evtLocal in context.CreateQuery("oems_eventlocal")
                                  where evtLocal.GetAttributeValue<EntityReference>("oems_billingevent").Id == eventRegistration.GetAttributeValue<EntityReference>("oems_event").Id
                                  where evtLocal.GetAttributeValue<EntityReference>("oems_local").Id == local.Id
                                  select evtLocal).FirstOrDefault();

                if (eventLocal != null)
                {
                    var eventCode = eventLocal.GetAttributeValue<string>("oems_paymentcode");
                    if (eventCode != null && code.ToLower().Trim() == eventCode.ToLower().Trim())
                    {
                        return Json(new { Success = true });
                    }
                }
            }
            return Json(new { Success = false });
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult ShoppingCart(string postUrl)
        {
            var portal = PortalCrmConfigurationManager.CreatePortalContext();
            if (string.IsNullOrWhiteSpace(postUrl) || postUrl == WebsitePathUtility.ToAbsolute(portal.Website, VirtualPathUtility.ToAbsolute("~/")))
                postUrl = XrmHelper.GetSiteMarkerUrl("Upcoming Events");

            ViewBag.BackUrl = postUrl;
            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("Events/Billing/ShoppingCart", "Shopping Cart");

            //Deferral
            ViewBag.Deferral = this.GetContactDeferral();

            //Total Payment
            var currentTotalPayment = this.CalculateShoppingCartTotal();
            ViewBag.TotalPayment = this.EncryptValue(currentTotalPayment.ToString(CultureInfo.InvariantCulture));

            //Calculate deferral
            var user = this.GetContact();
            ViewBag.DeferraPayAll = this.CalculateDeferalForPayment(PaymentUtil.ShoppingCart, user, true);

            return this.RazorView(PaymentUtil.ShoppingCart);
        }

        public ActionResult PayDeferral()
        {
            //Dont allow multiple payments
            if (PaymentUtil.IsUserPayingShoppingCart)
            {
                return RedirectToAction("ShoppingCart", "Payment");
            }

            var shoppingCart = PaymentUtil.ShoppingCart;
            var details = shoppingCart.Details;
            if (details == null || details.Count == 0)
            {
                return RedirectToAction("ShoppingCart", "Payment");
            }

            //Assigns deferral for the payment details
            var totalAmountBeforeTax = (decimal)0;
            var totalTax = (decimal)0;
            var totalDeferral = (decimal)0;
            foreach (var detail in shoppingCart.Details.Values)
            {
                var paymentInfo = detail.PaymentInfo;

                detail.PaymentInfo.Deferral = paymentInfo.AmountDueWithTax;
                totalDeferral += paymentInfo.AmountDueWithTax;
                totalAmountBeforeTax += paymentInfo.AmountDueBeforeTax;
                totalTax += paymentInfo.Tax;
            }

            //Load payment totals
            totalAmountBeforeTax += totalDeferral;
            shoppingCart.SubTotal = totalAmountBeforeTax;
            shoppingCart.TotalTax = totalTax;
            shoppingCart.Due = totalAmountBeforeTax + totalTax;
            shoppingCart.Deferral = -totalDeferral;
            shoppingCart.TaxPending = 0;
            shoppingCart.TotalToPay = 0;

            //Update all billing requests
            try
            {
                var user = this.GetContact();
                BillingService.UpdateDeferralPayment(shoppingCart, user);
                PaymentUtil.Clean();
            }
            catch (Exception ex)
            {
                return RedirectToAction("ShoppingCart", "Payment");
            }

            //ViewBag data
            ViewBag.NewDeferral = this.GetContactDeferral();
            ViewBag.IsDeferralPayment = true;
            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("Events/Billing/DeferralPaymentTitle", "Deferral Payment Completed");
            return this.RazorView("ZeroPayment", shoppingCart);
        }

        public ActionResult PayCheque(string postUrl, bool doReferral = false)
        {
            //Dont allow multiple payments
            if (PaymentUtil.IsUserPayingShoppingCart)
            {
                return RedirectToAction("ShoppingCart", "Payment");
            }

            var portal = PortalCrmConfigurationManager.CreatePortalContext();
            if (postUrl == WebsitePathUtility.ToAbsolute(portal.Website, VirtualPathUtility.ToAbsolute("~/")))
                postUrl = string.Empty;

            ViewBag.BackUrl = postUrl;

            var shoppingCart = PaymentUtil.ShoppingCart;
            var details = shoppingCart.Details;
            if (details == null || details.Count == 0)
            {
                return RedirectToAction("ShoppingCart", "Payment");
            }

            var user = this.GetContact();
            this.CalculateDeferalForPayment(shoppingCart, user, doReferral);

            //Update all the billing request
            try
            {
                BillingService.UpdateChequePending(shoppingCart, user);
                PaymentUtil.ShoppingCartCopy = shoppingCart;
                PaymentUtil.Clean();
            }
            catch (Exception ex)
            {
                return RedirectToAction("ShoppingCart", "Payment");
            }

            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("Events/Billing/ChequePaymentTitle", "Cheque Payment Completed");
            return this.RazorView(shoppingCart);
        }

        public ActionResult PayLater(string postUrl)
        {
            //Dont allow multiple payments
            if (PaymentUtil.IsUserPayingShoppingCart)
            {
                return RedirectToAction("ShoppingCart", "Payment");
            }

            var portal = PortalCrmConfigurationManager.CreatePortalContext();
            if (postUrl == WebsitePathUtility.ToAbsolute(portal.Website, VirtualPathUtility.ToAbsolute("~/")))
                postUrl = string.Empty;

            ViewBag.BackUrl = postUrl;

            var shoppingCart = PaymentUtil.ShoppingCart;
            var details = shoppingCart.Details;
            if (details == null || details.Count == 0)
            {
                return RedirectToAction("ShoppingCart", "Payment");
            }

            var user = this.GetContact();
            this.CalculateDeferalForPayment(shoppingCart, user, false);

            //Update all the billing request
            try
            {
                BillingService.UpdatePayLater(shoppingCart);
                PaymentUtil.ShoppingCartCopy = shoppingCart;
                PaymentUtil.Clean();
            }
            catch (Exception ex)
            {
                return RedirectToAction("ShoppingCart", "Payment");
            }

            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("Events/Billing/PayLaterTitle", "Transaction Completed");
            return this.RazorView(shoppingCart);
        }

        public ActionResult PayLocal(string postUrl)
        {
            //Dont allow multiple payments
            if (PaymentUtil.IsUserPayingShoppingCart)
            {
                return RedirectToAction("ShoppingCart", "Payment");
            }

            var portal = PortalCrmConfigurationManager.CreatePortalContext();
            if (postUrl == WebsitePathUtility.ToAbsolute(portal.Website, VirtualPathUtility.ToAbsolute("~/")))
                postUrl = string.Empty;

            ViewBag.BackUrl = postUrl;

            var shoppingCart = PaymentUtil.ShoppingCart;
            var details = shoppingCart.Details;
            if (details == null || details.Count == 0)
            {
                return RedirectToAction("ShoppingCart", "Payment");
            }

            //Update all the billing request
            var user = this.GetContact();
            this.CalculateDeferalForPayment(shoppingCart, user, false);

            try
            {
                BillingService.UpdatePayLocal(shoppingCart);
                PaymentUtil.ShoppingCartCopy = shoppingCart;
                PaymentUtil.Clean();
            }
            catch (Exception ex)
            {
                return RedirectToAction("ShoppingCart", "Payment");
            }

            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("Events/Billing/PayLaterTitle", "Transaction Completed");
            return this.RazorView(shoppingCart);
        }

        public ActionResult GetReceiptPDF()
        {
            var shoppingCart = PaymentUtil.ShoppingCartCopy;

            var subtotal = (decimal)0;
            var totalTax = (decimal)0;
            var totalDeferral = (decimal)0;
            foreach (var detail in shoppingCart.Details)
            {
                var paymentInfo = detail.Value.PaymentInfo;
                subtotal += paymentInfo.EventCost + paymentInfo.AdditionalFeeForMaterials + paymentInfo.RegistrationFee + paymentInfo.WorkshopsDiscount + paymentInfo.AccommodationCost + paymentInfo.TicketCost;
                totalTax += paymentInfo.EventTax + paymentInfo.AccommodationTax + paymentInfo.TicketTax;
                totalDeferral += paymentInfo.Deferral;
            }

            shoppingCart.SubTotal = subtotal;
            shoppingCart.TotalTax = totalTax;
            shoppingCart.Due = subtotal + totalTax;
            shoppingCart.Deferral = -totalDeferral;
            shoppingCart.TaxPending = totalTax;
            if (totalDeferral > 0)
            {
                var realHst = totalTax / shoppingCart.Due;
                var deferralTax = Math.Round(totalDeferral * realHst);

                shoppingCart.TaxPending = totalTax - deferralTax;
            }
            shoppingCart.TotalToPay = shoppingCart.Due - totalDeferral;

            //Created the pdf file
            var html = this.RenderPartialViewToString("PaymentReceiptPDF", shoppingCart);
            Document document = new Document(PageSize.A4, 25, 25, 25, 25);
            MemoryStream memStream = new MemoryStream();

            PdfWriter writer = PdfWriter.GetInstance(document, memStream);
            writer.CloseStream = false;
            document.Open();
            HTMLWorker htmlWorker = new HTMLWorker(document);
            StringReader sr = new StringReader(html);
            XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, sr);
            document.Close();

            byte[] byteInfo = memStream.ToArray();
            memStream.Write(byteInfo, 0, byteInfo.Length);
            memStream.Position = 0;

            return new FileStreamResult(memStream, "application/pdf");
        }

        public EmptyResult CleanShoppingCart()
        {
            PaymentUtil.Clean();
            PaymentUtil.CleanCopy();

            return new EmptyResult();
        }

        public ActionResult PayNow(string totalPayed, bool doReferral = false)
        {
            //Dont allow multiple payments
            if (PaymentUtil.IsUserPayingShoppingCart)
            {
                return RedirectToAction("ShoppingCart", "Payment");
            }

            var shoppingCart = PaymentUtil.ShoppingCart;
            var details = shoppingCart.Details;
            if (details == null || details.Count == 0)
            {
                return RedirectToAction("ShoppingCart", "Payment");
            }

            // Check if the user has deferral credit to pay all amount
            var user = this.GetContact();
            var zeroPayment = this.CalculateDeferalForPayment(shoppingCart, user, doReferral);

            //Update all the billing request
            BillingService.PaymentPreloadInfo paymentMoneris = null;
            try
            {
                paymentMoneris = BillingService.UpdateCreditCardPending(shoppingCart, user, zeroPayment);
            }
            catch (Exception ex)
            {
                return RedirectToAction("ShoppingCart", "Payment");
            }

            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("Events/Billing/EventPaymentTitle", "Credit Card Payment");

            //User needs to pay amount with credit ard
            if (paymentMoneris != null)
            {
                return this.RazorView(paymentMoneris);
            }

            //Empty shopping cart
            PaymentUtil.Clean();
            return this.RazorView("Payed", shoppingCart);
        }

        // if user chooses Cancel on Moneris payment page it will redirect here passing order_id parameter
        public ActionResult Cancel(string order_id)
        {
            var indexRetryResp = order_id.IndexOf("-");
            if (indexRetryResp > 0)
            {
                order_id = order_id.Substring(0, indexRetryResp);
            }
            Log.Debug("User cancelled payment for orderid " + order_id);

            Entity paymentTransaction;
            var shoppingCart = BillingService.GetPaymentInformation(order_id, out paymentTransaction);

            var user = this.GetContact();
            BillingService.UndoMarkingAsCreditCardPending(shoppingCart, user);

            shoppingCart.Details.Values.ToList().ForEach(p => p.PaymentInfo.Deferral = 0);

            // find out registration id and go back to shopping cart page
            return RedirectToAction("ShoppingCart", "Payment");
        }

        //receives response from moneris
        public ActionResult Processed()
        {
            Log.Debug("Got payment response from Moneris ");
            var response = Request.Form.AllKeys.ToDictionary(param => param, param => Request.Form[param]);

            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("Events/Billing/CreditCardPaymentTitle", "Credit Card Transaction Approved");

            try
            {
                bool isVoid;
                var shoppingCart = BillingService.ProcessMonerisResponse(response, false, out isVoid, this);
                if (shoppingCart != null && shoppingCart.Details.Any())
                {
                    if (!isVoid)
                    {
                        PaymentUtil.ShoppingCartCopy = ObjectCopier.Clone(PaymentUtil.ShoppingCart);

                        PaymentUtil.Clean();

                        //Assign creditcard response values
                        CreditCardInfo creditCard = new CreditCardInfo();

                        creditCard.TransactionID = Request.Form[BillingService.MonerisResponse.ResponseOrderId];
                        creditCard.TransactionType = @Request.Form[BillingService.MonerisResponse.TransName];
                        creditCard.DateandTime = Request.Form[BillingService.MonerisResponse.DateStamp] + " " + @Request.Form[BillingService.MonerisResponse.TimeStamp];
                        creditCard.PaymentAmount = Request.Form[BillingService.MonerisResponse.ChargeTotal];
                        creditCard.AuthorizationCode = Request.Form[BillingService.MonerisResponse.BankApprovalCode];
                        creditCard.ResponseCode = Request.Form[BillingService.MonerisResponse.ResponseCode];
                        creditCard.ISOCode = Request.Form[BillingService.MonerisResponse.IsoCode];
                        creditCard.Message = Request.Form[BillingService.MonerisResponse.Message];
                        creditCard.ReferenceNumber = Request.Form[BillingService.MonerisResponse.BankTransactionId];
                        creditCard.NameOnTheCard = Request.Form[BillingService.MonerisResponse.Cardholder];

                        var expireDate = Request.Form[BillingService.MonerisResponse.ExpiryDate];
                        var month = expireDate.Substring(2, 2);
                        var year = expireDate.Substring(0, 2);

                        creditCard.ExpiryDate = month + year;

                        if (shoppingCart.IsAsyncRequest)
                        {
                            creditCard.CreditCardNumber = Request.Form[BillingService.MonerisResponse.CardNumberAsync];
                        }
                        else
                        {
                            creditCard.CreditCardNumber = Request.Form[BillingService.MonerisResponse.CardNumber];
                        }

                        PaymentUtil.ShoppingCartCopy.CreditCard = creditCard;
                        return this.RazorView("Approved", shoppingCart);
                    }

                    PaymentUtil.ShoppingCart = shoppingCart;
                    PaymentUtil.IsUserPayingShoppingCart = false;
                    ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("Events/Billing/CreditCardPaymentFailedTitle", "Credit Card Transaction Failed");

                    return this.RazorView("Failed");
                }

                //Get payment information to retry
                Entity paymentTransaction;
                var orderId = response[BillingService.MonerisResponse.ResponseOrderId]; ;
                var indexRetryResp = orderId.IndexOf("-");
                if (indexRetryResp > 0)
                {
                    orderId = orderId.Substring(0, indexRetryResp);
                }

                Log.Debug("Declined order id: " + orderId);
                shoppingCart = !String.IsNullOrEmpty(orderId) ? BillingService.GetPaymentInformation(orderId, out paymentTransaction) : null;
                if (shoppingCart != null && shoppingCart.Details.Any())
                {
                    PaymentUtil.ShoppingCart = shoppingCart;
                    PaymentUtil.IsUserPayingShoppingCart = false;

                    var user = this.GetContact();
                    BillingService.UndoMarkingAsCreditCardPending(shoppingCart, user);
                }
            }
            catch (Exception ex)
            {
                PaymentUtil.Clean();
            }

            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("Events/Billing/CreditCardPaymentTitle", "Credit Card Transaction Declined");
            return this.RazorView("Declined");
        }

        [AllowAnonymous]
        [HttpPost]
        public void AsyncResponse()
        {
            Log.Debug("Got async response sleep");
            Thread.Sleep(30000);
            Log.Debug("Got async response start");

            HttpRequestBase request = this.ControllerContext.HttpContext.Request;
            string xml_response = request.Unvalidated.Form.Get("xml_response");
            var position = xml_response.IndexOf("?>");
            xml_response = xml_response.Substring(position + 2);

            var xDoc = XDocument.Parse(xml_response);
            var response = xDoc.Element("response");
            var verifiedResponse = response.Descendants().ToDictionary(element => element.Name.LocalName, element => element.Value);
            //Log.Debug("Async response fields: " + BillingService.ToLogString(verifiedResponse));

            bool isVoid;
            BillingService.ProcessMonerisResponse(verifiedResponse, true, out isVoid, this);
            //return new EmptyResult();
        }

        public ActionResult ProcessedTest()
        {
            //Create a test model
            var shoppingCart = new ShoppingCart()
            {
                Details = new Dictionary<Guid, ShoppingCartDetail>()
                {
                    {
                        Guid.NewGuid(),
                        new ShoppingCartDetail()
                        {
                            PaymentInfo = new BillingService.PaymentInfo
                            {
                                BillingRequestId = Guid.NewGuid(),
                                IsChequeAllowed = true,
                                PayLater = true,
                                PayLocal = true,
                                ChequeDueDate = new DateTime(2014, 12, 1),
                                AccommodationDays = 2,
                                AccommodationCost = 4.00m,
                                AdditionalTickets = 3,
                                TicketCost = 1.50m,
                                EventCost = 2.00m,
                                AmountDueWithTax = 9.00m,
                                EventName = "Event 1"
                            },
                            Event = new EventShopingCart()
                            {
                                EventName = "Event 1",
                                PayLater = false,
                                StartDate = DateTime.Now,
                                EndDate = DateTime.Now.AddDays(3)
                            }
                        }
                    },
                    {
                        Guid.NewGuid(),
                        new ShoppingCartDetail()
                        {
                            PaymentInfo = new BillingService.PaymentInfo
                            {
                                BillingRequestId = Guid.NewGuid(),
                                IsChequeAllowed = true,
                                PayLater = true,
                                PayLocal = true,
                                ChequeDueDate = new DateTime(2014, 12, 1),
                                AccommodationDays = 2,
                                AccommodationCost = 4.00m,
                                AdditionalTickets = 3,
                                AdditionalFeeForMaterials = 10,
                                TicketCost = 1.50m,
                                EventCost = 2.00m,
                                AmountDueWithTax = 9.00m,
                                EventName = "Event 2"
                            },
                            Event = new EventShopingCart()
                            {
                                EventName = "Event 2",
                                StartDate = DateTime.Now,
                                EndDate = DateTime.Now.AddDays(3),
                                PayLater = true,
                            }
                        }
                    }
                },
                SubTotal = 240,
                TotalTax = 31.2m,
                Deferral = -230,
                Due = 41.2m,
                TaxPending = 5.35m,
                TotalToPay = 46.55m
            };

            //Created the pdf file
            var html = this.RenderPartialViewToString("PaymentReceipt", shoppingCart);
            Document document = new Document(PageSize.A4, 25, 25, 25, 25);
            MemoryStream memStream = new MemoryStream();

            string filePath = @"c:\Temp\receipt1.pdf";
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.Create));
            document.Open();
            HTMLWorker htmlWorker = new HTMLWorker(document);
            //htmlWorker.Parse(new StringReader(html));
            StringReader sr = new StringReader(html);
            XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, sr);
            document.Close();

            //Render the view
            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("Events/Billing/CreditCardPaymentTitle", "Credit Card Transaction Approved");

            return this.RazorView(shoppingCart);
        }

        [HttpPost]
        public JsonResult DeleteEvent(Guid id)
        {
            PaymentUtil.DeleteShoppingCartDetail(id);
            return Json(new { Success = true });
        }

        [HttpPost]
        public JsonResult ValidatePaymentAmmount(string vt)
        {
            if (!this.IsValidShoppingCartTotal(vt))
            {
                return Json(new { Success = false });
            }

            var shoppingCartEvents = PaymentUtil.ShoppingCart.Details.Values.ToList();
            var coursesOverlapping = new List<object>();
            var pendingPayments = new List<object>();
            foreach (var det in shoppingCartEvents)
            {
                var evt = det.Event;

                //Courses validation
                if (evt.IsCourse)
                {
                    var eventRegistrationId = evt.EventRegistrationId.Value;
                    var termId = evt.CourseTermId.Value;
                    var startDate = evt.StartDate.Value;
                    var endDate = evt.EndDate.Value;

                    var overlappingRegistrationCourses = this.OverlapingRegistrationCourses(evt.Id, eventRegistrationId, termId, startDate, endDate);
                    if (overlappingRegistrationCourses.Count > 0)
                    {
                        var courseOverlappingList = new
                        {
                            CourseTitle = evt.EventName,
                            OverlappingSchedules = overlappingRegistrationCourses
                        };
                        coursesOverlapping.Add(courseOverlappingList);
                    }
                }

                // Event Status Validation
                var currentPaymentInfo = BillingService.GetPaymentInformation(det.PaymentInfo.RegistrationId);
                if (currentPaymentInfo.PaymentStatus != BillingService.PaymentStatus.Unpaid && currentPaymentInfo.PaymentStatus != BillingService.PaymentStatus.Paid)
                {
                    pendingPayments.Add(evt);
                }
            }

            if (coursesOverlapping.Count > 0)
            {
                var model = new { Success = false, RegistrationOverlapping = true, OverlappingCourses = coursesOverlapping };
                return new JsonNetResult() { Data = model };
            }

            if (pendingPayments.Count > 0)
            {
                var model = new { Success = false, InvalidPayments = true, PendingPayments = pendingPayments };
                return new JsonNetResult() { Data = model };
            }

            return Json(new { Success = true });
        }

        private string EncryptValue(string val)
        {
            //create new instance of md5
            SHA1 sha1 = SHA1.Create();

            //convert the input text to array of bytes
            byte[] hashData = sha1.ComputeHash(Encoding.Default.GetBytes(val));

            //create new instance of StringBuilder to save hashed data
            StringBuilder totalHash = new StringBuilder();

            //loop for each byte and add it to StringBuilder
            for (int i = 0; i < hashData.Length; i++)
            {
                totalHash.Append(hashData[i].ToString());
            }

            return totalHash.ToString();
        }

        /// <summary>
        /// Recalculate deferral and taxes for the payment
        /// </summary>
        /// <returns>Returns true if the total amount to pay was covered by the deferral credit, meaning that the user does not need to make a payment</returns>
        private bool CalculateDeferalForPayment(ShoppingCart shoppingCart, Contact user, bool doReferral)
        {
            //Deferral?
            var deferral = doReferral ? this.GetContactDeferral(user) : 0;
            var isDeferral = doReferral && deferral > 0;

            //Totals
            var totalAmountBeforeTax = (decimal)0;
            var totalTax = (decimal)0;
            var totalDeferral = (decimal)0;
            foreach (var detail in shoppingCart.Details.Values)
            {
                var paymentInfo = detail.PaymentInfo;
                var maxDeferralAmountApply = paymentInfo.AmountDueWithTax - paymentInfo.AccommodationCost - paymentInfo.TicketCost - paymentInfo.TicketTax - paymentInfo.AccommodationTax;

                //Shopping cart totals
                totalAmountBeforeTax += paymentInfo.AmountDueBeforeTax;
                totalTax += paymentInfo.Tax;

                //Deferral
                if (isDeferral)
                {
                    //Gets deferral amount
                    if (deferral >= maxDeferralAmountApply)
                    {
                        paymentInfo.Deferral = maxDeferralAmountApply;
                        deferral -= maxDeferralAmountApply;
                    }
                    else
                    {
                        paymentInfo.Deferral = deferral;
                        deferral = 0;
                    }

                    paymentInfo.AmountDueWithTax -= paymentInfo.Deferral;
                    totalDeferral += paymentInfo.Deferral;
                }

                //Get event for displaying purposes
                if (detail.Event == null)
                {
                    var evt = RegistrationHelper.GetEventFromRegistration(paymentInfo.RegistrationId);
                    detail.Event = new EventShopingCart()
                    {
                        Id = evt.Id,
                        EventRegistrationId = paymentInfo.RegistrationId,
                        EventName = evt.oems_EventName,
                        IsCourse = evt.GetAttributeValue<bool>("oems_courseflag"),
                        CourseCode = evt.GetAttributeValue<string>("oems_coursecode"),
                        CourseTermId = evt.GetAttributeValue<EntityReference>("oems_courseterm").Id
                    };
                }
            }

            //Final shopping cart totals
            shoppingCart.SubTotal = totalAmountBeforeTax;
            shoppingCart.TotalTax = totalTax;
            shoppingCart.Due = totalAmountBeforeTax + totalTax;
            shoppingCart.Deferral = -totalDeferral;
            shoppingCart.TaxPending = totalTax;
            if (totalDeferral > 0)
            {
                var realHst = totalTax / shoppingCart.Due;
                var deferralTax = Math.Round(totalDeferral * realHst);

                shoppingCart.TaxPending = totalTax - deferralTax > 0 ? totalTax - deferralTax : 0;
            }

            shoppingCart.TotalToPay = shoppingCart.Due - totalDeferral;

            return shoppingCart.TotalToPay == 0;
        }

        private decimal CalculateShoppingCartTotal()
        {
            var shoppingCart = PaymentUtil.ShoppingCart;
            var total = (decimal)0;

            foreach (var detail in shoppingCart.Details.Values)
                total += detail.PaymentInfo.AmountDueWithTax;

            return total;
        }

        private bool IsValidShoppingCartTotal(string hashTotalPayed)
        {
            var currentTotalPayment = this.CalculateShoppingCartTotal();
            var haschCurrent = this.EncryptValue(currentTotalPayment.ToString(CultureInfo.InvariantCulture));

            return hashTotalPayed == haschCurrent;
        }

        private decimal CalculateTax(decimal amount)
        {
            var hstRate = GetHstRate();
            return Math.Round(amount * hstRate, 2);
        }

        public static bool IsEventCostApplyHst(Guid eventPriceId)
        {
            var context = XrmHelper.GetContext();
            var entity = context.Retrieve("oems_eventpricing", eventPriceId, new ColumnSet("oems_applyhst"));

            if (entity == null)
                return true;

            var applyHst = entity.GetAttributeValue<bool>("oems_applyhst");
            return applyHst;
        }

        private decimal GetHstRate()
        {
            const decimal defaultHstRate = 0.13m;
            var context = XrmHelper.GetContext();
            var configuration =
            (
                from c in context.CreateQuery("oems_configuration")
                select c
            ).FirstOrDefault();

            if (configuration == null)
            {
                return defaultHstRate;
            }

            var hstRate = configuration.GetAttributeValue<decimal?>("oems_eventpricinghstrate");
            return hstRate / (decimal)100.0 ?? defaultHstRate;
        }

        private Xrm.Contact GetContact()
        {
            var ctx = XrmHelper.GetContext();
            var portal = PortalCrmConfigurationManager.CreatePortalContext();
            var contact = ctx.Retrieve(Xrm.Contact.EntityLogicalName, (portal.User as Contact).Id, new ColumnSet(true)).ToEntity<Xrm.Contact>();

            return contact;
        }

        private decimal GetContactDeferral(Entity contact = null)
        {
            if (contact == null)
            {
                contact = GetContact();
            }

            var deferral = contact.GetAttributeValue<decimal>("oems_deferralcreditamount");

            return deferral;
        }

        private bool IsOverlapingPeriods(DateTime newStartDate, DateTime newEndDate, DateTime overlapStartDate, DateTime overlapEndDate)
        {
            return newStartDate <= overlapEndDate && overlapStartDate <= newEndDate;
        }

        private List<EventListing> OverlapingRegistrationCourses(Guid eventId, Guid evtRegistrationID, Guid courseTermId, DateTime startDate, DateTime endDate)
        {
            var context = XrmHelper.GetContext();
            var contact = this.GetContact();
            List<EventListing> overlapingCourses = new List<EventListing>();

            var courses =
            (
                from reg in context.CreateQuery("oems_eventregistration")
                join evt in context.CreateQuery("oems_event") on ((EntityReference)reg["oems_event"]).Id equals evt.GetAttributeValue<Guid>("oems_eventid")
                join ds in context.CreateQuery("oems_coursedateslots") on ((EntityReference)evt["oems_coursedateslot"]).Id equals ds["oems_coursedateslotsid"]
                where reg.GetAttributeValue<EntityReference>("oems_contact").Id == contact.Id
                where reg.GetAttributeValue<Guid>("oems_eventregistrationid") != evtRegistrationID
                where reg.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventRegistration.Approved || reg.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventRegistration.Sumbitted
                where evt.GetAttributeValue<bool>("oems_courseflag")
                where evt.GetAttributeValue<EntityReference>("oems_courseterm").Id == courseTermId
                where evt.GetAttributeValue<Guid>("oems_eventid") != eventId
                select new
                {
                    EventTitle = evt.GetAttributeValue<string>("oems_eventname"),
                    StartDate = ds.GetAttributeValue<DateTime>("oems_startdate"),
                    EndDate = ds.GetAttributeValue<DateTime>("oems_enddate")
                }
            ).ToList();

            foreach (var courseReg in courses)
            {
                var overlapDate = this.IsOverlapingPeriods(startDate, endDate, courseReg.StartDate, courseReg.EndDate);
                if (overlapDate)
                {
                    overlapingCourses.Add(new EventListing()
                    {
                        EventTitle = courseReg.EventTitle,
                        StartDate = courseReg.StartDate,
                        EndDate = courseReg.EndDate
                    });
                }
            }

            return overlapingCourses;
        }

        private List<EventListing> OverlapingShoppingCartCourses(Guid courseTermId, DateTime startDate, DateTime endDate)
        {
            List<EventListing> overlapingCourses = new List<EventListing>();

            var shoppingCartEvents = PaymentUtil.ShoppingCart.Details.Values.ToList();
            foreach (var det in shoppingCartEvents)
            {
                var evt = det.Event;
                if (evt.IsCourse && evt.Id != det.Event.Id)
                {
                    var overlapDate = this.IsOverlapingPeriods(startDate, endDate, evt.StartDate.Value, evt.EndDate.Value);
                    if (overlapDate)
                    {
                        overlapingCourses.Add(new EventListing()
                        {
                            EventTitle = evt.EventName,
                            StartDate = evt.StartDate,
                            EndDate = evt.EndDate
                        });
                    }
                }
            }

            return overlapingCourses;
        }
    }
}