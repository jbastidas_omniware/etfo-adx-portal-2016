﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Adxstudio.Xrm.Web.Mvc;
using Microsoft.Xrm.Portal.Configuration;
using Site.Library;
using Site.Helpers;
using Xrm;

namespace Site.Areas.DocumentLocker.Controllers
{
    [PortalView]
    [Authorize]
    public class DocumentLockerController : Controller
    {
        private Contact Contact
        {
            get { return PortalCrmConfigurationManager.CreatePortalContext().User as Contact; }
        }

        //[ChildActionOnly]
        public ActionResult Index()
        {
            var annotations = AnnotationsHelper.GetAnnotations(Contact.Id, true); 
            CurrentUser.ProfileDocumentsCount = annotations.Count;
            CurrentUser.ProfileDocumentsSize = annotations.Sum(a => a.FileSize ?? 0);

            ViewBag.ProfileDocumentsCount = CurrentUser.ProfileDocumentsCount;
            ViewBag.ProfileDocumentsSize = CurrentUser.ProfileDocumentsSize;

            return View(annotations);
        }

        [HttpPost]
        public JsonResult UploadDocument(string description)
        {
            const int mbbytes = 1024000;
            var totalSize = Convert.ToInt32(ConfigurationManager.AppSettings["Document.Locker.Max.Files.Size"]);
            var totalCount = Convert.ToInt32(ConfigurationManager.AppSettings["Document.Locker.Max.Files.Count"]);

            if (CurrentUser.ProfileDocumentsSize == null || CurrentUser.ProfileDocumentsCount == null)
            {
                return Json(new { Success = false, Refresh = true });
            }

            if (Request.Files["file"] != null)
            {
                var file = Request.Files["file"];

                //Validate size
                var maxSize = Convert.ToInt32(ConfigurationManager.AppSettings["Document.Locker.Max.File.Size"]);
                var mb = 0;

                if (file.ContentLength > 0 && file.ContentLength <= maxSize)
                {
                    if (CurrentUser.ProfileDocumentsCount.Value + 1 > totalCount || CurrentUser.ProfileDocumentsSize.Value + file.ContentLength > totalSize)
                    {
                        mb = totalSize / mbbytes;
                        return Json(new { Success = false, Message = String.Format("It is not possible to upload the document. You can upload maximum {0} documents and all of them must have a maximum size of {1}MB", totalCount, mb) });
                    }

                    var extension = Path.GetExtension(file.FileName);
                    var mimetype = MimeTypeHelper.GetMimeTypeFromExtension(extension);
                    var output = new byte[file.InputStream.Length];
                    file.InputStream.Read(output, 0, output.Length);

                    var annotation = new Annotation()
                    {
                        FileName = file.FileName,
                        //FileSize = file.ContentLength,
                        IsDocument = true,
                        DocumentBody = Convert.ToBase64String(output),
                        MimeType = mimetype,
                        Subject = description
                    };
                    
                    AnnotationsHelper.CreateAnnotation(annotation, Contact);
                    var cannotation = new CustomAnnotation()
                    {
                        Id = annotation.Id,
                        AnnotationId = annotation.AnnotationId,
                        FileName = annotation.FileName,
                        FileSize = file.ContentLength,
                        IsDocument = annotation.IsDocument,
                        //DocumentBody = annotation.DocumentBody,
                        MimeType = annotation.MimeType,
                        Subject = annotation.Subject
                    };

                    CurrentUser.ProfileDocumentsCount++;
                    CurrentUser.ProfileDocumentsSize += file.ContentLength;

                    return Json(new { Success = true, Document = cannotation });
                }

                mb = maxSize / mbbytes;
                return Json(new { Success = false, Message = String.Format("Document should be {0}MB maximum, please pick up a different document from your computer and try again", mb) });
            }

            return Json(new {Success = false, NoFileSent = true, Message = "No file was uploaded, please pick up a file from your computer and try again"});
        }

        public JsonResult DeleteDocument(Guid id)
        {
            if (CurrentUser.ProfileDocumentsSize == null || CurrentUser.ProfileDocumentsCount == null)
            {
                return Json(new { Success = false, Refresh = true });
            }

            var size = AnnotationsHelper.GetAnnotationFileSize(id);
            AnnotationsHelper.DeleteAnnotation(id);

            CurrentUser.ProfileDocumentsCount--;
            CurrentUser.ProfileDocumentsSize -= size;

            return Json(new { Success = true });
        }

        [HttpPost, FileDownload]
        public FileResult DownloadDocument(Guid id)
        {
            var fileName = "";
            var mimetype = "";
            var document = AnnotationsHelper.GetAnnotationDocumentBody(id, out fileName, out mimetype);

            if (document == null || document.Length == 0)
            {
                throw new Exception("There is a problem downloading the file, it may not exists anymore");
            }

            return File(document, mimetype, fileName);
        }
    }
}