﻿using System.Web.Http;
using System.Web.Mvc;
using Adxstudio.Xrm.Web.Mvc;

namespace Site.Areas.DocumentLocker
{
	public class DocumentLockerAreaRegistration : AreaRegistration
	{
		public override string AreaName
		{
            get { return "DocumentLocker"; }
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapSiteMarkerRoute(
				"DocumentLocker",
				"DocumentLocker",
				"{action}",
                new { controller = "DocumentLocker", action = "Index" }, new { action = @"^DocumentLocker.*" });

            context.MapRoute("DocumentLockerDefault", "DocumentLocker/{action}/{id}", new { controller = "DocumentLocker", action = "Index", id = RouteParameter.Optional, area = "DocumentLocker" });
		}
	}
}
