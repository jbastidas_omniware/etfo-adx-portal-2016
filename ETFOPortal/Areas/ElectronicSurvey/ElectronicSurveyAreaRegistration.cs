﻿using System.Web.Http;
using System.Web.Mvc;
using Adxstudio.Xrm.Web.Mvc;

namespace Site.Areas.ElectronicSurvey
{
	public class ElectronicSurveyAreaRegistration : AreaRegistration
	{
		public override string AreaName
		{
            get { return "ElectronicSurvey"; }
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapSiteMarkerRoute(
				"ElectronicSurvey",
				"ElectronicSurvey",
				"{action}",
                new { controller = "ElectronicSurvey", action = "Index" }, new { action = @"^ElectronicSurvey.*" });

            context.MapRoute("ElectronicSurveyDefault", "ElectronicSurvey/{action}/{id}", new { controller = "ElectronicSurvey", action = "Index", id = RouteParameter.Optional, area = "ElectronicSurvey" });
		}
	}
}
