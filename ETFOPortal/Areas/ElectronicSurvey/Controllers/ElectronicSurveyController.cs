﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Adxstudio.Xrm.Web.Mvc;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Configuration;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Sdk;
using Site.Helpers;
using Xrm;
using log4net;

namespace Site.Areas.ElectronicSurvey.Controllers
{
    [PortalView]
    [Authorize]
    public class ElectronicSurveyController : Controller
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (ElectronicSurveyController));

        public static class SurveyStatus
        {
            public const int New = 347780000;
            public const int Saved = 347780001;
            public const int Submitted = 347780002;
        }

        private static CrmOrganizationServiceContext Context
        {
            get { return XrmHelper.GetContext(); }
        }

        private Guid CurrentSurveyId
        {
            set { Session["electrosurv::surveyId"] = value; }
            get { return (Guid?)Session["electrosurv::surveyId"] ?? CurrentYearSurvey().Id; }
        }

        private bool SurveySubmitted
        {
            set { Session["electrosurv::surveySubmitted"] = value; }
            get { return (bool?)Session["electrosurv::surveySubmitted"] ?? false; }
        }

        private Contact Contact
        {
            get { return PortalCrmConfigurationManager.CreatePortalContext().User as Contact; }
        }

        private Xrm.Account Account
        {
            get
            {
                var account = (from a in Context.CreateQuery<Xrm.Account>() where a.PrimaryContactId.Id == Contact.Id select a).FirstOrDefault();
                return account;
            }
        }

        private string GetPreferredSchoolBoardLocal()
        {
            // Clean (Contact.ParentCustomerId.Id => Contact.Id)
            var preferredSchoolBoardLocal = (from sb in XrmHelper.GetContext().CreateQuery("oems_accountschoolboardlocal")
                                             where sb.GetAttributeValue<EntityReference>("oems_account").Id == Contact.Id && sb.GetAttributeValue<bool>("oems_activeflag")
                                             select new
                                             {
                                                 Id = sb.GetAttributeValue<Guid>("oems_accountschoolboardlocalid"),
                                                 Name = sb.GetAttributeValue<string>("oems_name"),
                                                 Selected = sb.GetAttributeValue<bool>("oems_defaultflag")
                                             }).FirstOrDefault();

            return preferredSchoolBoardLocal != null ? preferredSchoolBoardLocal.Name : string.Empty;
        }

        private bool GetSelfIdentified()
        {
            return Account.oems_AboriginalFlag.GetValueOrDefault()
                   || Account.oems_DisabledFlag.GetValueOrDefault()
                   || Account.oems_LesbianGayBisexualTransgenderFlag.GetValueOrDefault()
                   || Account.oems_RacializedGroupFlag.GetValueOrDefault();
        }

        private void CopyFields(Entity fromSurvey, Entity toSurvey)
        {
            var fromPrefix = "oems_";
            var toPrefix = "oems_";
            var fromYear = fromSurvey.GetAttributeValue<string>("oems_year");
            var toYear = toSurvey.GetAttributeValue<string>("oems_year");

            Log.Debug("(" + Account.oems_ETFOMemberId + "): Copying fields from " + fromYear + " to " + toYear);
            foreach (string field in Fields.AllFormFields)
            {
                if (fromSurvey.Contains(fromPrefix + field))
                {
                    Log.DebugFormat("   copying " + field);
                    toSurvey[toPrefix + field] = fromSurvey[fromPrefix + field];
                }
            }
        }

        private void PrepopulateAnswers(Entity newSurvey)
        {
            Log.Debug("(" + Account.oems_ETFOMemberId + "): Trying to retrive previous year survey");
  
            Entity survey;
            using (var context = Context)
            {
                survey = Context.CreateQuery("oems_survey").Where(surv => ((EntityReference)surv["oems_account"]).Id == Account.Id)
                    .OrderByDescending(surv => surv.GetAttributeValue<string>("oems_year"))
                    .FirstOrDefault();

                if (survey == null)
                {
                    Log.Debug("(" + Account.oems_ETFOMemberId + "): Trying to retrive legacy survey"); //2013 survey
                    survey = Context.CreateQuery("oems_survey").FirstOrDefault(surv => surv.GetAttributeValue<string>("oems_etfoid") == Account.oems_ETFOMemberId);
                }

                if (survey != null)
                {
                    Log.DebugFormat("(" + Account.oems_ETFOMemberId + "): Found survey for {0}", survey.GetAttributeValue<string>("oems_year"));
                    LogSurvey(survey, " FOUND");
                    CopyFields(survey,  newSurvey);
                    context.Detach(survey); // allow saving with plain service
                }
            }
        }

        private Entity CurrentYearSurvey()
        {
            string currentYear = DateTime.Now.Year.ToString();
            Log.DebugFormat("(" + Account.oems_ETFOMemberId + "): Retrieving survey for year {0}", currentYear);
            
            Entity survey;
            using (var context = (XrmServiceContext) CrmConfigurationManager.CreateContext("XrmNoCache"))
            {
                survey = Context.CreateQuery("oems_survey")
                            .Where(surv => ((EntityReference)surv["oems_account"]).Id == Account.Id && surv.GetAttributeValue<string>("oems_year") == currentYear)
                            .OrderByDescending(surv=> surv.GetAttributeValue<string>("oems_year"))
                            .FirstOrDefault();

                if (survey != null)
                {
                    context.Detach(survey); // allow saving with plain service
                }
            }

            if (survey == null)
            {
                Log.Info("(" + Account.oems_ETFOMemberId + "): Creating NEW survey");
                
                survey = new Entity("oems_survey");
                survey["oems_year"] = currentYear;
                survey["oems_account"] = new EntityReference(Account.LogicalName, Account.Id);
                survey["oems_name"] = currentYear + " " + Account.oems_ETFOMemberId;
                survey["oems_surveystatus"] = new OptionSetValue(SurveyStatus.New);

                PrepopulateAnswers(survey);

                // saving will predefine binary fields.. at the best it could be postponed till after first save
                survey.Id = Context.Create(survey); //does not happen automatically
                Log.Debug("(" + Account.oems_ETFOMemberId + "): Survey created id " + survey.Id + " name " + survey["oems_name"]);
            }
            else
            {
                Log.DebugFormat("(" + Account.oems_ETFOMemberId + "): Found exising survey id " + survey.Id + " name " + survey["oems_name"]);
            }

            LogSurvey(survey, " RETRIEVING ");
            return survey;
        }

        // update survey entity with request parameters
        private Entity UpdatedSurvey()
        {
            //update using new entity 
            Entity survey = new Entity("oems_survey");
            survey.Id = CurrentSurveyId;

            Log.Debug("(" + Account.oems_ETFOMemberId + "): Update Survey " + CurrentSurveyId.ToString());
            foreach (string param in Fields.AllFormFields)
            {
                //Log.Debug("request " + param + "=> '" + Request[param] + "'");
                if (!String.IsNullOrEmpty(Request[param]))
                {
                    if (Fields.BoundAsTwoValues.Contains(param))
                    {
                        survey["oems_" + param] = Boolean.Parse(Request[param] ?? "false");
                    }
                    else if (Fields.BoundAsOptionSets.Contains(param))
                    {
                        survey["oems_" + param] = new OptionSetValue(int.Parse(Request[param]));
                    }
                    else if (Fields.DecimalInputs.Contains(param))
                    {
                        survey["oems_" + param] = Decimal.Parse((string) Request[param]);
                    }
                    else if (Fields.DateInputs.Contains(param))
                    {
                        survey["oems_" + param] = DateTime.ParseExact((string)Request[param], "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    }
                    else if (Fields.TextInputs.Contains(param))
                    {
                        survey["oems_" + param] = (string)Request[param];
                    }
                    else
                    {
                        Log.Debug(" Ignored " + param);
                    }
                }

            }

            //survey.EntityState = EntityState.Changed; //http://blog.simpletrees.com/2014/06/entitystate-must-be-set-to-null-created.html
            return survey;
        }

        private void LogSurvey(Entity survey, string message = "")
        {
            Log.Debug("(" + Account.oems_ETFOMemberId + "):" + message + " Survey contents: \n");

            var values = survey.FormattedValues.Count() > 0 ?
                            survey.FormattedValues
                                .Where(kv => kv.Key.StartsWith("oems"))
                                .Select(kv => "\t" + kv.Key + "=>" + kv.Value).ToList()
                         :
                            survey.Attributes
                                .Where(kv => kv.Key.StartsWith("oems"))
                                .Select(kv => "\t" + kv.Key + "=>" + kv.Value).ToList();

            Log.Debug(string.Join(", ", values));
        }

        ///  controller methods ======================
        public ActionResult Index()
        {
            ViewBag.Account = Account;
            Entity currentYearSurvey = CurrentYearSurvey();
            ViewBag.Survey = currentYearSurvey;
            CurrentSurveyId = currentYearSurvey.Id;
            SurveySubmitted = currentYearSurvey.GetAttributeValue<OptionSetValue>("oems_surveystatus").Value ==
                              SurveyStatus.Submitted;

            ViewBag.preferredSchoolBoardLocal = GetPreferredSchoolBoardLocal();
            ViewBag.selfIdentified = GetSelfIdentified();

            return View(Account);
        }

        public JsonResult Save()
        {
            var survey = UpdatedSurvey();
            if (SurveySubmitted)
            {
                Log.Warn("(" + Account.oems_ETFOMemberId + "): This survey has already been submitted.");
            }
            else
            {
                survey["oems_surveystatus"] = new OptionSetValue(SurveyStatus.Saved);
            }

            if (survey.Id != Guid.Empty)
            {
                LogSurvey(survey, " SAVING existing");
                Context.Update(survey);
            }
            else
            {
                LogSurvey(survey, " SAVING new");
                Context.Create(survey);
            }

            Log.Debug("(" + Account.oems_ETFOMemberId + "): Survey SAVED");
            return Json(new {message = "Saved"});
        }

        public JsonResult Submit()
        {
            var survey = UpdatedSurvey();
            survey["oems_surveystatus"] = new OptionSetValue(SurveyStatus.Submitted);

            if (survey.Id != Guid.Empty)
            {
                LogSurvey(survey, " SUBMITTING existing");
                Context.Update(survey);
            }
            else
            {
                LogSurvey(survey, " SUBMITTING new");
                Context.Create(survey);
            }

            SurveySubmitted = true;
            Log.Debug("(" + Account.oems_ETFOMemberId + "): Survey SUBMITTED");
            return Json(new {message = "Submitted"});
        }

        public JsonResult Ping()
        {
            return Json(new { message = "ok" });
        }
    }
}