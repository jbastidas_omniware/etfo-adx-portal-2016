﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Areas.ElectronicSurvey.Controllers
{
    public static class Fields
    {

        // different fields are bound in different ways
        public static readonly string[] BoundAsTwoValues = new[]
            {
                "positionteacher",
                "positionesp",
                "positionpsp", // may be misspelled in old system
                "positionotnotlto",
                "positionotlto",
                "positiondece",

                "librarian", 
                "guidance", 
                "nslteacher", 
                "eslteacher", 
                "corefrenchteacher", 
                "frenchimmersion", 
                "specialed", 
                "consultantcoordinator", 
                "specialassignmentteacher", 
                "otherteacher",
                "iamna", 

                "rotarymath", 
                "rotaryscience", 
                "rotaryhistorygeography", 
                "rotaryfamilystudies", 
                "rotarymusic", 
                "rotaryvisualarts", 
                "rotarydesignandtech", 
                "rotaryphysedhealth", 
                "rotarydrama", 
                "rotaryenglish", 
                "rotaryfrench", 
                "rotaryotherlanguage", 
                "rotarycomputertechnology", 
                "rotaryna",

                "degreesteachingcertificate",
                "degreesbabsc",
                "degreesbed",
                "degreesmastersdegree",
                "degreesdoctorate",
                "degreesother",

                 "specialtyna" ,
                 "specialtyprimary", 
                 "specialtyjunior" ,
                 "specialtyintermediate", 
                 "specialtyspecialed" ,
                 "specialtycoreextimrnfr", 
                 "specialtyesl" ,
                 "specialtymusic", 
                 "specialtyreading", 
                 "specialtylibrary" ,
                 "specialtyother" ,



                "addqualece",
                "addqualecd", 
                "addqualaqcreditbyoct",
                "addqualna",

                "principalpartone",
                "principalparttwo",
                "principalneither",

                // missed
                "degreesna",

                //extra
                "selfidaboriginal",
                "selfiddisabled",
                "selfidgaylesbianbisexualtransgender",
                "selfidracialminority",
                "selfidwoman", // not present in legacy system
               
                "communityactive",
                "communityfundraising",
                "communityvolunteer",
                "communitycoaching",
                "communitypolitical",
                "communityother"
            };

        public static readonly string[] BoundAsOptionSets = new[]
            {
                "assignment",
                "teachsinglegrade",
                "teachcombinedgrade",
                "teachtriplecombined",
                "specialtycertificates",
                "qecoevaluation",
                "childdependants",
                "numberofdependentchildrenunder12",
                "firstlanguage",
                "onleavethisyear",
                // repl with cd"activeinthecommunity",

                //Yes no options as optionsets
                "supervisoryofficerqualifications",
                "collegediplomacert",
                "gender",
                "adultdependant",


                //extra
                "selfidentify",
                "receivevoice",
                "receiveothercommunication"

            };

        public static readonly string[] DecimalInputs = new string[] { "yearsofexperience" };
        public static readonly string[] DateInputs = new string[] {  "dateofbirth"  /*extra */};
        public static readonly string[] TextInputs = new string[] { "otherfirstlanguage"  /*extra */};
        public static readonly string[] OtherFields = new string[] {
                                                                     "etfoid" //legacy id 
                                                                    };  


        // all fields carried over from year to year
        public static readonly string[] AllFormFields = BoundAsOptionSets.Concat(BoundAsTwoValues)
                                                                        .Concat(DecimalInputs)
                                                                        .Concat(DateInputs)
                                                                        .Concat(TextInputs)
                                                                        .ToArray();

        public static readonly string[] AllFields = AllFormFields.Concat(OtherFields).ToArray();

        public static readonly string[] ImportedFields = BoundAsTwoValues.Concat(OtherFields).ToArray();

    }
}