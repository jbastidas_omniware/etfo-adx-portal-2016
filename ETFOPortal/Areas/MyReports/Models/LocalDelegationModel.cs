﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Areas.MyReports.Models
{
    /// <summary>
    /// Local Delegation reports filter
    /// </summary>
    public class LocalDelegationReportsFilters : BasicFilter
    {
        public int? LocalDelegationStatus;
    }

    /// <summary>
    /// Release Time Request report
    /// </summary>
    public class LocalDelegationModel : BasicModel
    {
        public Guid? LocalDelegationId { get; set; }
        public string LocalDelegationName { get; set; }
        public int? LocalDelegationStatus { get; set; }
        public string LocalDelegationStatusText { get; set; }
    }
}