﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Xrm;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Site.Helpers;
using Xrm;

namespace Site.Areas.MyReports.Models
{
    public class AttendeeDetailModel
    {
        public int? Number { get; set; }
        public string Name { get; set; }
        public string OrderName { get; set; }
        public string SchoolBoard { get; set; }
        public List<string> AttendanceDatesList { get; set; }
        public string AttendanceDates { get; set; }
    }
}