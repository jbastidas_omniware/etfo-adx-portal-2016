﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Areas.MyReports.Models
{
    /// <summary>
    /// Event Listing report
    /// </summary>
    public class EventListingModel : BasicModel
    {
        public string CourseTermCode { get; set; }
        public string CourseTermName { get; set; }
        public int? RegistrationCaucusStatus { get; set; }
        public string RegistrationCaucusStatusText { get; set; }
        public int? RegistrationChildCareStatus { get; set; }
        public string RegistrationChildCareStatusText { get; set; }
        public int? RegistrationAccommodationStatus { get; set; }
        public string RegistrationAccommodationStatusText { get; set; }
        public int? RegistrationReleaseStatus { get; set; }
        public string RegistrationReleaseStatusText { get; set; }
        public int? ApplicationWorkshopStatus { get; set; }
        public string ApplicationWorkshopStatusText { get; set; }
        public bool WaitingListFlag { get; set; }
        public int? BillingPaymentStatus { get; set; }
        public string BillingPaymentStatusText { get; set; }
        public int? BillingPaymentMethod { get; set; }
        public string BillingPaymentMethodText { get; set; }
        public decimal? BillingDeferral { get; set; }
        public decimal? BillingTotalToPay { get; set; }
    }
}