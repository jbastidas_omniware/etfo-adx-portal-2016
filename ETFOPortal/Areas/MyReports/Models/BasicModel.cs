﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Xrm;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Site.Helpers;
using Xrm;

namespace Site.Areas.MyReports.Models
{
    /// <summary>
    /// My Reports Basic Model
    /// </summary>
    public class BasicModel
    {
        public int? EventStatus { get; set; }
        public string EventStatusText { get; set; }
        public Guid? EventCategoryId { get; set; }
        public string EventCategoryName { get; set; }
        public Guid? EventSubCategoryId { get; set; } 
        public string EventSubCategoryName { get; set; }
        public Guid? EventId { get; set; }
        public string EventTitle { get; set; }
        public string EventDescription { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public Guid? RegistrationId { get; set; }
        public int? RegistrationStatus { get; set; }
        public string RegistrationStatusText { get; set; }
        public DateTime? RegistrationStartDate { get; set; }
        public DateTime? RegistrationEndDate { get; set; }
        public Guid? CourseTermId { get; set; }
    }

    /// <summary>
    /// General report filter
    /// </summary>
    public class BasicFilter
    {
        //Event Listing
        public int? Status;
        public Guid? CategoryId;
        public Guid? SubCategoryId;
        public string EventName;
        public DateTime? EventStartDate;
        public DateTime? EventEndDate;
        public int? EventRegistrationStatusId;
        public DateTime? EventRegistrationStartDate;
        public DateTime? EventRegistrationEndDate;
    }

}