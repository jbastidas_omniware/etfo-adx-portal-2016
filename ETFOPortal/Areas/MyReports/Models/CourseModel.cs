﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Areas.MyReports.Models
{
    /// <summary>
    /// Release Time Request report
    /// </summary>
    public class CourseModel : BasicModel
    {
        public string CourseCode { get; set; }
        public string Topic { get; set; }
        public DateTime? InitialOferedDate { get; set; }
        public DateTime? FinalOferedDate { get; set; }
        public string InitialDailyTime { get; set; }
        public string FinalDailyTime { get; set; }
    }
}