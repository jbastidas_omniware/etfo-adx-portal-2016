﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotNetOpenAuth.Messaging;
using Microsoft.Xrm.Sdk;
using Site.Areas.Payment.Library;
using Xrm;

namespace Site.Areas.MyReports.Models
{
    [Serializable]
    public class MyCourseReceiptModel
    {
        public string OrderId { get; set; }
        public string AttendeeName { get; set; }
        public string AttendeeAddress { get; set; }
        public string TermName { get; set; }
        public string EventName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal Price { get; set; }
        
        public MyCourseReceiptModel()
        {}
    }
}