﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Areas.MyReports.Models
{
    /// <summary>
    /// Release time reports filter
    /// </summary>
    public class MyReleaseTimeReportsFilters : BasicFilter
    {
        public int? ReleaseTimeRequestStatus;
    }

    /// <summary>
    /// Release Time Request report
    /// </summary>
    public class ReleaseTimeRequestModel : BasicModel
    {
        public int? ReleaseTimeRequestStatus { get; set; }
        public string ReleaseTimeRequestStatusText { get; set; }
        public float? ReleaseTimeNumberOfDays { get; set; }
        public float? ReleaseTimeNumberOfExtraDays { get; set; }
    }
}