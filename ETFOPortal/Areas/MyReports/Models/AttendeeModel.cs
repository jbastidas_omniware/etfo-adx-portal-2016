﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Xrm;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Site.Helpers;
using Xrm;

namespace Site.Areas.MyReports.Models
{
    public class AttendeeModel
    {
        public int? Number { get; set; }
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string OrderName { get; set; }
        public string Mail { get; set; }        
        public string SchoolBoard { get; set; }
        public string NumberOfYearsTeaching { get; set; }
        public string CurrentAssignment { get; set; }
        public string FutureTeachingAssignment { get; set; }
    }
}