﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Areas.MyReports.Models
{
    /// <summary>
    /// Release Time Request report
    /// </summary>
    public class InstructorCourseHistoryModel : BasicModel
    {
        public string CourseTermCode { get; set; }
        public string CourseTermName { get; set; }
        public string CourseTitle { get; set; }
        public string CourseDescription { get; set; }
    }
}