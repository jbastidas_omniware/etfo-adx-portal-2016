﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotNetOpenAuth.Messaging;
using Microsoft.Xrm.Sdk;
using Site.Areas.Payment.Library;
using Xrm;

namespace Site.Areas.MyReports.Models
{
    [Serializable]
    public class MyEventReceiptModel
    {
        public string OrderId { get; set; }
        public string AttendeeName { get; set; }
        public string EventName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string EventLocation { get; set; }
        public string BillingStatus { get; set; }
        public string BillingType { get; set; }
        public decimal BillingAmount { get; set; }
        public DateTime? BillingPaymentDate { get; set; }

        public MyEventReceiptModel()
        {}
    }
}