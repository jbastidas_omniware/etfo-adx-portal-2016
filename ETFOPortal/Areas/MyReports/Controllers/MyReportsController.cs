﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Adxstudio.Xrm.Web.Mvc;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Org.BouncyCastle.Crypto.Engines;
using Site.Helpers;
using Site.Library;
using Xrm;
using Site.Areas.MyReports.Library;
using Site.Areas.MyReports.Models;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System.Globalization;

namespace Site.Areas.MyReports.Controllers
{
    [PortalView]
    [Authorize]
    public class MyReportsController : Controller
    {
        #region Event Listing

        /// <summary>
        /// Event listing report main view
        /// </summary>
        public ActionResult EventListing()
        {
            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("MyReports/EventListing/PageTitle", "Event Listing");
            ViewBag.Description = XrmHelper.GetSnippetValueOrDefault("MyReports/EventListing/Description", "Search for all Events with a Registration Form associated with this user that match the above criteria");
            ViewBag.Breadcrumb = XrmHelper.GetSnippetValueOrDefault("MyReports/EventListing/Breadcrumb", "My Reports");
            ViewBag.PresidentFlag = this.IsPresident();

            return this.RazorView();
        }

        /// <summary>
        /// Gets the event listing report data
        /// </summary>
        [HttpPost]
        public JsonResult GetEventListings(FormCollection formValues)
        {
            var portal = PortalCrmConfigurationManager.CreatePortalContext();

            // Filters
            BasicFilter filters = this.LoadFilters(formValues);
            MyReportsListing reportsListing = new MyReportsListing(filters, portal.User.Id);
            var results = reportsListing.EventListingGetData();

            var model = new
            {
                data = results,
                total = results.Count
            };

            return new JsonNetResult() { Data = model };
        }

        #endregion

        #region Release Time Request

        /// <summary>
        /// Release time request report main view
        /// </summary>
        /// <returns></returns>
        public ActionResult ReleaseTimeRequest()
        {
            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("MyReports/ReleaseTimeRequest/PageTitle", "Release Time Request");
            ViewBag.Description = XrmHelper.GetSnippetValueOrDefault("MyReports/ReleaseTimeRequest/Description", "Search for all Events with a Registration Form associated with this user with a Release Time Request that match the above criteria");
            ViewBag.Breadcrumb = XrmHelper.GetSnippetValueOrDefault("MyReports/ReleaseTimeRequest/Breadcrumb", "My Reports");
            ViewBag.PresidentFlag = this.IsPresident();

            return this.RazorView();
        }

        /// <summary>
        /// Gets the release time request report data
        /// </summary>
        [HttpPost]
        public JsonResult GetReleaseTimeRequest(FormCollection formValues)
        {
            var portal = PortalCrmConfigurationManager.CreatePortalContext();

            // Filters
            BasicFilter filters = this.LoadFilters(formValues);
            var serializedFilters = JsonConvert.SerializeObject(filters);
            MyReleaseTimeReportsFilters releaseFilter = JsonConvert.DeserializeObject<MyReleaseTimeReportsFilters>(serializedFilters);
            int releaseTimeRequestStatus = Convert.ToInt32(formValues["releaseTimeRequestStatus"]);
            releaseFilter.ReleaseTimeRequestStatus = releaseTimeRequestStatus == -1 ? (int?)null : releaseTimeRequestStatus;

            MyReportsListing reportsListing = new MyReportsListing(releaseFilter, portal.User.Id);
            var results = reportsListing.ReleaseTimeRequestGetData();

            var model = new
            {
                data = results,
                total = results.Count
            };

            return new JsonNetResult() { Data = model };
        }

        /// <summary>
        /// Loads the status filters
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult LoadReleaseTimeStatusFilters()
        {
            var context = XrmHelper.GetContext();

            //releast time request status
            RetrieveAttributeRequest attributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = "oems_releasetimerequest",
                LogicalName = "statuscode"
            };
            var releases = PopulateStatusDropdownList(context, attributeRequest);

            var result = new { Releases = releases };
            return Json(result);
        }

        #endregion

        #region Local Delegation

        /// <summary>
        /// Local delegation report main view
        /// </summary>
        /// <returns></returns>
        public ActionResult LocalDelegation()
        {
            ViewBag.PresidentFlag = this.IsPresident();
            if (!ViewBag.PresidentFlag)
                this.RazorView("EventListing");

            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("MyReports/Local Delegation/PageTitle", "Local Delegation");
            ViewBag.Description = XrmHelper.GetSnippetValueOrDefault("MyReports/Local Delegation/Description", "Search for all Events with a Local Delegation associated with this user (i.e. that match the user’s Local Office) that match the above criteria.");
            ViewBag.Breadcrumb = XrmHelper.GetSnippetValueOrDefault("MyReports/Local Delegation/Breadcrumb", "My Reports");

            return this.RazorView();
        }

        /// <summary>
        /// Gets the release time request report data
        /// </summary>
        [HttpPost]
        public JsonResult GetLocalDelegation(FormCollection formValues)
        {
            var portal = PortalCrmConfigurationManager.CreatePortalContext();

            // Filters
            BasicFilter filters = this.LoadFilters(formValues);
            var serializedFilters = JsonConvert.SerializeObject(filters);
            LocalDelegationReportsFilters localDelegationFilter = JsonConvert.DeserializeObject<LocalDelegationReportsFilters>(serializedFilters);
            int localDelegationStatus = Convert.ToInt32(formValues["localDelegationStatus"]);
            localDelegationFilter.LocalDelegationStatus = localDelegationStatus == -1 ? (int?)null : localDelegationStatus;

            MyReportsListing reportsListing = new MyReportsListing(localDelegationFilter, portal.User.Id);
            var results = reportsListing.LocalDelegationGetData();

            var model = new
            {
                data = results,
                total = results.Count
            };

            return new JsonNetResult() { Data = model };
        }

        /// <summary>
        /// Loads the status filters
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult LoadLocalDelegationStatusFilters()
        {
            var context = XrmHelper.GetContext();

            //Local delegation status
            RetrieveAttributeRequest attributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = "oems_eventlocal",
                LogicalName = "statuscode"
            };
            var locals = PopulateStatusDropdownList(context, attributeRequest);

            var result = new { Locals = locals };
            return Json(result);
        }

        #endregion

        #region My Receipts

        /// <summary>
        /// My course receipt report main view
        /// </summary>
        /// <returns></returns>
        public ActionResult MyCourseReceipt()
        {
            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("MyReports/MyCourseReceipt/PageTitle", "My Course Receipts");
            ViewBag.Description = XrmHelper.GetSnippetValueOrDefault("MyReports/MyCourseReceipt/Description", "ETFO Summer Academy Receipts");
            ViewBag.Breadcrumb = XrmHelper.GetSnippetValueOrDefault("MyReports/MyCourseReceipt/Breadcrumb", "My Reports");
            ViewBag.PresidentFlag = this.IsPresident();

            var context = XrmHelper.GetContext();
            var portal = PortalCrmConfigurationManager.CreatePortalContext();
            Entity user = context.Retrieve(portal.User.LogicalName, portal.User.Id, new ColumnSet("fullname", "address1_name"));
            ViewBag.Attendee = user.GetAttributeValue<string>("fullname");
            ViewBag.Address = user.GetAttributeValue<string>("address1_name");

            return this.RazorView();
        }

        [HttpPost]
        public JsonResult GetCourseReceipt()
        {
            // Filters
            var portal = PortalCrmConfigurationManager.CreatePortalContext();
            MyReportsListing reportsListing = new MyReportsListing(null, portal.User.Id);
            var results = reportsListing.MyCourseReceiptGetData();

            var model = new
            {
                data = results,
                total = results.Count
            };

            return new JsonNetResult() { Data = model };
        }

        /// <summary>
        /// My events receipt report main view
        /// </summary>
        /// <returns></returns>
        public ActionResult MyEventsReceipt()
        {
            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("MyReports/MyEventsReceipt/PageTitle", "My Events Receipts");
            ViewBag.Description = XrmHelper.GetSnippetValueOrDefault("MyReports/MyEventsReceipt/Description", "ETFO Summer Academy Receipts");
            ViewBag.Breadcrumb = XrmHelper.GetSnippetValueOrDefault("MyReports/MyEventsReceipt/Breadcrumb", "My Reports");
            ViewBag.PresidentFlag = this.IsPresident();

            var context = XrmHelper.GetContext();
            var portal = PortalCrmConfigurationManager.CreatePortalContext();
            Entity user = context.Retrieve(portal.User.LogicalName, portal.User.Id, new ColumnSet("fullname", "address1_name"));
            ViewBag.Attendee = user.GetAttributeValue<string>("fullname");
            ViewBag.Address = user.GetAttributeValue<string>("address1_name");

            return this.RazorView();
        }

        [HttpPost]
        public JsonResult GetEventsReceipt()
        {
            // Filters
            var portal = PortalCrmConfigurationManager.CreatePortalContext();
            MyReportsListing reportsListing = new MyReportsListing(null, portal.User.Id);
            var results = reportsListing.MyEventsReceiptGetData();

            var model = new
            {
                data = results,
                total = results.Count
            };

            return new JsonNetResult() { Data = model };
        }

        #endregion

        #region Instructor Course History

        /// <summary>
        /// Instructor course history report main view
        /// </summary>
        /// <returns></returns>
        public ActionResult InstructorCourseHistory()
        {
            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("MyReports/InstructorCourseHistory/PageTitle", "Instructor Course History");
            ViewBag.Description = XrmHelper.GetSnippetValueOrDefault("MyReports/InstructorCourseHistory/Description", "Search for all Events with an Application Form associated with this user that match the above criteria");
            ViewBag.Breadcrumb = XrmHelper.GetSnippetValueOrDefault("MyReports/InstructorCourseHistory/Breadcrumb", "My Reports");
            ViewBag.PresidentFlag = this.IsPresident();

            return this.RazorView();
        }

        [HttpPost]
        public JsonResult GetInstructorCourseHistory(FormCollection formValues)
        {
            var portal = PortalCrmConfigurationManager.CreatePortalContext();

            // Filters
            BasicFilter filters = this.LoadFilters(formValues);
            MyReportsListing reportsListing = new MyReportsListing(filters, portal.User.Id);
            var results = reportsListing.InstructorCourseHistoryGetData();

            var model = new
            {
                data = results,
                total = results.Count
            };

            return new JsonNetResult() { Data = model };
        }

        #endregion

        #region Instructor Course List

        /// <summary>
        /// Instructor course list report main view
        /// </summary>
        /// <returns></returns>
        public ActionResult InstructorCourseList()
        {
            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("MyReports/InstructorCourseList/PageTitle", "Instructor Course List");
            ViewBag.Description = XrmHelper.GetSnippetValueOrDefault("MyReports/InstructorCourseList/Description", "Search for all Course Events where this user is the Instructor that match the above criteria.");
            ViewBag.Breadcrumb = XrmHelper.GetSnippetValueOrDefault("MyReports/InstructorCourseList/Breadcrumb", "My Reports");
            ViewBag.PresidentFlag = this.IsPresident();

            return this.RazorView();
        }

        [HttpPost]
        public JsonResult GetInstructorCourseList(FormCollection formValues)
        {
            var portal = PortalCrmConfigurationManager.CreatePortalContext();

            // Filters
            BasicFilter filters = this.LoadFilters(formValues);
            MyReportsListing reportsListing = new MyReportsListing(filters, portal.User.Id);
            var results = reportsListing.InstructorCourseListGetData();

            var model = new
            {
                data = results,
                total = results.Count
            };

            return new JsonNetResult() { Data = model };
        }

        [HttpPost]
        public JsonResult GetCourseAttendees(Guid courseId)
        {
            var portal = PortalCrmConfigurationManager.CreatePortalContext();
            MyReportsListing reportsListing = new MyReportsListing(null, portal.User.Id);
            var results = reportsListing.GetCourseAttendees(courseId);

            var model = new
            {
                data = results,
                total = results.Count
            };

            return new JsonNetResult() { Data = model };
        }

        #endregion

        #region Instructor Course List Attendance

        /// <summary>
        /// Instructor course list attendance report main view
        /// </summary>
        /// <returns></returns>
        public ActionResult InstructorCourseListAttendance()
        {
            ViewBag.Title = XrmHelper.GetSnippetValueOrDefault("MyReports/InstructorCourseListAttendance/PageTitle", "Instructor Course List Attendance");
            ViewBag.Description = XrmHelper.GetSnippetValueOrDefault("MyReports/InstructorCourseListAttendance/Description", "Instructor Course List Attendance");
            ViewBag.Breadcrumb = XrmHelper.GetSnippetValueOrDefault("MyReports/InstructorCourseListAttendance/Breadcrumb", "My Reports");
            ViewBag.PresidentFlag = this.IsPresident();

            return this.RazorView();
        }

        [HttpPost]
        public JsonResult GetInstructorCourseListAttendance(FormCollection formValues)
        {
            var portal = PortalCrmConfigurationManager.CreatePortalContext();

            // Filters
            BasicFilter filters = this.LoadFilters(formValues);
            MyReportsListing reportsListing = new MyReportsListing(filters, portal.User.Id);
            var results = reportsListing.InstructorCourseListAttendanceGetData();

            var model = new
            {
                data = results,
                total = results.Count
            };

            return new JsonNetResult() { Data = model };
        }

        [HttpPost]
        public JsonResult GetCourseAttendance(Guid courseId)
        {
            var portal = PortalCrmConfigurationManager.CreatePortalContext();
            MyReportsListing reportsListing = new MyReportsListing(null, portal.User.Id);
            var results = reportsListing.GetCourseAttendance(courseId);

            var model = new
            {
                data = results,
                total = results.Count
            };

            return new JsonNetResult() { Data = model };
        }

        #endregion

        #region Common

        /// <summary>
        /// Loads the status filters
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult LoadBasicStatusFilters()
        {
            var context = XrmHelper.GetContext();

            //Event status
            RetrieveAttributeRequest attributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = "oems_event",
                LogicalName = "statuscode"
            };
            var evts = PopulateStatusDropdownList(context, attributeRequest);

            //registration status
            attributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = "oems_eventregistration",
                LogicalName = "statuscode"
            };
            var regs = PopulateStatusDropdownList(context, attributeRequest);
            var result =
                new
                {
                    Events = evts,
                    Registrations = regs
                };

            return Json(result);
        }

        /// <summary>
        /// Loads the categories
        /// </summary>
        /// <returns></returns>
        public JsonResult LoadCategories()
        {
            var context = XrmHelper.GetContext();
            var categories = (from reg in context.CreateQuery("oems_category") select reg);

            var results = new List<object>();
            results.Add(new { Id = Guid.Empty, Name = "All" });
            foreach (var cat in categories)
            {
                results.Add(new { Id = cat.GetAttributeValue<Guid>("oems_categoryid"), Name = cat.GetAttributeValue<string>("oems_categoryname") });
            }

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Loads the subcategories
        /// </summary>
        /// <returns></returns>
        public JsonResult LoadSubCategories(Guid? categoryId)
        {
            var context = XrmHelper.GetContext();

            var results = new List<object>();
            results.Add(new { Id = Guid.Empty, Name = "All" });

            if (categoryId.HasValue)
            {
                var subcategories = (from sub in context.CreateQuery("oems_subcategory")
                                     where sub.GetAttributeValue<EntityReference>("oems_category").Id == categoryId
                                     select sub);

                foreach (var sub in subcategories)
                {
                    results.Add(
                        new
                        {
                            Id = sub.GetAttributeValue<Guid>("oems_subcategoryid"),
                            Name = sub.GetAttributeValue<string>("oems_subcategoryname")
                        });
                }
            }

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Allow users to export reports on unsupported browsers
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult KendoSave(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);
            return File(fileContents, contentType, fileName);
        }

        /// <summary>
        /// Load the filters according to the report type
        /// </summary>
        /// <returns></returns>
        private BasicFilter LoadFilters(FormCollection formValues)
        {
            BasicFilter filters = new BasicFilter();
            CultureInfo culture = new CultureInfo("en-CA");

            string eventName = formValues["eventName"].ToString();
            int eventStatus = Convert.ToInt32(formValues["eventStatus"]);
            Guid category = new Guid(formValues["category"].ToString());
            Guid subcategory = new Guid(formValues["subcategory"].ToString());
            DateTime? eventStartDate = !string.IsNullOrWhiteSpace(formValues["eventStartDate"].ToString()) ? Convert.ToDateTime(formValues["eventStartDate"], culture) : (DateTime?)null;
            DateTime? eventEndDate = !string.IsNullOrWhiteSpace(formValues["eventEndDate"].ToString()) ? Convert.ToDateTime(formValues["eventEndDate"], culture) : (DateTime?)null;
            int registrationStatus = Convert.ToInt32(formValues["registrationStatus"]);
            DateTime? registrationStartDate = !string.IsNullOrWhiteSpace(formValues["registrationStartDate"].ToString()) ? Convert.ToDateTime(formValues["registrationStartDate"], culture) : (DateTime?)null;
            DateTime? registrationEndDate = !string.IsNullOrWhiteSpace(formValues["registrationEndDate"].ToString()) ? Convert.ToDateTime(formValues["registrationEndDate"], culture) : (DateTime?)null;

            // Basic filters
            filters.Status = eventStatus == -1 ? (int?)null : eventStatus;
            filters.CategoryId = category == Guid.Empty ? null : (Guid?)category;
            filters.SubCategoryId = subcategory == Guid.Empty ? null : (Guid?)subcategory;
            filters.EventName = eventName;
            filters.EventStartDate = eventStartDate;
            filters.EventEndDate = eventEndDate;
            filters.EventRegistrationStatusId = registrationStatus == -1 ? (int?)null : registrationStatus;
            filters.EventRegistrationStartDate = registrationStartDate;
            filters.EventRegistrationEndDate = registrationEndDate;

            return filters;
        }

        /// <summary>
        /// Populate drop downs with status info
        /// </summary>
        /// <param name="context"></param>
        /// <param name="attributeRequest"></param>
        /// <returns></returns>
        private object PopulateStatusDropdownList(XrmServiceContext context, RetrieveAttributeRequest attributeRequest)
        {
            var attributeResponse = (RetrieveAttributeResponse)context.Execute(attributeRequest);
            var attrMetadata = (AttributeMetadata)attributeResponse.AttributeMetadata;
            var statusMetadata = (StatusAttributeMetadata)attrMetadata;

            List<object> results = new List<object>();
            results.Add(new { Id = -1, Name = "All" });
            foreach (var optionMeta in statusMetadata.OptionSet.Options)
            {
                var statusOptionMeta = (StatusOptionMetadata)optionMeta;
                if (statusOptionMeta.State == 0)
                {
                    if (statusOptionMeta.Value != null)
                    {
                        results.Add(new { Id = statusOptionMeta.Value.Value, Name = statusOptionMeta.Label.UserLocalizedLabel.Label });
                    }
                }
            }

            return results;
        }

        /// <summary>
        /// Check if the user is a president
        /// </summary>
        /// <returns></returns>
        private bool IsPresident()
        {
            var now = DateTime.Now.Date;
            var portal = PortalCrmConfigurationManager.CreatePortalContext();
            var serviceContext = XrmHelper.GetContext();

            var orgContact =
            (
                from a in serviceContext.mbr_organizationcontactSet
                where a.mbr_ContactId != null && a.mbr_ContactId.Id == portal.User.Id
                    //&& a.mbr_LocalId != null && a.mbr_LocalId.Id == eventRegistration.oems_Local.Id
                    && a.mbr_EffectiveStartDate != null && a.mbr_EffectiveStartDate.Value <= now
                    && a.mbr_EffectiveEndDate != null && a.mbr_EffectiveEndDate.Value >= now
                    && a.mbr_Role != null && a.statecode == 0
                select a
            ).ToList();

            if(orgContact.Count() == 0)
            {
                return false;
            }

            var isPresident = orgContact.Any(c => c.mbr_Role.Name.ToLower().Trim() == "president");
            return isPresident;
        }

        #endregion
    }
}