﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Portal;
using Microsoft.Xrm.Sdk;
using Site.Areas.MyReports.Models;
using Site.Helpers;
using Xrm;
using Newtonsoft.Json;
using System.Globalization;
using Site.Areas.Payment.Library;

namespace Site.Areas.MyReports.Library
{
    public class MyReportsListing
    {
        private BasicFilter _myFilters;
        private Guid _contactId;

        public int Count { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public MyReportsListing(BasicFilter filters, Guid contactId)
        {
            this._myFilters = filters;
            this._contactId = contactId;
        }

        #region Common Functions

        /// <summary>
        /// Filters the current query
        /// </summary>
        /// <param name="query">Basic query results</param>
        /// <returns>Results filtered</returns>
        private void BasicFilter(ref List<BasicModel> query)
        {
            // Status
            if (this._myFilters.Status.HasValue)
            {
                query.RemoveAll(q => q.EventStatus != this._myFilters.Status);
            }

            // Category
            if (this._myFilters.CategoryId.HasValue)
            {
                query.RemoveAll(q => q.EventCategoryId != this._myFilters.CategoryId);
            }

            // SubCategoryId
            if (this._myFilters.SubCategoryId.HasValue)
            {
                query.RemoveAll(q => q.EventSubCategoryId != this._myFilters.SubCategoryId);
            }

            // EventName
            if (!string.IsNullOrWhiteSpace(this._myFilters.EventName))
            {
                query = query.Where(q => q.EventTitle.ToLower().Contains(this._myFilters.EventName.ToLower())).ToList();
            }

            // EventStartDate
            if (this._myFilters.EventStartDate.HasValue)
            {
                query.RemoveAll(q => !q.StartDate.HasValue || q.StartDate.Value < this._myFilters.EventStartDate.Value);
            }

            // EventEndDate
            if (this._myFilters.EventEndDate.HasValue)
            {
                query.RemoveAll(q => !q.EndDate.HasValue || q.EndDate.Value > this._myFilters.EventEndDate.Value);
            }

            // EventRegistrationStatusId
            if (this._myFilters.EventRegistrationStatusId.HasValue)
            {
                query.RemoveAll(q => q.RegistrationStatus != this._myFilters.EventRegistrationStatusId);
            }

            // EventRegistrationStartDate
            if (this._myFilters.EventRegistrationStartDate.HasValue)
            {
                query.RemoveAll(q => !q.RegistrationStartDate.HasValue || q.RegistrationStartDate.Value < this._myFilters.EventRegistrationStartDate.Value);
            }

            // EventRegistrationEndDate
            if (this._myFilters.EventRegistrationEndDate.HasValue)
            {
                query.RemoveAll(q => !q.RegistrationEndDate.HasValue || q.RegistrationEndDate.Value > this._myFilters.EventRegistrationEndDate.Value);
            }
        }

        /// <summary>
        /// Get status text from a curent stats code
        /// </summary>
        /// <param name="eventStatusCode">Event status code</param>
        /// <param name="registrationStatusCode">Registration Statud Code</param>
        /// <param name="eventid">Event Id</param>
        /// <returns>Returns the current test for the stats id</returns>
        private string GetEventStatusText(int? eventStatusCode, int? registrationStatusCode, Guid? eventid)
        {
            if (eventStatusCode == RegistrationHelper.EventStatus.Completed && registrationStatusCode == RegistrationHelper.EventRegistration.Approved)
            {
                using (var serviceContext = new XrmServiceContext())
                {
                    var result = serviceContext.oems_EventAttendeeSet.SingleOrDefault(q => q.oems_EventRegistration.Id == eventid);

                    if (result != null)
                        return EntityOptionSet.GetOptionSetLabel("oems_eventattendee", "statuscode", result.statuscode);
                }
            }

            return EntityOptionSet.GetOptionSetLabel("oems_event", "statuscode", eventStatusCode);
        }

        #endregion

        #region Event Listing Functions

        /// <summary>
        /// Gets event listings reports data
        /// </summary>
        /// <returns></returns>
        public List<EventListingModel> EventListingGetData()
        {
            var basicResults = new List<BasicModel>();
            var results = new List<EventListingModel>();
            using (var serviceContext = new XrmServiceContext())
            {
                #region Query

                var query =
                (
                    from reg in serviceContext.CreateQuery("oems_eventregistration")
                    join evt in serviceContext.CreateQuery("oems_event") on ((EntityReference)reg["oems_event"]).Id equals evt["oems_eventid"]
                    join sub in serviceContext.CreateQuery("oems_subcategory") on ((EntityReference)evt["oems_subcategory"]).Id equals sub["oems_subcategoryid"]
                    join cat in serviceContext.CreateQuery("oems_category") on ((EntityReference)sub["oems_category"]).Id equals cat["oems_categoryid"]
                    where reg.GetAttributeValue<EntityReference>("oems_contact").Id == this._contactId
                    where reg.GetAttributeValue<int>("statuscode") != RegistrationHelper.EventRegistration.Initial
                    select new
                    {
                        Event = new BasicModel()
                        {
                            EventStatus = (evt.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                            EventStatusText = GetEventStatusText((evt.GetAttributeValue<OptionSetValue>("statuscode")).Value, (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value, evt.Id),
                            EventCategoryId = (sub.GetAttributeValue<EntityReference>("oems_category")).Id,
                            EventCategoryName = cat.GetAttributeValue<string>("oems_categoryname"),
                            EventSubCategoryId = (evt.GetAttributeValue<EntityReference>("oems_subcategory")).Id,
                            EventSubCategoryName = sub.GetAttributeValue<string>("oems_subcategoryname"),
                            CourseTermId = evt.GetAttributeValue<EntityReference>("oems_courseterm") != null ? (evt.GetAttributeValue<EntityReference>("oems_courseterm")).Id : (Guid?)null,
                            EventId = evt.GetAttributeValue<Guid>("oems_eventid"),
                            EventTitle = evt.GetAttributeValue<string>("oems_eventname"),
                            StartDate = evt.GetAttributeValue<DateTime>("oems_startdate"),
                            EndDate = evt.GetAttributeValue<DateTime>("oems_enddate"),
                            RegistrationId = reg.GetAttributeValue<Guid?>("oems_eventregistrationid"),
                            RegistrationStatus = (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                            RegistrationStatusText = EntityOptionSet.GetOptionSetLabel("oems_eventregistration", "statuscode", (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value),
                            RegistrationStartDate = evt.GetAttributeValue<DateTime?>("oems_registrationopening"),
                            RegistrationEndDate = evt.GetAttributeValue<DateTime?>("oems_registrationdeadline"),
                        }
                    }
                ).ToList();
                query.ForEach(q => basicResults.Add(q.Event));

                #endregion

                //Filter the query
                this.BasicFilter(ref basicResults);

                //Serialize results into EvetLitings
                var serializedResults = JsonConvert.SerializeObject(basicResults);
                results = JsonConvert.DeserializeObject<List<EventListingModel>>(serializedResults);

                //Gets extra values
                this.EventListingGetExtraValues(serviceContext, ref results);
            }

            this.Count = results.Count();
            return results;
        }

        /// <summary>
        /// Filters the current query
        /// </summary>
        /// <param name="results">Basic query results</param>
        /// <returns>Results filtered</returns>
        private void EventListingGetExtraValues(XrmServiceContext context, ref List<EventListingModel> results)
        {
            foreach (var evt in results)
            {
                #region Courses

                // Caucus Request Status
                evt.CourseTermCode = "N/A";
                evt.CourseTermName = "N/A";
                if (evt.CourseTermId != null)
                {
                    var term = (from trm in context.CreateQuery("oems_event") where trm.GetAttributeValue<Guid>("oems_eventid") == evt.CourseTermId select trm).First();
                   
                    evt.CourseTermName = term.GetAttributeValue<string>("oems_eventname");
                    evt. CourseTermCode = term.GetAttributeValue<string>("oems_webtermdescription");
                }

                #endregion
                #region Requests

                // Caucus Request Status
                evt.RegistrationCaucusStatusText = "N/A";
                var cr = (from caucus in context.CreateQuery("oems_caucusrequest") where caucus.GetAttributeValue<EntityReference>("oems_eventregistration").Id == evt.RegistrationId.Value select caucus).FirstOrDefault();
                if (cr != null)
                {
                    evt.RegistrationCaucusStatus = (cr.GetAttributeValue<OptionSetValue>("statuscode")).Value;
                    evt.RegistrationCaucusStatusText = EntityOptionSet.GetOptionSetLabel("oems_caucusrequest", "statuscode", evt.RegistrationCaucusStatus);
                }

                // Child Care Request Status
                evt.RegistrationChildCareStatusText = "N/A";
                var ccr = (from childCare in context.CreateQuery("oems_childcarerequest") where childCare.GetAttributeValue<EntityReference>("oems_eventregistration").Id == evt.RegistrationId.Value select childCare).FirstOrDefault();
                if (ccr != null)
                {
                    evt.RegistrationChildCareStatus = (ccr.GetAttributeValue<OptionSetValue>("statuscode")).Value;
                    evt.RegistrationChildCareStatusText = EntityOptionSet.GetOptionSetLabel("oems_childcarerequest", "statuscode", evt.RegistrationChildCareStatus);
                }

                // Accommodation Request Status
                evt.RegistrationAccommodationStatusText = "N/A";
                var ar = (from acc in context.CreateQuery("oems_accommodationrequest") where acc.GetAttributeValue<EntityReference>("oems_eventregistration").Id == evt.RegistrationId.Value select acc).FirstOrDefault();
                if (ar != null)
                {
                    evt.RegistrationAccommodationStatus = (ar.GetAttributeValue<OptionSetValue>("statuscode")).Value;
                    evt.RegistrationAccommodationStatusText = EntityOptionSet.GetOptionSetLabel("oems_accommodationrequest", "statuscode", evt.RegistrationAccommodationStatus);
                }

                // Release Time Request Status
                evt.RegistrationReleaseStatusText = "N/A";
                var rtr = (from relTime in context.CreateQuery("mbr_releasetimetracking") where relTime.GetAttributeValue<EntityReference>("oems_eventregistration").Id == evt.RegistrationId.Value select relTime).FirstOrDefault();
                if (rtr != null)
                {
                    evt.RegistrationReleaseStatus = (rtr.GetAttributeValue<OptionSetValue>("statuscode")).Value;
                    evt.RegistrationReleaseStatusText = EntityOptionSet.GetOptionSetLabel("oems_releasetimerequest", "statuscode", evt.RegistrationReleaseStatus);
                }

                // Registration Workshop Status (Not defined)
                evt.ApplicationWorkshopStatusText = "N/A";
                var wsr = (from ws in context.CreateQuery("oems_eventworkshopsessionrequest") where ws.GetAttributeValue<EntityReference>("oems_eventapplication").Id == evt.RegistrationId.Value select ws).FirstOrDefault();
                if (wsr != null)
                {
                    evt.ApplicationWorkshopStatus = (wsr.GetAttributeValue<OptionSetValue>("statuscode")).Value;
                    evt.ApplicationWorkshopStatusText = EntityOptionSet.GetOptionSetLabel("oems_eventworkshopsessionrequest", "statuscode", evt.ApplicationWorkshopStatus);
                }

                #endregion
                #region Flags

                //Waiting List Flag
                evt.WaitingListFlag = false;
                var wl = (from waitingList in context.CreateQuery("oems_waitinglistrequest") where waitingList.GetAttributeValue<EntityReference>("oems_eventregistration").Id == evt.RegistrationId.Value select waitingList).FirstOrDefault();
                if (wl != null 
                    && ((wl.GetAttributeValue<OptionSetValue>("statuscode")).Value == RegistrationHelper.WaitingListStatus.Waiting || wl.GetAttributeValue<OptionSetValue>("statuscode").Value == RegistrationHelper.WaitingListStatus.Offered)
                )
                {
                    evt.WaitingListFlag = true;
                }

                //PersonalInfoChangeRequestedFlag (Not defined)

                #endregion
                #region Billing

                // Billing Request
                evt.BillingPaymentStatusText = "N/A";
                evt.BillingPaymentMethodText = "N/A";
                evt.BillingDeferral = 0;
                evt.BillingTotalToPay = 0;
                var br = (from billingReq in context.CreateQuery("oems_billingrequest") where billingReq.GetAttributeValue<EntityReference>("oems_eventregistration").Id == evt.RegistrationId.Value select billingReq).FirstOrDefault();
                if (br != null)
                {
                    evt.BillingPaymentStatus = (br.GetAttributeValue<OptionSetValue>("oems_paymentstatus")).Value;
                    evt.BillingPaymentStatusText = EntityOptionSet.GetOptionSetLabel("oems_billingrequest", "oems_paymentstatus", evt.BillingPaymentStatus);
                    evt.BillingDeferral = br.GetAttributeValue<decimal>("oems_deferralamount");
                    evt.BillingTotalToPay = br.GetAttributeValue<decimal>("oems_totalamountdue");

                    if (br.GetAttributeValue<OptionSetValue>("oems_paymentmethod") != null)
                    {
                        evt.BillingPaymentMethod = (br.GetAttributeValue<OptionSetValue>("oems_paymentmethod")).Value;
                        evt.BillingPaymentMethodText = EntityOptionSet.GetOptionSetLabel("oems_billingrequest", "oems_paymentmethod", evt.BillingPaymentMethod);
                    }
                }

                #endregion
            }
        }

        #endregion Event Listing Functions

        #region Release Time Functions

        /// <summary>
        /// Gets release time requests reports data
        /// </summary>
        /// <returns></returns>
        public List<ReleaseTimeRequestModel> ReleaseTimeRequestGetData()
        {
            var basicResults = new List<BasicModel>();
            var results = new List<ReleaseTimeRequestModel>();
            using (var serviceContext = new XrmServiceContext())
            {
                #region Query

                var query =
                (
                    from reg in serviceContext.CreateQuery("oems_eventregistration")
                    join evt in serviceContext.CreateQuery("oems_event") on ((EntityReference)reg["oems_event"]).Id equals evt["oems_eventid"]
                    join sub in serviceContext.CreateQuery("oems_subcategory") on ((EntityReference)evt["oems_subcategory"]).Id equals sub["oems_subcategoryid"]
                    join cat in serviceContext.CreateQuery("oems_category") on ((EntityReference)sub["oems_category"]).Id equals cat["oems_categoryid"]
                    where reg.GetAttributeValue<EntityReference>("oems_contact").Id == this._contactId
                    where reg.GetAttributeValue<int>("statuscode") != RegistrationHelper.EventRegistration.Initial
                    select new
                    {
                        Event = new BasicModel()
                        {
                            EventStatus = (evt.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                            EventStatusText = GetEventStatusText((evt.GetAttributeValue<OptionSetValue>("statuscode")).Value, (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value, evt.Id),
                            EventCategoryId = (sub.GetAttributeValue<EntityReference>("oems_category")).Id,
                            EventCategoryName = cat.GetAttributeValue<string>("oems_categoryname"),
                            EventSubCategoryId = (evt.GetAttributeValue<EntityReference>("oems_subcategory")).Id,
                            EventSubCategoryName = sub.GetAttributeValue<string>("oems_subcategoryname"),
                            CourseTermId = evt.GetAttributeValue<EntityReference>("oems_courseterm") != null ? (evt.GetAttributeValue<EntityReference>("oems_courseterm")).Id : (Guid?)null,
                            EventId = evt.GetAttributeValue<Guid>("oems_eventid"),
                            EventTitle = evt.GetAttributeValue<string>("oems_eventname"),
                            StartDate = evt.GetAttributeValue<DateTime>("oems_startdate"),
                            EndDate = evt.GetAttributeValue<DateTime>("oems_enddate"),
                            RegistrationId = reg.GetAttributeValue<Guid?>("oems_eventregistrationid"),
                            RegistrationStatus = (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                            RegistrationStatusText = EntityOptionSet.GetOptionSetLabel("oems_eventregistration", "statuscode", (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value),
                            RegistrationStartDate = evt.GetAttributeValue<DateTime?>("oems_registrationopening"),
                            RegistrationEndDate = evt.GetAttributeValue<DateTime?>("oems_registrationdeadline"),
                        }
                    }
                ).ToList();
                query.ForEach(q => basicResults.Add(q.Event));

                #endregion

                //Filter the query
                this.BasicFilter(ref basicResults);

                //Serialize results into EvetLitings
                var serializedResults = JsonConvert.SerializeObject(basicResults);
                results = JsonConvert.DeserializeObject<List<ReleaseTimeRequestModel>>(serializedResults);

                //Gets extra values
                this.ReleaseTimeRequestGetExtraValuesAndFilter(serviceContext, ref results);
            }

            this.Count = results.Count();
            return results;
        }

        /// <summary>
        /// Filters the current query
        /// </summary>
        /// <param name="query">Basic query results</param>
        /// <returns>Results filtered</returns>
        private void ReleaseTimeRequestGetExtraValuesAndFilter(XrmServiceContext context, ref List<ReleaseTimeRequestModel> query)
        {
            MyReleaseTimeReportsFilters filters = (MyReleaseTimeReportsFilters)_myFilters;
            var results = new List<ReleaseTimeRequestModel>();
            foreach (var evt in query)
            {
                // Release Time Request
                evt.ReleaseTimeRequestStatusText = "N/A";
                evt.ReleaseTimeNumberOfDays = 0;
                evt.ReleaseTimeNumberOfExtraDays = 0;
              
                var rtr = (from releaseTime in context.CreateQuery("oems_releasetimerequest") where releaseTime.GetAttributeValue<EntityReference>("oems_eventregistration").Id == evt.RegistrationId.Value select releaseTime).FirstOrDefault();
                if (rtr != null)
                {
                    evt.ReleaseTimeRequestStatus = (rtr.GetAttributeValue<OptionSetValue>("statuscode")).Value;
                    evt.ReleaseTimeRequestStatusText = EntityOptionSet.GetOptionSetLabel("oems_releasetimerequest", "statuscode", evt.ReleaseTimeRequestStatus);
                    evt.ReleaseTimeNumberOfDays = this.CalculateReleaseTimeDays(context, rtr);
                    evt.ReleaseTimeNumberOfExtraDays = this.CalculateReleaseTimeExtraDays(context, rtr);
                }

                //Extra Filter
                bool allow = this.ReleaseTimeRequestExtraFilter(filters, evt);
                if (allow)
                {
                    results.Add(evt);
                }
            }

            query = results;
        }

        /// <summary>
        /// Filters the current query
        /// </summary>
        /// <param name="query">Basic query results</param>
        /// <returns>Results filtered</returns>
        private bool ReleaseTimeRequestExtraFilter(MyReleaseTimeReportsFilters filters, ReleaseTimeRequestModel item)
        {
            bool allow = true;
           
            // Status
            if (filters.ReleaseTimeRequestStatus.HasValue && item.ReleaseTimeRequestStatus != filters.ReleaseTimeRequestStatus)
            {
                allow = false;
            }

            return allow;
        }

        /// <summary>
        /// Gets Release Time Requested Days
        /// </summary>
        private float CalculateReleaseTimeDays(XrmServiceContext context, Entity releaseTimeRequest)
        {
            var ret = 0.0f;
            var schedules = (from sched in context.CreateQuery("oems_releasetimerequestschedule") where sched.GetAttributeValue<EntityReference>("oems_releasetimerequest").Id == releaseTimeRequest.GetAttributeValue<Guid>("oems_releasetimerequestid") select sched);
            foreach (var schedule in schedules)
            {
                var timePeridOption = (schedule.GetAttributeValue<OptionSetValue>("oems_releasetimeperiod")).Value;
                switch (timePeridOption)
                {
                    case 1: //Full Day
                        ret += 1.0f;
                        break;
                    case 2: //Half Day (AM)
                    case 3: //Half Day (PM)
                        ret += 0.5f;
                        break;
                }
            }

            return ret;
        }

        /// <summary>
        /// Gets Release Time Requested Extra Days
        /// </summary>
        private float CalculateReleaseTimeExtraDays(XrmServiceContext context, Entity releaseTimeRequest)
        {
            var ret = 0.0f;

            if (releaseTimeRequest.GetAttributeValue<bool>("oems_extrareleasetimerequiredflag"))
            {
                var extraSchedules = (from extraSch in context.CreateQuery("oems_extrareleasetimerequestschedule") where extraSch.GetAttributeValue<EntityReference>("oems_releasetimerequest").Id == releaseTimeRequest.GetAttributeValue<Guid>("oems_releasetimerequestid") select extraSch);
                foreach (var schedule in extraSchedules)
                {
                    var timePeridOption = (schedule.GetAttributeValue<OptionSetValue>("oems_releasetimeperiod")).Value;
                    switch (timePeridOption)
                    {
                        case 1: //Full Day
                            ret += 1.0f;
                            break;
                        case 2: //Half Day (AM)
                        case 3: //Half Day (PM)
                            ret += 0.5f;
                            break;
                    }
                }
            }

            return ret;
        }

        #endregion Release Time Functions

        #region Local Delegation Functions

        /// <summary>
        /// Gets local delegation reports data
        /// </summary>
        /// <returns></returns>
        public List<LocalDelegationModel> LocalDelegationGetData()
        {
            var basicResults = new List<BasicModel>();
            var results = new List<LocalDelegationModel>();
            using (var serviceContext = new XrmServiceContext())
            {
                #region Query

                var query =
                (
                    from reg in serviceContext.CreateQuery("oems_eventregistration")
                    join evt in serviceContext.CreateQuery("oems_event") on ((EntityReference)reg["oems_event"]).Id equals evt["oems_eventid"]
                    join sub in serviceContext.CreateQuery("oems_subcategory") on ((EntityReference)evt["oems_subcategory"]).Id equals sub["oems_subcategoryid"]
                    join cat in serviceContext.CreateQuery("oems_category") on ((EntityReference)sub["oems_category"]).Id equals cat["oems_categoryid"]
                    where reg.GetAttributeValue<EntityReference>("oems_contact").Id == this._contactId
                    where reg.GetAttributeValue<int>("statuscode") != RegistrationHelper.EventRegistration.Initial
                    select new
                    {
                        Event = new LocalDelegationModel()
                        {
                            EventStatus = (evt.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                            EventStatusText = GetEventStatusText((evt.GetAttributeValue<OptionSetValue>("statuscode")).Value, (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value, evt.Id),
                            EventCategoryId = (sub.GetAttributeValue<EntityReference>("oems_category")).Id,
                            EventCategoryName = cat.GetAttributeValue<string>("oems_categoryname"),
                            EventSubCategoryId = (evt.GetAttributeValue<EntityReference>("oems_subcategory")).Id,
                            EventSubCategoryName = sub.GetAttributeValue<string>("oems_subcategoryname"),
                            CourseTermId = evt.GetAttributeValue<EntityReference>("oems_courseterm") != null ? (evt.GetAttributeValue<EntityReference>("oems_courseterm")).Id : (Guid?)null,
                            EventId = evt.GetAttributeValue<Guid>("oems_eventid"),
                            EventTitle = evt.GetAttributeValue<string>("oems_eventname"),
                            StartDate = evt.GetAttributeValue<DateTime>("oems_startdate"),
                            EndDate = evt.GetAttributeValue<DateTime>("oems_enddate"),
                            RegistrationId = reg.GetAttributeValue<Guid?>("oems_eventregistrationid"),
                            RegistrationStatus = (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                            RegistrationStatusText = EntityOptionSet.GetOptionSetLabel("oems_eventregistration", "statuscode", (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value),
                            RegistrationStartDate = evt.GetAttributeValue<DateTime?>("oems_registrationopening"),
                            RegistrationEndDate = evt.GetAttributeValue<DateTime?>("oems_registrationdeadline")
                        },
                    }
                ).ToList();
                query.ForEach(q => basicResults.Add(q.Event));

                #endregion

                //Filter the query
                this.BasicFilter(ref basicResults);

                //Local delegation filter
                var serializedResults = JsonConvert.SerializeObject(basicResults);
                results = JsonConvert.DeserializeObject<List<LocalDelegationModel>>(serializedResults);
                
                //Gets extra values
                this.LocalDelegationExtraFilter(serviceContext, ref results);
            }

            this.Count = results.Count();
            return results;
        }

        /// <summary>
        /// Filters the current query
        /// </summary>
        /// <param name="query">Basic query results</param>
        /// <returns>Results filtered</returns>
        private void LocalDelegationExtraFilter(XrmServiceContext context, ref List<LocalDelegationModel> query)
        {
            LocalDelegationReportsFilters filters = (LocalDelegationReportsFilters)_myFilters;
            var results = new List<LocalDelegationModel>();
            foreach (var evt in query)
            {
                var localDelegation = (from localDel in context.CreateQuery("oems_eventlocal") where localDel.GetAttributeValue<EntityReference>("oems_event").Id == evt.EventId.Value select localDel).FirstOrDefault();
                if (localDelegation != null && (!filters.LocalDelegationStatus.HasValue || (localDelegation.GetAttributeValue<OptionSetValue>("statuscode")).Value == filters.LocalDelegationStatus))
                {
                    evt.LocalDelegationId = localDelegation.GetAttributeValue<Guid>("oems_eventlocalid");
                    evt.LocalDelegationName = localDelegation.GetAttributeValue<string>("oems_eventlocalname");
                    evt.LocalDelegationStatus = (localDelegation.GetAttributeValue<OptionSetValue>("statuscode")).Value;
                    evt.LocalDelegationStatusText = EntityOptionSet.GetOptionSetLabel("oems_eventlocal", "statuscode", (localDelegation.GetAttributeValue<OptionSetValue>("statuscode")).Value);

                    results.Add(evt);
                }
            }

            query = results;
        }

        #endregion Local Delegation Functions

        #region My Receipts

        /// <summary>
        /// Gets my cource receipt reports data
        /// </summary>
        /// <returns></returns>
        public List<MyCourseReceiptModel> MyCourseReceiptGetData()
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var receipts =
                (
                    from br in serviceContext.CreateQuery("oems_billingrequest")
                    join reg in serviceContext.CreateQuery("oems_eventregistration") on br.GetAttributeValue<EntityReference>("oems_eventregistration").Id equals reg["oems_eventregistrationid"]
                    join evt in serviceContext.CreateQuery("oems_event") on ((EntityReference)reg["oems_event"]).Id equals evt["oems_eventid"]
                    join term in serviceContext.CreateQuery("oems_event") on ((EntityReference)evt["oems_courseterm"]).Id equals term["oems_eventid"]
                    where reg.GetAttributeValue<EntityReference>("oems_contact").Id == this._contactId
                    where evt.GetAttributeValue<EntityReference>("oems_courseterm") != null
                    where br.GetAttributeValue<int>("oems_paymentstatus") == BillingService.PaymentStatus.Paid
                    select new MyCourseReceiptModel()
                    {
                        TermName = term.GetAttributeValue<string>("oems_eventname"),
                        EventName = evt.GetAttributeValue<string>("oems_eventname"),
                        StartDate = evt.GetAttributeValue<DateTime>("oems_startdate"),
                        EndDate = evt.GetAttributeValue<DateTime>("oems_enddate"),
                        Price = br.GetAttributeValue<decimal>("oems_totalamountdue")
                    }
                ).ToList();

                return receipts;
            }
        }

        /// <summary>
        /// Gets my cource receipt reports data
        /// </summary>
        /// <returns></returns>
        public List<MyEventReceiptModel> MyEventsReceiptGetData()
        {
            using (var serviceContext = new XrmServiceContext())
            {
                try
                {
                    var receipts =
                    (
                        from br in serviceContext.CreateQuery("oems_billingrequest")
                        join tx in serviceContext.CreateQuery("oems_cctransaction") on br.GetAttributeValue<EntityReference>("oems_paymenttransaction").Id equals tx["oems_cctransactionid"]
                        join reg in serviceContext.CreateQuery("oems_eventregistration") on br.GetAttributeValue<EntityReference>("oems_eventregistration").Id equals reg["oems_eventregistrationid"]
                        join evt in serviceContext.CreateQuery("oems_event") on ((EntityReference)reg["oems_event"]).Id equals evt["oems_eventid"]
                        join loc in serviceContext.CreateQuery("oems_location") on ((EntityReference)evt["oems_eventlocation"]).Id equals loc["oems_locationid"]
                        where reg.GetAttributeValue<EntityReference>("oems_contact").Id == this._contactId
                        where !evt.GetAttributeValue<bool>("oems_courseflag") && !evt.GetAttributeValue<bool>("oems_coursetermflag") 
                            && (evt.GetAttributeValue<OptionSetValue>("statuscode")).Value == RegistrationHelper.EventStatus.Completed
                        where (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value == RegistrationHelper.EventRegistration.Approved
                        where br.GetAttributeValue<int>("oems_paymentstatus") != BillingService.PaymentStatus.Cancelled && br.GetAttributeValue<int>("oems_paymentstatus") != BillingService.PaymentStatus.Unpaid
                        select new MyEventReceiptModel()
                        {
                            EventName = evt.GetAttributeValue<string>("oems_eventname"),
                            StartDate = evt.GetAttributeValue<DateTime>("oems_startdate"),
                            EndDate = evt.GetAttributeValue<DateTime>("oems_enddate"),
                            EventLocation = loc.GetAttributeValue<string>("oems_locationname"),
                            BillingStatus = EntityOptionSet.GetOptionSetLabel("oems_billingrequest", "oems_paymentstatus", (br.GetAttributeValue<OptionSetValue>("oems_paymentstatus")).Value),
                            BillingType = EntityOptionSet.GetOptionSetLabel("oems_cctransaction", "oems_paymentmethod", (tx.GetAttributeValue<OptionSetValue>("oems_paymentmethod")).Value),
                            BillingAmount = br.GetAttributeValue<decimal>("oems_totalamountdue"),
                            BillingPaymentDate = tx.GetAttributeValue<DateTime?>("oems_paymentdate")
                        }
                    ).ToList();

                    return receipts;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }


        #endregion

        #region Instructor Course History

        /// <summary>
        /// Gets release time requests reports data
        /// </summary>
        /// <returns></returns>
        public List<InstructorCourseHistoryModel> InstructorCourseHistoryGetData()
        {
            var results = new List<InstructorCourseHistoryModel>();
            using (var serviceContext = new XrmServiceContext())
            {
                var coursesInfo = new Dictionary<Guid, InstructorCourseHistoryModel>();
                var basicResults = new List<BasicModel>();
                var query =
                (
                    from evt in serviceContext.CreateQuery("oems_event")
                    join reg in serviceContext.CreateQuery("oems_eventregistration") on ((EntityReference)evt["oems_courseofferingapplication"]).Id equals reg["oems_eventregistrationid"]
                    join term in serviceContext.CreateQuery("oems_event") on ((EntityReference)evt["oems_courseterm"]).Id equals term["oems_eventid"]
                    join sub in serviceContext.CreateQuery("oems_subcategory") on ((EntityReference)evt["oems_subcategory"]).Id equals sub["oems_subcategoryid"]
                    join cat in serviceContext.CreateQuery("oems_category") on ((EntityReference)sub["oems_category"]).Id equals cat["oems_categoryid"]
                    where reg.GetAttributeValue<EntityReference>("oems_contact").Id == this._contactId
                    where evt.GetAttributeValue<EntityReference>("oems_courseterm") != null
                    where evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.PublishedApplication
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.CloseApplication
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.PublishedRegistration
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.OpenRegistration
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.CloseRegistration
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.StartEvent
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.EndEvent
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.ExceededCapacity
                    select new
                    {
                        Course = new BasicModel()
                        {
                            EventStatus = (evt.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                            EventStatusText = GetEventStatusText((evt.GetAttributeValue<OptionSetValue>("statuscode")).Value, (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value, evt.Id),
                            EventCategoryId = (sub.GetAttributeValue<EntityReference>("oems_category")).Id,
                            EventCategoryName = cat.GetAttributeValue<string>("oems_categoryname"),
                            EventSubCategoryId = (evt.GetAttributeValue<EntityReference>("oems_subcategory")).Id,
                            EventSubCategoryName = sub.GetAttributeValue<string>("oems_subcategoryname"),
                            CourseTermId = evt.GetAttributeValue<EntityReference>("oems_courseterm") != null ? (evt.GetAttributeValue<EntityReference>("oems_courseterm")).Id : (Guid?)null,
                            EventId = evt.GetAttributeValue<Guid>("oems_eventid"),
                            EventTitle = evt.GetAttributeValue<string>("oems_eventname"),
                            StartDate = evt.GetAttributeValue<DateTime>("oems_startdate"),
                            EndDate = evt.GetAttributeValue<DateTime>("oems_enddate"),
                            RegistrationId = reg.GetAttributeValue<Guid?>("oems_eventregistrationid"),
                            RegistrationStatus = (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                            RegistrationStatusText = EntityOptionSet.GetOptionSetLabel("oems_eventregistration", "statuscode", (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value),
                            RegistrationStartDate = evt.GetAttributeValue<DateTime?>("oems_registrationopening"),
                            RegistrationEndDate = evt.GetAttributeValue<DateTime?>("oems_registrationdeadline")
                        },
                        CourseDetail = new InstructorCourseHistoryModel()
                        {
                            CourseTermName = term.GetAttributeValue<string>("oems_eventname"),
                            CourseTermCode = term.GetAttributeValue<string>("oems_webtermdescription"),
                            CourseTitle = reg.GetAttributeValue<string>("oems_coursetitle"),
                            CourseDescription = evt.GetAttributeValue<string>("oems_eventdescription")
                        }
                    }
                ).ToList();
                query.ForEach(q => 
                {
                    basicResults.Add(q.Course);
                    coursesInfo[q.Course.EventId.Value] = q.CourseDetail;
                });

                //Filter the query
                this.BasicFilter(ref basicResults);

                //Serialize results into EvetLitings
                var serializedResults = JsonConvert.SerializeObject(basicResults);
                results = JsonConvert.DeserializeObject<List<InstructorCourseHistoryModel>>(serializedResults);

                results.ForEach(r =>
                {
                    r.CourseTermName = coursesInfo[r.EventId.Value].CourseTermName;
                    r.CourseTermCode = coursesInfo[r.EventId.Value].CourseTermCode;
                    r.CourseTitle = coursesInfo[r.EventId.Value].CourseTitle;
                    if (!string.IsNullOrWhiteSpace(coursesInfo[r.EventId.Value].CourseDescription))
                    {
                        r.CourseDescription = coursesInfo[r.EventId.Value].CourseDescription.Length > 150 ? coursesInfo[r.EventId.Value].CourseDescription.Substring(0, 150) : coursesInfo[r.EventId.Value].CourseDescription;
                    }
                });
            }

            this.Count = results.Count(); 
            return results;
        }

        #endregion

        #region Instructor Course List

        /// <summary>
        /// Gets release time requests reports data
        /// </summary>
        /// <returns></returns>
        public List<InstructorCourseListModel> InstructorCourseListGetData()
        {
            var results = new List<InstructorCourseListModel>();
            using (var serviceContext = new XrmServiceContext())
            {
                var coursesInfo = new Dictionary<Guid, InstructorCourseListModel>();
                var basicResults = new List<BasicModel>();
                var query =
                (
                    from evt in serviceContext.CreateQuery("oems_event")
                    join reg in serviceContext.CreateQuery("oems_eventregistration") on ((EntityReference)evt["oems_courseofferingapplication"]).Id equals reg["oems_eventregistrationid"]
                    join term in serviceContext.CreateQuery("oems_event") on ((EntityReference)evt["oems_courseterm"]).Id equals term["oems_eventid"]
                    join sub in serviceContext.CreateQuery("oems_subcategory") on ((EntityReference)evt["oems_subcategory"]).Id equals sub["oems_subcategoryid"]
                    join cat in serviceContext.CreateQuery("oems_category") on ((EntityReference)sub["oems_category"]).Id equals cat["oems_categoryid"]
                    join loc in serviceContext.CreateQuery("oems_courselocation") on ((EntityReference)evt["oems_courselocation"]).Id equals loc["oems_courselocationid"]
                    join ds in serviceContext.CreateQuery("oems_coursedateslots") on ((EntityReference)evt["oems_coursedateslot"]).Id equals ds["oems_coursedateslotsid"]
                    join ts in serviceContext.CreateQuery("oems_coursetimeslot") on ((EntityReference)evt["oems_coursetimeslot"]).Id equals ts["oems_coursetimeslotid"]
                    where reg.GetAttributeValue<EntityReference>("oems_contact").Id == this._contactId
                    where evt.GetAttributeValue<EntityReference>("oems_courseterm") != null && evt.GetAttributeValue<bool>("oems_courselistreportavailableflag")
                    select new
                    {
                        Course = new BasicModel()
                        {
                            EventStatus = (evt.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                            EventStatusText = GetEventStatusText((evt.GetAttributeValue<OptionSetValue>("statuscode")).Value, (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value, evt.Id),
                            EventCategoryId = (sub.GetAttributeValue<EntityReference>("oems_category")).Id,
                            EventCategoryName = cat.GetAttributeValue<string>("oems_categoryname"),
                            EventSubCategoryId = (evt.GetAttributeValue<EntityReference>("oems_subcategory")).Id,
                            EventSubCategoryName = sub.GetAttributeValue<string>("oems_subcategoryname"),
                            CourseTermId = evt.GetAttributeValue<EntityReference>("oems_courseterm") != null ? (evt.GetAttributeValue<EntityReference>("oems_courseterm")).Id : (Guid?)null,
                            EventId = evt.GetAttributeValue<Guid>("oems_eventid"),
                            EventTitle = evt.GetAttributeValue<string>("oems_eventname"),
                            StartDate = evt.GetAttributeValue<DateTime>("oems_startdate"),
                            EndDate = evt.GetAttributeValue<DateTime>("oems_enddate"),
                            RegistrationId = reg.GetAttributeValue<Guid?>("oems_eventregistrationid"),
                            RegistrationStatus = (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                            RegistrationStatusText = EntityOptionSet.GetOptionSetLabel("oems_eventregistration", "statuscode", (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value),
                            RegistrationStartDate = evt.GetAttributeValue<DateTime?>("oems_registrationopening"),
                            RegistrationEndDate = evt.GetAttributeValue<DateTime?>("oems_registrationdeadline"),
                        },
                        CourseDetail = new InstructorCourseListModel()
                        {
                            CourseTermName = term.GetAttributeValue<string>("oems_eventname"),
                            CourseTermCode = term.GetAttributeValue<string>("oems_webtermdescription"),
                            CourseTitle = reg.GetAttributeValue<string>("oems_coursetitle"),
                            CourseCode = evt.GetAttributeValue<string>("oems_coursecode"),
                            CourseDates = ds.GetAttributeValue<DateTime>("oems_startdate").ToString("d", CultureInfo.CurrentCulture) + " - " + ds.GetAttributeValue<DateTime>("oems_enddate").ToString("d", CultureInfo.CurrentCulture),
                            CourseTimes = ts.GetAttributeValue<string>("oems_starttime") + " - " + ts.GetAttributeValue<string>("oems_endtime"),
                            CourseLocation = loc.GetAttributeValue<string>("oems_name")
                        }
                    }
                ).ToList();
                query.ForEach(q =>
                {
                    basicResults.Add(q.Course);
                    coursesInfo[q.Course.EventId.Value] = q.CourseDetail;
                });

                //Filter the query
                this.BasicFilter(ref basicResults);

                //Serialize results into EvetLitings
                var serializedResults = JsonConvert.SerializeObject(basicResults);
                results = JsonConvert.DeserializeObject<List<InstructorCourseListModel>>(serializedResults);

                results.ForEach(r =>
                {
                    r.CourseTermName = coursesInfo[r.EventId.Value].CourseTermName;
                    r.CourseTermCode = coursesInfo[r.EventId.Value].CourseTermCode;
                    r.CourseTitle = coursesInfo[r.EventId.Value].CourseTitle;
                    r.CourseCode = coursesInfo[r.EventId.Value].CourseCode;
                    r.CourseDates = coursesInfo[r.EventId.Value].CourseDates;
                    r.CourseTimes = coursesInfo[r.EventId.Value].CourseTimes;
                    r.CourseLocation = coursesInfo[r.EventId.Value].CourseLocation;
                });
            }

            this.Count = results.Count();
            return results;
        }

        /// <summary>
        /// Gets course attendees
        /// </summary>
        /// <returns></returns>
        public List<AttendeeModel> GetCourseAttendees(Guid courseId)
        {
            var results = new List<AttendeeModel>();
            using (var serviceContext = new XrmServiceContext())
            {
                var query =
                (
                    from reg in serviceContext.CreateQuery("oems_eventregistration")
                    join evt in serviceContext.CreateQuery("oems_event") on ((EntityReference)reg["oems_event"]).Id equals evt["oems_eventid"]
                    join con in serviceContext.CreateQuery("contact") on ((EntityReference)reg["oems_contact"]).Id equals (Guid)con["contactid"]
                    join sb in serviceContext.CreateQuery("oems_schoolboard") on ((EntityReference)reg["oems_schoolboard"]).Id equals sb["oems_schoolboardid"]
                    where evt.GetAttributeValue<Guid>("oems_eventid") == courseId
                    //where con.GetAttributeValue<Guid>("contactid") != this._contactId
                    where reg.GetAttributeValue<OptionSetValue>("oems_registrationtype") != null && reg.GetAttributeValue<OptionSetValue>("oems_registrationtype").Value == RegistrationHelper.EventRegistrationType.Attendee
                    where reg.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventRegistration.Sumbitted || reg.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventRegistration.Approved
                    select new
                    {
                        Attendee = new AttendeeModel()
                        {
                            UserId = con.GetAttributeValue<Guid>("contactid"),
                            Name = con.GetAttributeValue<string>("fullname"),
                            OrderName = con.GetAttributeValue<string>("fullname"),
                            Mail = con.GetAttributeValue<string>("emailaddress1"),
                            SchoolBoard = sb.GetAttributeValue<string>("oems_schoolboardname"),
                            NumberOfYearsTeaching = reg.GetAttributeValue<int>("oems_yearsofexperience").ToString(),
                            CurrentAssignment = reg.GetAttributeValue<string>("oems_currentteachingassignment"),
                            FutureTeachingAssignment = reg.GetAttributeValue<string>("oems_futureteachingassignment")
                        }
                    }
                ).ToList();

                int i = 1;
                query.ForEach(q =>
                {
                    q.Attendee.Number = i;
                    results.Add(q.Attendee);
                    ++i;
                });
                results.Insert(i - 1, new AttendeeModel() { OrderName = "zzzzzzz" });
            }

            this.Count = results.Count();
            return results;
        }

        #endregion

        #region Instructor Course List Attendance

        /// <summary>
        /// Gets release time requests reports data
        /// </summary>
        /// <returns></returns>
        public List<InstructorCourseListModel> InstructorCourseListAttendanceGetData()
        {
            var results = new List<InstructorCourseListModel>();
            using (var serviceContext = new XrmServiceContext())
            {
                var coursesInfo = new Dictionary<Guid, InstructorCourseListModel>();
                var basicResults = new List<BasicModel>();
                var query =
                (
                    from evt in serviceContext.CreateQuery("oems_event")
                    join reg in serviceContext.CreateQuery("oems_eventregistration") on ((EntityReference)evt["oems_courseofferingapplication"]).Id equals reg["oems_eventregistrationid"]
                    join term in serviceContext.CreateQuery("oems_event") on ((EntityReference)evt["oems_courseterm"]).Id equals term["oems_eventid"]
                    join sub in serviceContext.CreateQuery("oems_subcategory") on ((EntityReference)evt["oems_subcategory"]).Id equals sub["oems_subcategoryid"]
                    join cat in serviceContext.CreateQuery("oems_category") on ((EntityReference)sub["oems_category"]).Id equals cat["oems_categoryid"]
                    join loc in serviceContext.CreateQuery("oems_courselocation") on ((EntityReference)evt["oems_courselocation"]).Id equals loc["oems_courselocationid"]
                    join ds in serviceContext.CreateQuery("oems_coursedateslots") on ((EntityReference)evt["oems_coursedateslot"]).Id equals ds["oems_coursedateslotsid"]
                    join ts in serviceContext.CreateQuery("oems_coursetimeslot") on ((EntityReference)evt["oems_coursetimeslot"]).Id equals ts["oems_coursetimeslotid"]
                    where reg.GetAttributeValue<EntityReference>("oems_contact").Id == this._contactId
                    where evt.GetAttributeValue<EntityReference>("oems_courseterm") != null && evt.GetAttributeValue<bool>("oems_instructorcourseattendancelistavailable")
                    select new
                    {
                        Course = new BasicModel()
                        {
                            EventStatus = (evt.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                            EventStatusText = GetEventStatusText((evt.GetAttributeValue<OptionSetValue>("statuscode")).Value, (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value, evt.Id),
                            EventCategoryId = (sub.GetAttributeValue<EntityReference>("oems_category")).Id,
                            EventCategoryName = cat.GetAttributeValue<string>("oems_categoryname"),
                            EventSubCategoryId = (evt.GetAttributeValue<EntityReference>("oems_subcategory")).Id,
                            EventSubCategoryName = sub.GetAttributeValue<string>("oems_subcategoryname"),
                            CourseTermId = evt.GetAttributeValue<EntityReference>("oems_courseterm") != null ? (evt.GetAttributeValue<EntityReference>("oems_courseterm")).Id : (Guid?)null,
                            EventId = evt.GetAttributeValue<Guid>("oems_eventid"),
                            EventTitle = evt.GetAttributeValue<string>("oems_eventname"),
                            StartDate = evt.GetAttributeValue<DateTime>("oems_startdate"),
                            EndDate = evt.GetAttributeValue<DateTime>("oems_enddate"),
                            RegistrationId = reg.GetAttributeValue<Guid?>("oems_eventregistrationid"),
                            RegistrationStatus = (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                            RegistrationStatusText = EntityOptionSet.GetOptionSetLabel("oems_eventregistration", "statuscode", (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value),
                            RegistrationStartDate = evt.GetAttributeValue<DateTime?>("oems_registrationopening"),
                            RegistrationEndDate = evt.GetAttributeValue<DateTime?>("oems_registrationdeadline"),
                        },
                        CourseDetail = new InstructorCourseListModel()
                        {
                            CourseTermName = term.GetAttributeValue<string>("oems_eventname"),
                            CourseTermCode = term.GetAttributeValue<string>("oems_webtermdescription"),
                            CourseTitle = reg.GetAttributeValue<string>("oems_coursetitle"),
                            CourseCode = evt.GetAttributeValue<string>("oems_coursecode"),
                            CourseDates = ds.GetAttributeValue<DateTime>("oems_startdate").ToString("d", CultureInfo.CurrentCulture) + " - " + ds.GetAttributeValue<DateTime>("oems_enddate").ToString("d", CultureInfo.CurrentCulture),
                            CourseTimes = ts.GetAttributeValue<string>("oems_starttime") + " - " + ts.GetAttributeValue<string>("oems_endtime"),
                            CourseLocation = loc.GetAttributeValue<string>("oems_name")
                        }
                    }
                ).ToList();
                query.ForEach(q =>
                {
                    basicResults.Add(q.Course);
                    coursesInfo[q.Course.EventId.Value] = q.CourseDetail;
                });

                //Filter the query
                this.BasicFilter(ref basicResults);

                //Serialize results into EvetLitings
                var serializedResults = JsonConvert.SerializeObject(basicResults);
                results = JsonConvert.DeserializeObject<List<InstructorCourseListModel>>(serializedResults);

                results.ForEach(r =>
                {
                    r.CourseTermName = coursesInfo[r.EventId.Value].CourseTermName;
                    r.CourseTermCode = coursesInfo[r.EventId.Value].CourseTermCode;
                    r.CourseTitle = coursesInfo[r.EventId.Value].CourseTitle;
                    r.CourseCode = coursesInfo[r.EventId.Value].CourseCode;
                    r.CourseDates = coursesInfo[r.EventId.Value].CourseDates;
                    r.CourseTimes = coursesInfo[r.EventId.Value].CourseTimes;
                    r.CourseLocation = coursesInfo[r.EventId.Value].CourseLocation;
                });
            }

            this.Count = results.Count();
            return results;
        }

        /// <summary>
        /// Gets course attendees
        /// </summary>
        /// <returns></returns>
        public List<AttendeeDetailModel> GetCourseAttendance(Guid courseId)
        {
            var results = new List<AttendeeDetailModel>();
            using (var serviceContext = new XrmServiceContext())
            {
                var query =
                (
                    from reg in serviceContext.CreateQuery("oems_eventregistration")
                    join evt in serviceContext.CreateQuery("oems_event") on ((EntityReference)reg["oems_event"]).Id equals evt["oems_eventid"]
                    join con in serviceContext.CreateQuery("contact") on ((EntityReference)reg["oems_contact"]).Id equals (Guid)con["contactid"]
                    join sb in serviceContext.CreateQuery("oems_schoolboard") on ((EntityReference)reg["oems_schoolboard"]).Id equals sb["oems_schoolboardid"]
                    where evt.GetAttributeValue<Guid>("oems_eventid") == courseId
                    //where con.GetAttributeValue<Guid>("contactid") != this._contactId
                    where reg.GetAttributeValue<OptionSetValue>("oems_registrationtype") != null && reg.GetAttributeValue<OptionSetValue>("oems_registrationtype").Value == RegistrationHelper.EventRegistrationType.Attendee
                    where reg.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventRegistration.Sumbitted || reg.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventRegistration.Approved
                    select new
                    {
                        Attendee = new AttendeeDetailModel()
                        {
                            Name = con.GetAttributeValue<string>("fullname"),
                            OrderName = con.GetAttributeValue<string>("fullname"),
                            SchoolBoard = sb.GetAttributeValue<string>("oems_schoolboardname"),
                            AttendanceDatesList = new List<string>(),
                            AttendanceDates = string.Empty
                        }
                    }
                ).ToList();

                int i = 1;
                query.ForEach(q =>
                {
                    q.Attendee.Number = i;

                    ////Dates will be empty and the Instructor will then manually record the attendance
                    //for (int j = 0; j < dates?; ++j)
                    //{
                    //    if (j != 0)
                    //    {
                    //        q.Attendee.AttendanceDates += " - ";
                    //    }

                    //    q.Attendee.AttendanceDates += d.ToString("ddd MMM d YYYY", CultureInfo.CurrentCulture);
                    //}

                    results.Add(q.Attendee);
                    ++i;
                });
                results.Insert(i - 1, new AttendeeDetailModel() { OrderName = "zzzzzzz" });
            }

            this.Count = results.Count();
            return results;
        }

        #endregion

    }
}