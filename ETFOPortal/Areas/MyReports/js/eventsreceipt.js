﻿(function (eventsreceipt, $, undefined) 
{
    var grid;

    $(document).ajaxStop(function ()
    {
        AjaxUpdateProgressHide();
    });

    $(".export-xls").click(function ()
    {
        grid.saveAsExcel();
    })

    function loadResults()
    {       
        AjaxUpdateProgressShow();

        var url = urlSearch;
        if (grid == null)
        {
            grid = $("#my-reports-grid").kendoGrid
            ({
                //toolbar: ["excel", "pdf"],
                excel:
                {
                    allPages: true,
                    fileName: "My Events Receipt.xlsx",
                    proxyURL: urlProxy
                },
                pdf:
                {
                    allPages: true,
                    fileName: "My Events Receipt.pdf",
                    proxyURL: urlProxy
                },
                dataSource:
                {
                    type: "json",
                    transport:
                    {
                        read:
                        {
                            url: url,
                            type: "POST"//,
                            //data: data
                        }
                    },
                    schema:
                    {
                        data: "data",
                        total: "total",
                        model:     
                        {
                            fields:
                            {
                                EventName: { type: "string" },
                                StartDate: { type: "date" },
                                EndDate: { type: "date" },
                                EventLocation: { type: "string" },
                                BillingStatus: { type: "string" },
                                BillingType: { type: "string" },
                                BillingAmount: { type: "number" },
                                BillingPaymentDate: { type: "date" }
                            }
                        }
                    },
                    sort:
                    {
                        field: "EventName",
                        dir: "asc"
                    },
                    pageSize: 10,
                    serverPaging: false,
                    serverFiltering: false,
                    serverSorting: false,
                },
                sortable:
                {
                    mode: "single", // "multiple"
                    allowUnsort: false // "true"
                },
                filterable: false,
                pageable: true,
                columns: 
                [
                    {
                        field: "EventName",
                        title: "Event",
                        width: "175px"
                    },
                    {
                        field: "StartDate",
                        title: "Event Start Date",
                        format: "{0:dd/MM/yyyy}",
                        width: "150px"
                    },
                    {
                        field: "EndDate",
                        title: "Event End Date",
                        format: "{0:dd/MM/yyyy}",
                        width: "150px"
                    },
                    {
                        field: "EventLocation",
                        title: "Event Location",
                        width: "175px"
                    },
                    {
                        field: "BillingStatus",
                        title: "Billing Status",
                        width: "100px"
                    },
                    {
                        field: "BillingType",
                        title: "Billing Type",
                        width: "100px"
                    },
                    {
                        field: "BillingAmount",
                        title: "Amount",
                        format:"{0:c2}",
                        width: "120px"
                    },
                    {
                        field: "BillingPaymentDate",
                        title: "Payment Date",
                        format: "{0:dd/MM/yyyy}",
                        width: "120px"
                    }
                ],
                resizable: true
            }).data("kendoGrid");
        }
        else
        {
            grid.dataSource.transport.options.read.data = data;
            grid.dataSource.read();
        }
    }

    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                          PUBLIC FUNCTIONS
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    eventsreceipt.renderForm = function ()
    {
        loadResults();
        $("#report-title").hide();
    }

}(window.eventsreceipt = window.eventsreceipt || {}, jQuery));