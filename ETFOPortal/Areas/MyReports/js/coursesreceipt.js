﻿(function (coursesreceipt, $, undefined) 
{
    var grid;

    $(document).ajaxStop(function ()
    {
        AjaxUpdateProgressHide();
    });

    $(".export-xls").click(function ()
    {
        grid.saveAsExcel();
    })

    function loadResults()
    {       
        AjaxUpdateProgressShow();

        var url = urlSearch;
        if (grid == null)
        {
            grid = $("#my-reports-grid").kendoGrid
            ({
                //toolbar: ["excel", "pdf"],
                excel:
                {
                    allPages: true,
                    fileName: "My Course Receipt.xlsx",
                    proxyURL: urlProxy
                },
                pdf:
                {
                    allPages: true,
                    fileName: "My Course Receipt.pdf",
                    proxyURL: urlProxy
                },
                dataSource:
                {
                    type: "json",
                    transport:
                    {
                        read:
                        {
                            url: url,
                            type: "POST"//,
                            //data: data
                        }
                    },
                    schema:
                    {
                        data: "data",
                        total: "total",
                        model:     
                        {
                            fields:
                            {
                                TermName: { type: "string" },
                                EventName: { type: "string" },
                                StartDate: { type: "date" },
                                EndDate: { type: "date" },
                                Price: { type: "number" }
                            }
                        }
                    },
                    sort:
                    {
                        field: "EventName",
                        dir: "asc"
                    },
                    pageSize: 10,
                    serverPaging: false,
                    serverFiltering: false,
                    serverSorting: false,
                },
                sortable:
                {
                    mode: "single", // "multiple"
                    allowUnsort: false // "true"
                },
                filterable: false,
                pageable: true,
                columns: 
                [
                    {
                        field: "TermName",
                        title: "Term",
                        width: "175px"
                    },
                    {
                        field: "EventName",
                        title: "Course",
                        width: "220px"
                    },
                    {
                        field: "StartDate",
                        title: "Course Start Date",
                        format: "{0:dd/MM/yyyy}",
                        width: "150px"
                    },
                    {
                        field: "EndDate",
                        title: "Course End Date",
                        format: "{0:dd/MM/yyyy}",
                        width: "150px"
                    },
                    {
                        field: "Price",
                        title: "Price Paid",
                        format:"{0:c2}",
                        width: "120px"
                    }
                ]
            }).data("kendoGrid");
        }
        else
        {
            grid.dataSource.transport.options.read.data = data;
            grid.dataSource.read();
        }
    }

    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                                          PUBLIC FUNCTIONS
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    coursesreceipt.renderForm = function ()
    {
        loadResults();
        $("#report-title").hide();
    }

}(window.coursesreceipt = window.coursesreceipt || {}, jQuery));