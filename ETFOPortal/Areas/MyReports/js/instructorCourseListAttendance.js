﻿(function (instructorCourseListAttendance, $, undefined) 
{
    // used to wait for the children to finish async export
    var detailExportPromises = [];
    var grid;

    $(document).ajaxStop(function ()
    {
        AjaxUpdateProgressHide();
    });

    //form
    $("#search-tg").click
    (
        function ()
        {
            loadResults();
        }
    );

    function loadResults()
    {
        AjaxUpdateProgressShow();

        detailExportPromises = [];
        var url = urlSearch;

        var eventStartDate = kendo.toString($("#filter-form input[name='eventStartDate']").data("kendoDatePicker").value(), 'd');
        var eventEndDate = kendo.toString($("#filter-form input[name='eventEndDate']").data("kendoDatePicker").value(), 'd');
        var registrationStartDate = kendo.toString($("#filter-form input[name='registrationStartDate']").data("kendoDatePicker").value(), 'd');
        var registrationEndDate = kendo.toString($("#filter-form input[name='registrationEndDate']").data("kendoDatePicker").value(), 'd');

        var data =
        {
            eventName: $("#filter-form input[name='eventName']").val(),
            eventStatus: $("#filter-form select[name='eventStatus']").data("kendoDropDownList").value(),
            category: $("#filter-form select[name='category']").data("kendoDropDownList").value(),
            subcategory: $("#filter-form select[name='subcategory']").data("kendoDropDownList").value(),
            eventStartDate: eventStartDate,
            eventEndDate: eventEndDate,
            registrationStatus: $("#filter-form select[name='registrationStatus']").data("kendoDropDownList").value(),
            registrationStartDate: registrationStartDate,
            registrationEndDate: registrationEndDate
        };

        if (grid == null)
        {
            grid = $("#my-reports-grid").kendoGrid
            ({
                toolbar: ["excel"],//, "pdf"],
                excel:
                {
                    allPages: true,
                    fileName: "Instructor Course List Report.xlsx",
                    proxyURL: urlProxy
                },
                //pdf:
                //{
                //    allPages: true,
                //    fileName: "Instructor Course List Attendance Report.pdf",
                //    proxyURL: urlProxy
                //},
                dataBound: function ()
                {
                    detailExportPromises = [];
                },
                excelExport: function (e)
                {
                    e.preventDefault();

                    var workbook = e.workbook;

                    detailExportPromises = [];

                    var masterData = e.data;
                    for (var rowIndex = 0; rowIndex < masterData.length; rowIndex++)
                    {
                        exportChildData(masterData[rowIndex].EventId, rowIndex);
                    }

                    $.when.apply(null, detailExportPromises)
                    .then(function ()
                    {
                        // get the export results
                        var detailExports = $.makeArray(arguments);

                        // sort by masterRowIndex
                        detailExports.sort(function (a, b)
                        {
                            return a.masterRowIndex - b.masterRowIndex;
                        });

                        // add an empty column
                        workbook.sheets[0].columns.unshift(
                        {
                            width: 30
                        });

                        // prepend an empty cell to each row
                        for (var i = 0; i < workbook.sheets[0].rows.length; i++)
                        {
                            var row = workbook.sheets[0].rows[i];
                            row.cells.unshift({});

                            // prepend an empty cell to each row
                            for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex++)
                            {
                                row.cells[cellIndex].bold = true;
                            }
                        }

                        // merge the detail export sheet rows with the master sheet rows
                        // loop backwards so the masterRowIndex doesn't need to be updated
                        for (var i = detailExports.length - 1; i >= 0; i--)
                        {
                            var masterRowIndex = detailExports[i].masterRowIndex + 1; // compensate for the header row

                            var sheet = detailExports[i].sheet;

                            // prepend an empty cell to each row
                            for (var ci = 0; ci < sheet.rows.length; ci++)
                            {
                                var row = sheet.rows[ci];
                                if (row.cells[0].value)
                                {
                                    row.cells.unshift({});
                                    for (var cellIndex = 1; cellIndex < row.cells.length; cellIndex++)
                                    {
                                        if (ci === 0)
                                        {
                                            row.cells[cellIndex].background = "#aabbcc";
                                        }
                                        else
                                        {
                                            row.cells[cellIndex].background = "#D4D4D4";
                                        }
                                    }
                                }
                            }

                            // insert the detail sheet rows after the master row
                            //if (sheet.rows.length > 1)
                            //{
                                [].splice.apply(workbook.sheets[0].rows, [masterRowIndex + 1, 0].concat(sheet.rows));
                            //}
                        }

                        // save the workbook
                        kendo.saveAs(
                        {
                            dataURI: new kendo.ooxml.Workbook(workbook).toDataURL(),
                            fileName: "Instructor Course List Attendance.xlsx"
                        });
                    });
                },
                dataSource:
                {
                    type: "json",
                    transport:
                    {
                        read:
                        {
                            url: url,
                            type: "POST",
                            data: data
                        }
                    },
                    schema:
                    {
                        data: "data",
                        total: "total",
                        model:
                        {
                            fields:
                            {
                                EventStatusText: { type: "string" },
                                EventCategoryName: { type: "string" },
                                EventSubCategoryName: { type: "string" },
                                EventTitle: { type: "string" },
                                CourseTitle: { type: "string" },
                                CourseCode: { type: "string" },
                                CourseDates: { type: "string" },
                                CourseTimes: { type: "string" },
                                CourseLocation: { type: "string" }
                            }
                        }
                    },
                    sort:
                    {
                        field: "EventTitle",
                        dir: "asc"
                    },
                    pageSize: 10,
                    serverPaging: false,
                    serverFiltering: false,
                    serverSorting: false,
                    requestStart: function ()
                    {
                        $("#results-container").hide();
                        //kendo.ui.progress($("#loading"), true);
                    },
                    requestEnd: function ()
                    {
                        $("#results-container").show();
                        //kendo.ui.progress($("#loading"), false);
                    }
                },
                sortable:
                {
                    mode: "single",
                    allowUnsort: false
                },
                filterable: false,
                pageable: true,
                detailInit: loadCourseAttendees,
                columns:
                [
                    {
                        field: "EventStatusText",
                        title: "Status",
                        width: "150px"
                    },
                    {
                        field: "EventCategoryName",
                        title: "Category",
                        width: "200"
                    },
                    {
                        field: "EventSubCategoryName",
                        title: "Subcategory",
                        width: "200"
                    },
                    {
                        field: "CourseTermName",
                        title: "Course Term Name",
                        width: "150px"
                    },
                    {
                        field: "CourseTermCode",
                        title: "Course Term",
                        width: "150px"
                    },
                    {
                        field: "CourseTitle",
                        title: "Course Title",
                        width: "175px"
                    },
                    {
                        field: "CourseCode",
                        title: "Course Code",
                        width: "120px"
                    },
                    {
                        field: "CourseDates",
                        title: "Course Dates",
                        width: "200px"
                    },
                    {
                        field: "CourseTimes",
                        title: "Course Times",
                        width: "150px"
                    },
                    {
                        field: "CourseLocation",
                        title: "Course Location",
                        width: "150px"
                    }
                ]
            }).data("kendoGrid");
        }
        else
        {
            grid.dataSource.transport.options.read.data = data;
            grid.dataSource.read();
        }
    }

    function exportChildData(courseId, rowIndex)
    {
        var deferred = $.Deferred();
        detailExportPromises.push(deferred);

        var exporter = new kendo.ExcelExporter(
        {
            columns:
            [
                { field: "Number", width: "50px" },
                { field: "Name", width: "100px" },
                { field: "SchoolBoard", width: "100px" },
                { field: "AttendanceDates", width: "500px" }
            ],
            dataSource: 
            {
                type: "json",
                transport:
                {
                    read:
                    {
                        url: urlSearchAttendee,
                        type: "POST",
                        data:
                        {
                            courseId: courseId
                        }
                    }
                },
                schema:
                {
                    data: "data",
                    total: "total",
                    model:
                    {
                        fields:
                        {
                            Number: { type: "int" },
                            Name: { type: "string" },
                            OrderName: { type: "string" },
                            SchoolBoard: { type: "string" },
                            AttendanceDates: { type: "string" }
                        }
                    }
                },
                sort:
                {
                    field: "OrderName",
                    dir: "asc"
                },
                pageSize: 10,
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false
            }
        });

        exporter.workbook().then(function (book, data)
        {
            deferred.resolve(
            {
                masterRowIndex: rowIndex,
                sheet: book.sheets[0]
            });
        });

    }

    function loadCourseAttendees(e) 
    {
        $("<div/>").appendTo(e.detailCell).kendoGrid(
        {
            dataSource:
            {
                type: "json",
                transport:
                {
                    read:
                    {
                        url: urlSearchAttendee,
                        type: "POST",
                        data:
                        {
                            courseId: e.data.EventId
                        }
                    }
                },
                schema:
                {
                    data: "data",
                    total: "total",
                    model:
                    {
                        fields:
                        {
                            Number: { type: "int" },
                            Name: { type: "string" },
                            OrderName: { type: "string" },
                            SchoolBoard: { type: "string" },
                            AttendanceDates: { type: "string" }
                        }
                    }
                },
                sort:
                {
                    field: "OrderName",
                    dir: "asc"
                },
                pageSize: 10,
                serverPaging: false,
                serverFiltering: false,
                serverSorting: false
            },
            excelExport: function (e)
            {
                // prevent saving the file
                e.preventDefault();
            },
            sortable: false,
            filterable: false,
            pageable: true,
            columns:
            [
                { field: "Number", width: "50px" },
                { field: "Name", width: "100px" },
                { field: "SchoolBoard", width: "100px" },
                { field: "AttendanceDates", width: "100px" }
            ]
        });
    }

}(window.instructorCourseListAttendance = window.instructorCourseListAttendance || {}, jQuery));