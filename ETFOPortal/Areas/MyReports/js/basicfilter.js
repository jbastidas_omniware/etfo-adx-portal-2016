﻿(function (basicfilter, $, undefined) 
{
    //Load status filters ddls
    loadStatusDropDowns();

    //Category & Subcategory
    LoadDropdownScripts("select[name='category']", urlLoadCategories, "Name", "Id", "");
    LoadDropdownScripts("select[name='subcategory']", urlLoadSubcategories, "Name", "Id", "");
    $("select[name='category']").data("kendoDropDownList").dataSource.one("change", function ()
    {
        loadSubcategories();
        LoadDropdownEvents("select[name='category']", "change", loadSubcategories);
    });

    //dtp
    //Event start date
    var eStartDate = $("input[name='eventStartDate']").kendoDatePicker(
    {
        change: changeEventStartDate
    }).data("kendoDatePicker");

    //Event end date
    var eEndDate = $("input[name='eventEndDate']").kendoDatePicker(
    {
        change: changeEventEndDate
    }).data("kendoDatePicker");

    //Registration start date
    var rStartDate = $("input[name='registrationStartDate']").kendoDatePicker(
    {
        change: changeRegistrationStartDate
    }).data("kendoDatePicker");

    //Registration end date
    var rEndDate = $("input[name='registrationEndDate']").kendoDatePicker(
    {
        change: changeRegistrationEndDate
    }).data("kendoDatePicker");

    eStartDate.max(eEndDate.value());
    eEndDate.min(eStartDate.value());
    rStartDate.max(rEndDate.value());
    rEndDate.min(rStartDate.value());


    function loadSubcategories()
    {
        var categoryId = $("select[name='category']").data("kendoDropDownList").value();

        var urlSubCatergories = urlLoadSubcategories + '/?categoryId=' + categoryId;
        LoadDropdownScripts("select[name='subcategory']", urlSubCatergories, "Name", "Id", "");
    }

    function loadStatusDropDowns()
    {
        $.ajax
        ({
            type: 'POST',
            url: urlLoadBasicStatusDropDowns,
            data: {},
            dataType: "json",
            beforeSend: function ()
            {
                //AjaxUpdateProgressShow();
            },
            success: function (data)
            {
                //Status for: Data: Events, Registrations, Releases, Locals
                LoadDropdownScriptsWithDs("select[name='eventStatus']", data.Events, "Name", "Id", "");
                LoadDropdownScriptsWithDs("select[name='registrationStatus']", data.Registrations, "Name", "Id", "");
            },
            error: function (err)
            {

            }
        });

    }

    function changeEventStartDate()
    {
        var startDate = eStartDate.value();
        var endDate = eEndDate.value();

        if (startDate)
        {
            startDate = new Date(startDate);
            startDate.setDate(startDate.getDate());
            eEndDate.min(startDate);
        }
        else if (endDate)
        {
            eStartDate.max(new Date(endDate));
        }
        else
        {
            endDate = new Date();
            eStartDate.max(endDate);
            eEndDate.min(endDate);
        }
    }

    function changeEventEndDate()
    {
        var endDate = eEndDate.value();
        var startDate = eStartDate.value();

        if (endDate)
        {
            endDate = new Date(endDate);
            endDate.setDate(endDate.getDate());
            eStartDate.max(endDate);
        }
        else if (startDate)
        {
            eEndDate.min(new Date(startDate));
        }
        else
        {
            endDate = new Date();
            eStartDate.max(endDate);
            eEndDate.min(endDate);
        }
    }

    function changeRegistrationStartDate()
    {
        var startDate = rStartDate.value();
        var endDate = rEndDate.value();

        if (startDate)
        {
            startDate = new Date(startDate);
            startDate.setDate(startDate.getDate());
            rEndDate.min(startDate);
        }
        else if (endDate)
        {
            rStartDate.max(new Date(endDate));
        }
        else
        {
            endDate = new Date();
            rStartDate.max(endDate);
            rEndDate.min(endDate);
        }
    }

    function changeRegistrationEndDate()
    {
        var endDate = rEndDate.value();
        var startDate = rStartDate.value();

        if (endDate)
        {
            endDate = new Date(endDate);
            endDate.setDate(endDate.getDate());
            rStartDate.max(endDate);
        }
        else if (startDate)
        {
            rEndDate.min(new Date(startDate));
        }
        else
        {
            endDate = new Date();
            rStartDate.max(endDate);
            rEndDate.min(endDate);
        }
    }

}(window.basicfilter = window.basicfilter || {}, jQuery));