﻿(function (localdelegation, $, undefined) 
{
    var grid;

    $(document).ajaxStop(function ()
    {
        AjaxUpdateProgressHide();
    });

    //Load status filters ddls
    loadLocalDelegationDropDowns();

    //form
    $("#search-tg").click
    (
        function ()
        {
            loadResults();
        }
    );

    function loadLocalDelegationDropDowns()
    {
        $.ajax
        ({
            type: 'POST',
            url: urlLoadLocalDelegationStatusFilters,
            data: {},
            dataType: "json",
            beforeSend: function ()
            {
                //AjaxUpdateProgressShow();
            },
            success: function (data)
            {
                LoadDropdownScriptsWithDs("select[name='localDelegationStatus']", data.Locals, "Name", "Id", "");
            },
            error: function (err)
            {

            }
        });
    }

    function loadResults()
    {       
        AjaxUpdateProgressShow();

        var url = urlSearch;

        var eventStartDate = kendo.toString($("#filter-form input[name='eventStartDate']").data("kendoDatePicker").value(), 'd');
        var eventEndDate = kendo.toString($("#filter-form input[name='eventEndDate']").data("kendoDatePicker").value(), 'd');
        var registrationStartDate = kendo.toString($("#filter-form input[name='registrationStartDate']").data("kendoDatePicker").value(), 'd');
        var registrationEndDate = kendo.toString($("#filter-form input[name='registrationEndDate']").data("kendoDatePicker").value(), 'd');

        var data =
        {
            eventName:  $("#filter-form input[name='eventName']").val(),
            eventStatus: $("#filter-form select[name='eventStatus']").data("kendoDropDownList").value(),
            category: $("#filter-form select[name='category']").data("kendoDropDownList").value(),
            subcategory: $("#filter-form select[name='subcategory']").data("kendoDropDownList").value(),
            eventStartDate: eventStartDate,
            eventEndDate: eventEndDate,
            registrationStatus: $("#filter-form select[name='registrationStatus']").data("kendoDropDownList").value(),
            registrationStartDate: registrationStartDate,
            registrationEndDate: registrationEndDate,
            localDelegationStatus: $("#filter-form select[name='localDelegationStatus']").data("kendoDropDownList").value(),
        };

        if (grid == null)
        {
            grid = $("#my-reports-grid").kendoGrid
            ({
                toolbar: ["excel", "pdf"],
                excel:
                {
                    allPages: true,
                    fileName: "Local Delegation.xlsx",
                    proxyURL: urlProxy
                },
                pdf:
                {
                    allPages: true,
                    fileName: "Local Delegation.pdf",
                    proxyURL: urlProxy
                },
                dataSource:
                {
                    type: "json",
                    transport:
                    {
                        read:
                        {
                            url: url,
                            type: "POST",
                            data: data
                        }
                    },
                    schema:
                    {
                        data: "data",
                        total: "total",
                        model: 
                        {
                            fields:
                            {
                                EventStatusText: { type: "string" },
                                EventCategoryName: { type: "string" },
                                EventSubCategoryName: { type: "string" },
                                EventTitle: { type: "string" },
                                StartDate: { type: "date" },
                                EndDate: { type: "date" },
                                RegistrationStatusText: { type: "string" },
                                RegistrationStartDate: { type: "date" },
                                RegistrationEndDate: { type: "date" },
                                LocalDelegationName: { type: "string" },
                                LocalDelegationStatusText: { type: "string" }
                            }
                        }
                    },
                    sort:
                    {
                        field: "EventTitle",
                        dir: "asc"
                    },
                    pageSize: 10,
                    serverPaging: false,
                    serverFiltering: false,
                    serverSorting: false,
                    requestStart: function ()
                    {
                        $("#results-container").hide();
                        //kendo.ui.progress($("#loading"), true);
                    },
                    requestEnd: function ()
                    {
                        $("#results-container").show();
                        //kendo.ui.progress($("#loading"), false);
                    }
                },
                sortable:
                {
                    mode: "single",
                    allowUnsort: false 
                },
                filterable: false,
                pageable: true,
                columns: 
                [
                    {
                        field: "EventTitle",
                        title: "Event",
                        width: "220px"
                    },
                    {
                        field: "EventCategoryName",
                        title: "Category",
                        width: "200"
                    },
                    {
                        field: "EventSubCategoryName",
                        title: "Subcategory",
                        width: "200"
                    },
                    {
                        field: "EventStatusText",
                        title: "Status",
                        width: "150px"
                    },
                    {
                        field: "StartDate",
                        title: "Event Start Date",
                        format: "{0:dd/MM/yyyy}",
                        width: "150px"
                    },
                    {
                        field: "EndDate",
                        title: "Event End Date",
                        format: "{0:dd/MM/yyyy}",
                        width: "150px"
                    },
                    {
                        field: "RegistrationStatusText",
                        title: "Registration Status",
                        width: "150px"
                    },
                    {
                        field: "RegistrationStartDate",
                        title: "Registration Start Date",
                        format: "{0:dd/MM/yyyy}",
                        width: "150px"
                    },
                    {
                        field: "RegistrationEndDate",
                        title: "Registration End Date",
                        format: "{0:dd/MM/yyyy}",
                        width: "150px"
                    },
                    {
                        field: "LocalDelegationName",
                        title: "Local Delegation Name",
                        width: "150px"
                    },
                    {
                        field: "LocalDelegationStatusText",
                        title: "Local Delegation Status",
                        width: "150px"
                    }
                ]
            }).data("kendoGrid");
        }
        else
        {
            grid.dataSource.transport.options.read.data = data;
            grid.dataSource.read();
        }
    }

}(window.localdelegation = window.localdelegation || {}, jQuery));