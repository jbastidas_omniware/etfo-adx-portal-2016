﻿(function (instructorCourseHistory, $, undefined) 
{
    var grid;

    $(document).ajaxStop(function ()
    {
        AjaxUpdateProgressHide();
    });

    //form
    $("#search-tg").click
    (
        function ()
        {
            loadResults();
        }
    );

    function loadResults()
    {
        AjaxUpdateProgressShow();

        var url = urlSearch;

        var eventStartDate = kendo.toString($("#filter-form input[name='eventStartDate']").data("kendoDatePicker").value(), 'd');
        var eventEndDate = kendo.toString($("#filter-form input[name='eventEndDate']").data("kendoDatePicker").value(), 'd');
        var registrationStartDate = kendo.toString($("#filter-form input[name='registrationStartDate']").data("kendoDatePicker").value(), 'd');
        var registrationEndDate = kendo.toString($("#filter-form input[name='registrationEndDate']").data("kendoDatePicker").value(), 'd');

        var data =
        {
            eventName: $("#filter-form input[name='eventName']").val(),
            eventStatus: $("#filter-form select[name='eventStatus']").data("kendoDropDownList").value(),
            category: $("#filter-form select[name='category']").data("kendoDropDownList").value(),
            subcategory: $("#filter-form select[name='subcategory']").data("kendoDropDownList").value(),
            eventStartDate: eventStartDate,
            eventEndDate: eventEndDate,
            registrationStatus: $("#filter-form select[name='registrationStatus']").data("kendoDropDownList").value(),
            registrationStartDate: registrationStartDate,
            registrationEndDate: registrationEndDate
        };

        if (grid == null)
        {
            grid = $("#my-reports-grid").kendoGrid
            ({
                toolbar: ["excel", "pdf"],
                excel:
                {
                    allPages: true,
                    fileName: "Event Listing Report.xlsx",
                    proxyURL: urlProxy
                },
                pdf:
                {
                    allPages: true,
                    fileName: "Event Listing Report.pdf",
                    proxyURL: urlProxy
                },
                dataSource:
                {
                    type: "json",
                    transport:
                    {
                        read:
                        {
                            url: url,
                            type: "POST",
                            data: data
                        }
                    },
                    schema:
                    {
                        data: "data",
                        total: "total",
                        model:
                        {
                            fields:
                            {
                                EventStatusText: { type: "string" },
                                EventCategoryName: { type: "string" },
                                EventSubCategoryName: { type: "string" },
                                EventTitle: { type: "string" },
                                StartDate: { type: "date" },
                                EndDate: { type: "date" },
                                RegistrationStatusText: { type: "string" },
                                RegistrationStartDate: { type: "date" },
                                RegistrationEndDate: { type: "date" },
                                CourseTermCode: { type: "string" },
                                CourseTitle: { type: "string" },
                                CourseDescription: { type: "string" }
                            }
                        }
                    },
                    sort:
                    {
                        field: "EventTitle",
                        dir: "asc"
                    },
                    pageSize: 10,
                    serverPaging: false,
                    serverFiltering: false,
                    serverSorting: false,
                    requestStart: function ()
                    {
                        $("#results-container").hide();
                        //kendo.ui.progress($("#loading"), true);
                    },
                    requestEnd: function ()
                    {
                        $("#results-container").show();
                        //kendo.ui.progress($("#loading"), false);
                    }
                },
                sortable:
                {
                    mode: "single",
                    allowUnsort: false
                },
                filterable: false,
                pageable: true,
                columns:
                [
                    {
                        field: "EventStatusText",
                        title: "Status",
                        width: "150px"
                    },
                    {
                        field: "EventCategoryName",
                        title: "Category",
                        width: "200px"
                    },
                    {
                        field: "EventSubCategoryName",
                        title: "Subcategory",
                        width: "200px"
                    },
                    {
                        field: "EventTitle",
                        title: "Name",
                        width: "220px"
                    },
                    {
                        field: "StartDate",
                        title: "Event Start Date",
                        format: "{0:dd/MM/yyyy}",
                        width: "150px"
                    },
                    {
                        field: "EndDate",
                        title: "Event End Date",
                        format: "{0:dd/MM/yyyy}",
                        width: "150px"
                    },
                    {
                        field: "RegistrationStatusText",
                        title: "Application Status",
                        width: "150px"
                    },
                    {
                        field: "RegistrationStartDate",
                        title: "Application Start Date",
                        format: "{0:dd/MM/yyyy}",
                        width: "150px"
                    },
                    {
                        field: "RegistrationEndDate",
                        title: "Application End Date",
                        format: "{0:dd/MM/yyyy}",
                        width: "150px"
                    },
                    {
                        field: "CourseTermCode",
                        title: "Course Term",
                        width: "120px"
                    },
                    {
                        field: "CourseTitle",
                        title: "Course Title",
                        width: "175px"
                    },
                    {
                        field: "CourseDescription",
                        title: "Course Description",
                        width: "500px"
                    },
                ]
            }).data("kendoGrid");
        }
        else
        {
            grid.dataSource.transport.options.read.data = data;
            grid.dataSource.read();
        }
    }

}(window.instructorCourseHistory = window.instructorCourseHistory || {}, jQuery));