﻿using System.Web.Http;
using System.Web.Mvc;
using Adxstudio.Xrm.Web.Mvc;

namespace Site.Areas.MyReports
{
    public class MyReportsRegistration : AreaRegistration
	{
		public override string AreaName
		{
            get { return "MyReports"; }
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
            //context.MapRoute(
            //    "MyReports",
            //    "my/my-reports",
            //    new { controller = "MyReports", action = "EventListing", id = RouteParameter.Optional, area = "MyReports" });

            context.MapRoute(
                "MyReportsEventListing",
                "my/my-reports/event-listing/{id}",
                new { controller = "MyReports", action = "EventListing", id = RouteParameter.Optional, area = "MyReports" });

            context.MapRoute(
                "MyReportsReleaseTimeRequest",
                "my/my-reports/release-time-request/{id}",
                new { controller = "MyReports", action = "ReleaseTimeRequest", id = RouteParameter.Optional, area = "MyReports" });

            context.MapRoute(
                "MyReportsLocalDelegation",
                "my/my-reports/local-delegation/{id}",
                new { controller = "MyReports", action = "LocalDelegation", id = RouteParameter.Optional, area = "MyReports" });

            context.MapRoute(
                "MyReportsMyCourseReceipt",
                "my/my-reports/my-course-receipt/{id}",
                new { controller = "MyReports", action = "MyCourseReceipt", id = RouteParameter.Optional, area = "MyReports" });

            context.MapRoute(
                "MyReportsMyEventsReceipt",
                "my/my-reports/my-events-receipt/{id}",
                new { controller = "MyReports", action = "MyEventsReceipt", id = RouteParameter.Optional, area = "MyReports" });

            context.MapRoute(
                "MyReportsInstructorCourseHistory",
                "my/my-reports/instructor-course-history/{id}",
                new { controller = "MyReports", action = "InstructorCourseHistory", id = RouteParameter.Optional, area = "MyReports" });

            context.MapRoute(
                "MyReportsInstructorCourseList",
                "my/my-reports/instructor-course-list/{id}",
                new { controller = "MyReports", action = "InstructorCourseList", id = RouteParameter.Optional, area = "MyReports" });

            context.MapRoute(
                "MyReportsInstructorCourseListAttendance",
                "my/my-reports/instructor-course-list-attendance/{id}",
                new { controller = "MyReports", action = "InstructorCourseListAttendance", id = RouteParameter.Optional, area = "MyReports" });

            context.MapRoute(
                "MyReportsDefault", 
                "my/my-reports/{action}/{id}",
                new { controller = "MyReports", action = "EventListing", id = RouteParameter.Optional, area = "MyReports" });

            context.MapSiteMarkerRoute(
                "MyReports", "MyReports", "{action}",
                new { controller = "MyReports", action = "EventListing" }, new { action = @"^MyReports.*" });

		}
	}
}
