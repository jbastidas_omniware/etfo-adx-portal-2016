﻿using System.Web.Mvc;

namespace Site.Areas.ETFO_Login
{
    public class ETFO_LoginAreaRegistration : AreaRegistration
    {
        
        public override string AreaName
        {
            get { return "ETFO_Login"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
        }
    }
}
