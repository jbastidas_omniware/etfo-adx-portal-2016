﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.Security;
using Site.Areas.ETFO.Library;
using Site.Pages;
using Site.Helpers;
using Xrm;

namespace Site.Areas.ETFO_Login.Pages
{
    public partial class SendUnlockLink : PortalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (txtUserName.Text == "")
                return;

            var context = XrmHelper.GetContext();

            Contact contact = (from c in context.CreateQuery<Contact>() where c.Adx_username == txtUserName.Text select c).FirstOrDefault();
            if (contact == null)
            {
                pnlBadUserName.Visible = true;
                pnlSuccess.Visible = false;
                pnlError.Visible = false;
            }
            else
            {
                try
                {
                    SignUpHelper.RunSendVerificationEmailWorkflowForUser(contact);

                    pnlBadUserName.Visible = false;
                    pnlError.Visible = false;
                    pnlSuccess.Visible = true;

                    lblUserName.Visible = false;
                    txtUserName.Visible = false;
                    ltDescription.Visible = false;
                    sendVerificationContainer.Visible = false;
                }
                catch (Exception ex)
                {
                    pnlBadUserName.Visible = false;
                    pnlSuccess.Visible = false;
                    pnlError.Visible = true;
                }
            }
        }

        protected void btnSubmit_Command(object sender, CommandEventArgs e)
        {

        }

    }
}