﻿using System;
using Site.Pages;
using Site.Helpers;
using Xrm;

namespace Site.Areas.ETFO_Login.Pages
{
    public partial class UnlockAccount : PortalPage
    {
        Guid ContactId
        {
            get
            {
                Guid contactId = Guid.Empty;
                bool isGuid = ReferenceEquals(Request.QueryString["id"], null) ? false : Guid.TryParse(Request.QueryString["id"].ToString(), out contactId);

                return isGuid ? contactId : Guid.Empty;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ContactId == Guid.Empty)
            {
                //error message , button to login
                pnlUrlError.Visible = true;

            }
            //http://etfo/login/unlock-account/?id=
            var context = XrmHelper.GetContext();

            var contact = new Contact()
            {
                Id = ContactId,
                oems_PendingEmailVerificationFlag = false,
                Adx_lockedout = false,
                Adx_LogonEnabled = true
            };

            try
            {
                var contextNew = XrmHelper.GetContext();
                contextNew.Attach(contact);
                contextNew.UpdateObject(contact);
                contextNew.SaveChanges();
                //success message, buton to login
                pnlSuccess.Visible = true;
            }
            catch (Exception)
            {
                //error message - support data, button to login
                pnlError.Visible = true;
            }

        }
    }
}