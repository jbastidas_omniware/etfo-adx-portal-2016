﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/MasterPages/Default.master" CodeBehind="MembersLogin.aspx.cs" Inherits="Site.Areas.ETFO_Login.Pages.MembersLogin" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content ID="Content1"  ContentPlaceHolderID="Head" runat="server">
           <!-- BEGIN GLOBAL MANDATORY STYLES -->          
   <link href="../../../css/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
   <link href="../../../css/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
   <link href="../../../css/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
   <!-- END GLOBAL MANDATORY STYLES -->
   <!-- BEGIN PAGE LEVEL STYLES --> 
   <link rel="stylesheet" type="text/css" href="../../../css/assets/plugins/select2/select2_metro.css" />
   <!-- END PAGE LEVEL SCRIPTS -->
   <!-- BEGIN THEME STYLES --> 
   <link href="../../../css/style-metronic.css" rel="stylesheet" type="text/css"/>
   <link href="../../../css/assets/css/style.css" rel="stylesheet" type="text/css"/>
   <link href="../../../css/assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
   <link href="../../../css/assets/css/plugins.css" rel="stylesheet" type="text/css"/>
   <link href="../../../css/assets/css/themes/default.css"" rel="stylesheet" type="text/css" id="style_color"/>
   <link href="../../../css/assets/css/custom.css" rel="stylesheet" type="text/css"/>

    <link rel="shortcut icon" href="../../../img/metronic/favicon.ico" />

</asp:Content>

<asp:Content ID="Content2"   ContentPlaceHolderID="MainContent" runat="server">

    
    <%-- START --%>

                 <div class="portlet box blue" id="form_wizard_1">
                  <div class="portlet-title">
                     <div class="caption">
                        <i class="icon-reorder"></i><%: Html.SnippetLiteral("Member/Wizard Title", "Member Signup -") %>  <span class="step-title">Step 1 of 4</span>
                     </div>
<%--                     <div class="tools hidden-xs">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="javascript:;" class="reload"></a>
                        <a href="javascript:;" class="remove"></a>
                     </div>--%>
                  </div>
                  <div class="portlet-body form">
                     <form action="<%= Html.SiteMarkerUrl("Member Login") %>"  class="form-horizontal" id="submit_form">
                        <div class="form-wizard">
                           <div class="form-body">
                              <ul class="nav nav-pills nav-justified steps">
                                 <li>
                                    <a href="#tab1" data-toggle="tab" class="step">
                                    <span class="number">1</span>
                                    <span class="desc"><i class="icon-ok"></i> <%: Html.SnippetLiteral("Member/SignUp/Tab1", " Accept T&C") %></span>   
                                    </a>
                                 </li>
                                 <li>
                                    <a href="#tab2" data-toggle="tab" class="step">
                                    <span class="number">2</span>
                                    <span class="desc"><i class="icon-ok"></i><%: Html.SnippetLiteral("Member/SignUp/Tab2", " Verification") %></span>   
                                    </a>
                                 </li>
                                 <li>
                                    <a href="#tab3" data-toggle="tab" class="step active">
                                    <span class="number">3</span>
                                    <span class="desc"><i class="icon-ok"></i><%: Html.SnippetLiteral("Member/SignUp/Tab3", " Account Setup") %> </span>   
                                    </a>
                                 </li>
                                 <li>
                                    <a href="#tab4" data-toggle="tab" class="step">
                                    <span class="number">4</span>
                                    <span class="desc"><i class="icon-ok"></i><%: Html.SnippetLiteral("Member/SignUp/Tab4", " Confirm") %> </span>   
                                    </a> 
                                 </li>
                              </ul>
                              <div id="bar" class="progress progress-striped" role="progressbar">
                                 <div class="progress-bar progress-bar-success"></div>
                              </div>
                              <div class="tab-content">
                                 <div class="alert alert-danger display-none">
                                    <button class="close" data-dismiss="alert"></button>
                                   <%: Html.SnippetLiteral("Member/ErrorAlert", "You have some form errors. Please check below.") %>
                                 </div>
                                 <div class="alert alert-success display-none">
                                    <button class="close" data-dismiss="alert"></button>
                                    <%: Html.SnippetLiteral("Member/SuccessAlert", "Your form validation is successful!") %>
                                 </div>

                                  <div id="messageholder1"></div>


<%--                             <i class="icon-envelope"></i>                           
                            <i class="icon-mobile-phone"></i>
                            <i class="icon-phone"></i>--%>


                                 <div class="tab-pane active" id="tab1">
                                    <h3 class="block"></h3>

                                    <div class="form-group">
                                       <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Member ID Label", "ETFO Member ID") %><span class="required">*</span></label>
                                       <div class="col-md-4">
                                          <input type="text" class="form-control" id="etfomemberid" name="etfomemberid"/>
                                       </div>
                                    </div>

                    
                          

                                      <div class="form-group">
                                             <label class="control-label col-md-3"></label>
                                          <div class="col-md-4">
                                             <p class="form-control-static" >
                                              <%: Html.SnippetLiteral("Member/Tab1/TandC", "EFTO Terms and Conditions") %>                              
                                             </p>                                              
                                              </div>
                                          </div>
                                          
                                     <div class="form-group">
                                             <label class="control-label col-md-3"></label>
                                            <div class="col-md-7" >
                                                   <span class="help-block" style="overflow-x:auto;overflow-y:scroll;">

                                                   <%: Html.SnippetLiteral("Member/Tab1/TandC Text", "Please, insert desirable text to snippet named: Member/Tab1/TandC Text") %> 

                                                 </span>
                                              </div>
                                          </div>


                                     <div class="form-group">
                                       <label class="control-label col-md-3"><span class="required">*</span></label>
                                       <div class="col-md-4">
                                           <div class="radio-list">
                                             <label>                                              
                                                          <input type="checkbox"  class="form-control" name="agreed"  data-title=" " /><%: Html.SnippetLiteral("Member/Tab1/TandC Confirmation label", "I agreed to ETFO Term and Conditions") %><br />
                                              </label>     
                                               </div>    
                                         </div>                                                
                                         <div id="form-agreed-error"></div>
                                       </div> 

                                     <div style="visibility:hidden;"> 
                                         <input type="text" class="form-control" id="fullname" name="fullname"/>
                                         <input type="text" class="form-control" id="squestionconfirm" name="squestionconfirm"/>
                                       <%--  <input type="text" class="form-control" id="username" name="username" />--%>
                                         <input type="text" class="form-control" id="adx_username" name="adx_username"/>
                                     </div>

                                      </div>


                                 <div class="tab-pane" id="tab2">
                                    <h3 class="block"></h3>
                                     <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Member ID Label", "ETFO Member ID") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="etfomemberid"></p>
                                              </div>
                                          </div>

                                     <div class="form-group">
                                       <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Member Name Label", "Member Name") %></label>
                                       <div class="col-md-4">
                                         <p class="form-control-static" data-display="fullname"></p>
                                       </div>
                                    </div>

                                   <h4 class="block"> <%: Html.SnippetLiteral("Member/Tab2/Verification Section Title", "     Verification") %> </h4>

                                     
                                    <div class="form-group">
                                        <label class="control-label col-md-1"><span class="required">*</span></label>
                                         <div class="col-md-2">
                                              <input type="text" class="form-control" id="sincheck" name="sincheck"/>                                           
                                        </div>
                                       <div class="col-md-7">
                                         <p class="form-control-static" >
                                          <%: Html.SnippetLiteral("Member/Tab2/Verification Label", " Verify this is you by entering in the middle 3 digits of your Social Security Number") %>    
                                         </p>
                                         </div>                                      
                                    </div>  
                                     <br />
                                 </div>


                                 <div class="tab-pane" id="tab3">
                                    <h3 class="block"></h3>

                                     <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Member ID Label", "ETFO Member ID") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="etfomemberid"></p>
                                              </div>
                                          </div>

                                    <div class="form-group">
                                       <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Member Name Label", "Member Name") %></label>
                                       <div class="col-md-4">
                                         <p class="form-control-static" data-display="fullname"></p>
                                       </div>
                                    </div>


                                    <div id="SecutityPanel" >

                                     <%--<h2 class="block"> ETFO Member ID already has an account registered.
                                         Please use a security answer to recover a user name.
                                     </h2>--%>

                                       <div class="form-group">
                                             <label class="control-label col-md-3"></label>
                                          <div class="col-md-8">
                                             <p class="form-control-static" >
                                                 <%: Html.SnippetLiteral("Member/Account Exist Label", "ETFO Member ID already has an account registered. Please use a security answer to recover a user name.") %>
                                           
                                             </p>                                              
                                              </div>
                                          </div>


                                       <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Tab3/Security Question Label", "Security Question") %></label>
                                              <div class="col-md-4">
                                              <p class="form-control-static" data-display="squestionconfirm"></p>                                                  
                                         </div>
                                          </div>
                                       <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Tab3/Security Answer Label", "Security Answer") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" id="sanswerconfirm" name="sanswerconfirm" />                                                  
                                              </div>
                                          </div>

                                        <br />
                                       </div>


                                     <div id="ProfilePanel">
                                           <h4 class="block"><%: Html.SnippetLiteral("NonMember/Tab2/Must valid email", "User must be a valid email address") %></h4>

                                           <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Tab3/Username Label", "Username") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                   <%--<p class="form-control-static" data-display="username"></p> --%>                                               <input type="text" class="form-control" id="username" name="username" />
                                              </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Tab3/Password Label", "Password") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="password" class="form-control" name="password" id="submit_form_password" />
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Tab3/Confirm Password Label", "Confirm Password") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="password" class="form-control" name="rpassword" />
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Tab3/Security Question Label", "Security Question") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" name="squestion" />
                                                  
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Tab3/Security Answer Label", "Security Answer") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" name="sanswer" />                                                  
                                              </div>
                                          </div>

                                     </div>


                                 </div>


                                 <div class="tab-pane" id="tab4">
                                    <h3 class="block"><%: Html.SnippetLiteral("Member/Tab4/Section Title", "Confirm your account") %></h3>
                                    <h4 class="form-section"></h4>

                                     <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Member ID Label", "ETFO Member ID") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="etfomemberid"></p>
                                              </div>
                                          </div>

                                    <div class="form-group">
                                       <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Member Name Label", "Member Name") %></label>
                                       <div class="col-md-4">
                                         <p class="form-control-static" data-display="fullname"></p>
                                       </div>
                                    </div>


                                    <div id="UserNamePanel">
                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Tab3/Username Label", "Username") %></label>
                                              <div class="col-md-4">
                                                 <p class="form-control-static" data-display="adx_username"></p> 
                                              </div>
                                          </div>

                                          <div class="form-group">
                                             <label class="control-label col-md-3"></label>
                                          <div class="col-md-4">
                                             <p class="form-control-static" >
                                             <%: Html.SnippetLiteral("Member/Tab4/Reset Password Message", "Click Submit to move to the Reset Password page.") %>
                                             </p>                                              
                                              </div>
                                          </div>


                                     </div>

                                    <div id="ProfileConfirmPanel">

                                     <div class="form-group">
                                       <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Tab3/Username Label", "Username") %></label>
                                       <div class="col-md-4">
                                          <p class="form-control-static" data-display="username"></p>
                                       </div>
                                    </div>

                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Tab3/Security Question Label", "Security Question") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                    <p class="form-control-static" data-display="squestion"></p>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("Member/Tab3/Security Answer Label", "Security Answer") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                    <p class="form-control-static" data-display="sanswer"></p>                                               
                                              </div>
                                          </div>
                                     </div>
                              </div>
                           </div>
                           <div class="form-actions fluid">
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                           <a href="<%= Html.SiteMarkerUrl("Login") %>" class="btn blue">
                                                   <%: Html.SnippetLiteral("Member/Cancel Button", "Cancel") %>                                              
                                                   </a>
                                                   <a href="javascript:;" class="btn blue button-previous">
                                                  <i class="m-icon-swapleft"></i> <%: Html.SnippetLiteral("Member/Back Button", "Back") %> 
                                                  </a>
                                        <a href="javascript:;" class="btn blue button-next">
                                       <%: Html.SnippetLiteral("Member/Continue Button", "Continue") %> <i class="m-icon-swapright m-icon-white"></i>
                                       </a>
                                       <a href="javascript:;" class="btn blue button-submit">
                                       <%: Html.SnippetLiteral("Member/Submit Button", "Submit") %> <i class="m-icon-swapright m-icon-white"></i>
                                       </a>            
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>


    <%-- END --%>



</asp:Content>

<asp:Content ID="Content3"  ContentPlaceHolderID="Scripts" runat="server">
    <script src="../../../css/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
   <script src="../../../css/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>  
   <script src= "../../../css/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>  
   <script src="../../../css/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
   <script src="../../../css/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
   <script src="../../../css/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
   <script src="../../../css/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
   <script src="../../../css/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
   <script src= "../../../css/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
   <!-- END CORE PLUGINS -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   <script  src="../../../css/assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
   <script src="../../../css/assets/plugins/jquery-validation/dist/additional-methods.min.js" type="text/javascript"></script>
   <script src="../../../css/assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
   <!-- END PAGE LEVEL PLUGINS -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   <script src="../../../css/assets/plugins/select2/select2.min.js" type="text/javascript"></script>
   <!-- END PAGE LEVEL PLUGINS -->
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="../../../css/assets/scripts/app.js" type="text/javascript"></script>
   <script src="../../../css/assets/scripts/form-wizard.js" type="text/javascript"></script>  
   <script src="../../../css/assets/scripts/custom.js" type="text/javascript"></script>    
   <!-- END PAGE LEVEL SCRIPTS -->

   <script>

       var accountObject;


       function getMetronixValueByName(attrName) {

           var form = $('#submit_form');
           var input = $('[name="' + attrName + '"]', form);
           if (input.is(":text") || input.is("textarea")) {

               return input.val();
           } else if (input.is("select")) {

               return input.find('option:selected').text();
           } else if (input.is(":radio") && input.is(":checked")) {

               return input.attr("data-title");
           }

           return null;
       }

       //form-wizard.js uses this function to submit curent membership type
       //function is specified per page
       function SubmitCurrentMembershipType() {

           Custom.hideAlert();
           var contactInfo = {};
           var methodName;
           //debugger;
           if (accountObject.hasparrentcontact != 'True') {

               methodName = "CreateMemberProfile";
               contactInfo.accountid = accountObject.accountid;
               contactInfo.EMailAddress1 = getMetronixValueByName('username');
               contactInfo.password = document.getElementById("submit_form_password").value;
               contactInfo.sequrityQuestion = getMetronixValueByName('squestion');
               contactInfo.sequrityAnswer = getMetronixValueByName('sanswer');

           }
           else {

               //methodName = "UpdateMemberProfileSecurity";
               //contactInfo.sequrityAnswer = getMetronixValueByName('sanswerconfirm');
               //contactInfo.contactid = accountObject.contactid;
               //contactInfo.EMailAddress1 = accountObject.adx_username;

               window.location.href = "<%= Html.SiteMarkerUrl("Login") %>" + "PasswordRecovery";
               return;
           }

           //debugger;

           $.ajax({
               type: "POST",
               complete: function () {

               },
               url: "/Services/AjaxHelper.svc/" + methodName,
               data: JSON.stringify(contactInfo),
               contentType: "application/json",
               dataType: "json",
               success: function (result) {
                   if (result) {

                       Custom.showAlert('You created a new user account. \n We redirect you to login page.', 'messageholder1');

                       //alert('You created a new user account. \n We redirect you to login page.');
                       window.location.href = "<%= Html.SiteMarkerUrl("Login") %>";

                   } else {

                       Custom.showAlert('An error ocurred while processing you request. Please call for support', 'messageholder1');

                       // alert('An error ocurred while processing you request. Please call for support');
                   }
               },
               error: function (xhr, ajaxOptions, thrownError) {
                   //debugger;
                   var xhr1 = xhr;
                   var ajaxOptions1 = ajaxOptions;
                   var thrownError1 = thrownError;
                   Custom.showAlert('AJAX error connecting to service.  Please try again or contact support.', 'messageholder1');

                   //alert('AJAX error connecting to service.  Please try again or contact support.');
               }
           });

           return false;

       }


       jQuery(document).ready(function () {
           // initiate layout and plugins

           App.init();
           FormWizard.init();

           //debugger;

           $('#sanswerconfirm').change(function () {

               Custom.hideAlert();
               //Custom.showPleaseWait();

               if (this.value != accountObject.adx_resetpasswordanswer) {


                   // alert('Security Answer is not correct. Please retry or contact ETFO Web Support.');
                   //Custom.hidePleaseWait();
                   Custom.showAlert("Security Answer is not correct. Please retry or contact ETFO Web Support.", 'messageholder1');
                   return;
               }

               //Custom.hidePleaseWait();
           });

           $('#sincheck').change(function () {

               Custom.hideAlert();
               Custom.showPleaseWait();

               if (this.value != accountObject.sincheck) {

                   //alert('Verification failed. Please retry or contact support.');
                   Custom.hidePleaseWait();
                   Custom.showAlert('Verification failed. Please retry or contact support.', 'messageholder1');
                   return;
               }
               else {

                   if (accountObject.memberonleaveflag != 'False') {

                       //alert('Your Membership is set On Leave. Please contact ETFO Web support.');
                       Custom.hidePleaseWait();
                       Custom.showAlert('Your Membership is set On Leave. Please contact ETFO Web support.', 'messageholder1');
                       return;
                   }
                   if (accountObject.goodstandingflag != 'True') {

                       //alert('Your Membership is not in Good Standing. Please contact ETFO Web support.');
                       Custom.hidePleaseWait();
                       Custom.showAlert('Your Membership is not in Good Standing. Please contact ETFO Web support.', 'messageholder1');
                       return;
                   }
               }

               if (accountObject.hasparrentcontact == 'True') {


                   $('#ProfilePanel').hide();
                   $('#ProfileConfirmPanel').hide();
                   $('#SecutityPanel').show();
                   $('#UserNamePanel').show();
               }
               else {

                 //  if (accountObject.email == '') {

                //       //alert('The account does not contain email. Apply for support.');
                //       Custom.hidePleaseWait();
                //       Custom.showAlert('The account does not contain email. Apply for support.', 'messageholder1');

                //       window.location.href = "<%= Html.SiteMarkerUrl("Login") %>";

                //   }

                   $('#ProfileConfirmPanel').show();
                   $('#ProfilePanel').show();
                   $('#SecutityPanel').hide();
                   $('#UserNamePanel').hide();
               }

               Custom.hidePleaseWait();

           });

           $('#etfomemberid').change(function () {


               // debugger;
               if (!this.value || this.value == '' || this.value == 'undefined') return;
               Custom.hideAlert();
               Custom.showPleaseWait();

               var p = {};
               p.oems_ETFOMemberId = this.value;
               /*
               result
               name
                hasparrentcontact
                eftomemberid
                email
                contactid
                sincheck
                memberonleaveflag
                goodstandingflag
                adx_username
                adx_resetpasswordanswer
                adx_passwordquestion
                */
               $.ajax({
                   type: "POST",
                   complete: function () {

                   },
                   url: "/Services/AjaxHelper.svc/getMemberAccount",
                   data: JSON.stringify(p),
                   contentType: "application/json",
                   dataType: "json",
                   success: function (result) {
                       //debugger;

                       if (result) {

                           if (result.result != 'True') {
                               $('#etfomemberid').val('');
                               //alert('The number is not valid. \n Plese try again or call for support.');
                               Custom.hidePleaseWait();
                               Custom.showAlert('The number is not valid. \n Plese try again or call for support.', 'messageholder1');

                               return;
                           }

                           accountObject = result;
                           //populate fields
                           $('#fullname').val(result.name);
                           $('#squestionconfirm').val(result.adx_passwordquestion);
                           $('#adx_username').val(result.adx_username);
                           if(result.email != '') $('#username').val(result.email);
                           Custom.hidePleaseWait();
                       }

                   },
                   error: function (xhr, ajaxOptions, thrownError) {
                       //debugger;
                       var xhr1 = xhr;
                       var ajaxOptions1 = ajaxOptions;
                       var thrownError1 = thrownError;
                       $('#etfomemberid').val('');
                       //alert('AJAX error connecting to service.  Please try again or contact support.');
                       Custom.hidePleaseWait();
                       Custom.showAlertWindow('AJAX error connecting to service.  Please try again or contact support.');


                   }
               });

           });


           $('#username').change(function () {

                debugger;
               if (!this.value || this.value == '' || this.value == 'undefined') return;

               Custom.hideAlert();
               Custom.showPleaseWait();

               var p = {};
               p.EMailAddress1 = this.value;


               $.ajax({
                   type: "POST",
                   complete: function () {

                   },
                   url: "/Services/AjaxHelper.svc/isContactExist",
                   data: JSON.stringify(p),
                   contentType: "application/json",
                   dataType: "json",
                   success: function (result) {
                       debugger;

                       if (result) {

                           $('#username').val('');
                           Custom.hidePleaseWait();
                           Custom.showAlert('User Account with email ' + p.EMailAddress1 + ' already exists in the system.', 'messageholder1');
                       } else {

                           Custom.hidePleaseWait();
                       }
                   },
                   error: function (xhr, ajaxOptions, thrownError) {
                       //debugger;
                       var xhr1 = xhr;
                       var ajaxOptions1 = ajaxOptions;
                       var thrownError1 = thrownError;
                       alert('AJAX error connecting to service.  Please try again or contact support.');
                       Custom.hidePleaseWait();
                       Custom.showAlert('AJAX error connecting to service.  Please try again or contact support.', 'messageholder1');

                   }
               });

           });

       });
   </script>

</asp:Content>