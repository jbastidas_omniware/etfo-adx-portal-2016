﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Default.master" CodeBehind="UnlockAccount.aspx.cs" Inherits="Site.Areas.ETFO_Login.Pages.UnlockAccount" %>

<asp:Content ID="Content1"   ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2"  ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true">

<%--    			<div class="well form-inline">--%>
    <div class="page-header form-inline" >
				
					<asp:Panel ID="pnlUrlError" class="alert alert-error" Visible="False" runat="server">
						 <crm:Snippet  runat="server" SnippetName="Auth/UnlockUser/NoId" DefaultText="Url is corrupted. Please, apply for support." Literal="true" EditType="text" />
					</asp:Panel>
                     <asp:Panel ID="pnlSuccess" class="alert alert-success" runat="server">
						 <crm:Snippet ID="Snippet1" runat="server" SnippetName="Auth/UnlockUser/Success" DefaultText="Your account is ready for use. Press OK for SignIn page." Literal="true" EditType="text" />
                     </asp:Panel>
        			<asp:Panel ID="pnlError" class="alert alert-error" Visible="False" runat="server">
						 <crm:Snippet runat="server" SnippetName="Auth/UnlockUser/ExecutionError" DefaultText="Something went wrong. Please, apply for support." Literal="true" EditType="text" />
					</asp:Panel>



	</div>

    		<div class="form-actions">
                    <crm:CrmHyperLink ID="btnOkay" runat="server"  CssClass="btn btn-primary" Text="<%$ Snippet: Auth/UnlockUser/OkayBtn, OK %>" 
                         SiteMarkerName="Login" ></crm:CrmHyperLink>
			</div>


</asp:Content>

<asp:Content ID="Content3"   ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>