﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/MasterPages/Default.master" CodeBehind="NonMembersLogin.aspx.cs" Inherits="Site.Areas.ETFO_Login.Pages.NonMembersLogin" %>
<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=3.0.30930.28736, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="tg" Namespace="Site.Library" Assembly="Site" %>

<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content   ContentPlaceHolderID="Head" runat="server">
           <!-- BEGIN GLOBAL MANDATORY STYLES -->          
   <link href="../../../css/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
   <link href="../../../css/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
   <link href="../../../css/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
   <!-- END GLOBAL MANDATORY STYLES -->
   <!-- BEGIN PAGE LEVEL STYLES --> 
   <link rel="stylesheet" type="text/css" href="../../../css/assets/plugins/select2/select2_metro.css" />
   <!-- END PAGE LEVEL SCRIPTS -->
   <!-- BEGIN THEME STYLES --> 
   <link href="../../../css/style-metronic.css" rel="stylesheet" type="text/css"/>
   <link href="../../../css/assets/css/style.css" rel="stylesheet" type="text/css"/>
   <link href="../../../css/assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
   <link href="../../../css/assets/css/plugins.css" rel="stylesheet" type="text/css"/>
   <link href="../../../css/assets/css/themes/default.css"" rel="stylesheet" type="text/css" id="style_color"/>
   <link href="../../../css/assets/css/custom.css" rel="stylesheet" type="text/css"/>

    <link rel="shortcut icon" href="../../../img/metronic/favicon.ico" />

</asp:Content>

<asp:Content  ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true">

<%--    	<form id="test" runat="server">
		<asp:ScriptManager ID="ScriptManager1" runat="server"/>
            <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList>
        </form>--%>
    
    <%-- START --%>

                 <div class="portlet box blue" id="form_wizard_1">
                  <div class="portlet-title">
                     <div class="caption">
                        <i class="icon-reorder"></i> <%: Html.SnippetLiteral("NonMember/Wizard Title", "Non-Member Signup -") %> <span class="step-title">Step 1 of 4</span>
                     </div>
<%--                     <div class="tools hidden-xs">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="javascript:;" class="reload"></a>
                        <a href="javascript:;" class="remove"></a>
                     </div>--%>
                  </div>
                  <div class="portlet-body form">
                      <form action="<%= Html.SiteMarkerUrl("NonMember Login") %>"  class="form-horizontal" id="submit_form">
                          <div class="form-wizard">
                              <div class="form-body">
                                  <ul class="nav nav-pills nav-justified steps">
                                      <li>
                                          <a href="#tab1" data-toggle="tab" class="step">
                                              <span class="number">1</span>
                                              <span class="desc"><i class="icon-ok"></i><%: Html.SnippetLiteral("NonMember/SignUp/Tab1", " Accept T&C") %></span>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="#tab2" data-toggle="tab" class="step">
                                              <span class="number">2</span>
                                              <span class="desc"><i class="icon-ok"></i><%: Html.SnippetLiteral("NonMember/SignUp/Tab2", " Account Setup") %></span>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="#tab3" data-toggle="tab" class="step active">
                                              <span class="number">3</span>
                                              <span class="desc"><i class="icon-ok"></i><%: Html.SnippetLiteral("NonMember/SignUp/Tab3", " Profile Setup") %></span>
                                          </a>
                                      </li>
                                      <li>
                                          <a href="#tab4" data-toggle="tab" class="step">
                                              <span class="number">4</span>
                                              <span class="desc"><i class="icon-ok"></i><%: Html.SnippetLiteral("NonMember/SignUp/Tab4", " Confirm") %></span>
                                          </a>
                                      </li>
                                  </ul>
                                  <div id="bar" class="progress progress-striped" role="progressbar">
                                      <div class="progress-bar progress-bar-success"></div>
                                  </div>
                                  <div class="tab-content">
                                      <div class="alert alert-danger display-none">
                                          <button class="close" data-dismiss="alert"></button>
                                          <%: Html.SnippetLiteral("NonMember/ErrorAlert", "You have some form errors. Please check below.") %>
                                          
                                      </div>
                                      <div class="alert alert-success display-none">
                                          <button class="close" data-dismiss="alert"></button>
                                           <%: Html.SnippetLiteral("NonMember/SuccessAlert", "Your form validation is successful!") %>
                                          
                                      </div>

                                        <div id="messageholder1"></div>

                                      <div class="tab-pane active" id="tab1">

                                              
                                          <h3 class="block"><%: Html.SnippetLiteral("NonMember/Tab1/Legend", "Provide your account details") %></h3>


                                      <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab1/NonMemberType", "Type") %><span class="required">*</span></label>                                             
                                              <div class="col-md-4">
                                                  <select name="nonmembertype" id="nonmembertype" class="form-control">
                    <% 
                      var context = XrmHelper.GetContext();
                      
                      Microsoft.Xrm.Sdk.Messages.RetrieveOptionSetRequest retrieveOptionSetRequest =
                      new Microsoft.Xrm.Sdk.Messages.RetrieveOptionSetRequest
                          {
                              Name = "oems_nonmembertype"
                          };
                      
                      Microsoft.Xrm.Sdk.Messages.RetrieveOptionSetResponse retrieveOptionSetResponse =
                          (Microsoft.Xrm.Sdk.Messages.RetrieveOptionSetResponse)context.Execute(retrieveOptionSetRequest);
                                               
                      Microsoft.Xrm.Sdk.Metadata.OptionSetMetadata retrievedOptionSetMetadata =
                          (Microsoft.Xrm.Sdk.Metadata.OptionSetMetadata)retrieveOptionSetResponse.OptionSetMetadata;

                      Microsoft.Xrm.Sdk.Metadata.OptionMetadata[] optionList =  retrievedOptionSetMetadata.Options.ToArray();
                      
                    %>
                                                      <option value=""></option>
                                                      <%foreach (var item in optionList)
                                                        {%>
                                                            <option value="<%= item.Value.ToString() %>"><%=  item.Label.LocalizedLabels[0].Label %></option>
                                                      <%  } %>
                                                      </select>
                                                  </div>
                                              </div>
                

                                      <div class="form-group">
                                             <label class="control-label col-md-3"></label>
                                          <div class="col-md-4">
                                             <p class="form-control-static" >
                                                 <%: Html.SnippetLiteral("NonMember/Tab1/TandC", "EFTO Terms and Conditions") %>                                            
                                             </p>                                              
                                              </div>
                                          </div>
                                    
                                          
                                     <div class="form-group">
                                             <label class="control-label col-md-3"></label>
                                            <div class="col-md-7" >
                                                   <span class="help-block" style="overflow-x:auto;overflow-y:scroll;">
<%: Html.SnippetLiteral("NonMember/Tab1/TandC Text", "Please, insert desirable text to snippet named: NonMember/Tab1/TandC Text") %>                                                   

                                                 </span>
                                              </div>
                                        </div>
                                       


                                     <div class="form-group">
                                       <label class="control-label col-md-3"><span class="required">*</span></label>
                                       <div class="col-md-4">
                                           <div class="radio-list">
                                             <label>                                              
                                                  <input type="checkbox"  class="form-control" name="agreed"  data-title=" " /><%: Html.SnippetLiteral("NonMember/Tab1/TandC Confirmation label", "I agreed to ETFO Term and Conditions") %> <br />
                                              </label>     
                                               </div>    
                                         </div>                                                
                                         <div id="form-agreed-error"></div>
                                       </div>                                   

                                      </div>


                                      <div class="tab-pane" id="tab2">
                                          <h4 class="block"><%: Html.SnippetLiteral("NonMember/Tab2/Must valid email", "User must be a valid email address") %></h4>

                                           <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab2/Username Label", "Username") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" id="username" name="username" />                                                  
                                              </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab2/Password Label", "Password") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="password" class="form-control" name="password" id="submit_form_password" />
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab2/Confirm Password Label", "Confirm Password") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="password" class="form-control" name="rpassword" />
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab2/Security Question Label", "Security Question") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" name="squestion" />
                                                  
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab2/Security Answer Label", "Security Answer") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" name="sanswer" />                                                  
                                              </div>
                                          </div>


                                      </div>

                                      <div class="tab-pane" id="tab3">
                                          <h3 class="block"><%: Html.SnippetLiteral("NonMember/Tab3/Contact Information Label", "Contact Information") %></h3>
                                        <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/First Name Label", "First Name") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" id="fname" name="fname" />
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Last Name Label", "Last Name") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" id="lname" name="lname" />
                                              </div>
                                          </div>

                                           <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Email Label", "Email") %></label>
                                              <div class="col-md-4">
                                                     <p class="form-control-static" data-display="username"></p>
                                              </div>
                                          </div>
 <div id="teacherDiv">
                                           <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/OCT Number Label", "OCT Number") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" id="oct" name="oct"/>
                                              </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/School Board Label", "School Board") %><span class="required">*</span></label>                                             
                                              <div class="col-md-4">
                                                  <select name="sboard" id="sboard" class="form-control">
                    <% 
                      
                        var SchoolBoards = context.oems_SchoolBoardSet.OrderBy(m => m.oems_SchoolBoardName).Select(m => new { Name = m.oems_SchoolBoardName, GID = m.Id.ToString() }).ToList();
                     
                    %>
                                                      <option value=""></option>
                                                      <%foreach (var item in SchoolBoards)
                                                        {%>
                                                            <option value="<%= item.GID %>"><%=  item.Name %></option>
                                                      <%  } %>
                                                      </select>
                                                  </div>
                                              </div>

                                                <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/School Label", "School") %><span class="required">*</span></label>                                             
                                              <div class="col-md-4">
                                                      <select name="school" id="school" class="form-control">
                                                      </select>
                                                  </div>
                                              </div>


</div>

<%--                                          <div class="form-group">
                                              <label class="control-label col-md-3">Gender<span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <div class="radio-list">
                                                      <label>
                                                          <input type="radio" name="gender" value="M" data-title="Male" />
                                                          Male
                                                      </label>
                                                      <label>
                                                          <input type="radio" name="gender" value="F" data-title="Female" />
                                                          Female
                                                      </label>
                                                  </div>
                                                  <div id="form_gender_error"></div>
                                              </div>
                                          </div>
--%>


                                           <h3 class="block"><%: Html.SnippetLiteral("NonMember/Tab3/Home Address Label", "Home Address") %></h3>

                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Line 1 Label", "Line 1") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" name="address" />
                                              </div>
                                          </div>
                                           <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Line 2 Label", "Line 2") %></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" name="line2" />
                                              </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/City Label", "City") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" name="city" />
                                              </div>
                                          </div>

                                           <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/ProvinceState Label", "Province/State") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" name="state" />
                                              </div>
                                          </div>

                                           <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Postal Code Label", "Postal Code/Zip") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" name="zip" />
                                              </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Country Label", "Country") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <select name="country" id="country_list" class="form-control">
                                                      <option value=""></option>
                                                      <option value="AF">Afghanistan</option>
                                                      <option value="AL">Albania</option>
                                                      <option value="DZ">Algeria</option>
                                                      <option value="AS">American Samoa</option>
                                                      <option value="AD">Andorra</option>
                                                      <option value="AO">Angola</option>
                                                      <option value="AI">Anguilla</option>
                                                      <option value="AQ">Antarctica</option>
                                                      <option value="AR">Argentina</option>
                                                      <option value="AM">Armenia</option>
                                                      <option value="AW">Aruba</option>
                                                      <option value="AU">Australia</option>
                                                      <option value="AT">Austria</option>
                                                      <option value="AZ">Azerbaijan</option>
                                                      <option value="BS">Bahamas</option>
                                                      <option value="BH">Bahrain</option>
                                                      <option value="BD">Bangladesh</option>
                                                      <option value="BB">Barbados</option>
                                                      <option value="BY">Belarus</option>
                                                      <option value="BE">Belgium</option>
                                                      <option value="BZ">Belize</option>
                                                      <option value="BJ">Benin</option>
                                                      <option value="BM">Bermuda</option>
                                                      <option value="BT">Bhutan</option>
                                                      <option value="BO">Bolivia</option>
                                                      <option value="BA">Bosnia and Herzegowina</option>
                                                      <option value="BW">Botswana</option>
                                                      <option value="BV">Bouvet Island</option>
                                                      <option value="BR">Brazil</option>
                                                      <option value="IO">British Indian Ocean Territory</option>
                                                      <option value="BN">Brunei Darussalam</option>
                                                      <option value="BG">Bulgaria</option>
                                                      <option value="BF">Burkina Faso</option>
                                                      <option value="BI">Burundi</option>
                                                      <option value="KH">Cambodia</option>
                                                      <option value="CM">Cameroon</option>
                                                      <option value="CA">Canada</option>
                                                      <option value="CV">Cape Verde</option>
                                                      <option value="KY">Cayman Islands</option>
                                                      <option value="CF">Central African Republic</option>
                                                      <option value="TD">Chad</option>
                                                      <option value="CL">Chile</option>
                                                      <option value="CN">China</option>
                                                      <option value="CX">Christmas Island</option>
                                                      <option value="CC">Cocos (Keeling) Islands</option>
                                                      <option value="CO">Colombia</option>
                                                      <option value="KM">Comoros</option>
                                                      <option value="CG">Congo</option>
                                                      <option value="CD">Congo, the Democratic Republic of the</option>
                                                      <option value="CK">Cook Islands</option>
                                                      <option value="CR">Costa Rica</option>
                                                      <option value="CI">Cote d'Ivoire</option>
                                                      <option value="HR">Croatia (Hrvatska)</option>
                                                      <option value="CU">Cuba</option>
                                                      <option value="CY">Cyprus</option>
                                                      <option value="CZ">Czech Republic</option>
                                                      <option value="DK">Denmark</option>
                                                      <option value="DJ">Djibouti</option>
                                                      <option value="DM">Dominica</option>
                                                      <option value="DO">Dominican Republic</option>
                                                      <option value="EC">Ecuador</option>
                                                      <option value="EG">Egypt</option>
                                                      <option value="SV">El Salvador</option>
                                                      <option value="GQ">Equatorial Guinea</option>
                                                      <option value="ER">Eritrea</option>
                                                      <option value="EE">Estonia</option>
                                                      <option value="ET">Ethiopia</option>
                                                      <option value="FK">Falkland Islands (Malvinas)</option>
                                                      <option value="FO">Faroe Islands</option>
                                                      <option value="FJ">Fiji</option>
                                                      <option value="FI">Finland</option>
                                                      <option value="FR">France</option>
                                                      <option value="GF">French Guiana</option>
                                                      <option value="PF">French Polynesia</option>
                                                      <option value="TF">French Southern Territories</option>
                                                      <option value="GA">Gabon</option>
                                                      <option value="GM">Gambia</option>
                                                      <option value="GE">Georgia</option>
                                                      <option value="DE">Germany</option>
                                                      <option value="GH">Ghana</option>
                                                      <option value="GI">Gibraltar</option>
                                                      <option value="GR">Greece</option>
                                                      <option value="GL">Greenland</option>
                                                      <option value="GD">Grenada</option>
                                                      <option value="GP">Guadeloupe</option>
                                                      <option value="GU">Guam</option>
                                                      <option value="GT">Guatemala</option>
                                                      <option value="GN">Guinea</option>
                                                      <option value="GW">Guinea-Bissau</option>
                                                      <option value="GY">Guyana</option>
                                                      <option value="HT">Haiti</option>
                                                      <option value="HM">Heard and Mc Donald Islands</option>
                                                      <option value="VA">Holy See (Vatican City State)</option>
                                                      <option value="HN">Honduras</option>
                                                      <option value="HK">Hong Kong</option>
                                                      <option value="HU">Hungary</option>
                                                      <option value="IS">Iceland</option>
                                                      <option value="IN">India</option>
                                                      <option value="ID">Indonesia</option>
                                                      <option value="IR">Iran (Islamic Republic of)</option>
                                                      <option value="IQ">Iraq</option>
                                                      <option value="IE">Ireland</option>
                                                      <option value="IL">Israel</option>
                                                      <option value="IT">Italy</option>
                                                      <option value="JM">Jamaica</option>
                                                      <option value="JP">Japan</option>
                                                      <option value="JO">Jordan</option>
                                                      <option value="KZ">Kazakhstan</option>
                                                      <option value="KE">Kenya</option>
                                                      <option value="KI">Kiribati</option>
                                                      <option value="KP">Korea, Democratic People's Republic of</option>
                                                      <option value="KR">Korea, Republic of</option>
                                                      <option value="KW">Kuwait</option>
                                                      <option value="KG">Kyrgyzstan</option>
                                                      <option value="LA">Lao People's Democratic Republic</option>
                                                      <option value="LV">Latvia</option>
                                                      <option value="LB">Lebanon</option>
                                                      <option value="LS">Lesotho</option>
                                                      <option value="LR">Liberia</option>
                                                      <option value="LY">Libyan Arab Jamahiriya</option>
                                                      <option value="LI">Liechtenstein</option>
                                                      <option value="LT">Lithuania</option>
                                                      <option value="LU">Luxembourg</option>
                                                      <option value="MO">Macau</option>
                                                      <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                                                      <option value="MG">Madagascar</option>
                                                      <option value="MW">Malawi</option>
                                                      <option value="MY">Malaysia</option>
                                                      <option value="MV">Maldives</option>
                                                      <option value="ML">Mali</option>
                                                      <option value="MT">Malta</option>
                                                      <option value="MH">Marshall Islands</option>
                                                      <option value="MQ">Martinique</option>
                                                      <option value="MR">Mauritania</option>
                                                      <option value="MU">Mauritius</option>
                                                      <option value="YT">Mayotte</option>
                                                      <option value="MX">Mexico</option>
                                                      <option value="FM">Micronesia, Federated States of</option>
                                                      <option value="MD">Moldova, Republic of</option>
                                                      <option value="MC">Monaco</option>
                                                      <option value="MN">Mongolia</option>
                                                      <option value="MS">Montserrat</option>
                                                      <option value="MA">Morocco</option>
                                                      <option value="MZ">Mozambique</option>
                                                      <option value="MM">Myanmar</option>
                                                      <option value="NA">Namibia</option>
                                                      <option value="NR">Nauru</option>
                                                      <option value="NP">Nepal</option>
                                                      <option value="NL">Netherlands</option>
                                                      <option value="AN">Netherlands Antilles</option>
                                                      <option value="NC">New Caledonia</option>
                                                      <option value="NZ">New Zealand</option>
                                                      <option value="NI">Nicaragua</option>
                                                      <option value="NE">Niger</option>
                                                      <option value="NG">Nigeria</option>
                                                      <option value="NU">Niue</option>
                                                      <option value="NF">Norfolk Island</option>
                                                      <option value="MP">Northern Mariana Islands</option>
                                                      <option value="NO">Norway</option>
                                                      <option value="OM">Oman</option>
                                                      <option value="PK">Pakistan</option>
                                                      <option value="PW">Palau</option>
                                                      <option value="PA">Panama</option>
                                                      <option value="PG">Papua New Guinea</option>
                                                      <option value="PY">Paraguay</option>
                                                      <option value="PE">Peru</option>
                                                      <option value="PH">Philippines</option>
                                                      <option value="PN">Pitcairn</option>
                                                      <option value="PL">Poland</option>
                                                      <option value="PT">Portugal</option>
                                                      <option value="PR">Puerto Rico</option>
                                                      <option value="QA">Qatar</option>
                                                      <option value="RE">Reunion</option>
                                                      <option value="RO">Romania</option>
                                                      <option value="RU">Russian Federation</option>
                                                      <option value="RW">Rwanda</option>
                                                      <option value="KN">Saint Kitts and Nevis</option>
                                                      <option value="LC">Saint LUCIA</option>
                                                      <option value="VC">Saint Vincent and the Grenadines</option>
                                                      <option value="WS">Samoa</option>
                                                      <option value="SM">San Marino</option>
                                                      <option value="ST">Sao Tome and Principe</option>
                                                      <option value="SA">Saudi Arabia</option>
                                                      <option value="SN">Senegal</option>
                                                      <option value="SC">Seychelles</option>
                                                      <option value="SL">Sierra Leone</option>
                                                      <option value="SG">Singapore</option>
                                                      <option value="SK">Slovakia (Slovak Republic)</option>
                                                      <option value="SI">Slovenia</option>
                                                      <option value="SB">Solomon Islands</option>
                                                      <option value="SO">Somalia</option>
                                                      <option value="ZA">South Africa</option>
                                                      <option value="GS">South Georgia and the South Sandwich Islands</option>
                                                      <option value="ES">Spain</option>
                                                      <option value="LK">Sri Lanka</option>
                                                      <option value="SH">St. Helena</option>
                                                      <option value="PM">St. Pierre and Miquelon</option>
                                                      <option value="SD">Sudan</option>
                                                      <option value="SR">Suriname</option>
                                                      <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                                      <option value="SZ">Swaziland</option>
                                                      <option value="SE">Sweden</option>
                                                      <option value="CH">Switzerland</option>
                                                      <option value="SY">Syrian Arab Republic</option>
                                                      <option value="TW">Taiwan, Province of China</option>
                                                      <option value="TJ">Tajikistan</option>
                                                      <option value="TZ">Tanzania, United Republic of</option>
                                                      <option value="TH">Thailand</option>
                                                      <option value="TG">Togo</option>
                                                      <option value="TK">Tokelau</option>
                                                      <option value="TO">Tonga</option>
                                                      <option value="TT">Trinidad and Tobago</option>
                                                      <option value="TN">Tunisia</option>
                                                      <option value="TR">Turkey</option>
                                                      <option value="TM">Turkmenistan</option>
                                                      <option value="TC">Turks and Caicos Islands</option>
                                                      <option value="TV">Tuvalu</option>
                                                      <option value="UG">Uganda</option>
                                                      <option value="UA">Ukraine</option>
                                                      <option value="AE">United Arab Emirates</option>
                                                      <option value="GB">United Kingdom</option>
                                                      <option value="US">United States</option>
                                                      <option value="UM">United States Minor Outlying Islands</option>
                                                      <option value="UY">Uruguay</option>
                                                      <option value="UZ">Uzbekistan</option>
                                                      <option value="VU">Vanuatu</option>
                                                      <option value="VE">Venezuela</option>
                                                      <option value="VN">Viet Nam</option>
                                                      <option value="VG">Virgin Islands (British)</option>
                                                      <option value="VI">Virgin Islands (U.S.)</option>
                                                      <option value="WF">Wallis and Futuna Islands</option>
                                                      <option value="EH">Western Sahara</option>
                                                      <option value="YE">Yemen</option>
                                                      <option value="ZM">Zambia</option>
                                                      <option value="ZW">Zimbabwe</option>
                                                  </select>
                                              </div>
                                          </div>


                                         <h3 class="block"><%: Html.SnippetLiteral("NonMember/Tab3/Contact Label", "Contact") %></h3>

                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Home Number Label", "Home Number") %><span class="required">*</span></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" name="phone" />
                                              </div>
                                          </div>
                                           <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Work Number Label", "Work Number") %></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" name="wphone" />
                                              </div>
                                          </div>
                                           <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Cell Number Label", "Cell Number") %></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" name="cphone" />
                                              </div>
                                          </div>
                                           <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Fax Label", "Fax") %></label>
                                              <div class="col-md-4">
                                                  <input type="text" class="form-control" name="fax" />
                                              </div>
                                          </div>

                                      </div>

                                      <div class="tab-pane" id="tab4">
                                          <h3 class="block"><%: Html.SnippetLiteral("NonMember/Tab4/Confirm Info Label", "Confirm your account") %></h3>
                                          <h4 class="form-section"></h4>
                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/First Name Label", "First Name") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="fname"></p>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Last Name Label", "Last Name") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="lname"></p>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Email Label", "Email") %></label>
                                              <div class="col-md-4">
<%--                                                  <p class="form-control-static" data-display="email"></p>--%>
                                                      <p class="form-control-static" data-display="username"></p>
                                              </div>
                                          </div>

    <div id="teacherDivConfirm">
                                           <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/OCT Number Label", "OCT Number") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="oct"></p>
                                              </div>
                                          </div>
                                           <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/School Board Label", "School Board") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="sboard"></p>
                                              </div>
                                          </div> 
                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/School Label", "School") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="school"></p>
                                              </div>
                                          </div>

        </div>
<%--                                          <div class="form-group">
                                              <label class="control-label col-md-3">Gender:</label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="gender"></p>
                                              </div>
                                          </div>--%>

                                          <h4 class="form-section"><%: Html.SnippetLiteral("NonMember/Tab3/Home Address Label", "Home Address") %></h4>
                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Line 1 Label", "Line 1") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="address"></p>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Line 2 Label", "Line 2") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="line2"></p>
                                              </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/ProvinceState Label", "Province/State") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="state"></p>
                                              </div>
                                          </div>
                                           <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Postal Code Label", "Postal Code/Zip") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="zip"></p>
                                              </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Country Label", "Country") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="country"></p>
                                              </div>
                                          </div>
                                          <h4 class="form-section"><%: Html.SnippetLiteral("NonMember/Tab4/Contact Section Label", "Contact") %></h4>
                                            <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Home Number Label", "Home Phone") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="phone"></p>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Work Number Label", "Work Number") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="wphone"></p>
                                              </div>
                                          </div>
                                            <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Cell Number Label", "Cell Number") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="cphone"></p>
                                              </div>
                                          </div>
                                            <div class="form-group">
                                              <label class="control-label col-md-3"><%: Html.SnippetLiteral("NonMember/Tab3/Fax Label", "Fax") %></label>
                                              <div class="col-md-4">
                                                  <p class="form-control-static" data-display="fax"></p>
                                              </div>
                                          </div>



                                      </div>
                                  </div>
                              </div>
                              <div class="form-actions fluid">
                                  <div class="row">
                                      <div class="col-md-12">
                                          <div class="col-md-offset-3 col-md-9">
                                              <a href="<%= Html.SiteMarkerUrl("Login") %>" class="btn blue">
                                                   <%: Html.SnippetLiteral("NonMember/Cancel Button", "Cancel") %>                                             
                                                   </a>
                                                   <a href="javascript:;" class="btn blue button-previous">
                                                  <i class="m-icon-swapleft"></i> <%: Html.SnippetLiteral("NonMember/Back Button", "Back") %> 
                                                  </a>
                                        <a href="javascript:;" class="btn blue button-next">
                                        <%: Html.SnippetLiteral("NonMember/Continue Button", "Continue") %><i class="m-icon-swapright m-icon-white"></i>
                                       </a>
                                       <a href="javascript:;" class="btn blue button-submit">
                                        <%: Html.SnippetLiteral("NonMember/Submit Button", "Submit") %> <i class="m-icon-swapright m-icon-white"></i>
                                       </a>            
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </form>
                  </div>
               </div>


    <%-- END --%>



</asp:Content>

<asp:Content   ContentPlaceHolderID="Scripts" runat="server">
    <script src="../../../css/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
   <script src="../../../css/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>  
   <script src= "../../../css/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>  
   <script src="../../../css/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
   <script src="../../../css/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
   <script src="../../../css/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
   <script src="../../../css/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
   <script src="../../../css/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
   <script src= "../../../css/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
   <!-- END CORE PLUGINS -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   <script  src="../../../css/assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
   <script src="../../../css/assets/plugins/jquery-validation/dist/additional-methods.min.js" type="text/javascript"></script>
   <script src="../../../css/assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
   <!-- END PAGE LEVEL PLUGINS -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   <script src="../../../css/assets/plugins/select2/select2.min.js" type="text/javascript"></script>
   <!-- END PAGE LEVEL PLUGINS -->
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="../../../css/assets/scripts/app.js" type="text/javascript"></script>
   <script src="../../../css/assets/scripts/form-wizard.js" type="text/javascript"></script>  
   <script src="../../../css/assets/scripts/custom.js" type="text/javascript"></script>    
   <!-- END PAGE LEVEL SCRIPTS -->

   <script>

      

       var isTeacher;
      

       function getMetronixValueByName(attrName) {

                    var form = $('#submit_form');
                   var input = $('[name="' + attrName + '"]', form);
                   if (input.is(":text") || input.is("textarea")) {
                      
                       return input.val();
                   } else if (input.is("select")) {
                       
                       return input.find('option:selected').text();
                   } else if (input.is(":radio") && input.is(":checked")) {
                      
                       return input.attr("data-title");
                   }
           
           return null;
       }

       //form-wizard.js uses this function to submit curent membership type
       //function is specified per page
       function SubmitCurrentMembershipType() {

           Custom.hideAlert();

           var contactInfo = {};

           //debugger;
           contactInfo.FirstName = getMetronixValueByName('fname');
           contactInfo.LastName = getMetronixValueByName('lname');
           contactInfo.EMailAddress1 = getMetronixValueByName('username');
           contactInfo.Telephone1_business = getMetronixValueByName('wphone');
           contactInfo.Telephone2_home = getMetronixValueByName('phone');
           contactInfo.MobilePhone = getMetronixValueByName('cphone');
           contactInfo.fax = getMetronixValueByName('fax'); 
           //contactInfo.GenderCode = (getMetronixValueByName('gender') == 'Male') ? 1 : 2;
           //     //BirthDate = 
           //     //PreferredContactMethodCode
           contactInfo.Address1_Line1 = getMetronixValueByName('address');
           contactInfo.Address1_Line2 = getMetronixValueByName('line2');
           contactInfo.Address1_City = getMetronixValueByName('city');
           contactInfo.Address1_StateOrProvince = getMetronixValueByName('state');
           contactInfo.Address1_PostalCode = getMetronixValueByName('zip');         
           contactInfo.Address1_Country = getMetronixValueByName('country');
           var form = $('#submit_form');
           contactInfo.oems_NonMemberTypeCode = $('[name="nonmembertype"]', form).find('option:selected').val();
           contactInfo.password = document.getElementById("submit_form_password").value; //getMetronixValueByName('password');//submit_form_password
           contactInfo.sequrityQuestion = getMetronixValueByName('squestion');
           contactInfo.sequrityAnswer = getMetronixValueByName('sanswer');
           contactInfo.oems_SchoolBoardId = $('[name="sboard"]', form).find('option:selected').val();
           contactInfo.oems_SchoolId = $('[name="school"]', form).find('option:selected').val();
           contactInfo.oems_OctNumber = getMetronixValueByName('oct');

           //debugger;

           $.ajax({
               type: "POST",
               complete: function () {
                      
               },
               url: "/Services/AjaxHelper.svc/CreateNonMemberProfile",
               data: JSON.stringify(contactInfo),
               contentType: "application/json",
               dataType: "json",
               success: function (result) {
                   if (result) {

                       //alert('You created a new user account. \n We redirect you to login page.');
                       Custom.hidePleaseWait();
                       Custom.showAlert('You created a new user account. \n We redirect you to login page.', 'messageholder1');
                       window.location.href = "<%= Html.SiteMarkerUrl("Login") %>";                   

                   } else {

                      // alert('An error ocurred while processing you request. Please call for support');
                       Custom.showAlert('An error ocurred while processing you request. Please call for support', 'messageholder1');
                   }
               },
               error: function (xhr, ajaxOptions, thrownError) {
                   //debugger;
                   var xhr1 = xhr;
                   var ajaxOptions1 = ajaxOptions;
                   var thrownError1 = thrownError;
                   //alert('AJAX error connecting to service.  Please try again or contact support.');
                   Custom.hidePleaseWait();
                   Custom.showAlert('AJAX error connecting to service.  Please try again or contact support', 'messageholder1');

               }
           });

           return false;
     
       }

       jQuery(document).ready(function () {
           // initiate layout and plugins

           App.init();
           FormWizard.init();

           $('#nonmembertype').change(function () {

               //debugger;
               Custom.hideAlert();
               //isTeacher = (getMetronixValueByName('nonmembertype') == 'Teacher') ? true : false;
               var form = $('#submit_form');
               isTeacher = ($('[name="nonmembertype"]', form).find('option:selected').val() == 1) ? true : false;

               if (isTeacher) {
                   
                   document.getElementById('teacherDiv').style.display = "block";
                   document.getElementById('teacherDivConfirm').style.display = "block";
                   //start filtered lookup functionality
                   if( $('[name="sboard"]', form).find('option:selected').val() == '')  $('#school').attr('disabled', true);
               }
               else {

                   $('#sboard').val('');//?set 
                   $('#school').val('');//? works
                   $('#oct').val('');
                   document.getElementById('teacherDiv').style.display = "none";
                   document.getElementById('teacherDivConfirm').style.display = "none";

               }
           });

           $('#sboard').change(function () {

               // debugger;
               var form = $('#submit_form');
               var p = {};
               p.schoolboardid = $('[name="sboard"]', form).find('option:selected').val();

               if (!p.schoolboardid || p.schoolboardid == '') return;

               Custom.hideAlert();
               Custom.showPleaseWait();



               $.ajax({
                   type: "POST",
                   complete: function () {

                   },
                   url: "/Services/AjaxHelper.svc/getSchoolByBoard",
                   data: JSON.stringify(p),
                   contentType: "application/json",
                   dataType: "json",
                   success: function (result) {

                       if (result != '') {
                           //debugger;
                           $('#school').attr('disabled', false);
                           $('#school').html(result);
                           Custom.hidePleaseWait();                           

                       } else {

                           Custom.hidePleaseWait();
                           Custom.showAlert('The board has no schools in the system.', 'messageholder1');
                       }
                   },
                   error: function (xhr, ajaxOptions, thrownError) {
                       //debugger;
                       var xhr1 = xhr;
                       var ajaxOptions1 = ajaxOptions;
                       var thrownError1 = thrownError;
                       //alert('AJAX error connecting to service.  Please try again or contact support.');
                       Custom.hidePleaseWait();
                       Custom.showAlert('AJAX error connecting to service.  Please try again or contact support.', 'messageholder1');

                   }
               });

           });


           $('#username').change(function () {
             
              // debugger;
               if (!this.value || this.value == '' || this.value == 'undefined') return;

               Custom.hideAlert();
               Custom.showPleaseWait();

               var p = {};
               p.EMailAddress1 = this.value;

           
               $.ajax({
                   type: "POST",
                   complete: function () {
                      
                   },
                   url: "/Services/AjaxHelper.svc/isContactExist",
                   data: JSON.stringify(p),
                   contentType: "application/json",
                   dataType: "json",
                   success: function (result) {
                       //debugger;
                       
                       if (result) {

                           $('#username').val('');
                           //$('#email').val('');
                           //$('#email')[0].value = '';

                           //alert('Account with email ' + p.EMailAddress1 + ' exists in the system.');
                           Custom.hidePleaseWait();
                           Custom.showAlert('User Account with email ' + p.EMailAddress1 + ' already exists in the system.', 'messageholder1');


                       } else {

                           Custom.hidePleaseWait();

                       }
                   },
                   error: function (xhr, ajaxOptions, thrownError) {
                       //debugger;
                       var xhr1 = xhr;
                       var ajaxOptions1 = ajaxOptions;
                       var thrownError1 = thrownError;
                       alert('AJAX error connecting to service.  Please try again or contact support.');
                       Custom.hidePleaseWait();
                       Custom.showAlert('AJAX error connecting to service.  Please try again or contact support.', 'messageholder1');

                   }
               });
     
           });

       });
   </script>




</asp:Content>