﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/WebForms.master"  CodeBehind="SendUnlockLink.aspx.cs" Inherits="Site.Areas.ETFO_Login.Pages.SendUnlockLink" %>


<asp:Content ID="Content1"   ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2"  ContentPlaceHolderID="MainContent" runat="server" ViewStateMode="Enabled">
    <asp:Panel ID="pnlMain" runat="server">
        <div class="form-horizontal">
            <div class="page-header form-inline" >
			    <asp:Literal ID="ltDescription" Text="<%$ Snippet: Auth/SendUnlockLink/Legend, Please insert User Name and press submit to get verification email.%>" runat="server"></asp:Literal>
	
                <asp:Panel ID="pnlSuccess" class="alert alert-success" Visible="False" runat="server">
						<crm:Snippet runat="server"  SnippetName="Auth/SendUnlockLink/Success" DefaultText="The verification email was sent to you.  Please follow instructions." Literal="true" EditType="text" />
                </asp:Panel>
        		<asp:Panel ID="pnlError" class="alert alert-warning alert-block alert-danger" Visible="False" runat="server">
						<crm:Snippet  runat="server" SnippetName="Auth/SendUnlockLink/ExecutionError" DefaultText="Something went wrong. Please, apply for support." Literal="true" EditType="text" />
				</asp:Panel>
                <asp:Panel ID="pnlBadUserName" class="alert alert-warning alert-block alert-danger" Visible="False" runat="server">
						<crm:Snippet ID="Snippet1"  runat="server" SnippetName="Auth/SendUnlockLink/BadUserName" DefaultText="User Name Is Invalid." Literal="true" EditType="text" />
				</asp:Panel>
	        </div>

			<div class="form-group row">				
                <asp:Label ID="lblUserName" runat="server" CssClass="col-sm-2 control-label" Text="<%$ Snippet: Auth/SendUnlockLink/Label, User Name %>" AssociatedControlID="txtUserName"></asp:Label>
				<div class="controls col-sm-5">
                    <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtUserName" ErrorMessage="<%$ Snippet: Auth/SendUnlockLink/RFmessage, User Name is a required field! %>" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
				</div>
			</div>

			<div id="sendVerificationContainer" class="form-actions" runat="server">	
                <asp:Button ID="btnSubmit" CssClass="btn btn-primary" runat="server" Text="<%$ Snippet: Auth/SubmitButtonText/SubmitText, Submit %>" OnCommand="btnSubmit_Command" />
			</div>
        </div>
	</asp:Panel>
</asp:Content>

<asp:Content ID="Content3"   ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>