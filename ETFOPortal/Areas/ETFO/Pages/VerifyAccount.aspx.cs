﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Web.Security;
using Site.Areas.ETFO.Library;
using Site.Helpers;
using Site.Pages;

namespace Site.Areas.ETFO.Pages
{
	public partial class VerifyAccount : PortalPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
		    if (Request.IsAuthenticated)
		    {
                Response.Redirect("~/account-signout?returnUrl=" + Request.RawUrl);
		    }

		    var id = Request.QueryString["id"];
			
			if(string.IsNullOrEmpty(id))
			{
				VerifyAccountFailedPlaceHolder.Visible = true;
				return;
			}

            var contact = XrmContext.ContactSet.FirstOrDefault(cont => cont.oems_verificationid == id);

		    if (contact == null)
		    {
                VerifyAccountFailedPlaceHolder.Visible = true;
                return;
		    }

		    if (!string.IsNullOrEmpty(contact.oems_PendingUsername))
		    {
		        ChangeUsernamePanel.Visible = true;
		        UserNameExistingDataLabel.Text = contact.Adx_username;
		        UsernameNewDataLabel.Text = contact.oems_PendingUsername;
		        return;
		    }

			if(!SignUpHelper.VerifyAccount(contact))
			{
				VerifyAccountFailedPlaceHolder.Visible = true;
				return;
			}

			VerifyAccountSuccessPlaceHolder.Visible = true;

            SignUpHelper.RunSendSucessSignUpEmailWorkflowForUser(contact);
		}

	    protected void ChangeUsernameButton_OnClick(object sender, EventArgs e)
	    {
            try
            {
                var membershipProvider = (CrmContactMembershipProvider)Membership.Provider;

                if (!membershipProvider.ValidateUser(UserNameExistingDataLabel.Text, Password.Text.Trim()))
                {
                    ShowMessage(ChangeUsernameResultPanel, ChangeUsernameResultLiteral, XrmHelper.GetSnippetValueOrDefault("Account Verification Change Username Invalid Message",
                    "Invalid username and/or password."), "error");
                    return;
                }

                var id = Request.QueryString["id"];

                if (string.IsNullOrEmpty(id))
                {
                    VerifyAccountFailedPlaceHolder.Visible = true;
                    return;
                }

                var contact = XrmContext.ContactSet.FirstOrDefault(cont => cont.oems_verificationid == id);

                if (contact == null)
                {
                    ShowMessage(ChangeUsernameResultPanel, ChangeUsernameResultLiteral, XrmHelper.GetSnippetValueOrDefault("Account Verification Change Username Not Found Message",
                    "Existing account could not be found."), "error");
                    return;
                }

                contact.EMailAddress1 = contact.oems_PendingUsername;
                contact.Adx_username = contact.oems_PendingUsername;
                contact.oems_PendingUsername = string.Empty;
                contact.oems_verificationid = string.Empty;

                XrmContext.UpdateObject(contact);
                XrmContext.SaveChanges();

                ShowMessage(ChangeUsernameResultPanel, ChangeUsernameResultLiteral,
                XrmHelper.GetSnippetValueOrDefault("Account Verfication Change Username Success Message",
                    "Your username has been successfully changed.  You may now login with your new username."), "success");

                ChangeUsernamePanel.Visible = false;
            }
            catch (Exception ex)
            {
                ShowMessage(ChangeUsernameResultPanel, ChangeUsernameResultLiteral,
                XrmHelper.GetSnippetValueOrDefault("Account Verfication Change Username Error Message",
                    "There was a problem changing your username."), "error");
            }
	    }

        private static void ShowMessage(WebControl panel, ITextControl literal, string msg, string type)
        {
            panel.Visible = true;
            literal.Text = msg;
            panel.CssClass += string.Format(" alert-{0}", type);
        }
	}
}