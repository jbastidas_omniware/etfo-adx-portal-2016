﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/ETFO/MasterPages/WebFormsEventContent.master" AutoEventWireup="true" CodeBehind="ResolutionsPassed.aspx.cs" Inherits="Site.Areas.ETFO.Pages.ResolutionsPassed" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<%@ Import Namespace="Xrm" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Breadcrumbs" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageHeader" runat="server">
</asp:Content>--%>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <crm:Snippet ID="Snippet1" SnippetName="Social Share Widget Code Page Top" EditType="text" DefaultText="" runat="server"/>
	<crm:Property ID="Property1" DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
    <asp:ListView runat="server" ID="DocumentList" OnItemDataBound="DocumentList_ItemDataBound">
        <LayoutTemplate>
                <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="document-item">
                <h4 style="background-color:<%# Eval("oems_titlebackgroundcolour") %>"><%# Eval("oems_documentname") %></h4>
                <div class="row-fluid document-decscription"><%# Eval("oems_description") %></div>
                <div class="row-fluid document-links">
                    <asp:Repeater ID="DocumentLinksRepeater" runat="server">
                        <ItemTemplate>
                             <a href="<%# getUrl(Container.DataItem as Xrm.Adx_webfile) %>" target="_blank">
				                <%# Eval("adx_name") %>
                             </a>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </ItemTemplate>
    </asp:ListView>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentBottom" runat="server">
	<div class="page-metadata clearfix">
		<div class="page-last-updated">
			<crm:Snippet ID="Snippet2" SnippetName="Page Modified On Prefix" DefaultText="This page was last updated" EditType="text" runat="server"/>
			<abbr class="timeago">
				<%: string.Format("{0:r}", Html.AttributeLiteral("modifiedon")) %>
			</abbr>.
			<% var displayDate = Html.AttributeLiteral("adx_displaydate"); %>
			<% if (displayDate != null) { %>
				<crm:Snippet ID="Snippet3" SnippetName="Page Published On Prefix" DefaultText="It was published" EditType="text" runat="server"/>
				<abbr class="timeago">
					<%: string.Format("{0:r}", displayDate) %>
				</abbr>.
			<% } %>
		</div>
		<crm:Snippet ID="Snippet4" SnippetName="Social Share Widget Code Page Bottom" EditType="text" DefaultText="" runat="server"/>
	</div>
</asp:Content>
<%--<asp:Content ID="Content5" ContentPlaceHolderID="EntityControls" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentBottom" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="SidebarAbove" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="SidebarTop" runat="server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="SidebarBottom" runat="server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SidebarBelow" runat="server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>--%>
