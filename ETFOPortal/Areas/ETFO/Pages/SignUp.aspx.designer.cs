﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Site.Areas.ETFO.Pages {
    
    
    public partial class SignUp {
        
        /// <summary>
        /// CurrentHeaderEntity control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Microsoft.Xrm.Portal.Web.UI.WebControls.CrmEntityDataSource CurrentHeaderEntity;
        
        /// <summary>
        /// CurrentContentEntity control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Microsoft.Xrm.Portal.Web.UI.WebControls.CrmEntityDataSource CurrentContentEntity;
        
        /// <summary>
        /// Snippet1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Microsoft.Xrm.Portal.Web.UI.WebControls.Snippet Snippet1;
        
        /// <summary>
        /// Property1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Microsoft.Xrm.Portal.Web.UI.WebControls.Property Property1;
        
        /// <summary>
        /// ErrorPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ErrorPanel;
        
        /// <summary>
        /// WebFormControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Adxstudio.Xrm.Web.UI.WebControls.WebForm WebFormControl;
        
        /// <summary>
        /// EntityFormControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Adxstudio.Xrm.Web.UI.WebControls.EntityForm EntityFormControl;
        
        /// <summary>
        /// EntityListControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Adxstudio.Xrm.Web.UI.WebControls.EntityList EntityListControl;
        
        /// <summary>
        /// Snippet2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Microsoft.Xrm.Portal.Web.UI.WebControls.Snippet Snippet2;
        
        /// <summary>
        /// MultiRatingControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Site.Controls.MultiRatingControl MultiRatingControl;
    }
}
