﻿using Adxstudio.Xrm.Blogs;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Portal;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using Site.Areas.ETFO.DataAdapters;
using Site.Helpers;
using Site.Pages;
using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using Xrm;

namespace Site.Areas.ETFO.Pages
{
    public partial class EventLandingPage : PortalPage
    {
        private readonly Lazy<IPortalContext> _portal = new Lazy<IPortalContext>(() => PortalCrmConfigurationManager.CreatePortalContext());

        private readonly Xrm.XrmServiceContext _context = XrmHelper.GetContext();

        public String retrieveEventInfoJSON { get; set; }
        public oems_Event Event { get; set; }

        protected Entity CarouselBlog
        {
            get
            {
                return _context.adx_blogSet.FirstOrDefault(b => b.adx_websiteid.Id == _portal.Value.Website.Id && b.adx_parentpageid.Id == _portal.Value.Entity.Id && b.oems_type == (int)Enums.BlogType.Carousel);
            }
        }

        protected Entity AnnouncementBlog
        {
            get
            {
                return _context.adx_blogSet.FirstOrDefault(b => b.adx_websiteid.Id == _portal.Value.Website.Id && b.adx_parentpageid.Id == _portal.Value.Entity.Id && b.oems_type == (int)Enums.BlogType.Annoucement);
            }
        }

        protected Entity DefaultBlog
        {
            get
            {
                return _context.adx_blogSet.FirstOrDefault(b => b.adx_websiteid.Id == _portal.Value.Website.Id && b.adx_parentpageid.Id == _portal.Value.Entity.Id && b.oems_type == (int)Enums.BlogType.Blog);
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            CarouselPanel.Visible = (CarouselBlog != null);
            AnnouncementPanel.Visible = (AnnouncementBlog != null);
            BlogPanel.Visible = (DefaultBlog != null);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var oemsEvent = ServiceContext.oems_EventSet.FirstOrDefault(ev => ev.oems_WebPage.Id == Portal.Entity.Id);
            if (oemsEvent == null)
            {
                return;
            }

            Event = oemsEvent;

            var columset = new ColumnSet(new string[] { "oems_retrieveeventinformation" });
            var evt = (oems_Event)ServiceContext.Retrieve("oems_event", oemsEvent.Id, columset);

            //Waiting list flags
            var waitingListComponentFlag = oemsEvent.GetAttributeValue<bool>("oems_waitinglistcomponentflag");
            var waitingListFullFlag = oemsEvent.GetAttributeValue<bool>("oems_waitinglistfullflag");
            var eventFullFlag = oemsEvent.GetAttributeValue<bool>("oems_eventfullflag");

            retrieveEventInfoJSON = evt.oems_RetrieveEventInformation;  

            bool registrationOpen;
            bool applicationOpen;

            //Registration/Application open
            if (Contact == null)
            {
                registrationOpen = RegistrationHelper.RegistrationOpen(null, oemsEvent.Id, Portal);
                applicationOpen = RegistrationHelper.ApplicationOpen(null, oemsEvent.Id, Portal);
            }
            else
            {
                registrationOpen = RegistrationHelper.RegistrationOpen(Contact.Id, oemsEvent.Id, Portal);
                applicationOpen = RegistrationHelper.ApplicationOpen(Contact.Id, oemsEvent.Id, Portal);
            }

            //Registration status
            Guid registrationId;
            _isRegistered = IsRegistered(out registrationId);
            _isApplied = IsApplied();
            _isFull = !waitingListComponentFlag && IsFull();
            
            //Urls
            registerLnk.NavigateUrl = registrationOpen || _isRegistered ? GetRegistrationURL() : "";
            applyLnk.NavigateUrl = applicationOpen || _isApplied ? GetApplicationURL() : "";

            //Texts
            registerLnk.Text = _isRegistered ? 
                XrmHelper.GetSnippetValueOrDefault("EventsLandingPage/MyRegistrationButtonText", "My Registration") :
                XrmHelper.GetSnippetValueOrDefault("EventsLandingPage/RegisterButtonText", "Register");
            applyLnk.Text = _isApplied ?
                XrmHelper.GetSnippetValueOrDefault("EventsLandingPage/MyApplicationButtonText", "My Application") :
                XrmHelper.GetSnippetValueOrDefault("EventsLandingPage/ApplyButtonText", "Apply");

            //Panels visibility
            RegistrationPanel.Visible = registrationOpen && (!_isFull || _isRegistered);
            ApplicationPanel.Visible = applicationOpen || _isApplied;
            FullPanel.Visible = _isFull && !_isRegistered;

            //Waiting list is enabled?
            if (!waitingListComponentFlag)
                return;

            //Waiting list additional validations
            if (waitingListFullFlag)
            {
                RegistrationPanel.Visible = false;
                FullPanel.Visible = false;
                eventFullText.Visible = true;
                eventFullText.InnerText = XrmHelper.GetSnippetValueOrDefault("UpcomingEvents/WaitingList/RegistrationClosedEventFull", "Registration closed: this event is completely full");
            }
            else if (eventFullFlag)
            {
                if (!_isRegistered)
                {
                    FullPanel.Visible = false;
                    eventFullText.Visible = true;
                    eventFullText.InnerText = XrmHelper.GetSnippetValueOrDefault("UpcomingEvents/WaitingList/EventFullSpotsAvailable", "This event is full but waiting list spots are available");
                }
                else
                {
                    //Is this person on the waiting list?
                    var isWaiting = RegistrationHelper.IsRegistrationWithWaitingListWaiting(oemsEvent.Id, registrationId);
                    FullPanel.Visible = false;
                    if (isWaiting)
                    {
                        eventFullText.Visible = true;
                        eventFullText.InnerText = XrmHelper.GetSnippetValueOrDefault("UpcomingEvents/WaitingList/OnWaitingList", "On waiting list");
                    }
                }
            }
            else
            {
                FullPanel.Visible = false;
            }

            //EventChildNavigation.StartNodeUrl = startNodeUrl;
        }

        public bool _isRegistered;
        public bool IsRegistered(out Guid registrationId)
        {
            registrationId = Guid.Empty;
            try
            {
                return Contact != null && RegistrationHelper.IsUserRegisteredToEvent(Contact.Id, Event.Id, out registrationId);
            }
            catch
            {
                return false;
            }
        }

        public bool _isApplied;
        public bool IsApplied()
        {
            try
            {
                Guid registrationId = Guid.Empty;
                return Contact != null && RegistrationHelper.IsUserAppliedToEvent(Contact.Id, Event.Id, out registrationId);
            }
            catch
            {
                return false;
            }
        }

        public bool _isFull;
        public bool IsFull()
        {
            var totalRegistrations = 0;
            using (var localContext = new XrmServiceContext())
            {
                var oemsEventRegistrations = 
                (
                    from eventRegs in localContext.oems_EventRegistrationSet
                    join ev in localContext.oems_EventSet on eventRegs.oems_Event.Id equals ev.oems_EventId
                    where (eventRegs.statuscode == RegistrationHelper.EventRegistration.Sumbitted || eventRegs.statuscode == RegistrationHelper.EventRegistration.Approved) && eventRegs.oems_Event.Id == Event.Id
                    where eventRegs["oems_registrationtype"] == null || ((OptionSetValue)eventRegs["oems_registrationtype"]).Value == 347780000
                    select eventRegs
                );
                totalRegistrations = oemsEventRegistrations.ToList().Count();
            }

            return totalRegistrations >= (Event.oems_RegistrationMaximum ?? Int32.MaxValue);
        }

		protected void CreateCarouselAggregateDataAdapter(object sender, ObjectDataSourceEventArgs args)
		{
			var adapter = new ETFOBlogDataAdapter(CarouselBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
			adapter.Type = Enums.BlogType.Carousel;
			args.ObjectInstance = adapter;
		}

		protected void CreateAnnouncementAggregateDataAdapter(object sender, ObjectDataSourceEventArgs args)
		{
			var adapter = new ETFOBlogDataAdapter(AnnouncementBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
			adapter.Type = Enums.BlogType.Annoucement;
			args.ObjectInstance = adapter;
		}

		protected void CreateBlogAggregateDataAdapter(object sender, ObjectDataSourceEventArgs args)
		{
			var adapter = new ETFOBlogDataAdapter(DefaultBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
			adapter.Type = Enums.BlogType.Blog;
			args.ObjectInstance = adapter;
		}

        protected string GetThumbnailUrl(object entityObject)
        {
            var entity = entityObject as Entity;

            if (entity == null) return null;

            var blogItem = _context.adx_blogpostSet.FirstOrDefault(bp => bp.adx_blogpostId == entity.Id);

            if (blogItem == null || blogItem.oems_Image == null)
            {
                return @"http://placehold.it/1280x720";
            }

            var webfile = ServiceContext.CreateQuery<Xrm.Adx_webfile>().FirstOrDefault(file => file.Id == blogItem.oems_Image.Id);

            return webfile == null ? @"http://placehold.it/1280x720" : new UrlBuilder(ServiceContext.GetUrl(webfile)).Path;
        }

        public string GetRegistrationURL()
        {
           var oems_event = ServiceContext.oems_EventSet.FirstOrDefault(e => e.oems_WebPage.Id == Portal.Entity.Id);
           //var regPage= ServiceContext.GetPageBySiteMarkerName(Website, "event registration");
           var regPage = ServiceContext.GetPageBySiteMarkerName(Website, "Registration Validation");
           var url = new UrlBuilder(ServiceContext.GetUrl(regPage));
           if (oems_event == null) return "";
           url.QueryString.Set("oems_EventId", oems_event.Id.ToString());
           url.QueryString.Set("type", "1");
           return (url.PathWithQueryString); 
        }

        public string GetApplicationURL()
        {
            var oems_event = ServiceContext.oems_EventSet.FirstOrDefault(e => e.oems_WebPage.Id == Portal.Entity.Id);
            //var regPage= ServiceContext.GetPageBySiteMarkerName(Website, "event registration");
            var regPage = ServiceContext.GetPageBySiteMarkerName(Website, "Registration Validation");
            var url = new UrlBuilder(ServiceContext.GetUrl(regPage));
            if (oems_event == null) return "";
            url.QueryString.Set("oems_EventId", oems_event.Id.ToString());
            url.QueryString.Set("type", "2");
            return (url.PathWithQueryString);
        }

        public string Truncatestring(string mys) {
           int myL = int.Parse(XrmHelper.GetSiteSettingValueOrDefault("Blog-Announcement/SummaryLength", "200"));
           string expn = "<.*?>";
           string noHtml = Regex.Replace(mys,expn, string.Empty);

           if (noHtml.Length <= myL)
           {
               return noHtml;
           }
           else
           {
               return noHtml.Substring(0, myL);
           }
        }
    }
}