﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Xrm;
using Site.Helpers;
using Microsoft.Xrm.Portal.Web;
using Site.Pages;

using Adxstudio.Xrm.Cms;

namespace Site.Areas.ETFO.Pages
{
    public partial class CategoryRedirect : PortalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var regPage = ServiceContext.GetPageBySiteMarkerName(Website, "Home");
            var url = new UrlBuilder(ServiceContext.GetUrl(regPage));
            Response.Redirect(url.PathWithQueryString); 
        }
    }
}