﻿using Adxstudio.Xrm.Blogs;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Portal;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using Site.Areas.ETFO.DataAdapters;
using Site.Helpers;
using Site.Pages;
using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.HtmlControls;
using Adxstudio.Xrm.Web.Mvc.Html;
using Microsoft.Xrm.Portal.Web.UI.WebControls;
using Microsoft.Xrm.Sdk.Metadata;
using Site.Areas.ETFO.Library;
using Xrm;
using System.Collections.Generic;

namespace Site.Areas.ETFO.Pages
{
    public partial class CourseTermPage : PortalPage
    {
        private readonly Lazy<IPortalContext> _portal = new Lazy<IPortalContext>(() => PortalCrmConfigurationManager.CreatePortalContext());

        private readonly Xrm.XrmServiceContext _context = XrmHelper.GetContext();

        private enum HtmlToDisplay
        {
            Presenter = 347780000,
            Registration = 347780001
        }

        public String retrieveEventInfoJSON { get; set; }
        public oems_Event Event { get; set; }

        protected Entity CarouselBlog
        {
            get
            {
                return _context.adx_blogSet.FirstOrDefault(b => b.adx_websiteid.Id == _portal.Value.Website.Id && b.adx_parentpageid.Id == _portal.Value.Entity.Id && b.oems_type == (int) Enums.BlogType.Carousel);
            }
        }

        protected Entity AnnouncementBlog
        {
            get
            {
                return _context.adx_blogSet.FirstOrDefault(b => b.adx_websiteid.Id == _portal.Value.Website.Id && b.adx_parentpageid.Id == _portal.Value.Entity.Id && b.oems_type == (int)Enums.BlogType.Annoucement);
            }
        }

        protected Entity DefaultBlog
        {
            get
            {
                return _context.adx_blogSet.FirstOrDefault(b => b.adx_websiteid.Id == _portal.Value.Website.Id && b.adx_parentpageid.Id == _portal.Value.Entity.Id && b.oems_type == (int)Enums.BlogType.Blog);
            }
        }

        public bool loadCoursesListing = false;
        public string GridSearchID;
        public string GridSearchPlaceholder;

        protected void Page_Init(object sender, EventArgs e)
        {
            CarouselPanel.Visible = (CarouselBlog != null);
            AnnouncementPanel.Visible = (AnnouncementBlog != null);
            BlogPanel.Visible = (DefaultBlog != null);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var page = XrmContext.Adx_webpageSet.FirstOrDefault(p => p.Adx_webpageId == Portal.Entity.Id);
                if (page != null)
                {
                    //CurrentEntity = new CrmEntityDataSource();
                    CurrentEntity.DataItem = page;

                    var htmlToDisplay = page.GetAttributeValue<int>("oems_htmltodisplay");
                    if(htmlToDisplay == (int)HtmlToDisplay.Presenter)
                    {
                        dynamichtml.InnerHtml = page.GetAttributeValue<string>("oems_presenterhtml");
                    }
                    else if(htmlToDisplay == (int)HtmlToDisplay.Registration)
                    {
                        dynamichtml.InnerHtml = page.GetAttributeValue<string>("oems_registrationhtml");
                    }
                }
            }

            GridSearchID = "#" + GridSearchText.ClientID;
            GridSearchPlaceholder = ServiceContext.GetSnippetValueByName(Portal.Website, "ListingCourses/GridSearchPlaceholder") ?? "search courses";
            
            var oemsEvent = ServiceContext.oems_EventSet.FirstOrDefault(ev => ev.oems_WebPage.Id == Portal.Entity.Id);
            if (oemsEvent == null)
            {
                return;
            }

            Event = oemsEvent;

            var columset = new ColumnSet(new string[] { "oems_retrieveeventinformation", "statuscode" });
            var evt = (oems_Event)ServiceContext.Retrieve("oems_event", oemsEvent.Id, columset);

            if (evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.OpenRegistration
                ||evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.PublishedRegistration
                || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.CloseRegistration
                || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.StartEvent
                || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.EndEvent
                || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.Completed)
            {
                loadCoursesListing = true;
            }

            //retrieveEventInfoJSON = evt.oems_RetrieveEventInformation;  // Clean
            //retrieveEventInfoJSON = "{\"showSection\":false,\"showEventName\":false,\"eventName\":\"billing event - DO NOT DELETE\",\"showEventDate\":false,\"eventDate\":\"Thursday, July 2, 2015 12:00 AM - Sunday, July 2, 2017 12:00 AM\",\"showEventLocation\":false,\"eventLocation\":\"Westin Harbour Castle<br/>1 Harbour Square<br/>Toronto, ON M5J 1A6\",\"showAcommodationtLocation\":false,\"accommodationtLocation\":null,\"showRegistrationDeadline\":false,\"registrationDeadline\":\"Wednesday, July 26, 2017 12:00 AM\",\"showEventAdminUserList\":false,\"eventAdminUserList\":[{\"fullName\":\"ETFO Tester2\",\"emailAddress\":\"ehilland@gmail.com\",\"telephoneNumber\":null,\"faxeNumber\":null}],\"showEventExecutiveUserList\":false,\"eventExecutiveUserList\":[{\"fullName\":\"Erasmus Hilland\",\"emailAddress\":\"ehilland@rogers.com\",\"telephoneNumber\":null,\"faxeNumber\":null},{\"fullName\":\"Omniware Admin\",\"emailAddress\":\"development@events.etfo.org\",\"telephoneNumber\":null,\"faxeNumber\":null},{\"fullName\":\"ETFO Tester1\",\"emailAddress\":\"etfo.test1@etfo.ca\",\"telephoneNumber\":\"416-666-1212\",\"faxeNumber\":\"416-444-6666\"}],\"portalBillingDescription\":\"<p>This event costs $200 to $300 but it is worth it. You are going to get free food and do not have to go to work.&nbsp;</p>\",\"eventStatus\":347780005,\"applicationDeadLine\":null,\"courseCode\":null,\"coursePresenters\":null,\"courseTopic\":null,\"courseLocation\":null,\"courseTargetGrades\":null,\"courseInitialOferedDate\":null,\"courseFinalOferedDate\":null,\"courseInitialDailyTime\":null,\"courseFinalDailyTime\":null,\"courseDescription\":null}"; //evt.oems_RetrieveEventInformation;
            retrieveEventInfoJSON = "{\"showSection\":false,\"showEventName\":false,\"eventName\":\"JB Application Event (Do Not Delete)\",\"showEventDate\":false,\"eventDate\":\"Wednesday, February 10, 2016 12:00 AM - Thursday, March 24, 2016 12:00 AM\",\"showEventLocation\":false,\"eventLocation\":\"Canada\",\"showAcommodationtLocation\":false,\"accommodationtLocation\":\"Canada\",\"showRegistrationDeadline\":false,\"registrationDeadline\":null,\"showEventAdminUserList\":true,\"eventAdminUserList\":[{\"fullName\":\"Omniware Admin\",\"emailAddress\":\"development@events.etfo.org\",\"telephoneNumber\":null,\"faxeNumber\":null},{\"fullName\":\"eventsadmin Team\",\"emailAddress\":\"development@events.etfo.org\",\"telephoneNumber\":null,\"faxeNumber\":null}],\"showEventExecutiveUserList\":true,\"eventExecutiveUserList\":[{\"fullName\":\"ETFO Tester2\",\"emailAddress\":\"ehilland@gmail.com\",\"telephoneNumber\":null,\"faxeNumber\":null},{\"fullName\":\"Admin User\",\"emailAddress\":\"info@omniware.ca\",\"telephoneNumber\":null,\"faxeNumber\":null},{\"fullName\":\"ETFO Tester1\",\"emailAddress\":\"etfo.test1@etfo.ca\",\"telephoneNumber\":\"416-666-1212\",\"faxeNumber\":\"416-444-6666\"}],\"portalBillingDescription\":\"\",\"eventStatus\":347780005,\"applicationDeadLine\":\"Wednesday, July 6, 2016 12:00 AM\",\"courseCode\":null,\"coursePresenters\":null,\"courseTopic\":null,\"courseLocation\":null,\"courseTargetGrades\":null,\"courseInitialOferedDate\":null,\"courseFinalOferedDate\":null,\"courseInitialDailyTime\":null,\"courseFinalDailyTime\":null,\"courseDescription\":null}";

            //Registration/Application open
            bool applicationOpen;
            if (Contact == null)
            {
                applicationOpen = RegistrationHelper.ApplicationOpen(null, oemsEvent.Id, Portal);
            }
            else
            {
                // Clean (Contact.ParentCustomerId.Id => Contact.Id)
                applicationOpen = RegistrationHelper.ApplicationOpen(Contact.Id, oemsEvent.Id, Portal);
            }
            
            //Application Url, text and visible 
            applyLnk.NavigateUrl = applicationOpen ? GetApplicationURL() : "javascript:void(0);";
            applyLnk.Visible = applicationOpen;
            applyLnk.Text = XrmHelper.GetSnippetValueOrDefault("CourseTermPage/ApplyButtonText", "Apply");
        }

		protected void CreateCarouselAggregateDataAdapter(object sender, ObjectDataSourceEventArgs args)
		{
			var adapter = new ETFOBlogDataAdapter(CarouselBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
			adapter.Type = Enums.BlogType.Carousel;
			args.ObjectInstance = adapter;
		}

		protected void CreateAnnouncementAggregateDataAdapter(object sender, ObjectDataSourceEventArgs args)
		{
			var adapter = new ETFOBlogDataAdapter(AnnouncementBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
			adapter.Type = Enums.BlogType.Annoucement;
			args.ObjectInstance = adapter;
		}

		protected void CreateBlogAggregateDataAdapter(object sender, ObjectDataSourceEventArgs args)
		{
			var adapter = new ETFOBlogDataAdapter(DefaultBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
			adapter.Type = Enums.BlogType.Blog;
			args.ObjectInstance = adapter;
		}

        protected string GetThumbnailUrl(object entityObject)
        {
            var entity = entityObject as Entity;

            if (entity == null) return null;

            var blogItem = _context.adx_blogpostSet.FirstOrDefault(bp => bp.adx_blogpostId == entity.Id);

            if (blogItem == null || blogItem.oems_Image == null)
            {
                return @"http://placehold.it/1280x720";
            }

            var webfile = ServiceContext.CreateQuery<Xrm.Adx_webfile>().FirstOrDefault(file => file.Id == blogItem.oems_Image.Id);

            return webfile == null ? @"http://placehold.it/1280x720" : new UrlBuilder(ServiceContext.GetUrl(webfile)).Path;
        }

        public string GetApplicationURL()
        {
            var oems_event = ServiceContext.oems_EventSet.FirstOrDefault(e => e.oems_WebPage.Id == Portal.Entity.Id);
            var regPage = ServiceContext.GetPageBySiteMarkerName(Website, "event registration");
            var url = new UrlBuilder(ServiceContext.GetUrl(regPage));
            if (oems_event == null) return "";
            url.QueryString.Set("oems_EventId", oems_event.Id.ToString());
            url.QueryString.Set("type", "3");
            return (url.PathWithQueryString);
        }

        public string Truncatestring(string mys) {
           int myL = int.Parse(XrmHelper.GetSiteSettingValueOrDefault("Blog-Announcement/SummaryLength", "200"));
           string expn = "<.*?>";
           string noHtml = Regex.Replace(mys,expn, string.Empty);

           if (noHtml.Length <= myL)
           {
               return noHtml;
           }
           else
           {
               return noHtml.Substring(0, myL);
           }
        }

        private void ShowModalInfoNoEvent()
        {
            ShowModalInfo("ShowInfoModalEventRegForm", "Error loading term information", "Sorry there is an error loading term information", "Go to Home", Html.SiteMarkerUrl("Home"));
        }

        #region Courses Listing

        public void CoursesListingDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if(!Page.IsPostBack && !loadCoursesListing)
                e.Cancel = true;
        }

        public void CoursesListingDataSource_OnObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            if (e == null)
            {
                return;
            }

            //Filter
            var searchText = GridSearchText.Text;

            //Contact
            Guid? contactId = null;
            if (Contact != null)
            {
                contactId = this.Contact.Id;
            }

            //Term PageId
            var pageId = Portal.Entity.Id;
            var courseTerm = ServiceContext.oems_EventSet.FirstOrDefault(ev => ev.oems_WebPage.Id == pageId);
            if (courseTerm == null)
            {
                return;
            }
            Guid courseTermId = courseTerm.Id;
            string courseTermName = courseTerm.GetAttributeValue<string>("oems_eventname");

            var courseListings = new CourseListings(courseTermId, courseTermName, contactId, searchText, Portal);

            e.ObjectInstance = courseListings;
        }

        protected void FilterButton_Click(object sender, EventArgs e)
        {
            CourseListDataSource.DataBind();
        }

        protected void GridSearchButton_Click(object sender, EventArgs e)
        {
            CourseListDataSource.DataBind();
        }

        public string GetRegistrationButtonText(Guid courseId)
        {
            var text = XrmHelper.GetSnippetValueOrDefault("CoursesListing/RegisterButtonText", "Register");
            if (Contact != null)
            {
                Guid registrationId;
                var isRegistered = RegistrationHelper.IsUserRegisteredToEvent(Contact.Id, courseId, out registrationId);

                if (isRegistered)
                {
                    text = XrmHelper.GetSnippetValueOrDefault("CoursesListing/ManageRegistrationButtonText", "My Registration");
                }
            }

            return text;
        }

        protected void Register_Command(object sender, CommandEventArgs e)
        {
            var regPage = ServiceContext.GetPageBySiteMarkerName(Website, "Registration Validation");
            var url = new UrlBuilder(ServiceContext.GetUrl(regPage));
            url.QueryString.Set("oems_EventId", e.CommandArgument.ToString());
            url.QueryString.Set("type", "4");
            Response.Redirect(url.PathWithQueryString);
        }

        protected void CoursesListingView_OnSorting(Object sender, ListViewSortEventArgs e)
        {
            // Check the sort direction to set the image URL accordingly.
            var cssClass = e.SortDirection == SortDirection.Ascending ? "icon-caret-up rediconcolor" : "icon-caret-down rediconcolor";

            // Check which field is being sorted
            // to set the visibility of the image controls.
            var sortLocation = (HtmlGenericControl)CoursesListingView.FindControl("sortLocation");
            var sortTopic = (HtmlGenericControl)CoursesListingView.FindControl("sortTopic");
            var sortEventTitle = (HtmlGenericControl)CoursesListingView.FindControl("sortEventTitle");
            var sortOferedDates = (HtmlGenericControl)CoursesListingView.FindControl("sortOferedDates");

            sortLocation.Visible = false;
            sortTopic.Visible = false;
            sortEventTitle.Visible = false;
            sortOferedDates.Visible = false;

            switch (e.SortExpression)
            {
                case "Location":
                    sortLocation.Visible = true;
                    sortLocation.Attributes["class"] = cssClass;
                    break;
                case "Topic":
                    sortTopic.Visible = true;
                    sortTopic.Attributes["class"] = cssClass;
                    break;
                case "EventTitle":
                    sortEventTitle.Visible = true;
                    sortEventTitle.Attributes["class"] = cssClass;
                    break;
                case "OferedDates":
                    sortOferedDates.Visible = true;
                    sortOferedDates.Attributes["class"] = cssClass;
                    break;
            }
        }

        protected void CoursesListingView_OnLayoutCreated(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CoursesListingView.Sort("InitialOferedDate", SortDirection.Ascending);
            }
        }

        protected void CoursesListingView_DataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var dataItem = (ListViewDataItem)e.Item;
                if (dataItem.DisplayIndex == CoursesListingView.EditIndex)
                {
                    var item = e.Item;
                    var el = (EventListing)e.Item.DataItem;
                    var register = (Button)item.FindControl("Register");
                    var waitingList = (Button)item.FindControl("WaitingList");
                    var isFullPanel = (Panel)item.FindControl("IsFullPanel");
                    var eventFullBtn = (HtmlGenericControl)isFullPanel.FindControl("eventFullBtn");
                    var eventFullText = (HtmlGenericControl)isFullPanel.FindControl("eventFullText");

                    var showRegistration = ShowRegistrationButton(el.EventId.Value);

                    //If waiting list is enabled dont call .IsFull to avoid that query
                    Guid registrationId;
                    var isFull = !el.WaitingListComponentFlag && IsCourseFull(el.IsFull);
                    var isUserRegistered = IsUserRegisteredToCourse(el.EventId.Value, out registrationId);

                    register.Visible = showRegistration && (!isFull || isUserRegistered);
                    waitingList.Visible = false;
                    isFullPanel.Visible = isFull && !isUserRegistered;

                    //Registration Periods
                    var registrationPeriodsContainer = (HtmlGenericControl)isFullPanel.FindControl("registrationPeriodsContainer");
                    var registrationPeriods = EventListings.GetCourseRegistrationPeriods(Portal, el.EventId.Value, el.EventStatus.Value);
                    if (!string.IsNullOrWhiteSpace(registrationPeriods))
                    {
                        registrationPeriodsContainer.InnerHtml = registrationPeriods;
                    }

                    //Course presenters
                    var presenters = el.Presenter;
                    if (!string.IsNullOrWhiteSpace(presenters))
                    {
                        var presentersArray = presenters.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        var presentersList = string.Empty;
                        foreach (var presenter in presentersArray)
                        {
                            //presenter =
                            presentersList += string.Format(@"<div class=""details-content long"">{0}</div>", presenter);
                        }

                        var presentersContainer = (HtmlGenericControl)isFullPanel.FindControl("presentersContainer");
                        presentersContainer.InnerHtml = presentersList;
                    }

                    //Waiting list is enabled?
                    if (!el.WaitingListComponentFlag)
                        return;

                    //Waiting list additional validations
                    if (el.WaitingListFullFlag)
                    {
                        register.Visible = false;
                        isFullPanel.Visible = true;
                        eventFullBtn.Visible = false;
                        eventFullText.Visible = true;
                        eventFullText.InnerText = XrmHelper.GetSnippetValueOrDefault("CoursesListing/WaitingList/RegistrationClosedEventFull", "Registration closed: this event is completely full");
                    }
                    else if (el.EventFullFlag)
                    {
                        if (!isUserRegistered)
                        {
                            isFullPanel.Visible = true;
                            eventFullBtn.Visible = false;
                            eventFullText.Visible = true;
                            register.Visible = false;
                            waitingList.Visible = true;
                            eventFullText.InnerText = XrmHelper.GetSnippetValueOrDefault("CoursesListing/WaitingList/EventFullSpotsAvailable", "This event is full but waiting list spots are available");
                        }
                        else
                        {
                            //Is this person on the waiting list?
                            var isWaiting = RegistrationHelper.IsRegistrationWithWaitingListWaiting(el.EventId.Value, registrationId);
                            isFullPanel.Visible = false;
                            if (isWaiting)
                            {
                                isFullPanel.Visible = true;
                                eventFullBtn.Visible = false;
                                eventFullText.Visible = true;
                                eventFullText.InnerText = XrmHelper.GetSnippetValueOrDefault("CoursesListing/WaitingList/OnWaitingList", "On waiting list");
                            }
                            else
                            {
                                isFullPanel.Visible = true;
                                eventFullBtn.Visible = false;
                                eventFullText.Visible = true;
                                register.Visible = false;
                                waitingList.Visible = true;
                                eventFullText.InnerText = XrmHelper.GetSnippetValueOrDefault("UpcomingEvents/WaitingList/EventFullSpotsAvailable", "This event is full but waiting list spots are available");
                            }
                        }
                    }
                    else
                    {
                        isFullPanel.Visible = false;
                    }
                }
            }
        }

        // Private Functions
        private bool ShowRegistrationButton(Guid courseId)
        {
            // Clean (Contact.ParentCustomerId.Id => Contact.Id)
            if (Contact == null)
                return RegistrationHelper.RegistrationOpen(null, courseId, Portal);
            else
                return RegistrationHelper.RegistrationOpen(Contact.Id, courseId, Portal);
        }

        private bool IsCourseFull(Object isFull)
        {
            return (bool)isFull;
        }

        private bool IsUserRegisteredToCourse(Guid courseId, out Guid registrationId)
        {
            registrationId = Guid.Empty;
            return Contact != null && RegistrationHelper.IsUserRegisteredToEvent(Contact.Id, courseId, out registrationId);
        }

        #endregion
    }
}