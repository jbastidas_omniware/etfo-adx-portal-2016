﻿using System;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Sdk.Query;
using Site.Pages;
using log4net;

namespace Site.Areas.ETFO.Pages {

	public partial class ElectronicSurveyTemplate : PortalPage
	{

        private readonly ILog Log = LogManager.GetLogger(typeof(ElectronicSurveyTemplate));

		protected void Page_Load(object sender, EventArgs args)
		{
            Log.Debug("Page_load");

            //see AuthenticatedPage.aspx.cs
            RedirectToLoginIfAnonymous();
            
            // Clean (Contact.ParentCustomerId.Id => Contact.Id)
            var account = XrmContext.Retrieve(Xrm.Account.EntityLogicalName, Contact.Id, new ColumnSet(true)).ToEntity<Xrm.Account>();
            
            if (account == null)
            {
                // Clean (Contact.ParentCustomerId.Id => Contact.Id)
                Log.Error("Contact is not connected to account "+ Contact.Id);
                throw new Exception(string.Format("Contacts parent account not found {0}", Contact.Id));
            }

            if (!account.oems_MemberFlag.GetValueOrDefault())
            {
                Log.Warn("Non member attempted access ");
                // access for non-member
                var nonMemberPage = ServiceContext.GetPageBySiteMarkerName(Website, "Access Denied - Member Only");
               if (nonMemberPage == null)
               {
                   Log.Info("non member specific access denied page was not found");
                    nonMemberPage = ServiceContext.GetPageBySiteMarkerName(Website, "Access Denied");
               }
                Response.Redirect(ServiceContext.GetUrl(nonMemberPage));
            }

		}
	}
}
