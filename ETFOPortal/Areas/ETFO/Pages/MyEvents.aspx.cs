﻿using System;
using System.Web.UI.HtmlControls;
using Site.Pages;
using System.Linq;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using Site.Areas.ETFO.Library;
using Microsoft.Xrm.Client;
using Xrm;
using Site.Helpers;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Label = System.Web.UI.WebControls.Label;
using Newtonsoft.Json;
using Microsoft.Xrm.Portal.Configuration;

namespace Site.Areas.ETFO.Pages
{
    public partial class MyEvents : PortalPage
    {
        public string GridSearchID;
        public string GridSearchPlaceholder;
        public string CancelConfirmText = "";
        public string WithdrawConfirmText = "";
        public string CancelApplicationConfirmText = "";
        public string WithdrawApplicationConfirmText = "";
        public string RejectWaitingListConfirmText = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            RedirectToLoginIfAnonymous();

            CancelConfirmText = XrmHelper.GetSnippetValueOrDefault("MyEvents/CancelButtonConfirmText", "Are you sure you want to cancel your registration for this event?");
            WithdrawConfirmText = XrmHelper.GetSnippetValueOrDefault("MyEvents/WithdrawButtonConfirmText", "Are you sure you want to withdraw your registration for this event?");
            CancelApplicationConfirmText = XrmHelper.GetSnippetValueOrDefault("MyEvents/CancelApplicationButtonConfirmText", "Are you sure you want to cancel your application for this event?");
            WithdrawApplicationConfirmText = XrmHelper.GetSnippetValueOrDefault("MyEvents/WithdrawApplicationButtonConfirmText", "Are you sure you want to withdraw your application for this event?");
            RejectWaitingListConfirmText = XrmHelper.GetSnippetValueOrDefault("MyEvents/RejectWaitingListButtonConfirmText", "Are you sure you want to reject your waiting list offer for this event?");

            GridSearchID = "#"+GridSearchText.ClientID;
            GridSearchPlaceholder = ServiceContext.GetSnippetValueByName(Portal.Website, "MyEvents/GridSearchPlaceholder")?? "search events";
        }

        public void MyEventsDataSource_OnObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            if (e == null)
            {
                return;
            }            
            
            Guid? contactId=null;
            if (Contact != null)
            {
                contactId = this.Contact.Id;
            }

            var searchText = GridSearchText.Text;
            var eventListings = new EventListings(null, searchText, false, contactId, Portal);

            e.ObjectInstance = eventListings;
        }
        
        protected void GridSearchButton_Click(object sender, EventArgs e)
        {
            EventListDataSource.DataBind();
        }

        protected void ManageRegistration_Command(object sender, CommandEventArgs e)
        {
            var regPage = ServiceContext.GetPageBySiteMarkerName(Website, "Registration Page");
            var url = new UrlBuilder(ServiceContext.GetUrl(regPage));

            var args = e.CommandArgument.ToString().Split(',');
            var eventRegistrationId = args[0];
            url.QueryString.Set("oems_EventRegistrationId", eventRegistrationId);

            if (args.Length > 1)
            {
                var eventTypeId = args[1];
                if (!string.IsNullOrWhiteSpace(eventTypeId))
                {
                    if (eventTypeId == "2")
                    {
                        url.QueryString.Set("type", "4");
                    }
                    else
                    {
                        url.QueryString.Set("type", eventTypeId);
                    }
                }
            }
            Response.Redirect(url.PathWithQueryString);
        }

        protected bool ShowManageDelegationButton(object flag)
        {
            bool? eventLocalDelegationComponentFlag = (bool?) flag;
            bool showLocalDelegation = eventLocalDelegationComponentFlag.HasValue && eventLocalDelegationComponentFlag.Value
                && ((Site.Helpers.RegistrationHelper.MyEventConfiguration)Eval("EventConfig")).showManageDelegationButton;

            return showLocalDelegation;
        }

        protected bool ShowViewButton(int? eventStatus, int? registrationStatus)
        {
            return RegistrationHelper.CanViewRegistration(eventStatus, registrationStatus);
        }

        protected bool IsPaymentPendingOrPayed(Guid registrationId)
        {
            return RegistrationHelper.IsPaymentPendingOrPayed(registrationId);
        }

        protected void ManageDelegation_Command(object sender, CommandEventArgs e)
        {
            var regPage = ServiceContext.GetPageBySiteMarkerName(Website, "Manage Local Delegation");
            var url = new UrlBuilder(ServiceContext.GetUrl(regPage));
            url.QueryString.Set("oems_EventRegistrationId", e.CommandArgument.ToString());
            Response.Redirect(url.PathWithQueryString); 
        }

        protected void MyItinerary_Command(object sender, CommandEventArgs e)
        {
            var regPage = ServiceContext.GetPageBySiteMarkerName(Website, "My Itinerary");
            var url = new UrlBuilder(ServiceContext.GetUrl(regPage));
            url.QueryString.Set("oems_EventRegistrationId", e.CommandArgument.ToString());
            Response.Redirect(url.PathWithQueryString); 
        }

        protected void AcceptWaitingListOffer_Command(object sender, CommandEventArgs e)
        {
            var argument = e.CommandArgument.ToString();
            var parts = argument.Split(new [] { '~' }, StringSplitOptions.RemoveEmptyEntries);
            var eventId = new Guid(parts[0]);
            var registrationId = new Guid(parts[1]);

            var regPage = ServiceContext.GetPageBySiteMarkerName(Website, "Registration Page");
            var url = new UrlBuilder(ServiceContext.GetUrl(regPage));
            url.QueryString.Set("oems_EventRegistrationId", registrationId.ToString());
            Response.Redirect(url.PathWithQueryString);
        }

        protected void MyEventsView_DataBound(object sender, ListViewItemEventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["oems_EventRegistrationId"]))
                {
                    var el = (EventListing)e.Item.DataItem;
                    if (el.EventRegistrationId.ToString() == Request.QueryString["oems_EventRegistrationId"])
                        MyEventsView.EditIndex = e.Item.DataItemIndex;
                }
            }

            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var dataItem = (ListViewDataItem)e.Item;
                if (dataItem.DisplayIndex == MyEventsView.EditIndex)
                {
                    var item = e.Item;
                    var el = (EventListing)e.Item.DataItem;
                    var manageRegistration = (Button)item.FindControl("ManageRegistration");
                    var viewRegistration = (Button)item.FindControl("ViewRegistration");
                    var manageApplication = (Button)item.FindControl("ManageApplication");
                    var viewApplication = (Button)item.FindControl("ViewApplication");
                    var myCoursePackage = (Button)item.FindControl("MyCoursePackage");
                    var myCourseCertificate = (Button)item.FindControl("MyCourseCertificate");

                    var billing = (Panel)item.FindControl("billingPnl");

                    var showManageRegistration = el.EventConfig.showManageRegistrationButton && el.RegistrationType == RegistrationHelper.EventRegistrationType.Attendee;
                    var showViewRegistration = !el.EventConfig.showManageRegistrationButton && this.ShowViewButton(el.EventStatus, el.EventRegistrationStatus) && el.RegistrationType == RegistrationHelper.EventRegistrationType.Attendee;
                    var showManageApplication = el.EventConfig.showManageApplicationButton && el.RegistrationType == RegistrationHelper.EventRegistrationType.Presenter;
                    var showViewApplication = !el.EventConfig.showManageApplicationButton && this.ShowViewButton(el.EventStatus, el.EventRegistrationStatus) && el.RegistrationType == RegistrationHelper.EventRegistrationType.Presenter;
                    var isPaymentPendingOrPayed = this.IsPaymentPendingOrPayed(el.EventRegistrationId.Value);

                    manageRegistration.Visible = showManageRegistration && !isPaymentPendingOrPayed && (el.ListingType == 1 || el.ListingType == 2);
                    viewRegistration.Visible = showViewRegistration || isPaymentPendingOrPayed;
                    manageApplication.Visible = showManageApplication;
                    viewApplication.Visible = showViewApplication;
                    myCoursePackage.Visible = el.CoursePackageAvailableFlag.HasValue && el.CoursePackageAvailableFlag.Value;
                    myCourseCertificate.Visible = false;// el.EventConfig.showMyCourseCertificateButton && el.ListingType == 2; // Clean
                    billing.Visible = isPaymentPendingOrPayed;

                    // My Itinerary
                    if (el.EventConfig.showMyItineraryButton)
                    {
                        bool showItinerary = true;
                        oems_EventRegistration oemsEventRegistration = ServiceContext.oems_EventRegistrationSet.Where(a => a.oems_EventRegistrationId == el.EventRegistrationId.Value).Single();

                        #region My Itinerary validations

                        // Local Delegation
                        if (el.LocalDelegationFlag.Value && !string.IsNullOrWhiteSpace(el.AttendingAsJson))
                        {
                            var jsonAttendingAs = JsonConvert.DeserializeObject<AttendingAs>(el.AttendingAsJson);
                            if (jsonAttendingAs.attendingAsList.Any(att => att.isVotingRole.HasValue && att.isVotingRole.Value == true))
                            {
                                var local = oemsEventRegistration.oems_Local;
                                if (local != null)
                                {
                                    var delegation =
                                    (
                                        from d in ServiceContext.oems_EventLocalSet
                                        where (d.oems_Event.Id == el.EventId && d.oems_local.Id == local.Id)
                                        select d
                                    ).FirstOrDefault();

                                    //Not Approved
                                    if (delegation.statuscode != 347780000)
                                    {
                                        showItinerary = false;
                                    }
                                }
                            }
                        }

                        //Child Care
                        var childCareRequests = oemsEventRegistration.oems_eventregistration_to_childcarerequest;
                        if (showItinerary && childCareRequests != null && childCareRequests.Count() > 0)
                        {
                            var requested = childCareRequests.Where(r => r.statuscode == 1);
                            if (requested.Count() > 0)
                            {
                                showItinerary = false;
                            }
                        }

                        // Accommodation
                        var accommodationRequests = oemsEventRegistration.oems_eventregistration_to_accommodationrequest;
                        if (showItinerary && accommodationRequests != null && accommodationRequests.Count() > 0)
                        {
                            var requested = accommodationRequests.Where(r => r.statuscode == 1);
                            if (requested.Count() > 0)
                            {
                                showItinerary = false;
                            }
                        }

                        //Personal Accommodation
                        var personalAcommodationRequests = oemsEventRegistration.oems_eventregistration_to_personalaccommodationrequest;
                        if (showItinerary && personalAcommodationRequests != null && personalAcommodationRequests.Count() > 0)
                        {
                            var requested = personalAcommodationRequests.Where(r => r.statuscode == 1);
                            if (requested.Count() > 0)
                            {
                                showItinerary = false;
                            }
                        }

                        // Caucus
                        var caucusRequests = oemsEventRegistration.oems_eventregistration_to_caucusrequest;
                        if (showItinerary && caucusRequests != null && caucusRequests.Count() > 0)
                        {
                            var requested = caucusRequests.Where(r => r.statuscode == 1);
                            if (requested.Count() > 0)
                            {
                                showItinerary = false;
                            }
                        }

                        // Release Time
                        var releaseTimeRequests = oemsEventRegistration.oems_oems_eventregistration_mbr_releasetimetracking_EventRegistration;
                        if (showItinerary && releaseTimeRequests != null && releaseTimeRequests.Count() > 0)
                        {
                            var requested = releaseTimeRequests.Where(r => r.statuscode == 1);
                            if (requested.Count() > 0)
                            {
                                showItinerary = false;
                            }
                        }

                        var myItinerary = (Button)item.FindControl("MyItinerary");
                        myItinerary.Visible = showItinerary;

                        #endregion
                    }

                    HtmlGenericControl registrationPeriodsContainer = null;
                    HtmlGenericControl ApplicationSchedulesContainer = null;

                    //Registration Periods
                    if (el.ListingType == 1)
                    {
                        registrationPeriodsContainer = (HtmlGenericControl)item.FindControl("registrationPeriodsContainer");
                        ApplicationSchedulesContainer = (HtmlGenericControl)item.FindControl("ApplicationSchedulesContainer");     
                    }
                    else if (el.ListingType == 2)
                    {
                        registrationPeriodsContainer = (HtmlGenericControl)item.FindControl("registrationPeriodsContainer_Course");
                    }

                    //TODO: Pending to validate "el.ListingType != 2" are we gonna use the check validation?
                    if (registrationPeriodsContainer != null && el.RegistrationType.Value == RegistrationHelper.EventRegistrationType.Attendee)
                    {
                        var registrationPeriods = el.ListingType != 2 ? EventListings.GetRegistrationPeriods(Portal, el.EventId.Value, el.EventStatus.Value)
                            : EventListings.GetCourseRegistrationPeriods(Portal, el.EventId.Value, el.EventStatus.Value);

                        if (!string.IsNullOrWhiteSpace(registrationPeriods))
                        {
                            registrationPeriodsContainer.InnerHtml = registrationPeriods;
                        }
                    }

                    if (ApplicationSchedulesContainer != null && el.RegistrationType.Value != RegistrationHelper.EventRegistrationType.Attendee && el.ListingType != 2)
                    {
                        var ApplicationSchedules = EventListings.GetEventApplicationSchedules(el.EventId.Value, el.EventStatus.Value);
                        if (!string.IsNullOrWhiteSpace(ApplicationSchedules))
                        {
                            ApplicationSchedulesContainer.InnerHtml = ApplicationSchedules;
                        }
                    }
                    
                    //Waiting list validations
                    if (el.WaitingListComponentFlag)
                    {
                        //Is this person on the waiting list?
                        var request = RegistrationHelper.GetRegistrationWithWaitingListRequest(el.EventId.Value, el.EventRegistrationId.Value);
                        var isWaiting = request != null && (request.GetAttributeValue<OptionSetValue>("statuscode").Value == RegistrationHelper.WaitingListStatus.Offered || request.GetAttributeValue<OptionSetValue>("statuscode").Value == RegistrationHelper.WaitingListStatus.Waiting);
                        var isOffered = request != null && request.GetAttributeValue<OptionSetValue>("statuscode").Value == RegistrationHelper.WaitingListStatus.Offered;
                        var waitingListText = (Label)item.FindControl("waitingListLabel");

                        if (isWaiting)
                        {
                            waitingListText.Visible = true;
                            waitingListText.Text = XrmHelper.GetSnippetValueOrDefault("MyEvents/WaitingList/OnWaitingList", "On waiting list");
                        }

                        if (isOffered)
                        {
                            var acceptWaitingListOfferBtn = (Button)item.FindControl("AcceptWaitingListOffer");
                            var rejectWaitingListOfferBtn = (Button)item.FindControl("RejectWaitingListOffer");

                            waitingListText.Text = XrmHelper.GetSnippetValueOrDefault("MyEvents/WaitingList/WaitingListOffered", "Please reject your waiting list offer if you are not interested");
                            acceptWaitingListOfferBtn.Visible = true;
                            rejectWaitingListOfferBtn.Visible = true;
                            viewRegistration.Visible = false;
                        }
                    }
                }
            }
        }

        protected void MyEventsView_OnSorting(Object sender, ListViewSortEventArgs e)
        {
            // Check the sort direction to set the image URL accordingly.
            var cssClass = e.SortDirection == SortDirection.Ascending ? "icon-caret-up rediconcolor" : "icon-caret-down rediconcolor";

            // Check which field is being sorted
            // to set the visibility of the image controls.
            var sortRegistrationNumber = (HtmlGenericControl)MyEventsView.FindControl("sortRegistrationNumber");
            var sortTitle = (HtmlGenericControl)MyEventsView.FindControl("sortTitle");
            var sortStatus = (HtmlGenericControl)MyEventsView.FindControl("sortStatus");
            var sortStatusText = (HtmlGenericControl)MyEventsView.FindControl("sortStatusText");
            var sortStartDate = (HtmlGenericControl)MyEventsView.FindControl("sortStartDate");
            var sortEndDate = (HtmlGenericControl)MyEventsView.FindControl("sortEndDate");

            sortRegistrationNumber.Visible = false;
            sortTitle.Visible = false;
            sortStatus.Visible = false;
            sortStatusText.Visible = false;
            sortStartDate.Visible = false;
            sortEndDate.Visible = false;

            switch (e.SortExpression)
            {
                case "EventRegistrationNumber":
                    sortRegistrationNumber.Visible = true;
                    sortRegistrationNumber.Attributes["class"] = cssClass;
                    break;
                case "EventTitle":
                    sortTitle.Visible = true;
                    sortTitle.Attributes["class"] = cssClass;
                    break;
                case "EventStatusText":
                    sortStatus.Visible = true;
                    sortStatus.Attributes["class"] = cssClass;
                    break;
                case "EventRegistrationStatusText":
                    sortStatusText.Visible = true;
                    sortStatusText.Attributes["class"] = cssClass;
                    break;
                case "StartDate":
                    sortStartDate.Visible = true;
                    sortStartDate.Attributes["class"] = cssClass;
                    break;
                case "EndDate":
                    sortEndDate.Visible = true;
                    sortEndDate.Attributes["class"] = cssClass;
                    break;
            }
        }

    }
}