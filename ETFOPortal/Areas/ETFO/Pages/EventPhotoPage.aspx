<%@ Page Language="C#" MasterPageFile="~/Areas/ETFO/MasterPages/WebFormsEventContent.master" AutoEventWireup="True" CodeBehind="EventPhotoPage.aspx.cs" Inherits="Site.Areas.ETFO.Pages.EventPhotoPage" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Register src="~/Controls/Comments.ascx" tagname="Comments" tagprefix="adx" %>
<%@ Register src="~/Controls/MultiRatingControl.ascx" tagname="MultiRatingControl" tagprefix="adx" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/css/prettyPhoto.css") %>" />
    
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="Scripts">
	<script type="text/javascript" src="<%: Url.Content("~/js/jquery.prettyPhoto.min.js") %>"></script>
    <script type="text/javascript">
        $("a[rel^='gallery']").prettyPhoto({ autoplay_slideshow: false, deeplinking: false });
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
        <crm:Snippet ID="Snippet1" SnippetName="Social Share Widget Code Page Top" EditType="text" DefaultText="" runat="server"/>
		<crm:Property ID="Property1" DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
    <asp:ListView runat="server" ID="PhotoList" >
        <LayoutTemplate>
            <div id="event-image-box" class="span8">
                <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
            </div>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="span2 photogallery">
                <a href="<%# getUrl(Container.DataItem as Xrm.Adx_webfile) %>" class="thumbnail" rel="gallery[ev_gal]" title='<%# Eval("Adx_Summary") %>' >
				    <img id="product-image" src="<%# getUrl(Container.DataItem as Xrm.Adx_webfile) %>" title='<%# Eval("adx_title") %>' alt='<%# Eval("oems_AlternateText") %>'/>
			    </a>
            </div>
        </ItemTemplate>
    </asp:ListView>
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<div class="page-metadata clearfix">
		<div class="page-last-updated">
			<crm:Snippet SnippetName="Page Modified On Prefix" DefaultText="This page was last updated" EditType="text" runat="server"/>
			<abbr class="timeago">
				<%: string.Format("{0:r}", Html.AttributeLiteral("modifiedon")) %>
			</abbr>.
			<% var displayDate = Html.AttributeLiteral("adx_displaydate"); %>
			<% if (displayDate != null) { %>
				<crm:Snippet SnippetName="Page Published On Prefix" DefaultText="It was published" EditType="text" runat="server"/>
				<abbr class="timeago">
					<%: string.Format("{0:r}", displayDate) %>
				</abbr>.
			<% } %>
		</div>
		<crm:Snippet SnippetName="Social Share Widget Code Page Bottom" EditType="text" DefaultText="" runat="server"/>
	</div>
	<adx:Comments RatingType="vote" EnableRatings="false" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="SidebarBottom" runat="server">
	<div class="section">
		<adx:MultiRatingControl ID="MultiRatingControl" RatingType="rating" runat="server" />
	</div>
</asp:Content>
