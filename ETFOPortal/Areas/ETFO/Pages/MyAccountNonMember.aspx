﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="MyAccountNonMember.aspx.cs" Inherits="Site.Areas.ETFO.Pages.MyAccountNonMember" %>
<%@ Register Src="~/Areas/ETFO/Controls/MyAccountUserCreds.ascx" TagPrefix="uc1" TagName="MyAccountUserCreds" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <link rel="stylesheet" href="~/css/bootstrap-switch.min.css" />
    <link rel="stylesheet" href="~/css/bootstrap-datepicker3.min.css" />
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="Breadcrumbs" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageHeader" runat="server">
</asp:Content>--%>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentBottom" runat="server" ViewStateMode="Enabled">
    <ul class="nav nav-tabs" id="myAccountTabGroup">
        <li class="active tab-link" data-hash="#tab1">
            <a href="#tab1"><crm:Snippet runat="server" DefaultText="Personal Information" SnippetName="My Account Non Member Tab 1" /></a>
        </li>
        <li class="tab-link" data-hash="#tab2">
            <a href="#tab2"><crm:Snippet runat="server" DefaultText="User Credentials" SnippetName="My Account Non Member Tab 2" /></a>
        </li>
        <li class="tab-link" data-hash="#tab3">
            <a href="#tab3"><crm:Snippet runat="server" DefaultText="Document Locker" SnippetName="My Account Non Member Tab 3" /></a>
        </li>
    </ul>

    <div class="tab-content">
        <!-- Personal Info -->
        <div class="tab-pane active" id="tab1">
            <div class="form-horizontal">
                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                    <ContentTemplate>

                        <asp:Panel runat="server" ID="PersonInfoResultPanel" CssClass="alert" Visible="False">
                            <asp:Literal runat="server" ID="PersonalInfoResultLiteral" />
                        </asp:Panel>

                        <div class="my-account-section">
                            <asp:Panel runat="server" ID="ContactDetails">
                                <h4><crm:Snippet runat="server" DefaultText="Personal Info" SnippetName="My Account Personal Info Title"/></h4>
                                <div class="my-account-section-content">
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="FirstNameLabel" CssClass="control-label col-sm-4 required" AssociatedControlID="FirstNameTextBox" Text="<%$ Snippet: My Account First Name Label, First Name %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="FirstNameTextBox" CssClass="input-large"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="FirstNameValidator" runat="server" ControlToValidate="FirstNameTextBox"
                                                ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error"
                                                Text="<%$ Snippet: My Account Required Field, This is a required field. %>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="LastNameLabel" CssClass="control-label col-sm-4 required" AssociatedControlID="LastNameTextBox" Text="<%$ Snippet: My Account Last Name Label, Last Name %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="LastNameTextBox" CssClass="input-large"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="LastNameValidator" runat="server" ControlToValidate="LastNameTextBox"
                                                ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error"
                                                Text="<%$ Snippet: My Account Required Field, This is a required field. %>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="OrganizationLabel" CssClass="control-label col-sm-4" AssociatedControlID="OrganizationTextBox" Text="<%$ Snippet: My Account Organization Label, Organization %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="OrganizationTextBox" CssClass="input-large"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="EmailLabel" CssClass="control-label col-sm-4" AssociatedControlID="EmailTextBox" Text="<%$ Snippet: My Account Email Label, Email %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="EmailTextBox" CssClass="input-large" Enabled="False"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="GenderLabel" CssClass="control-label col-sm-4" AssociatedControlID="GenderDropdown" Text="<%$ Snippet: My Account Gender Label, Gender %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:DropDownList runat="server" ID="GenderDropdown">
                                                <asp:ListItem Text="Male" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Female" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="NonMemberBirthdayLabel" CssClass="control-label col-sm-4" AssociatedControlID="NonMemberBirthdayTextBox" Text="<%$ Snippet: My Account Non Member Birthday Label, Birthday %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <div class="input-group date" id="NonMemberBirthdayWrapper">
                                                <asp:TextBox runat="server" ID="NonMemberBirthdayTextBox" CssClass="input-small form-control"></asp:TextBox>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                            </div>
                                            <asp:HiddenField runat="server" ID="NonMemberBirthdayHiddenField" />
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                            <asp:Panel runat="server" ID="AddressPanel">
                                <h4>
                                    <crm:Snippet runat="server" DefaultText="Home Address/Contact" SnippetName="My Account Home Address Title" /></h4>
                                <div class="my-account-section-content">
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Address1Label" CssClass="control-label col-sm-4 required" AssociatedControlID="Address1TextBox" Text="<%$ Snippet: My Account Address 1 Label, Address Line 1 %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="Address1TextBox" CssClass="input-large"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Address1Validator" runat="server" ControlToValidate="Address1TextBox"
                                                ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error"
                                                Text="<%$ Snippet: My Account Required Field, This is a required field. %>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Address2Label" CssClass="control-label col-sm-4" AssociatedControlID="Address2TextBox" Text="<%$ Snippet: My Account Address 2 Label, Address Line 2 %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="Address2TextBox" CssClass="input-large"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="CityLabel" CssClass="control-label col-sm-4 required" AssociatedControlID="CityTextBox" Text="<%$ Snippet: My Account City Label, City %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="CityTextBox" CssClass="input-large"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="CityValidator" runat="server" ControlToValidate="CityTextBox"
                                                ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error"
                                                Text="<%$ Snippet: My Account Required Field, This is a required field. %>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="ProvinceLabel" CssClass="control-label col-sm-4 required" AssociatedControlID="ProvinceTextBox" Text="<%$ Snippet: My Account Province Label, Province %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="ProvinceTextBox" CssClass="input-large"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ProvinceValidator" runat="server" ControlToValidate="ProvinceTextBox"
                                                ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error"
                                                Text="<%$ Snippet: My Account Required Field, This is a required field. %>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="PostalCodeLabel" CssClass="control-label col-sm-4 required" AssociatedControlID="PostalCodeTextBox" Text="<%$ Snippet: My Account Postal Code Label, Postal Code %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="PostalCodeTextBox" CssClass="input-large"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="PostalCodeValidator" runat="server" ControlToValidate="PostalCodeTextBox"
                                                ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error"
                                                Text="<%$ Snippet: My Account Required Field, This is a required field. %>">
                                            </asp:RequiredFieldValidator>
                                            <asp:CustomValidator id="PostalCodeLengthValidator" runat="server" ControlToValidate = "PostalCodeTextBox"
                                                ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error"
                                                ErrorMessage = "<%$ Snippet: Invalid Postal Code, Invalid Postal Code. %>"
                                                ClientValidationFunction="validatePostalCodeLength" >
                                            </asp:CustomValidator>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="CountryLabel" CssClass="control-label col-sm-4 required" AssociatedControlID="CountryTextBox" Text="<%$ Snippet: My Account Country Label, Country %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="CountryTextBox" CssClass="input-large"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="CountryValidator" runat="server" ControlToValidate="CountryTextBox"
                                                ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error"
                                                Text="<%$ Snippet: My Account Required Field, This is a required field. %>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="HomePhoneLabel" CssClass="control-label col-sm-4 required" AssociatedControlID="HomePhoneTextBox" Text="<%$ Snippet: My Account Home Phone Label, Home Phone %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="HomePhoneTextBox" CssClass="input-large"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="HomePhoneValidator" runat="server" ControlToValidate="HomePhoneTextBox"
                                                ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error"
                                                Text="<%$ Snippet: My Account Required Field, This is a required field. %>" />
                                            <asp:RegularExpressionValidator ID="HomePhoneRegEx" runat="server" ControlToValidate="HomePhoneTextBox"
                                                ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error"
                                                Text="<%$ Snippet: PhoneNumberFormat, Please enter a valid 10 digit phone number. %>"
                                                ValidationExpression="<%$ Snippet: RegexPhoneNumber %>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="WorkPhoneLabel" CssClass="control-label col-sm-4" AssociatedControlID="WorkPhoneTextBox" Text="<%$ Snippet: My Account Work Phone Label, Work Phone %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="WorkPhoneTextBox" CssClass="input-large"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="WorkPhoneRegEx" runat="server" ControlToValidate="WorkPhoneTextBox"
                                                ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error"
                                                Text="<%$ Snippet: PhoneNumberFormat, Please enter a valid 10 digit phone number. %>"
                                                ValidationExpression="<%$ Snippet: RegexPhoneNumber %>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="CellPhoneLabel" CssClass="control-label col-sm-4" AssociatedControlID="CellPhoneTextBox" Text="<%$ Snippet: My Account Work Phone Label, Cell Phone %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="CellPhoneTextBox" CssClass="input-large"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="CelPhoneRegEx" runat="server" ControlToValidate="CellPhoneTextBox"
                                                ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error"
                                                Text="<%$ Snippet: PhoneNumberFormat, Please enter a valid 10 digit phone number. %>"
                                                ValidationExpression="<%$ Snippet: RegexPhoneNumber %>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="FaxLabel" CssClass="control-label col-sm-4" AssociatedControlID="FaxTextBox" Text="<%$ Snippet: My Account Work Phone Label, Fax %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="FaxTextBox" CssClass="input-large"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="FaxRegEx" runat="server" ControlToValidate="FaxTextBox"
                                                ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error"
                                                Text="<%$ Snippet: PhoneNumberFormat, Please enter a valid 10 digit phone number. %>"
                                                ValidationExpression="<%$ Snippet: RegexPhoneNumber %>" />
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                
                            <asp:Panel runat="server" ID="EmergencyContactPanel">
                            <h4>
                                <crm:Snippet runat="server" DefaultText="Emergency Contact" SnippetName="My Account Emergency Contact Title" /></h4>
                                <div class="my-account-section-content">
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="EmergencyNameLabel" CssClass="control-label col-sm-4" AssociatedControlID="EmergencyNameTextBox" Text="<%$ Snippet: My Account Emergency Name Label, Name %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="EmergencyNameTextBox" CssClass="input-large"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="EmergencyDescriptionLabel" CssClass="control-label col-sm-4" AssociatedControlID="EmergencyDescriptionTextBox" Text="<%$ Snippet: My Account Emergency Description Label, Description %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="EmergencyDescriptionTextBox" CssClass="input-xxlarge" TextMode="MultiLine" Rows="3" Columns="41"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="EmergencyPhoneLabel" CssClass="control-label col-sm-4" AssociatedControlID="EmergencyPhoneTextBox" Text="<%$ Snippet: My Account Emergency Phone Label, Phone %>"></asp:Label>
                                        <div class="controls col-sm-8">
                                            <asp:TextBox runat="server" ID="EmergencyPhoneTextBox" CssClass="input-large"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="EmergencyPhoneValidator" runat="server" ControlToValidate="EmergencyPhoneTextBox"
                                                ValidationGroup="MyAccountUserProfile" Display="Dynamic" CssClass="help-inline error"
                                                Text="<%$ Snippet: PhoneNumberFormat, Please enter a valid 10 digit phone number. %>"
                                                ValidationExpression="<%$ Snippet: RegexPhoneNumber %>" />
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                    
                            <h4>
                                <crm:Snippet runat="server" DefaultText="Self Identification" SnippetName="My Account Self Identification Title" />
                                <input type="checkbox" id="selfIdentSwitch" class="switch-small" checked data-on-label="Hide" data-off-label="Show">
                            </h4>

                            <asp:Panel runat="server" ID="selfIdentPanel">
                                <div class="my-account-section-content">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <asp:Label ID="FirstNationSelfIdentLabel" runat="server" CssClass="checkbox" AssociatedControlID="FirstNationSelfIdentCheckBox">
                                                <asp:CheckBox ID="FirstNationSelfIdentCheckBox" runat="server" />
                                                <asp:Literal ID="FirstNationSelfIdentLiteral" runat="server" Text="<%$ Snippet: My Account Self Ident FirstNation, First Nation %>" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:Label ID="DisabledSelfIdentLabel" runat="server" CssClass="checkbox" AssociatedControlID="DisabledSelfIdentCheckBox">
                                                <asp:CheckBox ID="DisabledSelfIdentCheckBox" runat="server" />
                                                <asp:Literal ID="DisabledSelfIdentLiteral" runat="server" Text="<%$ Snippet: My Account Self Ident Disabled, Person With A Disability %>" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:Label ID="MinoritySelfIdentLabel" runat="server" CssClass="checkbox" AssociatedControlID="MinoritySelfIdentCheckBox">
                                                <asp:CheckBox ID="MinoritySelfIdentCheckBox" runat="server" />
                                                <asp:Literal ID="MinoritySelfIdentLiteral" runat="server" Text="<%$ Snippet: My Account Self Ident Racial Minority, Racialized Group %>" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:Label ID="MetisSelfIdentLabel" runat="server" CssClass="checkbox" AssociatedControlID="MetisSelfIdentCheckBox">
                                                <asp:CheckBox ID="MetisSelfIdentCheckBox" runat="server" />
                                                <asp:Literal ID="MetisSelfIdentLiteral" runat="server" Text="<%$ Snippet: My Account Self Ident Metis, Metis %>" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:Label ID="InuitSelfIdentLabel" runat="server" CssClass="checkbox" AssociatedControlID="InuitSelfIdentCheckBox">
                                                <asp:CheckBox ID="InuitSelfIdentCheckBox" runat="server" />
                                                <crm:Snippet ID="InuitSelfIdentLiteral" runat="server" SnippetName="My Account Self Ident Inuit" DefaultText="Inuit" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:Label ID="LgbtSelfIdentLabel" runat="server" CssClass="checkbox" AssociatedControlID="LgbtSelfIdentCheckBox">
                                                <asp:CheckBox ID="LgbtSelfIdentCheckBox" runat="server" />
                                                <crm:Snippet ID="LgbtSelfIdentLiteral" runat="server" SnippetName="My Account Self Ident LGBT" DefaultText="Lesbian, Gay, Bisexual, Transgender, Queer or Questioning" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:Label ID="AccountWomanSelfIdentLabel" runat="server" CssClass="checkbox" AssociatedControlID="WomanSelfIdentCheckBox">
                                                <asp:CheckBox ID="WomanSelfIdentCheckBox" runat="server" />
                                                <crm:Snippet ID="Snippet10" runat="server" SnippetName="My Account Self Ident Woman" DefaultText="Woman" />
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                    
                            <p class="my-account-section-footer">
                                <asp:Button runat="server" ID="PersonInfoUpdate" ValidationGroup="MyAccountAddress" CssClass="btn btn-primary btn-small" OnClick="PersonalInfoUpdate_OnClick" Text="<%$ Snippet: My Account Non Member Personal Info Button, Update %>" />
                            </p>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>

        <div class="tab-pane" id="tab2">
            <uc1:MyAccountUserCreds runat="server" id="MyAccountUserCreds" />
        </div>

        <div class="tab-pane" id="tab3">
            <iframe id="document-locker-iframe" src="/DocumentLocker/Index" frameborder="0" width="100%" scrolling="no"></iframe>
            <script src="/js/iframeResizer.min.js"></script>
            <script>
                $(document).ready(function () {
                    $('#document-locker-iframe').iFrameResize([{ autoResize: true, enablePublicMethods: true }]);
                });
            </script>
        </div>
    </div>

    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/css/assets/img/ajax-modal-loading.gif" AlternateText="Loading ..." ToolTip="Loading ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="SidebarAbove" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="SidebarTop" runat="server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="SidebarBottom" runat="server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="SidebarBelow" runat="server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="Scripts" runat="server">
    <script src="<%: Url.Content("~/js/bootstrap-datepicker.min.js") %>"></script>
    <script src="<%: Url.Content("~/js/bootstrap-switch.min.js") %>"></script>
    <script>
        function EndRequestHandler(sender, args)
        {
            scrollTo(0, 0);

            var nowTemp = new Date();
            var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
            var nonMemberDateSelection = $('#NonMemberBirthdayWrapper').datepicker(
            {
                autoclose: true,
                endDate: now,
                format: '<%= XrmHelper.GetSiteSettingValueOrDefault("Date Format", "mm-dd-yyyy") %>'
            }).on('changeDate', function (ev)
            {
                if (ev && ev.date)
                {
                    $('#<%= NonMemberBirthdayHiddenField.ClientID %>').val((ev.date.getMonth() + 1) + "-" + ev.date.getDate() + "-" + ev.date.getFullYear());
                }
            });
        }

        $(document).ready(function ()
        {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            $("#content_form").attr("action", "");


            var tabGroup = $('#myAccountTabGroup');
            tabGroup.on('click', 'a', function (e)
            {
                var $this = $(this);
                e.preventDefault();
                window.location.hash = $this.attr('href');
                $this.tab('show');
            });

            if (window.location.hash)
            {
                tabGroup.find('a[href="' + window.location.hash + '"]').tab('show');
            }

            $('#selfIdentSwitch').bootstrapSwitch().on('switch-change', function (e, data)
            {
                if (data.value)
                {
                    $('#<%=selfIdentPanel.ClientID%>').slideDown();
                }
                else
                {
                    $('#<%=selfIdentPanel.ClientID%>').slideUp();
                }
            });

            $('#selfIdentSwitch').bootstrapSwitch('setState', false);

            var nowTemp = new Date();
            var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

            var nonMemberDateSelection = $('#NonMemberBirthdayWrapper').datepicker({
                autoclose: true,
                endDate: now,
                format: '<%= XrmHelper.GetSiteSettingValueOrDefault("Date Format", "mm-dd-yyyy") %>'
            }).on('changeDate', function (ev)
            {
                if (ev && ev.date)
                {
                    $('#<%= NonMemberBirthdayHiddenField.ClientID %>').val((ev.date.getMonth() + 1) + "-" + ev.date.getDate() + "-" + ev.date.getFullYear());
                }
            });

            nonMemberDateSelection.datepicker('setDate', $('#<%= NonMemberBirthdayHiddenField.ClientID %>').val()).datepicker('fill');
        })

        function validatePostalCodeLength(oSrc, args)
        {
            var value = args.Value;
            if (value.indexOf(' ') >= 0)
            {
                args.IsValid = (value.length == 7);
            }
            else
            {
                args.IsValid = (value.length == 6);
            }
        }

    </script>
</asp:Content>
