﻿using System;
using Adxstudio.Xrm.Web.Mvc;
using Site.Pages;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Adxstudio.Xrm.Cms;
using Xrm;
using Microsoft.Xrm.Portal.Web;

namespace Site.Areas.ETFO.Pages
{
    public partial class EventPhotoPage : PortalPage
	{
		protected void Page_Load(object sender, EventArgs args)
		{
            //Response.Write(Portal.Entity.GetAttributeValue<Guid>("adx_parentpageid"));
            var parentEvent= ServiceContext.oems_EventSet.FirstOrDefault(ev=>ev.oems_WebPage.Id==Portal.Entity.GetAttributeValue<Guid>("adx_parentpageid"));

            var photoFiles = ServiceContext.Adx_webfileSet.Where(wf => wf.oems_eventphoto.Id == parentEvent.Id);
            PhotoList.DataSource = photoFiles;
            PhotoList.DataBind();
		}

        public string getUrl(Adx_webfile photo)
        {
            return new UrlBuilder(ServiceContext.GetUrl(photo));
        }
	}
}
