﻿using Adxstudio.Xrm.Web.Mvc.Html;
using Microsoft.Xrm.Portal.Configuration;
using Site.Pages;
using System;
using System.Linq;
using System.Web;
using Site.Helpers;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Xrm;
using System.Web.Script.Serialization;
using Microsoft.Xrm.Portal.Web;

namespace Site.Areas.ETFO.Pages
{
    public partial class DynamicETFORegistrationPage : PortalPage
    {
        public string backUrl;
        public string retrieveEventInfoJSON { get; set; }
        public String registrationStatus { get; set; }
        public String regFormSource { get; set; }
        public String regFormData { get; set; }
        public bool ReadOnlyForm {get;set;}
        public bool IsApproved { get; set; }
        public Guid EventId { get; set; }
        public int EventRegistrationStatus { get; set; }
        public bool RemoveSubmitButton { get; set; }
        public bool IsWaitingListOffered { get; set; }
        public string SubmitButtonText { get; set; }
        public bool waitingListSpostsAvailable { get; set; }

        public Guid ContactId
        {
            get
            {
                return Contact.ContactId.Value;
            }
        }

        Guid EventRegistrationId
        {
            get
            {
                Guid eventRegistrationId = Guid.Empty;
                bool isGuid = ReferenceEquals(Request.QueryString["oems_EventRegistrationId"], null) ? false : Guid.TryParse(Request.QueryString["oems_EventRegistrationId"].ToString(), out eventRegistrationId);

                return isGuid ? eventRegistrationId : Guid.Empty;
            }
        }

        public int Type
        {
            get
            {
                var type = 1;
                var isInt = ReferenceEquals(Request.QueryString["type"], null) ? false : Int32.TryParse(Request.QueryString["type"].ToString(), out type);

                return isInt && (type == 1 || type == 2 || type == 3 || type == 4) ? type : 0;
            }
        }

        private void ShowModalInfoNoEvent()
        {
            regFormData = null;
            ReadOnlyForm = true;
            ShowModalInfo("ShowInfoModalEventRegForm", "Error loading event information", "Sorry there is an error loading event information", "Go to upcoming events", Html.SiteMarkerUrl("Upcoming Events"));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RedirectToLoginIfAnonymous();

            var portalContext = PortalCrmConfigurationManager.CreatePortalContext();
            if (!this.IsPostBack)
            {
                backUrl = this.Request.UrlReferrer != null && this.Request.UrlReferrer.AbsolutePath != "/login/" ? this.Request.UrlReferrer.ToString() : WebsitePathUtility.ToAbsolute(portalContext.Website, VirtualPathUtility.ToAbsolute("~/"));
                ViewState["BackUrl"] = backUrl;
            }
            backUrl = ViewState["BackUrl"] != null ? ViewState["BackUrl"].ToString() : WebsitePathUtility.ToAbsolute(portalContext.Website, VirtualPathUtility.ToAbsolute("~/"));

            eventregistrationid.Value = EventRegistrationId.ToString();

            if (EventRegistrationId == Guid.Empty)
            {
                ShowModalInfoNoEvent();
                return;
            }

            var eventRegistrationColumnSet = new ColumnSet(new String[] { "oems_contact", "oems_dynamicregformjson", "statuscode", "oems_event", "oems_registrationtype" });
            var eventRegistration = (oems_EventRegistration)ServiceContext.Retrieve("oems_eventregistration", EventRegistrationId, eventRegistrationColumnSet);

            // check if account mataches logged in user
            if (!(eventRegistration.Contains("oems_contact") && eventRegistration.Contains("oems_dynamicregformjson") && eventRegistration.Contains("statuscode")))
            {
                ShowModalInfoNoEvent();
                return;
            }

            var registrationContacttRef = (EntityReference)eventRegistration["oems_contact"];
            if (registrationContacttRef.Id != Contact.Id)
            {
                ShowModalInfoNoEvent();
                return;
            }

            var id = ((EntityReference)eventRegistration["oems_event"]).Id;
            var eventColumnSet = new ColumnSet(new String[] { "oems_retrieveeventinformation", "statuscode", "oems_waitinglistcomponentflag", "oems_eventfullflag" });
            var oemsEvent = (oems_Event)ServiceContext.Retrieve("oems_event", id, eventColumnSet);

            SubmitButtonText = XrmHelper.GetSnippetValueOrDefault("EventRegistration/SubmitButtonText", "Submit");
            var isPendingOrPayed = RegistrationHelper.IsPaymentPendingOrPayed(EventRegistrationId);
            var isRegistrationOpen = eventRegistration.oems_RegistrationType == RegistrationHelper.EventRegistrationType.Attendee ?
                RegistrationHelper.RegistrationOpen(Contact.Id, oemsEvent.Id, Portal) :
                RegistrationHelper.ApplicationOpen(Contact.Id, oemsEvent.Id, Portal);
           
            ReadOnlyForm = RegistrationHelper.CanViewRegistration(oemsEvent.statuscode, eventRegistration.statuscode) || isPendingOrPayed;
            if (RegistrationHelper.IsPaymentPendingOrPayed(EventRegistrationId))
            {
                ReadOnlyForm = true;
            }
            else
            {
                bool overrideProvileges = isRegistrationOpen &&
                    (eventRegistration.statuscode == RegistrationHelper.EventRegistration.Initial
                    || eventRegistration.statuscode == RegistrationHelper.EventRegistration.Created
                    || eventRegistration.statuscode == RegistrationHelper.EventRegistration.Sumbitted);

                if (!overrideProvileges && RegistrationHelper.CanViewRegistration(oemsEvent.statuscode, eventRegistration.statuscode))
                {
                    ReadOnlyForm = true;
                }
            }            
            
            IsApproved = eventRegistration.statuscode.Value == RegistrationHelper.EventRegistration.Approved;
            RemoveSubmitButton = ReadOnlyForm;
            var eventFullFlag = oemsEvent.GetAttributeValue<bool>("oems_eventfullflag");
            var waitingListComponentFlag = oemsEvent.GetAttributeValue<bool>("oems_waitinglistcomponentflag");
            if (waitingListComponentFlag && isRegistrationOpen && !isPendingOrPayed)
            {
                //If registration has a waiting list request attached and the status is not Offered, disable or remove Submit button from the reg form. 
                var request = RegistrationHelper.GetRegistrationWithWaitingListRequest(id, EventRegistrationId);
                if (request != null)
                {
                    if (request.GetAttributeValue<OptionSetValue>("statuscode").Value == RegistrationHelper.WaitingListStatus.Offered)
                    {
                        ReadOnlyForm = false;
                        IsWaitingListOffered = true;
                        SubmitButtonText = XrmHelper.GetSnippetValueOrDefault("EventRegistration/SaveWaitingListButtonText", "Confirm Waitng List Offer");
                    }
                    else
                    {
                        RemoveSubmitButton = true;
                    }
                }
                else if (eventFullFlag)
                {
                    waitingListSpostsAvailable = true;
                }
            }

            regFormData = ((string)eventRegistration["oems_dynamicregformjson"]);
            regFormData = regFormData.Replace("'", "\\'").Replace("\"", "\\\"");
            if(ReadOnlyForm)
            {
                var i = regFormData.IndexOf("{");
                if(i > -1)
                {
                    regFormData = regFormData.Substring(0, i+1) + "\"disabled\":true," + regFormData.Substring(i+1);
                }
            }

            EventId = id;
            EventRegistrationStatus = eventRegistration.statuscode.HasValue ? eventRegistration.statuscode.Value : 0;
            retrieveEventInfoJSON = oemsEvent.oems_RetrieveEventInformation;
            registrationStatus = "(" + EntityOptionSet.GetOptionSetLabel("oems_eventregistration", "statuscode", eventRegistration.statuscode) + ")";
        }
    }
}