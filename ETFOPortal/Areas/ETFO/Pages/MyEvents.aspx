﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="MyEvents.aspx.cs" Inherits="Site.Areas.ETFO.Pages.MyEvents" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Import Namespace="Site.Areas.ETFO.Library" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css" rel="stylesheet">
        .Submitted { <% Html.RenderPartialFromSetting("MyEvents/Status/Submitted/Background"); %> } 
        .Created { <% Html.RenderPartialFromSetting("MyEvents/Status/Created/Background"); %> }
        .Approved {<% Html.RenderPartialFromSetting("MyEvents/Status/Approved/Background"); %>}
        .Attended { <% Html.RenderPartialFromSetting("MyEvents/Status/Attended/Background"); %> }
        .Withdrawn{ <% Html.RenderPartialFromSetting("MyEvents/Status/Withdrawn/Background"); %> }
        .Cancelled{ <% Html.RenderPartialFromSetting("MyEvents/Status/Cancelled/Background"); %> }
        .Rejected { <% Html.RenderPartialFromSetting("MyEvents/Status/Rejected/Background"); %> }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageHeader" runat="server">
    <div class="page-header">
		<div class="grid-search">
            <asp:TextBox runat="server" ID="GridSearchText" CssClass="search-query" clientidmode="Static" onkeypress="return EnterEvent(event)" />
            <asp:LinkButton ID="GridSearchButton" runat="server" CssClass="btn search" OnClick="GridSearchButton_Click" >
                <i class="fa fa-search"></i>
            </asp:LinkButton>
		</div>
        <h1>
			<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
		</h1>
	</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/css/assets/img/ajax-modal-loading.gif" AlternateText="Loading ..." ToolTip="Loading ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="portlet box light-grey">
                <asp:ObjectDataSource runat="server" ID="EventListDataSource" EnablePaging="true" 
                    TypeName="Site.Areas.ETFO.Library.EventListings" OnObjectCreating="MyEventsDataSource_OnObjectCreating"
                    SelectMethod="MySelect" SelectCountMethod="MySelectCount" SortParameterName="sortExpression" />
    
                <asp:ListView ID="MyEventsView" runat="server" DataSourceID="EventListDataSource" OnItemDataBound="MyEventsView_DataBound" OnSorting="MyEventsView_OnSorting">
                    <LayoutTemplate>                    
                        <div class="portlet-body">
                            <div class="dataTables_wrapper form-inline my-events" role="grid">
                                <table class="table">
                                    <tr>
                                        <th class="event-link-header" style="text-align: left;">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument="EventRegistrationNumber" CommandName="Sort">
                                                <crm:Snippet ID="Snippet1" runat="server" SnippetName="MyEvents/RegistrationNumberHeader" DefaultText="Number" />
                                                &nbsp;
                                                <i id="sortRegistrationNumber" runat="server" Visible="False" class="fa fa-caret-down rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
                                        <th class="event-title-header" style="text-align: left;">
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandArgument="EventTitle" CommandName="Sort">
                                                <crm:Snippet ID="Snippet2" runat="server" SnippetName="MyEvents/EventTitleHeader" DefaultText="Event Title" />
                                                &nbsp;
                                                <i id="sortTitle" runat="server" Visible="False" class="fa fa-caret-down rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
                                        <th class="event-date-header" style="text-align: left;">
                                            <asp:LinkButton ID="LinkButton5" runat="server" CommandArgument="EventStatus" CommandName="Sort">
                                                <crm:Snippet ID="Snippet6" runat="server" SnippetName="MyEvents/EventStatusHeader" DefaultText="Event Status" />
                                                &nbsp;
                                                <i id="sortStatus" runat="server" Visible="False" class="fa fa-caret-down rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
							            <th class="event-date-header" style="text-align: left;">
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandArgument="EventRegistrationStatusText" CommandName="Sort">
                                                <crm:Snippet ID="Snippet5" runat="server" SnippetName="MyEvents/EventRegistrationStatusHeader" DefaultText="My Status" />
                                                &nbsp;
                                                <i id="sortStatusText" runat="server" Visible="False" class="fa fa-caret-down rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
                                        <th class="event-date-header" style="text-align: left;">
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandArgument="StartDate" CommandName="Sort">
                                                <crm:Snippet ID="Snippet3" runat="server" SnippetName="MyEvents/StartDateHeader" DefaultText="Start Date" />
                                                &nbsp;
                                                <i id="sortStartDate" runat="server" Visible="True" class="icon-caret-up rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
                            
							            <th class="event-date-header" style="text-align: left;">
                                            <asp:LinkButton ID="LinkButton6" runat="server" CommandArgument="EndDate" CommandName="Sort">
                                                <crm:Snippet ID="Snippet4" runat="server" SnippetName="MyEvents/EndDateHeader" DefaultText="End Date" />
                                                &nbsp;
                                                <i id="sortEndDate" runat="server" Visible="False" class="fa fa-caret-down rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
							
                                    </tr>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                                </table>
                            </div>
                        </div>
                        <div class="event-pager">
                            <asp:DataPager  ID="ListDataPager" runat="server" PagedControlID="MyEventsView" PageSize="10" >
                            <Fields>
                                <asp:TemplatePagerField>
                                    <PagerTemplate>
                                        <div class="page-count">
                                        Showing <asp:Label runat="server" ID="StartIndexLabel" Text="<%# Container.TotalRowCount > 0 ? Container.StartRowIndex + 1 : 0 %>" /> to
                                        <asp:Label runat="server" ID="PageSizeLabel" Text="<%#  Container.StartRowIndex + Container.PageSize > Container.TotalRowCount ? Container.TotalRowCount :(Container.StartRowIndex + Container.PageSize) %>" />
                                        of
                                        <asp:Label runat="server" ID="PageCountLabel" Text="<%#  (Container.TotalRowCount) %>" />
                                        entries
                                    </div>
                                    </PagerTemplate>
                                </asp:TemplatePagerField>
                                <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowNextPageButton="False" PreviousPageText="<i class='fa fa-angle-left'></i>" />
                                <asp:NumericPagerField CurrentPageLabelCssClass="active" />
                                <asp:NextPreviousPagerField ShowLastPageButton="false" ShowPreviousPageButton="False" NextPageText="<i class='fa fa-angle-right'></i>" />
                            </Fields>
                        </asp:DataPager>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="EditLink" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <i class="fa fa-caret-right fa-lg icon-spacer"></i><span class="event-detail-txt"><%#Eval("EventRegistrationNumber") %></span>
                                </asp:LinkButton>                            
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton6" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <div> <%#Eval("EventTitle") %> </div>
                                </asp:LinkButton>                            
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton9" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <div> <%#Eval("EventStatusText") %> </div>
                                </asp:LinkButton>
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton10" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                <div ><div class="status-box me-<%#Eval("EventRegistrationStatusText").ToString().Replace(" ", "-") %>"><%#Eval("EventRegistrationStatusText") %></div></div>
                                </asp:LinkButton>
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton11" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                <div ><crm:DateTimeLiteral ID="DateTimeLiteral1" runat="server" Format="MMMM dd, yyyy" Value='<%# Eval("StartDate") %>' OutputTimeZoneLabel="false" /></div>
                                </asp:LinkButton>
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton12" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                <div ><crm:DateTimeLiteral ID="DateTimeLiteral2" runat="server" Format="MMMM dd, yyyy" Value='<%# Eval("EndDate") %>' OutputTimeZoneLabel="false" /></div>
                                </asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <tr class="active">
                            <td class="event-link-cell active">
                                <asp:LinkButton ID="EditLink" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <i class="fa fa-caret-down icon-large icon-spacer rediconcolor"></i><span class="event-detail-txt"><%#Eval("EventRegistrationNumber") %></span>
                                </asp:LinkButton>                            
                            </td>
                            <td class="event-link-cell active">
                                <asp:LinkButton ID="LinkButton7" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <div class="event-link-cell"><%#Eval("EventTitle") %>&nbsp;</div> 
                                </asp:LinkButton>                            
                            </td>
                            <td class="event-link-cell active">
                                <asp:LinkButton ID="LinkButton8" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <div class="event-link-cell"><%#Eval("EventStatusText") %></div>
                                </asp:LinkButton>                            
                            </td>
                            <td class="event-link-cell active">
                                <asp:LinkButton ID="LinkButton13" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                <div class="event-link-cell"><div class="status-box me-<%#Eval("EventRegistrationStatusText").ToString().Replace(" ", "-") %>"><%#Eval("EventRegistrationStatusText") %></div></div>
                                </asp:LinkButton>
                            </td>
                            <td class="event-link-cell active">
                                <asp:LinkButton ID="LinkButton14" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                <div class="event-link-cell"><crm:DateTimeLiteral ID="DateTimeLiteral1" runat="server" Format="MMMM dd, yyyy" Value='<%# Eval("StartDate") %>' OutputTimeZoneLabel="false" /></div>
                                </asp:LinkButton>
                            </td>
                            <td class="event-link-cell active">
                                <asp:LinkButton ID="LinkButton15" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                <div class="event-link-cell"><crm:DateTimeLiteral ID="DateTimeLiteral2" runat="server" Format="MMMM dd, yyyy" Value='<%# Eval("EndDate") %>' OutputTimeZoneLabel="false" /></div>
                                </asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" class="details-row">
                                <!-- Events/Applications  -->
                                <div class="details" style='<%# (int)Eval("ListingType") == 1 ? "display:block;" : "display:none;" %>'>
                                    <div class="row">
                                        <div class="details-heading">
                                            <asp:Label ID="Label4" runat="server" Text="Event"></asp:Label>
                                        </div>
                                        <div class="event-detail-title details-content long" >
                                            <b><a href="<%#Eval("EventUrl") %>"><%#Eval("EventTitle") %></a></b>
                                        </div>
                                        <div style='<%# !String.IsNullOrEmpty((string)Eval("EventRejectionReasonText")) || !String.IsNullOrEmpty((string)Eval("EventRejectionReasonDetails")) ? "display:block;" : "display:none;" %>'>
                                            <div class="details-heading long">
                                                <asp:Label ID="Label5" runat="server" Text="Denied Reason"></asp:Label>
                                            </div>
                                            <div class="details-content long" style="width: 250px;">
                                                <%#Eval("EventRejectionReasonText") %>
                                                <br/>
                                                <%#Eval("EventRejectionReasonDetails") %>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 10px">
                                        <div class="details-heading">
                                            <asp:Label ID="Label1" runat="server" Text="<%$ Snippet:MyEvents/LocationHeader,Location %>"></asp:Label>
                                        </div>
                                        <div class="details-content long">
                                            <%# EventListings.getEventLocationDisplayText((Guid?)Eval("LocationId")) %></div>
                                        </div>
                                    <div class="row" style="margin-top: 10px">
                                        <div class="details-heading long">
                                            <asp:Label ID="Label2" runat="server" Text="<%$ Snippet:MyEvents/ExecutiveOfficerHeader,Executive Officer %>"></asp:Label>
                                        </div>
                                        <div class="details-content"><%# EventListings.getExecutiveStaff((Guid)Eval("EventId")) %></div>
                                        <div class="details-heading">
                                            <asp:Label ID="Label3" runat="server" Text="<%$ Snippet:MyEvents/SupportStaffHeader,Support Staff %>"></asp:Label>
                                        </div>
                                        <div class="details-content"><%# EventListings.getSupportStaff((Guid)Eval("EventId")) %></div>
                                    </div>
                                    <br />
                                    <div id="registrationPeriodsContainer" runat="server" class="row registration-period"></div>
                                    <div id="applicationSchedulesContainer" runat="server" class="row registration-period"></div>
                                </div>
                                
                                <!-- Courses Registrations  -->
                                <div class="details" style='<%# (int)Eval("ListingType") == 2 ? "display:block;" : "display:none;" %>'>
                                    <div class="row">
                                        <div class="details-heading">
                                            <crm:Snippet ID="Snippet11" runat="server" SnippetName="MyCourses/CourseCode" DefaultText="Course Code" />
                                        </div>
                                        <div class="event-detail-title long">
                                            <b><%#Eval("CourseCode") %></b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="details-heading">
                                            <crm:Snippet ID="Snippet18" runat="server" SnippetName="MyCourses/CourseTitle" DefaultText="Course Title" />
                                        </div>
                                        <div class="event-detail-title long">
                                            <b><%#Eval("EventTitle") %></b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="details-heading">
                                            <crm:Snippet ID="Snippet16" runat="server" SnippetName="MyCourses/Description" DefaultText="Description" />
                                        </div>
                                        <div class="details-content long" style="width: 80%;"><%#Eval("CourseDescription") %></div>
                                    </div>
                                    <div class="row" style="margin-top: 10px">
                                        <div class="row" style="width: 50%; float: left;">
                                            <div class="details-heading">
                                                <crm:Snippet ID="Snippet12" runat="server" SnippetName="MyCourses/Topic" DefaultText="Topic" />
                                            </div>
                                            <div class="details-content long" style="width: 35%;"><%#EventListings.getCourseTopic((Guid)Eval("EventId")) %></div>
                                        </div>
                                        <div class="row" style="width: 50%; float: left;">
                                            <div class="details-heading">
                                                <crm:Snippet ID="Snippet19" runat="server" SnippetName="MyCourses/TargetGrades" DefaultText="Target Grades" />
                                            </div>
                                            <div class="details-content long"><%#EventListings.getCourseTargetGrade((Guid)Eval("EventId")) %></div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 10px">
                                        <div class="row" style="width: 50%; float: left;">
                                            <div class="details-heading">
                                                <crm:Snippet ID="Snippet8" runat="server" SnippetName="MyCourses/Presenters" DefaultText="Presenters" />
                                            </div>
                                            <div class="details-content long" style="width: 35%;"><%#EventListings.getCoursePresenters((Guid)Eval("EventId")) %></div>
                                            <%--<div class="details-content long" style="width: 35%;"><%#Eval("Presenter") %></div>--%>
                                        </div>
                                        <div class="row" style="width: 50%; float: left;">
                                            <div class="details-heading">
                                                <crm:Snippet ID="Snippet14" runat="server" SnippetName="MyCourses/Location" DefaultText="Location" />
                                            </div>
                                            <div class="details-content long"><%#EventListings.getCourseLocation((Guid)Eval("EventId")) %></div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 10px">
                                        <div class="row" style="width: 50%; float: left;">
                                            <div class="details-heading">
                                                <crm:Snippet ID="Snippet17" runat="server" SnippetName="CourseListing/DatesOffered" DefaultText="Dates Offered" />
                                            </div>
                                            <div class="details-content long" style="width:50%">
                                                <crm:DateTimeLiteral ID="DateTimeLiteral3" runat="server" Format="MMM dd yyyy" Value='<%# EventListings.getCourseInitialDate((Guid)Eval("EventId")) %>' OutputTimeZoneLabel="false" />
                                                -
                                                <crm:DateTimeLiteral ID="DateTimeLiteral4" runat="server" Format="MMM dd yyyy" Value='<%# EventListings.getCourseFinalDate((Guid)Eval("EventId")) %>' OutputTimeZoneLabel="false" />
                                            </div>
                                        </div>
                                        <div class="row" style="width: 50%; float: left;">
                                            <div id="registrationPeriodsContainer_Course" runat="server" class="row registration-period"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                <!-- Courses Applications  -->
                                <div class="details" style='<%# (int)Eval("ListingType") == 3 ? "display:block;" : "display:none;" %>'>
                                    <div class="row">
                                        <div class="details-heading">
                                            <crm:Snippet ID="Snippet3" runat="server" SnippetName="MyCourseTermApplications/CourseCode" DefaultText="Term Code" />
                                        </div>
                                        <div class="event-detail-title details-content long" style="width: 84%;">
                                            <b><a href="<%#Eval("EventUrl") %>"><%#Eval("TermCodeName") %></a></b>
                                        </div>
                                    </div>                                 
                                    <div class="row">
                                        <div class="details-heading">
                                            <crm:Snippet ID="Snippet7" runat="server" SnippetName="MyCourseTermApplications/CourseTitle" DefaultText="Term Name" />
                                        </div>
                                        <div class="details-content long" ><a href="<%#Eval("EventUrl") %>"><%#Eval("TermName") %></a></div>
                                        <div>
                                            <asp:Label ID="Label9" runat="server" Text="Denied Reason" Visible='<%# !String.IsNullOrEmpty((string)Eval("EventRejectionReasonText")) || !String.IsNullOrEmpty((string)Eval("EventRejectionReasonDetails")) %>'></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="details-heading">
                                            <crm:Snippet ID="Snippet9" runat="server" SnippetName="MyCourseTermApplications/TermName" DefaultText="Course Title" />
                                        </div>
                                        <div class="details-content long" style="width: 84%;"><%#Eval("EventTitle") %></div>
                                        <div class="details-content long" style="width: 250px;">
                                            <%#Eval("EventRejectionReasonText") %>
                                            <br/>
                                            <%#Eval("EventRejectionReasonDetails") %>
                                        </div>
                                    </div>
                                </div>

                                <!-- General buttons  -->
                                <div class="details">
                                    <div class="row registration-period registration-buttons-myevents">
                                        <div class="me-actions">
                                            <asp:Button ID="ManageRegistration" runat="server" Text="<%$ Snippet: MyEvents/ManageRegistrationButtonText, Modify Registration %>" CommandArgument='<%# Eval("EventRegistrationId") + "," + Eval("ListingType") %>' OnCommand="ManageRegistration_Command" CssClass="btn event-btn orange-btn"></asp:Button>
                                            <asp:Button ID="ViewRegistration" runat="server" Text="<%$ Snippet: MyEvents/ViewRegistrationButtonText, View Registration %>" CommandArgument='<%# Eval("EventRegistrationId") + "," + Eval("ListingType") %>' OnCommand="ManageRegistration_Command" CssClass="btn event-btn orange-btn"></asp:Button>
                                            <asp:Button ID="AcceptWaitingListOffer" runat="server" Visible="False" Text="<%$ Snippet: MyEvents/WaitingList/AcceptWaitingListOffer, Accept Waiting List Offer %>" CommandArgument='<%# Eval("EventId") + "~" + Eval("EventRegistrationId") %>' OnCommand="AcceptWaitingListOffer_Command" CssClass="btn event-btn orange-btn"></asp:Button>
                                            <asp:Button ID="RejectWaitingListOffer" runat="server" Visible="False" Text="<%$ Snippet: MyEvents/WaitingList/RejectWaitingListOffer, Reject Waiting List Offer %>" CssClass="btn event-btn orange-btn" OnClientClick='<%# "javascript:return showConfirmationRejectWaitingList(\"" + RejectWaitingListConfirmText + "\", \"" + Eval("EventId") + "\", \"" + Eval("EventRegistrationId") + "\", \"Yes\", \"No\");" %>'></asp:Button>
                                            <asp:Button ID="ManageApplication" runat="server" Text="<%$ Snippet: MyEvents/ManageApplicationButtonText, Modify Application %>" CommandArgument='<%# Eval("EventRegistrationId") %>' OnCommand="ManageRegistration_Command" CssClass="btn event-btn orange-btn"></asp:Button>
                                            <asp:Button ID="ViewApplication" runat="server" Text="<%$ Snippet: MyEvents/ViewApplicationButtonText, View Application %>" CommandArgument='<%# Eval("EventRegistrationId") %>' OnCommand="ManageRegistration_Command" CssClass="btn event-btn orange-btn"></asp:Button>
                                            <asp:Button ID="ManageDelegation" runat="server" Text="<%$ Snippet: MyEvents/ManageLocalDelegationButtonText, Manage Local Delegation %>" CommandArgument='<%# Eval("EventRegistrationId") %>' OnCommand="ManageDelegation_Command" CssClass="btn event-btn navy-btn" Visible='<%# ShowManageDelegationButton(Eval("EventLocalDelegationComponentFlag")) %>'></asp:Button>
                                            <asp:Button ID="CancelRegistration" runat="server" Text="<%$ Snippet: MyEvents/CancelButtonText, Cancel Registration %>" CssClass="btn event-btn blue-btn" Visible='<%# ((RegistrationHelper.MyEventConfiguration)Eval("EventConfig")).showCancelRegistrationButton %>' OnClientClick='<%# "javascript:return showConfirmationWindow(\"" + CancelConfirmText + "\", \"" + Eval("EventRegistrationId") + "\", \"Yes\", \"No\", false);" %>'></asp:Button>
                                            <asp:Button ID="CancelApplication" runat="server" Text="<%$ Snippet: MyEvents/CancelApplicationButtonText, Cancel Application %>"  CssClass="btn event-btn blue-btn" Visible='<%# ((RegistrationHelper.MyEventConfiguration)Eval("EventConfig")).showCancelApplicationButton %>' OnClientClick='<%# "javascript:return showConfirmationWindow(\"" + CancelApplicationConfirmText + "\", \"" + Eval("EventRegistrationId") + "\", \"Yes\", \"No\", false);" %>'></asp:Button>
                                            <asp:Button ID="WithdrawRegistration" runat="server" Text="<%$ Snippet: MyEvents/WithdrawButtonText, Withdraw Registration %>" CssClass="btn event-btn blue-btn" Visible='<%# ((RegistrationHelper.MyEventConfiguration)Eval("EventConfig")).showWithdrawRegistrationButton %>' OnClientClick='<%# "javascript:return showConfirmationWindow(\"" + WithdrawConfirmText + "\", \"" + Eval("EventRegistrationId") + "\", \"Yes\", \"No\", true);" %>'></asp:Button>
                                            <asp:Button ID="WithdrawApplication" runat="server" Text="<%$ Snippet: MyEvents/WithdrawApplicationButtonText, Withdraw Application %>" CssClass="btn event-btn blue-btn" Visible='<%# ((RegistrationHelper.MyEventConfiguration)Eval("EventConfig")).showWithdrawApplicationButton %>' OnClientClick='<%# "javascript:return showConfirmationWindow(\"" + WithdrawApplicationConfirmText + "\", \"" + Eval("EventRegistrationId") + "\", \"Yes\", \"No\", true);" %>'></asp:Button>
                                            <asp:Button ID="MyItinerary" runat="server" Text="<%$ Snippet: MyEvents/MyItineraryButtonText, My Itinerary %>" CommandArgument='<%# Eval("EventRegistrationId") %>' OnCommand="MyItinerary_Command" CssClass="btn event-btn yellow-btn" Visible='<%# ((RegistrationHelper.MyEventConfiguration)Eval("EventConfig")).showMyItineraryButton %>'></asp:Button>
                                            <asp:Button ID="MyCoursePackage" runat="server" Text="<%$ Snippet: MyEvents/MyCoursePackageButtonText, My Course Package %>" CssClass="btn event-btn yellow-btn" OnClientClick='<%# "javascript:return downloadCoursePackage(\"" + Eval("CoursePackageDocument") + "\");" %>'></asp:Button>
                                            <asp:Button ID="MyCourseCertificate" runat="server" Text="<%$ Snippet: MyEvents/MyCourseCertificateButtonText, My Course Certificate %>" CssClass="btn event-btn yellow-btn" OnClientClick='<%# "javascript:return downloadCourseCertificate(\"" + Eval("EventRegistrationId") + "\");" %>'></asp:Button>
                                        </div>
                                        <asp:Label ID="waitingListLabel" runat="server" Visible="False" style="font-weight: bold; display: block;"></asp:Label>
                                    </div>
                                    <asp:Panel ID="billingPnl" runat="server">
                                        <div class="row registration-period">
                                            <div class="span7">
                                                Your registration was successfully submitted with payment. Please contact ETFO to make any changes
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
                    </EditItemTemplate>
                    <EmptyDataTemplate>
                        <b><crm:Snippet ID="Snippet13" runat="server" SnippetName="MyEvents/NoResultsFound" DefaultText="No events found" /></b>
                    </EmptyDataTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <div class="modal fade" style="" id="confirmationWindow" tabindex="-1" role="dialog" aria-labelledby="ConformationWindowlLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body" id="confirmationWindowBody"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary red" data-dismiss="modal" id="cancelBtn">Cancel</button>
            <button type="button" class="btn btn-primary" id="continueBtn">Continue</button>
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" style="" id="informationWindow" tabindex="-1" role="dialog" aria-labelledby="ConformationWindowlLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body" id="informationWindowBody"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="reloadBtn">Continue</button>
          </div>
        </div>
      </div>
    </div>

    <script src="~/js/jquery.fileDownload.js"></script>
    <script>
        function showConfirmationRejectWaitingList(message, eventId, eventRegistrationId, continueText, cancelText)
        {
            if (continueText)
            {
                $("#continueBtn").html(continueText);
            }
            else
            {
                $("#continueBtn").html('Continue');
            }

            if (cancelText)
            {
                $("#cancelBtn").html(cancelText);
            }
            else
            {
                $("#cancelBtn").html('Cancel');
            }

            $("#confirmationWindowBody").html(message);

            $("#continueBtn").unbind("click");
            $("#continueBtn").click
            (
                function () {
                    $("#continueBtn").unbind("click");
                    $("#confirmationWindow").modal('hide');

                    $.ajax
                        ({
                            type: "POST",
                            url: "/api/registrationform/RejectWaitingListOffer?eventid=" + eventId + "&registrationId=" + eventRegistrationId,
                            contentType: "application/json",
                            statusCode:
                            {
                                400: function ()
                                {
                                    $("#informationWindowBody").html("Something went wrong. Please try again or contact support.");

                                    $("#reloadBtn").unbind("click");
                                    $("#reloadBtn").click(function ()
                                    {
                                        $("#informationWindow").modal('hide');
                                    });
                                },
                                200: function (response)
                                {
                                    window.location.reload(true);
                                }
                            }
                        });
                }
            );
            $("#confirmationWindow").modal('show');
            return false;
        }

        function showConfirmationWindow(message, eventRegistrationId, continueText, cancelText, isWithdrawn)
        {
            if (continueText)
            {
                $("#continueBtn").html(continueText);
            }
            else
            {
                $("#continueBtn").html('Continue');
            }

            if (cancelText)
            {
                $("#cancelBtn").html(cancelText);
            }
            else
            {
                $("#cancelBtn").html('Cancel');
            }

            $("#confirmationWindowBody").html(message);

            $("#continueBtn").unbind("click");
            $("#continueBtn").click
            (
                function () {
                    $("#continueBtn").unbind("click");
                    $("#confirmationWindow").modal('hide');

                    $.ajax
                    ({
                        type: "POST",
                        url: "/api/registrationform/CancelOrWithdrawnRegistration?eventRegistrationId=" + eventRegistrationId + "&withdrawn=" + isWithdrawn,
                        contentType: "application/json",
                        statusCode:
                        {
                            400: function ()
                            {
                                $("#informationWindowBody").html("Something went wrong. Please try again or contact support.");

                                $("#reloadBtn").unbind("click");
                                $("#reloadBtn").click(function ()
                                {
                                    $("#informationWindow").modal('hide');
                                });
                            },
                            200: function (response)
                            {
                                if (response)
                                {
                                    $("#informationWindowBody").html(response);

                                    $("#reloadBtn").unbind("click");
                                    $("#reloadBtn").click(function ()
                                    {
                                        window.location.reload(true);
                                    });

                                    $("#informationWindow").modal('show');
                                }
                                else
                                {
                                    window.location.reload(true);
                                }
                            }
                        }
                    });
                }
            );
            $("#confirmationWindow").modal('show');
            return false;
        }

        function downloadCoursePackage(uid) {
            var url = '/Attachments/DownloadDocumentByObjectId/' + uid;
            $.fileDownload
            (
                url,
                {
                    httpMethod: 'POST',
                    cookieName: "OMWFileDownload",
                    failCallback: function (html, url) {
                        alert('There was an error downloading the document. Please try again');
                    }
                }
            );
        }

        function downloadCourseCertificate(uid) {
            var url = '/Attachments/DownloadCourseCertificate/' + uid;
            $.fileDownload
            (
                url,
                {
                    httpMethod: 'POST',
                    cookieName: "OMWCourseCertificateDownload",
                    failCallback: function (html, url)
                    {
                        alert('There was an error downloading the document. Please try again');
                    }
                }
            );
        }
    </script>
</asp:Content>

<asp:Content ID="Content13" ContentPlaceHolderID="SidebarAbove" runat="server">
    <ul class="etfo-event-nav pre-nav nav nav-tabs nav-stacked">
        <li class="item-purple">
            <crm:CrmHyperLink runat="server" ID="HyperLinkUpcoming" Text="<%$ Snippet: Upcoming Events Label, Upcoming Events %>" SiteMarkerName="Upcoming Events"></crm:CrmHyperLink>
        </li>
        <li class="item-pink">
            <crm:CrmHyperLink runat="server" ID="HyperLinkHistorical" Text="<%$ Snippet: Historical Events Label, Historical Events %>" SiteMarkerName="Historical Events"></crm:CrmHyperLink>
        </li>
    </ul>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="SidebarBottom" runat="server">
    <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/ETFOnews" data-widget-id="392320152093458432">Tweets by @ETFOnews</a>
    <script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");</script>
</asp:Content>

<asp:Content ID="Content11" ContentPlaceHolderID="SidebarBelow" runat="server">
    <adx:AdPlacement ID="AdPlacement1" runat="server" PlacementName="Sidebar Bottom" CssClass="ad" />
</asp:Content>

<asp:Content ID="Content12" ContentPlaceHolderID="Scripts" runat="server">
    <script type="text/javascript">
        var searchid = "<%=GridSearchID%>";
        var searchPlaceHolder = "<%=GridSearchPlaceholder %>";
        $(searchid).attr('placeholder', searchPlaceHolder);

        function EnterEvent(e) {
            if (e.keyCode == 13) {
                __doPostBack('<%=GridSearchButton.UniqueID%>', "");
            }
        }
    </script>
</asp:Content>
