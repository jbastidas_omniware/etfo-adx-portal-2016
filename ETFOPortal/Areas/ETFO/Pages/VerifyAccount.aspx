﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="VerifyAccount.aspx.cs" Inherits="Site.Areas.ETFO.Pages.VerifyAccount" %>
<%@ Register src="~/Controls/Comments.ascx" tagname="Comments" tagprefix="adx" %>
<%@ Register src="~/Controls/MultiRatingControl.ascx" tagname="MultiRatingControl" tagprefix="adx" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<crm:Snippet ID="Snippet4" SnippetName="Social Share Widget Code Page Top" EditType="text" DefaultText="" runat="server"/>
	<asp:PlaceHolder runat="server" ID="VerifyAccountSuccessPlaceHolder" Visible="False">
		<crm:Snippet ID="Snippet5" SnippetName="VerifyAccount/SuccessMessage" EditType="text" DefaultText="VerifyAccount/SuccessMessage" runat="server" />
	</asp:PlaceHolder>
	<asp:PlaceHolder runat="server" ID="VerifyAccountFailedPlaceHolder" Visible="False">
		<crm:Snippet ID="Snippet6" SnippetName="VerifyAccount/ErrorMessage" EditType="text" DefaultText="VerifyAccount/ErrorMessage" runat="server"/>
	</asp:PlaceHolder>
    
    <asp:Panel runat="server" ID="ChangeUsernamePanel" CssClass="form-horizontal" Visible="False">
        <div class="control-group">
            <asp:Label runat="server" ID="UserNameExistingLabel" CssClass="control-label" AssociatedControlID="UserNameExistingDataLabel" Text="<%$ Snippet: My Account Change Username Existing Label, Current Username %>"/>
            <div class="controls">
                <asp:Label runat="server" ID="UserNameExistingDataLabel" CssClass="label-control" />
            </div>
        </div>
        <div class="control-group">
            <asp:Label runat="server" ID="UsernameNewLabel" CssClass="control-label" AssociatedControlID="UsernameNewDataLabel" Text="<%$ Snippet: My Account Change Username New Label, New Username %>"/>
            <div class="controls">
                <asp:Label runat="server" ID="UsernameNewDataLabel" CssClass="label-control" />
            </div>
        </div>
	    <div class="control-group">
		    <label class="control-label required" for="Password"><crm:Snippet runat="server" SnippetName="Account Verification Password Label" DefaultText="Password"/></label>
		    <div class="controls">
			    <asp:TextBox ID="Password" runat="server" textMode="Password"></asp:TextBox>
			    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ValidationGroup="ChangeUsername" Display="None" ErrorMessage="Password is a required field." Text="" CssClass="help-inline error"></asp:RequiredFieldValidator>
		    </div>
	    </div>
        <div class="form-actions">
            <asp:Button runat="server" ID="ChangeUsernameButton" CssClass="btn btn-primary" Text="<%$ Snippet: Account Verfication Change Username Button, Confirm Username Change %>" OnClick="ChangeUsernameButton_OnClick"/>
        </div>
    </asp:Panel>
    
    <asp:Panel ID="ChangeUsernameResultPanel" runat="server" CssClass="alert" Visible="false">
        <asp:Literal ID="ChangeUsernameResultLiteral" runat="server"></asp:Literal>
    </asp:Panel>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentBottom" runat="server">
	<div class="page-metadata clearfix">
		<div class="page-last-updated">
			<crm:Snippet ID="Snippet1" SnippetName="Page Modified On Prefix" DefaultText="This page was last updated" EditType="text" runat="server"/>
			<abbr class="timeago">
				<%: string.Format("{0:r}", Html.AttributeLiteral("modifiedon")) %>
			</abbr>.
			<% var displayDate = Html.AttributeLiteral("adx_displaydate"); %>
			<% if (displayDate != null) { %>
				<crm:Snippet ID="Snippet2" SnippetName="Page Published On Prefix" DefaultText="It was published" EditType="text" runat="server"/>
				<abbr class="timeago">
					<%: string.Format("{0:r}", displayDate) %>
				</abbr>.
			<% } %>
		</div>
		<crm:Snippet ID="Snippet3" SnippetName="Social Share Widget Code Page Bottom" EditType="text" DefaultText="" runat="server"/>
	</div>
	<adx:Comments ID="Comments1" RatingType="vote" EnableRatings="false" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="SidebarBottom" runat="server">
	<div class="section">
		<adx:MultiRatingControl ID="MultiRatingControl" RatingType="rating" runat="server" />
	</div>
</asp:Content>

