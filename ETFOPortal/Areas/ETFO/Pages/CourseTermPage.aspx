﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/ETFO/MasterPages/WebFormsEventContent.master" AutoEventWireup="true" CodeBehind="CourseTermPage.aspx.cs" Inherits="Site.Areas.ETFO.Pages.CourseTermPage" %>
<%@ Import Namespace="Adxstudio.Xrm.Blogs" %>
<%@ Import Namespace="Site.Areas.ETFO.Library" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <!-- ace settings handler -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/knockout/2.3.0/knockout-min.js"></script>
    <link rel="stylesheet" href="<%= Page.ResolveUrl("~/css/carouselHeaderStyle.css") %>" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageHeader" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <div id="eventFullText" runat="server" Visible="False" style="font-weight: bold;"></div>
    <div id="HeaderContainer"></div>
    
    <crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />

    <!-- Dynamic HTML -->
    <div class="page-copy xrm-attribute xrm-editable-html" style="font-size: 14px; line-height: 20px;">
	    <div id="dynamichtml" class="xrm-attribute-value" style="font-size: 14px; line-height: 20px;" runat="server"></div>
    </div>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/js/carouselHeaderEngine.js") %>"></script>
    <script type="text/javascript">
        var myModel = <%= retrieveEventInfoJSON %>;
        model = myModel;
        model.dateFirstPart = ko.computed(function() {
            return this.eventDate.substring(0, this.eventDate.indexOf("-") - 1);
        }, model);
        model.dateSecondPart = ko.computed(function() {
            return this.eventDate.substring(this.eventDate.indexOf("-") + 1);
        }, model);

        urlfile = "~/Controls/CarouselHeaderCourseTerm.html";
        controlId = HeaderContainer;
        var eventOpenApplication = parseInt('<%= RegistrationHelper.EventStatus.OpenApplication %>');
    </script>

    <script>
        $(document).ready(function () {
            //$('#landing-carousel').carousel({
            //    interval: 5000,
            //    pause: "false"
            //});
	        
            $('#landing-carousel').carousel(portal.carouselSettings);
            
            $('#playButton').hide();
            $('#playButton').click(function () {
                $('#landing-carousel').carousel('cycle');
                $('#pauseButton').show();
                $('#playButton').hide();
            });
            $('#pauseButton').click(function () {
                $('#landing-carousel').carousel('pause');
                $('#pauseButton').hide();
                $('#playButton').show();
            });
           
        });
    </script>

    <asp:Panel runat="server" ID="CarouselPanel" Visible="False">
        <asp:ObjectDataSource ID="CarouselDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateCarouselAggregateDataAdapter" SelectMethod="SelectPosts" runat="server">
            <SelectParameters>
                <asp:Parameter Name="startRowIndex" DefaultValue="0" />
                <asp:Parameter Name="maximumRows" DefaultValue='<%$ SiteSetting: Landing Page Carousel Post Count, 4 %>' />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ListView ID="CarouselListView" DataSourceID="CarouselDataSource" runat="server">
            <LayoutTemplate>
                <div id="landing-carousel" class="etfo-carousel-landing carousel slide" style="margin-bottom: 40px;">
                    <div class="carousel-inner">
                        <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                    </div>
                    
                    <div id="carouselButtons">
                        <a class="etfo-carousel-control left" href="#landing-carousel" data-slide="prev"><i class="icon-angle-left"></i></a>
                        <a class="etfo-carousel-control right" href="#landing-carousel" data-slide="next"><i class="icon-angle-right"></i></a>
                        <button id="playButton" type="button" class="playpausebutton">
                            <i class="icon-play icon-large"></i>
                        </button>
                        <button id="pauseButton" type="button" class="playpausebutton" >
                            <i class="icon-pause icon-large"></i>
                        </button>
                    </div>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <div class="item <%# Container.DataItemIndex == 0 ? @"active" : string.Empty %>">
                    <asp:Image ImageUrl='<%# GetThumbnailUrl(Eval("Entity")) %>' title='<%# Eval("Title") %>' alt='<%# Eval("Summary") %>' runat="server" />
                    <div class="carousel-caption">
                        <h4>
                            <asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AppRelativePath") %>' alt='<%# Eval("Summary") %>' runat="server"><%# Eval("Title") %></asp:HyperLink>
                        </h4>
                    </div>
                </div>
            </ItemTemplate>
        </asp:ListView>
    </asp:Panel>

    <asp:Panel runat="server" ID="AnnouncementPanel" Visible="False" CssClass="etfo-announcement-list">
        <asp:ObjectDataSource ID="AnnouncementDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateAnnouncementAggregateDataAdapter" SelectMethod="SelectPosts" runat="server">
            <SelectParameters>
                <asp:Parameter Name="startRowIndex" DefaultValue="0" />
                <asp:Parameter Name="maximumRows" DefaultValue='<%$ SiteSetting: Landing Page Announcement Post Count, 4 %>' />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ListView ID="AnnouncementListView" DataSourceID="AnnouncementDataSource" runat="server">
            <LayoutTemplate>
                <asp:ObjectDataSource ID="AnnouncementBlogDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateAnnouncementAggregateDataAdapter" SelectMethod="Select" runat="server" />
                <div class="header">
                    <asp:Repeater DataSourceID="AnnouncementBlogDataSource" runat="server">
                        <ItemTemplate>
                            <h2>
                                <%# Eval("Title") %>
                                <asp:HyperLink NavigateUrl='<%# Eval("FeedPath.AbsolutePath") %>' ImageUrl="~/img/feed-icon-14x14.png" ToolTip='<%$ Snippet: Landing Page Announcement Feed Subscribe Tooltip Label, Subscribe %>' runat="server" />
                            </h2>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <ul>
                    <li id="itemPlaceholder" runat="server" />
                </ul>
                <div class="footer">
                    <asp:Repeater DataSourceID="AnnouncementBlogDataSource" runat="server">
                        <ItemTemplate>
                            <asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AbsolutePath") %>' Text='<%$ Snippet: Landing Page All Announcements Link Text, All Announcements %>' runat="server" />
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <li class="etfo-announcement-item" runat="server">
                    <h3>
                        <asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AppRelativePath") %>' runat="server"><%# Eval("Title") %></asp:HyperLink>
                    </h3>
                    <div><%# Eval("Summary") %></div>
                    <asp:Panel runat="server" Visible='<%# (((BlogCommentPolicy)Eval("CommentPolicy")) == BlogCommentPolicy.None) %>'>
                        <abbr class="posttime"><%# Eval("Entity.Adx_date", "{0:r}") %></abbr>
                    </asp:Panel>
                    <asp:Panel runat="server" Visible='<%# (((BlogCommentPolicy)Eval("CommentPolicy")) != BlogCommentPolicy.None) %>'>
                        <%# Eval("Entity.Adx_date", "{0:f}") %>
								&ndash;
								<asp:HyperLink ID="HyperLink4" NavigateUrl='<%# string.Format("{0}#comments", Eval("ApplicationPath.AbsolutePath")) %>' runat="server">
									<i class="fa fa-comment"></i> <%# Eval("CommentCount") %>
                                </asp:HyperLink>
                    </asp:Panel>
                </li>
            </ItemTemplate>
        </asp:ListView>
    </asp:Panel>

    <asp:Panel runat="server" ID="BlogPanel" Visible="False" CssClass="etfo-blog-list">
        <asp:ObjectDataSource ID="BlogDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateBlogAggregateDataAdapter" SelectMethod="SelectPosts" runat="server">
            <SelectParameters>
                <asp:Parameter Name="startRowIndex" DefaultValue="0" />
                <asp:Parameter Name="maximumRows" DefaultValue='<%$ SiteSetting: Landing Page Blog Post Count, 4 %>' />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ListView ID="BlogListView" DataSourceID="BlogDataSource" runat="server">
            <LayoutTemplate>
                <asp:ObjectDataSource ID="DefaultBlogDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateBlogAggregateDataAdapter" SelectMethod="Select" runat="server" />
                <div class="header">
                    <asp:Repeater DataSourceID="DefaultBlogDataSource" runat="server">
                        <ItemTemplate>
                            <h2>
                                <%# Eval("Title") %>
                                <asp:HyperLink NavigateUrl='<%# Eval("FeedPath.AbsolutePath") %>' ImageUrl="~/img/feed-icon-14x14.png" ToolTip='<%$ Snippet: Landing Page Blog Posts Feed Subscribe Tooltip Label, Subscribe %>' runat="server" />
                            </h2>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <ul>
                    <li id="itemPlaceholder" runat="server" />
                </ul>
                <div class="footer">
                    <asp:Repeater DataSourceID="DefaultBlogDataSource" runat="server">
                        <ItemTemplate>
                            <asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AbsolutePath") %>' Text='<%$ Snippet: Landing Page All Blog Posts Link Text, All Blog Posts %>' runat="server" />
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <li class="etfo-blog-item clearfix" runat="server">
                    <div class="blog-image">
                        <asp:Image ImageUrl='<%# GetThumbnailUrl(Eval("Entity")) %>' runat="server" />
                        <div class="blog-image-title">
                            <%# Eval("Title") %>
                            <span class="blog-title-fade"></span>
                        </div>
                    </div>
                    <div class="blog-content">
                        <h3>
                            <asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AppRelativePath") %>' runat="server"><%# Eval("Title") %></asp:HyperLink>
                        </h3>
                        <asp:Panel runat="server" CssClass="blog-date-comment" Visible='<%# (((BlogCommentPolicy)Eval("CommentPolicy")) == BlogCommentPolicy.None) %>'>
                            <i class="fa fa-calendar"></i>
                            <abbr class="posttime"><%# Eval("Entity.Adx_date", "{0:d}") %></abbr>
                        </asp:Panel>
                        <asp:Panel runat="server" CssClass="blog-date-comment" Visible='<%# (((BlogCommentPolicy)Eval("CommentPolicy")) != BlogCommentPolicy.None) %>'>
                            <span class="blog-date"><i class="fa fa-calendar"></i><%# Eval("Entity.Adx_date", "{0:d}") %></span>
                            <asp:HyperLink NavigateUrl='<%# string.Format("{0}#comments", Eval("ApplicationPath.AbsolutePath")) %>' CssClass="blog-comment" runat="server">
										<i class="fa fa-comment"></i> <%# Eval("CommentCount") %>
                            </asp:HyperLink>
                        </asp:Panel>
                        <div class="blognewssummary"><%# Eval("Summary") %><asp:HyperLink ID="HyperLink1" CssClass="blogsummaryreadmore" NavigateUrl='<%# Eval("ApplicationPath.AppRelativePath") %>' runat="server">Read More</asp:HyperLink></div> 
                        <%--<p>
                            <asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AppRelativePath") %>' runat="server">Read More</asp:HyperLink>
                        </p>--%>
                    </div>
                </li>
            </ItemTemplate>
        </asp:ListView>
    </asp:Panel>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="GridContent" runat="server">
    <% if (loadCoursesListing) { %>
    
        <!--Search bar -->
        <div class="page-heading">
            <div class="page-header">
		        <div class="grid-search">
                    <asp:TextBox runat="server" ID="GridSearchText" CssClass="search-query" clientidmode="Static" onkeypress="return EnterEvent(event)" />
                    <asp:LinkButton ID="GridSearchButton" runat="server" CssClass="btn search" OnClick="GridSearchButton_Click" ><i class="fa fa-search"></i></asp:LinkButton>
		        </div>
                <h1>
                    <span class="xrm-attribute xrm-editable-text" style="font-size: 38px; line-height: 40px;">
                        <span class="xrm-attribute-value" style="font-size: 38px; line-height: 40px;">
                            <crm:Snippet ID="Snippet6" runat="server" SnippetName="CourseListing/Title" DefaultText="Course Listing" />
                        </span>
                    </span>
		        </h1>
	        </div>
        </div>
    
        <!-- Courses Login Message-->
        <asp:LoginView ID="LoginView1" runat="server" >
            <AnonymousTemplate>
                <div class="loginMsg">
                    <crm:Snippet ID="Snippet1" runat="server" SnippetName="CousesListing/LoginMessage1" DefaultText="Members must " />
                    <a href="<%: Html.SignInUrl() %>"><crm:Snippet ID="Snippet6" runat="server" SnippetName="CoursesListing/LoginMessage2" DefaultText="login " /></a>
                    <crm:Snippet ID="Snippet7" runat="server" SnippetName="CousesListing/LoginMessage3" DefaultText="to view members only courses." />
                </div>
            </AnonymousTemplate>
        </asp:LoginView>
    
        <!-- Courses Listing Grid -->
        <asp:UpdatePanel ID="UpdatePanelXA" runat="server">
            <ContentTemplate>
                <asp:UpdateProgress ID="updateProgress" runat="server">
                    <ProgressTemplate>
                        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/css/assets/img/ajax-modal-loading.gif" AlternateText="Loading ..." ToolTip="Loading ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <div>
                    <asp:ObjectDataSource runat="server" ID="CourseListDataSource" EnablePaging="true" TypeName="Site.Areas.ETFO.Library.CourseListings" 
                        OnSelecting ="CoursesListingDataSource_Selecting" OnObjectCreating="CoursesListingDataSource_OnObjectCreating" 
                        SelectMethod="Select" SelectCountMethod="SelectCount" SortParameterName="sortExpression" />
    
                    <asp:ListView ID="CoursesListingView" runat="server" DataSourceID="CourseListDataSource" OnSorting="CoursesListingView_OnSorting" OnLayoutCreated="CoursesListingView_OnLayoutCreated" OnItemDataBound="CoursesListingView_DataBound">
                        <LayoutTemplate>
                            <div class="portlet-body">
                                <div class="dataTables_wrapper form-inline " role="grid">
                                    <table class="table">
                                        <tr>
                                            <th class="event-link-header">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument="Location" CommandName="Sort">
                                                    <crm:Snippet ID="Snippet1" runat="server" SnippetName="CourseListing/LocationHeader" DefaultText="Location" />
                                                    &nbsp;
                                                    <i id="sortLocation" runat="server" Visible="False" class="icon-caret-down rediconcolor"></i>
                                                </asp:LinkButton>
                                            </th>
                                            <th class="event-title-header" style="text-align: left;">
                                                <asp:LinkButton ID="LinkButton5" runat="server" CommandArgument="Topic" CommandName="Sort">
                                                    <crm:Snippet ID="Snippet6" runat="server" SnippetName="CourseListing/TopicHeaderHeader" DefaultText="Topic" />
                                                    &nbsp;
                                                    <i id="sortTopic" runat="server" Visible="False" class="icon-caret-down rediconcolor"></i>
                                                </asp:LinkButton>
                                            </th>
							                <th class="event-link-header">
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandArgument="EventTitle" CommandName="Sort">
                                                    <crm:Snippet ID="Snippet2" runat="server" SnippetName="CourseListing/CourseTitleHeader" DefaultText="Course Title" />
                                                    &nbsp;
                                                    <i id="sortEventTitle" runat="server" Visible="True" class="icon-caret-down rediconcolor"></i>
                                                </asp:LinkButton>
                                            </th>
							                <th class="event-title-header">
                                                <div><crm:Snippet ID="Snippet4" runat="server" SnippetName="CourseListing/CourseInstructorHeader" DefaultText="Course Instructor" /></div>
                                            </th>
							                <th class="event-date-header">
                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandArgument="InitialOferedDate" CommandName="Sort">
                                                    <crm:Snippet ID="Snippet5" runat="server" SnippetName="CourseListing/OfferedDatesHeader" DefaultText="Offered Dates" />
                                                    &nbsp;
                                                    <i id="sortOferedDates" runat="server" Visible="False" class="icon-caret-down rediconcolor"></i>
                                                </asp:LinkButton>
                                            </th>
                                        </tr>
                                        <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                                    </table>
                                </div>
                            </div>
                            <div class="event-pager course-listing-pager">
                                <asp:DataPager  ID="ListDataPager" runat="server" PagedControlID="CoursesListingView" PageSize="10" >
                                    <Fields>
                                        <asp:TemplatePagerField>
                                            <PagerTemplate>
                                                <div class="page-count">
                                                Showing <asp:Label runat="server" ID="StartIndexLabel" Text="<%# Container.TotalRowCount > 0 ? Container.StartRowIndex + 1 : 0 %>" /> to
                                                <asp:Label runat="server" ID="PageSizeLabel" Text="<%#  Container.StartRowIndex + Container.PageSize > Container.TotalRowCount ? Container.TotalRowCount : (Container.StartRowIndex + Container.PageSize) %>" />
                                                of
                                                <asp:Label runat="server" ID="PageCountLabel" Text="<%#  (Container.TotalRowCount) %>" />
                                                entries
                                            </div>
                                            </PagerTemplate>
                                        </asp:TemplatePagerField>
                                        <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowNextPageButton="False" PreviousPageText="<i class='icon-angle-left'></i>" />
                                        <asp:NumericPagerField CurrentPageLabelCssClass="active" />
                                        <asp:NextPreviousPagerField ShowLastPageButton="false" ShowPreviousPageButton="False" NextPageText="<i class='icon-angle-right'></i>" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="event-link-cell">
                                    <asp:LinkButton ID="EditLink" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                        <i class="icon-caret-right icon-large icon-spacer"></i><span class="event-detail-txt"><%# Eval("Location") %></span>
                                    </asp:LinkButton>                            
                                </td>
                                <td class="event-link-cell">
                                    <asp:LinkButton ID="LinkButton9" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                        <div><%# Eval("Topic") %></div>
                                    </asp:LinkButton>
                                </td>
                                <td class="event-link-cell">
                                    <asp:LinkButton ID="LinkButton6" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                        <div><%#Eval("EventTitle") %></div>
                                    </asp:LinkButton>                            
                                </td>
                                <td class="event-link-cell">
                                    <asp:LinkButton ID="LinkButton7" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                        <div><%#Eval("Presenter") %></div>
                                    </asp:LinkButton>                            
                                 </td>
                                <td class="event-link-cell">
                                    <asp:LinkButton ID="LinkButton8" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                        <div>
                                            <crm:DateTimeLiteral ID="DateTimeLiteral1" runat="server" Format="MMMM dd, yyyy" Value='<%# Eval("InitialOferedDate") %>' OutputTimeZoneLabel="false" />
                                            -
                                            <crm:DateTimeLiteral ID="DateTimeLiteral2" runat="server" Format="MMMM dd, yyyy" Value='<%# Eval("FinalOferedDate") %>' OutputTimeZoneLabel="false" />
                                        </div>
                                    </asp:LinkButton>                            
                                </td>
                            </tr>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <tr class="active">
                                <td class="event-link-cell">
                                    <asp:LinkButton ID="EditLink" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                        <i class="icon-caret-down icon-large icon-spacer rediconcolor"></i><span class="event-detail-txt"><%# Eval("Location") %></span>
                                    </asp:LinkButton>                            
                                </td>
                                <td class="event-link-cell">
                                    <asp:LinkButton ID="LinkButton9" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                        <div><%# Eval("Topic") %></div>
                                    </asp:LinkButton>
                                </td>
                                <td class="event-link-cell">
                                    <asp:LinkButton ID="LinkButton6" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                        <div><%#Eval("EventTitle") %></div>
                                    </asp:LinkButton>                            
                                </td>
                                <td class="event-link-cell">
                                    <asp:LinkButton ID="LinkButton7" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                        <div><%#Eval("Presenter") %></div>
                                    </asp:LinkButton>                            
                                 </td>
                                <td class="event-link-cell">
                                    <asp:LinkButton ID="LinkButton8" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                        <div>
                                            <crm:DateTimeLiteral ID="DateTimeLiteral3" runat="server" Format="MMMM dd, yyyy" Value='<%# Eval("InitialOferedDate") %>' OutputTimeZoneLabel="false" />
                                            -
                                            <crm:DateTimeLiteral ID="DateTimeLiteral4" runat="server" Format="MMMM dd, yyyy" Value='<%# Eval("FinalOferedDate") %>' OutputTimeZoneLabel="false" />
                                        </div>
                                    </asp:LinkButton>                            
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" class="details-row">
                                    <div class="details">
                                        <div class="row">
                                            <div class="details-heading">
                                                <crm:Snippet ID="Snippet11" runat="server" SnippetName="CourseListing/CourseCode" DefaultText="Course Code" />
                                            </div>
                                            <div class="event-detail-title long">
                                                <%--<a href="<%#Eval("EventUrl") %>">--%>
                                                    <b><%#Eval("CourseCode") %></b>
                                                <%--</a>--%>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="details-heading">
                                                <crm:Snippet ID="Snippet12" runat="server" SnippetName="CourseListing/CourseTitle" DefaultText="Course Title" />
                                            </div>
                                            <div class="event-detail-title long">
                                                <%--<a href="<%#Eval("EventUrl") %>">--%>
                                                    <b><%#Eval("EventTitle") %></b>
                                                <%--</a>--%>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="details-heading">
                                                <crm:Snippet ID="Snippet16" runat="server" SnippetName="CourseListing/Description" DefaultText="Description" />
                                            </div>
                                            <div class="details-content long" style="width: 80%;"><%#Eval("CourseDescription") %></div>
                                        </div>
                                        <div class="row" style="margin-top: 10px">
                                            <div class="row" style="width: 50%; float: left;">
                                                <div class="details-heading">
                                                    <crm:Snippet ID="Snippet14" runat="server" SnippetName="CourseListing/Topic" DefaultText="Topic" />
                                                </div>
                                                <div class="details-content long" style="width: 35%;"><%#Eval("Topic") %></div>
                                            </div>
                                            <div class="row" style="width: 50%; float: left;">
                                                <div class="details-heading">
                                                    <crm:Snippet ID="Snippet8" runat="server" SnippetName="CourseListing/TargetGrades" DefaultText="Target Grades" />
                                                </div>
                                                <div class="details-content long"><%#Eval("TargetGrades") %></div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 10px">
                                            <div class="row" style="width: 50%; float: left;">
                                                <div class="details-heading">
                                                    <crm:Snippet ID="Snippet9" runat="server" SnippetName="CourseListing/Presenters" DefaultText="Presenters" />
                                                </div>
                                                <div class="details-content long" style="width: 35%;">
                                                    <div class="details-content long">
                                                        <div id="presentersContainer" runat="server"></div>
                                                    </div>
                                                    <div class="details-content long" visible="<%# !string.IsNullOrWhiteSpace(Eval("CoPresenter1").ToString()) %>">
                                                        <%#Eval("CoPresenter1") %>
                                                    </div>
                                                    <div class="details-content long" visible="<%# !string.IsNullOrWhiteSpace(Eval("CoPresenter2").ToString()) %>">
                                                        <%#Eval("CoPresenter2") %>
                                                    </div>
                                                    <div class="details-content long" visible="<%# !string.IsNullOrWhiteSpace(Eval("CoPresenter3").ToString()) %>">
                                                        <%#Eval("CoPresenter3") %>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="width: 50%; float: left;">
                                                <div class="details-heading">
                                                    <crm:Snippet ID="Snippet17" runat="server" SnippetName="CourseListing/Location" DefaultText="Location" />
                                                </div>
                                                <div class="details-content long"><%#Eval("Location") %></div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 10px">
                                            <div class="row" style="width: 50%; float: left;">
                                                <div class="details-heading">
                                                    <crm:Snippet ID="Snippet10" runat="server" SnippetName="CourseListing/DatesOffered" DefaultText="Dates Offered" />
                                                </div>
                                                <div class="details-content long" style="width:50%">
                                                    <crm:DateTimeLiteral ID="DateTimeLiteral1" runat="server" Format="MMM dd yyyy" Value='<%# Eval("InitialOferedDate") %>' OutputTimeZoneLabel="false" />
                                                    -
                                                    <crm:DateTimeLiteral ID="DateTimeLiteral2" runat="server" Format="MMM dd yyyy" Value='<%# Eval("FinalOferedDate") %>' OutputTimeZoneLabel="false" />
                                                </div>
                                            </div>
                                            <div class="row" style="width: 50%; float: left;">
                                                <div id="registrationPeriodsContainer" runat="server"></div>
                                            </div>
                                        </div>


                                        <!-- Prices -->
                                        <%--<div class="row coursePrices">
                                            <%# CourseListings.GetCoursePrices((Guid)Eval("EventId")) %>
                                        </div>--%>

                                        <!-- Times -->
                                        <%--<div class="row" style="width: 50%; float: left;">
                                            <div class="details-heading">
                                                <crm:Snippet ID="Snippet15" runat="server" SnippetName="CourseListing/CourseRunsDailyFrom" DefaultText="Daily From" />
                                            </div>
                                            <div class="details-content long">
                                                <%# Eval("InitialDailyTime") %> - <%# Eval("FinalDailyTime") %>
                                            </div>
                                        </div>--%>


                                        
                                        <asp:Panel ID="Panel1" runat="server">
                                            <div class="row registration-period registration-buttons-courselisting">
                                                <div class="span7"> 
                                                    <%--<%# GetRegistrationButtonText((Guid)Eval("EventId")) + "-" + Eval("EventTitle") + "-" + Eval("EventId") %>--%>
                                                    <asp:Button ID="Register" runat="server" Text='<%# GetRegistrationButtonText((Guid)Eval("EventId")) %>' CommandName='<%# Eval("EventTitle") %>' CommandArgument='<%# Eval("EventId") %>' OnCommand="Register_Command" CssClass="btn event-btn green-btn pull-right"></asp:Button>
                                                    <asp:Button ID="WaitingList" runat="server" Text='Join waiting list' CommandArgument='<%# Eval("EventId") %>' OnCommand="Register_Command" CssClass="btn event-btn yellow-btn pull-right"></asp:Button>                                                
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="IsFullPanel" runat="server">
                                            <div id="eventFullBtn" runat="server" class="row registration-period">
                                                <div class="span7">
                                                    <a href="#" class="btn event-btn btn-danger btn pull-right">Course Full</a>
                                                </div>
                                            </div>
                                            <div id="eventFullText" runat="server" Visible="false" class="span7" style="text-align: right; font-weight: bold;"></div>
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                        </EditItemTemplate>
                        <EmptyDataTemplate>
                            <div class="loginMsg">
                                <b><crm:Snippet ID="Snippet3" runat="server" SnippetName="Courses/NoResultsFound" DefaultText="No courses found" /></b>
                            </div>
                        </EmptyDataTemplate>
                    </asp:ListView>

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    
    <% } %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="EntityControls" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentBottom" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="SidebarAbove" runat="server">
    <crm:Property DataSourceID="CurrentEntity" PropertyName="oems_sidebar" EditType="html" runat="server" />
    <ul class="etfo-event-nav pre-nav nav nav-tabs nav-stacked">
        <li class="item-green">
            <asp:HyperLink runat="server" ID="applyLnk" Visible="False"></asp:HyperLink>
        </li>
    </ul>    
</asp:Content>

<asp:Content ID="Content11" ContentPlaceHolderID="SidebarBelow" runat="server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="SidebarBottom" runat="server">
    <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/ETFOnews" data-widget-id="392320152093458432">Tweets by @ETFOnews</a>
    <script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");</script>
</asp:Content>

<asp:Content ID="Content12" ContentPlaceHolderID="Scripts" runat="server">
    <% if (loadCoursesListing) { %>
        <script type="text/javascript">
            var searchid = "<%= GridSearchID %>";
            var searchPlaceHolder = "<%= GridSearchPlaceholder %>";
            $(searchid).attr('placeholder', searchPlaceHolder);

            function EnterEvent(e)
            {
                if (e.keyCode == 13)
                {
                    __doPostBack('<%=GridSearchButton.UniqueID%>', "");
                }
            }

        </script>   
    <% } %>
</asp:Content>
