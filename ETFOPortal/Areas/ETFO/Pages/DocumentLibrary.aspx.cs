﻿using System;
using Site.Pages;
using System.Linq;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using Xrm;
using Site.Helpers;
using Microsoft.Xrm.Portal.Web;

namespace Site.Areas.ETFO.Pages
{
    public partial class DocumentLibrary : PortalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Guid eventID;
            int? documentType = null;

            var type = "Reference Document";
            if (!string.IsNullOrEmpty(Request.QueryString["DocumentType"]))
                type = Request.QueryString["DocumentType"];
            
            documentType = new EntityOptionSet().GetOptionSetValueByLabel(oems_EventDocument.EntityLogicalName, "oems_documenttype", type);
            
            if (string.IsNullOrEmpty(Request.QueryString["EventId"]))
            {
                var parentEvent = ServiceContext.oems_EventSet.FirstOrDefault(ev => ev.oems_WebPage.Id == Portal.Entity.GetAttributeValue<Guid>("adx_parentpageid"));
                eventID = parentEvent.oems_EventId.Value;
            }
            else
            {
                eventID = new Guid(Request.QueryString["EventId"]);
            }
            
            var documents = ServiceContext.oems_EventDocumentSet
                     .Where(x => x.oems_Event.Id == eventID);
            if (documentType != null)
            {
                documents = documents.Where(x => x.oems_DocumentType == documentType);
            }

            DocumentList.DataSource = documents;
            DocumentList.DataBind();
		}

        public string getUrl(Adx_webfile file)
        {
            var url = ServiceContext.GetUrl(file);
            return new UrlBuilder(url);
        }

        protected void DocumentList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewItem item = e.Item;
            oems_EventDocument ed = (oems_EventDocument)item.DataItem;

            var attachments = from wf in ServiceContext.Adx_webfileSet
                               where wf.oems_eventdocument.Id == ed.oems_EventDocumentId.Value
                               select wf;

            Repeater DocumentLinksRepeater = (Repeater)item.FindControl("DocumentLinksRepeater");
            DocumentLinksRepeater.DataSource = attachments;
            DocumentLinksRepeater.DataBind();

        }
    }
}