﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using System.Text;
using Site.Helpers;
using Site.Pages;
using Microsoft.Xrm.Sdk;
using Microsoft.Crm.Sdk.Messages;
using Xrm;
using LinqKit;
using log4net;

namespace Site.Areas.ETFO.Pages
{
    public partial class MyAccountMember : PortalPage
    { 
        private static readonly ILog Log = LogManager.GetLogger(typeof(MyAccountNonMember));

        protected void Page_Load(object sender, EventArgs e)
        {
            RedirectToLoginIfAnonymous();
            
            if (Page.IsPostBack)
                return;

            //HttpContext
            if (Contact.oems_isnon_member.HasValue && Contact.oems_isnon_member.Value)
            {
                var myAccountPage = ServiceContext.GetPageBySiteMarkerName(Website, "My Account Non Member");
                Response.Redirect(ServiceContext.GetUrl(myAccountPage));
            }

            #region Main info

            FirstNameTextBox.Text = Contact.FirstName;
            LastNameTextBox.Text = Contact.LastName;
            MemberIdTextBox.Text = Contact.mbr_ETFOID;
            EmailTextBox.Text = Contact.EMailAddress1;

            #endregion

            #region Preferred School board / School / Locals / Member Type

            var completeList = new List<dynamic>();
            var memberSchoolBoards = (from ms in XrmContext.CreateQuery("mbr_memberschool")
                                      join s in XrmContext.CreateQuery("mbr_school") on ((EntityReference)ms["mbr_school"]).Id equals s["mbr_schoolid"]
                                      where ms.GetAttributeValue<EntityReference>("mbr_member").Id == Contact.Id && ms.GetAttributeValue<bool>("mbr_active")
                                      select new
                                      {
                                          Id = ms.GetAttributeValue<Guid>("mbr_memberschoolid"),
                                          Name = ms.GetAttributeValue<string>("mbr_name"),
                                          MemberType = ms.GetAttributeValue<EntityReference>("mbr_membertype").Name,
                                          MemberTypeId = ms.GetAttributeValue<EntityReference>("mbr_membertype").Id,
                                          SchoolBoard = s.GetAttributeValue<EntityReference>("mbr_schoolboard").Name,
                                          SchoolBoardId = s.GetAttributeValue<EntityReference>("mbr_schoolboard").Id,
                                          School = ms.GetAttributeValue<EntityReference>("mbr_school").Name,
                                          Default = ms.GetAttributeValue<bool>("mbr_default")
                                      }
                                    ).ToList();

            // Local keys
            var localKeys = (from lk in memberSchoolBoards
                             select new
                             {
                                 MemberTypeId = lk.MemberTypeId,
                                 SchoolBoardId = lk.SchoolBoardId
                             }).ToList().Distinct();

            if (localKeys.Count() > 0)
            {
                var predicate = PredicateBuilder.New<Entity>();
                foreach (var key in localKeys)
                {
                    predicate = predicate.Or
                    (
                        sbl => ((EntityReference)sbl["mbr_membertypeid"]).Id == key.MemberTypeId &&
                                ((EntityReference)sbl["mbr_schoolboardid"]).Id == key.SchoolBoardId
                    );
                }

                var locals = (from sbl in ServiceContext.CreateQuery("mbr_schoolboardlocal").AsExpandable().Where(predicate)
                              where sbl.GetAttributeValue<EntityReference>("mbr_localid") != null
                              select new
                              {
                                  MemberTypeId = ((EntityReference)sbl["mbr_membertypeid"]).Id,
                                  SchoolBoardId = ((EntityReference)sbl["mbr_schoolboardid"]).Id,
                                  LocalName = sbl.GetAttributeValue<EntityReference>("mbr_localid").Name,
                              }).ToList();

                foreach (var msb in memberSchoolBoards)
                {
                    var local = locals.Where(l => l.MemberTypeId == msb.MemberTypeId && l.SchoolBoardId == msb.SchoolBoardId).FirstOrDefault();

                    if (local != null)
                    {
                        if(msb.Default)
                        {
                            InitialId.Value = msb.Id.ToString();
                            InitialLocal.Value = local.LocalName.ToString();
                            InitialSchool.Value = msb.School.ToString();
                            InitialSchoolBoard.Value = msb.SchoolBoard.ToString();
                            InitialMemberType.Value = msb.MemberType.ToString();
                        }

                        completeList.Add(new
                        {
                            Id = msb.Id,
                            Name = msb.Name,
                            MemberType = msb.MemberType,
                            SchoolBoard = msb.SchoolBoard,
                            SchoolBoardId = msb.SchoolBoardId,
                            School = msb.School,
                            Local = local.LocalName,
                            Default = msb.Default
                        });
                    }
                }
            }
            
            SchoolBoardList.DataSource = completeList;
            SchoolBoardList.DataBind();
            var selectedId = completeList.Where(a => a.Default == true).Select(a => a.Id.ToString()).FirstOrDefault();

            if (!completeList.Any())
            {
                NoPreferredSchoolBoardLocalSnippet.Visible = true;
                PreferredUpdate.Visible = false;
            }
            else
            {
                NoPreferredSchoolBoardLocalSnippet.Visible = false;
                PreferredUpdate.Visible = true;
            }

            #endregion

            #region Member Details

            Address1TextBox.Text = Contact.Address1_Line1;
            Address2TextBox.Text = Contact.Address1_Line2;
            CityTextBox.Text = Contact.Address1_City;
            ProvinceTextBox.Text = Contact.Address1_StateOrProvince;
            CountryTextBox.Text = Contact.Address1_Country;
            PostalCodeTextBox.Text = Contact.Address1_PostalCode;
            HomePhoneTextBox.Text = Contact.Telephone1;
            WorkPhoneTextBox.Text = Contact.Telephone2;
            CellPhoneTextBox.Text = Contact.Telephone3;
            FaxTextBox.Text = Contact.Fax;

            #endregion

            #region Self identification

            FirstNationSelfIdentCheckBox.Checked = Contact.mbr_FirstNation.GetValueOrDefault();
            InuitSelfIdentCheckBox.Checked = Contact.mbr_Inuit.GetValueOrDefault();
            MetisSelfIdentCheckBox.Checked = Contact.mbr_Metis.GetValueOrDefault();
            MinoritySelfIdentCheckBox.Checked = Contact.mbr_RacializedGroup.GetValueOrDefault();
            LgbtSelfIdentCheckBox.Checked = Contact.mbr_GayLesbianBisexualTransgender.GetValueOrDefault();
            DisabledSelfIdentCheckBox.Checked = Contact.mbr_Disabled.GetValueOrDefault();
            WomanSelfIdentCheckBox.Checked = Contact.mbr_Woman.GetValueOrDefault();

            #endregion

            #region Emergency info
            var dateFormat = XrmHelper.GetSiteSettingValueOrDefault("Date Format", "MM-dd-yyyy");

            CollegeIdTextBox.Text = Contact.mbr_collegeid;
            BirthdayHiddenField.Value = Contact.BirthDate.HasValue ? Contact.BirthDate.Value.ToString(dateFormat) : string.Empty;
            EmergencyNameTextBox.Text = Contact.oems_emergencycontact_name;
            EmergencyDescriptionTextBox.Text = Contact.oems_EmergencyContactDescription;
            EmergencyPhoneTextBox.Text = Contact.oems_emergencycontactphone;

            retrievePersonalAccommodation.Value = Contact.oems_RetrievePersonalAccommodationData;

            #endregion
        }

        protected void PersonalInformationUpdate_OnClick(object sender, EventArgs e)
        {
            var c = new Contact
            {
                Id = Contact.Id,
                FirstName = FirstNameTextBox.Text,
                LastName = LastNameTextBox.Text,
                EMailAddress1 = EmailTextBox.Text,
            };

            XrmContext.Attach(c);
            XrmContext.UpdateObject(c);
            XrmContext.SaveChanges();

            //Values for mail
            Dictionary<string, string> oldValues = new Dictionary<string, string>();
            Dictionary<string, string> newValues = new Dictionary<string, string>();

            //FirstName
            if(Contact.FirstName != c.FirstName)
            {
                oldValues["First Name"] = Contact.FirstName;
                newValues["First Name"] = c.FirstName;
            }
            //LastName
            if (Contact.LastName != c.LastName)
            {
                oldValues["Last Name"] = Contact.LastName;
                newValues["Last Name"] = c.LastName;
            }
            //EMailAddress1
            if (Contact.EMailAddress1 != c.EMailAddress1)
            {
                oldValues["Email Address"] = Contact.EMailAddress1;
                newValues["Email Address"] = c.EMailAddress1;
            }

            SendAdminNotificationEmail("Personal Information", oldValues, newValues);

            ShowMessage(MemberInformationResultPanel, MemberInformationResultLiteral, XrmHelper.GetSnippetValueOrDefault("My Account Personal Information Update Success Message",
                "Your personal information has been successfully updated."), "success");

            MemberInformationUpdateResultPanel.Update();
        }

        protected void PreferredUpdate_OnClick(object sender, EventArgs e)
        {
            Guid selectedId = Guid.Empty;
            string selectedLocal = string.Empty;
            string selectedSchool = string.Empty;
            string selectedSchoolBoard = string.Empty;
            string selectedMemberType = string.Empty;
            for (int i = 0; i < SchoolBoardList.Rows.Count; i++)
            {
                RadioButton rb = (RadioButton)SchoolBoardList.Rows[i].Cells[0].FindControl("rbSchoolBoardId");
                if (rb != null && rb.Checked)
                {
                    HiddenField hf = (HiddenField)SchoolBoardList.Rows[i].Cells[0].FindControl("Id");
                    if (hf != null)
                    {
                        selectedId = new Guid(hf.Value);
                        selectedLocal = SchoolBoardList.Rows[i].Cells[1].Text;
                        selectedSchool = SchoolBoardList.Rows[i].Cells[4].Text;
                        selectedSchoolBoard = SchoolBoardList.Rows[i].Cells[2].Text;
                        selectedMemberType = SchoolBoardList.Rows[i].Cells[3].Text;
                    }

                    break;
                }
            }

            if (selectedId == Guid.Empty)
            {
                ShowMessage(MemberInformationResultPanel, MemberInformationResultLiteral, XrmHelper.GetSnippetValueOrDefault("My Account Preferred Board School Local No Selected Item Error Message", 
                    "To continue please select a Preferred Board School Local."), "warning");

                MemberInformationUpdateResultPanel.Update();
                return;
            }
            
            var items = (from sb in XrmContext.CreateQuery("mbr_memberschool")
                        where sb.GetAttributeValue<EntityReference>("mbr_member").Id == Contact.Id && sb.GetAttributeValue<bool>("mbr_active")
                        select sb
                        ).ToList();  

            var selectedItem = items.FirstOrDefault(a => a.GetAttributeValue<Guid>("mbr_memberschoolid") == selectedId);
            if (selectedItem == null)
            {
                ShowMessage(MemberInformationResultPanel, MemberInformationResultLiteral, XrmHelper.GetSnippetValueOrDefault("My Account Preferred Board School Local No Item Error Message", 
                        "Unable to find selected item.  Please try again or contact customer service."), "warning");

                MemberInformationUpdateResultPanel.Update();
                return;
            }

            foreach (var b in items.Where(a => a.GetAttributeValue<bool>("mbr_default") == true))
            {
                b["mbr_default"] = false;
                XrmContext.UpdateObject(b);
            }

            selectedItem["mbr_default"] = true;
            XrmContext.UpdateObject(selectedItem);
            XrmContext.SaveChanges();

            //Values for mail
            Dictionary<string, string> oldValues = new Dictionary<string, string>();
            Dictionary<string, string> newValues = new Dictionary<string, string>();
            
            var oldId = InitialId.Value;
            if(oldId != selectedId.ToString())
            {
                var oldLocal = InitialLocal.Value;
                var oldSchool = InitialSchool.Value;
                var oldSchoolBoard = InitialSchoolBoard.Value;
                var oldMemberType = InitialMemberType.Value;

                oldValues.Add("Preferred Local / School Board / Member Type / School ", oldLocal + " / " + oldSchoolBoard + " / " + oldMemberType + " / " + oldSchool);
                newValues.Add("Preferred Local / School Board / Member Type / School ", selectedLocal + " / " + selectedSchoolBoard + " / " + selectedMemberType + " / " + selectedSchool);

                SendAdminNotificationEmail("Preferred School Board / School / Local", oldValues, newValues);
            }

            ShowMessage(MemberInformationResultPanel, MemberInformationResultLiteral, XrmHelper.GetSnippetValueOrDefault("My Account Preferred Board School Local Update Error Message",
                    "Preferred school board / local / school has been updated."), "success");

            MemberInformationUpdateResultPanel.Update();
        }

        protected void AddressUpdate_OnClick(object sender, EventArgs e)
        {
            var c = new Contact
            {
                Id = Contact.Id,
                Address1_Line1 = Address1TextBox.Text,
                Address1_Line2 = Address2TextBox.Text,
                Address1_City = CityTextBox.Text,
                Address1_StateOrProvince = ProvinceTextBox.Text,
                Address1_PostalCode = PostalCodeTextBox.Text,
                Address1_Country = CountryTextBox.Text,
                Telephone1 = WorkPhoneTextBox.Text,
                Telephone2 = HomePhoneTextBox.Text,
                Telephone3 = CellPhoneTextBox.Text,
                MobilePhone = CellPhoneTextBox.Text,
                Fax = FaxTextBox.Text
            };

            XrmContext.Attach(c);
            XrmContext.UpdateObject(c);
            XrmContext.SaveChanges();

            //Values for mail
            Dictionary<string, string> oldValues = new Dictionary<string, string>();
            Dictionary<string, string> newValues = new Dictionary<string, string>();

            //Address1_Line1
            if (Contact.Address1_Line1 != c.Address1_Line1)
            {
                oldValues["Address Line 1"] = Contact.Address1_Line1;
                newValues["Address Line 1"] = c.Address1_Line1;
            }
            //Address1_Line2
            if (Contact.Address1_Line2 != c.Address1_Line2)
            {
                oldValues["Address Line 2"] = Contact.Address1_Line2;
                newValues["Address Line 2"] = c.Address1_Line2;
            }
            //Address1_City
            if (Contact.Address1_City != c.Address1_City)
            {
                oldValues["City"] = Contact.Address1_City;
                newValues["City"] = c.Address1_City;
            }
            //Address1_StateOrProvince
            if (Contact.Address1_StateOrProvince != c.Address1_StateOrProvince)
            {
                oldValues["State or Province"] = Contact.Address1_StateOrProvince;
                newValues["State or Province"] = c.Address1_StateOrProvince;
            }
            //Address1_PostalCode
            if (Contact.Address1_PostalCode != c.Address1_PostalCode)
            {
                oldValues["Postal Code"] = Contact.Address1_PostalCode;
                newValues["Postal Code"] = c.Address1_PostalCode;
            }
            //Address1_Country
            if (Contact.Address1_Country != c.Address1_Country)
            {
                oldValues["Country"] = Contact.Address1_Country;
                newValues["Country"] = c.Address1_Country;
            }
            //Telephone1
            if (Contact.Telephone1 != c.Telephone1)
            {
                oldValues["Telephone1"] = Contact.Telephone1;
                newValues["Telephone1"] = c.Telephone1;
            }
            //Telephone2
            if (Contact.Telephone2 != c.Telephone2)
            {
                oldValues["Telephone2"] = Contact.Telephone2;
                newValues["Telephone2"] = c.Telephone2;
            }            
            //Telephone3
            if (Contact.Telephone3 != c.Telephone3)
            {
                oldValues["Telephone3"] = Contact.Telephone3;
                newValues["Telephone3"] = c.Telephone3;
            }
            //MobilePhone
            if (Contact.MobilePhone != c.MobilePhone)
            {
                oldValues["MobilePhone"] = Contact.MobilePhone;
                newValues["MobilePhone"] = c.MobilePhone;
            }
            //Fax
            if (Contact.Fax != c.Fax)
            {
                oldValues["Fax"] = Contact.Fax;
                newValues["Fax"] = c.Fax;
            }

            SendAdminNotificationEmail("Home Address/Contact", oldValues, newValues);

            ShowMessage(MemberInformationResultPanel, MemberInformationResultLiteral,
                XrmHelper.GetSnippetValueOrDefault("My Account Address Update Success Message", "Your account has been successfully updated."), "success");

            MemberInformationUpdateResultPanel.Update();
        }

        protected void SelfIdentUpdate_OnClick(object sender, EventArgs e)
        {
            var c = new Contact
            {
                Id = Contact.Id,
                mbr_FirstNation = FirstNationSelfIdentCheckBox.Checked,
                mbr_Disabled = DisabledSelfIdentCheckBox.Checked,
                mbr_GayLesbianBisexualTransgender = LgbtSelfIdentCheckBox.Checked,
                mbr_RacializedGroup = MinoritySelfIdentCheckBox.Checked,
                mbr_Metis = MetisSelfIdentCheckBox.Checked,
                mbr_Inuit = InuitSelfIdentCheckBox.Checked,
                mbr_Woman = WomanSelfIdentCheckBox.Checked
            };

            XrmContext.Attach(c);
            XrmContext.UpdateObject(c);
            XrmContext.SaveChanges();

            //Values for mail
            Dictionary<string, string> oldValues = new Dictionary<string, string>();
            Dictionary<string, string> newValues = new Dictionary<string, string>();

            //mbr_FirstNation
            if (Contact.mbr_FirstNation != c.mbr_FirstNation)
            {
                oldValues["First Nation"] = Contact.mbr_FirstNation != null ? Contact.mbr_FirstNation.ToString() : string.Empty;
                newValues["First Nation"] = c.mbr_FirstNation.ToString();
            }
            //mbr_Disabled
            if (Contact.mbr_Disabled != c.mbr_Disabled)
            {
                oldValues["Disabled"] = Contact.mbr_Disabled != null ? Contact.mbr_Disabled.ToString() : string.Empty;
                newValues["Disabled"] = c.mbr_Disabled.ToString();
            }
            //mbr_GayLesbianBisexualTransgender
            if (Contact.mbr_GayLesbianBisexualTransgender != c.mbr_GayLesbianBisexualTransgender)
            {
                oldValues["Gay Lesbian Bisexual Transgender"] = Contact.mbr_GayLesbianBisexualTransgender != null ? Contact.mbr_GayLesbianBisexualTransgender.ToString() : string.Empty;
                newValues["Gay Lesbian Bisexual Transgender"] = c.mbr_GayLesbianBisexualTransgender.ToString();
            }
            //mbr_RacializedGroup
            if (Contact.mbr_RacializedGroup != c.mbr_RacializedGroup)
            {
                oldValues["Racialized Group"] = Contact.mbr_RacializedGroup != null ? Contact.mbr_RacializedGroup.ToString() : string.Empty;
                newValues["Racialized Group"] = c.mbr_RacializedGroup.ToString();
            }
            //mbr_Metis
            if (Contact.mbr_Metis != c.mbr_Metis)
            {
                oldValues["Metis"] = Contact.mbr_Metis != null ? Contact.mbr_Metis.ToString() : string.Empty;
                newValues["Metis"] = c.mbr_Metis.ToString();
            }
            //mbr_Inuit
            if (Contact.mbr_Inuit != c.mbr_Inuit)
            {
                oldValues["Inuit"] = Contact.mbr_Inuit != null ? Contact.mbr_Inuit.ToString() : string.Empty;
                newValues["Inuit"] = c.mbr_Inuit.ToString();
            }
            //mbr_Woman
            if (Contact.mbr_Woman != c.mbr_Woman)
            {
                oldValues["Woman"] = Contact.mbr_Woman != null ? Contact.mbr_Woman.ToString() : string.Empty;
                newValues["Woman"] = c.mbr_Woman.ToString();
            }

            SendAdminNotificationEmail("Self Identification", oldValues, newValues);

            ShowMessage(MemberInformationResultPanel, MemberInformationResultLiteral, XrmHelper.GetSnippetValueOrDefault("My Account Self Ident Update Success Message", 
                "Your self identification details has been successfully updated."), "success");

            MemberInformationUpdateResultPanel.Update();
        }

        protected void UserProfileUpdate_OnClick(object sender, EventArgs e)
        {
            var c = new Contact
            {
                Id = Contact.Id,
                mbr_collegeid = CollegeIdTextBox.Text
            };

            if (!string.IsNullOrEmpty(BirthdayHiddenField.Value))
            {
                var format = XrmHelper.GetSiteSettingValueOrDefault("Date Format", "MM-dd-yyyy");
                DateTime birthdate;
                DateTime.TryParseExact(BirthdayHiddenField.Value, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out birthdate);
                c.BirthDate = birthdate;
            }

            XrmContext.Attach(c);
            XrmContext.UpdateObject(c);
            XrmContext.SaveChanges();

            //Values for mail
            Dictionary<string, string> oldValues = new Dictionary<string, string>();
            Dictionary<string, string> newValues = new Dictionary<string, string>();

            //mbr_collegeid
            if (Contact.mbr_collegeid != c.mbr_collegeid)
            {
                oldValues["College Id"] = Contact.mbr_collegeid;
                newValues["College Id"] = c.mbr_collegeid;
            }
            //mbr_collegeid
            if (Contact.BirthDate != c.BirthDate)
            {
                oldValues["Birth Date"] = Contact.BirthDate != null ? Contact.BirthDate.ToString() : string.Empty;
                newValues["Birth Date"] = c.BirthDate != null ? c.BirthDate.ToString() : string.Empty;
            }

            SendAdminNotificationEmail("User Profile - Personal Information", oldValues, newValues);

            ShowMessage(UserProfileResultPanel, UserProfileResultLiteral, XrmHelper.GetSnippetValueOrDefault("My Account User Profile Update Request Success Message", 
                "Your change to user profile details has been successful."), "success");

            UserProfileUpdateResultPanel.Update();
        }

        protected void UserEmergencyContactUpdate_OnClick(object sender, EventArgs e)
        {
            var c = new Contact
            {
                Id = Contact.Id,
                oems_emergencycontact_name = EmergencyNameTextBox.Text,
                oems_EmergencyContactDescription = EmergencyDescriptionTextBox.Text,
                oems_emergencycontactphone = EmergencyPhoneTextBox.Text
            };

            XrmContext.Attach(c);
            XrmContext.UpdateObject(c);
            XrmContext.SaveChanges();

            //Values for mail
            Dictionary<string, string> oldValues = new Dictionary<string, string>();
            Dictionary<string, string> newValues = new Dictionary<string, string>();

            //oems_emergencycontact_name
            if (Contact.oems_emergencycontact_name != c.oems_emergencycontact_name)
            {
                oldValues["Contact Name"] = Contact.oems_emergencycontact_name;
                newValues["Contact Name"] = c.oems_emergencycontact_name;
            }
            //oems_EmergencyContactDescription
            if (Contact.oems_EmergencyContactDescription != c.oems_EmergencyContactDescription)
            {
                oldValues["Contact Description"] = Contact.oems_EmergencyContactDescription;
                newValues["Contact Description"] = c.oems_EmergencyContactDescription;
            }
            //oems_emergencycontactphone
            if (Contact.oems_emergencycontactphone != c.oems_emergencycontactphone)
            {
                oldValues["Contact Phone"] = Contact.oems_emergencycontactphone;
                newValues["Contact Phone"] = c.oems_emergencycontactphone;
            }

            SendAdminNotificationEmail("Emergency Contact", oldValues, newValues);

            ShowMessage(UserProfileResultPanel, UserProfileResultLiteral, XrmHelper.GetSnippetValueOrDefault("My Account User Profile Emergency Update Request Success Message", 
                "Your change to user profile emergency contact details has been successful."), "success");

            UserProfileUpdateResultPanel.Update();
        }

        protected void PersonalAccommodationUpdate_OnClick(object sender, EventArgs e)
        {
            var c = new Contact
            {
                Id = Contact.Id,
                oems_UpdatePersonalAccommodationData = retrievePersonalAccommodation.Value
            };

            XrmContext.Attach(c);
            XrmContext.UpdateObject(c);
            XrmContext.SaveChanges();

            SendAdminNotificationEmail("Personal Accommodation", new Dictionary<string, string>(), new Dictionary<string, string>());

            ShowMessage(UserProfileResultPanel, UserProfileResultLiteral, XrmHelper.GetSnippetValueOrDefault("My Account User Profile Personal Accommodation Update Request Success Message", 
                "Your change to user profile personal accommodation details has been successful."), "success");

            UserProfileUpdateResultPanel.Update();
        }

        private void ShowMessage(WebControl panel, ITextControl literal, string msg, string type)
        {
            panel.Visible = true;
            literal.Text = msg;
            panel.CssClass = "alert " + string.Format(" alert-{0}", type);
        }

        private void SendAdminNotificationEmail(string section, Dictionary<string, string> oldValues, Dictionary<string, string> newValues)
        {
            try
            {
                using (var xrmServiceContext = XrmHelper.GetContext())
                {
                    var fromUser = xrmServiceContext.SystemUserSet.FirstOrDefault(u => u.FullName == XrmHelper.GetSiteSettingValueOrDefault("SystemEmail/Defaults/EmailFrom", "Admin User"));
                    if (fromUser == null)
                    {
                        throw new Exception("Error retrieving system user: " + XrmHelper.GetSiteSettingValueOrDefault("SystemEmail/Defaults/EmailFrom", "Admin User") + " from Site Setting: SystemEmail/Defaults/EmailFrom");
                    }

                    var mailTo = XrmHelper.GetSiteSettingValueOrDefault("SystemEmail/MyAccountMember/EmailTo", "trecords@etfo.org");
                    var emailTo = new ActivityParty { AddressUsed = mailTo };
                    var emailFrom = new ActivityParty { PartyId = fromUser.ToEntityReference() };

                    string defaultBody = "<p>User {0}, ETFO ID: {1}, updated their account information</p>";
                    defaultBody += "<p><b>Section</b>: {2}</p></ br></ br>";

                    var body = XrmHelper.GetSnippetValueOrDefault("AccountMember/UpdateProfile/MailBodyTemplate", defaultBody);

                    var itemTemplate = XrmHelper.GetSnippetValueOrDefault("AccountMember/UpdateProfile/ItemMailTemplate", "<p><b>{0}</b>. Previous: {1} - New: {2}</p>");
                    foreach (var item in oldValues.Keys)
                    {
                        body += string.Format(itemTemplate, item, oldValues[item], newValues[item]);
                    }

                    var userName = Contact.FirstName + " " + Contact.LastName;
                    body = string.Format(body, userName, Contact.mbr_ETFOID, section);

                    var htmlBody = new StringBuilder();
                    htmlBody.Append(body);

                    var email = new Email
                    {
                        To = new[] { emailTo },
                        From = new[] { emailFrom },
                        Subject = XrmHelper.GetSnippetValueOrDefault("AccountMember/UpdateProfile/MailSubject", "Member profile has been modified"),
                        Description = htmlBody.ToString()
                    };

                    xrmServiceContext.AddObject(email);
                    xrmServiceContext.SaveChanges();

                    var trackingTokenEmailRequest = new GetTrackingTokenEmailRequest();
                    var trackingTokenEmailResponse = (GetTrackingTokenEmailResponse)xrmServiceContext.Execute(trackingTokenEmailRequest);

                    var sendEmailreq = new SendEmailRequest
                    {
                        EmailId = email.Id,
                        TrackingToken = trackingTokenEmailResponse.TrackingToken,
                        IssueSend = true
                    };

                    var sendEmailresp = (SendEmailResponse)xrmServiceContext.Execute(sendEmailreq);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error Sending Mail: " + ex.Message);
            }
        }

    }
}