﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="HistoricalEvents.aspx.cs" Inherits="Site.Areas.ETFO.Pages.HistoricalEvents" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Import Namespace="Site.Areas.ETFO.Library" %>

<asp:Content ID="Content3" ContentPlaceHolderID="PageHeader" runat="server">
    <div class="page-header">
		<div class="grid-search">
            <asp:TextBox runat="server" ID="GridSearchText" CssClass="search-query" clientidmode="Static" onkeypress="return EnterEvent(event)" />
            <asp:LinkButton ID="GridSearchButton" runat="server" CssClass="btn search" OnClick="GridSearchButton_Click" ><i class="fa fa-search"></i></asp:LinkButton>
		</div>
        <h1>
			<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
		</h1>
	</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <asp:LoginView runat="server" >
        <AnonymousTemplate>
            <div class="loginMsg">
             <crm:Snippet runat="server" SnippetName="HistoricalEvents/LoginMessage1" DefaultText="Members must " /><a href="<%: Html.SignInUrl() %>"><crm:Snippet ID="Snippet6" runat="server" SnippetName="HistoricalEvents/LoginMessage2" DefaultText="login " /></a>
            <crm:Snippet ID="Snippet7" runat="server" SnippetName="HistoricalEvents/LoginMessage3" DefaultText="to view members only events." />

            </div>
        </AnonymousTemplate>
    </asp:LoginView>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/css/assets/img/ajax-modal-loading.gif" AlternateText="Loading ..." ToolTip="Loading ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="portlet box light-grey">
                <div class="portlet-title">
                    <div class="events-list form-group">
                        <asp:Label ID="EventTypeLabel" CssClass="col-md-10 col-sm-10 col-xs-10" runat="server" AssociatedControlID="EventTypeDropdown">
                            <crm:Snippet ID="Snippet3" runat="server" SnippetName="HistoricalEvents/EventTypeLabel" DefaultText="Event Type" />
                        </asp:Label>
                        <asp:DropDownList runat="server" ID="EventTypeDropdown" AppendDataBoundItems="True" CssClass="select" AutoPostBack="true" OnSelectedIndexChanged="FilterButton_Click">
                            <asp:ListItem Text="<%$ Snippet: DropDownListDefaultText, All %>" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <asp:ObjectDataSource runat="server" ID="EventListDataSource" EnablePaging="true"
                    TypeName="Site.Areas.ETFO.Library.EventListings" OnObjectCreating="HistoricalEventsDataSource_OnObjectCreating"
                    SelectMethod="Select" SelectCountMethod="SelectCount" SortParameterName="sortExpression" />
    
                <asp:ListView ID="HistoricalEventsView" runat="server" DataSourceID="EventListDataSource" OnSorting="HistoricalEventsView_OnSorting" OnItemDataBound="HistoricalEventsView_DataBound">
                    <LayoutTemplate>
                        <div class="portlet-body">
                            <div class="dataTables_wrapper form-inline" role="grid">
                                <table class="table">
                                    <tr>
                                        <th class="event-link-header">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument="EventTypeName" CommandName="Sort">
                                                <crm:Snippet ID="Snippet1" runat="server" SnippetName="HistoricalEvents/EventTypeHeader" DefaultText="Event Type" />
                                                &nbsp;
                                                <i id="sortTypeName" runat="server" Visible="False" class="fa fa-caret-down rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
                                        <th class="event-title-header">
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandArgument="EventTitle" CommandName="Sort">
                                                <crm:Snippet ID="Snippet2" runat="server" SnippetName="HistoricalEvents/EventTitleHeader" DefaultText="Event Title" />
                                                &nbsp;
                                                <i id="sortTitle" runat="server" Visible="False" class="fa fa-caret-down rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
                                        <th class="event-date-header" style="text-align: left;">
                                            <asp:LinkButton ID="LinkButton5" runat="server" CommandArgument="EventStatus" CommandName="Sort">
                                                <crm:Snippet ID="Snippet6" runat="server" SnippetName="MyEvents/EventStatusHeader" DefaultText="Event Status" />
                                                &nbsp;
                                                <i id="sortStatus" runat="server" Visible="False" class="fa fa-caret-down rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
							            <th class="event-date-header">
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandArgument="StartDate" CommandName="Sort">
                                                <crm:Snippet ID="Snippet4" runat="server" SnippetName="HistoricalEvents/StartDateHeader" DefaultText="Start Date" />
                                                &nbsp;
                                                <i id="sortStartDate" runat="server" Visible="True" class="fa fa-caret-up rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
							            <th class="event-date-header">
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandArgument="EndDate" CommandName="Sort">
                                                <crm:Snippet ID="Snippet5" runat="server" SnippetName="HistoricalEvents/EndDateHeader" DefaultText="End Date" />
                                                &nbsp;
                                                <i id="sortEndDate" runat="server" Visible="False" class="fa fa-caret-down rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
							            <th class="event-date-header">
                                            <asp:LinkButton ID="LinkButton6" runat="server" CommandArgument="RegistrationDeadline" CommandName="Sort">
                                                <crm:Snippet ID="Snippet8" runat="server" SnippetName="HistoricalEvents/RegistrationDeadlineHeader" DefaultText="Registration Deadline" />
                                                &nbsp;
                                                <i id="sortEndDeadline" runat="server" Visible="False" class="fa fa-caret-down rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
                                    </tr>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                                </table>
                            </div>
                        </div>
                        <div class="event-pager">
                            <asp:DataPager  ID="ListDataPager" runat="server" PagedControlID="HistoricalEventsView" PageSize="10" >
                                <Fields>
                                    <asp:TemplatePagerField>
                                        <PagerTemplate>
                                            <div class="page-count">
                                            Showing <asp:Label runat="server" ID="StartIndexLabel" Text="<%# Container.TotalRowCount > 0 ? Container.StartRowIndex + 1 : 0 %>" /> to
                                            <asp:Label runat="server" ID="PageSizeLabel" Text="<%#  Container.StartRowIndex + Container.PageSize > Container.TotalRowCount ? Container.TotalRowCount : (Container.StartRowIndex + Container.PageSize) %>" />
                                            of
                                            <asp:Label runat="server" ID="PageCountLabel" Text="<%#  (Container.TotalRowCount) %>" />
                                            entries
                                        </div>
                                        </PagerTemplate>
                                    </asp:TemplatePagerField>
                                    <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowNextPageButton="False" PreviousPageText="<i class='fa fa-angle-left'></i>" />
                                    <asp:NumericPagerField CurrentPageLabelCssClass="active" />
                                    <asp:NextPreviousPagerField ShowLastPageButton="false" ShowPreviousPageButton="False" NextPageText="<i class='fa fa-angle-right'></i>" />
                                </Fields>
                            </asp:DataPager>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="EditLink" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <i class="fa fa-caret-right fa-lg icon-spacer"></i><span class="event-detail-txt"><%#Eval("EventTypeName") %></span>
                                </asp:LinkButton>                            
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton5" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <div ><%#Eval("EventTitle") %></div>
                                </asp:LinkButton>                            
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton9" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <div> <%#Eval("EventStatusText") %> </div>
                                </asp:LinkButton>
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton6" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <div ><crm:DateTimeLiteral ID="DateTimeLiteral1" runat="server" Format="MMMM dd, yyyy" Value='<%# Eval("StartDate") %>' OutputTimeZoneLabel="false" /></div>
                                </asp:LinkButton>                            
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton7" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <div ><crm:DateTimeLiteral ID="DateTimeLiteral2" runat="server" Format="MMMM dd, yyyy" Value='<%# Eval("EndDate") %>' OutputTimeZoneLabel="false" /></div>
                                </asp:LinkButton>                            
                             </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton8" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <div ><crm:DateTimeLiteral ID="DateTimeLiteral3" runat="server" Format="MMMM dd, yyyy" Value='<%# Eval("RegistrationDeadline") %>' OutputTimeZoneLabel="false" /></div>
                                </asp:LinkButton>                            
                            </td>
                        </tr>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <tr class="active">
                            <td class="event-link-cell active">
                                <asp:LinkButton ID="EditLink" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn active">
                                    <i class="fa fa-caret-down fa-lg icon-spacer rediconcolor"></i><span class="event-detail-txt"><%#Eval("EventTypeName") %></span>
                                </asp:LinkButton>                            
                            </td>
                            <td class="event-link-cell active">
                                <asp:LinkButton ID="LinkButton9" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn active">
                                    <div><%#Eval("EventTitle") %>&nbsp; </div>
                                </asp:LinkButton>                            
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton13" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn active">
                                    <div> <%#Eval("EventStatusText") %> </div>
                                </asp:LinkButton>
                            </td>
                            <td class="event-link-cell active">
                                <asp:LinkButton ID="LinkButton10" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn active">
                                    <div><crm:DateTimeLiteral ID="DateTimeLiteral1" runat="server" Format="MMMM dd, yyyy" Value='<%# Eval("StartDate") %>' OutputTimeZoneLabel="false" />&nbsp;</div>
                                </asp:LinkButton>                            
                            </td>
                            <td class="event-link-cell active">
                                <asp:LinkButton ID="LinkButton11" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn active">
                                    <div><crm:DateTimeLiteral ID="DateTimeLiteral2" runat="server" Format="MMMM dd, yyyy" Value='<%# Eval("EndDate") %>' OutputTimeZoneLabel="false" />&nbsp;</div>
                                </asp:LinkButton>                            
                             </td>
                            <td class="event-link-cell active">
                                <asp:LinkButton ID="LinkButton12" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn active">
                                    <div><crm:DateTimeLiteral ID="DateTimeLiteral3" runat="server" Format="MMMM dd, yyyy" Value='<%# Eval("RegistrationDeadline") %>' OutputTimeZoneLabel="false" />&nbsp;</div>
                                </asp:LinkButton>                            
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" class="details-row">
                                <div class="details">
                                    <div class="row">
                                        <div class="details-heading">
                                            <asp:Label ID="Label4" runat="server" Text="Event"></asp:Label>
                                        </div>
                                        <div class="details-content long" style="width: 350px;">
                                            <b><a href="<%#Eval("EventUrl") %>"><%#Eval("EventTitle") %></a></b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="details-heading">
                                            <crm:Snippet ID="Snippet9" runat="server" SnippetName="HistoricalEvents/LocationHeader" DefaultText="Location" />
                                        </div>
                                        <div class="details-content long"><%# Eval("Location") %></div>
                                        <div class="details-heading long">
                                            <crm:Snippet ID="Snippet10" runat="server" SnippetName="HistoricalEvents/ExecutiveOfficerHeader" DefaultText="Executive Officer" />
                                        </div>
                                        <div class="details-content"><%# EventListings.getExecutiveStaff((Guid)Eval("EventId")) %></div>
                                        <div class="details-heading">
                                            <crm:Snippet ID="Snippet11" runat="server" SnippetName="HistoricalEvents/SupportStaffHeader" DefaultText="Support Staff" />
                                        </div>
                                        <div class="details-content"><%# EventListings.getSupportStaff((Guid)Eval("EventId")) %></div>
                                    </div>
                                    <div id="registrationPeriodsContainer" runat="server" class="row registration-period"></div>
                                    <div id="applicationSchedulesContainer" runat="server" class="row"></div>
                                </div>
                            </td>
                        </tr>
                    </EditItemTemplate>
                    <EmptyDataTemplate>
                        <b><crm:Snippet ID="Snippet12" runat="server" SnippetName="HistoricalEvents/NoResultsFound" DefaultText="No events found" /></b>
                    </EmptyDataTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

<asp:Content ID="Content13" ContentPlaceHolderID="SidebarAbove" runat="server">
    <ul class="etfo-event-nav pre-nav nav nav-tabs nav-stacked">
        <li class="item-purple">
            <crm:CrmHyperLink runat="server" ID="HyperLinkUpcoming" Text="<%$ Snippet: Upcoming Events Label, Upcoming Events %>" SiteMarkerName="Upcoming Events"></crm:CrmHyperLink>
        </li>
        <li class="item-pink">
            <crm:CrmHyperLink runat="server" ID="HyperLinkHistorical" Text="<%$ Snippet: Historical Events Label, Historical Events %>" SiteMarkerName="Historical Events"></crm:CrmHyperLink>
        </li>
    </ul>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="SidebarBottom" runat="server">
    <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/ETFOnews" data-widget-id="392320152093458432">Tweets by @ETFOnews</a>
    <script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");</script>
</asp:Content>

<asp:Content ID="Content11" ContentPlaceHolderID="SidebarBelow" runat="server">
    <adx:AdPlacement ID="AdPlacement1" runat="server" PlacementName="Sidebar Bottom" CssClass="ad" />
</asp:Content>

<asp:Content ID="Content12" ContentPlaceHolderID="Scripts" runat="server">
    <script type="text/javascript">
        var searchid = "<%=GridSearchID%>";
        var searchPlaceHolder = "<%=GridSearchPlaceholder %>";
        $(searchid).attr('placeholder', searchPlaceHolder);
    </script>
</asp:Content>
