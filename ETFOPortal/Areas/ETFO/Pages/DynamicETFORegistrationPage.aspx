﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Default.master" CodeBehind="DynamicETFORegistrationPage.aspx.cs" Inherits="Site.Areas.ETFO.Pages.DynamicETFORegistrationPage" %>

<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <title></title>
    <!--[if IE 7]>
		  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
		<![endif]-->
    <!-- page specific plugin styles -->
    <!-- fonts -->
    <link rel="stylesheet" href="~/css/carouselHeaderStyle.css" />
    <link rel="stylesheet" href="~/css/ace_fonts.css" />
        <link rel="stylesheet" href="~/css/bootstrap-switch.min.css" />
    <link rel="stylesheet" href="~/css/datepicker.css" />
    <link rel="stylesheet" href="~/css/ChildCareStyle.css" />
    <!-- ace styles -->
    <%-- <link rel="stylesheet" href="~/Areas/ETFO/Resources/ace.min.css" />--%>
    <!--[if lte IE 8]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->
    <!-- inline styles related to this page -->
    <!-- ace settings handler -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript" src="~/js/knockout-min.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form id="thisDynamicForm" runat="server">
        <asp:HiddenField ID="retrieveEventRegistrationJSON" runat="server" />
        <asp:HiddenField ID="eventregistrationid" runat="server" />

        <asp:ScriptManager ID="ScriptManager1" runat="server" />

        <div class="row etfo-row-wrap">
            <div class="span8 etfo-main-wrap">
                <div class="page-heading">
                    <% Html.RenderPartial("Breadcrumbs"); %>
                    <crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
                </div>
            </div>
        </div>
    
        <!-- Event details -->
        <% if (!String.IsNullOrEmpty(retrieveEventInfoJSON)) { %>
            <div id="HeaderContainer"></div>
            <%
               //Events by default
               var js = "~/js/carouselHeaderEngine.js";
               var view = "~/Controls/CarouselHeader.html";

               if (Type == 3) 
               {
                   view = "~/Controls/CarouselHeaderCourseTerm.html";
               }
               else if (Type == 4)
               {
                   view = "~/Controls/CarouselHeaderCourse.html";
               }
            %>
            <script type="text/javascript" src="<%= Page.ResolveUrl(js) %>"></script>
            <script type="text/javascript">
                var myModel = <%= retrieveEventInfoJSON %>;
                var registrationStatus = '<%= registrationStatus %>';
                //myModel.eventName = myModel.eventName + " - " + registrationStatus;
                model = myModel;
                model.dateFirstPart = ko.computed(function() 
                {
                    return this.eventDate.substring(0, this.eventDate.indexOf("-") - 1);
                }, model);
                model.dateSecondPart = ko.computed(function() 
                {
                    return this.eventDate.substring(this.eventDate.indexOf("-") + 1);
                }, model);
                urlfile = "<%= view %>";
                controlId = HeaderContainer;

                var eventOpenApplication = parseInt('<%= RegistrationHelper.EventStatus.OpenApplication %>');
                var iframeElement;;
                var schema  = null;
                var omniwareFormAPI = null;
                var messages =
                {
                    documents:
                    {
                        noData: '<%= Html.SnippetLiteral("SupportingDocuments/Messages/Documents/NoData", "Please select a document from your computer and enter a description.") %>',
                        noDescription: '<%= Html.SnippetLiteral("SupportingDocuments/Messages/Documents/NoDescription", "Please enter a description for the document.") %>',
                        noDocument: '<%= Html.SnippetLiteral("SupportingDocuments/Messages/Documents/NoDocument", "Please select a document from your computer.") %>',
                        maxSize: '<%= Html.SnippetLiteral("SupportingDocuments/Messages/Documents/MaxSize", "The document you selected exceeds the maximum of 5MB allowed. Please select another one.") %>',
                        minimumDocuments: '<%= Html.SnippetLiteral("SupportingDocuments/Messages/Documents/MinimumDocuments", "Minimum {0} doc(s) required.") %>'
                    },
                    attachments:
                    {
                        downloadError: '<%= Html.SnippetLiteral("Registration/Messages/Attachments/MaxSize", "There was an error downloading the document. Please try again") %>',
                        requiredField: '<%= Html.SnippetLiteral("Registration/Messages/Attachments/MaxSize", "The following fields are required:") %>'
                    },
                    other:
                    {
                        fieldRequired: '<%= Html.SnippetLiteral("Registration/Messages/Other/Field Required", "* At least one item is required") %>',
                        caucusOverlapping: '<%= Html.SnippetLiteral("Registration/Messages/Other/CaucusOverlapping", "* You cannot have overlapping caucus schedules") %>'
                    }
                };
                var eventRegistrationStatus =
                {
                    Initial:  <%= RegistrationHelper.EventRegistration.Initial %>,
                    Created: <%= RegistrationHelper.EventRegistration.Created %>,
                    Cancelled: <%= RegistrationHelper.EventRegistration.Cancelled %>,
                    Withdrawn: <%= RegistrationHelper.EventRegistration.Withdrawn %>,
                    Sumbitted: <%= RegistrationHelper.EventRegistration.Sumbitted %>,
                    Rejected: <%= RegistrationHelper.EventRegistration.Denied %>,
                    Approved: <%= RegistrationHelper.EventRegistration.Approved %>
                };
                var genderOptions =
                {
                    Male: 1,
                    Female: 2,
                    Other: 864350000
                };
                var callbacks =
                {
                    showLoadingPanel: function () 
                    {
                        $("#schoolboardLoadingPanel").show();	
                    },
                    hideLoadingPanel: function () 
                    {
                        $("#schoolboardLoadingPanel").hide();	
                    },
                    onLoadDataSuccess: function () 
                    {
                        $("#btnSave").prop('disabled', false);
                        $("#btnSubmit").prop('disabled', false);
                    },
                    onLoadDataError: function () 
                    {
                        $('#errorLoadingModal').modal('show');
                    }
                }

                function setFormJson(elm)
                {
                    iframeElement = elm;
                    var schemaString = '<%= regFormData.Replace(@"\n", @"\\n").Replace(@"\r", @"\\r").Replace(@"\t", @"\\t") %>';
                    try 
                    {
                        //schemaString = schemaString.replace(/<[\/]{0,1}(\w)[^><]*>/g, function($0, $1, $2){return $0.replace(/"/g, '\\"')});
                        schema =  JSON.parse(schemaString);
                        omniwareFormAPI = iframeElement.contentWindow.omniware;
        
                        omniwareFormAPI.renderForm
                        (
                            schema,
                            {
                                selector: '#regFormIframe',
                                extraHeight: 15, 
                                fullPaths: true,
                                isPortal: true,
                                currentStatus: <%= EventRegistrationStatus %>,
                                schoolboardURI: '/api/RegistrationForm/SchoolBoardList',
                                schoolURI: '/api/RegistrationForm/SchoolList?contactid=<%= ContactId %>',
                                localURI: '/api/RegistrationForm/LocalList?eventid=<%= EventId %>&contactid=<%= ContactId %>',
                                attachmentDownloadUrl: '/Attachments/DownloadDocument/'
                            },
                            messages,
                            eventRegistrationStatus,
                            genderOptions,
                            callbacks
                        );
                    }
                    catch(err)
                    {
                        console.log(err.message);
                        showAlert();
                    }
                }
            </script> 
            <div id="regFormDiv">
                <iframe id="regFormIframe" src="/areas/etfo/pages/dynamicForm.html" style="width:100%;overflow: hidden;border-width:0px;" frameborder="0" scrolling="no" onload="setFormJson(this);"></iframe>
            </div>
        <% } else { %>
            <h1><%= Html.SnippetLiteral("RegistrationForm/ErrorLoading", "Error loading event information") %></h1>
            <a href="<%= Html.SiteMarkerUrl("Upcoming Events") %>"><%= Html.SnippetLiteral("RegistrationForm/UpcomingEvents", "Go to upcoming events") %></a>
        <% } %>

        <div id="Div1"></div>

        <div class="form-actions">
            <input id="btnCancel" type="button" onclick="Cancel()" class="btn btn-primary" value="Cancel" />
            <% if(!ReadOnlyForm) { %>     
                  
                <% if(!IsWaitingListOffered) { %>
                    <input id="btnSave" type="button" onclick="Save(false)" class="btn btn-primary" value="Save"  />
                <% } %>
                
                <% if(!RemoveSubmitButton) { %>
                    <input id="btnSubmit" type="button" onclick="Submit()" class="btn btn-primary" value="<%= SubmitButtonText %>" />
                <% } %>

            <% } else if (IsApproved){ %>
                <input type="button" onclick="UpdateAttachments()" class="btn btn-primary" value="Save"  />
            <% } %>
            <input id="btnPrint" type="button" onclick="printForm()" class="btn btn-primary" value="Print" />
            <% if (waitingListSpostsAvailable){ %>
                <p class="registration-submit-message"><%= XrmHelper.GetSnippetValueOrDefault("EventRegistration/WaitingList/EventFullSpotsAvailable", "This event is full but waiting list spots are available") %></p>
            <% } %>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="saveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel"><%= Html.SnippetLiteral("RegistrationForm/SaveDialog/Title", "ETFO Registration Save") %></h4>
                    </div>
                    <div class="modal-body">
                        <h4><%= Html.SnippetLiteral("RegistrationForm/SaveDialog/Message", "Your changes have been saved.") %></h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="reloadPage()">OK</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="saveApplicationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="H1"><%= Html.SnippetLiteral("RegistrationForm/SaveApplicationDialog/Title", "ETFO Application Save") %></h4>
                    </div>
                    <div class="modal-body">
                        <h4 class="modal-title" id="H4">
                            <%= XrmHelper.GetSnippetValueOrDefault("EventRegistration/ApplicationSaved", "Your application has been saved. To access it in future, navigate to My ETFO > My Events.") %>
                        </h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="reloadPage()">OK</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="saveAndUpdateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="SubmittedDialog">
                            <%= XrmHelper.GetSnippetValueOrDefault("EventRegistration/RegistrationSubmitted", "Your registration has been submitted.") %>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="goBack()">OK</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="saveAndPayModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <%-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                        <h4 class="modal-title" id="H2"><%= Html.SnippetLiteral("RegistrationForm/SaveAndPayDialog/Message", "Your registration has been saved. Click OK to go to the payment options.") %></h4>
                    </div>
                    <div class="modal-body">
                        <p></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="redirectToPaymentOptionsIfRequired();">OK</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <p class="ow_alert_text"><%= Html.SnippetLiteral("RegistrationForm/AlertDialog/Message", "We experiencing temporary problem, please try again...") %></p>
                        <div id="error-detail" class="error-detail" style="display:none;"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn-detail btn btn-default">Detail</button>
                        <button type="button" class="btn-ok btn btn-default" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="waitingListFullModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"><%= XrmHelper.GetSnippetValueOrDefault("EventRegistration/WaitingList/RegistrationClosedEventFull", "Registration is closed because this event is completely full") %></h4>
                    </div>
                    <div class="modal-body">
                        <p></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="reloadPage();">OK</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="waitingListOfferModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">
                            <%= XrmHelper.GetSnippetValueOrDefault("EventRegistration/WaitingList/OfferSpot", "Do you want to be put on the waiting list? (You will not be charged until you get the spot) ") %>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="acceptWaitingListOffer();">Yes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="reloadPage();">No</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="errorLoadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="H3"><%= Html.SnippetLiteral("RegistrationForm/LoadingData/Error", "There was an error loading the data. Please refresh the page and if the problem persists contact ETFO.") %></h4>
                    </div>
                    <div class="modal-body">
                        <p></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="reloadPage()">REFRESH</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="overlapModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="overlapTitle"></h4>
                    </div>
                    <div class="modal-body" id="overlapCourses">
                        <ul>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="submitModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <p class="ow_alert_text"><%= Html.SnippetLiteral("RegistrationForm/SubmitModal/Message", "Please review your registration and confirm that all of your information is correct before submitting.") %></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="Save(true)">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="timeoutModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <p class="ow_alert_text"><%= Html.SnippetLiteral("RegistrationForm/SessionTimesOut", "Your session has timed out. Please login and try again") %></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="redirectToLogin()">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7; display: none;" id="loadingPanel">
            <img src="../../../css/assets/img/ajax-modal-loading.gif" style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
        </div>

        <!-- /.modal -->
        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7; display: none;" id="schoolboardLoadingPanel">
            <img src="../../../css/assets/img/ajax-modal-loading.gif" style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
        </div>

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
   <script src="assets/plugins/respond.min.js"></script>
   <script src="assets/plugins/excanvas.min.js"></script> 
   <![endif]-->

        <script type="text/javascript" src="~/js/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="~/js/jquery.cookie.min.js"></script>
        <script type="text/javascript" src="~/js/jquery.flot.crosshair.js"></script>

        <script type="text/javascript" src="/js/metronic/app.js"></script>
        <script src="<%: Url.Content("~/js/bootstrap-datepicker.min.js") %>"></script>
        <script src="<%: Url.Content("~/js/bootstrap-switch.min.js") %>"></script>
        <%--<script type="text/javascript" src="~/Areas/ETFO/Resources/app.js"></script>--%>

        <!-- END PAGE LEVEL SCRIPTS -->

        <script>
            var paymentUrl = '/payment/ShoppingCart';
            var requirePayment = false;
            var backUrl = '<%= backUrl %>';
            var registrationSubmittedMessage = '<%= XrmHelper.GetSnippetValueOrDefault("EventRegistration/RegistrationSubmitted", "Your registration has been submitted.") %>';
            var applicationSubmittedMessage = '<%= XrmHelper.GetSnippetValueOrDefault("EventRegistration/ApplicationSubmitted", "Your application has been submitted.") %>'
            var overlapCoursesRegistrations = '<%= Html.SnippetLiteral("RegistrationForm/Overlap/Registrations", "You cannot register to this due to overlapping schedules with the following course(s) for which you are already registered:") %>'
            var overlapShoppingCartRegistrations = '<%= Html.SnippetLiteral("RegistrationForm/Overlap/Shoppingcart", "You cannot register to this due to overlapping schedules with the following course(s) for which you are already added on your shopping cart:") %>'
            var MONTHS = ["January","February","March","April","May","June","July","August","September","October","November","December"];

            $(function() 
            {
                var paymentForm = "~/Controls/PostForm.html";
                $.ajax({
                    url: paymentForm,
                    success: function (html) 
                    {
                        var $html = $(html);
                        $html.find("#postUrl").val('<%= backUrl %>');
                        $html.find("#postForm").attr('action', paymentUrl);
                        $("body").append($html);
                    },
                    error: function (xhr, ajaxOptions, thrownError) 
                    {  
                        if (xhr.status === 401)
                        {
                            $('#loadingPanel').hide();
                            $('#timeoutModal').modal('show');
                        }
                        else
                        {          
                            $('.form-actions input').prop('disabled', false);
                            $('#loadingPanel').hide();
                            showAlert('<%= Html.SnippetLiteral("RegistrationForm/AlertDialog/Error", "Something went wrong. Please try again or contact support.") %>');
                        }                    
                    }
                });
            });

            function goBack() 
            {
                //window.history.back();
                $('#loadingPanel').show();
                window.location.href = backUrl;
            }

            function redirectToPaymentOptionsIfRequired() 
            {
                if (requirePayment) 
                {
                    $("#paymentForm").submit();
                    //window.location.href = paymentUrl;
                } 
                else 
                {
                    goBack();
                }
            }

            function redirectToLogin() 
            {
                $('#loadingPanel').show();

                var returnUrl = '?returnurl=<%= Request.Url.PathAndQuery %>';
				var url = '<%= Html.SiteMarkerUrl("Login") %>' + returnUrl;
                window.location = url;
            }
            
            function acceptWaitingListOffer() 
            {
                $('#loadingPanel').show();

                var data =
                {
                    eventRegistrationId: '<%= eventregistrationid.Value  %>',
                    eventId: '<%= EventId %>'
                };

                $.ajax
                ({
                    type: "POST",
                    complete: function () 
                    {
                        //alert("done");
                    },
                    url: "/payment/AcceptWaitingListOffer",
                    data: JSON.stringify(data),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (result) 
                    {
                        //reloadPage();
                        goBack();
                    },
                    error: function (xhr, ajaxOptions, thrownError) 
                    {  
                        if (xhr.status === 401)
                        {
                            $('#loadingPanel').hide();
                            $('#timeoutModal').modal('show');
                        }
                        else
                        {          
                            $('.form-actions input').prop('disabled', false);
                            $('#loadingPanel').hide();
                            showAlert('<%= Html.SnippetLiteral("RegistrationForm/AlertDialog/Error", "Something went wrong. Please try again or contact support.") %>');
                        }                    
                    }
                });
            }

            function reloadPage() 
            {
                $('#loadingPanel').show();
                window.location.reload();
            }

            function Cancel()
            {
                $('.form-actions input').prop('disabled', true);
                $('#loadingPanel').show();

                var data =
                {
                    eventRegistrationId: '<%= eventregistrationid.Value  %>'
                }

                $.ajax
                ({
                    type: "POST",
                    complete: function () 
                    {
                        //alert("done");
                    },
                    url: "/payment/CancelRegistration",
                    data: JSON.stringify(data),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (result) 
                    {
                        goBack();
                    },
                    error: function(xhr, ajaxOptions, thrownError) 
                    {
                        if (xhr.status === 401)
                        {
                            $('#loadingPanel').hide();
                            $('#timeoutModal').modal('show');
                        }
                        else
                        {          
                            $('.form-actions input').prop('disabled', false);
                            $('#loadingPanel').hide();
                            showAlert('<%= Html.SnippetLiteral("RegistrationForm/AlertDialog/Error", "Something went wrong. Please try again or contact support.") %>');
                        }
                    }
                });
            }

            function Submit()
            {
                $('#submitModal').modal('show');
            }

            function Save(isSubmit) 
            {
                var formIsValid = omniwareFormAPI.validateForm(isSubmit);
                if(formIsValid) 
                {
                    $('.form-actions input').prop('disabled', true);
                    $('#loadingPanel').show();

                    var data =
                    {
                        eventRegistrationId: '<%= eventregistrationid.Value  %>',
                        eventId: '<%= EventId %>',
                        updateEventRegistrationData: omniwareFormAPI.getFormData(),
                        documents: omniwareFormAPI.getDocuments ? omniwareFormAPI.getDocuments() : null,
                        attachments: omniwareFormAPI.getAttachments ? omniwareFormAPI.getAttachments() : null,
                        returningPresenter: omniwareFormAPI.getReturningPresenter ? omniwareFormAPI.getReturningPresenter() : null,
                        isSubmit: isSubmit
                    };

                    $.ajax
                    ({
                        type: "POST",
                        complete: function () 
                        {
                            //alert("done");
                        },
                        url: "/payment/UpdateEventRegistrationWithPayment",
                        data: JSON.stringify(data),
                        contentType: "application/json",
                        dataType: "json",
                        success: function (result)
                        {
                            $('.form-actions input').prop('disabled', false);
                            $('#loadingPanel').hide();
                            if (result.Success)
                            {
                                if (!result.WaitingListFull && !result.DoWaitingListOffer)
                                {
                                    requirePayment = result.RequirePayment;
                                    if (!isSubmit)
                                    {
                                        if (result.IsApplication)
                                        {
                                            $('#saveApplicationModal').modal('show');
                                        }
                                        else
                                        {
                                            $('#saveModal').modal('show');
                                        }
                                    }
                                    else
                                    {
                                        if (!requirePayment)
                                        {
                                            if (result.IsApplication)
                                            {
                                                $("#SubmittedDialog").text(applicationSubmittedMessage);
                                            }
                                            else
                                            {
                                                $("#SubmittedDialog").text(registrationSubmittedMessage);
                                            }

                                            $('#saveAndUpdateModal').modal('show');
                                        }
                                        else
                                        {
                                            $('#loadingPanel').show();
                                            $('#postForm').submit();
                                        }
                                    }
                                }
                                else if (result.WaitingListFull)
                                {
                                    $('#waitingListFullModal').modal('show');
                                }
                                else if (result.DoWaitingListOffer)
                                {
                                    $('#waitingListOfferModal').modal('show');
                                }
                            }
                            else if (!result.Success)
                            {
                                $("#overlapCourses ul").empty();
                                if(result.RegistrationOverlapping)
                                {
                                    $("#overlapTitle").html(overlapCoursesRegistrations);
                                    for (i = 0; i < result.OverlappingSchedules.length; i++) 
                                    { 
                                        var startDate = new Date(result.OverlappingSchedules[i].StartDate);
                                        startDate = new Date(startDate.getUTCFullYear(), startDate.getUTCMonth(), startDate.getUTCDate());
                                       
                                        var endDate = new Date(result.OverlappingSchedules[i].EndDate);
                                        endDate = new Date(endDate.getUTCFullYear(), endDate.getUTCMonth(), endDate.getUTCDate());

                                        var text = "<li>" 
                                            + result.OverlappingSchedules[i].EventTitle + ": " 
                                            + startDate.toString('MMM dd, yyyy') + " To " 
                                            + endDate.toString('MMM dd, yyyy') + "</li>";
                                        $("#overlapCourses ul").append(text);                                        
                                    }

                                    $('#overlapModal').modal('show');
                                }
                                else if(result.ShoppingCartOverlapping)
                                {
                                    $("#overlapTitle").html(overlapShoppingCartRegistrations);
                                    for (i = 0; i < result.OverlappingSchedules.length; i++) 
                                    { 
                                        var startDate = new Date(result.OverlappingSchedules[i].StartDate);
                                        startDate = new Date(startDate.getUTCFullYear(), startDate.getUTCMonth(), startDate.getUTCDate());
                                       
                                        var endDate = new Date(result.OverlappingSchedules[i].EndDate);
                                        endDate = new Date(endDate.getUTCFullYear(), endDate.getUTCMonth(), endDate.getUTCDate());

                                        var text = "<li>" 
                                            + result.OverlappingSchedules[i].EventTitle + ": " 
                                            + startDate.toString('MMM dd, yyyy') + " To " 
                                            + endDate.toString('MMM dd, yyyy') + "</li>";
                                        $("#overlapCourses ul").append(text);                                        
                                    }

                                    $('#overlapModal').modal('show');
                                }
                                else if (result.Message)
                                {
                                    var error = '<%= Html.SnippetLiteral("RegistrationForm/AlertDialog/Error", "Something went wrong. Please try again or contact support.") %>'
                                    showAlert(error, null, result.Message);
                                    //showAlert(result.Message);
                                }
                                else
                                {
                                    showAlert('<%= Html.SnippetLiteral("RegistrationForm/AlertDialog/Error", "Something went wrong. Please try again or contact support.") %>');
                                }
                            } 
                            else if (result == -1) 
                            {
                                showAlert('<%= Html.SnippetLiteral("RegistrationForm/AlertDialog/CapacityExeeded", "Sorry the capacity of this event has been exceeded, you are not able to register at this time.") %>');
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) 
                        {
                            if (xhr.status === 401)
                            {
                                $('#loadingPanel').hide();
                                $('#timeoutModal').modal('show');
                            }
                            else
                            {          
                                $('.form-actions input').prop('disabled', false);
                                $('#loadingPanel').hide();
                                showAlert('<%= Html.SnippetLiteral("RegistrationForm/AlertDialog/Error", "Something went wrong. Please try again or contact support.") %>');
                            }
                        }
                    });
                }
                else
                {
                    var fn = function() 
                    {
                        var topIframe = $("#regFormIframe").offset().top;
                        var topElement = $("#regFormIframe").contents().find(".has-error:first");
                        if (topElement.length > 0) 
                        {
                            var top =  topElement.offset().top;
                            setTimeout
                            (
                                function() {
                                    $('html, body').animate({
                                        scrollTop: topIframe + top - 80
                                    }, 500);
                                },
                                400
                            );
                        }
                    };
                    var text = isSubmit ? '<%= Html.SnippetLiteral("RegistrationForm/Submit/FormWithErrors", "The Registration Form contains errors. Please correct them before submitting.") %>' 
                        : '<%= Html.SnippetLiteral("RegistrationForm/Save/FormWithErrors", "The Registration Form contains errors. Please correct them before saving.") %>';
                    showAlert(text, fn);
                }
            }
            
            function UpdateAttachments() 
            {
                var isSubmit = true;
                var formIsValid = omniwareFormAPI.validateForm(isSubmit);
                if(formIsValid) 
                {
                    $('.form-actions input').prop('disabled', true);
                    $('#loadingPanel').show();

                    var data =
                    {
                        eventRegistrationId: '<%= eventregistrationid.Value  %>',
                        eventId: '<%= EventId %>',
                        attachments: omniwareFormAPI.getAttachments ? omniwareFormAPI.getAttachments() : null
                    };

                    $.ajax
                    ({
                        type: "POST",
                        complete: function () 
                        {
                            //alert("done");
                        },
                        url: "/payment/UpdateEventRegistrationAttachments",
                        data: JSON.stringify(data),
                        contentType: "application/json",
                        dataType: "json",
                        success: function (result) 
                        {
                            $('.form-actions input').prop('disabled', false);
                            $('#loadingPanel').hide();
                            if (result.Success)
                            {
                                $('#saveModal').modal('show');
                            }
                            else 
                            {
                                showAlert('<%= Html.SnippetLiteral("RegistrationForm/AlertDialog/Error", "Something went wrong. Please try again or contact support.") %>');
                            } 
                        },
                        error: function (xhr, ajaxOptions, thrownError) 
                        {         
                            if (xhr.status === 401)
                            {
                                $('#loadingPanel').hide();
                                $('#timeoutModal').modal('show');
                            }
                            else
                            {          
                                $('.form-actions input').prop('disabled', false);
                                $('#loadingPanel').hide();
                                showAlert('<%= Html.SnippetLiteral("RegistrationForm/AlertDialog/Error", "Something went wrong. Please try again or contact support.") %>');
                            }
                        }
                    });
                }
                else
                {
                    var fn = function() 
                    {
                        var topIframe = $("#regFormIframe").offset().top;
                        var topElement = $("#regFormIframe").contents().find(".has-error:first");
                        if (topElement.length > 0) 
                        {
                            var top =  topElement.offset().top;
                            setTimeout
                            (
                                function() {
                                    $('html, body').animate({
                                        scrollTop: topIframe + top - 80
                                    }, 500);
                                },
                                400
                            );
                        }
                    };
                    var text = isSubmit ? '<%= Html.SnippetLiteral("RegistrationForm/Submit/FormWithErrors", "The Registration Form contains errors. Please correct them before submitting.") %>' 
                        : '<%= Html.SnippetLiteral("RegistrationForm/Save/FormWithErrors", "The Registration Form contains errors. Please correct them before saving.") %>';
                    showAlert(text, fn);
                }
            }

            function printForm() 
            {
                var printWin = window.open('/areas/etfo/pages/dynamicPrintForm.html#print', 'Print','height=1000,scrollbars=yes');
                var header = $("#HeaderContainer").html();

                // this was tricky to get working when page load is slow
                // See: http://stackoverflow.com/questions/6460630/close-window-automatically-after-printing-dialog-closes
                var printAndClose = function() {
                    if (printWin.document && 
                        printWin.omniware &&
                        printWin.document.readyState == 'complete') {
                        
                        var schemaLoaded = omniwareFormAPI.getSchema();
                        printWin.omniware.renderPrintableForm(header, schemaLoaded, {});

                        clearInterval(sched);
                        printWin.print();
                        printWin.close();
                    }
                };
                var sched = setInterval(printAndClose, 200);
            }
            
            function showAlert(text, callback, detailMessage)
            {
                $('#alertModal .ow_alert_text').html(text ? text : '<%= Html.SnippetLiteral("RegistrationForm/AlertDialog/Message", "We experiencing temporary problem, please try again...") %>');

                var detailContainer = $("#error-detail");
                var detailBtn = $('#alertModal').find("button.btn-detail");

                detailBtn.hide();
                detailContainer.html("");
                detailContainer.hide();

                if (detailMessage)
                {
                    detailContainer.html("<hr></hr>" + detailMessage);
                    detailBtn.show();
                    detailBtn.click
                    (
                        function ()
                        {
                            detailContainer.show();
                            detailBtn.hide();
                        }
                    );
                }

                $('#alertModal').modal('show');

                if (callback) 
                {
                    $('#alertModal').find("button.btn-ok").click
                    (
                        function() 
                        {
                            callback();
                        }
                    );
                }
            }

        </script>
    </form>
</asp:Content>


