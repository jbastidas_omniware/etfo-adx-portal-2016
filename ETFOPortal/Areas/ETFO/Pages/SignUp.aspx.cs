﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Web.UI.WebControls;
using Site.Areas.ETFO.Library;
using Site.Helpers;
using Site.Pages;
using log4net;
using Microsoft.Xrm.Sdk;
using Site.Library;

namespace Site.Areas.ETFO.Pages
{
    public partial class SignUp : PortalPage
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(SignUp));

        private const string EtfoMemberIdErrorMessageSnippetName = "MemberSignUp/EtfoMemberIdInvalidErrorMessage";
        private const string AcceptTermsAndConditionsErrorMessageSnippetName = "MemberSignUp/AcceptTermsAndConditionsRequiredErrorMessage";
        private const string EtfoMemberVerificationErrorMessageSnippetName = "MemberSignUp/EtfoMemberVerificationInvalidErrorMessage";
        private const string ValidateSecurityQuestionsErrorMessageSnippetName = "MemberSignUp/SecurityAnswerInvalidErrorMessage";
        private const string UsernameInUseErrorMessageSnippetName = "MemberSignUp/UsernameInUseErrorMessage";
        private const string UsernamesDoNotMatchErrorMessageSnippetName = "MemberSignUp/UsernamesDoNotMatchErrorMessage";
        private const string PasswordSecurityCheckErrorMessageSnippetName = "MemberSignUp/PasswordSecurityCheckErrorMessage";
        private const string PasswordsDoNotMatchErrorMessageSnippetName = "MemberSignUp/PasswordsDoNotMatchErrorMessage";
        private const string CannotFindUserForAccountRecoveryErrorMessageSnippetName = "MemberSignUp/CannotFindUserForAccountRecoveryErrorMessage";
        private const string UserIneligibleForPasswordResetErrorMessageSnippetName = "MemberSignUp/UserIneligibleForPasswordResetErrorMessage";
        private const string InvalidUsernameSnippetName = "MemberSignUp/InvalidUsernameErrorMessage";
        private const string InvalidPasswordSnippetName = "MemberSignUp/InvalidPasswordErrorMessage";
        private const string DuplicateUsernameSnippetName = "MemberSignUp/DuplicateUsernameErrorMessage";
        private const string InvalidAnswerSnippetName = "MemberSignUp/InvalidAnswerErrorMessage";
        private const string InvalidEmailSnippetName = "MemberSignUp/InvalidEmailErrorMessage";
        private const string DuplicateEmailSnippetName = "MemberSignUp/DuplicateEmailErrorMessage";
        private const string UserRejectedSnippetName = "MemberSignUp/UserRejectedErrorMessage";
        private const string InvalidProviderUserKeySnippetName = "MemberSignUp/InvalidProviderUserKeyErrorMessage";
        private const string DuplicateProviderUserKeySnippetName = "MemberSignUp/DuplicateProviderUserKeyErrorMessage";
        private const string ProviderErrorSnippetName = "MemberSignUp/ProviderErrorMessage";
        private const string MemberSignUpMemberRecordsInactiveAccountSnippetName = "MemberSignUp/MemberRecordsInactiveAccount";
        private const string InvalidPostalCodeSnippetName = "MemberSignUp/InvalidPostalCode";

        private const string NonMemberInvalidStaffID = "NonMemberSignUp/InvalidStaffID";
        private const string InvalidSchoolBoardOrSchoolSelectionErrorMessageSnippetName = "NonMemberSignUp/InvalidSchoolBoardOrSchoolSelection";
        private const string PostalCodeDoNotMatch = "NonMemberSignUp/PostalCodeDoNotMatch";
        private const string StaffUSerInUse = "NonMemberSignUp/StaffUserInUse";

        private bool cancel=false;
        private string staffid= "null";
        private string firstname="null";
        private string lastname = "null";
        private string eventkey = "null";

        public string GetCancel()
        {
             return cancel.ToString().ToLower(); 
        }

        public string GetStaffID() 
        {
            return staffid;
        }

        public string GetFirstName() 
        {
            return firstname;
        }

        public string GetLastName() 
        {
            return lastname;
        }

        public string GetEventKey() 
        {
            return eventkey;
        }

        public Guid? SessionId
        {
            get
            {
                Guid guid;
                if (!Guid.TryParse(Request.QueryString["sessionid"], out guid))
                {
                    return null;
                }
                return guid;
            }
        }
		
		public bool LoadSchoolData { get; private set; }

		protected void Page_Load(object sender, EventArgs e)
		{
		    if (Request.IsAuthenticated)
		    {
                var homePage = ServiceContext.GetPageBySiteMarkerName(Website, "Home");
                Response.Redirect(ServiceContext.GetUrl(homePage));
            }
        }

        void LoadStaffUser(string eventkey) 
        {
            this.eventkey = string.Format("\"{0}\"", eventkey);

            var loginRegistrationSession = SignUpHelper.GetLoginSessionBySessionId(SessionId.GetValueOrDefault());

            if (loginRegistrationSession != null)
            {
                if (((OptionSetValue)loginRegistrationSession["oems_nonmembertype"]).Value == 3)
                {
                    var context = XrmHelper.GetContext();

                    var oemsStaffUser = (from c in context.CreateQuery("oems_etfostaffuser")
                                         where c["oems_staffid"] == loginRegistrationSession["oems_staffid"]
                                         select c).FirstOrDefault();

                    if (oemsStaffUser != null)
                    {
                        this.staffid = string.Format("\"{0}\"", loginRegistrationSession["oems_staffid"].ToString());
                        this.firstname = string.Format("\"{0}\"", oemsStaffUser["oems_firstname"].ToString());
                        this.lastname = string.Format("\"{0}\"", oemsStaffUser["oems_lastname"].ToString());
                    }

                }
            }
        }

        public override void Validate(string grp)
        {
            //bool enableValidators = SignUpHelper.IsStaffUser(SessionId.GetValueOrDefault());
            DropDownList nonmembertype = (DropDownList)FindControlIterative(this,"oems_nonmembertype");

            if (nonmembertype != null)
            {
                string selectedText = nonmembertype.SelectedItem.Text;

                if (selectedText != "Staff")
                {
                    ValidatorCollection validators = GetValidators(grp);
                    foreach (IValidator val in validators)
                    {
                        if (val.ErrorMessage.Contains("oems_staffid"))
                        {
                            base.Validators.Remove(val);
                        }
                    }
                }
            }
            //PasswordRequired.Enabled = enableValidators;
            base.Validate();
        }

        public static System.Web.UI.Control FindControlIterative(System.Web.UI.Control root, string id)
        {
            System.Web.UI.Control ctl = root;
            var ctls = new LinkedList<System.Web.UI.Control>();

            while (ctl != null)
            {
                if (ctl.ID == id)
                    return ctl;
                foreach (System.Web.UI.Control child in ctl.Controls)
                {
                    if (child.ID == id)
                        return child;
                    if (child.HasControls())
                        ctls.AddLast(child);
                }
                if (ctls.First != null)
                {
                    ctl = ctls.First.Value;
                    ctls.Remove(ctl);
                }
                else return null;
            }
            return null;
        }

        protected void WebFormControl_ItemSaved(object sender, Adxstudio.Xrm.Web.UI.WebControls.WebFormSavedEventArgs e)
        {
            switch (e.KeyName)
            {

            }
        }

        private void FinalizeSignUp(WebFormSubmitEventArgs e)
        {
            try
            {
                var loginRegistrationSession = SignUpHelper.GetLoginSessionBySessionId(SessionId.GetValueOrDefault());
                if (!loginRegistrationSession.oems_accountexists.GetValueOrDefault())
                {
                    //new account
                    var result = SignUpHelper.CreateNewMemberLogin(loginRegistrationSession);
                    switch (result)
                    {
                        case MembershipCreateStatus.InvalidUserName:
                            e.Cancel = true;
                            AppendErrorMessage(InvalidUsernameSnippetName);
                            break;
                        case MembershipCreateStatus.InvalidPassword:
                            e.Cancel = true;
                            AppendErrorMessage(InvalidPasswordSnippetName);
                            break;
                        case MembershipCreateStatus.DuplicateUserName:
                            e.Cancel = true;
                            AppendErrorMessage(DuplicateUsernameSnippetName);
                            break;
                        case MembershipCreateStatus.InvalidAnswer:
                            e.Cancel = true;
                            AppendErrorMessage(InvalidAnswerSnippetName);
                            break;
                        case MembershipCreateStatus.InvalidEmail:
                            e.Cancel = true;
                            AppendErrorMessage(InvalidEmailSnippetName);
                            break;
                        case MembershipCreateStatus.DuplicateEmail:
                            e.Cancel = true;
                            AppendErrorMessage(DuplicateEmailSnippetName);
                            break;
                        case MembershipCreateStatus.UserRejected:
                            e.Cancel = true;
                            AppendErrorMessage(UserRejectedSnippetName);
                            break;
                        case MembershipCreateStatus.InvalidProviderUserKey:
                            e.Cancel = true;
                            AppendErrorMessage(InvalidProviderUserKeySnippetName);
                            break;
                        case MembershipCreateStatus.DuplicateProviderUserKey:
                            e.Cancel = true;
                            AppendErrorMessage(DuplicateProviderUserKeySnippetName);
                            break;
                        case MembershipCreateStatus.ProviderError:
                            e.Cancel = true;
                            AppendErrorMessage(ProviderErrorSnippetName);
                            break;
                        default:
                            if (result != MembershipCreateStatus.Success)
                            {
                                e.Cancel = true;
                                string error = XrmHelper.GetSnippetValueOrDefault("SignUp/FinalizeSignUp/InvalidStatus", "Unhandled status returned in the function FinalizeSignUp");
                                AppendErrorMessage(error);
                            }
                            break;
                    }
                }
                else
                {
                    //existing account
                    RecoverAccount(e);
                }
            }
            catch (Exception ex)
            {
                e.Cancel = true;
                AppendErrorMessage(XrmHelper.GetSnippetValueOrDefault("SignUp/FinalizeSignUp/Error", "Something went wrong. Please sign out from the site, delete your browser history, refresh your browser and sign in again to proceed with your registration."));
            }
        }

		private void FinalizeNonMemberSignUp(WebFormSubmitEventArgs e)
		{
            try
            {
                var loginRegistrationSession = SignUpHelper.GetLoginSessionBySessionId(SessionId.GetValueOrDefault());

                //create account
                var result = SignUpHelper.CreateNewNonMemberLogin(loginRegistrationSession);
                switch (result)
                {
                    case MembershipCreateStatus.InvalidUserName:
                        e.Cancel = true;
                        AppendErrorMessage(InvalidUsernameSnippetName);
                        break;
                    case MembershipCreateStatus.InvalidPassword:
                        e.Cancel = true;
                        AppendErrorMessage(InvalidPasswordSnippetName);
                        break;
                    case MembershipCreateStatus.DuplicateUserName:
                        e.Cancel = true;
                        AppendErrorMessage(DuplicateUsernameSnippetName);
                        break;
                    case MembershipCreateStatus.InvalidAnswer:
                        e.Cancel = true;
                        AppendErrorMessage(InvalidAnswerSnippetName);
                        break;
                    case MembershipCreateStatus.InvalidEmail:
                        e.Cancel = true;
                        AppendErrorMessage(InvalidEmailSnippetName);
                        break;
                    case MembershipCreateStatus.DuplicateEmail:
                        e.Cancel = true;
                        AppendErrorMessage(DuplicateEmailSnippetName);
                        break;
                    case MembershipCreateStatus.UserRejected:
                        e.Cancel = true;
                        AppendErrorMessage(UserRejectedSnippetName);
                        break;
                    case MembershipCreateStatus.InvalidProviderUserKey:
                        e.Cancel = true;
                        AppendErrorMessage(InvalidProviderUserKeySnippetName);
                        break;
                    case MembershipCreateStatus.DuplicateProviderUserKey:
                        e.Cancel = true;
                        AppendErrorMessage(DuplicateProviderUserKeySnippetName);
                        break;
                    case MembershipCreateStatus.ProviderError:
                        e.Cancel = true;
                        AppendErrorMessage(ProviderErrorSnippetName);
                        break;
                    default:
                        if (result != MembershipCreateStatus.Success)
                        {
                            e.Cancel = true;
                            string error = XrmHelper.GetSnippetValueOrDefault("SignUp/FinalizeNonMemberSignUp/InvalidStatus", "Unhandled status returned in the function FinalizeNonMemberSignUp");
                            AppendErrorMessage(error);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                e.Cancel = true;
                AppendErrorMessage(XrmHelper.GetSnippetValueOrDefault("SignUp/FinalizeNonMemberSignUp/Error", "Something went wrong. Please sign out from the site, delete your browser history, refresh your browser and sign in again to proceed with your registration."));
            }

        }

        protected void WebFormControl_FormLoad(object sender, Adxstudio.Xrm.Web.UI.WebControls.WebFormLoadEventArgs e)
        {
            switch (e.KeyName)
            {
                case "TeacherProfileLoading":
                    LoadSchoolData = true;
                    break;
                case "StaffVerification":
                    LoadStaffUser("StaffVerification");
                    break;
            }
        }

        protected void WebFormControl_Submit(object sender, Adxstudio.Xrm.Web.UI.WebControls.WebFormSubmitEventArgs e)
        {
            switch (e.KeyName)
            {
                case "AccountRecoverySubmit":
                    RecoverAccount(e);
                    break;
                case "ConfirmStepSubmit":
                    //finalize the signup by converting the collected data.
                    FinalizeSignUp(e);
                    break;
                case "NonMemberConfirmStepSubmit":
                    FinalizeNonMemberSignUp(e);
                    break;
            }
        }

        protected void WebFormControl_ItemSaving(object sender, Adxstudio.Xrm.Web.UI.WebControls.WebFormSavingEventArgs e)
        {
            switch (e.KeyName)
            {
                case "ValidateTermsAndConditions":
                    ValidateTermsAndConditionsStep(e);
                    break;

                case "ValidateVerification":
                    ValidateVerificationStep(e);
                    break;

                case "AccountSetupSaving":
                    AccountSetupSaving(e);
                    break;

                case "ValidateSecurityQuestions":
                    ValidateSecurityQuestionsStep(e);
                    break;

                case "ValidateNonMemberTermsAndConditions":
                    ValidateTermsAndConditionsStep(e);
                    break;

                case "ValidateNonMemberAccountSetup":
                    NonMemberAccountSetupSaving(e);
                    break;

                case "ValidateProfileTeacher":
                    ValidateProfileTeacher(e);

                    break;

                case "ValidateProfileNonTeacher":
                    ValidateProfileNonTeacher(e);

                    break;
                case "NonMemberValidateTermsAndConditions":
                    ValidateStaffID(e);
                    break;

                case "ValidateStaffPostalCode":
                    ValidateStaffPostalCode(e);
                    break;
            }
            if (e.Cancel)
            {
                ErrorPanel.Visible = true;
            }
        }

        private void ValidateStaffPostalCode(Adxstudio.Xrm.Web.UI.WebControls.WebFormSavingEventArgs e)
        {
            var loginRegistrationSession = SignUpHelper.GetLoginSessionBySessionId(SessionId.GetValueOrDefault());
            if (loginRegistrationSession != null)
            {
                var context = XrmHelper.GetContext();

                var oemsStaffUser = (from c in context.CreateQuery("oems_etfostaffuser")
                                     where c["oems_staffid"] == loginRegistrationSession["oems_staffid"]
                                     select c).FirstOrDefault();

                if (oemsStaffUser != null)
                {
                    string postalcodeSession = StringExtensions.ConvetToAlphaNumeric(((TextBox)FindControlIterative(this, "oems_staffpostalcode")).Text).ToLower();
                    string postalcodeStaff = StringExtensions.ConvetToAlphaNumeric(oemsStaffUser["oems_postalcode"].ToString()).ToLower();
                    if (postalcodeSession != postalcodeStaff)
                    {
                        e.Cancel = true;
                        AppendErrorMessage(PostalCodeDoNotMatch);
                    }
                }
            }
        }

        private bool ValidateProfileTeacher(WebFormSavingEventArgs e)
        {
            //if (!SignUpHelper.ValidateTeacherSchool(e))
            //{
            //    e.Cancel = true;
            //    cancel = true;
            //    AppendErrorMessage(InvalidSchoolBoardOrSchoolSelectionErrorMessageSnippetName);
            //    return false;
            //}

            var postalCode = e.Values["oems_postalcode"].ToString();
            if(postalCode.Contains(" "))
            {
                if (postalCode.Length != 7)
                {
                    e.Cancel = true;
                    cancel = true;
                    AppendErrorMessage(InvalidPostalCodeSnippetName);
                    return false;
                }
            }
            else
            {
                if (postalCode.Length != 6)
                {
                    e.Cancel = true;
                    cancel = true;
                    AppendErrorMessage(InvalidPostalCodeSnippetName);
                    return false;
                }
            }

            return true;
        }

        private bool ValidateProfileNonTeacher(WebFormSavingEventArgs e)
        {
            var postalCode = e.Values["oems_postalcode"].ToString();
            if (postalCode.Contains(" "))
            {
                if (postalCode.Length != 7)
                {
                    e.Cancel = true;
                    cancel = true;
                    AppendErrorMessage(InvalidPostalCodeSnippetName);
                    return false;
                }
            }
            else
            {
                if (postalCode.Length != 6)
                {
                    e.Cancel = true;
                    cancel = true;
                    AppendErrorMessage(InvalidPostalCodeSnippetName);
                    return false;
                }
            }

            return true;
        }

        private void RecoverAccount(WebFormSubmitEventArgs e)
        {
            var loginRegistrationSession = SignUpHelper.GetLoginSessionBySessionId(SessionId.GetValueOrDefault());
            switch (SignUpHelper.RecoverContact(loginRegistrationSession))
            {
                case ContactRecoveryStatus.CannotFindUser:
                    e.Cancel = true;
                    AppendErrorMessage(CannotFindUserForAccountRecoveryErrorMessageSnippetName);

                    break;
                case ContactRecoveryStatus.UserIsIneligibleForPasswordReset:
                    e.Cancel = true;
                    AppendErrorMessage(UserIneligibleForPasswordResetErrorMessageSnippetName);
                    break;
            }
        }

        private bool AccountSetupSaving(WebFormSavingEventArgs e)
        {
            var contact = SignUpHelper.GetAccountForWebFormSession(SessionId.GetValueOrDefault());

            if (!SignUpHelper.ConfirmUsernameValid(e))
            {
                e.Cancel = true;
                AppendErrorMessage(UsernameInUseErrorMessageSnippetName);
                return false;
            }

            if (!SignUpHelper.ConfirmUsernameMatch(e))
            {
                e.Cancel = true;
                AppendErrorMessage(UsernamesDoNotMatchErrorMessageSnippetName);
                return false;
            }

            if (!SignUpHelper.PasswordSecurityCheck(e))
            {
                e.Cancel = true;
                AppendErrorMessage(PasswordSecurityCheckErrorMessageSnippetName);
                return false;
            }

            if (!SignUpHelper.ConfirmPasswordsMatch(e))
            {
                e.Cancel = true;
                AppendErrorMessage(PasswordsDoNotMatchErrorMessageSnippetName);
                return false;
            }

            if (!SignUpHelper.EncodePassword(e))
            {
                e.Cancel = true;
                throw new Exception(string.Format("An error occurred during password generation for Account: '{0}'", contact.FullName));
            }

            return true;
        }

        private void ValidateTermsAndConditionsStep(WebFormSavingEventArgs e)
        {
            Xrm.Contact contact;

            if (!ValidateEtfoMemberIdAndActiveAccount(e, out contact))
            {
                return;
            }

            if (!ValidateAcceptedTermsAndConditions(e))
            {
                return;
            }
            
            e.Values["oems_membername"] = contact.FullName;
            e.Values["oems_contactid"] = contact.ToEntityReference();

            if (contact.oems_hasemsaccount.HasValue && contact.oems_hasemsaccount.Value)
            {
                e.Values["oems_accountexists"] = true;
                e.Values["oems_securityquestion"] = contact.Adx_passwordquestion;
                e.Values["oems_username"] = contact.Adx_username;
            }
            else
            {
                e.Values["oems_accountexists"] = false;
                e.Values["oems_securityquestion"] = string.Empty;
                e.Values["oems_username"] = string.Empty;
            }
        }

        /// <summary>
        /// Validates that the ETFO Member ID Exists in the system.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="contact">out parameter containing associated Account</param>
        /// <returns></returns>
        private bool ValidateEtfoMemberIdAndActiveAccount(WebFormSavingEventArgs e, out Xrm.Contact contact)
        {
            var memberId = e.Values["oems_etfoid"] as string;
            contact = SignUpHelper.GetMemberByEtfoMemberId(memberId);

            if (contact == null)
            {
                e.Cancel = true;
                cancel = true;
                AppendErrorMessage(EtfoMemberIdErrorMessageSnippetName);
                return false;
            }

            if (!contact.Contains("mbr_active") || (!(bool)contact["mbr_active"]))
            {
                e.Cancel = true;
                cancel = true;
                AppendErrorMessage(MemberSignUpMemberRecordsInactiveAccountSnippetName);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Validates that the user has accepted the terms and conditions
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private bool ValidateAcceptedTermsAndConditions(WebFormSavingEventArgs e)
        {
            var value = e.Values["oems_accepttermsandconditions"] as bool?;

            if (!value.GetValueOrDefault())
            {
                e.Cancel = true;
                cancel = true;
                AppendErrorMessage(AcceptTermsAndConditionsErrorMessageSnippetName);
                return false;
            }

            return true;
        }

        private bool ValidateStaffID(WebFormSavingEventArgs e)
        {
            var nonmembertype = Convert.ToInt32(e.Values["oems_nonmembertype"]);

            if (nonmembertype == 3)
            {
                var value = e.Values["oems_staffid"];
                if (value != null)
                {
                    var context = XrmHelper.GetContext();

                    var oemsStaffUser = (from c in context.CreateQuery("oems_etfostaffuser")
                                         where c["oems_staffid"] == value
                                         select c).FirstOrDefault();

                    if (oemsStaffUser != null)
                    {
                        var contact = (from c in context.CreateQuery("contact") where c["oems_staffid"] == value select c).FirstOrDefault();
                        if (contact == null)
                            return true;

                        e.Cancel = true;
                        cancel = true;
                        AppendErrorMessage(StaffUSerInUse);
                    }
                    else
                    {
                        e.Cancel = true;
                        cancel = e.Cancel;
                        AppendErrorMessage(NonMemberInvalidStaffID);
                    }
                }
            }
            //AppendErrorMessage(NonMemberInvalidStaffID);
            return false;
        }

        /// <summary>
        /// Validates that the verification number submitted during the webform step is correct for the current account in
        /// the login registration session.
        /// Also checks that the account is in good standing
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private bool ValidateVerificationStep(WebFormSavingEventArgs e)
        {
            var contact = SignUpHelper.GetAccountForWebFormSession(SessionId.GetValueOrDefault());

            string sessionOCT = StringExtensions.ConvetToAlphaNumeric((string)e.Values["oems_octnumber"]).ToLower();
            string contactOCT = StringExtensions.ConvetToAlphaNumeric(contact.mbr_collegeid).ToLower();
            if (sessionOCT != contactOCT)
            {
                e.Cancel = true;
                AppendErrorMessage(EtfoMemberVerificationErrorMessageSnippetName);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Validates the security answer submitted against the answer in the account's contact record.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private bool ValidateSecurityQuestionsStep(WebFormSavingEventArgs e)
        {
            var contact = SignUpHelper.GetAccountForWebFormSession(SessionId.GetValueOrDefault());
            if (contact == null)
            {
                e.Cancel = true;
                AppendErrorMessage(ValidateSecurityQuestionsErrorMessageSnippetName);
                throw new Exception(string.Format("Attempt to to retrieve Security Question for contact - Name:{0}", contact.FullName));
            }

            var securityAnswer = StringExtensions.ConvetToAlphaNumeric(e.Values["oems_securityanswer"] as string).ToLower();
            if (!SignUpHelper.ValidateSecurityAnswer(contact, securityAnswer))
            {
                e.Cancel = true;
                AppendErrorMessage(ValidateSecurityQuestionsErrorMessageSnippetName);
                return false;
            }


            return true;
        }

        private bool NonMemberAccountSetupSaving(WebFormSavingEventArgs e)
        {
            var loginReistrationSession = SignUpHelper.GetLoginSessionBySessionId(SessionId.GetValueOrDefault());

            if (!SignUpHelper.ConfirmUsernameValid(e))
            {
                e.Cancel = true;
                AppendErrorMessage(UsernameInUseErrorMessageSnippetName);
                return false;
            }

            if (!SignUpHelper.ConfirmUsernameMatch(e))
            {
                e.Cancel = true;
                AppendErrorMessage(UsernamesDoNotMatchErrorMessageSnippetName);
                return false;
            }

            if (!SignUpHelper.PasswordSecurityCheck(e))
            {
                e.Cancel = true;
                AppendErrorMessage(PasswordSecurityCheckErrorMessageSnippetName);
                return false;
            }

            if (!SignUpHelper.ConfirmPasswordsMatch(e))
            {
                e.Cancel = true;
                AppendErrorMessage(PasswordsDoNotMatchErrorMessageSnippetName);
                return false;
            }

            if (!SignUpHelper.EncodePassword(e))
            {
                e.Cancel = true;
                throw new Exception(string.Format("An error occurred during password encoding for Login Registration Session: '{0}'", loginReistrationSession.oems_loginregistrationsessionId));
            }

            return true;
        }

        protected string CallLoadSchoolData()
        {
            return LoadSchoolData ? "LoadSchoolData();" : string.Empty;
        }

        protected string GetCancelButtonHtml()
        {
            var signInUrl = XrmHelper.GetSiteMarkerUrl("Login");
            return string.Format("\"<a id='CancelButton' class='btn' href='{1}' >{0}</a>\"", "Cancel", signInUrl);
        }

        protected string GetSignInUrl()
        {
            return XrmHelper.GetSiteMarkerUrl("Login");
        }

        private void AppendErrorMessage(string SnippetName)
        {
            var snippet = new Adxstudio.Xrm.Web.UI.WebControls.Snippet();
            snippet.DefaultText = SnippetName;
            snippet.SnippetName = SnippetName;
            snippet.EditType = "Html";

            ErrorPanel.Controls.Add(snippet);
            ErrorPanel.Visible = true;

            Log.Error("Error on Sign up process: " + SnippetName);
        }
    }
}
