﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="SubCategoryLandingPage.aspx.cs" Inherits="Site.Areas.ETFO.Pages.SubCategoryLandingPage" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<%@ Register TagPrefix="site" TagName="ChildNavigation" Src="~/Controls/ChildNavigation.ascx" %>
<%@ Import Namespace="Adxstudio.Xrm.Blogs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageHeader" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $(document).ready(function () {
            $('#landing-carousel').carousel(portal.carouselSettings);

            $('#playButton').hide();
            $('#playButton').click(function () {
                $('#landing-carousel').carousel('cycle');
                $('#pauseButton').show();
                $('#playButton').hide();
            });
            $('#pauseButton').click(function () {
                $('#landing-carousel').carousel('pause');
                $('#pauseButton').hide();
                $('#playButton').show();
            });
        });
    </script>

    <asp:Panel runat="server" ID="CarouselPanel" Visible="False">
        <asp:ObjectDataSource ID="CarouselDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateCarouselAggregateDataAdapter" SelectMethod="SelectPosts" runat="server">
            <SelectParameters>
                <asp:Parameter Name="startRowIndex" DefaultValue="0" />
                <asp:Parameter Name="maximumRows" DefaultValue='<%$ SiteSetting: Landing Page Carousel Post Count, 4 %>' />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ListView ID="CarouselListView" DataSourceID="CarouselDataSource" runat="server">
            <LayoutTemplate>
                <div id="landing-carousel" class="etfo-carousel-landing carousel slide" style="margin-bottom: 40px;">
                    <div class="carousel-inner">
                        <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                    </div>
                    <div id="carouselButtons">
                        <a class="etfo-carousel-control left" href="#landing-carousel" data-slide="prev"><i class="icon-angle-left"></i></a>
                        <a class="etfo-carousel-control right" href="#landing-carousel" data-slide="next"><i class="icon-angle-right"></i></a>
                        <button id="playButton" type="button" class="playpausebutton">
                            <i class="icon-play icon-large"></i>
                        </button>
                        <button id="pauseButton" type="button" class="playpausebutton" >
                            <i class="icon-pause icon-large"></i>
                        </button>
                    </div>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <div class="item <%# Container.DataItemIndex == 0 ? @"active" : string.Empty %>">
                    <asp:Image ID="Image1" ImageUrl='<%# GetThumbnailUrl(Eval("Entity")) %>' title='<%# Eval("Title") %>' alt='<%# Eval("Summary") %>' runat="server" />
                    <div class="carousel-caption">
                        <h4>
                            <asp:HyperLink ID="HyperLink3" NavigateUrl='<%# Eval("ApplicationPath.AppRelativePath") %>' alt='<%# Eval("Summary") %>' runat="server"><%# Eval("Title") %></asp:HyperLink>
                        </h4>
                    </div>
                </div>
            </ItemTemplate>
        </asp:ListView>
    </asp:Panel>
    
    <asp:Panel runat="server" ID="AnnouncementPanel" Visible="False" CssClass="etfo-announcement-list">
        <asp:ObjectDataSource ID="AnnouncementDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateAnnouncementAggregateDataAdapter" SelectMethod="SelectPosts" runat="server">
            <SelectParameters>
                <asp:Parameter Name="startRowIndex" DefaultValue="0" />
                <asp:Parameter Name="maximumRows" DefaultValue='<%$ SiteSetting: Landing Page Announcement Post Count, 4 %>' />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ListView ID="AnnouncementListView" DataSourceID="AnnouncementDataSource" runat="server">
            <LayoutTemplate>
                <asp:ObjectDataSource ID="AnnouncementBlogDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateAnnouncementAggregateDataAdapter" SelectMethod="Select" runat="server" />
                <div class="header">
                    <asp:Repeater DataSourceID="AnnouncementBlogDataSource" runat="server">
                        <ItemTemplate>
                            <h2>
                                <%# Eval("Title") %>
                                <asp:HyperLink NavigateUrl='<%# Eval("FeedPath.AbsolutePath") %>' ImageUrl="~/img/feed-icon-14x14.png" ToolTip='<%$ Snippet: Landing Page Announcement Feed Subscribe Tooltip Label, Subscribe %>' runat="server" />
                            </h2>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <ul>
                    <li id="itemPlaceholder" runat="server" />
                </ul>
                <div class="footer">
                    <asp:Repeater DataSourceID="AnnouncementBlogDataSource" runat="server">
                        <ItemTemplate>
                            <asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AbsolutePath") %>' Text='<%$ Snippet: Landing Page All Announcements Link Text, All Announcements %>' runat="server" />
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <li class="etfo-announcement-item" runat="server">
                    <h3>
                        <asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AppRelativePath") %>' runat="server"><%# Eval("Title") %></asp:HyperLink>
                    </h3>
                    <div>
                        <%# Eval("Summary") %>
                    </div>
                    <asp:Panel ID="Panel3" runat="server" Visible='<%# (((BlogCommentPolicy)Eval("CommentPolicy")) == BlogCommentPolicy.None) %>'>
                        <abbr class="posttime"><%# Eval("Entity.Adx_date", "{0:r}") %></abbr>
                    </asp:Panel>
                    <asp:Panel ID="Panel4" runat="server" Visible='<%# (((BlogCommentPolicy)Eval("CommentPolicy")) != BlogCommentPolicy.None) %>'>
                        <%# Eval("Entity.Adx_date", "{0:f}") %>
						&ndash;
						<asp:HyperLink ID="HyperLink4" NavigateUrl='<%# string.Format("{0}#comments", Eval("ApplicationPath.AbsolutePath")) %>' runat="server">
							<i class="fa fa-comment"></i> <%# Eval("CommentCount") %>
                        </asp:HyperLink>
                    </asp:Panel>
                </li>
            </ItemTemplate>
        </asp:ListView>
    </asp:Panel>
    
    <asp:Panel runat="server" ID="BlogPanel" Visible="False" CssClass="etfo-blog-list">
        <asp:ObjectDataSource ID="BlogDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateBlogAggregateDataAdapter" SelectMethod="SelectPosts" runat="server">
            <SelectParameters>
                <asp:Parameter Name="startRowIndex" DefaultValue="0" />
                <asp:Parameter Name="maximumRows" DefaultValue='<%$ SiteSetting: Landing Page Blog Post Count, 4 %>' />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ListView ID="BlogListView" DataSourceID="BlogDataSource" runat="server">
            <LayoutTemplate>
                <asp:ObjectDataSource ID="DefaultBlogDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateBlogAggregateDataAdapter" SelectMethod="Select" runat="server" />
                <div class="header">
                    <asp:Repeater DataSourceID="DefaultBlogDataSource" runat="server">
                        <ItemTemplate>
                            <h2>
                                <%# Eval("Title") %>
                                <asp:HyperLink NavigateUrl='<%# Eval("FeedPath.AbsolutePath") %>' ImageUrl="~/img/feed-icon-14x14.png" ToolTip='<%$ Snippet: Landing Page Blog Posts Feed Subscribe Tooltip Label, Subscribe %>' runat="server" />
                            </h2>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <ul>
                    <li id="itemPlaceholder" runat="server" />
                </ul>
                <div class="footer">
                    <asp:Repeater DataSourceID="DefaultBlogDataSource" runat="server">
                        <ItemTemplate>
                            <asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AbsolutePath") %>' Text='<%$ Snippet: Landing Page All Blog Posts Link Text, All Blog Posts %>' runat="server" />
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <li class="etfo-blog-item clearfix" runat="server">
                    <div class="blog-image">
                        <asp:Image ImageUrl='<%# GetThumbnailUrl(Eval("Entity")) %>' runat="server" />
                        <div class="blog-image-title">
                            <%# Eval("Title") %>
                            <span class="blog-title-fade"></span>
                        </div>
                    </div>
                    <div class="blog-content">
                        <h3>
                            <asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AppRelativePath") %>' runat="server"><%# Eval("Title") %></asp:HyperLink>
                        </h3>
                        <asp:Panel ID="Panel1" runat="server" CssClass="blog-date-comment" Visible='<%# (((BlogCommentPolicy)Eval("CommentPolicy")) == BlogCommentPolicy.None) %>'>
                            <i class="fa fa-calendar"></i>
                            <abbr class="posttime"><%# Eval("Entity.Adx_date", "{0:d}") %></abbr>
                        </asp:Panel>
                        <asp:Panel ID="Panel2" runat="server" CssClass="blog-date-comment" Visible='<%# (((BlogCommentPolicy)Eval("CommentPolicy")) != BlogCommentPolicy.None) %>'>
                            <span class="blog-date"><i class="fa fa-calendar"></i><%# Eval("Entity.Adx_date", "{0:d}") %></span>
                            <asp:HyperLink ID="HyperLink2" NavigateUrl='<%# string.Format("{0}#comments", Eval("ApplicationPath.AbsolutePath")) %>' CssClass="blog-comment" runat="server">
										<i class="fa fa-comment"></i> <%# Eval("CommentCount") %>
                            </asp:HyperLink>
                        </asp:Panel>
                         <div class="blognewssummary"><%# Eval("Summary") %><asp:HyperLink ID="HyperLink1" CssClass="blogsummaryreadmore" NavigateUrl='<%# Eval("ApplicationPath.AppRelativePath") %>' runat="server">Read More</asp:HyperLink></div> 
                    </div>
                </li>
            </ItemTemplate>
        </asp:ListView>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="EntityControls" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentBottom" runat="server">
</asp:Content>
<asp:Content ID="Content13" ContentPlaceHolderID="SidebarAbove" runat="server">
    <ul class="etfo-event-nav pre-nav nav nav-tabs nav-stacked">
        <li class="item-purple">
            <asp:HyperLink runat="server" ID="HyperLinkUpcoming" Text="<%$ Snippet: Upcoming Events Label, Upcoming Events %>"></asp:HyperLink>
        </li>
        <li class="item-pink">
            <asp:HyperLink runat="server" ID="HyperLinkHistorical" Text="<%$ Snippet: Historical Events Label, Historical Events %>"></asp:HyperLink>
        </li>
    </ul>
    <%--<site:ChildNavigation runat="server"/>--%>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="SidebarBottom" runat="server">
    <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/ETFOnews" data-widget-id="392320152093458432">Tweets by @ETFOnews</a>
    <script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");</script>
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
