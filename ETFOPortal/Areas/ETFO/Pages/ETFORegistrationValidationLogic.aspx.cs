﻿using Site.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Site.Helpers;
using Xrm;
using Microsoft.Xrm.Sdk;
using AttributeCollection = Microsoft.Xrm.Sdk.AttributeCollection;

namespace Site.Areas.ETFO.Pages
{
    public partial class ETFORegistrationValidationLogic : PortalPage
    {
        #region Declarations

        Guid EventId
        {
            get
            {
                var eventId = Guid.Empty;
                var isGuid = ReferenceEquals(Request.QueryString["oems_EventId"], null) ? false : Guid.TryParse(Request.QueryString["oems_EventId"].ToString(), out eventId);

                return isGuid ? eventId : Guid.Empty;
            }
        }

        int Type
        {
            get
            {
                var type = 1;
                var isInt = ReferenceEquals(Request.QueryString["type"], null) ? false : Int32.TryParse(Request.QueryString["type"].ToString(), out type);

                return isInt && (type == 1 || type == 2 || type == 3 || type == 4) ? type : 0;
            }
        }

        Guid EventRegistrationId
        {
            get
            {
                Guid eventRegistrationId = Guid.Empty;
                bool isGuid = ReferenceEquals(Request.QueryString["oems_EventRegistrationId"], null) ? false : Guid.TryParse(Request.QueryString["oems_EventRegistrationId"].ToString(), out eventRegistrationId);

                return isGuid ? eventRegistrationId : Guid.Empty;
            }
        }

        #endregion Declarations

        protected void Page_Load(object sender, EventArgs e)
        {
            RedirectToLoginIfAnonymous(); 

            //registered
            if (EventRegistrationId == Guid.Empty && EventId == Guid.Empty && Type != 1 && Type != 2 && Type != 3 && Type != 4 && Type != 5)
            {
                return;
            }

            var registrationType = 0;

            if (Type == 1 || Type == 4) // Event Registration, Course Registration
            {
                registrationType = RegistrationHelper.EventRegistrationType.Attendee;
                
                var registrationId = Guid.Empty;
                var isUserRegistered = Contact != null && RegistrationHelper.IsUserRegisteredToEvent(Contact.Id, EventId, out registrationId);
                if (isUserRegistered && EventRegistrationId != registrationId && EventRegistrationId != Guid.Empty)
                {
                    return;
                }

                var registrationOpen = RegistrationHelper.RegistrationOpen(Contact.Id, EventId, Portal);
                if (registrationOpen || isUserRegistered)
                {
                    isRegistrationOpen.Value = "1";
                }
                else
                {
                    isRegistrationOpen.Value = "0";
                }
            }
            else if (Type == 2 || Type == 3) // Event Application, Course Term Application
            {
                registrationType = RegistrationHelper.EventRegistrationType.Presenter;
                
                var registrationId = Guid.Empty;
                var isUserApplied = Contact != null && RegistrationHelper.IsUserAppliedToEvent(Contact.Id, EventId, out registrationId);
                if (isUserApplied && EventRegistrationId != registrationId && EventRegistrationId != Guid.Empty)
                {
                    return;
                }

                var applicationOpen = RegistrationHelper.ApplicationOpen(Contact.Id, EventId, Portal);
                if (applicationOpen || isUserApplied)
                {
                    isRegistrationOpen.Value = "1";
                }
                else
                {
                    isRegistrationOpen.Value = "0";
                }
            }

            if (isRegistrationOpen.Value == "1")
            {
                if (Type == 3)
                {
                    CreateEventRegistration(registrationType);
                    return;
                }

                oems_EventRegistration eventRegistration = null;
                if (registrationType == RegistrationHelper.EventRegistrationType.Attendee)
                {
                    eventRegistration =
                    (
                        from er in ServiceContext.CreateQuery<oems_EventRegistration>()
                        where er.oems_Contact.Id == Contact.Id
                        && er.oems_Event.Id == EventId
                        && er.statecode == 0
                        && er.statuscode != RegistrationHelper.EventRegistration.Cancelled
                        && (er["oems_registrationtype"] == null || ((OptionSetValue)er["oems_registrationtype"]).Value == RegistrationHelper.EventRegistrationType.Attendee)
                        select er
                    ).FirstOrDefault();
                }
                else
                {
                    eventRegistration =
                    (
                        from er in ServiceContext.CreateQuery<oems_EventRegistration>()
                        where er.oems_Contact.Id == Contact.Id
                        && er.oems_Event.Id == EventId
                        && er.statecode == 0
                        && er.statuscode != RegistrationHelper.EventRegistration.Cancelled
                        && er["oems_registrationtype"] != null 
                        && ((OptionSetValue)er["oems_registrationtype"]).Value == RegistrationHelper.EventRegistrationType.Presenter
                        select er
                    ).FirstOrDefault();
                }

                if (!ReferenceEquals(eventRegistration, null))
                {
                    isRegistered.Value = "1";
                    oems_EventRegistrationId.Value = eventRegistration.Id.ToString();

                    Response.Redirect(GetRegistrationUrl(eventRegistration.Id.ToString()));
                }
                else
                {
                    var invitedGuest = registrationType == RegistrationHelper.EventRegistrationType.Attendee
                        ? 
                        (
                            from ig in ServiceContext.CreateQuery<oems_EventInvitedGuest>()
                            where ig.oems_Invitee.Id == Contact.Id && ig.oems_Event.Id == EventId
                            select ig
                        ).FirstOrDefault() 
                        : null;


                    if (!ReferenceEquals(invitedGuest, null))
                    {
                        isUserInvitedGuest.Value = "1";
                        eventInvitedGuestId.Value = invitedGuest.Id.ToString();
                        isRegistered.Value = "0";
                    }
                    else
                    {
                        CreateEventRegistration(registrationType);
                    }

                    //set client side variables
                    Eventid.Value = EventId.ToString();
                }
            }
        }

        #region Helpers

        private void CreateEventRegistration(int registrationType)
        {
            var reg = new oems_EventRegistration
            {
                oems_Event = new EntityReference("oems_event", EventId),
                oems_Contact = new EntityReference("contact", Contact.Id)
            };
            reg["oems_registrationtype"] = new OptionSetValue(registrationType);

            Guid regid = XrmContext.Create(reg);
            Response.Redirect(GetRegistrationUrl(regid.ToString()));

            oems_EventRegistrationId.Value = regid.ToString();
            isRegistered.Value = "1";
        }

        private string GetRegistrationUrl(string eventRegistrationId)
        {
            if (EventRegistrationId == null)
            {
                return null;
            }

            var registrationUrl = GetUrlForRequiredSiteMarker("Registration Page");

            registrationUrl.QueryString.Set("oems_EventRegistrationId", eventRegistrationId);
            registrationUrl.QueryString.Set("type", Type.ToString());

            return registrationUrl.ToString();
        }

        #endregion
    }
}