﻿using System;
using System.Web.UI.HtmlControls;
using Site.Pages;
using System.Linq;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using Site.Areas.ETFO.Library;
using Site.Helpers;
using Microsoft.Xrm.Portal.Web;

namespace Site.Areas.ETFO.Pages
{
    public partial class UpcomingEvents : PortalPage
    {
        public string GridSearchID;
        public string GridSearchPlaceholder;

        protected void Page_Init(object sender, EventArgs e)
        {
                // temp fix UP2 .. it will be slow because query runs twice
                var allEvents = new EventListings(null, null, false, null, Portal);
                var eventTypes =
                    allEvents.Select("", 1000, 0)
                             .Select(et => new {id = et.EventTypeID, name = et.EventTypeName})
                             .Distinct();

                EventTypeDropdown.DataSource = eventTypes;
                EventTypeDropdown.DataTextField = "name";
                EventTypeDropdown.DataValueField = "id";
                EventTypeDropdown.DataBind();
                
                if(!Page.IsPostBack && !String.IsNullOrEmpty(Request.QueryString["subcategoryId"]))
                {
                    EventTypeDropdown.SelectedValue = Request.QueryString["subcategoryId"];
                }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            GridSearchID = "#"+GridSearchText.ClientID;
            GridSearchPlaceholder = ServiceContext.GetSnippetValueByName(Portal.Website, "UpcomingEvents/GridSearchPlaceholder")?? "search events";
        }

        public void UpcomingEventsDataSource_OnObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            if (e == null)
            {
                return;
            }            
            
            Guid? contactId=null;
            if (Contact != null)
            {
                contactId = this.Contact.Id;
            }
            Guid? subcategory = null;
            
            if (EventTypeDropdown.SelectedValue != "")
            {
                subcategory = new Guid(EventTypeDropdown.SelectedValue);
            }
            var searchText = GridSearchText.Text;
            var eventListings = new EventListings(subcategory, searchText, false, contactId, Portal);

            e.ObjectInstance = eventListings;
        }

        protected void FilterButton_Click(object sender, EventArgs e)
        {            
            EventListDataSource.DataBind();
        }
        
        protected void GridSearchButton_Click(object sender, EventArgs e)
        {
            EventListDataSource.DataBind();
        }

        public bool showRegistrationButton(Guid eventID)
        {
            if (Contact == null)
                return RegistrationHelper.RegistrationOpen(null, eventID, Portal);
            else
                return RegistrationHelper.RegistrationOpen(Contact.Id, eventID, Portal);
            
        }

        public bool showApplicationButton(Guid eventID)
        {
            if (Contact == null)
                return RegistrationHelper.ApplicationOpen(null, eventID, Portal);
            else
                return RegistrationHelper.ApplicationOpen(Contact.Id, eventID, Portal);

        }

        protected bool ShowMyRegistrationApplicationButton(int? eventStatus, int? registrationStatus)
        {
            return RegistrationHelper.CanViewRegistration(eventStatus, registrationStatus);
        }

        public bool isEventFull(Object isFull)
        {
             return (bool)isFull;
        }

        public bool isUserRegisteredToEvent(Guid eventId, out Guid registrationId)
        {
            registrationId = Guid.Empty;
            return Contact != null && RegistrationHelper.IsUserRegisteredToEvent(Contact.Id, eventId, out registrationId);
        }

        public bool isUserAppliedToEvent(Guid eventId, out Guid registrationId)
        {
            registrationId = Guid.Empty;
            return Contact != null && RegistrationHelper.IsUserAppliedToEvent(Contact.Id, eventId, out registrationId);
        }

        public string GetRegistrationButtonText(Guid eventId)
        {
            var text = XrmHelper.GetSnippetValueOrDefault("UpcomingEvents/RegisterButtonText", "Register");
            if (Contact != null)
            {
                Guid registrationId;
                var isRegistered = RegistrationHelper.IsUserRegisteredToEvent(Contact.Id, eventId, out registrationId);

                if (isRegistered)
                {
                    text = XrmHelper.GetSnippetValueOrDefault("UpcomingEvents/ManageRegistrationButtonText", "My Registration");
                }
            }

            return text;
        }

        public string GetApplicationButtonText(Guid eventId)
        {
            var text = XrmHelper.GetSnippetValueOrDefault("UpcomingEvents/ApplyButtonText", "Apply");
            if (Contact != null)
            {
                Guid registrationId;
                var isRegistered = RegistrationHelper.IsUserAppliedToEvent(Contact.Id, eventId, out registrationId);

                if (isRegistered)
                {
                    text = XrmHelper.GetSnippetValueOrDefault("UpcomingEvents/ManageApplicationButtonText", "My Application");
                }
            }

            return text;
        }

        protected void Register_Command(object sender, CommandEventArgs e)
        {
           var regPage = ServiceContext.GetPageBySiteMarkerName(Website, "Registration Validation");
           var url = new UrlBuilder(ServiceContext.GetUrl(regPage));
           url.QueryString.Set("oems_EventId", e.CommandArgument.ToString());
           url.QueryString.Set("type", "1");
           Response.Redirect(url.PathWithQueryString); 
        }

        protected void Application_Command(object sender, CommandEventArgs e)
        {
            var regPage = ServiceContext.GetPageBySiteMarkerName(Website, "Registration Validation");
            var url = new UrlBuilder(ServiceContext.GetUrl(regPage));
            url.QueryString.Set("oems_EventId", e.CommandArgument.ToString());
            url.QueryString.Set("type", "2");
            Response.Redirect(url.PathWithQueryString);
        }

        protected void UpcomingEventsView_OnSorting(Object sender, ListViewSortEventArgs e)
        {
            // Check the sort direction to set the image URL accordingly.
            var cssClass = e.SortDirection == SortDirection.Ascending ? "icon-caret-up rediconcolor" : "icon-caret-down rediconcolor";

            // Check which field is being sorted
            // to set the visibility of the image controls.
            var sortTypeName = (HtmlGenericControl)UpcomingEventsView.FindControl("sortTypeName");
            var sortTitle = (HtmlGenericControl)UpcomingEventsView.FindControl("sortTitle");
            var sortStatus = (HtmlGenericControl)UpcomingEventsView.FindControl("sortStatus");
            var sortStartDate = (HtmlGenericControl)UpcomingEventsView.FindControl("sortStartDate");
            var sortEndDate = (HtmlGenericControl)UpcomingEventsView.FindControl("sortEndDate");
            var sortEndDeadline = (HtmlGenericControl)UpcomingEventsView.FindControl("sortEndDeadline");

            sortTypeName.Visible = false;
            sortTitle.Visible = false;
            sortStatus.Visible = false;
            sortStartDate.Visible = false;
            sortEndDate.Visible = false;
            sortEndDeadline.Visible = false;

            switch (e.SortExpression)
            {
                case "EventTypeName":
                    sortTypeName.Visible = true;
                    sortTypeName.Attributes["class"] = cssClass;
                    break;
                case "EventTitle":
                    sortTitle.Visible = true;
                    sortTitle.Attributes["class"] = cssClass;
                    break;
                case "EventStatusText":
                    sortStatus.Visible = true;
                    sortStatus.Attributes["class"] = cssClass;
                    break;
                case "StartDate":
                    sortStartDate.Visible = true;
                    sortStartDate.Attributes["class"] = cssClass;
                    break;
                case "EndDate":
                    sortEndDate.Visible = true;
                    sortEndDate.Attributes["class"] = cssClass;
                    break;
                case "RegistrationDeadline":
                    sortEndDeadline.Visible = true;
                    sortEndDeadline.Attributes["class"] = cssClass;
                    break;

            }
        }

        protected void UpcomingEventsView_OnLayoutCreated(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UpcomingEventsView.Sort("StartDate", SortDirection.Ascending);
            }
        }

        protected void UpcomingEventsView_DataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var dataItem = (ListViewDataItem)e.Item;
                if (dataItem.DisplayIndex == UpcomingEventsView.EditIndex)
                {
                    var item = e.Item;
                    var el = (EventListing)e.Item.DataItem;
                    var register = (Button)item.FindControl("Register");
                    var application = (Button)item.FindControl("Application");
                    var waitingList = (Button)item.FindControl("WaitingList");
                    var isFullPanel = (Panel)item.FindControl("IsFullPanel");
                    var eventFullBtn = (HtmlGenericControl)isFullPanel.FindControl("eventFullBtn");
                    var eventFullText = (HtmlGenericControl)isFullPanel.FindControl("eventFullText");

                    //If waiting list is enabled dont call .IsFull to avoid that query
                    Guid registrationId;
                    var isFull = !el.WaitingListComponentFlag && isEventFull(el.IsFull);
                    var isUserRegistered = isUserRegisteredToEvent(el.EventId.Value, out registrationId);
                    var isUserApplied = isUserAppliedToEvent(el.EventId.Value, out registrationId);

                    var showRegistration = showRegistrationButton(el.EventId.Value);
                    var showApplication = showApplicationButton(el.EventId.Value);
                    if(isUserRegistered || isUserApplied)
                    {
                        var registrationStatus = RegistrationHelper.GetRegistrationStatus(registrationId);
                        isUserRegistered = ShowMyRegistrationApplicationButton(el.EventStatus, registrationStatus);
                        isUserApplied = isUserRegistered;
                    }

                    register.Visible = showRegistration && (!isFull || isUserRegistered);
                    application.Visible = showApplication || isUserApplied;
                    waitingList.Visible = false;
                    isFullPanel.Visible = isFull && !isUserRegistered;

                    //Registration Periods
                    var registrationPeriodsContainer = (HtmlGenericControl)isFullPanel.FindControl("registrationPeriodsContainer");
                    var registrationPeriods = EventListings.GetRegistrationPeriods(Portal, el.EventId.Value, el.EventStatus.Value);
                    if (!string.IsNullOrWhiteSpace(registrationPeriods))
                    {
                        registrationPeriodsContainer.InnerHtml = registrationPeriods + "<br/>";
                    }

                    //Application Schedules
                    var ApplicationSchedulesContainer = (HtmlGenericControl)isFullPanel.FindControl("ApplicationSchedulesContainer");
                    var ApplicationSchedules = EventListings.GetEventApplicationSchedules(el.EventId.Value, el.EventStatus.Value);
                    if (!string.IsNullOrWhiteSpace(ApplicationSchedules))
                    {
                        ApplicationSchedulesContainer.InnerHtml = ApplicationSchedules + "<br/>";
                    }

                    //Waiting list is enabled?
                    if (!el.WaitingListComponentFlag) 
                        return;

                    //Waiting list additional validations
                    if (el.WaitingListFullFlag)
                    {
                        register.Visible = false;
                        isFullPanel.Visible = true;
                        eventFullBtn.Visible = false;
                        eventFullText.Visible = true;
                        eventFullText.InnerText = XrmHelper.GetSnippetValueOrDefault("UpcomingEvents/WaitingList/RegistrationClosedEventFull", "Registration closed: this event is completely full");
                    }
                    else if (el.EventFullFlag)
                    {
                        if (!isUserRegistered)
                        {
                            isFullPanel.Visible = true;
                            eventFullBtn.Visible = false;
                            eventFullText.Visible = true;
                            register.Visible = false;
                            application.Visible = false;
                            waitingList.Visible = true;
                            eventFullText.InnerText = XrmHelper.GetSnippetValueOrDefault("UpcomingEvents/WaitingList/EventFullSpotsAvailable", "This event is full but waiting list spots are available");
                        }
                        else
                        {
                            //Is this person on the waiting list?
                            var isWaiting = RegistrationHelper.IsRegistrationWithWaitingListWaiting(el.EventId.Value, registrationId);
                            isFullPanel.Visible = false;
                            if (isWaiting)
                            {
                                isFullPanel.Visible = true;
                                eventFullBtn.Visible = false;
                                eventFullText.Visible = true;
                                eventFullText.InnerText = XrmHelper.GetSnippetValueOrDefault("UpcomingEvents/WaitingList/OnWaitingList", "On waiting list");
                            }
                            else
                            {
                                isFullPanel.Visible = true;
                                eventFullBtn.Visible = false;
                                eventFullText.Visible = true;
                                register.Visible = false;
                                application.Visible = false;
                                waitingList.Visible = true;
                                eventFullText.InnerText = XrmHelper.GetSnippetValueOrDefault("UpcomingEvents/WaitingList/EventFullSpotsAvailable", "This event is full but waiting list spots are available");
                            }
                        }
                    }
                    else
                    {
                        isFullPanel.Visible = false;
                    }
                }
            }
        }
    }
}