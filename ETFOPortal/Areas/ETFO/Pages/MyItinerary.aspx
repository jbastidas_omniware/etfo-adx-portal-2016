﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="MyItinerary.aspx.cs" Inherits="Site.Areas.ETFO.Pages.MyItinerary" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Import Namespace="Site.Areas.ETFO.Library" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css" media="print">
        .hidewhenprint {
            display: none;
        }
    </style>  
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageHeader" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <div ID="MyItineraryPanel">
        <div id="General" class="col-md-12 col-sm-12">
            <div class="body">
                <div class="table">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="my-etfo-title">
                                <h3><asp:Label ID="EventName" runat="server" /></h3>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <input type="button" value="Print this page" onClick="window.print()">
                        </div>
                    </div>
                </div>
            </div>
            <div class="body">
                <div class="table">
                    <div class="row" runat="server" id="CourseTermContainer">
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet21" runat="server" SnippetName="MyItinerary/General/CourseTerm" DefaultText="Course Term:" />
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <asp:Label runat="server" ID="CourseTerm"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet1" runat="server" SnippetName="MyItinerary/General/EventDateLabel" DefaultText="Event Dates:" />
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <crm:Snippet ID="Snippet6" runat="server" SnippetName="MyItinerary/General/FromLabel" DefaultText="From: " />
                            <asp:Label runat="server" ID="StartDate"></asp:Label>
                            <crm:Snippet ID="Snippet7" runat="server" SnippetName="MyItinerary/General/ToLabel" DefaultText="To: " />
                            <asp:Label ID="EndDate" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet2" runat="server" SnippetName="MyItinerary/General/RegStatusLabel" DefaultText="Registration Status: " />
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <asp:Label ID="RegStatus" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet33" runat="server" SnippetName="MyItinerary/ETFOContact/EventOwnerLabel" DefaultText="Event Owner Team: " />
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <asp:Label ID="EventOwner" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="Location" class="col-md-6 col-sm-6" runat="server">
            <div class="title item-grey">
                <crm:Snippet ID="Snippet3" runat="server" SnippetName="MyItinerary/Location/LocationHeaderText" DefaultText="Event Location"></crm:Snippet>
            </div>
            <div class="body">
                <div class="table">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <asp:Label ID="LocationName" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <asp:Label ID="LocationStreet" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                             <asp:Label ID="LocationCity" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <asp:Label ID="LocationPostcode" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <asp:Label ID="LocationTel" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="AccommodationLocation" class="col-md-6 col-sm-6" runat="server">
            <div class="title item-grey">
                <crm:Snippet ID="Snippet4" runat="server" SnippetName="MyItinerary/AccommodationLocation/AccommodationLocationHeaderText" DefaultText="Accommodation Location"></crm:Snippet>
            </div>
            <div class="body">
                <div class="table">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <asp:Label ID="AccomoLocationName" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <asp:Label ID="AccomoLocationStreet" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                             <asp:Label ID="AccomoLocationCity" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <asp:Label ID="AccomoLocationPostcode" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <asp:Label ID="AccomoTel" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="ETFOContact" class="col-md-12 col-sm-12" runat="server">
            <%-- display multiple ETFO contacts' name and phone number --%>
            <div class="title item-grey">
                <crm:Snippet ID="Snippet5" runat="server" SnippetName="MyItinerary/ETFOContact/EventDateLabel" DefaultText="ETFO Contact" />
            </div>
            <div class="body">
                <asp:Panel ID="SupportStaffOwnerPanel" CssClass="table" runat="server">
                </asp:Panel>
            </div>
        </div>
        <div id="LocalDelegation" class="col-md-12 col-sm-12" runat="server">
            <div class="title item-grey">
                <crm:Snippet ID="Snippet8" runat="server" SnippetName="MyItinerary/LocalDelegation/LocalDelegationLabel" DefaultText="Local Delegation" />
            </div>
            <div class="body">
                <div class="table">
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet9" runat="server" SnippetName="MyItinerary/LocalDelegation/LocalDelegationLabel" DefaultText="Attending As: " />
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <asp:Label ID="AttendingAs" Text="Delegate" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="Course"  class="col-md-12 col-sm-12" runat="server">
            <div class="title item-grey">
                <crm:Snippet ID="Snippet22" runat="server" SnippetName="MyItinerary/Course/CourseLabel" DefaultText="Course" />
            </div>
            <div class="body">
                <div class="table">
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet24" runat="server" SnippetName="MyItinerary/Course/Description" DefaultText="Course Description: " />
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <asp:Label ID="CourseDescription" Text="" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet25" runat="server" SnippetName="MyItinerary/Course/Presenters" DefaultText="Presenters: " />
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <asp:Label ID="CoursePresenters" Text="" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snipet99" runat="server" SnippetName="MyItinerary/Course/CourseLocation" DefaultText="Course Location: " />
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <asp:Label ID="CourseLocation" Text="" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet23" runat="server" SnippetName="MyItinerary/Course/CourseTargetGrades" DefaultText="Course Target Grades: " />
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <asp:Label ID="CourseTargetGrades" Text="" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet27" runat="server" SnippetName="MyItinerary/Course/CourseTopic" DefaultText="Course Topic: " />
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <asp:Label ID="CourseTopic" Text="" runat="server" />
                        </div>
                        <div class="col-md-2 col-sm-2"></div>
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet28" runat="server" SnippetName="MyItinerary/Course/CourseCode" DefaultText="Course Code: " />
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <asp:Label ID="CourseCode" Text="" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet26" runat="server" SnippetName="MyItinerary/Course/CourseDateSlot" DefaultText="Course Date Slot: " />
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <asp:Label ID="CourseDateSlot" Text="" runat="server" />
                        </div>
                        <div class="col-md-2 col-sm-2"></div>
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet29" runat="server" SnippetName="MyItinerary/Course/CourseWeekNumber" DefaultText="Course Week Number: " />
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <asp:Label ID="CourseWeekNumber" Text="" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet30" runat="server" SnippetName="MyItinerary/Course/CourseTimeSlot" DefaultText="Course Time Slot: " />
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <asp:Label ID="CourseTimeSlot" Text="" runat="server" />
                        </div>
                        <div class="col-md-2 col-sm-2"></div>
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet32" runat="server" SnippetName="MyItinerary/Course/CourseLocalHost" DefaultText="Course Local Host: " />
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <asp:Label ID="CourseLocalHost" Text="" runat="server" />
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div id="AccommodationDetails" class="col-md-12 col-sm-12" runat="server">
            <div class="title item-grey">
                <crm:Snippet ID="Snippet10" runat="server" SnippetName="MyItinerary/AccommodationDetails/AccommodationDetailsLabel" DefaultText="Accommodation Details" />
            </div>
            <div class="body">
                <div class="table">
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet11" runat="server" SnippetName="MyItinerary/AccommodationDetails/ArrivingDateLabel" DefaultText="Arriving Date: " />
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <asp:Label ID="ArriveDate" Text="Feb 22, 2014" runat="server" />
                        </div>
                        <div class="col-md-2 col-sm-2"></div>
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet12" runat="server" SnippetName="MyItinerary/AccommodationDetails/DepartingDateLabel" DefaultText="Departing Date: " />
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <asp:Label ID="DepartureDate" Text="Feb 24, 2014" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet13" runat="server" SnippetName="MyItinerary/AccommodationDetails/AccommodationDateLabel" DefaultText="Room Type: " />
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <asp:Label ID="RoomType" Text="Double" runat="server" />
                        </div>
                        <div class="col-md-2 col-sm-2"></div>
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet14" runat="server" SnippetName="MyItinerary/AccommodationDetails/AccommodationDateLabel" DefaultText="Smoking: " />
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <asp:Label ID="Smoking" Text="No" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet15" runat="server" SnippetName="MyItinerary/AccommodationDetails/AccommodationDateLabel" DefaultText="Room Mate: " />
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <asp:Label ID="RoomMate" Text="Denise Jones" runat="server" />
                        </div>
                        <div class="col-md-5 col-sm-5"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="Caucus"  class="col-md-12 col-sm-12" runat="server">
            <div class="title item-grey">
                <crm:Snippet ID="Snippet16" runat="server" SnippetName="MyItinerary/Caucus/CaucusLabel" DefaultText="Caucus" />
            </div>
            <asp:Panel ID="CaucusPanel" CssClass="body" runat="server">
            </asp:Panel>
        </div>
        <div id="ChildCare"  class="col-md-12 col-sm-12" runat="server">
            <div class="title item-grey">
                <crm:Snippet ID="Snippet19" runat="server" SnippetName="MyItinerary/ChildCare/ChildCareLabel" DefaultText="Child Care" />
            </div>
            <asp:Panel ID="ChildCarePanel" CssClass="body" runat="server">
            </asp:Panel>
        </div>
        <div id="PersonalAccommodation" class="col-md-12 col-sm-12" runat="server">
            <div class="title item-grey">
                <crm:Snippet ID="Snippet31" runat="server" SnippetName="MyItinerary/PersonalAccommodation/PersonalAccommodationLabel" DefaultText="Personal Accommodation" />
            </div>
            <div class="body">
                <asp:Panel ID="PersonalAccommodationPanel" CssClass="table" runat="server">
                </asp:Panel>
            </div>
        </div>
        <div id="ReleaseTime" class="col-md-12 col-sm-12" runat="server">
            <div class="title item-grey">
                <crm:Snippet ID="Snippet35" runat="server" SnippetName="MyItinerary/ReleaseTime/ReleaseTimeLabel" DefaultText="Release Time" />
            </div>
            <div class="body">
                <asp:Panel ID="ReleaseTimePanel" CssClass="table" runat="server"></asp:Panel>
            </div>
        </div>
        <div id="Billing" class="col-md-12 col-sm-12" runat="server">
            <div class="title item-grey">
                <crm:Snippet ID="Snippet17" runat="server" SnippetName="MyItinerary/BillingDetails/BillingDetailsLabel" DefaultText="Billing Details" />
            </div>
            <div class="body">
                <div class="table">
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet18" runat="server" SnippetName="MyItinerary/BillingDetails/BillingStatus" DefaultText="Billing Status: " />
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <asp:Label ID="BillingStatus" Text="" runat="server" />
                        </div>
                        <div class="col-md-2 col-sm-2"></div>
                        <div class="col-md-3 col-sm-3">
                            <crm:Snippet ID="Snippet20" runat="server" SnippetName="MyItinerary/BillingDetails/BillingAmount" DefaultText="Billing Amount: " />
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <asp:Label ID="BillingAmount" Text="" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content13" ContentPlaceHolderID="SidebarAbove" runat="server">
    <div class="hidewhenprint">
        <ul class="etfo-event-nav pre-nav nav nav-tabs nav-stacked">
            <li class="item-purple">
                <crm:CrmHyperLink runat="server" ID="HyperLinkUpcoming" Text="<%$ Snippet: Upcoming Events Label, Upcoming Events %>" SiteMarkerName="Upcoming Events"></crm:CrmHyperLink>
            </li>
            <li class="item-pink">
                <crm:CrmHyperLink runat="server" ID="HyperLinkHistorical" Text="<%$ Snippet: Historical Events Label, Historical Events %>" SiteMarkerName="Historical Events"></crm:CrmHyperLink>
            </li>
        </ul>
    </div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="SidebarBottom" runat="server">
    <div class="hidewhenprint">
        <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/ETFOnews" data-widget-id="392320152093458432">Tweets by @ETFOnews</a>
        <script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");</script>
    </div>
</asp:Content>  

<asp:Content ID="Content11" ContentPlaceHolderID="SidebarBelow" runat="server">
    <div class="hidewhenprint">
        <adx:AdPlacement ID="AdPlacement1" runat="server" PlacementName="Sidebar Bottom" CssClass="ad" />
    </div>
</asp:Content>

<asp:Content ID="Content12" ContentPlaceHolderID="Scripts" runat="server">   
</asp:Content>