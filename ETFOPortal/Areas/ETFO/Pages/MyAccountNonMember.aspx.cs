﻿using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using Site.Helpers;
using Site.Pages;
using Xrm;

namespace Site.Areas.ETFO.Pages
{
    public partial class MyAccountNonMember : PortalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RedirectToLoginIfAnonymous();

            if (Page.IsPostBack)
                return;

            if (!Contact.oems_isnon_member.HasValue || !Contact.oems_isnon_member.Value)
            {
                var myAccountPage = ServiceContext.GetPageBySiteMarkerName(Website, "My Account Member");
                Response.Redirect(ServiceContext.GetUrl(myAccountPage));
            }
            
            FirstNameTextBox.Text = Contact.FirstName;
            LastNameTextBox.Text = Contact.LastName;
            EmailTextBox.Text = Contact.EMailAddress1;

            OrganizationTextBox.Text = Contact.oems_NonMemberOrganization;
            if (Contact.GenderCode.GetValueOrDefault() == 0)
            {
                GenderDropdown.SelectedIndex = -1;
            }
            else
            {
                GenderDropdown.SelectedValue = Contact.GenderCode.GetValueOrDefault().ToString(CultureInfo.InvariantCulture);
            }

            NonMemberBirthdayHiddenField.Value = Contact.BirthDate.HasValue ? Contact.BirthDate.Value.ToString("M-dd-yyyy") : string.Empty;
            
            Address1TextBox.Text = Contact.Address1_Line1;
            Address2TextBox.Text = Contact.Address1_Line2;
            CityTextBox.Text = Contact.Address1_City;
            ProvinceTextBox.Text = Contact.Address1_StateOrProvince;
            CountryTextBox.Text = Contact.Address1_Country;
            PostalCodeTextBox.Text = Contact.Address1_PostalCode;
            WorkPhoneTextBox.Text = Contact.Telephone1;
            HomePhoneTextBox.Text = Contact.Telephone2;
            CellPhoneTextBox.Text = Contact.Telephone3;
            FaxTextBox.Text = Contact.Fax;

            //Self Identification

            FirstNationSelfIdentCheckBox.Checked = Contact.mbr_FirstNation.GetValueOrDefault();
            InuitSelfIdentCheckBox.Checked = Contact.mbr_Inuit.GetValueOrDefault();
            MetisSelfIdentCheckBox.Checked = Contact.mbr_Metis.GetValueOrDefault();
            MinoritySelfIdentCheckBox.Checked = Contact.mbr_RacializedGroup.GetValueOrDefault();
            LgbtSelfIdentCheckBox.Checked = Contact.mbr_GayLesbianBisexualTransgender.GetValueOrDefault();
            DisabledSelfIdentCheckBox.Checked = Contact.mbr_Disabled.GetValueOrDefault();
            WomanSelfIdentCheckBox.Checked = Contact.mbr_Woman.GetValueOrDefault();

            // user profile tab
            EmergencyNameTextBox.Text = Contact.oems_emergencycontact_name;
            EmergencyDescriptionTextBox.Text = Contact.oems_EmergencyContactDescription;
            EmergencyPhoneTextBox.Text = Contact.oems_emergencycontactphone;
        }

        protected void PersonalInfoUpdate_OnClick(object sender, EventArgs e)
        {
            var c = new Contact
            {
                Id = Contact.Id,
                FirstName = FirstNameTextBox.Text,
                LastName = LastNameTextBox.Text,
                oems_NonMemberOrganization = OrganizationTextBox.Text,
                Address1_Line1 = Address1TextBox.Text,
                Address1_Line2 = Address2TextBox.Text,
                Address1_City = CityTextBox.Text,
                Address1_StateOrProvince = ProvinceTextBox.Text,
                Address1_PostalCode = PostalCodeTextBox.Text,
                Address1_Country = CountryTextBox.Text,
                Telephone1 = WorkPhoneTextBox.Text,
                Telephone2 = HomePhoneTextBox.Text,
                Telephone3 = CellPhoneTextBox.Text,
                Fax = FaxTextBox.Text,
                oems_emergencycontact_name = EmergencyNameTextBox.Text,
                oems_EmergencyContactDescription = EmergencyDescriptionTextBox.Text,
                oems_emergencycontactphone = EmergencyPhoneTextBox.Text,
                mbr_FirstNation = FirstNationSelfIdentCheckBox.Checked,
                mbr_Disabled = DisabledSelfIdentCheckBox.Checked,
                mbr_GayLesbianBisexualTransgender = LgbtSelfIdentCheckBox.Checked,
                mbr_RacializedGroup = MinoritySelfIdentCheckBox.Checked,
                mbr_Metis = MetisSelfIdentCheckBox.Checked,
                mbr_Inuit = InuitSelfIdentCheckBox.Checked,
                mbr_Woman = WomanSelfIdentCheckBox.Checked
            };

            if (!string.IsNullOrEmpty(NonMemberBirthdayHiddenField.Value))
            {
                DateTime birthdate;
                DateTime.TryParseExact(NonMemberBirthdayHiddenField.Value, "M-dd-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out birthdate);
                c.BirthDate = birthdate;
            }

            XrmContext.Attach(c);
            XrmContext.UpdateObject(c);
            XrmContext.SaveChanges();

            ShowMessage(PersonInfoResultPanel, PersonalInfoResultLiteral, XrmHelper.GetSnippetValueOrDefault("My Account Self Ident Update Request Success Message", "Your personal information has been saved."), "success");
        }

        private static void ShowMessage(WebControl panel, ITextControl literal, string msg, string type)
        {
            panel.Visible = true;
            literal.Text = msg;
            panel.CssClass += string.Format(" alert-{0}", type);
        }

    }
}