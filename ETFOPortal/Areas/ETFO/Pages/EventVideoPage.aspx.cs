﻿using System;
using Adxstudio.Xrm.Web.Mvc;
using Site.Pages;
using System.Linq;
using Adxstudio.Xrm.Cms;
using Site.Areas.ETFO.Library;
using Xrm;
using Microsoft.Xrm.Portal.Web;

namespace Site.Areas.ETFO.Pages
{
    public partial class EventVideoPage : PortalPage
	{
		protected void Page_Load(object sender, EventArgs args)
		{
            //Response.Write(Portal.Entity.GetAttributeValue<Guid>("adx_parentpageid"));
            var parentEvent= ServiceContext.oems_EventSet.FirstOrDefault(ev=>ev.oems_WebPage.Id==Portal.Entity.GetAttributeValue<Guid>("adx_parentpageid"));

            // Clean
            var videoShortCuts = ServiceContext.adx_shortcutSet.Where(sc => sc.oems_eventvideo.Id == parentEvent.Id);
            VideoList.DataSource = videoShortCuts;
            VideoList.DataBind();
		}

        public string GetVideoThumb(adx_shortcut video)
        {
            if (!string.IsNullOrEmpty(video.adx_ExternalURL))
            {
                return new VideoHelper().GetVideoThumbUrl(new UrlBuilder(video.adx_ExternalURL));
            }
            return "";
        }

        public string GetUrl(adx_shortcut video)
        {
            if (!string.IsNullOrEmpty(video.adx_ExternalURL))
            {
                return new VideoHelper().GetVideoUrl(new UrlBuilder(video.adx_ExternalURL));
            }

            if (video.adx_WebFileId != null)
            {
                Adx_webfile file = ServiceContext.Adx_webfileSet.FirstOrDefault(v => v.Id == video.adx_WebFileId.Id);
                if (file != null)
                {
                    return new UrlBuilder(ServiceContext.GetUrl(file));
                }
                else
                {
                    return "#";
                }
            }
            return "#";
        }
	}
}
