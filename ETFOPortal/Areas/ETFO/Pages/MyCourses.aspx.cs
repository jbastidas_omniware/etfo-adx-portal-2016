﻿using System;
using System.Web.UI.HtmlControls;
using Site.Pages;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using System.Text;
using Site.Areas.ETFO.Library;
using Site.Areas.ETFO.Pages;
using Microsoft.Xrm.Portal;
using Microsoft.Xrm.Client;
using Xrm;
using Site.Helpers;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Label = System.Web.UI.WebControls.Label;

namespace Site.Areas.ETFO.Pages
{
    public partial class MyCourses : PortalPage
    {
        public string GridSearchID;
        public string GridSearchPlaceholder;
        public string CancelConfirmText = "";
        public string WithdrawConfirmText = "";
        public string CancelApplicationConfirmText = "";
        public string WithdrawApplicationConfirmText = "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //Contact
            Guid? contactId = null;
            if (Contact != null)
            {
                contactId = this.Contact.Id;
            }
            var allTerms = new CourseListings(null, string.Empty, contactId, string.Empty, Portal);
            var count = allTerms.MySelectCount();
            var terms =
                allTerms.MySelect("", 1000, 0)
                         .Select(trm => new { id = trm.TermId, name = trm.TermName })
                         .Distinct();

            TermDropdown.DataSource = terms;
            TermDropdown.DataTextField = "name";
            TermDropdown.DataValueField = "id";
            TermDropdown.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RedirectToLoginIfAnonymous();

            CancelConfirmText = XrmHelper.GetSnippetValueOrDefault("MyCourses/CancelButtonConfirmText", "Are you sure you want to cancel your registration for this course?");
            WithdrawConfirmText = XrmHelper.GetSnippetValueOrDefault("MyCourses/WithdrawButtonConfirmText", "Are you sure you want to withdraw your registration for this course?");
            CancelApplicationConfirmText = XrmHelper.GetSnippetValueOrDefault("MyCourses/CancelApplicationButtonConfirmText", "Are you sure you want to cancel your application for this course?");
            WithdrawApplicationConfirmText = XrmHelper.GetSnippetValueOrDefault("MyCourses/WithdrawApplicationButtonConfirmText", "Are you sure you want to withdraw your application for this course?");

            GridSearchID = "#"+GridSearchText.ClientID;
            GridSearchPlaceholder = ServiceContext.GetSnippetValueByName(Portal.Website, "MyCourses/GridSearchPlaceholder")?? "search courses";
        }

        public void MyCoursesDataSource_OnObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            if (e == null)
            {
                return;
            }

            //Filter
            var searchText = GridSearchText.Text;

            //Contact
            Guid? contactId = null;
            if (Contact != null)
            {
                contactId = this.Contact.Id;
            }

            //Term
            Guid? courseTermId = null;
            string courseTermName = string.Empty;
            if (TermDropdown.SelectedValue != "")
            {
                courseTermId = new Guid(TermDropdown.SelectedValue);
                courseTermName = TermDropdown.SelectedItem.Text;
            }

            //var courseListings = new CourseListings(courseTermId, courseTermName, contactId, searchText, Portal);
            var courseListings = new CourseListings(courseTermId, courseTermName, contactId, searchText, Portal);
            e.ObjectInstance = courseListings;
        }
        
        protected void GridSearchButton_Click(object sender, EventArgs e)
        {
            CourseListDataSource.DataBind();
        }

        protected void FilterButton_Click(object sender, EventArgs e)
        {
            CourseListDataSource.DataBind();
        }

        protected void ManageRegistration_Command(object sender, CommandEventArgs e)
        {
            //var regPage = ServiceContext.GetPageBySiteMarkerName(Website, "Registration Validation");
            var regPage = ServiceContext.GetPageBySiteMarkerName(Website, "Registration Page");
            var url = new UrlBuilder(ServiceContext.GetUrl(regPage));
            url.QueryString.Set("oems_EventRegistrationId", e.CommandArgument.ToString());
            Response.Redirect(url.PathWithQueryString);
        }

        protected bool ShowManageDelegationButton(object flag, object eventLocal)
        {
            bool? eventLocalDelegationComponentFlag = (bool?) flag;
            return eventLocal != null && eventLocalDelegationComponentFlag.HasValue && eventLocalDelegationComponentFlag.Value 
                && ((Site.Helpers.RegistrationHelper.MyEventConfiguration) Eval("EventConfig")).showManageDelegationButton;
        }

        protected bool ShowViewButton(int? eventStatus, int? registrationStatus)
        {
            return RegistrationHelper.CanViewRegistration(eventStatus, registrationStatus);
        }

        protected bool IsPaymentPendingOrPayed(Guid registrationId)
        {
            return RegistrationHelper.IsPaymentPendingOrPayed(registrationId);
        }

        protected void ManageDelegation_Command(object sender, CommandEventArgs e)
        {
            var regPage = ServiceContext.GetPageBySiteMarkerName(Website, "Manage Local Delegation");
            var url = new UrlBuilder(ServiceContext.GetUrl(regPage));
            url.QueryString.Set("oems_EventRegistrationId", e.CommandArgument.ToString());
            Response.Redirect(url.PathWithQueryString); 
        }

        protected void Cancel_Command(object sender, CommandEventArgs e)
        {
            var argument = e.CommandArgument.ToString();
            var parts = argument.Split(new[] { '~' }, StringSplitOptions.RemoveEmptyEntries);
            var eventId = new Guid(parts[0]);
            var registrationId = new Guid(parts[1]);

            //// set status to cancelled
            XrmHelper.SetStatus
            (
                oems_EventRegistration.EntityLogicalName, 
                registrationId, 
                RegistrationHelper.EventRegistration.Cancelled, 
                RegistrationHelper.State.Active
            );

            var request = RegistrationHelper.GetRegistrationWithWaitingListRequest(eventId, registrationId);

            if (request != null)
            {
                XrmHelper.SetStatus
                (
                    "oems_waitinglistrequest",
                    request.GetAttributeValue<Guid>("oems_waitinglistrequestid"),
                    RegistrationHelper.WaitingListStatus.Cancelled,
                    RegistrationHelper.State.Active
                );
            }

            Response.Redirect(Page.Request.Url.ToString(), true);
        }

        protected void Withdraw_Command(object sender, CommandEventArgs e)
        {
            //// set status to withdraw
            XrmHelper.SetStatus
                ( oems_EventRegistration.EntityLogicalName
                , new Guid(e.CommandArgument.ToString())
                , RegistrationHelper.EventRegistration.Withdrawn
                , RegistrationHelper.State.Active
                );

            Response.Redirect(Page.Request.Url.ToString(), true);
        }

        protected void MyItinerary_Command(object sender, CommandEventArgs e)
        {
            var regPage = ServiceContext.GetPageBySiteMarkerName(Website, "My Itinerary");
            var url = new UrlBuilder(ServiceContext.GetUrl(regPage));
            url.QueryString.Set("oems_EventRegistrationId", e.CommandArgument.ToString());
            Response.Redirect(url.PathWithQueryString); 
        }

        protected void AcceptWaitingListOffer_Command(object sender, CommandEventArgs e)
        {
            var argument = e.CommandArgument.ToString();
            var parts = argument.Split(new [] { '~' }, StringSplitOptions.RemoveEmptyEntries);
            var eventId = new Guid(parts[0]);
            var registrationId = new Guid(parts[1]);

            var regPage = ServiceContext.GetPageBySiteMarkerName(Website, "Registration Page");
            var url = new UrlBuilder(ServiceContext.GetUrl(regPage));
            url.QueryString.Set("oems_EventRegistrationId", registrationId.ToString());
            Response.Redirect(url.PathWithQueryString);
        }

        protected void RejectWaitingListOffer_Command(object sender, CommandEventArgs e)
        {
            var argument = e.CommandArgument.ToString();
            var parts = argument.Split(new[] { '~' }, StringSplitOptions.RemoveEmptyEntries);
            var eventId = new Guid(parts[0]);
            var registrationId = new Guid(parts[1]);

            var ctx = XrmHelper.GetContext();
            var request = RegistrationHelper.GetRegistrationWithWaitingListRequest(eventId, registrationId, ctx);
            //request.SetAttributeValue("statuscode", RegistrationHelper.WaitingListStatus.Declined);
            request.SetAttributeValue("oems_declineddate", DateTime.UtcNow);
            ctx.UpdateObject(request);
            ctx.SaveChanges();

            XrmHelper.SetStatus
            (
                "oems_waitinglistrequest",
                request.GetAttributeValue<Guid>("oems_waitinglistrequestid"),
                RegistrationHelper.WaitingListStatus.Declined,
                RegistrationHelper.State.Active
            );

            Response.Redirect(Page.Request.Url.ToString(), true);
        }

        protected void MyCoursesView_DataBound(object sender, ListViewItemEventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["oems_EventRegistrationId"]))
                {
                    var el = (EventListing)e.Item.DataItem;
                    if (el.EventRegistrationId.ToString() == Request.QueryString["oems_EventRegistrationId"])
                        MyCoursesView.EditIndex = e.Item.DataItemIndex;
                }
            }

            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var dataItem = (ListViewDataItem)e.Item;
                if (dataItem.DisplayIndex == MyCoursesView.EditIndex)
                {
                    var item = e.Item;
                    var el = (EventListing)e.Item.DataItem;
                    var manageRegistration = (Button)item.FindControl("ManageRegistration");
                    var viewRegistration = (Button)item.FindControl("ViewRegistration");
                    var manageApplication = (Button)item.FindControl("ManageApplication");
                    var viewApplication = (Button)item.FindControl("ViewApplication");

                    var billing = (Panel)item.FindControl("billingPnl");

                    var showManageRegistration = el.EventConfig.showManageRegistrationButton && el.RegistrationType == RegistrationHelper.EventRegistrationType.Attendee;
                    var showViewRegistration = !el.EventConfig.showManageRegistrationButton && this.ShowViewButton(el.EventStatus, el.EventRegistrationStatus) && el.RegistrationType == RegistrationHelper.EventRegistrationType.Attendee;
                    var showManageApplication = el.EventConfig.showManageApplicationButton && el.RegistrationType == RegistrationHelper.EventRegistrationType.Presenter;
                    var showViewApplication = !el.EventConfig.showManageApplicationButton && this.ShowViewButton(el.EventStatus, el.EventRegistrationStatus) && el.RegistrationType == RegistrationHelper.EventRegistrationType.Presenter;
                    var isPaymentPendingOrPayed = this.IsPaymentPendingOrPayed(el.EventRegistrationId.Value);

                    manageRegistration.Visible = showManageRegistration && !isPaymentPendingOrPayed;
                    viewRegistration.Visible = showViewRegistration || isPaymentPendingOrPayed;
                    manageApplication.Visible = showManageApplication;
                    viewApplication.Visible = showViewApplication;
                    billing.Visible = isPaymentPendingOrPayed;

                    //Waiting list validations
                    if (el.WaitingListComponentFlag)
                    {
                        //Is this person on the waiting list?
                        var request = RegistrationHelper.GetRegistrationWithWaitingListRequest(el.EventId.Value, el.EventRegistrationId.Value);
                        var isWaiting = request != null && (request.GetAttributeValue<OptionSetValue>("statuscode").Value == RegistrationHelper.WaitingListStatus.Offered || request.GetAttributeValue<OptionSetValue>("statuscode").Value == RegistrationHelper.WaitingListStatus.Waiting);
                        var isOffered = request != null && request.GetAttributeValue<OptionSetValue>("statuscode").Value == RegistrationHelper.WaitingListStatus.Offered;
                        var waitingListText = (Label)item.FindControl("waitingListLabel");

                        if (isWaiting)
                        {
                            waitingListText.Visible = true;
                            waitingListText.Text = XrmHelper.GetSnippetValueOrDefault("MyCourses/WaitingList/OnWaitingList", "On waiting list");
                        }

                        if (isOffered)
                        {
                            var acceptWaitingListOfferBtn = (Button)item.FindControl("AcceptWaitingListOffer");
                            var rejectWaitingListOfferBtn = (Button)item.FindControl("RejectWaitingListOffer");

                            acceptWaitingListOfferBtn.Visible = true;
                            rejectWaitingListOfferBtn.Visible = true;
                        }
                    }
                }
            }
        }

        protected void MyCoursesView_OnSorting(Object sender, ListViewSortEventArgs e)
        {
            // Check the sort direction to set the image URL accordingly.
            var cssClass = e.SortDirection == SortDirection.Ascending ? "icon-caret-down rediconcolor" : "icon-caret-up rediconcolor";

            // Check which field is being sorted
            // to set the visibility of the image controls.
            var sortRegistrationNumber = (HtmlGenericControl)MyCoursesView.FindControl("sortRegistrationNumber");
            var sortStatus = (HtmlGenericControl)MyCoursesView.FindControl("sortStatus");
            var sortStatusText = (HtmlGenericControl)MyCoursesView.FindControl("sortStatusText");
            var sortTermName = (HtmlGenericControl)MyCoursesView.FindControl("sortTermName");
            var sortTopic = (HtmlGenericControl)MyCoursesView.FindControl("sortTopic");
            var sortEventTitle = (HtmlGenericControl)MyCoursesView.FindControl("sortEventTitle");

            sortRegistrationNumber.Visible = false;
            sortStatus.Visible = false;
            sortStatusText.Visible = false;
            sortTopic.Visible = false;
            sortEventTitle.Visible = false;
            sortTermName.Visible = false;

            switch (e.SortExpression)
            {
                case "EventRegistrationNumber":
                    sortRegistrationNumber.Visible = true;
                    sortRegistrationNumber.Attributes["class"] = cssClass;
                    break;
                case "EventStatusText":
                    sortStatus.Visible = true;
                    sortStatus.Attributes["class"] = cssClass;
                    break;
                case "EventRegistrationStatusText":
                    sortStatusText.Visible = true;
                    sortStatusText.Attributes["class"] = cssClass;
                    break;
                case "Topic":
                    sortTopic.Visible = true;
                    sortTopic.Attributes["class"] = cssClass;
                    break;
                case "EventTitle":
                    sortEventTitle.Visible = true;
                    sortEventTitle.Attributes["class"] = cssClass;
                    break;
                case "sortTermName":
                    sortTermName.Visible = true;
                    sortTermName.Attributes["class"] = cssClass;
                    break;
            }
        }
    }
}