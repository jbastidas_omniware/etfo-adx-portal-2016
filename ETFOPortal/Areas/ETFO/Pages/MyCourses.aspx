﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="MyCourses.aspx.cs" Inherits="Site.Areas.ETFO.Pages.MyCourses" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Import Namespace="Site.Areas.ETFO.Library" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css" rel="stylesheet">
        .Submitted { <% Html.RenderPartialFromSetting("MyEvents/Status/Submitted/Background"); %> } 
        .Created { <% Html.RenderPartialFromSetting("MyEvents/Status/Created/Background"); %> }
        .Approved {<% Html.RenderPartialFromSetting("MyEvents/Status/Approved/Background"); %>}
        .Attended { <% Html.RenderPartialFromSetting("MyEvents/Status/Attended/Background"); %> }
        .Withdrawn{ <% Html.RenderPartialFromSetting("MyEvents/Status/Withdrawn/Background"); %> }
        .Cancelled{ <% Html.RenderPartialFromSetting("MyEvents/Status/Cancelled/Background"); %> }
        .Rejected { <% Html.RenderPartialFromSetting("MyEvents/Status/Rejected/Background"); %> }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageHeader" runat="server">
    <div class="page-header">
		<div class="grid-search">
            <asp:TextBox runat="server" ID="GridSearchText" CssClass="search-query" />
            <asp:LinkButton ID="GridSearchButton" runat="server" CssClass="btn search" OnClick="GridSearchButton_Click" ><i class="icon-search"></i></asp:LinkButton>
		</div>
        <h1>
			<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
		</h1>
	</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/css/assets/img/ajax-modal-loading.gif" AlternateText="Loading ..." ToolTip="Loading ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="portlet box light-grey">
                <div class="portlet-title">
                    <div class="row">
                        <div class="span4">
                            <asp:Label ID="TermNameLabel" runat="server" AssociatedControlID="TermDropdown">
                                <crm:Snippet ID="Snippet3" runat="server" SnippetName="MyCourses/TermName" DefaultText="Term" />
                            </asp:Label>
                            <asp:DropDownList runat="server" ID="TermDropdown" AppendDataBoundItems="True" CssClass="select" AutoPostBack="true" OnSelectedIndexChanged="FilterButton_Click">
                                <asp:ListItem Text="<%$ Snippet: DropDownListDefaultText, All %>" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <asp:ObjectDataSource runat="server" ID="CourseListDataSource" EnablePaging="true" TypeName="Site.Areas.ETFO.Library.CourseListings" 
                    OnObjectCreating="MyCoursesDataSource_OnObjectCreating" SelectMethod="MySelect" SelectCountMethod="MySelectCount" SortParameterName="sortExpression" />
    
                <asp:ListView ID="MyCoursesView" runat="server" DataSourceID="CourseListDataSource" OnItemDataBound="MyCoursesView_DataBound" OnSorting="MyCoursesView_OnSorting">
                    <LayoutTemplate>
                        <div class="portlet-body">
                            <div class="dataTables_wrapper form-inline my-events" role="grid">
                                <table class="table">
                                    <tr>
                                        <th class="event-link-header" style="text-align: left;">
                                            <asp:LinkButton runat="server" CommandArgument="EventRegistrationNumber" CommandName="Sort">
                                                <crm:Snippet runat="server" SnippetName="MyCourses/RegistrationNumberHeader" DefaultText="Registration No" />
                                                &nbsp;
                                                <i id="sortRegistrationNumber" runat="server" Visible="False" class="icon-caret-down rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
                                        <th class="event-title-header" style="text-align: left;">
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandArgument="TermName" CommandName="Sort">
                                                <crm:Snippet ID="Snippet2" runat="server" SnippetName="MyCourses/TermNameHeader" DefaultText="Term" />
                                                &nbsp;
                                                <i id="sortTermName" runat="server" Visible="False" class="icon-caret-down rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
                                        <th class="event-title-header" style="text-align: left;">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument="Topic" CommandName="Sort">
                                                <crm:Snippet ID="Snippet1" runat="server" SnippetName="MyCourses/TopicHeaderHeader" DefaultText="Topic" />
                                                &nbsp;
                                                <i id="sortTopic" runat="server" Visible="False" class="icon-caret-down rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
							            <th class="event-link-header">
                                            <asp:LinkButton ID="LinkButton16" runat="server" CommandArgument="EventTitle" CommandName="Sort">
                                                <crm:Snippet ID="Snippet3" runat="server" SnippetName="MyCourses/CourseTitleHeader" DefaultText="Course Title" />
                                                &nbsp;
                                                <i id="sortEventTitle" runat="server" Visible="True" class="icon-caret-down rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
                                        <th class="event-date-header" style="text-align: left;">
                                            <asp:LinkButton ID="LinkButton5" runat="server" CommandArgument="EventStatus" CommandName="Sort">
                                                <crm:Snippet ID="Snippet6" runat="server" SnippetName="MyCourses/EventStatusHeader" DefaultText="Event Status" />
                                                &nbsp;
                                                <i id="sortStatus" runat="server" Visible="False" class="icon-caret-down rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th>
							            <th class="event-date-header" style="text-align: left;">
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandArgument="EventRegistrationStatusText" CommandName="Sort">
                                                <crm:Snippet ID="Snippet5" runat="server" SnippetName="MyCourses/EventRegistrationStatusHeader" DefaultText="My Status" />
                                                &nbsp;
                                                <i id="sortStatusText" runat="server" Visible="False" class="icon-caret-down rediconcolor"></i>
                                            </asp:LinkButton>
                                        </th> 
                                    </tr>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
                                </table>
                            </div>
                        </div>
                        <div class="event-pager">
                        <asp:DataPager  ID="ListDataPager" runat="server" PagedControlID="MyCoursesView" PageSize="10" >
                            <Fields>
                                <asp:TemplatePagerField>
                                    <PagerTemplate>
                                        <div class="page-count">
                                        Showing <asp:Label runat="server" ID="StartIndexLabel" Text="<%# Container.TotalRowCount > 0 ? Container.StartRowIndex + 1 : 0 %>" /> to
                                        <asp:Label runat="server" ID="PageSizeLabel" Text="<%#  Container.StartRowIndex + Container.PageSize > Container.TotalRowCount ? Container.TotalRowCount :(Container.StartRowIndex + Container.PageSize) %>" />
                                        of
                                        <asp:Label runat="server" ID="PageCountLabel" Text="<%#  (Container.TotalRowCount) %>" />
                                        entries
                                    </div>
                                    </PagerTemplate>
                                </asp:TemplatePagerField>
                                <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowNextPageButton="False" PreviousPageText="<i class='icon-angle-left'></i>" />
                                <asp:NumericPagerField CurrentPageLabelCssClass="active" />
                                <asp:NextPreviousPagerField ShowLastPageButton="false" ShowPreviousPageButton="False" NextPageText="<i class='icon-angle-right'></i>" />
                            </Fields>
                        </asp:DataPager>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="EditLink" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <i class="icon-caret-right icon-large icon-spacer"></i><span class="event-detail-txt"><%#Eval("EventRegistrationNumber") %></span>
                                </asp:LinkButton>                            
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <div><%# Eval("TermName") %></div>
                                </asp:LinkButton>
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton6" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <div><%# Eval("Topic") %></div>
                                </asp:LinkButton>
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton17" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <div><%#Eval("EventTitle") %></div>
                                </asp:LinkButton>                            
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton9" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <div> <%#Eval("EventStatusText") %> </div>
                                </asp:LinkButton>
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton10" runat="server" CommandName="Edit" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                <div><div class="status-box me-<%#Eval("EventRegistrationStatusText").ToString().Replace(" ", "-") %>"><%#Eval("EventRegistrationStatusText") %></div></div>
                                </asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <tr class="active">
                            <td class="event-link-cell active">
                                <asp:LinkButton ID="EditLink" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <i class="icon-caret-down icon-large icon-spacer rediconcolor"></i><span class="event-detail-txt"><%#Eval("EventRegistrationNumber") %></span>
                                </asp:LinkButton>                            
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <div><%# Eval("TermName") %></div>
                                </asp:LinkButton>
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton9" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <div><%# Eval("Topic") %></div>
                                </asp:LinkButton>
                            </td>
                            <td class="event-link-cell">
                                <asp:LinkButton ID="LinkButton6" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                    <div><%#Eval("EventTitle") %></div>
                                </asp:LinkButton>                            
                            </td>
                            <td class="event-link-cell active">
                                <asp:LinkButton ID="LinkButton8" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                   <div class="event-link-cell"><%#Eval("EventStatusText") %></div>
                                </asp:LinkButton>                            
                            </td>
                            <td class="event-link-cell active">
                                <asp:LinkButton ID="LinkButton13" runat="server" CommandName="Cancel" CausesValidation="false" CommandArgument='<%# Eval("EventId") %>' CssClass="event-detail-btn">
                                <div class="event-link-cell"><div class="status-box me-<%#Eval("EventRegistrationStatusText").ToString().Replace(" ", "-") %>"><%#Eval("EventRegistrationStatusText") %></div></div>
                                </asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" class="details-row">
                                <div class="details">
                                    <div class="row">
                                        <div class="details-heading">
                                            <crm:Snippet ID="Snippet11" runat="server" SnippetName="MyCourses/CourseCode" DefaultText="Course Code" />
                                        </div>
                                        <div class="details-content long" style="width: 84%;"><a href="<%#Eval("EventUrl") %>"><%#Eval("CourseCode") %></a></div>
                                    </div>                                 
                                    <div class="row">
                                        <div class="details-heading">
                                            <crm:Snippet ID="Snippet12" runat="server" SnippetName="MyCourses/CourseTitle" DefaultText="Course" />
                                        </div>
                                        <div class="details-content long" ><a href="<%#Eval("EventUrl") %>"><%#Eval("EventTitle") %></a></div>
                                        <div class="details-heading long">
                                            <asp:Label ID="Label5" runat="server" Text="Rejection Reason" Visible='<%# !String.IsNullOrEmpty((string)Eval("EventRejectionReasonText")) || !String.IsNullOrEmpty((string)Eval("EventRejectionReasonDetails")) %>'></asp:Label>
                                        </div>
                                        <div class="details-content long" style="width: 250px;">
                                            <%#Eval("EventRejectionReasonText") %>
                                            <br/>
                                            <%#Eval("EventRejectionReasonDetails") %>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="details-heading">
                                            <crm:Snippet ID="Snippet4" runat="server" SnippetName="MyCourses/TermName" DefaultText="Term" />
                                        </div>
                                        <div class="details-content long" style="width: 84%;"><%#Eval("TermName") %></div>
                                    </div>     
                                    <div class="row">
                                        <div class="details-heading">
                                            <crm:Snippet ID="Snippet13" runat="server" SnippetName="MyCourses/Instructor" DefaultText="Instructor" />
                                        </div>
                                        <div class="details-content long" style="width: 84%;"><%#Eval("Instructor") %></div>
                                    </div>
                                    <div class="row">
                                        <div class="details-heading">
                                            <crm:Snippet ID="Snippet14" runat="server" SnippetName="MyCourses/Topic" DefaultText="Topic" />
                                        </div>
                                        <div class="details-content long" style="width: 84%;"><%#Eval("Topic") %></div>
                                    </div>
                                    <div class="row">
                                        <div class="details-heading">
                                            <crm:Snippet ID="Snippet17" runat="server" SnippetName="MyCourses/Location" DefaultText="Location" />
                                        </div>
                                        <div class="details-content long" style="width: 84%;"><%#Eval("Location") %></div>
                                    </div>
                                    <div class="row">
                                        <div class="details-heading">
                                            <crm:Snippet ID="Snippet8" runat="server" SnippetName="MyCourses/TargetGrades" DefaultText="Grade Level" />
                                        </div>
                                        <div class="details-content long" style="width:65%"><%#Eval("TargetGrades") %></div>
                                    </div>
                                    <div class="row" style="margin-top: 10px">
                                        <div class="row" style="width: 50%; float: left;">
                                            <div class="details-heading">
                                                <crm:Snippet ID="Snippet10" runat="server" SnippetName="MyCourses/DatesOffered" DefaultText="Dates Offered" />
                                            </div>
                                            <div class="details-content long" style="width:50%">
                                                <crm:DateTimeLiteral ID="DateTimeLiteral5" runat="server" Format="MMMM dd, yyyy" Value='<%# Eval("InitialOferedDate") %>' OutputTimeZoneLabel="false" />
                                                -
                                                <crm:DateTimeLiteral ID="DateTimeLiteral6" runat="server" Format="MMMM dd, yyyy" Value='<%# Eval("FinalOferedDate") %>' OutputTimeZoneLabel="false" />
                                            </div>
                                        </div>
                                        <div class="row" style="width: 50%; float: left;">
                                            <div class="details-heading">
                                                <crm:Snippet ID="Snippet15" runat="server" SnippetName="MyCourses/CourseRunsDailyFrom" DefaultText="Daily From" />
                                            </div>
                                            <div class="details-content long">
                                                <%# Eval("InitialDailyTime") %> - <%# Eval("FinalDailyTime") %>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 10px">
                                        <div style=" <%# (bool)Eval("DisplayExecutiveStaffOwner") ? "" : "display:none;" %>">
                                            <div class="details-heading long">
                                                <asp:Label ID="Label2" runat="server" Text="<%$ Snippet:MyCourses/ExecutiveOfficerHeader,Executive Officer %>"></asp:Label>
                                            </div>
                                            <div class="details-content"><%# EventListings.getExecutiveStaff((Guid)Eval("EventId")) %></div>
                                        </div>
                                        <div style="<%# (bool)Eval("DisplaySupportStaffOwner") ? "" : "display:none;" %>">
                                            <div class="details-heading">
                                                <asp:Label ID="Label3" runat="server" Text="<%$ Snippet:MyCourses/SupportStaffHeader,Support Staff %>"></asp:Label>
                                            </div>
                                            <div class="details-content"><%# EventListings.getSupportStaff((Guid)Eval("EventId")) %></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <%# (int)Eval("RegistrationType") == RegistrationHelper.EventRegistrationType.Attendee ? EventListings.GetRegistrationPeriods((Guid)Eval("EventId"), (int)Eval("EventStatus")) : EventListings.GetEventApplicationSchedules((Guid)Eval("EventId"), (int)Eval("EventStatus")) %>
                                        <br />
                                    </div>
                                    <div class="row registration-period">
                                        <div class="me-actions">
                                            <asp:Button ID="ManageRegistration" runat="server" Text="<%$ Snippet: MyCourses/ManageRegistrationButtonText, Modify Registration %>" CommandArgument='<%# Eval("EventRegistrationId") %>' OnCommand="ManageRegistration_Command" CssClass="btn event-btn orange-btn"></asp:Button>
                                            <asp:Button ID="ViewRegistration" runat="server" Text="<%$ Snippet: MyCourses/ViewRegistrationButtonText, View Registration %>" CommandArgument='<%# Eval("EventRegistrationId") %>' OnCommand="ManageRegistration_Command" CssClass="btn event-btn orange-btn"></asp:Button>
                                            <asp:Button ID="AcceptWaitingListOffer" runat="server" Visible="False" Text="<%$ Snippet: MyCourses/WaitingList/AcceptWaitingListOffer, Accept Waiting List Offer %>" CommandArgument='<%# Eval("EventId") + "~" + Eval("EventRegistrationId") %>' OnCommand="AcceptWaitingListOffer_Command" CssClass="btn event-btn orange-btn"></asp:Button>
                                            <asp:Button ID="RejectWaitingListOffer" runat="server" Visible="False" Text="<%$ Snippet: MyCourses/WaitingList/RejectWaitingListOffer, Reject Waiting List Offer %>" CommandArgument='<%# Eval("EventId") + "~" + Eval("EventRegistrationId") %>' OnCommand="RejectWaitingListOffer_Command" CssClass="btn event-btn orange-btn"></asp:Button>
                                            <asp:Button ID="ManageApplication" runat="server" Text="<%$ Snippet: MyCourses/ManageApplicationButtonText, Modify Application %>" CommandArgument='<%# Eval("EventRegistrationId") %>' OnCommand="ManageRegistration_Command" CssClass="btn event-btn orange-btn"></asp:Button>
                                            <asp:Button ID="ViewApplication" runat="server" Text="<%$ Snippet: MyCourses/ViewApplicationButtonText, View Application %>" CommandArgument='<%# Eval("EventRegistrationId") %>' OnCommand="ManageRegistration_Command" CssClass="btn event-btn orange-btn"></asp:Button>
                                            <asp:Button ID="ManageDelegation" runat="server" Text="<%$ Snippet: MyCourses/ManageLocalDelegationButtonText, Manage Local Delegation %>" CommandArgument='<%# Eval("EventRegistrationId") %>' OnCommand="ManageDelegation_Command" CssClass="btn event-btn navy-btn" Visible='<%# ShowManageDelegationButton(Eval("EventLocalDelegationComponentFlag"), Eval("EventLocal")) %>'></asp:Button>
                                            <%-- <asp:Button ID="ManageDelegationTest" runat="server" Text="<%$ Snippet: MyCourses/ManageLocalDelegationButtonText, Manage Local Delegation %>" CommandArgument='<%# Eval("EventRegistrationId") %>' OnCommand="ManageDelegation_Command" CssClass="btn event-btn navy-btn" Visible='true'></asp:Button>--%>
                                            <asp:Button ID="CancelRegistration" runat="server" Text="<%$ Snippet: MyCourses/CancelButtonText, Cancel Registration %>" CommandArgument='<%# Eval("EventId") + "~" + Eval("EventRegistrationId") %>' OnCommand="Cancel_Command" CssClass="btn event-btn blue-btn" Visible='<%# ((RegistrationHelper.MyEventConfiguration)Eval("EventConfig")).showCancelRegistrationButton %>' OnClientClick='<%# "javascript:return showConfirmationWindow(\"" + CancelConfirmText + "\", this.name);" %>'></asp:Button>
                                            <asp:Button ID="CancelApplication" runat="server" Text="<%$ Snippet: MyCourses/CancelApplicationButtonText, Cancel Application %>" CommandArgument='<%# Eval("EventRegistrationId") %>' OnCommand="Cancel_Command" CssClass="btn event-btn blue-btn" Visible='<%# ((RegistrationHelper.MyEventConfiguration)Eval("EventConfig")).showCancelApplicationButton %>' OnClientClick='<%# "javascript:return showConfirmationWindow(\"" + CancelApplicationConfirmText + "\", this.name);" %>'></asp:Button>
                                            <asp:Button ID="WithdrawRegistration" runat="server" Text="<%$ Snippet: MyCourses/WithdrawButtonText, Withdraw Registration %>" CommandArgument='<%# Eval("EventRegistrationId") %>' OnCommand="Withdraw_Command" CssClass="btn event-btn blue-btn" Visible='<%# ((RegistrationHelper.MyEventConfiguration)Eval("EventConfig")).showWithdrawRegistrationButton %>' OnClientClick='<%# "javascript:return showConfirmationWindow(\"" + WithdrawConfirmText + "\", this.name);" %>'></asp:Button>
                                            <asp:Button ID="WithdrawApplication" runat="server" Text="<%$ Snippet: MyCourses/WithdrawApplicationButtonText, Withdraw Application %>" CommandArgument='<%# Eval("EventRegistrationId") %>' OnCommand="Withdraw_Command" CssClass="btn event-btn blue-btn" Visible='<%# ((RegistrationHelper.MyEventConfiguration)Eval("EventConfig")).showWithdrawApplicationButton %>' OnClientClick='<%# "javascript:return showConfirmationWindow(\"" + WithdrawApplicationConfirmText + "\", this.name);" %>'></asp:Button>
                                            <asp:Button ID="MyItinerary" runat="server" Text="<%$ Snippet: MyCourses/MyItineraryButtonText, My Itinerary %>" CommandArgument='<%# Eval("EventRegistrationId") %>' OnCommand="MyItinerary_Command" CssClass="btn event-btn yellow-btn" Visible='<%# ((RegistrationHelper.MyEventConfiguration)Eval("EventConfig")).showMyItineraryButton %>'></asp:Button>
                                        </div>
                                        <asp:Label ID="waitingListLabel" runat="server" Visible="False" style="font-weight: bold; display: block;"></asp:Label>
                                    </div>
                                    <asp:Panel ID="billingPnl" runat="server">
                                        <div class="row registration-period">
                                            <div class="span7">
                                                Your registration was successfully submitted with payment. Please contact ETFO to make any changes
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
                    </EditItemTemplate>
                    <EmptyDataTemplate>
                        <b><crm:Snippet runat="server" SnippetName="MyCourses/NoResultsFound" DefaultText="No courses found" /></b>
                    </EmptyDataTemplate>

                </asp:ListView>

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <div class="modal fade" style="" id="confirmationWindow" tabindex="-1" role="dialog" aria-labelledby="ConformationWindowlLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body" id="confirmationWindowBody"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary red" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary" id="continueBtn">Continue</button>
          </div>
        </div>
      </div>
    </div>
    
    <script>
        function showConfirmationWindow (message, uid)
        {
            $("#confirmationWindowBody").html(message);

            $("#continueBtn").unbind("click");
            $("#continueBtn").click
            (
                function ()
                {
                    __doPostBack(uid, '');
                    $("#continueBtn").unbind("click");
                    $("#confirmationWindow").modal('hide');
                }
            );
            $("#confirmationWindow").modal('show');
            return false;
        }
    </script>

</asp:Content>


<asp:Content ID="Content13" ContentPlaceHolderID="SidebarAbove" runat="server">
<%--    <ul class="etfo-event-nav pre-nav nav nav-tabs nav-stacked">
        <li class="item-purple">
            <crm:CrmHyperLink runat="server" ID="HyperLinkUpcoming" Text="<%$ Snippet: Upcoming Events Label, Upcoming Events %>" SiteMarkerName="Upcoming Events"></crm:CrmHyperLink>
        </li>
        <li class="item-pink">
            <crm:CrmHyperLink runat="server" ID="HyperLinkHistorical" Text="<%$ Snippet: Historical Events Label, Historical Events %>" SiteMarkerName="Historical Events"></crm:CrmHyperLink>
        </li>
    </ul>--%>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="SidebarBottom" runat="server">
    <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/ETFOnews" data-widget-id="392320152093458432">Tweets by @ETFOnews</a>
    <script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");</script>
</asp:Content>

<asp:Content ID="Content11" ContentPlaceHolderID="SidebarBelow" runat="server">
    <adx:AdPlacement runat="server" PlacementName="Sidebar Bottom" CssClass="ad" />
</asp:Content>

<%--<asp:Content ID="Content5" ContentPlaceHolderID="EntityControls" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentBottom" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="SidebarContent" runat="server">
</asp:Content>

<asp:Content ID="Content9" ContentPlaceHolderID="SidebarTop" runat="server">
</asp:Content>--%>

<asp:Content ID="Content12" ContentPlaceHolderID="Scripts" runat="server">
    <script type="text/javascript">
        var searchid = "<%=GridSearchID%>";
        var searchPlaceHolder = "<%=GridSearchPlaceholder %>";
        $(searchid).attr('placeholder', searchPlaceHolder);
    </script>
</asp:Content>
