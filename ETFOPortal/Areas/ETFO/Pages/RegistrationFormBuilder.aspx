﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegistrationFormBuilder.aspx.cs" Inherits="Site.Areas.ETFO.Pages.RegistrationFormBuilder" %>

<script src="js/jquery-1.11.0.min.js"></script>
<script src="js/knockout-3.0.0.js"></script>
<script src="js/knockout.mapping-latest.js"></script>
<script src="js/app.js"></script>
<link href="../../../css/assets/css/style-metronic.css" rel="stylesheet" />
<link href="../../../css/bootstrap.min.css" rel="stylesheet" />
<link href="css/portlet_metronic.css" rel="stylesheet" />
<style>
    #controlBar {
        /*border: 1px solid red;*/
        float: left;
        /*height: 200px;*/
        width: 300px;
    }

    #formTempalteBar {
        /*border: 1px solid red;*/
        float: left;
        /*height: 200px;*/
        width: 700px;
        margin-left: 20px;
    }

    #propertyBar {
        float: left;
        width: 200px;
        margin-left: 20px;
    }

    #formTitle {
        /*height: 34px;
        width: 200px;*/
    }

    .control_label {
        background-color: #35aa47;
        /*background-color: #555555;*/
        color: white;
        font-size: 18px;
        margin: 5px 5px 5px 5px;
        text-align: center;
        padding: 10px;
        font-size: 17px;
        cursor: pointer;
    }
    .control_label:hover {
        -ms-opacity: 0.8;
        opacity: 0.8
    }

    .btn {
        cursor: pointer;
    }
    #property_inside_label {
        font-size: 13px;
        text-align: center;
        min-height: 100px;
        line-height: 90px; 
    }
</style>

<div id="formTemplate_style">

</div>


<div id="formBuilder_content" >
    
    <div id="formSettings" style="margin-top: 20px;margin-bottom: 20px;">
         <div><label class="" style="font-size: 18px;margin-bottom: 3px;" >Form Name:</label></div>
         <input type="text" class="" data-bind="value: formTemplateName" id="formName" style="height: 34px;width: 600px;">
        <div><label class="" style="font-size: 18px;margin-bottom: 3px;" >Form Title:</label></div>
         <input type="text" class="" data-bind="value: formTemplateTitle, valueUpdate: 'afterkeydown'" id="formTitle" style="height: 34px;width: 600px;">
         <button type="button" class="btn btn-primary" data-bind='click: saveFormTemplateModel'>Save</button>
    </div>

    
   <div id="controlBar">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <span style="color: white">Controls</span>
                </div>
             <%--   <div class="tools">
                  <a href="javascript:;" class="collapse"></a>
                </div>--%>
            </div>
            <div class="portlet-body">
                <div data-bind="foreach: controlList">
                    <div class="control_label" data-bind="text: Name, click: function () { self.addControltoFormTemplate(ID); }"></div>
                </div>
            </div>
        </div>
    </div> 
    
     <div id="formTempalteBar">
         <div class="portlet box green">
             <div class="portlet-title">
                  <div class="caption">
                      <span style="color: white" data-bind="text: formTemplateTitle" ></span>
                  </div>
         <%--           <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>--%>
              </div>
         <div class="portlet-body" id="formTemplate_body" style="min-height:100px;"></div>
         </div>
    </div>
    
    
       <div id="propertyBar">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption">
                    <span style="color: white">Properties</span>
                </div>
             <%--   <div class="tools" >
                    <a href="javascript:;" class="collapse"></a>
                </div>--%>
            </div>
            <div class="portlet-body" style="min-height:100px;">
                <div id="property_inside_label">Select Control</div>
            </div>
        </div>
    </div> 
    
    

</div>
     
            <script>
                var data1 = { "Name": "Joe", "Title": "Bloggs" };
            
                $(function () {
                    var element = $("#formBuilder_content")[0];
                    ko.applyBindings(MyViewModel(), element);
                });

                function MyViewModel() {
                    var self = this;
                    self.controlList = $.parseJSON(getControlList());
                    //console.log(self.controlList);
                    //self.formTemplate = ko.mapping.fromJSON(getDefaultFormTemplate());

                    self.formTemplate = $.parseJSON(getDefaultFormTemplate());
                    self.formTemplateID = ko.observable(self.formTemplate.ID);
                    self.formTemplateName = ko.observable(self.formTemplate.Name);
                    self.formTemplateTitle = ko.observable(self.formTemplate.Title);
                    self.formTemplateControls = ko.observableArray(self.formTemplate.Controls);


                    self.saveFormTemplateModel = function () {
                        //var val = ko.mapping.toJSON(self.formTemplate);
                        //console.log(val);
                        //saveFormTemplate(val);

                    };

                    self.addControltoFormTemplate = function (id) {
                        var tempObj = self.getControlByID(id);
                        if (tempObj != null) {
                            console.log(tempObj);
                            tempObj.UniqueID = getRandomGUID();
                            self.formTemplateControls.push(tempObj);                            
                            console.log(self.formTemplateControls());
                            BuildControl(tempObj);
                        }

                    };

                    self.getControlByID = function (id) {
                        for (var i = 0; i < self.controlList.length; i++) {
                            if (self.controlList[i].ID === id) {                              
                                return self.controlList[i];                                
                            }
                        }
                        return null;
                    };


                    self.BuildControl = function(control) {                       
                        switch(control.ID)
                        {
                            case 1000:
                                
                                break;
                            case 1001:
                                $("<style id='" + control.UniqueID + "'>#" + control.UniqueID + " " +
                                    "{ font-size: " + control.FontSize + "px; " +
                                    "font-family: " + control.FontFamily + "; " +
                                    "Color: " + control.Color + "; " +
                                    "Width: " + control.Width + "; " +
                                    "font-weight: " + control.FontWeight + ";  } " +
                                    "</style>").appendTo("#formTemplate_style");
                                $("<div><input type='text' id=" + control.UniqueID + "></div>").appendTo("#formTemplate_body");
                                break;
                            case 1002:

                                break;
                            case 1003:

                                break;
                            case 1004:

                                break;
                            case 1005:
                                $("<style id='" + control.UniqueID + "'>#" + control.UniqueID + " { font-size: " + control.FontSize + "px; font-family: " + control.FontFamily + "; font-weight: " + control.FontWeight + ";  } </style>").appendTo("#formTemplate_style");
                                $("<div><label id=" + control.UniqueID + ">" + control.Value + "</label></div>").appendTo("#formTemplate_body");
                                break;
                            default:
                                break;
                        }

                        self.addControlCSS = function (control) {
                            // formTemplate_style

                            
                        };

                    };
                }








                function getControlList() {
                    var result = null;
                    $.ajax({
                        url: "/api/RegistrationForm/GetControlList",
                        type: "GET",
                        dataType: "json",
                        async: false,
                        statusCode: {
                            400: function () {
                                alert("Bad Request");
                            },
                            200: function (request) {
                                result = request;
                            }
                        },
                    });
                    return result;
                }

                function getDefaultFormTemplate() {
                    var result = null;
                    $.ajax({
                        url: "/api/RegistrationForm/GetDefaultFormTemplate",
                        type: "GET",
                        dataType: "json",
                        async: false,
                        statusCode: {
                            400: function () {
                                alert("Bad Request");
                            },
                            200: function (request) {
                                result = request;
                            }
                        },
                    });
                    return result;
                }

                function saveFormTemplate(val) {
                    $.ajax({
                        url: "/api/RegistrationForm/SaveFormTemplate",
                        type: "POST",
                        data: val,
                        contentType: "application/json",
                        statusCode: {
                            400: function () {
                                alert("Bad Request");
                            },
                            200: function (request) {
                                alert("ok");
                            }
                        },
                    });
                }

                function getRandomGUID() {
                    var result = null;
                    $.ajax({
                        url: "/api/RegistrationForm/GetRandomGUID",
                        type: "GET",
                        dataType: "json",
                        async: false,
                        statusCode: {
                            400: function () {
                                alert("Bad Request");
                            },
                            200: function (request) {
                                result = request;
                            }
                        },
                    });
                    return result;
                }

            </script>
