﻿using System;
using Adxstudio.Xrm.Cms;
using Site.Pages;

namespace Site.Areas.ETFO.Pages
{
    public partial class ManageLocalDelegation : PortalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RedirectToLoginIfAnonymous();

            if (!String.IsNullOrWhiteSpace(Request.QueryString["oems_EventRegistrationId"]))
            {
                EventRegistrationId.Value = Request.QueryString["oems_EventRegistrationId"];
            }
            else
            {
                EventRegistrationId.Value = null;
                return;
            }
        }

        public string GetAlertIcon()
        {
            return ServiceContext.GetSiteSettingValueByName(Website, "ManageLocalDelegation/AlertIcon");
        }
        public string GetSuccessIcon()
        {
            return ServiceContext.GetSiteSettingValueByName(Website, "ManageLocalDelegation/SuccessIcon");
        }
    }
}