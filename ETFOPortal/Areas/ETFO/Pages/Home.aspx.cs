﻿using Adxstudio.Xrm.Blogs;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Portal;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Site.Areas.ETFO.DataAdapters;
using Site.Helpers;
using Site.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Site.Areas.ETFO.Library;
using PortalContextDataAdapterDependencies = Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies;

namespace Site.Areas.ETFO.Pages
{
    public partial class Home : PortalPage
    {
        private readonly Lazy<IPortalContext> _portal = new Lazy<IPortalContext>(() => PortalCrmConfigurationManager.CreatePortalContext());

        private readonly Xrm.XrmServiceContext _context = XrmHelper.GetContext();

        public String HeaderText
        {
            get
            {
                if (Contact != null)
                    return "Welcome " + Contact.FullName;
                else
                    return null;
            }
        }			

        protected Entity CarouselBlog
        {
            get
            {
                return _context.adx_blogSet.FirstOrDefault(b => b.adx_websiteid.Id == _portal.Value.Website.Id && b.adx_parentpageid.Id == _portal.Value.Entity.Id && b.oems_type == (int)Enums.BlogType.Carousel);
            }
        }

        protected Entity AnnouncementBlog
        {
            get
            {
                return _context.adx_blogSet.FirstOrDefault(b => b.adx_websiteid.Id == _portal.Value.Website.Id && b.adx_parentpageid.Id == _portal.Value.Entity.Id && b.oems_type == (int)Enums.BlogType.Annoucement);
            }
        }

        protected Entity DefaultBlog
        {
            get
            {
                return _context.adx_blogSet.FirstOrDefault(b => b.adx_websiteid.Id == _portal.Value.Website.Id && b.adx_parentpageid.Id == _portal.Value.Entity.Id && b.oems_type == (int)Enums.BlogType.Blog);
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            CarouselPanel.Visible = (CarouselBlog != null);
            AnnouncementPanel.Visible = (AnnouncementBlog != null);
            BlogPanel.Visible = (DefaultBlog != null);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var webPage = _context.Adx_webpageSet.FirstOrDefault(wp => wp.Adx_webpageId == _portal.Value.Entity.Id);

            if (webPage == null) return;

            if (new SiteMarkerHelper().IsSiteMarker(webPage, "My ETFO"))
            {
                RedirectToLoginIfAnonymous();

                EventListButtons.Visible = false;
            }

            var upcomingPage = ServiceContext.GetPageBySiteMarkerName(Website, "Upcoming Events");
            var upcomingUrl = new UrlBuilder(ServiceContext.GetUrl(upcomingPage));
            HyperLinkUpcoming.NavigateUrl = upcomingUrl;

            var historicalPage = ServiceContext.GetPageBySiteMarkerName(Website, "Historical Events");
            var historicalUrl = new UrlBuilder(ServiceContext.GetUrl(historicalPage));
            HyperLinkHistorical.NavigateUrl = historicalUrl;

            if (new SiteMarkerHelper().IsSiteMarker(webPage, "My ETFO"))
            {
                ProfileView.Visible = true;
                WelcomeHeader.Visible = true;

                GetAccountDetails();
            }
            else
            {
                ProfileView.Visible = false;
                WelcomeHeader.Visible = false;
            }
        }

        protected void GetAccountDetails()
        {
            if (Contact != null)
            {
                var portalUser = Contact;

                ((System.Web.UI.WebControls.Label)ProfileView.FindControl("ProfileName")).Text = portalUser.FullName;
                ((System.Web.UI.WebControls.Label)ProfileView.FindControl("ETFOID")).Text = portalUser.mbr_ETFOID;

                System.Web.UI.WebControls.Label addressLabel = (System.Web.UI.WebControls.Label)ProfileView.FindControl("ProfileAddress");
                addressLabel.Text = portalUser.Address1_Line1 + "<br/>" + portalUser.Address1_City + ", " + portalUser.Address1_StateOrProvince + "<br/>" + portalUser.Address1_PostalCode;

                System.Web.UI.WebControls.Label phoneLabel = ((System.Web.UI.WebControls.Label)ProfileView.FindControl("ProfilePhone"));
                if (!string.IsNullOrEmpty(portalUser.Telephone1))
                    phoneLabel.Text = "(H) " + portalUser.Telephone1 + "<br/>";
                if (!string.IsNullOrEmpty(portalUser.MobilePhone))
                    phoneLabel.Text += "(C) " + portalUser.MobilePhone;

                ((System.Web.UI.WebControls.Label)ProfileView.FindControl("ProfileEmail")).Text = portalUser.EMailAddress1;
            }
        }

        protected void CreateCarouselDataAdapter(object sender, ObjectDataSourceEventArgs args)
        {
            args.ObjectInstance = new BlogDataAdapter(CarouselBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
        }

        protected void CreateCarouselAggregateDataAdapter(object sender, ObjectDataSourceEventArgs args)
        {
            var adapter = new ETFOBlogDataAdapter(CarouselBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
            adapter.Type = Enums.BlogType.Carousel;
            args.ObjectInstance = adapter;
        }

        protected void CreateAnnouncementAggregateDataAdapter(object sender, ObjectDataSourceEventArgs args)
        {
            var adapter = new ETFOBlogDataAdapter(AnnouncementBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
            adapter.Type = Enums.BlogType.Annoucement;
            args.ObjectInstance = adapter;
        }

        protected void CreateBlogAggregateDataAdapter(object sender, ObjectDataSourceEventArgs args)
        {
            var adapter = new ETFOBlogDataAdapter(DefaultBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
            adapter.Type = Enums.BlogType.Blog;
            args.ObjectInstance = adapter;
        }

        protected string GetThumbnailUrl(object entityObject)
        {
            var entity = entityObject as Entity;

            if (entity == null) return null;

            var blogItem = _context.adx_blogpostSet.FirstOrDefault(bp => bp.adx_blogpostId == entity.Id);

            if (blogItem == null || blogItem.oems_Image == null)
            {
                return @"http://placehold.it/1280x720";
            }

            var webfile = ServiceContext.CreateQuery<Xrm.Adx_webfile>().FirstOrDefault(file => file.Id == blogItem.oems_Image.Id);

            return webfile == null ? @"http://placehold.it/1280x720" : new UrlBuilder(ServiceContext.GetUrl(webfile)).Path;
        }

        public void MyEventsDataSource_OnObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            if (e == null)
            {
                return;
            }

            Guid? contactId = null;
            if (Contact != null)
            {
                contactId = this.Contact.Id;
            }
            var eventListings = new EventListings(null, "", false, contactId, Portal);

            e.ObjectInstance = eventListings;
        }
    }
}