﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="MyAccountMember.aspx.cs" Inherits="Site.Areas.ETFO.Pages.MyAccountMember" %>
<%@ Register Src="~/Areas/ETFO/Controls/MyAccountUserCreds.ascx" TagPrefix="uc1" TagName="MyAccountUserCreds" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <link rel="stylesheet" href="~/css/bootstrap-switch.min.css" />
    <link rel="stylesheet" href="~/css/bootstrap-datepicker3.min.css" />
    <script type="text/javascript" src="~/js/ETFO/JQGrid_PA.js"></script>
    <style>
        #gbox_pa_Component{
            width: 865px;
        }
    </style>
    
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server"></asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ContentBottom" runat="server" ViewStateMode="Enabled">
    <ul class="nav nav-tabs" id="myAccountTabGroup">
        <li class="active tab-link" data-hash="#tab1">
            <a href="#tab1"><crm:Snippet runat="server" DefaultText="Member Information" SnippetName="My Account Tab 1" /></a>
        </li>
        <li class="tab-link" data-hash="#tab2">
            <a href="#tab2"><crm:Snippet runat="server" DefaultText="User Profile" SnippetName="My Account Tab 2" /></a>
        </li>
        <li class="tab-link" data-hash="#tab3">
            <a href="#tab3"><crm:Snippet runat="server" DefaultText="User Credentials" SnippetName="My Account Tab 3" /></a>
        </li>
        <li class="tab-link" data-hash="#tab4">
            <a href="#tab4"><crm:Snippet runat="server" DefaultText="Document Locker" SnippetName="My Account Tab 4" /></a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <asp:UpdatePanel ID="MemberInformationUpdateResultPanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="MemberInformationResultPanel" CssClass="alert" Visible="false">
                        <asp:Literal runat="server" ID="MemberInformationResultLiteral" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div class="form-horizontal">
                <!-- Personal Info -->
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Panel runat="server" ID="ContactDetails" CssClass="my-account-section">
                            <h4><crm:Snippet runat="server" DefaultText="Personal Info" SnippetName="My Account Personal Info Title" /></h4>
                            <div class="my-account-section-content">
                                <div class="form-group">
                                    <asp:Label runat="server" ID="AccountFirstNameLabel" CssClass="control-label required" AssociatedControlID="FirstNameTextBox" Text="<%$ Snippet: My Account First Name Label, First Name %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="FirstNameTextBox" CssClass="input-large"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="FirstNameValidator" runat="server" ControlToValidate="FirstNameTextBox" 
                                            ValidationGroup="MyAccountPersonalInformation"  Display="Dynamic" CssClass="help-inline error" 
                                            Text="<%$ Snippet: My Account Required Field, This is a required field. %>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="AccountLastNameLabel" CssClass="control-label required" AssociatedControlID="LastNameTextBox" Text="<%$ Snippet: My Account Last Name Label, Last Name %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="LastNameTextBox" CssClass="input-large"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="LastNameValidator" runat="server" ControlToValidate="LastNameTextBox" ValidationGroup="MyAccountPersonalInformation" Display="Dynamic" CssClass="help-inline error" Text="<%$ Snippet: My Account Required Field, This is a required field. %>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="AccountMemberIdLabel" CssClass="control-label required" AssociatedControlID="MemberIdTextBox" Text="<%$ Snippet: My Account Member Id Label, Member ID %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="MemberIdTextBox" CssClass="input-large" Enabled="False"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="AccountEmailLabel" CssClass="control-label required" AssociatedControlID="EmailTextBox" Text="<%$ Snippet: My Account Email Label, Email %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="EmailTextBox" CssClass="input-large"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="EmailValidator" runat="server" ControlToValidate="EmailTextBox" ValidationGroup="MyAccountPersonalInformation" Display="Dynamic" CssClass="help-inline error" Text="<%$ Snippet: My Account Required Field, This is a required field. %>" />
                                        <asp:RegularExpressionValidator ID="EmailValidatorRegEx" runat="server" ControlToValidate="EmailTextBox" ValidationGroup="MyAccountPersonalInformation" Display="Dynamic" CssClass="help-inline error" Text="<%$ Snippet: My Account Email Field Invalid, Email is invalid. %>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                            </div>
                            <p class="my-account-section-footer">
                                <asp:Button runat="server" CausesValidation="True" ValidationGroup="MyAccountPersonalInformation" OnClientClick="ValidatePersonalInformationRequest()" ID="PersonalInformationUpdate" CssClass="btn btn-primary btn-small" OnClick="PersonalInformationUpdate_OnClick" Text="<%$ Snippet: My Account Personal Information Update Button, Update %>" />
                            </p>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <!-- School Board / School / Local -->
                <asp:HiddenField ID="InitialId" runat="server" />
                <asp:HiddenField ID="InitialLocal" runat="server" />
                <asp:HiddenField ID="InitialSchool" runat="server" />
                <asp:HiddenField ID="InitialSchoolBoard" runat="server" />
                <asp:HiddenField ID="InitialMemberType" runat="server" />
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:Panel runat="server" ID="PreferredPanel" CssClass="my-account-section">
                            <h4><crm:Snippet runat="server" DefaultText="Preferred School Board / School / Local" SnippetName="My Account Preferred Board School Local Title" /></h4>

                            <div class="my-account-section-content">
                                <crm:Snippet runat="server" ID="NoPreferredSchoolBoardLocalSnippet" DefaultText="No School Information Provided." SnippetName="My Account No Preferred Board School Local Message" Visible="false" />
                                <div class="form-group">
                                    <div class="controls col-sm-8">
                                        <asp:GridView ID="SchoolBoardList" runat="server" AutoGenerateColumns="false" AllowPaging="false" CssClass="SchoolBoardList"
                                            BackColor="White" BorderColor="#0a0a0a" BorderStyle="None" BorderWidth="1px" CellPadding="4">
                                            <HeaderStyle BackColor="#428bca" Font-Bold="True" ForeColor="#FFFFCC" HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:RadioButton ID="rbSchoolBoardId" GroupName="SchoolBoards" Checked='<%#Eval("Default")%>' runat="server" onclick="checkRadioBtn(this)" />
                                                        <asp:HiddenField ID="Id" runat="server" Value='<%#Eval("Id")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField ItemStyle-Wrap="false" DataField="Local" HeaderText="Local" SortExpression="Local" />
                                                <asp:BoundField ItemStyle-Wrap="false" DataField="SchoolBoard" HeaderText="School Board" SortExpression="SchoolBoard" />
                                                <asp:BoundField ItemStyle-Wrap="false" DataField="MemberType" HeaderText="MemberType" SortExpression="MemberType" />
                                                <asp:BoundField ItemStyle-Wrap="false" DataField="School" HeaderText="School" SortExpression="School" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>

                            <p class="my-account-section-footer">
                                <asp:Button runat="server" ID="PreferredUpdate" OnClick="PreferredUpdate_OnClick" CssClass="btn btn-primary btn-small" Text="<%$ Snippet: My Account Preferred School Update Button, Update %>" />
                            </p>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <!-- Address Panel -->
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:Panel runat="server" ID="AddressPanel" CssClass="my-account-section">
                            <h4><crm:Snippet runat="server" DefaultText="Home Address/Contact" SnippetName="My Account Home Address Title" /></h4>

                            <div class="my-account-section-content">
                                <div class="form-group">
                                    <asp:Label runat="server" ID="AccountAddress1Label" CssClass="control-label required" AssociatedControlID="Address1TextBox" Text="<%$ Snippet: My Account Address 1 Label, Address Line 1 %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="Address1TextBox" CssClass="input-large"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="Address1Validator" runat="server" ControlToValidate="Address1TextBox" ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error" Text="<%$ Snippet: My Account Required Field, This is a required field. %>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="AccountAddress2Label" CssClass="control-label" AssociatedControlID="Address2TextBox" Text="<%$ Snippet: My Account Address 2 Label, Address Line 2 %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="Address2TextBox" CssClass="input-large"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="AccountCityLabel" CssClass="control-label required" AssociatedControlID="CityTextBox" Text="<%$ Snippet: My Account City Label, City %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="CityTextBox" CssClass="input-large"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="CityValidator" runat="server" ControlToValidate="CityTextBox" ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error" Text="<%$ Snippet: My Account Required Field, This is a required field. %>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="AccountProvinceLabel" CssClass="control-label required" AssociatedControlID="ProvinceTextBox" Text="<%$ Snippet: My Account Province Label, Province %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="ProvinceTextBox" CssClass="input-large"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ProvinceValidator" runat="server" ControlToValidate="ProvinceTextBox" ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error" Text="<%$ Snippet: My Account Required Field, This is a required field. %>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="AccountPostalCodeLabel" CssClass="control-label required" AssociatedControlID="PostalCodeTextBox" Text="<%$ Snippet: My Account Postal Code Label, Postal Code %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="PostalCodeTextBox" CssClass="input-large"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="PostalCodeValidator" runat="server" ControlToValidate="PostalCodeTextBox" 
                                            ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error" 
                                            Text="<%$ Snippet: My Account Required Field, This is a required field. %>" />
                                        <asp:CustomValidator id="PostalCodeLengthValidator" runat="server" ControlToValidate = "PostalCodeTextBox"
                                            ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error"
                                            ErrorMessage = "<%$ Snippet: Invalid Postal Code, Invalid Postal Code. %>"
                                            ClientValidationFunction="validatePostalCodeLength" >
                                        </asp:CustomValidator>                            
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="AccountCountryLabel" CssClass="control-label required" AssociatedControlID="CountryTextBox" Text="<%$ Snippet: My Account Country Label, Country %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="CountryTextBox" CssClass="input-large"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="CountryValidator" runat="server" ControlToValidate="CountryTextBox" ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error" Text="<%$ Snippet: My Account Required Field, This is a required field. %>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="AccountHomePhoneLabel" CssClass="control-label required" AssociatedControlID="HomePhoneTextBox" Text="<%$ Snippet: My Account Home Phone Label, Home Phone %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="HomePhoneTextBox" CssClass="input-large"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="HomePhoneValidator" runat="server" ControlToValidate="HomePhoneTextBox" ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error" Text="<%$ Snippet: My Account Required Field, This is a required field. %>" />
                                        <asp:RegularExpressionValidator ID="HomePhoneRegEx" runat="server" ControlToValidate="HomePhoneTextBox" ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error" Text="<%$ Snippet: PhoneNumberFormat, Please enter a valid 10 digit phone number. %>" ValidationExpression="<%$ Snippet: RegexPhoneNumber %>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="AccountWorkPhoneLabel" CssClass="control-label" AssociatedControlID="WorkPhoneTextBox" Text="<%$ Snippet: My Account Work Phone Label, Work Phone %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="WorkPhoneTextBox" CssClass="input-large"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="WorkPhoneRegEx" runat="server" ControlToValidate="WorkPhoneTextBox" ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error" Text="<%$ Snippet: PhoneNumberFormat, Please enter a valid 10 digit phone number. %>" ValidationExpression="<%$ Snippet: RegexPhoneNumber %>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="AccountCellPhoneLabel" CssClass="control-label" AssociatedControlID="CellPhoneTextBox" Text="<%$ Snippet: My Account Work Phone Label, Cell Phone %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="CellPhoneTextBox" CssClass="input-large"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="CelPhoneRegEx" runat="server" ControlToValidate="CellPhoneTextBox" ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error" Text="<%$ Snippet: PhoneNumberFormat, Please enter a valid 10 digit phone number. %>" ValidationExpression="<%$ Snippet: RegexPhoneNumber %>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="AccountFaxLabel" CssClass="control-label" AssociatedControlID="FaxTextBox" Text="<%$ Snippet: My Account Work Phone Label, Fax %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="FaxTextBox" CssClass="input-large"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="FaxRegEx" runat="server" ControlToValidate="FaxTextBox" ValidationGroup="MyAccountAddress" Display="Dynamic" CssClass="help-inline error" Text="<%$ Snippet: PhoneNumberFormat, Please enter a valid 10 digit phone number. %>" ValidationExpression="<%$ Snippet: RegexPhoneNumber %>" />
                                    </div>
                                </div>
                            </div>
                            <p class="my-account-section-footer">
                                <asp:Button runat="server" CausesValidation="True" ValidationGroup="MyAddressUpdate" OnClientClick="ValidateAddressRequest()" ID="AddressUpdate" CssClass="btn btn-primary btn-small" OnClick="AddressUpdate_OnClick" Text="<%$ Snippet: My Account Address Update Button, Update %>" />
                            </p>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <!-- Self Identification -->  
                <div class="my-account-section">
                    <h4>
                        <crm:Snippet runat="server" DefaultText="Self Identification" SnippetName="My Account Self Identification Title" />
                        <input type="checkbox" id="selfIdentSwitch" class="switch-small" checked data-on-label="Hide" data-off-label="Show">
                    </h4>
                    <asp:UpdatePanel ID="ajaxSelfIdentPanel" runat="server">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="selfIdentPanel">
                                <div class="my-account-section-content">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <asp:Label ID="AccountFirstNationSelfIdentLabel" runat="server" CssClass="checkbox" AssociatedControlID="FirstNationSelfIdentCheckBox">
                                                <asp:CheckBox ID="FirstNationSelfIdentCheckBox" runat="server" />
                                                <asp:Literal ID="FirstNationSelfIdentLiteral" runat="server" Text="<%$ Snippet: My Account Self Ident FirstNation, First Nation %>" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:Label ID="AccountDisabledSelfIdentLabel" runat="server" CssClass="checkbox" AssociatedControlID="DisabledSelfIdentCheckBox">
                                                <asp:CheckBox ID="DisabledSelfIdentCheckBox" runat="server" />
                                                <asp:Literal ID="DisabledSelfIdentLiteral" runat="server" Text="<%$ Snippet: My Account Self Ident Disabled, Person With A Disability %>" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:Label ID="AccountMinoritySelfIdentLabel" runat="server" CssClass="checkbox" AssociatedControlID="MinoritySelfIdentCheckBox">
                                                <asp:CheckBox ID="MinoritySelfIdentCheckBox" runat="server" />
                                                <asp:Literal ID="MinoritySelfIdentLiteral" runat="server" Text="<%$ Snippet: My Account Self Ident Racial Minority, Racialized Group %>" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:Label ID="AccountMetisLabel" runat="server" CssClass="checkbox" AssociatedControlID="MetisSelfIdentCheckBox">
                                                <asp:CheckBox ID="MetisSelfIdentCheckBox" runat="server" />
                                                <asp:Literal ID="MetisSelfIdentLiteral" runat="server" Text="<%$ Snippet: My Account Self Ident Metis, Metis %>" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:Label ID="AccountInuitLabel" runat="server" CssClass="checkbox" AssociatedControlID="InuitSelfIdentCheckBox">
                                                <asp:CheckBox ID="InuitSelfIdentCheckBox" runat="server" />
                                                <asp:Literal ID="InuitSelfIdentLitera" runat="server" Text="<%$ Snippet: My Account Self Ident Inuit,Inuit %>" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:Label ID="AccountLgbtSelfIdentLabel" runat="server" CssClass="checkbox" AssociatedControlID="LgbtSelfIdentCheckBox">
                                                <asp:CheckBox ID="LgbtSelfIdentCheckBox" runat="server" />
                                                <crm:Snippet ID="Snippet9" runat="server" SnippetName="My Account Self Ident LGBT" DefaultText="Lesbian, Gay, Bisexual, Transgender, Queer or Questioning" />
                                            </asp:Label>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:Label ID="AccountWomanSelfIdentLabel" runat="server" CssClass="checkbox" AssociatedControlID="WomanSelfIdentCheckBox">
                                                <asp:CheckBox ID="WomanSelfIdentCheckBox" runat="server" />
                                                <crm:Snippet ID="Snippet10" runat="server" SnippetName="My Account Self Ident Woman" DefaultText="Woman" />
                                            </asp:Label>
                                        </div>
                                    </div> 
                                </div>
                                <p class="my-account-section-footer">
                                    <asp:Button runat="server" ID="SelfIdentUpdate" CssClass="btn btn-primary btn-small" OnClick="SelfIdentUpdate_OnClick" Text="<%$ Snippet: My Account Self Ident Button, Update %>" />
                                </p>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="tab2">
            <asp:UpdatePanel ID="UserProfileUpdateResultPanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="UserProfileResultPanel" CssClass="alert" Visible="False">
                        <asp:Literal runat="server" ID="UserProfileResultLiteral" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div class="form-horizontal">

                <!-- Personal Information -->
                <div class="my-account-section">
                    <h4>
                        <crm:Snippet runat="server" DefaultText="Personal Information" SnippetName="My Account Personal Information Title" />
                    </h4>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <div class="my-account-section-content">
                                <div class="form-group">
                                    <asp:Label runat="server" ID="CollegeIdLabel" CssClass="control-label" AssociatedControlID="CollegeIdTextBox" Text="<%$ Snippet: My Account College of TeacherId Label, College of Teachers Id %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                         <asp:TextBox runat="server" ID="CollegeIdTextBox"  MaxLength="6" CssClass="input-large"></asp:TextBox>
                                         <asp:RegularExpressionValidator ID="rfvCollegeId" ControlToValidate="CollegeIdTextBox" runat="server" ValidationGroup="MyAccountUserProfile" Display="Dynamic" CssClass="help-inline error" ValidationExpression="\d+" Text="<%$ Snippet: College of TeacherId Validation, Only digits are allowed. %>" ></asp:RegularExpressionValidator>                           
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="BirthdayLabel" CssClass="control-label col-sm-4" AssociatedControlID="BirthdayTextBox" Text="<%$ Snippet: My Account Birthday Label, Birthday %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <div class="input-group date" id="birthdayWrapper">
                                            <asp:TextBox runat="server" ID="BirthdayTextBox" CssClass="input-small form-control"></asp:TextBox>
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                        </div>
                                        <asp:HiddenField runat="server" ID="BirthdayHiddenField" />
                                    </div>
                                </div>
                            </div>
                            <p class="my-account-section-footer">
                                <asp:Button runat="server" ID="Button2" CausesValidation="True" ValidationGroup="MyAccountUserProfile" CssClass="btn btn-primary btn-small" OnClick="UserProfileUpdate_OnClick" Text="<%$ Snippet: My Account User Profile Update Button, Update %>" />
                            </p>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

                <!-- Personal Accommodation -->
                <div class="my-account-section">
                    <h4>
                        <crm:Snippet runat="server" DefaultText="Emergency Contact" SnippetName="My Account Emergency Contact Title" />
                    </h4>
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <div class="my-account-section-content">
                                <div class="form-group">
                                    <asp:Label runat="server" ID="EmergencyNameLabel" CssClass="control-label" AssociatedControlID="EmergencyNameTextBox" Text="<%$ Snippet: My Account Emergency Name Label, Name %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="EmergencyNameTextBox" CssClass="input-large"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="EmergencyDescriptionLabel" CssClass="control-label" AssociatedControlID="EmergencyDescriptionTextBox" Text="<%$ Snippet: My Account Emergency Description Label, Description %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="EmergencyDescriptionTextBox" CssClass="input-xxlarge" TextMode="MultiLine" Rows="3" Columns="41"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="EmergencyPhoneLabel" CssClass="control-label" AssociatedControlID="EmergencyPhoneTextBox" Text="<%$ Snippet: My Account Emergency Phone Label, Phone %>"></asp:Label>
                                    <div class="controls col-sm-8">
                                        <asp:TextBox runat="server" ID="EmergencyPhoneTextBox" CssClass="input-large"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="EmergencyPhoneValidator" runat="server" ControlToValidate="EmergencyPhoneTextBox" ValidationGroup="MyAccountEmergencyContact" Display="Dynamic" CssClass="help-inline error" Text="<%$ Snippet: PhoneNumberFormat, Please enter a valid 10 digit phone number. %>" ValidationExpression="<%$ Snippet: RegexPhoneNumber %>" />
                                    </div>
                                </div>
                            </div>
                  
                            <p class="my-account-section-footer">
                                <asp:Button runat="server" ID="Button1" CausesValidation="True" ValidationGroup="MyAccountEmergencyContact" CssClass="btn btn-primary btn-small" OnClick="UserEmergencyContactUpdate_OnClick" Text="<%$ Snippet: My Account User Profile Update Button, Update %>" />
                            </p>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

                <!-- Address Panel -->
                <div class="my-account-section">
                    <h4>
                        <crm:Snippet runat="server" DefaultText="Personal Accommodation" SnippetName="My Account Personal Accommodation Title" />
                    </h4>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <table style="width:100%" id="pa_Component">
                            </table>
                            <asp:HiddenField ID="retrievePersonalAccommodation" runat="server" />
                            <p class="my-account-section-footer">
                                <asp:Button runat="server" ID="UserProfileUpdate" CausesValidation="True" ValidationGroup="MyAccountUserProfile" CssClass="btn btn-primary btn-small" OnClick="PersonalAccommodationUpdate_OnClick" Text="<%$ Snippet: My Account User Profile Update Button, Update %>" />
                            </p>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="tab3">
            <uc1:MyAccountUserCreds runat="server" ID="MyAccountUserCreds" />
        </div>

        <div class="tab-pane" id="tab4">
            <iframe id="document-locker-iframe" src="/DocumentLocker/Index" frameborder="0" width="100%" scrolling="no"></iframe>
            <script src="/js/iframeResizer.min.js"></script>
            <script>
                $(document).ready(function () {
                    $('#document-locker-iframe').iFrameResize([{autoResize: true, enablePublicMethods: true}]);
                });
            </script>
        </div>
    </div>
    
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/css/assets/img/ajax-modal-loading.gif" AlternateText="Loading ..." ToolTip="Loading ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>

<asp:Content ID="Content12" ContentPlaceHolderID="Scripts" runat="server">
    <script src="<%: Url.Content("~/js/bootstrap-datepicker.min.js") %>"></script>
    <script src="<%: Url.Content("~/js/bootstrap-switch.min.js") %>"></script>
    <script>
        function EndRequestHandler(sender, args)
        {
            scrollTo(0, 0);
            LoadJQGrid(myGridData, "pa_Component", UpdatePaComponent);

            var nowTemp = new Date();
            var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
            $('#birthdayWrapper').datepicker(
            {
                autoclose: true,
                endDate: now,
                format: '<%= XrmHelper.GetSiteSettingValueOrDefault("Date Format", "mm-dd-yyyy") %>'
            }).on('changeDate', function (ev)
            {
                if (ev.date)
                {
                    var dd = ev.date.getDate();
                    var mm = ev.date.getMonth() + 1; //January is 0!
                    var yyyy = ev.date.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd;
                    }
                    if (mm < 10) {
                        mm = '0' + mm;
                    }
                    var formatDate = mm + '-' + dd + '-' + yyyy;

                    $('#<%= BirthdayHiddenField.ClientID %>').val(formatDate);
                }
            });
        }

        function UpdatePaComponent(gridPa)
        {
            myPaData.personalAccommodationList = gridPa;
            var hiddenPa = document.getElementById('<%= retrievePersonalAccommodation.ClientID %>');
            hiddenPa.value = JSON.stringify(myPaData);
        }
        
        var myPaJson = '<%= retrievePersonalAccommodation.Value %>';
        var myPaData = '';
        if(myPaJson !== '')
        {
            myPaData = jQuery.parseJSON(myPaJson);
        }

        var myGridData = myPaData.personalAccommodationList;

        ValidateAddressRequest = function ()
        {
            if (Page_ClientValidate('MyAccountAddress'))
            {
                return true;
            }
            return false;
        };

        ValidatePersonalInformationRequest = function ()
        {
            if (Page_ClientValidate('MyAccountPersonalInformation'))
            {
                return true;
            }
            return false;
        };

        $(document).ready(function ()
        {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            LoadJQGrid(myGridData, "pa_Component", UpdatePaComponent);
            $("#content_form").attr("action", "");

            var tabGroup = $('#myAccountTabGroup');
            tabGroup.on('click', 'a', function (e)
            {
                var $this = $(this);
                e.preventDefault();
                window.location.hash = $this.attr('href');
                $this.tab('show');
            });
            
            if (window.location.hash)
            {
                tabGroup.find('a[href="' + window.location.hash + '"]').tab('show');
            }

            $('#selfIdentSwitch').bootstrapSwitch().on('switch-change', function (e, data)
            {
                if (data.value)
                {
                    $('#<%=selfIdentPanel.ClientID%>').slideDown();
                }
                else
                {
                    $('#<%=selfIdentPanel.ClientID%>').slideUp();

                }
            });

            $('#selfIdentSwitch').bootstrapSwitch('setState', false);

            var nowTemp = new Date();
            var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

            var dateSelection = $('#birthdayWrapper').datepicker(
            {
                autoclose: true,
                endDate: now,
                format: '<%= XrmHelper.GetSiteSettingValueOrDefault("Date Format", "mm-dd-yyyy") %>'
            }).on('changeDate', function (ev)
            {
                if (ev.date)
                {
                    var dd = ev.date.getDate();
                    var mm = ev.date.getMonth() + 1; //January is 0!
                    var yyyy = ev.date.getFullYear();

                    if (dd < 10)
                    {
                        dd = '0' + dd;
                    }
                    if (mm < 10) {
                        mm = '0' + mm;
                    }
                    var formatDate = mm + '-' + dd + '-' + yyyy;

                    $('#<%= BirthdayHiddenField.ClientID %>').val(formatDate);
                }
            });

            dateSelection.datepicker('setDate', $('#<%= BirthdayHiddenField.ClientID %>').val()).datepicker('fill');
        });

        function checkRadioBtn(id) 
        {
            var gv = document.getElementById('<%= SchoolBoardList.ClientID %>');
            for (var i = 1; i < gv.rows.length; i++)
            {
                var radioBtn = gv.rows[i].cells[0].getElementsByTagName("input");

                // Check if the id not same
                if (radioBtn[0].id != id.id)
                {
                    radioBtn[0].checked = false;
                }
            }
        }

        function validatePostalCodeLength(oSrc, args)
        {
            var value = args.Value;
            if (value.indexOf(' ') >= 0)
            {
                args.IsValid = (value.length == 7);
            }
            else
            {
                args.IsValid = (value.length == 6);
            }
        }

    </script>
</asp:Content>
