<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="ManageLocalDelegation.aspx.cs" Inherits="Site.Areas.ETFO.Pages.ManageLocalDelegation" %>

<%@ Import Namespace="Site.Areas.ETFO.Library" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <link rel="stylesheet" href="~/css/kendo/2017.1.223/kendo.common.min.css"/>
    <link rel="stylesheet" href="~/css/kendo/2017.1.223/kendo.bootstrap.min.css" />  
      
    <script src="//cdnjs.cloudflare.com/ajax/libs/knockout/3.2.0/knockout-min.js"> </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/knockout.mapping/2.4.1/knockout.mapping.js"> </script>
    <style type="text/css" rel="stylesheet">
        .Submitted { color:#337655; } 
        .Creating { color:#000000; }
        .Approved { color:#337655; }
        .Revisit { color: #ff2014; }
        .Inactive{ color:#ff2014; }
        .Cancelled{ color: #ff2014; }
        .Rejected { color:#ff2014; }
    </style>
    <style>
        #StatusLabel {
            background: none!important;
            font-weight: bold;
        }

        .AttendingAsContainer {
            width: 100%;
            display: block;
            overflow: visible;
            float: none;
        }

        .AttendingAsBoxes {
            float: left;
            display: block;
            width: 90%;
            overflow: hidden;
            cursor: default;
            -moz-user-select: none;
            -ms-user-select: none;
            -webkit-user-select: none;
            user-select: none;
        }

        .AttendingAsEdit {
            display: block;
            float: right;
            z-index: 1000;
           
        }

        .AttendingAsBox {
            padding: 4px;
            margin: 0 10px 5px 0;
            white-space: nowrap;
            float: left;
        }

        .modal-header.yellow {

            background: #ffb848 !important;
            color: white !important;
        }

        .conformation_msg {
            /*font-size: 17px;*/
            margin-top: 10px;
            padding: 10px 10px 1px 10px;
        }

        .btn.blue {
            width: 155px !important;
        }

        .btn-voting-role {
            color: #ffffff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-color: #5bb75b;
            background-image: -moz-linear-gradient(top, #62c462, #51a351);
            background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#62c462), to(#51a351));
            background-image: -webkit-linear-gradient(top, #62c462, #51a351);
            background-image: -o-linear-gradient(top, #62c462, #51a351);
            background-image: linear-gradient(to bottom, #62c462, #51a351);
            background-repeat: repeat-x;
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff62c462', endColorstr='#ff51a351', GradientType=0);
            border-color: #51a351 #51a351 #387038;
            border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
            filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);        
        }

        .btn-novoting-role {
            color: #ffffff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-color: #faa732;
            background-image: -moz-linear-gradient(top, #fbb450, #f89406);
            background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fbb450), to(#f89406));
            background-image: -webkit-linear-gradient(top, #fbb450, #f89406);
            background-image: -o-linear-gradient(top, #fbb450, #f89406);
            background-image: linear-gradient(to bottom, #fbb450, #f89406);
            background-repeat: repeat-x;
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fffbb450', endColorstr='#fff89406', GradientType=0);
            border-color: #f89406 #f89406 #ad6704;
            border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
        }

        .portlet-title .header-data {
            padding: 3px 15px 5px !important;
        }

        .header-label
        {
            margin-top: 3px;
        }

        /*#attedingAsModal {
            width: 630px; 
            margin-left: -250px;
        }*/

    </style>
	<style type="text/css">
		.SectionForm {
			width: 100%;
			display: block;
			color: #000;
		}

		.SectionForm label {
			font-weight: normal;
		}

		.SubSection {
			padding: 0 0 0 10px;
			 width: 100%;
			display: inline-block;
		}

		.FullSectionItem {
			width: 100%;
			display: inline-block;
			clear: both;
			padding: 3px 0;
		}

		.HalfSectionItem {
			width: 45%;
			display: inline-block;
			padding: 3px 0;
		}

		.CustomSection {
			width: auto;
			display: inline-block;
			padding: 3px 0;
			float: left;
		}

		.SectionCheckbox input[type="checkbox"], .SectionCheckbox input[type="radio"] {
			margin: 0 10px 0 0;
			padding: 0;
			display: inline-block;
		}

		.SectionCheckbox label {
			cursor: pointer;
			-webkit-touch-callout: none;
			-webkit-user-select: none;
			-khtml-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
			display: inline-block;
		}

		.SectionComboBox select {
			/*width: 400px;*/
		}

		.SectionTextArea textarea {
			width: 85%;
			resize: none;
			height: 100px;
		}

		.SectionTextArea2 textarea {
			width: 95%;
			resize: none;
			height: 100px;
			margin: 0 0 0 25px;
		}

		.SectionTextArea textarea:disabled, .SectionTextArea2 textarea:disabled {
			background-color: #EEE;
		}

		.SectionButton, .SectionTextArea, .SectionTextArea2, .SectionTextBox {
			padding: 3px 0;
		}

		.SectionButton button {
			background-color: #EEE;
			border: 1px solid black;
		}

		.SectionTextArea label, .SectionComboBox label, .SectionTextBox label {
			padding: 3px 0 0 0;
			vertical-align: top;
			width: 125px;
			display: inline-block;
		}

		.OtherTextBox {
			margin: 0 10px;
		}

        .ErrorList {
            width:275px;
            text-align: left;
        }

        .error-item{
            text-align: left;
        }

        .row {
            margin-left:0px !important;
            margin-right:0px !important;
        }
	</style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <div id="Manage-Local-Delegation" style="display: none;">          
        <asp:HiddenField runat="server" ID="EventRegistrationId" />
        <div class="portlet box">
            <div class="portlet-title">
                <div class="row">
                    <div class="col-sm-6">
                        <crm:Snippet runat="server" SnippetName="ManageLocalDelegation/EventLabel" DefaultText="EVENT" CssClass="header-label" />
                        <label id="EventNameLabel" class="header-data x2"></label>

                        <crm:Snippet runat="server" SnippetName="ManageLocalDelegation/MaxDelegatesText" DefaultText="MAXIMUM # OF DELEGATES" CssClass="header-label" />
                        <label id="MaxDelegatesLabel" class="header-data"></label>

                    </div>
                    <div class="col-sm-6">
                        <crm:Snippet runat="server" SnippetName="ManageLocalDelegation/LocalOfficeLabel" DefaultText="LOCAL OFFICE" CssClass="header-label" />
                        <label id="LocalOfficeLabel" class="header-data x2"></label>
                    </div>
                </div>
            </div>
        </div>
        <div id = "alert_placeholder"></div>
        <div id = "success_placeholder"></div>
        <div class="portlet grey">
            <div class="row" style="height: 32px;">
                <div class="col-sm-10">
                    <crm:Snippet runat="server" SnippetName="ManageLocalDelegation/StatusLabel" DefaultText="Local Delegation Status" CssClass="status-label" />
                </div>
                <div class="col-sm-2">
                    <label id="StatusLabel" class="status-box pull-right">Creating</label>
                </div>
            </div>
        </div>

        <div class="portlet box grey">
            <%-- Delegation List --%>
            <div class="row olive-green">
                <div class="col-sm-12 grid-header">
                    <crm:Snippet runat="server" SnippetName="ManageLocalDelegation/DelegationGrid" DefaultText="DELEGATION LIST"></crm:Snippet>
                </div>
            </div>
            <div class="portlet-body">
                <div class="dataTables_wrapper form-inline" role="grid">
                    <table class="table" id="DelegationList_table">
                        <tr>
                            <th>
                                <a>
                                    <crm:Snippet runat="server" SnippetName="ManageLocalDelegation/DelegateNameHeader" DefaultText="Member Name" />
                                    <i class="fa fa-caret-down rediconcolor"></i>
                                </a>
                            </th>
                            <th style="width:440px !important;">
                                <crm:Snippet ID="Snippet3" runat="server" SnippetName="ManageLocalDelegation/DelegateAttendinAsHeader" DefaultText="Attending As" />
                            </th>
                            <th>
                                <a>
                                    <crm:Snippet ID="Snippet5" runat="server" SnippetName="ManageLocalDelegation/DelegateRegistrationStatusHeader" DefaultText="Registration Status" />
                                    <i class="fa fa-caret-down rediconcolor"></i>
                                </a>
                            </th>
                            <th></th>
                        </tr>
                        <tr style="display: none;" data-empty="true">
                            <td colspan="4" style="text-align: center; padding: 10px 0"><b>
                                <crm:Snippet ID="Snippet8" runat="server" SnippetName="ManageLocalDelegation/NoResultsFound" DefaultText="No delegates found" /></b></td>
                        </tr>
                    </table>
                </div>

                <div class="row dark-grey">
                    <div class="col-sm-12 grid-header">
                        <crm:Snippet runat="server" SnippetName="ManageLocalDelegation/RegistrantGridHeader" DefaultText="REGISTRANTS"></crm:Snippet>
                        <div class="grid-buttons">
                            <a id="RemoveDelegate" class="aspNetDisabled"><i class='fa fa-arrow-down fa-2x pointer'></i></a>
                            <a id="AddDelegate" class="aspNetDisabled"><i class='fa fa-arrow-up fa-2x pointer'></i></a>
                        </div>

                        <ul class="select pull-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="filterValue"></a>
                                <ul class="dropdown-menu">
                                    <li>  
                                        <a id="FilterAllButton">
                                           <crm:Snippet runat="server" SnippetName="ManageLocalDelegation/RegistrantFilterAll" DefaultText="All" />
                                        </a>
                                    </li>
                                    <li>
                                      <a id="FilterVotingButton">
                                            <crm:Snippet ID="Snippet4" runat="server" SnippetName="ManageLocalDelegation/RegistrantFilterVoting" DefaultText="Voting Role" />
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <asp:HiddenField ID="FilterVotingHidden" runat="server" />
                    </div>
                </div>

                <%-- Registrants List --%>
                <div class="portlet-body">
                    <div class="dataTables_wrapper form-inline" role="grid">
                        <table class="table" id="Registrant_table">
                            <tr>
                                <th>
                                    <a id="LinkButton1">
                                        <crm:Snippet ID="Snippet2" runat="server" SnippetName="ManageLocalDelegation/RegistrantNameHeader" DefaultText="Member Name" />
                                        <i class="fa fa-caret-down rediconcolor"></i>
                                    </a>
                                </th>
                                <th style="width:440px !important;">
                                    <crm:Snippet ID="Snippet1" runat="server" SnippetName="ManageLocalDelegation/RegistrantAttendinAsHeader" DefaultText="Attending As" />
                                </th>
                                <th>
                                    <a id="LinkButton4">
                                        <crm:Snippet ID="Snippet6" runat="server" SnippetName="ManageLocalDelegation/RegistrantRegistrationStatusHeader" DefaultText="Registration Status" />
                                        <i class="fa fa-caret-down rediconcolor"></i>
                                    </a>
                                </th>
                                <th></th>
                            </tr>
                            <tr style="display: none;" data-empty="true">
                                <td colspan="4" style="text-align: center; padding: 10px 0"><b>
                                    <crm:Snippet ID="Snippet7" runat="server" SnippetName="ManageLocalDelegation/RegistrantsNoResultsFound" DefaultText="No registrants found" /></b>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <label id="SubmitButton" class="btn btn-primary blue" style="float: left;  width: 143px;text-align:center;" onclick="showConformationWindow_Submit()"><asp:Literal runat="server" Text="<%$ Snippet: ManageLocalDelegation/SubmitButtonText, Approve Delegation %>"></asp:Literal></label>
        <label id="SaveButton" class="btn btn-primary blue" style="float: left;margin-left: 10px;  width: 120px;">Save</label>
    </div>
    
    <div class="modal fade" style="" id="attedingAsModal" tabindex="-1" role="dialog" aria-labelledby="attedingAsModallLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header yellow">
                    <button type="button"  class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="attedingAsModalLabel">Attending As</h4>
                </div>
                <div class="modal-body"   id="attedingAsModal_body">
		            <div id="attendingas" style=" margin: 0; border: none;" class=" portlet box yellow">
			        <div class="portlet-body">
				        <div class="SectionForm">
					        <div class="FullSectionItem">
						        <label data-bind="text: sectionMessage" style="padding: 2px 10px"></label>
					        </div>
					        <div class="SubSection">
						        <div class="CustomSection" style="width: 300px;">
							        <div class="FullSectionItem">
								        <label style="font-weight: bold">Voting Role</label>
							        </div>
							        <div class="FullSectionItem" data-bind="foreach: attendingAsList">
								        <!-- ko if: (isVotingRole()==true) -->
								        <div class="FullSectionItem SectionCheckbox">
									        <label style="margin: 0">
										        <input type="checkbox" data-bind="checked: isSelected, attr: { id: id }, value: id, event: { click: $parent.SetVoteRole }" />
										        <span data-bind="text: name"></span>
									        </label>
								        </div>
								        <!-- /ko-->
							        </div>
						        </div>
						        <div class="CustomSection" style="width: 300px;">
							        <div class="FullSectionItem">
								        <label style="font-weight: bold">Non Voting Role</label>
							        </div>
							        <div class="FullSectionItem" data-bind="foreach: attendingAsList">
								        <!-- ko if: (isVotingRole()==false) -->
								        <div class="FullSectionItem SectionCheckbox">
									        <label style="margin: 0">
										        <input type="checkbox" data-bind="checked: isSelected, attr: { id: id }, value: id, event: { click: $parent.ShowHideOtherTextBox }" />
										        <span data-bind="    text: name"></span>
										        <input class="OtherTextBox" type="text" data-bind="value: otherInput, style: { display: isOther() && isSelected() ? 'inline' : 'none' }, event: { blur: $parent.ValidateOtherContent }" />
									        </label>
								        </div>
								        <!-- /ko-->
							        </div>
						        </div>
					        </div>
				        </div>
			        </div>
		        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" data-bind="event: { click: SaveChanges }">Apply Changes</button>
                </div>
            </div>
        </div>
    </div>
     
    <div class="modal fade" style="" id="ConformationWindow" tabindex="-1" role="dialog" aria-labelledby="ConformationWindowlLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="ConformationWindow_body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary red" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="Continue_btn">Continue</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" style="" id="messageWindow" tabindex="-1" role="dialog" aria-labelledby="messageWindowlLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="messageWindowBody"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary red" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7; display: none;" id="loadingPanel">
        <img src="../../../css/assets/img/ajax-modal-loading.gif" style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
    </div>
    
    <script src="~/js/kendo/2017.1.223/kendo.web.min.js"></script>
    <script src="~/js/kendo/2017.1.223/cultures/kendo.culture.en-CA.min.js"></script>
</asp:Content>

<asp:Content ID="Content12" ContentPlaceHolderID="Scripts" runat="server">
    <script type="text/javascript">
        var eventRegistrationId = null;
        var eventDelegationId = null;
        var eventId = null;
        var maxDelegates = 0;
        var succesIconCSS = '<%= GetSuccessIcon() %>';
        var alertIconCSS = '<%= GetAlertIcon() %>';
        var tempAttendingAsHtml = null;
        var isAttendingAsChange = false;
        var isRowMovedChange = false;
        var SaveAttendingAsChanges_msg = null;
        var SaveDelegationChanges_msg = null;
        var Submit_msg = null;
        var registrationApproved = parseInt('<%= RegistrationHelper.EventRegistration.Approved %>');
        var registrationSubmitted = parseInt('<%= RegistrationHelper.EventRegistration.Sumbitted %>');
        var registrationDenied = parseInt('<%= RegistrationHelper.EventRegistration.Denied %>');
        var registrationCancelled = parseInt('<%= RegistrationHelper.EventRegistration.Cancelled %>');
        var registrationWithdrawn = parseInt('<%= RegistrationHelper.EventRegistration.Withdrawn %>');
        var delegationApproved = parseInt('<%= RegistrationHelper.LocalDelegation.Approved %>');
        var delegationCreating = parseInt('<%= RegistrationHelper.LocalDelegation.Creating %>');
        var delegationRevisit = parseInt('<%= RegistrationHelper.LocalDelegation.Revisit %>');
        var delegationStatus = null;
        var maxDelegatesMessage = '';
        
        //Error tooltips
        var errorDelegationTitle = getSnippetValue("ManageLocalDelegation/ErrorMsg/Title", "Error(s)");
        var errorDelegationNoVotingRole = getSnippetValue("ManageLocalDelegation/ErrorMsg/DelegationsNoVotingRole", "User must have a voting role");
        var errorDelegationNoApproved = getSnippetValue("ManageLocalDelegation/ErrorMsg/DelegationsNoApproved", "Registration must be approved");
        var errorRegistrantApproved = getSnippetValue("ManageLocalDelegation/ErrorMsg/RegistrantsVotingRole", "Users with approved or submitted registrations cannot have a voting role");
        var defaultAuthorizationError = getSnippetValue("ManageLocalDelegation/ErrorMsg/DefaultAuthorizationError", "You are not authorized to see this page");

        //0: OK
        //1: No voting role
        //2: Registration status
        var delegationDisabled = 0;
		
		var currentData = null;
		var mapping =
		{
			'ignore': ["registrationId"]
		};

        function getEventRegistrationInfo()
        {
            eventRegistrationId = $("input[id$='<%=EventRegistrationId.ClientID %>']").val();
            $.ajax(
            {
                url: "/api/delegation/GetEventRegistrationInfo/" + eventRegistrationId,
                type: "GET",
                dataType: "json",
                statusCode: 
                {
                    401: function (response)
                    {
                        var responseText = defaultAuthorizationError;
                        if (response.responseText !== "")
                        {
                            responseText = JSON.parse(response.responseText);
                        }

                        $("#SubmitButton").hide();
                        $("#SaveButton").hide();
                        $("#loadingPanel").hide();
                        bootstrap_modal(null, responseText);
                    },
                    200: function (response) 
                    {
                        console.log(response);
                        eventDelegationId = response.Id;
                        eventId = response.Event;
                        maxDelegates = response.MaxDelegates;
                        bindEventRegistrationInfo(response);
                        getEventRegistrants();

                        maxDelegatesMessage = getSnippetValue("ManageLocalDelegation/ErrorMsg/MaxDelegatesError", "You have reached the maximum number of delegates permited ({0})").format(maxDelegates);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    if (xhr.status === 401)
                    {
                        $("#SubmitButton").hide();
                        $("#SaveButton").hide();
                        $("#loadingPanel").hide();

                        bootstrap_modal(null, "Your session has timed out. Please login and try again");
                    }
                }
            });
        }

        String.prototype.format = function ()
        {
            var args = arguments;
            return this.replace(/{(\d+)}/g, function (match, number)
            {
                return typeof args[number] != 'undefined' ? args[number] : match;
            });
        };

        function getEventRegistrants()
        {
            $.ajax(
            {
                url: "/api/delegation/GetEventRegistrants?eventRegistrationId=" + eventRegistrationId,
                type: "GET",
                dataType: "json",
                statusCode:
                {
                    401: function (response)
                    {
                        var responseText = defaultAuthorizationError;
                        if (response.responseText !== "")
                        {
                            responseText = JSON.parse(response.responseText);
                        }

                        $("#SubmitButton").hide();
                        $("#SaveButton").hide();
                        $("#loadingPanel").hide();
                        bootstrap_modal(null, responseText);
                    },
                    200: function (response)
                    {
                        console.log(response);
                        bindDelegationList(response);

                        $("#DelegationList_table").kendoTooltip(
                        {
                            filter: "i.fa-exclamation-triangle",
                            position: "top",
                            content: function (e)
                            {
                                var target = e.target; // the element for which the tooltip is shown
                                var error = $(target).attr("data-error");
                                var errorList = '';
                                if (error)
                                {
                                    var errors = error.split("|");
                                    $.each(errors, function (i)
                                    {
                                        var item = errors[i];
                                        if (item)
                                        {
                                            errorList += '<li class="error-item">' + item + '</li>';
                                        }
                                    });
                                }

                                if (errorList)
                                {
                                    return '<p class="ErrorList">' + errorDelegationTitle + '<ol>' + errorList + '</ol></p>';
                                }
                            }
                        });

                        $("#Registrant_table").kendoTooltip(
                        {
                            filter: "i.fa-exclamation-triangle",
                            position: "top",
                            content: function (e)
                            {
                                var target = e.target; // the element for which the tooltip is shown
                                var error = $(target).attr("data-error");
                                var errorList = '';
                                if (error)
                                {
                                    var errors = error.split("|");
                                    $.each(errors, function (i)
                                    {
                                        var item = errors[i];
                                        if (item)
                                        {
                                            errorList += '<li>' + item + '</li>';
                                        }
                                    });
                                }

                                if (errorList)
                                {
                                    return '<p class="ErrorList">' + errorDelegationTitle + '<ol>' + errorList + '</ol></p>';
                                }
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    if (xhr.status === 401)
                    {
                        $("#SubmitButton").hide();
                        $("#SaveButton").hide();
                        $("#loadingPanel").hide();

                        bootstrap_modal(null, "Your session has timed out. Please login and try again");
                    }
                }
            });
        }

        function getSnippetValue(val, defVal)
        {
            var msg = "Server Error";
            if (val != null)
            {
                $.ajax(
                {
                    url: "/api/delegation/GetSnippetValue?id=" + val,
                    type: "GET",
                    dataType: "json",
                    async: false,
                    statusCode:
                    {
                        401: function ()
                        {
                        },
                        200: function (request)
                        {
                            console.log(request);
                            msg = request;

                            if (!request && defVal)
                            {
                                msg = defVal;
                            }
                        }
                    }
                });
            }
            return msg;
        }

        function showConformationWindow_Save()
        {
            $("#ConformationWindow_body").empty();
            if (isRowMovedChange)
            {
                if (SaveDelegationChanges_msg == null)
                {
                    SaveDelegationChanges_msg = getSnippetValue("ManageLocalDelegation/ButtonMessage/Save");
                }
                $("<div class='alert-info conformation_msg'>" + SaveDelegationChanges_msg + "</div>").appendTo("#ConformationWindow_body");
            }
            if (isAttendingAsChange)
            {
                if (SaveAttendingAsChanges_msg == null)
                {
                    SaveAttendingAsChanges_msg = getSnippetValue("ManageLocalDelegation/ButtonMessage/SaveAttendingAsChanges");
                }
                $("<div class='alert-info conformation_msg'>" + SaveAttendingAsChanges_msg + "</div>").appendTo("#ConformationWindow_body");
            }

            $("#Continue_btn").attr("onclick", "saveDelegationRegistrants()");
            $("#ConformationWindow").modal('show');
        }

        function showConformationWindow_Submit()
        {
            $("#ConformationWindow_body").empty();
            if (Submit_msg == null)
            {
                Submit_msg = getSnippetValue("ManageLocalDelegation/ButtonMessage/Submit");
            }
            $("<div class='alert-info conformation_msg'>" + Submit_msg + "</div>").appendTo("#ConformationWindow_body");
            $("#Continue_btn").attr("onclick", "submitDelegation()");
            $("#ConformationWindow").modal('show');
        }

        function refreshAdxCache()
        {
	        var url = document.location.protocol + '//' + document.location.host + '/Cache.axd?Message=InvalidateAll&d=' + (new Date()).valueOf(); 
	        var req = new XMLHttpRequest(); 
	        req.open('GET', url, false); 
	        req.send(null);
        }

        function saveDelegationRegistrants()
        {
            if (!validateFullDelegation(false))
            {
                return;
            }

            $("#ConformationWindow").modal('hide');
            $("#loadingPanel").show();
            var tempData = CreateDatamodel(false);
 
            if (tempData != null)
            {
                $.ajax({
                    url: "/api/delegation/SaveDelegationRegistrants",
                    type: "POST",
                    contentType: "application/json",
                    data: tempData,
                    statusCode:
                        {
                        401: function () 
                        {
                            $("#loadingPanel").hide();
                            bootstrap_modal(null, "You are not authorized to save the delegation registrants");
                        },
                        200: function ()
                        {
                            //location.reload();
                            $("#loadingPanel").hide();
					
					        refreshAdxCache();
					        RefreshAttendingAsValues();
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError)
                    {
                        if (xhr.status === 401)
                        {
                            $("#SubmitButton").hide();
                            $("#SaveButton").hide();
                            $("#loadingPanel").hide();

                            bootstrap_modal(null, "Your session has timed out. Please login and try again");
                        }
                        else
                        {
                            $("#loadingPanel").hide();
                            bootstrap_modal("ManageLocalDelegation/ErrorMsg/SubmissionError");
                        }
                    }
                });
            }
        }

        function submitDelegation()
        {
            $("#ConformationWindow").modal('hide');
            if (!validateFullDelegation(true))
            {
                return;
            }

            var tempData = CreateDatamodel(true);

            $('#loadingPanel').show();
            $.ajax({
                url: "/api/delegation/SubmitDelegation/" + eventDelegationId,
                type: "POST",
                contentType: "application/json",
                data: tempData,
                statusCode:
                {
                    400: function ()
                    {
                        $('#loadingPanel').hide();
                        bootstrap_modal("ManageLocalDelegation/ErrorMsg/SubmissionError");
                    },
                    200: function () 
			        {
                        bootstrap_modal("ManageLocalDelegation/SuccessMsg/SubmitSuccessful");
                        $("#StatusLabel").attr("class", "status-box pull-right Approved").html("Approved");
                        delegationStatus = delegationApproved;
                        $("#SaveButton").hide();
				
				        refreshAdxCache();
				        RefreshAttendingAsValues();

				        $('#loadingPanel').hide();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    if (xhr.status === 401)
                    {
                        $("#SubmitButton").hide();
                        $("#SaveButton").hide();
                        $("#loadingPanel").hide();
 
                        bootstrap_modal(null, "Your session has timed out. Please login and try again");
                    }
                    else
                    {
                        $("#loadingPanel").hide();
                        bootstrap_modal("ManageLocalDelegation/ErrorMsg/SubmissionError");
                    }
                }
            });
        }

        function CreateDatamodel(isApprove)
        {
            var itemArray = new Array();
            var items = $("tr[data-state='modified']");
            console.log(items);

            var tempObj;
            for (var i = 0; i < items.length; i++) 
	        {
                tempObj = new Object();
                tempObj.Item1 = $(items[i]).attr("data-registrationID");
                tempObj.Item2 = $(items[i]).attr("data-indelegationlist");
                tempObj.Item3 = $(items[i]).attr("data-attendingasjson");
                tempObj.Item4 = $(items[i]).attr("data-state");
                itemArray.push(tempObj);
            }

            if (isApprove)
            {
                items = $("tr[data-state='normal'][data-votingstatus='true']");
                console.log(items);
                for (var i = 0; i < items.length; i++)
                {
                    tempObj = new Object();
                    tempObj.Item1 = $(items[i]).attr("data-registrationID");
                    tempObj.Item2 = $(items[i]).attr("data-indelegationlist");
                    tempObj.Item3 = $(items[i]).attr("data-attendingasjson");
                    tempObj.Item4 = $(items[i]).attr("data-state");
                    itemArray.push(tempObj);
                }
            }

            console.log(itemArray);
            if (itemArray.length > 0)
            {
                return JSON.stringify(itemArray);
            }

            return null;
        }

        function RefreshAttendingAsValues()
        {
	        var items = $("tr.registrant-row");
	        for (var j = 0; j < items.length; j++) 
	        {
		        //Update attending as json
		        var data = $(items[j]).attr("data-attendingasjson");
		        if (typeof data == 'string' || data instanceof String)
		        {
			        data = JSON.parse(data);
		        }
		
		        var list = data.attendingAsList;
		        for (var i = 0; i < list.length; i++)
		        {
			        if (list[i].isSelected)
			        {
				        list[i].selected = 
				        {
					        Id: list[i].id,
					        LogicalName: "oems_attendingasoption",
					        Name: null
				        };
			        }
			        else
			        {
				        list[i].selected = null;
			        }
		        }
		        $(items[j]).attr("data-attendingasjson", JSON.stringify(data));
	        }
        }

        function findVotingStatus(val)
        {
            var obj = $.parseJSON(val);
            var tempAttedning = obj.attendingAsList;
            if (tempAttedning.length > 0)
            {
                for (var i = 0; i < tempAttedning.length; i++)
                {
                    if (tempAttedning[i].isVotingRole)
                    {
                        if (tempAttedning[i].isSelected)
                        {
                            return true;
                        }
                    }
                }
            } 
    
            return false;
        }

        function bindDelegationList(data)
        {
            $("#DelegationList_table tr[data-registrationid]").remove();
            if (data.length > 0)
            {
                for (var i = 0; i < data.length; i++)
                {
                    var votingStatus = findVotingStatus(data[i].AttendingAsJSON);
                    if (data[i].InDelegationList)
                    {
                
                        $("<tr style='z-index:100' class='registrant-row'  data-registrationID='" + data[i].RegistrationID + "' data-registrationStatus='" + data[i].RegistrationStatusId + "' data-indelegationlist='" + data[i].InDelegationList + "' data-state='normal' data-votingStatus='" + votingStatus + "' data-attendingasjson='" + data[i].AttendingAsJSON + "' ><td>" + data[i].RegistrantFirstName + " " + data[i].RegistrantLastName + "</td><td data-attendingasVAL='true'></td> <td>" + data[i].RegistrationStatusText + "</td><td class='center' data-name='status_icon' ></td></tr>").appendTo("#DelegationList_table");
                        bindAttedingAs(data[i].RegistrationID);
                        bindStatusIconCSS_Delegation(data[i].RegistrationID, data[i].RegistrationStatusId);
                    } else
                    {
                        $("<tr style='z-index:100' class='registrant-row'  data-registrationID='" + data[i].RegistrationID + "' data-registrationStatus='" + data[i].RegistrationStatusId + "' data-indelegationlist='" + data[i].InDelegationList + "' data-state='normal' data-votingStatus='" + votingStatus + "' data-attendingasjson='" + data[i].AttendingAsJSON + "' ><td>" + data[i].RegistrantFirstName + " " + data[i].RegistrantLastName + "</td><td data-attendingasVAL='true'></td> <td>" + data[i].RegistrationStatusText + "</td> <td class='center' data-name='status_icon' ></td></tr>").appendTo("#Registrant_table");
                        bindAttedingAs(data[i].RegistrationID);
                        bindStatusIconCSS_Registrants(data[i].RegistrationID, data[i].RegistrationStatusId);
                    }
                }
            }
            else
            {
                $("tr[data-empty='true']").show();
            }
            bindArrows();
            bindfilter(0);

            //finishing Load
            $("#loadingPanel").hide();
        }

        function bindfilter(filter)
        {
            var filterAll = $("#FilterAllButton");
            var filterVoting = $("#FilterVotingButton");
            $("#filterValue").empty();
            if (filter === 1)
            {

                $("#filterValue").append(filterVoting.text() + "<i class='fa fa-caret-down pull-right'></i>");
                $("#filterValue").attr("data-filter", "1");
                $("#Registrant_table tr[data-votingStatus='false']").hide();
            }
            else
            {
                $("#filterValue").append(filterAll.text() + "<i class='fa fa-caret-down pull-right'></i>");
                $("#filterValue").attr("data-filter", "0");
                $("#Registrant_table tr.registrant-row").show();
            }
        }

        function bindAttedingAs(id)
        {
            var obj = $("tr[data-registrationID='" + id + "']");

            var tempObj = jQuery.parseJSON(obj.attr("data-attendingasjson"));
            var attendingAsList = tempObj.attendingAsList;
            var tempHtml = "";
            for (var i = 0; i < attendingAsList.length; i++)
            {
                if (attendingAsList[i].isSelected)
                {
                    if (attendingAsList[i].isVotingRole)
                    {
                        tempHtml += "<span class='btn-primary btn-voting-role AttendingAsBox'>" + attendingAsList[i].name + "</span>";
                    }
                    else
                    {
                        tempHtml += "<span class='btn-primary btn-novoting-role AttendingAsBox'>" + attendingAsList[i].name + "</span>";
                    }
                }
            }

            var tempBindobj = $("tr[data-registrationID='" + id + "'] > td[data-attendingasVAL='true']");

            tempBindobj.empty();
            tempBindobj.html("<div id='attendingAsContainer' class='AttendingAsContainer'>" +
                "<div id='attendingAsBoxes' class='AttendingAsBoxes'>" + tempHtml + "</div>" +
                "<div class='AttendingAsEdit'><a style='text-decoration: none; cursor: pointe' id='editattending' class='fa fa-pencil-square-o fa-large greyiconcolor' onclick='OpenAttendingAsWindow(this)' ></a></div>" +
                "</div>");
        }

        function bindStatusIconCSS_Delegation(id, registrationStatus) {
            var votingStatus = $("tr[data-registrationID='" + id + "']").attr("data-votingStatus");
            var tempObj = $("tr[data-registrationID='" + id + "'] > td[data-name='status_icon']");
            tempObj.empty();

            var error = '';
            var valid = true;
            if (votingStatus !== 'true') {
                valid = false;
                error = errorDelegationNoVotingRole + "|";
            }

            if (delegationStatus == delegationCreating || delegationStatus == delegationRevisit) {
                var validAux = registrationStatus != registrationDenied && registrationStatus != registrationCancelled && registrationStatus != registrationWithdrawn;
                if (!validAux) {
                    valid = false;
                    error = error + errorDelegationNoApproved + "|";
                }
            }
            else if (delegationStatus == delegationApproved) {
                var validAux = registrationStatus == registrationApproved;
                if (!validAux) {
                    valid = false;
                    error = error + errorDelegationNoApproved + "|";
                }
            }

            if (valid) {
                tempObj.html("<i class='" + succesIconCSS + "'></i>");
            }
            else {
                tempObj.html("<i class='" + alertIconCSS + "' data-error='" + error + "'></i>");
            }
        }

        function bindStatusIconCSS_Registrants(id, registrationStatus) {
            var votingStatus = $("tr[data-registrationID='" + id + "']").attr("data-votingStatus");
            var tempObj = $("tr[data-registrationID='" + id + "'] > td[data-name='status_icon']");
            tempObj.empty();

            var valid = true;
            if (delegationStatus == delegationCreating) {
                valid = !(votingStatus === 'true' && (registrationStatus == registrationApproved || registrationStatus == registrationSubmitted));
            }
            else if (delegationStatus == delegationApproved || delegationStatus == delegationRevisit) {
                valid = !(votingStatus === 'true' && (registrationStatus == registrationApproved));
            }

            if (valid) {
                tempObj.html("<i class='" + succesIconCSS + "'></i>");
            }
            else {
                tempObj.html("<i class='" + alertIconCSS + "' data-error='" + errorRegistrantApproved + "'></i>");
            }
        }

        function maxDelegationMessage()
        {
            bootstrap_modal(null, maxDelegatesMessage);
        }

        function bindArrows()
        {
            var disableIconCss = "aspNetDisabled";
            var countDelegates = $("#DelegationList_table tr[data-registrationID]").length;

            var addDelegate = $("#AddDelegate");
            var removeDelegate = $("#RemoveDelegate");

            if (countDelegates >= maxDelegates)
            {
                if (!addDelegate.hasClass(disableIconCss))
                {
                    addDelegate.addClass(disableIconCss);
                    addDelegate.removeAttr("onclick");
                    addDelegate.attr("onclick", "maxDelegationMessage()");
                }
            }
            else
            {
                addDelegate.removeClass();
                addDelegate.removeAttr("onclick");
                addDelegate.attr("onclick", "addDelegate()");
            }

            removeDelegate.removeClass();
            removeDelegate.attr("onclick", "removeDelegate()");
        }

        function bindEventRegistrationInfo(data)
        {
            $("#EventNameLabel").text(data.EventName);
            $("#MaxDelegatesLabel").text(data.MaxDelegates);
            $("#LocalOfficeLabel").text(data.LocalOffice);
            $("#StatusLabel").text(data.Status);
            $("#StatusLabel").addClass(data.Status);
            $("#Manage-Local-Delegation").show();
            delegationStatus = data.StatusId;
            if (delegationStatus == delegationApproved || delegationStatus == delegationRevisit)
            {
                $("#SaveButton").hide();
            }
        }

        function validateFullDelegation(isApprove)
        {
            var valid = true;
            var status;
            var votingStatus;
            var id;
            var icon;

            if (delegationStatus == delegationApproved || isApprove)
            {
                $("#DelegationList_table tr.registrant-row").each
                (
                    function ()
                    {
                        status = parseInt($(this).attr("data-registrationStatus"));
                        votingStatus = $(this).attr("data-votingstatus") === 'true';
                        id = $(this).attr("data-registrationID");
                        icon = $("tr[data-registrationID='" + id + "'] > td[data-name='status_icon']");
                        if (icon.children("i") && icon.children("i").attr("data-error"))
                        {
                            valid = false;
                        }
                        else
                        {
                            var error = '';
                            if (!votingStatus)
                            {
                                valid = false;
                                error = error + errorDelegationNoVotingRole + "|";
                            }

                            if (status != registrationApproved)
                            {
                                valid = false;
                                error = error + errorDelegationNoApproved + "|";
                            }

                            if (votingStatus && status == registrationApproved)
                            {
                                icon.html("<i class='" + succesIconCSS + "'></i>");
                            }
                            else
                            {
                                icon.html("<i class='" + alertIconCSS + "' data-error='" + error + "'></i>");
                            }
                        }
                    }
                );

                $("#Registrant_table tr.registrant-row").each
                (
                    function ()
                    {
                        status = parseInt($(this).attr("data-registrationStatus"));
                        votingStatus = $(this).attr("data-votingstatus") === 'true';
                        id = $(this).attr("data-registrationID");
                        icon = $("tr[data-registrationID='" + id + "'] > td[data-name='status_icon']");
                        if (icon.children("i") && icon.children("i").attr("data-error"))
                        {
                            valid = false;
                        }
                        else
                        {
                            valid = valid && !(votingStatus && status == registrationApproved);
                            if (!(votingStatus && status == registrationApproved))
                            {
                                icon.html("<i class='" + succesIconCSS + "'></i>");
                            }
                            else
                            {
                                tempObj.html("<i class='" + alertIconCSS + "' data-error-approved='" + errorRegistrantApproved + "'></i>");
                            }
                        }
                    }
                );
            }

            if (!valid) {
                bootstrap_modal("ManageLocalDelegation/ErrorMsg/ApproveLocalValidationMsg", "There are errors in the local delegation form, move the mosue over the warning icon(s) in order to see the error(s). Please fix them before you can continue");
            }

            return valid;
        }

        function validateAddDelegate()
        {
            var enabled = true;
            var status;
            var votingStatus;
            delegationDisabled = 0;
            $("#AddDelegate").removeClass("aspNetDisabled");

            $("#Registrant_table tr.registrant-row.active").each
            (
                function ()
                {
                    status = parseInt($(this).attr("data-registrationStatus"));
                    votingStatus = $(this).attr("data-votingstatus") === 'true';
                    if (delegationStatus == delegationCreating)
                    {
                        enabled = enabled && (status == registrationApproved || status == registrationSubmitted) && votingStatus;
                        if (!((status == registrationApproved || status == registrationSubmitted) && votingStatus))
                        {
                            delegationDisabled = 1;
                        }
                    }
                    else if (delegationStatus == delegationApproved || delegationStatus == delegationRevisit)
                    {
                        enabled = enabled && status == registrationApproved && votingStatus;
                        if (!(status == registrationApproved && votingStatus))
                        {
                            delegationDisabled = 2;
                        }
                    }
                }
            );

            if (!enabled)
            {
                $("#AddDelegate").addClass("aspNetDisabled");
            }
        }

        function validateRemoveDelegate()
        {
            var enabled = true;
            var status;
            var votingStatus;
            delegationDisabled = 0;
            $("#RemoveDelegate").removeClass("aspNetDisabled");

            $("#DelegationList_table tr.registrant-row.active").each
            (
                function ()
                {
                    status = parseInt($(this).attr("data-registrationStatus"));
                    votingStatus = $(this).attr("data-votingstatus") === 'true';
                    if (delegationStatus == delegationCreating)
                    {
                        enabled = enabled && (!votingStatus || status == registrationDenied || status == registrationCancelled || status == registrationWithdrawn);
                        if (votingStatus && status != registrationDenied && status != registrationCancelled && status != registrationWithdrawn)
                        {
                            delegationDisabled = -1;
                        }
                    }
                    else if (delegationStatus == delegationApproved || delegationStatus == delegationRevisit)
                    {
                        enabled = enabled && (!votingStatus || status == registrationSubmitted || status == registrationDenied || status == registrationCancelled || status == registrationWithdrawn);
                        if (votingStatus && status != registrationSubmitted && status != registrationDenied && status != registrationCancelled && status != registrationWithdrawn)
                        {
                            delegationDisabled = -1;
                        }
                    }
                }
            );

            if (!enabled)
            {
                $("#RemoveDelegate").addClass("aspNetDisabled");
            }
        }

        function addDelegate()
        {
            validateAddDelegate();
            if ($("#AddDelegate").hasClass("aspNetDisabled"))
            {
                if (delegationDisabled == 1) 
                {
                    bootstrap_modal("ManageLocalDelegation/ErrorMsg/MoveRegistrantCreatingDelegation", "The selected Members cannot be moved into the Local Delegation. Please make sure that all selected Members have a voting role and their registration is in either Submitted or Approved status. <br/> Please change their Attending As to a voting role first and/or contact event support to check on the registration status.");
                }
                else if (delegationDisabled == 2) 
                {
                    bootstrap_modal("ManageLocalDelegation/ErrorMsg/MoveRegistrantApprovedOrRevisitDelegation", "The selected Members cannot be moved into the Local Delegation. Please make sure that all selected Members have a voting role and their registration is in Approved status. <br /> Please change their Attending As to a voting role first and/or contact the event support to check on the registration status.");
                }
                return;
            }
            var item = $('#Registrant_table .registrant-row.active');
            var existItems = $('#DelegationList_table .registrant-row');
            if (item.length > 0) {
                if (maxDelegates < (existItems.length + item.length)) {
                    bootstrap_modal("ManageLocalDelegation/ErrorMsg/DelegationLimitExceeded");
                } else {

                    for (var i = 0; i < item.length; i++) {
                        var tempItem = $(item[i]);
                
                        tempItem.removeClass('active');
                        tempItem.addClass('added');
                        tempItem.attr("data-indelegationlist", "true");
                        tempItem.attr("data-state", "modified");
                        $('#DelegationList_table tbody').append(item[i]);

                        var tempid = tempItem.attr("data-registrationID");
                        var status = parseInt(tempItem.attr("data-registrationStatus"));
                        bindStatusIconCSS_Delegation(tempid, status);
 
                    }

                    $("#SaveButton").attr("onclick", "saveDelegationRegistrants()");
                    isRowMovedChange = true;
                    bindArrows();
                }
            }
        }

        function removeDelegate()
        {
            validateRemoveDelegate();
            if ($("#RemoveDelegate").hasClass("aspNetDisabled"))
            {
                var message = '';
                if (delegationDisabled == -1)
                {
                    message += 'The selected Members cannot be moved out of the Local Delegation. To move the selected members out of the Local Delegation, please make sure that:';
                    message += '<ol>';
                    message += '<li>They have a non-voting role OR</li>';
                    message += '<li>They are no longer attending (the registration is in Cancelled, Withdraw, Declined status)</li>';
                    message += '</ol>';
                    message += 'Please change their Attending As to a non-voting role first and then retry.';
                    bootstrap_modal("ManageLocalDelegation/ErrorMsg/MoveVotingRoleOutOfDelegation", message);
                }
                return;
            }
            var item = $('#DelegationList_table .registrant-row.active');
            if (item.length > 0) {
                for (var i = 0; i < item.length; i++) {
                    var tempItem = $(item[i]);
                    tempItem.removeClass('active');
                    tempItem.addClass('removed');
                    tempItem.attr("data-indelegationlist", "false");
                    tempItem.attr("data-state", "modified");
                    $('#Registrant_table tbody').append(item[i]);

                    var tempid = tempItem.attr("data-registrationID");
                    var status = parseInt(tempItem.attr("data-registrationStatus"));
                    bindStatusIconCSS_Registrants(tempid, status);
                }

                $("#SaveButton").attr("onclick", "saveDelegationRegistrants()");
                isRowMovedChange = true;
                bindArrows();
            }
        }

        function OpenAttendingAsWindow(val) 
        {
	        var item = $(val).parent().parent().parent().parent();
	        currentData = item.attr("data-attendingasjson");
	        if (typeof currentData == 'string' || currentData instanceof String)
	        {
		        currentData = JSON.parse(currentData);
	        }
	        var registrationId = item.attr("data-registrationID");

	        attendingAsViewModel.registrationId = registrationId;
	        ko.mapping.fromJS(currentData, mapping, attendingAsViewModel);
	
	        $("#attedingAsModal").modal('show');
        }

        function RenderHtmlWindow()
        {
            if (tempAttendingAsHtml == null)
            {
                $.ajax(
                {
                    url: '<%= Page.ResolveUrl("~/Controls/AttendingAsWindow.htm") %>',
                    type: "GET",
                    dataType: "json",
                    async: false,
                    statusCode: {
                        401: function ()
                        {
                        },
                        200: function (request)
                        {
                            tempAttendingAsHtml = request.responseText;
                        }
                    },
                });
            }

            return tempAttendingAsHtml;
        };

        var attendingAsViewModel =
        {
	        registrationId: null,
            ShowHideOtherTextBox: function (element, e) 
	        {
		        if (element.isOther())
		        {
			        var textbox = $(".OtherTextBox", $(e.currentTarget).parent());
			        if (element.isSelected())
			        {
				        textbox.show();
				        textbox.focus();
			        }
			        else
			        {
				        textbox.hide();
				        textbox.val("");
				        element.otherInput = null;
			        }
		        }
		
		        var checkbox = $(e.currentTarget);
		        var val = checkbox.val();
		        var list = currentData.attendingAsList;

		        for (var i = 0; i < list.length; i++)
		        {
			        if (!list[i].isVotingRole)
			        {
				        if (list[i].id == val)
				        {
					        var isChecked = checkbox.is(':checked');
					        list[i].isSelected = isChecked;
					        checkbox.checked = isChecked;
				        }
			        }
		        }
		
		        ko.mapping.fromJS(currentData, mapping, attendingAsViewModel);
		        return true;
            },	
            ValidateOtherContent: function (element, e) 
	        {
                if (element.otherInput() == null || element.otherInput() == "")
		        {
			        element.isSelected(false);
		        }
		        return true;
            },	
            SetVoteRole: function (element, e) 
	        {
                var checkbox = $(e.currentTarget);
		        var val = checkbox.val();
		        var list = currentData.attendingAsList;

		        for (var i = 0; i < list.length; i++)
		        {
			        if (list[i].isVotingRole)
			        {
				        if (list[i].id == val)
				        {
					        var isChecked = checkbox.is(':checked');
					        list[i].isSelected = isChecked;
					        checkbox.checked = isChecked;
				        }
				        else
				        {
					        var chk = $('#' + list[i].id)[0];
					        list[i].isSelected = false;
					        chk.checked = false;
				        }
			        }
		        }
		        ko.mapping.fromJS(currentData, mapping, attendingAsViewModel);
		        return true;
            },
            SaveChanges: function (element, e)
            {
                var dataRow = $("tr[data-registrationid='" + this.registrationId + "']");
                if (dataRow.length > 0) 
		        {
			        var data = ko.mapping.toJS(attendingAsViewModel);
			
                    var tempValue = JSON.stringify(data);
                    dataRow.attr("data-attendingasjson", tempValue);
                    dataRow.attr("data-state", "modified");
                    var tempVoutingStatus = findVotingStatus(tempValue);
                    dataRow.attr("data-votingStatus", tempVoutingStatus);

                    var inDelegationList = dataRow.attr("data-indelegationlist");
                    var status = parseInt(dataRow.attr("data-registrationStatus"));
                    if (inDelegationList === 'true') {
                        bindStatusIconCSS_Delegation(this.registrationId, status);
                        validateRemoveDelegate();
                    } else {
                        bindStatusIconCSS_Registrants(this.registrationId, status);
                        validateAddDelegate();
                    }

                    bindAttedingAs(this.registrationId);
                    $("#attedingAsModal").modal('hide');
                    $("#SaveButton").attr("onclick", "saveDelegationRegistrants()");
                    isAttendingAsChange = true;
                }

                return true;
            },
            attendingAsList: ko.observableArray(),
	        sectionMessage: ko.observable(),
	        showAttendingAsComponent: ko.observable(),
	        showSection: ko.observable(),
	        showSectionMessage: ko.observable()
        };

        function pageLoad(sender, args)
        {
            ko.applyBindings(attendingAsViewModel, $("#attedingAsModal")[0]);
            $("#loadingPanel").show();

            $(document).on('click', '.registrant-row', function (e)
            {
                e.preventDefault();

                var $this = $(this);

                // Detecting ctrl (windows) / meta (mac) key.
                if (e.ctrlKey || e.metaKey)
                {
                    if ($this.hasClass('active'))
                    {
                        $this.removeClass('active');
                    }
                    else
                    {
                        $this.addClass('active');
                    }
                }
                // Detecting shift key
                else if (e.shiftKey)
                {
                    // Get the first possible element that is selected.
                    var currentSelectedIndex = $this.closest("table").find('tr.active').eq(0).index();

                    // Get the shift+click element
                    var selectedElementIndex = $this.closest("table").find('tr').index($this);

                    // Mark selected between them

                    var indexOfRows;
                    if (currentSelectedIndex < selectedElementIndex)
                    {
                        for (indexOfRows = currentSelectedIndex; indexOfRows <= selectedElementIndex; indexOfRows++)
                        {
                            $this.closest("table").find('tr').eq(indexOfRows).addClass('active');
                        }
                    }
                    else
                    {
                        for (indexOfRows = selectedElementIndex; indexOfRows <= currentSelectedIndex; indexOfRows++)
                        {
                            $this.closest("table").find('tr').eq(indexOfRows).addClass('active');
                        }
                    }
                }
                else
                {
                    $this.closest("table").find('tr').removeClass('active');
                    $this.addClass('active');
                }
        
                if ($this.hasClass("added"))
                {
                    $this.removeClass("added");
                }
                if ($this.hasClass("removed"))
                {
                    $this.removeClass("removed");
                }

                if ($this.closest("table").attr("id") == "Registrant_table")
                {
                    validateAddDelegate();
                }

                if ($this.closest("table").attr("id") == "DelegationList_table")
                {
                    validateRemoveDelegate();
                }
            });
            $(document).on('click', '#FilterAllButton', function () {
                bindfilter(0);
            });
            $(document).on('click', '#FilterVotingButton', function () {
                bindfilter(1);

            });

            getEventRegistrationInfo();
        }
        bootstrap_alert = function (val) {
            $('#alert_placeholder').html('<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert">×</a><span>' + getSnippetValue(val) + '</span></div>');
        };
        bootstrap_success = function (val) {
            $('#success_placeholder').html('<div class="alert alert-success fade in"><a class="close" data-dismiss="alert">×</a><span>' + getSnippetValue(val) + '</span></div>');
        };
        bootstrap_modal = function (val, defVal, callback)
        {
            $("#messageWindowBody").html(val ? getSnippetValue(val, defVal) : defVal);
	        if (callback)
	        {
		        $("#messageWindow").find("button").click
		        (
			        function()
			        {
				        callback();
			        }
		        );
	        }
            $("#messageWindow").modal('show');
        };
    </script>
    <script id="template" type="text/x-kendo-template">
        #=target.data('title')#
    </script>

</asp:Content>


