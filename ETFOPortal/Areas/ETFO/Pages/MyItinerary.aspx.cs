﻿using System;
using Site.Pages;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Xrm;
using Site.Helpers;
using Microsoft.Xrm.Sdk.Query;

namespace Site.Areas.ETFO.Pages
{
    public partial class MyItinerary : PortalPage
    {
        public string GridSearchID;
        public string GridSearchPlaceholder;

        protected void Page_Load(object sender, EventArgs e)
        {
            RedirectToLoginIfAnonymous();

            string eventRegId = Request.QueryString["oems_EventRegistrationId"];

            if (!string.IsNullOrEmpty(eventRegId))
            {
                var context = XrmHelper.GetContext();
                Guid oems_EventRegistrationId = new Guid(eventRegId);

                oems_EventRegistration oemsEventRegistration = context.oems_EventRegistrationSet.Where(a => a.oems_EventRegistrationId == oems_EventRegistrationId).Single();
                var oemsEvent = (from evt in context.CreateQuery("oems_event") where evt.GetAttributeValue<Guid>("oems_eventid") == oemsEventRegistration.oems_Event.Id select evt).Single();

                #region General Event Information

                EventName.Text = oemsEvent.GetAttributeValue<string>("oems_eventname");
                StartDate.Text = oemsEvent.GetAttributeValue<DateTime?>("oems_startdate") == null ? "" : oemsEvent.GetAttributeValue<DateTime?>("oems_startdate").Value.ToString();
                EndDate.Text = oemsEvent.GetAttributeValue<DateTime?>("oems_enddate") == null ? "" : oemsEvent.GetAttributeValue<DateTime?>("oems_enddate").Value.ToString();
                RegStatus.Text = oemsEventRegistration.statuscode.HasValue?getRegistrationStatusText(oemsEventRegistration.statuscode.Value):"";
                EventOwner.Text = oemsEvent.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ownerid").Name;

                //Course term
                if (oemsEvent.GetAttributeValue<bool>("oems_courseflag"))
                {
                    CourseTerm.Text = oemsEvent.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("oems_courseterm").Name;
                }
                else
                {
                    CourseTermContainer.Visible = false;
                }

                //Event Location
                var evtLoc = oemsEvent.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("oems_eventlocation");
                if (evtLoc != null)
                {
                    oems_Location eventLocation = (oems_Location)context.Retrieve(evtLoc.LogicalName, evtLoc.Id, new ColumnSet(true));
                    LocationName.Text = eventLocation.oems_LocationName;
                    LocationStreet.Text = eventLocation.oems_AddressLine1;
                    LocationCity.Text = eventLocation.oems_City;
                    LocationPostcode.Text = eventLocation.oems_PostalCode;
                }
                else
                {
                    Location.Visible = false;
                }

                //Accommodation Location
                var evtAccLoc = oemsEvent.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("oems_accommodationlocation");
                if (evtAccLoc != null)
                {
                    oems_Location accommdationLocation = (oems_Location)context.Retrieve(evtAccLoc.LogicalName, evtAccLoc.Id, new ColumnSet(true));
                    AccomoLocationName.Text = accommdationLocation.oems_LocationName;
                    AccomoLocationStreet.Text = accommdationLocation.oems_AddressLine1;
                    AccomoLocationCity.Text = accommdationLocation.oems_City;
                    AccomoLocationPostcode.Text = accommdationLocation.oems_PostalCode;
                }
                else
                {
                    AccommodationLocation.Visible = false;
                }

                //ETFOContact - display multiple ETFO support staff owners' name and phone number
                var staffOwners = (from sOwuners in context.SystemUserSet
                                        join utoe in context.oems_event_support_staff_ownersSet on sOwuners.Id equals utoe.systemuserid
                                   where utoe.oems_eventid.Value == oemsEventRegistration.oems_Event.Id
                                   select sOwuners).ToList();

                if (staffOwners != null && staffOwners.Count() > 0)
                {
                    foreach (SystemUser user in staffOwners)
                    {
                        string name = "";

                        if (!string.IsNullOrEmpty(user.FirstName))
                        {
                            name = user.FirstName;
                        }

                        if (!string.IsNullOrEmpty(user.LastName))
                        {
                            name += " " + user.LastName;
                        }

                        string phone = "";
                        if (!string.IsNullOrEmpty(user.Address1_Telephone1))
                        {
                            phone = user.Address1_Telephone1;
                        }
                        else if (!string.IsNullOrEmpty(user.Address1_Telephone2))
                        {
                            phone = user.Address1_Telephone2;
                        }

                        Panel row = new Panel();
                        row.CssClass = "row";

                        Panel cellName = new Panel();
                        cellName.CssClass = "col-md-3 col-sm-3";
                        Label nameLabel = new Label();
                        nameLabel.Text = name;
                        cellName.Controls.Add(nameLabel);

                        Panel cellTelephone = new Panel();
                        cellTelephone.CssClass = "col-md-3 col-sm-3";
                        Label TelephoneLabel = new Label();
                        TelephoneLabel.Text = phone;
                        cellTelephone.Controls.Add(TelephoneLabel);

                        row.Controls.Add(cellName);
                        row.Controls.Add(cellTelephone);
                        SupportStaffOwnerPanel.Controls.Add(row);
                    }
                }
                else
                {
                    ETFOContact.Visible = false;
                }
                #endregion General Event Information

                #region Course

                if (oemsEvent.GetAttributeValue<bool>("oems_courseflag"))
                {
                    Panel table = new Panel();
                    table.CssClass = "table";

                    CourseDescription.Text = oemsEvent.GetAttributeValue<string>("oems_coursedescription");
                    CourseCode.Text = oemsEvent.GetAttributeValue<string>("oems_coursecode");
                    CourseWeekNumber.Text = oemsEvent.GetAttributeValue<string>("oems_course_week_number");

                    //Presenters
                    var application = (from evt in context.CreateQuery("oems_event")
                                       join coa in context.CreateQuery("oems_eventregistration")
                                            on evt.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("oems_courseofferingapplication").Id 
                                                equals coa.GetAttributeValue<Guid>("oems_eventregistrationid")
                                       where evt.GetAttributeValue<Guid>("oems_eventid") == oemsEventRegistration.oems_Event.Id
                                       select coa).FirstOrDefault();

                    if (application != null)
                    {
                        var presenters = string.Empty;

                        var presenter = application.GetAttributeValue<string>("oems_firstname") + " " + application.GetAttributeValue<string>("oems_lastname");
                        var coPresenter1 = application.GetAttributeValue<string>("oems_copresenter1firstname") + " " + application.GetAttributeValue<string>("oems_copresenter1lastname");
                        var coPresenter2 = application.GetAttributeValue<string>("oems_copresenter2firstname") + " " + application.GetAttributeValue<string>("oems_copresenter2lastname");
                        var coPresenter3 = application.GetAttributeValue<string>("oems_copresenter3firstname") + " " + application.GetAttributeValue<string>("oems_copresenter3lastname");

                        //Presenter
                        if (!string.IsNullOrWhiteSpace(presenter))
                        {
                            presenters = presenter;
                        }

                        //coInstructor1
                        if (!string.IsNullOrWhiteSpace(coPresenter1))
                        {
                            presenters += "</br>" + coPresenter1;
                        }

                        //coInstructor2
                        if (!string.IsNullOrWhiteSpace(coPresenter2))
                        {
                            presenters += "</br>" + coPresenter2;
                        }

                        //coInstructor3
                        if (!string.IsNullOrWhiteSpace(coPresenter3))
                        {
                            presenters += "</br>" + coPresenter3;
                        }
                        CoursePresenters.Text = presenters;
                    }

                    // Course topic
                    var coursetopic = oemsEvent.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("oems_coursetopic");
                    if (coursetopic != null)
                        CourseTopic.Text = coursetopic.Name;
                   
                    //Course target grades
                    var coursetargetgrades = oemsEvent.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("oems_coursetargetgrades");
                    if (coursetargetgrades != null)
                        CourseTargetGrades.Text = coursetargetgrades.Name;
                   
                    //Course location
                    var courselocation = oemsEvent.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("oems_courselocation");
                    if (courselocation != null)
                    {
                        string courseLocationStr = courselocation.Name;
                        var cLocDetail = (from cloc in context.CreateQuery("oems_courselocation") where cloc.GetAttributeValue<Guid>("oems_courselocationid") == courselocation.Id select cloc).FirstOrDefault();
                        
                        //Address
                        if (!string.IsNullOrWhiteSpace(cLocDetail.GetAttributeValue<string>("oems_addressline1")) || !string.IsNullOrWhiteSpace(cLocDetail.GetAttributeValue<string>("oems_addressline1")))
                        {
                            courseLocationStr += " - " + cLocDetail.GetAttributeValue<string>("oems_addressline1") + " " + cLocDetail.GetAttributeValue<string>("oems_addressline2");
                        }

                        //Phone Number
                        if (!string.IsNullOrWhiteSpace(cLocDetail.GetAttributeValue<string>("oems_phone")))
                        {
                            courseLocationStr += " - " + cLocDetail.GetAttributeValue<string>("oems_phone");
                        }

                        CourseLocation.Text = courseLocationStr;
                    }
                   
                    //Course dateslot
                    var coursedateslot = oemsEvent.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("oems_coursedateslot");
                    if (coursedateslot != null)
                        CourseDateSlot.Text = coursedateslot.Name;
                    
                    //Course time slot
                    var coursetimeslot = oemsEvent.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("oems_coursetimeslot");
                    if (coursetimeslot != null)
                        CourseTimeSlot.Text = coursetimeslot.Name;
                    
                    //Course localhost
                    var courselocalhost = oemsEvent.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("oems_courselocalhost");
                    if (courselocalhost != null)
                        CourseLocalHost.Text = courselocalhost.Name;
                }
                else
                {
                    Course.Visible = false;
                }

                #endregion Course

                #region Local Delegation

                AttendingAs.Text = "";
                var attendindAs = oemsEventRegistration.oems_eventregistration_attending_as;
                if (attendindAs != null && attendindAs.Count() > 0)
                {
                    foreach (oems_attendingasoption attendingOption in attendindAs)
                    {
                        if (!string.IsNullOrEmpty(attendingOption.oems_OptionName))
                        {
                            AttendingAs.Text += attendingOption.oems_OptionName + " ";
                        }
                    }
                }
                else
                {
                    LocalDelegation.Visible = false;
                }

                #endregion Local Delegation

                #region Accommodation Details
                ArriveDate.Text = "";
                DepartureDate.Text = "";
                RoomType.Text = "";
                Smoking.Text = "";
                RoomMate.Text = "";

                var accommodationRequests = oemsEventRegistration.oems_eventregistration_to_accommodationrequest;
                if (accommodationRequests != null && accommodationRequests.Count() > 0)
                {
                    DateTime? arrivingDate = null;
                    DateTime? departingDate = null;
                    oems_AccommodationRequest accomodationRequest = accommodationRequests.ElementAt(0);
                    foreach(oems_EventAccommodationSchedule schedule in accomodationRequest.oems_AccommodationRequestSchedule)
                    {
                        if (arrivingDate == null || arrivingDate > schedule.oems_AccommodationDate)
                        {
                            arrivingDate = schedule.oems_AccommodationDate;
                        }

                        if (departingDate == null || departingDate < schedule.oems_AccommodationDate)
                        {
                            departingDate = schedule.oems_AccommodationDate;
                        }
                    }

                    if(arrivingDate != null)
                    {
                        ArriveDate.Text = arrivingDate.ToString();
                    }

                    if(departingDate != null)
                    {
                        DepartureDate.Text = departingDate.ToString();
                    }

                    if (accomodationRequest.oems_RoomType != null)
                    {
                        oems_AccommodationRoomType roomType = (oems_AccommodationRoomType)context.Retrieve(accomodationRequest.oems_RoomType.LogicalName, accomodationRequest.oems_RoomType.Id, new ColumnSet(true));
                        RoomType.Text = roomType.oems_RoomType;
                    }
  
                    Smoking.Text = "No";
                    if(accomodationRequest.oems_SmokingFlag == true)
                    {
                        Smoking.Text = "Yes";
                    }

                    RoomMate.Text = accomodationRequest.oems_ShareRoomWith;
                }
                else
                {
                    AccommodationDetails.Visible = false;
                }

                #endregion Accommodation Details

                #region Caucus

                var caucusRequests = oemsEventRegistration.oems_eventregistration_to_caucusrequest;
                if (caucusRequests != null && caucusRequests.Count() > 0)
                {
                    foreach (oems_CaucusRequest request in caucusRequests)
                    {
                        if(request.oems_EventCaucusSchedule != null)
                        {
                            string name = "";
                            oems_EventCaucus caucus = (oems_EventCaucus)context.Retrieve(request.oems_EventCaucusSchedule.LogicalName, request.oems_EventCaucusSchedule.Id, new ColumnSet(true));
                            name = caucus.oems_CaucusName;

                            string date = "";
                            string time = "";
                            DateTime? startDateTime = caucus.oems_StartDateTime;
                            if(startDateTime != null){
                                date = startDateTime.Value.ToShortDateString();
                                time = startDateTime.Value.ToShortTimeString();
                            }

                            Panel table = new Panel();
                            table.CssClass = "table";

                            Panel row1 = new Panel();
                            row1.CssClass = "row";

                            Panel cellNameTitle = new Panel();
                            cellNameTitle.CssClass = "col-md-3 col-sm-3";
                            Label nameTitleLabel = new Label();
                            nameTitleLabel.Text = "Name: ";
                            cellNameTitle.Controls.Add(nameTitleLabel);

                            Panel cellName = new Panel();
                            cellName.CssClass = "col-md-3 col-sm-3";
                            Label nameLabel = new Label();
                            nameLabel.Text = name;
                            cellName.Controls.Add(nameLabel);

                            row1.Controls.Add(cellNameTitle);
                            row1.Controls.Add(cellName);

                            Panel row2 = new Panel();
                            row2.CssClass = "row";

                            Panel cellDateTitle = new Panel();
                            cellDateTitle.CssClass = "col-md-3 col-sm-3";
                            Label dateTitleLabel = new Label();
                            dateTitleLabel.Text = "Date: ";
                            cellDateTitle.Controls.Add(dateTitleLabel);

                            Panel cellDate = new Panel();
                            cellDate.CssClass = "col-md-2 col-sm-2";
                            Label dateLabel = new Label();
                            dateLabel.Text = date;
                            cellDate.Controls.Add(dateLabel);

                            Panel cellEmpty = new Panel();
                            cellEmpty.CssClass = "col-md-2 col-sm-2";

                            Panel cellTimeitle = new Panel();
                            cellTimeitle.CssClass = "col-md-3 col-sm-3";
                            Label timeTitleLabel = new Label();
                            timeTitleLabel.Text = "Time: ";
                            cellTimeitle.Controls.Add(timeTitleLabel);

                            Panel cellTime = new Panel();
                            cellTime.CssClass = "col-md-2 col-sm-2";
                            Label timeLabel = new Label();
                            timeLabel.Text = time;
                            cellTime.Controls.Add(timeLabel);

                            row2.Controls.Add(cellDateTitle);
                            row2.Controls.Add(cellDate);
                            row2.Controls.Add(cellEmpty);
                            row2.Controls.Add(cellTimeitle);
                            row2.Controls.Add(cellTime);

                            table.Controls.Add(row1);
                            table.Controls.Add(row2);

                            CaucusPanel.Controls.Add(table);
                        }
                    }
                }
                else
                {
                    Caucus.Visible = false;
                }

                #endregion Caucus

                #region Child Care

                var childCareRequests = oemsEventRegistration.oems_eventregistration_to_childcarerequest;
                if (childCareRequests != null && childCareRequests.Count() > 0)
                {
                    foreach (oems_ChildCareRequest request in childCareRequests)
                    {
                        string childName = request.oems_ChildName + " " + request.oems_childlastname;
                        string childAge = request.oems_ChildAge != null ? request.oems_ChildAge.ToString( ): "";
                        string comments = request.oems_PublicNotes;

                        Panel table = new Panel();
                        table.CssClass = "table";

                        Panel row1 = new Panel();
                        row1.CssClass = "row";

                        Panel cellNameTitle = new Panel();
                        cellNameTitle.CssClass = "col-md-3 col-sm-3";
                        Label nameTitleLabel = new Label();
                        nameTitleLabel.Text = "Child Name: ";
                        cellNameTitle.Controls.Add(nameTitleLabel);

                        Panel cellName = new Panel();
                        cellName.CssClass = "col-md-2 col-sm-2";
                        Label nameLabel = new Label();
                        nameLabel.Text = childName;
                        cellName.Controls.Add(nameLabel);

                        Panel cellEmpty = new Panel();
                        cellEmpty.CssClass = "col-md-2 col-sm-2";

                        Panel cellAgeTitle = new Panel();
                        cellAgeTitle.CssClass = "col-md-3 col-sm-3";
                        Label ageTitleLabel = new Label();
                        ageTitleLabel.Text = "Age: ";
                        cellAgeTitle.Controls.Add(ageTitleLabel);

                        Panel cellAge = new Panel();
                        cellAge.CssClass = "col-md-2 col-sm-2";
                        Label ageLabel = new Label();
                        ageLabel.Text = childAge;
                        cellAge.Controls.Add(ageLabel);

                        row1.Controls.Add(cellNameTitle);
                        row1.Controls.Add(cellName);
                        row1.Controls.Add(cellEmpty);
                        row1.Controls.Add(cellAgeTitle);
                        row1.Controls.Add(cellAge);

                        Panel row2 = new Panel();
                        row2.CssClass = "row";

                        Panel cellDateTitle = new Panel();
                        cellDateTitle.CssClass = "col-md-3 col-sm-3";
                        Label dateTitleLabel = new Label();
                        dateTitleLabel.Text = "Dates: ";
                        cellDateTitle.Controls.Add(dateTitleLabel);

                        string schedules = string.Empty;
                        foreach (var schedule in request.oems_ChildCareRequestSchedule)
                        {
                            if(!string.IsNullOrWhiteSpace(schedules))
                            {
                                schedules += "<br />";
                            }

                            schedules += schedule.oems_StartDateTime.Value.ToString() + " To " + schedule.oems_EndDateTime.Value.ToString(); ;
                        }

                        Panel cellDate = new Panel();
                        cellDate.CssClass = "col-md-6 col-sm-6";
                        Label dateLabel = new Label();
                        dateLabel.Text = schedules;
                        cellDate.Controls.Add(dateLabel);

                        row2.Controls.Add(cellDateTitle);
                        row2.Controls.Add(cellDate);


                        Panel row3 = new Panel();
                        row3.CssClass = "row";

                        Panel cellCommentsTitle = new Panel();
                        cellCommentsTitle.CssClass = "col-md-3 col-sm-3";
                        Label CommentsTitleLabel = new Label();
                        CommentsTitleLabel.Text = "Comments: ";
                        cellCommentsTitle.Controls.Add(CommentsTitleLabel);

                        Panel cellComments = new Panel();
                        cellComments.CssClass = "col-md-8 col-sm-8";
                        Label commentsLabel = new Label();
                        commentsLabel.Text = comments;
                        cellComments.Controls.Add(commentsLabel);

                        row3.Controls.Add(cellCommentsTitle);
                        row3.Controls.Add(cellComments);

                        table.Controls.Add(row1);
                        table.Controls.Add(row2);
                        table.Controls.Add(row3);

                        ChildCarePanel.Controls.Add(table);
                    }
                }
                else
                {
                    ChildCare.Visible = false;
                }

                #endregion Child Care

                #region Personal Accommodation

                var personalAcommodationRequests = oemsEventRegistration.oems_eventregistration_to_personalaccommodationrequest;
                if (personalAcommodationRequests != null && personalAcommodationRequests.Count() > 0)
                {
                    var index = 0;
                    foreach (oems_PersonalAccommodationRequest request in personalAcommodationRequests)
                    {
                        index++;
                        string requestStatus = request.statuscode != null ?  EntityOptionSet.GetOptionSetLabel("oems_personalaccommodationrequest", "statuscode", request.statuscode.Value) : "";

                        //Title and request status
                        Panel rowRequest = new Panel();
                        rowRequest.CssClass = "row";

                        Panel cellTitle = new Panel();
                        cellTitle.CssClass = "col-md-3 col-sm-3";
                        Label titleLabel = new Label();
                        titleLabel.Text = "Request " + index.ToString(); ;
                        cellTitle.Controls.Add(titleLabel);

                        Panel cellEmpty = new Panel();
                        cellEmpty.CssClass = "col-md-4 col-sm-4";

                        Panel cellStatusLabelRequest = new Panel();
                        cellStatusLabelRequest.CssClass = "col-md-3 col-sm-3";
                        Label statusLabelRequest = new Label();
                        statusLabelRequest.Text = "Status: ";
                        cellStatusLabelRequest.Controls.Add(statusLabelRequest);

                        Panel cellStatusRequest = new Panel();
                        cellStatusRequest.CssClass = "col-md-2 col-sm-2";
                        Label statusRequest = new Label();
                        statusRequest.Text = requestStatus;
                        cellStatusRequest.Controls.Add(statusRequest);

                        rowRequest.Controls.Add(cellTitle);
                        rowRequest.Controls.Add(cellEmpty);
                        rowRequest.Controls.Add(cellStatusLabelRequest);
                        rowRequest.Controls.Add(cellStatusRequest);

                        PersonalAccommodationPanel.Controls.Add(rowRequest);

                        var acommodationOptions = request.oems_personalaccommodationrequest_to_personalaccommodationrequestoption;
                        if (acommodationOptions != null && acommodationOptions.Count() > 0)
                        {
                            //Options subtitle
                            Panel rowOptionTitle = new Panel();
                            rowOptionTitle.CssClass = "row";

                            Panel cellOptionTitle = new Panel();
                            cellOptionTitle.CssClass = "col-md-2 col-sm-2";
                            Label titleOptionLabel = new Label();
                            titleOptionLabel.Text = "Options: ";
                            cellOptionTitle.Controls.Add(titleOptionLabel);

                            rowOptionTitle.Controls.Add(cellOptionTitle);

                            PersonalAccommodationPanel.Controls.Add(rowOptionTitle);
                            foreach (oems_PersonalAccommodationRequestOption option in acommodationOptions)
                            {
                                string optionName = "";
                                string statusOpt = option.statuscode != null ? EntityOptionSet.GetOptionSetLabel("oems_personalaccommodationrequestoption", "statuscode", option.statuscode.Value) : "";

                                if(option.oems_PersonalAccommodationOption != null)
                                {
                                    oems_PersonalAccommodationOption paOption = (oems_PersonalAccommodationOption)context.Retrieve(option.oems_PersonalAccommodationOption.LogicalName,
                                                                                                                                    option.oems_PersonalAccommodationOption.Id,
                                                                                                                                    new ColumnSet(true));
                                    optionName = paOption.oems_OptionName;
                                }

                                Panel row = new Panel();
                                row.CssClass = "row";

                                Panel cellSpacer = new Panel();
                                cellSpacer.CssClass = "col-md-1 col-sm-1";

                                Panel cellOptionName = new Panel();
                                cellOptionName.CssClass = "col-md-2 col-sm-2";
                                Label optionNameLabel = new Label();
                                optionNameLabel.Text = optionName;
                                cellOptionName.Controls.Add(optionNameLabel);

                                Panel cellStatus = new Panel();
                                cellStatus.CssClass = "col-md-2 col-sm-2";
                                Label statusLabel = new Label();
                                statusLabel.Text = statusOpt;
                                cellStatus.Controls.Add(statusLabel);

                                row.Controls.Add(cellSpacer);
                                row.Controls.Add(cellOptionName);
                                row.Controls.Add(cellStatus);

                                PersonalAccommodationPanel.Controls.Add(row);
                            }
                        }
                    }
                }
                else
                {
                    PersonalAccommodation.Visible = false;
                }

                #endregion Personal Accommodation

                #region Release Time

                //var releaseTimeRequests = oemsEventRegistration.oems_eventregistration_to_releasetimerequest;
                var releaseTimeRequests = oemsEventRegistration.oems_oems_eventregistration_mbr_releasetimetracking_EventRegistration;
                if (releaseTimeRequests != null && releaseTimeRequests.Count() > 0)
                {
                    foreach (mbr_releasetimetracking request in releaseTimeRequests)
                    {
                        Panel row = new Panel();
                        row.CssClass = "row";

                        Panel cellStatusTitle = new Panel();
                        cellStatusTitle.CssClass = "col-md-3 col-sm-3";
                        Label StatusTitleLabel = new Label();
                        StatusTitleLabel.Text = "Status: ";
                        cellStatusTitle.Controls.Add(StatusTitleLabel);

                        Panel cellStatus = new Panel();
                        cellStatus.CssClass = "col-md-2 col-sm-2";
                        Label statusLabel = new Label();
                        statusLabel.Text = request.statecode != null ? EntityOptionSet.GetOptionSetLabel("oems_releasetimerequest", "statuscode", request.statuscode.Value) : ""; ;
                        cellStatus.Controls.Add(statusLabel);

                        Panel cellEmpty = new Panel();
                        cellEmpty.CssClass = "col-md-2 col-sm-2";

                        Panel cellExtraDaysTitle = new Panel();
                        cellExtraDaysTitle.CssClass = "col-md-3 col-sm-3";
                        Label ExtraDaysTitleLabel = new Label();
                        ExtraDaysTitleLabel.Text = "Extra Days: ";
                        cellExtraDaysTitle.Controls.Add(ExtraDaysTitleLabel);

                        Panel cellExtraDays = new Panel();
                        cellExtraDays.CssClass = "col-md-3 col-sm-3";
                        Label extraDaysLabel = new Label();
                        extraDaysLabel.Text = calculateReleaseTimeExtraDays(request).ToString(); ;
                        cellExtraDays.Controls.Add(extraDaysLabel);

                        row.Controls.Add(cellStatusTitle);
                        row.Controls.Add(cellStatus);
                        row.Controls.Add(cellExtraDays);
                        row.Controls.Add(cellExtraDaysTitle);
                        row.Controls.Add(cellExtraDays);

                        ReleaseTimePanel.Controls.Add(row);

                        //School board
                        if (request.oems_SchoolBoard != null && !string.IsNullOrWhiteSpace(request.oems_SchoolBoard.Name))
                        {
                            Panel row1 = new Panel();
                            row1.CssClass = "row";

                            Panel cellSchoolBoardTitle = new Panel();
                            cellSchoolBoardTitle.CssClass = "col-md-3 col-sm-3";
                            Label SchoolBoardTitleLabel = new Label();
                            SchoolBoardTitleLabel.Text = "School board: ";
                            cellSchoolBoardTitle.Controls.Add(SchoolBoardTitleLabel);

                            Panel cellSchoolBoard = new Panel();
                            cellSchoolBoard.CssClass = "col-md-3 col-sm-3";
                            Label scholBoardLabel = new Label();
                            scholBoardLabel.Text = request.oems_SchoolBoard.Name;
                            cellSchoolBoard.Controls.Add(scholBoardLabel);

                            row1.Controls.Add(cellSchoolBoardTitle);
                            row1.Controls.Add(cellSchoolBoard);

                            ReleaseTimePanel.Controls.Add(row1);
                        }

                        //School
                        if (request.mbr_School != null && !string.IsNullOrWhiteSpace(request.mbr_School.Name))
                        {
                            Panel row2 = new Panel();
                            row2.CssClass = "row";

                            Panel cellSchoolTitle = new Panel();
                            cellSchoolTitle.CssClass = "col-md-3 col-sm-3";
                            Label schoolTitleLabel = new Label();
                            schoolTitleLabel.Text = "School: ";
                            cellSchoolTitle.Controls.Add(schoolTitleLabel);

                            Panel cellSchool = new Panel();
                            cellSchool.CssClass = "col-md-3 col-sm-3";
                            Label schoolLabel = new Label();
                            schoolLabel.Text = request.mbr_School.Name;
                            cellSchool.Controls.Add(schoolLabel);


                            row2.Controls.Add(cellSchoolTitle);
                            row2.Controls.Add(cellSchool);

                            ReleaseTimePanel.Controls.Add(row2);
                        }

                        //Local
                        if (request.mbr_Local != null && !string.IsNullOrWhiteSpace(request.mbr_Local.Name))
                        {
                            Panel row3 = new Panel();
                            row3.CssClass = "row";

                            Panel cellLocalTitle = new Panel();
                            cellLocalTitle.CssClass = "col-md-3 col-sm-3";
                            Label localTitleLabel = new Label();
                            localTitleLabel.Text = "Local: ";
                            cellLocalTitle.Controls.Add(localTitleLabel);

                            Panel cellLocal = new Panel();
                            cellLocal.CssClass = "col-md-3 col-sm-3";
                            Label localLabel = new Label();
                            localLabel.Text = request.mbr_Local.Name;
                            cellLocal.Controls.Add(localLabel);
                            
                            row3.Controls.Add(cellLocalTitle);
                            row3.Controls.Add(cellLocal);

                            ReleaseTimePanel.Controls.Add(row3);
                        }

                        //Dates
                        Panel rowDates = new Panel();
                        rowDates.CssClass = "row";

                        Panel cellNotesTitle = new Panel();
                        cellNotesTitle.CssClass = "col-md-3 col-sm-3";
                        Label SchedulesTitleLabel = new Label();
                        SchedulesTitleLabel.Text = "Schedules: ";
                        cellNotesTitle.Controls.Add(SchedulesTitleLabel);

                        Panel cellSchedules = new Panel();
                        cellSchedules.CssClass = "col-md-7 col-sm-7";
                        Label scheduleLabel = new Label();
                        scheduleLabel.Text = request.mbr_Notes;
                        cellSchedules.Controls.Add(scheduleLabel);

                        rowDates.Controls.Add(cellNotesTitle);
                        rowDates.Controls.Add(cellSchedules);

                        ReleaseTimePanel.Controls.Add(rowDates);
                    }
                }
                else
                {
                    ReleaseTime.Visible = false;
                }

                #endregion Release Time

                #region Billing

                var breq = (from br in context.CreateQuery("oems_billingrequest")
                              where (Guid)br["oems_eventregistration"] == oems_EventRegistrationId
                              select br).FirstOrDefault();

                if (breq != null)
                {
                    BillingStatus.Text = EntityOptionSet.GetOptionSetLabel("oems_billingrequest", "oems_paymentstatus", breq.GetAttributeValue<int>("oems_paymentstatus"));
                    BillingAmount.Text = breq.GetAttributeValue<decimal>("oems_totalamountdue").ToString("c");
                }
                else
                {
                    Billing.Visible = false;
                }

                #endregion
            }
        }

        private string getReleaseTimeStatusText(int statusCode)
        {
            string ret = null;

            switch (statusCode)
            {
                case 1:
                    ret = "Submitted";
                    break;

                case 347780000:
                    ret = "Approved";
                    break;
            }

            return ret;
        }

        private string getRegistrationStatusText(int statusCode){
            string ret = null;

            switch(statusCode){
                case 347780004:
                    ret = "Initial";
                    break;
                case 1:
                    ret = "Created";
                    break;
                case 347780001:
                    ret = "Submitted";
                    break;
                case 347780003:
                    ret = "Approved";
                    break;
            }

            return ret;
        }

        private float calculateReleaseTimeExtraDays(mbr_releasetimetracking rt)
        {
            float ret = 0.0f;

            if (rt.oems_ExtraReleaseTimeRequiredFlag == true)
            {
                foreach (oems_ExtraReleaseTimeRequestSchedule schedule in rt.oems_mbr_releasetimetracking_oems_extrareleasetimerequestschedule_ReleaseTimeTracking)
                {
                    int timePeridOption = schedule.oems_ReleaseTimePeriod.Value;
                    switch (timePeridOption)
                    {
                        case 1: //Full Day
                            ret += 1.0f;
                            break;
                        case 2: //Half Day (AM)
                        case 3: //Half Day (PM)
                            ret += 0.5f;
                            break;
                    }
                }
            }

            return ret;
        }

    }
}