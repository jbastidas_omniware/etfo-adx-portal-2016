﻿using Adxstudio.Xrm.Blogs;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Portal;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Site.Areas.ETFO.DataAdapters;
using Site.Helpers;
using Site.Pages;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace Site.Areas.ETFO.Pages
{
    public partial class SubCategoryLandingPage : PortalPage
    {
        private readonly Lazy<IPortalContext> _portal = new Lazy<IPortalContext>(() => PortalCrmConfigurationManager.CreatePortalContext());

        private readonly Xrm.XrmServiceContext _context = XrmHelper.GetContext();

        protected Entity CarouselBlog
        {
            get
            {
                return _context.adx_blogSet.FirstOrDefault(b => b.adx_websiteid.Id == _portal.Value.Website.Id && b.adx_parentpageid.Id == _portal.Value.Entity.Id && b.oems_type == (int) Enums.BlogType.Carousel);
            }
        }

        protected Entity AnnouncementBlog
        {
            get
            {
                return _context.adx_blogSet.FirstOrDefault(b => b.adx_websiteid.Id == _portal.Value.Website.Id && b.adx_parentpageid.Id == _portal.Value.Entity.Id && b.oems_type == (int)Enums.BlogType.Annoucement);
            }
        }

        protected Entity DefaultBlog
        {
            get
            {
                return _context.adx_blogSet.FirstOrDefault(b => b.adx_websiteid.Id == _portal.Value.Website.Id && b.adx_parentpageid.Id == _portal.Value.Entity.Id && b.oems_type == (int)Enums.BlogType.Blog);
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            CarouselPanel.Visible = (CarouselBlog != null);
            AnnouncementPanel.Visible = (AnnouncementBlog != null);
            BlogPanel.Visible = (DefaultBlog != null);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var webPage = _context.Adx_webpageSet.FirstOrDefault(wp => wp.Adx_webpageId == _portal.Value.Entity.Id);

            if (webPage == null) return;

            var upcomingPage = ServiceContext.GetPageBySiteMarkerName(Website, "Upcoming Events");
            var upcomingUrl = new UrlBuilder(ServiceContext.GetUrl(upcomingPage));
            upcomingUrl.QueryString.Set("subcategoryId", webPage.oems_SubCategoryId.Id.ToString());
            HyperLinkUpcoming.NavigateUrl = upcomingUrl;

            var historicalPage = ServiceContext.GetPageBySiteMarkerName(Website, "Historical Events");
            var historicalUrl = new UrlBuilder(ServiceContext.GetUrl(historicalPage));
            historicalUrl.QueryString.Set("subcategoryId", webPage.oems_SubCategoryId.Id.ToString());
            HyperLinkHistorical.NavigateUrl = historicalUrl;
        }

        protected void CreateCarouselDataAdapter(object sender, ObjectDataSourceEventArgs args)
        {
            args.ObjectInstance = new BlogDataAdapter(CarouselBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
        }

        protected void CreateAnnouncementDataAdapter(object sender, ObjectDataSourceEventArgs args)
        {
            args.ObjectInstance = new BlogDataAdapter(AnnouncementBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
        }

        protected void CreateBlogDataAdapter(object sender, ObjectDataSourceEventArgs args)
        {
            args.ObjectInstance = new BlogDataAdapter(DefaultBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
        }

		protected void CreateCarouselAggregateDataAdapter(object sender, ObjectDataSourceEventArgs args)
		{
			var adapter = new ETFOBlogDataAdapter(CarouselBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
			adapter.Type = Enums.BlogType.Carousel;
			args.ObjectInstance = adapter;
		}

		protected void CreateAnnouncementAggregateDataAdapter(object sender, ObjectDataSourceEventArgs args)
		{
			var adapter = new ETFOBlogDataAdapter(AnnouncementBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
			adapter.Type = Enums.BlogType.Annoucement;
			args.ObjectInstance = adapter;
		}

		protected void CreateBlogAggregateDataAdapter(object sender, ObjectDataSourceEventArgs args)
		{
			var adapter = new ETFOBlogDataAdapter(DefaultBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
			adapter.Type = Enums.BlogType.Blog;
			args.ObjectInstance = adapter;
		}

        protected string GetThumbnailUrl(object entityObject)
        {
            var entity = entityObject as Entity;

            if (entity == null) return null;

            var blogItem = _context.adx_blogpostSet.FirstOrDefault(bp => bp.adx_blogpostId == entity.Id);
            
            if (blogItem == null || blogItem.oems_Image == null)
            {
                return @"http://placehold.it/1280x720";
            }

            var webfile = ServiceContext.CreateQuery<Xrm.Adx_webfile>().FirstOrDefault(file => file.Id == blogItem.oems_Image.Id);

            return webfile == null ? @"http://placehold.it/1280x720" : new UrlBuilder(ServiceContext.GetUrl(webfile)).Path;
        }
    }
}