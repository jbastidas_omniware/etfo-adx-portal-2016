﻿using System;
using System.Web.UI.HtmlControls;
using Site.Pages;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using System.Text;
using Site.Areas.ETFO.Library;
using Site.Areas.ETFO.Pages;
using Microsoft.Xrm.Portal;
using Microsoft.Xrm.Client;
using Xrm;
using Site.Helpers;
using Microsoft.Xrm.Portal.Web;

namespace Site.Areas.ETFO.Pages
{
    public partial class HistoricalEvents : PortalPage
    {
        public string GridSearchID;
        public string GridSearchPlaceholder;

        protected void Page_Init(object sender, EventArgs e)
        {
            var allEvents = new EventListings(null, null, true, null, Portal);
            var eventTypes = allEvents.Select("", 1000, 0).Select(et => new { id = et.EventTypeID, name = et.EventTypeName }).Distinct();
            
            EventTypeDropdown.DataSource = eventTypes;
            EventTypeDropdown.DataTextField = "name";
            EventTypeDropdown.DataValueField = "id";
            EventTypeDropdown.DataBind();

            if (!Page.IsPostBack && !String.IsNullOrEmpty(Request.QueryString["subcategoryId"]))
            {
                EventTypeDropdown.SelectedValue = Request.QueryString["subcategoryId"];
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            GridSearchID = "#" + GridSearchText.ClientID;
            GridSearchPlaceholder = ServiceContext.GetSnippetValueByName(Portal.Website, "HistoricalEvents/GridSearchPlaceholder") ?? "search events";
        }

        public void HistoricalEventsDataSource_OnObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            if (e == null)
            {
                return;
            }            
            
            Guid? contactId=null;
            if (Contact != null)
            {
                contactId = this.Contact.Id;
            }
            Guid? subcategory = null;
            
            if (EventTypeDropdown.SelectedValue != "")
            {
                subcategory = new Guid(EventTypeDropdown.SelectedValue);
            }
            var searchText = GridSearchText.Text;
            var eventListings = new EventListings(subcategory, searchText, true, contactId, Portal);

            e.ObjectInstance = eventListings;
        }

        protected void FilterButton_Click(object sender, EventArgs e)
        {            
            EventListDataSource.DataBind();
        }
        
        protected void GridSearchButton_Click(object sender, EventArgs e)
        {
            EventListDataSource.DataBind();
        }

        protected void HistoricalEventsView_OnSorting(Object sender, ListViewSortEventArgs e)
        {
            // Check the sort direction to set the image URL accordingly.
            var cssClass = e.SortDirection == SortDirection.Ascending ? "icon-caret-down rediconcolor" : "icon-caret-up rediconcolor";

            // Check which field is being sorted
            // to set the visibility of the image controls.
            var sortTypeName = (HtmlGenericControl)HistoricalEventsView.FindControl("sortTypeName");
            var sortTitle = (HtmlGenericControl)HistoricalEventsView.FindControl("sortTitle");
            var sortStartDate = (HtmlGenericControl)HistoricalEventsView.FindControl("sortStartDate");
            var sortEndDate = (HtmlGenericControl)HistoricalEventsView.FindControl("sortEndDate");
            var sortEndDeadline = (HtmlGenericControl)HistoricalEventsView.FindControl("sortEndDeadline");

            sortTypeName.Visible = false;
            sortTitle.Visible = false;
            sortStartDate.Visible = false;
            sortEndDate.Visible = false;
            sortEndDeadline.Visible = false;

            switch (e.SortExpression)
            {
                case "EventTypeName":
                    sortTypeName.Visible = true;
                    sortTypeName.Attributes["class"] = cssClass;
                    break;
                case "EventTitle":
                    sortTitle.Visible = true;
                    sortTitle.Attributes["class"] = cssClass;
                    break;
                case "StartDate":
                    sortStartDate.Visible = true;
                    sortStartDate.Attributes["class"] = cssClass;
                    break;
                case "EndDate":
                    sortEndDate.Visible = true;
                    sortEndDate.Attributes["class"] = cssClass;
                    break;
                case "RegistrationDeadline":
                    sortEndDeadline.Visible = true;
                    sortEndDeadline.Attributes["class"] = cssClass;
                    break;

            }
        }

        protected void HistoricalEventsView_DataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var dataItem = (ListViewDataItem)e.Item;
                if (dataItem.DisplayIndex == HistoricalEventsView.EditIndex)
                {
                    var item = e.Item;
                    var el = (EventListing)e.Item.DataItem;

                    //Registration Periods
                    var registrationPeriodsContainer = (HtmlGenericControl)item.FindControl("registrationPeriodsContainer");
                    var registrationPeriods = EventListings.GetRegistrationPeriods(Portal, el.EventId.Value, el.EventStatus.Value);
                    if (!string.IsNullOrWhiteSpace(registrationPeriods))
                    {
                        registrationPeriodsContainer.InnerHtml = registrationPeriods + "<br/>";
                    }

                    //Application Schedules
                    var ApplicationSchedulesContainer = (HtmlGenericControl)item.FindControl("ApplicationSchedulesContainer");
                    var ApplicationSchedules = EventListings.GetEventApplicationSchedules(el.EventId.Value, el.EventStatus.Value);
                    if (!string.IsNullOrWhiteSpace(ApplicationSchedules))
                    {
                        ApplicationSchedulesContainer.InnerHtml = ApplicationSchedules + "<br/>";
                    }
                }
            }
        }

    }
}