﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/WebForms.master"
    CodeBehind="ETFORegistrationValidationLogic.aspx.cs" Inherits="Site.Areas.ETFO.Pages.ETFORegistrationValidationLogic" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>



<asp:Content   ContentPlaceHolderID="Head" runat="server">
</asp:Content>


<asp:Content   ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true">
    <%-- boolean values: 0 || 1; Guids strings default no breckets --%>
    <asp:HiddenField ID="contactId" runat="server"  ClientIDMode="Static"/>
    <asp:HiddenField ID="eventInvitedGuestId" runat="server" ClientIDMode="Static"/>
    <asp:HiddenField ID="Eventid" runat="server" ClientIDMode="Static"/>
    <asp:HiddenField ID="isRegistrationOpen" runat="server" ClientIDMode="Static"/>
    <asp:HiddenField ID="isUserInvitedGuest" runat="server" ClientIDMode="Static"/>
    <asp:HiddenField ID="isRegistered" runat="server" ClientIDMode="Static"/>
    <asp:HiddenField ID="oems_RetrieveEventRegistrationData" runat="server" ClientIDMode="Static"/>
    <asp:HiddenField ID="oems_RetrievePersonalAccommodationData" runat="server" ClientIDMode="Static"/>
    <asp:HiddenField ID="oems_EventRegistrationId" runat="server" ClientIDMode="Static"/>

    <!-- Modal -->
    <div class="modal fade" id="declineRegistration" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">ETFO Registration</h4>
                </div>
                <div class="modal-body">
                    <h4> You are not able to register at this time.</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:window.location.href='<%= Html.SiteMarkerUrl("Home")  %>';">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

        <div class="modal fade" id="AcceptInvitation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
<%--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                    <h4 class="modal-title" id="H1">Accept Invite?</h4>
                </div>
                <div class="modal-body">
                   <p></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="updateGuest()">Decline Invite</button>
                    <button type="button" class="btn btn-primary" onclick="createRegistration()">Accept Invite</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


</asp:Content>


<asp:Content   ContentPlaceHolderID="Scripts" runat="server">    
    <script type="text/javascript">

        var contactId = $('#contactId').val();
        var eventInvitedGuestId = $('#eventInvitedGuestId').val();
        var Eventid = $('#Eventid').val();
        var isRegistrationOpen = $('#isRegistrationOpen').val();
        var isUserInvitedGuest = $('#isUserInvitedGuest').val();
        var isRegistered = $('#isRegistered').val();

        var updateSuccess = 0;
        var createSuccess = 0;

        function createRegistration()
        {
            debugger;

            var p = {};
            p.contactId = contactId;
            p.eventid = Eventid;
            p.oems_EventInvitedGuestId = eventInvitedGuestId;
            p.statusCode = "347780001";

            $.ajax(
            {
                type: "POST",
                complete: function () {},
                url: "/Services/AjaxHelper.svc/createRegistration",
                data: JSON.stringify(p),
                contentType: "application/json",
                dataType: "json",
                success: function (result) {

                    //no need for Jason
                    if (result.result = 'true')
                    {
                        debugger;
                        createSuccess = 1;
                        oems_EventRegistrationId = result.oems_EventRegistrationId;
                        $('#AcceptInvitation').modal('hide');
                    }
                    else
                    {
                        debugger;
                        createSuccess = 2;
                        $('#AcceptInvitation').modal('hide');
                        alert('An error ocurred while processing you request. Please contact support.');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    //debugger;
                    var xhr1 = xhr;
                    var ajaxOptions1 = ajaxOptions;
                    var thrownError1 = thrownError;
                    $('#AcceptInvitation').modal('hide');
                    alert('AJAX error connecting to service.  Please try again or contact support.');
                }
            });
        }


        function updateGuest()
        {
            debugger;
            var p = {};

            p.oems_EventInvitedGuestId = eventInvitedGuestId;
            p.statuscode = 347780002;

            $.ajax(
            {
                type: "POST",
                complete: function () { },
                url: "/Services/AjaxHelper.svc/updateInvitedGuest",
                data: JSON.stringify(p),
                contentType: "application/json",
                dataType: "json",
                success: function (result)
                {
                    if (result)
                    {
                        updateSuccess = 1;
                        $('#AcceptInvitation').modal('hide');
                    }
                    else
                    {
                        updateSuccess = 2;
                        $('#AcceptInvitation').modal('hide');
                        alert('An error ocurred while processing you request. Please call for support');
                    }
                    
                },
                error: function (xhr, ajaxOptions, thrownError)
                {
                    $('#AcceptInvitation').modal('hide');
                    alert('AJAX error connecting to service.  Please try again or contact support.');
                }
            });
        }

        $(function ()
        {
            if (isRegistrationOpen == "0")
            {
                $('#declineRegistration').modal('show');
            }
            else if (isRegistrationOpen == "1" && isUserInvitedGuest == "1")
            {
                $('#AcceptInvitation').modal('show');
            }
            else
            {
                $('#declineRegistration').modal('show');
            }

            $('#declineRegistration').on('hide',function (e)
            {

                // debugger;
                // edit for clarity: "that" will now reference the modal that was hidden
                var that = this;
            });

            $('#AcceptInvitation').on('hide', function (e)
            {
                debugger;
                if (updateSuccess == "2" || createSuccess == "2")
                {
                    window.location.href = "<%= Html.SiteMarkerUrl("Home")  %>";
                }
                if (updateSuccess == "1")
                {
                    window.location.href = "<%= Html.SiteMarkerUrl("Home")  %>";
                }
                if (createSuccess == "1")
                {
                    debugger;

                    //get Json and show registration form
                    // openRegistrationForm(oems_RetrieveEventRegistrationData, oems_RetrievePersonalAccommodationData);
                    window.location.href = "<%= Html.SiteMarkerUrl("Registration Page") %>" + "?oems_EventRegistrationId=" + oems_EventRegistrationId;
                }
            });
        });

        jQuery(document).ready(function ()
        {
            //debugger;
        });

    </script>
</asp:Content>