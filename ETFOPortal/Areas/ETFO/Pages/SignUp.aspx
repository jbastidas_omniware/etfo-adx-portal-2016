﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="Site.Areas.ETFO.Pages.SignUp" ValidateRequest="false" EnableEventValidation="false" ViewStateMode="enabled" %>
<%@ Register src="~/Controls/Comments.ascx" tagname="Comments" tagprefix="adx" %>
<%@ Register src="~/Controls/MultiRatingControl.ascx" tagname="MultiRatingControl" tagprefix="adx" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>


<asp:Content ContentPlaceHolderID="PageHeader" runat="server">
	<%-- This is the only way I could figure out how to make the crm:property render content after postback without making modifications globally in the master page --%>
	<crm:CrmEntityDataSource ID="CurrentHeaderEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<div class="page-header">
		<h1>
			<crm:Property DataSourceID="CurrentHeaderEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
		</h1>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<%-- This is the only way I could figure out how to make the crm:property render content after postback without making modifications globally in the master page --%>
	<crm:CrmEntityDataSource ID="CurrentContentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<crm:Snippet ID="Snippet1" SnippetName="Social Share Widget Code Page Top" EditType="text" DefaultText="" runat="server"/>
	<crm:Property ID="Property1" DataSourceID="CurrentContentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="EntityControls" runat="server" ViewStateMode="Enabled">
	<link href="/css/ETFO/etfo-signup.css" rel="stylesheet" />
	<script type="text/javascript">
		var schoolData;
		var cancelButtonHtml = <%= GetCancelButtonHtml() %>;
	    var cancel = <%= GetCancel() %>;
	    var staffid = <%= GetStaffID() %>;
	    var staffFristName = <%= GetFirstName() %>;
	    var staffLastName = <%= GetLastName() %>;
        var staffEventKey = <%= GetEventKey()%>;

		function LoadSchoolData() {
			$.ajax({
				url: "/services/AjaxHelper.svc/GetSchoolData",
				success: function (data) {
					schoolData = $.parseJSON(data);
					$("#oems_schoolboardid").bind('change', function () { FilterSchools(); });
				}
			});
			//call this once on init.
			//FilterSchools();
		}
		
		function FilterSchools() {
			var schoolboardId = $("#oems_schoolboardid").val();
			
			var schoolDropdown = $("#oems_schoolid");
			schoolDropdown.children().hide();
			
			if (schoolboardId == "") return;

			var filteredList = $.grep(schoolData, function (item) { return item.schoolboardid == schoolboardId; });
			
			//check if currently selected school is in the filteredList
			var selectedSchoolId = schoolDropdown.val();
			var selectedInFilter = $.grep(filteredList, function (item) { return item.schoolid == selectedSchoolId; }).length == 1;
			
			if (!selectedInFilter) {
				schoolDropdown.val(undefined);
			}

			$.each(filteredList, function(index, item) {
				schoolDropdown.children("option[value=" + item.schoolid +"]").show();
			});
		}
		
		jQuery.fn.outerHTML = function (s) {
			return s
				? this.before(s).remove()
				: jQuery("<p>").append(this.eq(0).clone()).html();
		};
		
		function AddCancelButton() {
			var previousButton = $("#PreviousButton.Previous");
			if(previousButton.length > 0) {
				//if this is marked as actually previous (indicated by Previous class), then don't remove it.
				return;
			}

			var button = $("#PreviousButton");
			if(button == undefined) {
				return;
			}

			var parent = button.parent();

			button.remove();

			parent.prepend(cancelButtonHtml);
		}

		function RemoveStaffIDControl()
		{
		    if(!cancel)
		    {
		        var staffidCtrl = $("#oems_staffid");
		        var staffidlbl =$("#oems_staffid_label");
		        var option = $('#oems_nonmembertype').find(":selected").text();
		        var validator = document.getElementById("RequiredFieldValidatoroems_staffid");

		        if (staffidCtrl == undefined || staffidlbl == undefined || validator == undefined || option == "Staff") 
		        {
		            return;
		        }

		        staffidCtrl.hide();
		        staffidlbl.hide();
		        ValidatorEnable(validator, false);
		    }
		    else
		    {
		        cancel=false;
		    }
		}

		function ShowStaffIdControl()
		{
		    var staffidCtrl = $("#oems_staffid");
		    var staffidlbl =$("#oems_staffid_label");
		    var validator = document.getElementById("RequiredFieldValidatoroems_staffid");

		    if(staffidCtrl == undefined || staffidlbl == undefined || validator == undefined)
		    {
		        return;
		    }

		    ValidatorEnable(validator, true); 
		    staffidCtrl.show();
		    staffidlbl.show();
		}
		
		function entityFormClientValidate() {
			// Custom client side validation. Method is called by the submit button's onclick event.
			// Must return true or false. Returning false will prevent the form from submitting.
			return true;
		}
		
		$(document).ready(function ()
		{
			AddCancelButton();
			RemoveStaffIDControl();
			LoadStaffUserData();

			$("time").each(function () 
			{
				var dateTime = Date.parse($(this).attr("datetime"));
				if (dateTime)
				{
					$(this).text(dateTime.toString("MM/dd/yyyy h:mm tt"));
				}
			});

			$.each($("input.signup-password"), function(index)
			{
				var html = $(this).parent().html();
				var parent = $(this).parent();
				parent.children().remove();
				parent.append(html.replace('type="text"', 'type="password"'));

			});
			
			$( "#oems_nonmembertype" ).change(
                function()
                {
                    var selection = $("#oems_nonmembertype option:selected").text();
                    if(selection=="Staff")
                    {
                        ShowStaffIdControl();
                    }
                    else
                    {
                        RemoveStaffIDControl();
                    }
                });
			
			$("input.signup-password-hidden").removeClass("signup-password-hidden");
			
			<%= CallLoadSchoolData() %>
		});
		
	    function webFormClientValidate()
	    {
			// Custom client side validation. Method is called by the next/submit button's onclick event.
			// Must return true or false. Returning false will prevent the form from submitting.
			return true;
		}


		function LoadStaffUserData()
		{
		    if(staffEventKey == undefined) 
		    {
		        return;
		    }

		    if(staffEventKey == "StaffVerification")
		    {
		        var inputStaffid = document.getElementById("oems_staffid");
		        var inputFirstName = document.getElementById("oems_stafffirstname");
		        var inputLastName = document.getElementById("oems_stafflastname");

		        if (inputStaffid == undefined)
		            return;

		        if (inputFirstName == undefined)
		            return;

		        if (inputLastName == undefined)
		            return;

		        inputStaffid.value = staffid;
		        inputFirstName.value = staffFristName;
		        inputLastName.value = staffLastName;

		    }

		}
	</script>
	<asp:Panel runat="server" ID="ErrorPanel" CssClass="errorMessage alert alert-danger alert-block" Visible="False" >
		
	</asp:Panel>
	<adx:WebForm ID="WebFormControl" runat="server" OnSubmit="WebFormControl_Submit" OnItemSaving="WebFormControl_ItemSaving" OnItemSaved="WebFormControl_ItemSaved" OnFormLoad="WebFormControl_FormLoad"  FormCssClass="crmEntityFormView" PreviousButtonCssClass="btn" NextButtonCssClass="btn btn-primary" SubmitButtonCssClass="btn btn-primary" ClientIDMode="Static" Style="display: block; width: 100%; height: auto;" />
    <adx:EntityForm ID="EntityFormControl" runat="server" FormCssClass="crmEntityFormView" PreviousButtonCssClass="btn" NextButtonCssClass="btn btn-primary" SubmitButtonCssClass="btn btn-primary" ClientIDMode="Static" />
	<adx:EntityList ID="EntityListControl" runat="server" ListCssClass="table table-striped" DefaultEmptyListText="There are no items to display." ClientIDMode="Static" />
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<div class="page-metadata clearfix">
		<div class="page-last-updated">
			<crm:Snippet SnippetName="Page Modified On Prefix" DefaultText="This page was last updated" EditType="text" runat="server"/>
			<abbr class="timeago">
				<%: string.Format("{0:r}", Html.AttributeLiteral("modifiedon")) %>
			</abbr>.
			<% var displayDate = Html.AttributeLiteral("adx_displaydate"); %>
			<% if (displayDate != null) { %>
				<crm:Snippet ID="Snippet2" SnippetName="Page Published On Prefix" DefaultText="It was published" EditType="text" runat="server"/>
				<abbr class="timeago">
					<%: string.Format("{0:r}", displayDate) %>
				</abbr>.
			<% } %>
		</div>
		<crm:Snippet SnippetName="Social Share Widget Code Page Bottom" EditType="text" DefaultText="" runat="server"/>
	</div>
	<adx:Comments RatingType="vote" EnableRatings="false" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="SidebarBottom" runat="server">
	<div class="section">
		<adx:MultiRatingControl ID="MultiRatingControl" RatingType="rating" runat="server" />
	</div>
</asp:Content>

