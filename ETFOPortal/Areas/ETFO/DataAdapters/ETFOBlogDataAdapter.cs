﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Adxstudio.Xrm;
using Adxstudio.Xrm.Blogs;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Licensing;
using Adxstudio.Xrm.Tagging;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Site.Areas.ETFO.Library;
using Xrm;
using IDataAdapterDependencies = Adxstudio.Xrm.Blogs.IDataAdapterDependencies;

namespace Site.Areas.ETFO.DataAdapters
{
	/// <summary>
	/// this class overrides the BlogDataAdapter to retrieve featured articles of it's children in addition to its own blog posts.
	/// </summary>
	public class ETFOBlogDataAdapter : BlogDataAdapter
	{
		public Xrm.adx_blog Blog { get; set; }
		public XrmServiceContext ServiceContext { get; set; }
		public Enums.BlogType Type { get; set; }

		public ETFOBlogDataAdapter(Entity blog, IDataAdapterDependencies dependencies)
			: base(blog, dependencies)
		{
			Blog = blog as Xrm.adx_blog;
			ServiceContext = dependencies.GetServiceContext() as XrmServiceContext;
		}

		public override IEnumerable<IBlogPost> SelectPosts()
		{
			return SelectPosts(0, -1);
		}

		
		/// <summary>
		/// Retrieves blog posts for the current blog and its descendents with featured posts
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IBlogPost> SelectPosts(int startRowIndex, int maximumRows)
		{
			if (startRowIndex < 0)
			{
				throw new ArgumentException("Value must be a positive integer.", "startRowIndex");
			}

			if (maximumRows == 0)
			{
				return new IBlogPost[] { };
			}

			//var context = Dependencies.GetServiceContext() as XrmServiceContext;
			var parentPage = ServiceContext.Adx_webpageSet.First(wp => wp.Id == Blog.adx_parentpageid.Id);
			
			var pagesList = new List<Entity>();
			//pagesList.Add(parentPage);

			//get a list of all the descendant pages that this blog belongs to.
			var descendants = GetDescendantPages(parentPage);
			pagesList.AddRange(descendants);

			//for each page in the list, get all their child blogs matching the correct type.
			var blogs = pagesList.SelectMany(page => ((Adx_webpage)page).adx_webpage_blog).Where(blog => blog.oems_type == (int)Type);

			//get all the blog posts from our root blogpost.
			var blogposts = Blog.adx_blog_blogpost;

			//get all featured blog posts from the child blogs
			var featuredBlogPosts = blogs.SelectMany(blog => blog.adx_blog_blogpost.Where(post => post.oems_Feature == true));

			//join these two collections together
			var allPosts = blogposts.Union(featuredBlogPosts);

			//get the current user, filter by published and, if logged in, the posts this user owns.
			var portalUser = Dependencies.GetPortalUser();
			if (portalUser != null && portalUser.LogicalName == "contact")
			{
				allPosts = allPosts.Where(post => post.adx_published == true || post.GetAttributeValue<EntityReference>("adx_authorid") == portalUser);
			}
			else
			{
				allPosts = allPosts.Where(post => post.adx_published == true);
			}

			//skip the starting number.
			if (startRowIndex > 0)
			{
				allPosts = allPosts.Skip(startRowIndex);
			}

			//trim the maximum.
			if (maximumRows > 0)
			{
				allPosts = allPosts.Take(maximumRows);
			}

			//wrap the posts into blogpost objects.
			return CreateBlogPosts(allPosts.OrderByDescending(post => post.adx_date));
		}

		/// <summary>
		/// Gets descendent pages
		/// </summary>
		/// <param name="page"></param>
		/// <returns></returns>
		private IEnumerable<Entity> GetDescendantPages(Entity page)
		{
			var childPages = ServiceContext.GetChildPages(page);

			return childPages.Union(childPages.SelectMany(GetDescendantPages));
		}

		/// <summary>
		/// Takes a list of blog post entities and creates a wrapper blog post object
		/// </summary>
		/// <param name="blogPostEntities"></param>
		/// <returns></returns>
		public IEnumerable<IBlogPost> CreateBlogPosts(IEnumerable<Entity> blogPostEntities)
		{
			//var posts = blogPostEntities.ToArray();
			var postIds = blogPostEntities.Select(e => e.Id).ToArray();

			var context = new Xrm.XrmServiceContext();

			//var serviceContext = Dependencies.GetServiceContext();
			var urlProvider = Dependencies.GetUrlProvider();

			var extendedDatas = ServiceContext.FetchBlogPostExtendedData(postIds, BlogCommentPolicy.Closed, PortalContext.Current.Website.Id);
			var commentCounts = ServiceContext.FetchBlogPostCommentCounts(postIds);

			return blogPostEntities.Select(e =>
			{
				Tuple<string, string, BlogCommentPolicy, string[], IRatingInfo> extendedDataValue;
				var extendedData = extendedDatas.TryGetValue(e.Id, out extendedDataValue)
					? extendedDataValue
					: new Tuple<string, string, BlogCommentPolicy, string[], IRatingInfo>(null, null, BlogCommentPolicy.Closed, new string[] { }, null);

				//var authorReference = e.GetAttributeValue<EntityReference>("adx_authorid");
				var author = new NullBlogAuthor() as IBlogAuthor;

				int commentCountValue;
				var commentCount = commentCounts.TryGetValue(e.Id, out commentCountValue) ? commentCountValue : 0;

				var clone = context.AttachClone(e);
				return new BlogPost(e, urlProvider.GetApplicationPath(context, clone), author, extendedData.Item3, commentCount, new IBlogPostTag[] { });
			}).ToArray();
		}

	}

	internal class NullBlogAuthor : IBlogAuthor
	{
		public ApplicationPath ApplicationPath
		{
			get { return null; }
		}

		public string EmailAddress
		{
			get { return null; }
		}

		public Guid Id
		{
			get { return Guid.Empty; }
		}

		public string Name
		{
			get { return null; }
		}
	}
}