﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Web;
using Adxstudio.Xrm.Web.UI.WebForms;
using AuthorizeNet.APICore;
using Site.Areas.ETFO.RegistrationForm;

namespace Site.Areas.ETFO.RegistrationForm
{
    public static class FormControlHandler
    {
        //=========== Form Control Builder =========== //
        public static List<object> GetControlList()
        {
            var controlsList = new List<object>
            {
                new CheckBoxControl(),
                new TextBoxControl(),
                new TextAreaControl(),
                new DateControl(),
                new DateTimeControl(),
                new LabelControl(),
                new NumberControl(),
                new CurrencyControl()
            };
            return controlsList;
        }

        public static object GetControl(int id)
        {
            // Logic
            return new object();
        }
        public static List<object> GetControl(int[] id)
        {
            // Logic
            return new List<object>();
        }

        public static string GetControl_Json(int id)
        {
            // Logic
            return "{}";
        }
        public static string GetControl_Json(int[] id)
        {
            // Logic
            return "{}";
        }

        //=========== Form Controls (Sub) =========== //
        public static FormTemplate GetDefaultFormTemplate()
        {
            
            return new FormTemplate
            {
                ID = 0,
                Title = "Form Title 1",
                Name = "Enter Form Name",
                Controls = new List<object>()
            };
        }
        public static FormTemplate GetFormTemplate(int formid)
        {
            return new FormTemplate();
        }
        public static List<FormTemplate> GetFormTemplate(int[] formid)
        {
            return new List<FormTemplate>();
        }
        public static string GetFormTemplate_Json(int formid)
        {
            return "{}";
        }

        public static string GetFormTemplate_Json(int[] formid)
        {
            return "{}";
        }

        //=========== RegistrationForm =========== //

        public static RegistrationForm GetRegistrationForm(int regformid)
        {
            return new RegistrationForm();
        }
        public static string GetRegistrationForm_Json(int regformid)
        {
            return "{}";
        }
    }
}