﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Areas.ETFO.RegistrationForm
{
    public class LabelControl : CustomControl
    {

        public string FontFamily { get; set; }
        public int? FontSize { get; set; }
        public int? FontWeight { get; set; }  //700 normal, bold 900
        public string Color { get; set; }
        public string Value { get; set; }
        public bool Member { get; set; }

        public LabelControl()
        {
            ID = 1005;
            Name = "Label";
            Value = "New Label";
            FontSize = 14;
            FontWeight = 400;
            FontFamily = "Arial";
            Color = "#333333";
        }

    }
}