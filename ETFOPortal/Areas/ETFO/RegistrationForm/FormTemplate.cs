﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Areas.ETFO.RegistrationForm
{
    public class FormTemplate
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public List<object> Controls { get; set; }
    }
}