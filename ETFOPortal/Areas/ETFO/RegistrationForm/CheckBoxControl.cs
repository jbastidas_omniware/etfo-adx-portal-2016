﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Areas.ETFO.RegistrationForm
{
    public class CheckBoxControl : CustomControl
    {
        public bool? Value { get; set; }
        public bool? Member { get; set; }
        public bool? Required { get; set; }
        public string Placeholder { get; set; }
        public string Type { get; set; }
        public string ModelEntity { get; set; }

        public CheckBoxControl()
        {
            ID = 1000;
            Name = "CheckBox";
            Required = false;
        }
    }
}