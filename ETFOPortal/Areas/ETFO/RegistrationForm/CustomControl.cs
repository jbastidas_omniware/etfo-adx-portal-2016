﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Areas.ETFO.RegistrationForm
{
    public class CustomControl
    {
        public int ID { get; set; }
        public int UniqueID { get; set; }
        public string Name { get; set; }
    }
}