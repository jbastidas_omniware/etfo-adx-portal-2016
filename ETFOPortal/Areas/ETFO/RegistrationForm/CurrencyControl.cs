﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Areas.ETFO.RegistrationForm
{
    public class CurrencyControl : CustomControl
    {
        public string FontFamily { get; set; }
        public int? FontSize { get; set; }
        public int? FontWeight { get; set; }  //700 normal, bold 900
        public string Color { get; set; }
        public int? Value { get; set; }
        public bool? Member { get; set; }
        public int? Width { get; set; }
        public bool? Required { get; set; }
        public int? MinValue { get; set; }
        public int? MaxValue { get; set; }
        public string Placeholder { get; set; }
        public string Type { get; set; }
        public string ModelEntity { get; set; }

        public CurrencyControl()
        {
            ID = 1007;
            Name = "Currency";
            Required = false;
            
        }
    }
}