﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Areas.ETFO.RegistrationForm
{
    public class TextBoxControl : CustomControl
    {
        public string FontFamily { get; set; }
        public int? FontSize { get; set; }
        public int? FontWeight { get; set; }  //700 normal, bold 900
        public string Color { get; set; }
        public string Value { get; set; }
        public bool? Member { get; set; }
        public int? Width { get; set; }
        public bool? Required { get; set; }
        public int? MaxLength { get; set; }
        public int? MinLength { get; set; }
        public string Placeholder { get; set; }
        public string Type { get; set; }
        public string ModelEntity { get; set; }

        public TextBoxControl()
        {
            ID = 1001;
            Name = "TextBox";
            Required = false;
            FontSize = 14;
            FontWeight = 400;
            FontFamily = "Arial";
            Color = "#333333";
            Member = false;
            Width = 200;
            MaxLength = 9999;
            MinLength = 0;
        }
    }
}