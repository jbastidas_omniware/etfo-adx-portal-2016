﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Areas.ETFO.RegistrationForm
{
    public class HtmlControl : CustomControl
    {
        public string Html { get; set; }
    }
}