﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Areas.ETFO.RegistrationForm
{
    public class RegistrationForm
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public List<FormTemplate> FormTemplates { get; set; }
    }
}