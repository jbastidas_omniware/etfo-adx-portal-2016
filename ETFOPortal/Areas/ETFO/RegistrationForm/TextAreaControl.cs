﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Areas.ETFO.RegistrationForm
{
    public class TextAreaControl : CustomControl
    {

        public string FontFamily { get; set; }
        public int? FontSize { get; set; }
        public int? FontWeight { get; set; }  //700 normal, bold 900
        public string Color { get; set; }
        public string Value { get; set; }
        public bool? Member { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }
        public bool? Required { get; set; }
        public int? Length { get; set; }
        public string Placeholder { get; set; }
        public string Type { get; set; }
        public string ModelEntity { get; set; }

        public TextAreaControl()
        {
            ID = 1002;
            Name = "TextArea";
            Required = false;
        }
    }
}