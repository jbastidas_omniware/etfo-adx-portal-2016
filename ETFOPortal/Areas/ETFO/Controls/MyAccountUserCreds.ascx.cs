﻿using System;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Web.Security;
using Microsoft.Crm.Sdk.Messages;
using Site.Controls;
using Site.Helpers;
using Xrm;

namespace Site.Areas.ETFO.Controls
{
    public partial class MyAccountUserCreds : PortalUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack) return;

            if (!string.IsNullOrEmpty(Contact.oems_PendingUsername))
            {
                UsernameChangePendingLabel.Text = string.Format(XrmHelper.GetSiteSettingValueOrDefault("My Account Username Change Pending Message", "A username change to {0} is already pending."),
                    Contact.oems_PendingUsername);
                UsernameChangePendingLabel.Visible = true;
            }
            UserNameExistingDataLabel.Text = Contact.Adx_username;
            ExistingQuestionDataLabel.Text = HttpUtility.HtmlEncode(Contact.Adx_passwordquestion);
        }

        private void ShowMessage(WebControl panel, ITextControl literal, string msg, string type)
        {
            panel.Visible = true;
            literal.Text = msg;
            panel.CssClass = "alert " + string.Format(" alert-{0}", type);

            ChangeCredentialsUpdateResultPanel.Update();
        }

        protected void ChangeSecurityQuestionButton_OnClick(object sender, EventArgs e)
        {
            ChangeCredentialsResultPanel.Visible = false;
            try
            {
                var membershipProvider = (CrmContactMembershipProvider)Membership.Provider;

                var changeResult = membershipProvider.ChangePasswordQuestionAndAnswer(Contact.Adx_username, SecurityConfirmPassword.Text.Trim(), SecurityQuestionTextBox.Text, Answer.Text.Trim());
                if (changeResult)
                {
                    ShowMessage(ChangeCredentialsResultPanel, ChangeCredentialsResultLiteral, XrmHelper.GetSnippetValueOrDefault("My Account Change Security Question Update Request Success Message",
                    "Your change to your security question and answer has been successful."), "success");

                    return;
                }

                ShowMessage(ChangeCredentialsResultPanel, ChangeCredentialsResultLiteral,
                    XrmHelper.GetSnippetValueOrDefault("My Account Change Security Question Update Request Success Message",
                    "Cannot update your security question and answer because you provided an incorrect password."), "warning");
            }
            catch (Exception x)
            {
                ShowMessage(ChangeCredentialsResultPanel, ChangeCredentialsResultLiteral,
                XrmHelper.GetSnippetValueOrDefault("My Account Change Security Question Update Request Success Message",
                    "There was a problem updating your security question and answer."), "warning");
            }
        }

        protected void ChangeUsernameButton_OnClick(object sender, EventArgs e)
        {
            ChangeCredentialsResultPanel.Visible = false;
            var membershipProvider = (CrmContactMembershipProvider)Membership.Provider;

            // check duplicate username
            if (membershipProvider.GetUser(UsernameNewTextBox.Text, false) != null)
            {
                ShowChangeUsernameError(MembershipCreateStatus.DuplicateUserName);
                return;
            }

            // check duplicate email
            if (membershipProvider.GetUserNameByEmail(UsernameNewTextBox.Text) != null &&
                Contact.EMailAddress1 != UsernameNewTextBox.Text &&
                Contact.oems_PendingUsername != UsernameNewTextBox.Text)
            {
                ShowChangeUsernameError(MembershipCreateStatus.DuplicateEmail);
                return;
            }

            try
            {
                var contact = XrmContext.ContactSet.FirstOrDefault(c => c.ContactId == Contact.Id);

                contact.oems_PendingUsername = UsernameNewTextBox.Text;
                contact.oems_verificationid = Guid.NewGuid().ToString();

                XrmContext.UpdateObject(contact);
                XrmContext.SaveChanges();

                SendEmailChangeVerification(contact);

                UsernameNewTextBox.Text = string.Empty;

                UsernameChangePendingLabel.Text = string.Format(XrmHelper.GetSiteSettingValueOrDefault("My Account Username Change Pending Message", "A username change to {0} is already pending."),
                    contact.oems_PendingUsername);
                UsernameChangePendingLabel.Visible = true;

                ShowMessage(ChangeCredentialsResultPanel, ChangeCredentialsResultLiteral,
                string.Format(XrmHelper.GetSnippetValueOrDefault("My Account Change Username Success Message",
                    "A verification email has been sent to {0}."), contact.oems_PendingUsername), "success");
            }
            catch (Exception ex)
            {
                ShowMessage(ChangeCredentialsResultPanel, ChangeCredentialsResultLiteral,
                XrmHelper.GetSnippetValueOrDefault("My Account Change Username Error Message",
                    "There was a problem changing your username."), "warning");
            }
        }

        protected void ChangePasswordButton_OnClick(object sender, EventArgs e)
        {
            try
            {
                ChangeCredentialsResultPanel.Visible = false;

                var newPassword = NewPassword.Text;
                if (newPassword.Contains(" "))
                {
                    ShowMessage(ChangeCredentialsResultPanel, ChangeCredentialsResultLiteral, XrmHelper.GetSnippetValueOrDefault("My Account Change Password Regex Error Message",
                        "Your password does not meet the minimum requirements. Passwords must not have spaces, must have at least 1 digit, 1 lower case, 1 upper case character and be at least 6 characters long."), "warning");

                    return;
                }

                var passwordRegex = XrmHelper.GetSiteSettingValueOrDefault("PasswordValidationRegex", @"^(?=.*\d)(?=.*[a-z\\s])(?=.*[A-Z\\s]).{6,}$");
                if (!Regex.Match(newPassword, passwordRegex).Success)
                {
                    ShowMessage(ChangeCredentialsResultPanel, ChangeCredentialsResultLiteral, XrmHelper.GetSnippetValueOrDefault("My Account Change Password Regex Error Message",
                        "Your password does not meet the minimum requirements. Passwords must not have spaces, must have at least 1 digit, 1 lower case, 1 upper case character and be at least 6 characters long."), "warning");
                    
                    return;
                }

                var membershipProvider = (CrmContactMembershipProvider)Membership.Provider;
                var oldPassword = CurrentPassword.Text.Trim();
                var changeResult = membershipProvider.ChangePassword(Contact.Adx_username, oldPassword, newPassword);

                if (changeResult)
                {
                    ShowMessage(ChangeCredentialsResultPanel, ChangeCredentialsResultLiteral, XrmHelper.GetSnippetValueOrDefault("My Account Change Password Success Message",
                      "Your password has been changed successfully.."), "success");
                }
                else
                {
                    ShowMessage(ChangeCredentialsResultPanel, ChangeCredentialsResultLiteral, XrmHelper.GetSnippetValueOrDefault("My Account Change Password Invalid Message",
                      "Your current password doesn't match. Please try again"), "warning");
                }
               
            }
            catch (Exception x)
            {
                ShowMessage(ChangeCredentialsResultPanel, ChangeCredentialsResultLiteral,
                XrmHelper.GetSnippetValueOrDefault("My Account Change Passworrd Update Request Error Message",
                    "There was a problem updating your password."), "warning");
            }

        }

        private void SendEmailChangeVerification(Contact c)
        {
            var fromUser = XrmContext.SystemUserSet.FirstOrDefault(u => u.FullName == XrmHelper.GetSiteSettingValueOrDefault("SystemEmail/Defaults/EmailFrom", "Admin User"));
            if (fromUser == null)
                throw new Exception("Error retrieving system user: " + XrmHelper.GetSiteSettingValueOrDefault("SystemEmail/Defaults/EmailFrom", "Admin User") + " from Site Setting: SystemEmail/Defaults/EmailFrom");

            var emailTo = new ActivityParty {AddressUsed = c.oems_PendingUsername};
            var emailFrom = new ActivityParty {PartyId = fromUser.ToEntityReference()};

            var htmlBody = new StringBuilder();

            htmlBody.Append(XrmHelper.GetSnippetValueOrDefault("Account Verification Change Username Email Body Pre",
                "<p>A request to change your account has been made.</p><p>Please click the link below to verify your email address:</p>"));

            var portalUrl = ConfigurationManager.AppSettings["ExternalPortalUrl"];

            htmlBody.AppendFormat("<p><a href='{0}/verify-account?id={1}'>Verify My Account</a></p>", portalUrl, c.oems_verificationid);

            htmlBody.Append(XrmHelper.GetSnippetValueOrDefault("Account Verification Change Username Email Body Post", ""));

            var email = new Email
            {
                To = new [] {emailTo},
                From = new [] {emailFrom},
                Subject = XrmHelper.GetSnippetValueOrDefault("Account Verification Change Username Email Subject",
                        "ETFO Email Address Verification"),
                Description = htmlBody.ToString()
            };

            XrmContext.AddObject(email);
            XrmContext.SaveChanges();

            var trackingTokenEmailRequest = new GetTrackingTokenEmailRequest();
            var trackingTokenEmailResponse = (GetTrackingTokenEmailResponse)XrmContext.Execute(trackingTokenEmailRequest);

            var sendEmailreq = new SendEmailRequest
            {
                EmailId = email.Id,
                TrackingToken = trackingTokenEmailResponse.TrackingToken,
                IssueSend = true
            };

            var sendEmailresp = (SendEmailResponse)XrmContext.Execute(sendEmailreq);
        }

        private void ShowChangeUsernameError(MembershipCreateStatus membershipCreateStatus)
        {
            string msg;
            switch (membershipCreateStatus)
            {
                case MembershipCreateStatus.DuplicateEmail:
                    msg = XrmHelper.GetSnippetValueOrDefault("My Account Change Username Error Duplicate Email Message", "This email address is already in use.");
                    break;
                case MembershipCreateStatus.DuplicateUserName:
                    msg = XrmHelper.GetSnippetValueOrDefault("My Account Change Username Error Duplicate Username Message", "This username/email is already in use.");
                    break;
                default:
                    msg = XrmHelper.GetSnippetValueOrDefault("My Account Change Username Error Default Message", "There was an error changing your username/email.");
                    break;
            }

            ShowMessage(ChangeCredentialsResultPanel, ChangeCredentialsResultLiteral, msg, "warning");
        }
    }
}