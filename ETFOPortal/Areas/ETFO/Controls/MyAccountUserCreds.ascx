﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyAccountUserCreds.ascx.cs" Inherits="Site.Areas.ETFO.Controls.MyAccountUserCreds" %>
<div class="form-horizontal">
    <asp:UpdatePanel ID="ChangeCredentialsUpdateResultPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="ChangeCredentialsResultPanel" runat="server" CssClass="alert" Visible="false">
                <asp:Literal ID="ChangeCredentialsResultLiteral" runat="server"></asp:Literal>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- Username -->
    <div class="my-account-section">
        <h4>
            <crm:Snippet runat="server" DefaultText="Login" SnippetName="My Account Login Title" />
        </h4>
        <asp:UpdatePanel ID="UsernameUpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="my-account-section-content">
                    <p>
                        <asp:Label runat="server" ID="UsernameChangePendingLabel" CssClass="label label-info" Visible="False" />
                    </p>
                    <div class="form-group">
                        <asp:Label runat="server" ID="UserNameExistingLabel" CssClass="control-label col-sm-4" AssociatedControlID="UserNameExistingDataLabel" Text="<%$ Snippet: My Account Change Username Existing Label, Current Username %>" />
                        <div class="controls col-sm-8">
                            <asp:Label runat="server" ID="UserNameExistingDataLabel" CssClass="label-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" ID="UsernameNewLabel" CssClass="control-label col-sm-4 required" AssociatedControlID="UsernameNewTextBox" Text="<%$ Snippet: My Account Change Username New Label, New Username %>" />
                        <div class="controls col-sm-8">
                            <asp:TextBox runat="server" ID="UsernameNewTextBox" CssClass="input-xlarge"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="UsernameNewValidator" runat="server" ControlToValidate="UsernameNewTextBox"
                                ValidationGroup="ChangeUsername" Display="Dynamic" CssClass="help-inline error"
                                Text="<%$ Snippet: Profile/Personal/Email-required, Email is a required field. %>" />
                            <asp:RegularExpressionValidator ID="UsernameNewRegex" runat="server" ControlToValidate="UsernameNewTextBox"
                                ValidationGroup="ChangeUsername" Display="Dynamic" CssClass="help-inline error"
                                Text="<%$ Snippet: EmailFormat, Please enter a valid email address. %>" ValidationExpression="<%$ Snippet: RegexEmail %>" />
                        </div>
                    </div>
                </div>
                <p class="my-account-section-footer">
                    <asp:Button runat="server" ID="ChangeUsernameButton" CausesValidation="True" ValidationGroup="ChangeUsername" CssClass="btn btn-primary btn-small" Text="<%$ Snippet: My Account Change Username Button, Update %>" OnClick="ChangeUsernameButton_OnClick" />
                </p>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <!-- Password -->
    <div class="my-account-section">
        <h4>
            <crm:Snippet runat="server" DefaultText="Password" SnippetName="My Account Password Title" />
        </h4>
        <asp:UpdatePanel ID="PasswordUpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="my-account-section-content">
                    <div class="form-group">
                        <label class="control-label col-sm-4 required" for="CurrentPassword">
                            <crm:Snippet runat="server" SnippetName="Profile Change Password Current Password Label" DefaultText="Current Password" />
                        </label>
                        <div class="controls col-sm-8">
                            <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password" CssClass="input-xlarge"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword" ValidationGroup="ChangePassword" Display="Dynamic" 
                                ErrorMessage="Current Password is a required field." Text="" CssClass="help-inline error"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4 required" for="NewPassword">
                            <crm:Snippet runat="server" SnippetName="Profile Change Password New Password Label" DefaultText="New Password" /></label>
                        <div class="controls col-sm-8">
                            <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" CssClass="input-xlarge"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword" ValidationGroup="ChangePassword" Display="Dynamic" 
                                ErrorMessage="New Password is a required field." Text="" CssClass="help-inline error"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4 required" for="ConfirmNewPassword">
                            <crm:Snippet runat="server" SnippetName="Profile Change Password Confirm New Password Label" DefaultText="Confirm New Password" /></label>
                        <div class="controls col-sm-8">
                            <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password" CssClass="input-xlarge"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword" ValidationGroup="ChangePassword" 
                                Display="Dynamic" ErrorMessage="Confirm New Password is a required field." Text="" CssClass="help-inline error"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" ValidationGroup="ChangePassword" 
                                Display="Dynamic" ErrorMessage="The New Password and Confirm New Password must match." Text="" CssClass="help-inline error" />
                        </div>
                    </div>
                </div>
                <p class="my-account-section-footer">
                    <asp:Button ID="ChangePasswordButton" runat="server" CssClass="btn btn-primary btn-small" Text="<%$ Snippet: My Account Change Security Question Button, Update %>"
                        ValidationGroup="ChangePassword" CausesValidation="true" OnClick="ChangePasswordButton_OnClick" />
                
                </p>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <!-- Security Questions -->
    <div class="my-account-section">
        <h4>
            <crm:Snippet runat="server" DefaultText="Security Question" SnippetName="My Account Security Question Title" />
        </h4>
        <asp:UpdatePanel ID="SecurityUpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="my-account-section-content">
                    <div class="form-group">
                        <asp:Label runat="server" ID="ExistingQuestionLabel" CssClass="control-label col-sm-4" AssociatedControlID="ExistingQuestionDataLabel" 
                            Text="<%$ Snippet: My Account Change Security Question Existing Label, Existing Question %>"></asp:Label>
                        <div class="controls col-sm-8">
                            <asp:Label runat="server" ID="ExistingQuestionDataLabel" CssClass="label-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="SecurityConfirmPassword" CssClass="control-label col-sm-4 required" 
                            Text="<%$ Snippet:  My Account Change Security Question Password Label, Confirm Password %>" />
                        <div class="controls col-sm-8">
                            <asp:TextBox ID="SecurityConfirmPassword" runat="server" CssClass="input-xlarge" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="SecurityConfirmPassword"
                                ValidationGroup="ChangeSecurity" Display="Dynamic" CssClass="help-inline error"
                                Text="<%$ Snippet:  My Account Required Field, This is a required field. %>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="QuestionLabel" runat="server" CssClass="control-label col-sm-4 required" AssociatedControlID="SecurityQuestionTextBox"
                            Text="<%$ Snippet: My Account Change Security Question Label, Security Question %>" />
                        <div class="controls col-sm-8">
                            <asp:TextBox runat="server" ID="SecurityQuestionTextBox" CssClass="input-xlarge" />
                            <asp:RequiredFieldValidator ID="rfvSecurityQuestion" runat="server" ControlToValidate="SecurityQuestionTextBox"
                                ValidationGroup="ChangeSecurity" Display="Dynamic" CssClass="help-inline error" Text="<%$ Snippet:  My Account Required Field, This is a required field. %>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="AnswerLabel" runat="server" CssClass="control-label col-sm-4 required" AssociatedControlID="Answer" Text="<%$ Snippet:  My Account Change Security Question Answer Label, Security Answer %>" />
                        <div class="controls col-sm-8">
                            <asp:TextBox ID="Answer" runat="server" CssClass="input-xlarge"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvAnswer" runat="server" ControlToValidate="Answer"
                                ValidationGroup="ChangeSecurity" Display="Dynamic" CssClass="help-inline error"
                                Text="<%$ Snippet:  My Account Required Field, This is a required field. %>" />
                        </div>
                    </div>
            
                </div>
                <p class="my-account-section-footer">
                    <asp:Button ID="ChangeSecurityQuestionButton" runat="server" CssClass="btn btn-primary btn-small" Text="<%$ Snippet: My Account Change Security Question Button, Update %>"
                        ValidationGroup="ChangeSecurity" CausesValidation="true" OnClick="ChangeSecurityQuestionButton_OnClick" />
                </p>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/css/assets/img/ajax-modal-loading.gif" AlternateText="Loading ..." ToolTip="Loading ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</div>
