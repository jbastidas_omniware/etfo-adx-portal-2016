﻿namespace Site.Areas.ETFO
{
    public class Enums
    {
        public enum BlogType
        {
            Annoucement = 1,
            Blog = 2,
            Carousel = 3,
            MediaRoom = 4
        }

        public enum UpdateRequestType
        {
            Address = 1,
            SelfId = 2,
            PersonalInformation = 3
        }
    }
}
