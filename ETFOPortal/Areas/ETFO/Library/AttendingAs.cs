﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Xrm.Sdk;
using System.ComponentModel;

namespace Site.Areas.ETFO.Library
{

    public class AttendingAs
    {
        public Boolean showSection { get; set; }
        public Boolean showSectionMessage { get; set; }
        public String sectionMessage { get; set; }
        public Boolean showAttendingAsComponent { get; set; }
        public List<AttendingAsOption> attendingAsList { get; set; }
    }

    public class AttendingAsOption
    {
        [DefaultValue(false)]
        public Boolean? isSelected { get; set; }
        public Guid id { get; set; }
        public EntityReference selected { get; set; }
        public String name { get; set; }
        [DefaultValue(false)]
        public Boolean? isVotingRole { get; set; }
        [DefaultValue(false)]
        public Boolean? isGuestRole { get; set; }
        [DefaultValue(false)]
        public Boolean? isOther { get; set; }
        public String otherInput { get; set; }
        public String colour { get; set; }
    }
}