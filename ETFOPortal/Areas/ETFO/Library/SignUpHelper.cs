﻿using System;
using System.Configuration;
using System.Configuration.Provider;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Security;
using Adxstudio.Xrm.Account;
using Adxstudio.Xrm.Services;
using Adxstudio.Xrm.Web.Security;
using Adxstudio.Xrm.Web.UI.WebControls;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Portal.Cms;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Sdk;
using Site.Helpers;
using Xrm;
using log4net;

namespace Site.Areas.ETFO.Library
{

	public enum ContactRecoveryStatus
	{
		Success,
		CannotFindUser,
		UserIsIneligibleForPasswordReset
	}

    public enum PasswordValidationStatus
    {
        Success,
        InvalidPasswordIsEmpty,
        InvalidMinRequiredPasswordLength,
        InvalidMinRequiredNonAlphanumericCharacters,
        InvalidPasswordStrengthRegularExpression,
    }

    public static class SignUpHelper
	{
		private const int SaltLength = 0x10;
		private static readonly string _defaultPasswordRecoveryWorkflowName = "ADX Sign In Password Recovery";
        private static readonly ILog Log = LogManager.GetLogger(typeof(SignUpHelper));

        /// <summary>
        /// Gets the login registration session for the web form session id
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public static Xrm.oems_loginregistrationsession GetLoginSessionBySessionId(Guid sessionId)
		{
			var context = XrmHelper.GetContext();
			var webFormSession = context.adx_webformsessionSet.FirstOrDefault(wfs => wfs.adx_webformsessionId == sessionId);
			
			if(webFormSession == null)
			{
				return null;
			}

			var loginSessionId = new Guid(webFormSession.adx_primaryrecordid);
			return context.oems_loginregistrationsessionSet.FirstOrDefault(lrs => lrs.Id == loginSessionId);
		}

		/// <summary>
		/// Looks up the account for the current session
		/// </summary>
		/// <returns></returns>
		public static Xrm.Contact GetAccountForWebFormSession(Guid sessionId)
		{
			var loginSession = GetLoginSessionBySessionId(sessionId);
			if (loginSession == null)
			{
				throw new Exception(string.Format("Unable to find login session for sessionid '{0}'", sessionId.ToString()));
			}

		    var contact = loginSession.oems_contact_oems_loginregistrationsession_ContactId;

			if (contact == null)
			{
				throw new Exception(string.Format("Unable to find contact for login session '{0}'", loginSession.Id.ToString()));
			}

			return contact;
		}

		/// <summary>
		/// Given the ETFO Member ID, returns the associated account
		/// </summary>
		/// <param name="etfoMemberId"></param>
		/// <returns></returns>
		public static Xrm.Contact GetMemberByEtfoMemberId(string etfoId)
		{
			var context = XrmHelper.GetContext();
			return context.ContactSet.FirstOrDefault(co => co.mbr_ETFOID == etfoId);
		}

        public static bool ValidateSecurityAnswer(Contact contact, string securityAnswer)
        {
            var membershipProvider = (CrmContactMembershipProvider)Membership.Provider;

            try
            {
                var currentPass = membershipProvider.GetPassword(contact.Adx_username, securityAnswer);
                return  !string.IsNullOrWhiteSpace(currentPass);
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        public static MembershipCreateStatus CreateNewMemberLogin(oems_loginregistrationsession loginSession)
		{
            var context = XrmHelper.GetContext();

            var contact = context.AttachClone(loginSession.oems_contact_oems_loginregistrationsession_ContactId);
            var statusCode = contact.StatusCode;
            var stateCode = contact.StateCode;
            var etfoManaged = contact.oems_ETFOManaged;
            var userName = contact.Adx_username;
            var emailAddress = contact.EMailAddress1;
            var logonEnabled = contact.Adx_LogonEnabled;

            try
            {
                Log.Info("Start New member login");
                Log.Info("Contact ID: " + contact.Id);
                Log.Info("Contact Name: " + contact.FullName);

                var membershipProvider = (CrmContactMembershipProvider)Membership.Provider;

                //Validate password
                var decodedPassword = DecodePassword(loginSession.oems_encodedpassword, loginSession.oems_passwordsalt);
                if ((uint)ValidatePassword(decodedPassword, membershipProvider) > 0U)
                {
                    return MembershipCreateStatus.InvalidPassword;
                }

                //Update the contact
                contact.Adx_username = loginSession.oems_username;
                contact.EMailAddress1 = loginSession.oems_username;
                contact.Adx_resetpassword = decodedPassword;
                contact.adx_resetpasswordanswer = loginSession.oems_securityanswer;
                contact.StatusCode = 1; // set status as active
                contact.oems_hasemsaccount = true;
                contact.oems_isnon_member = false;
                contact.oems_ETFOManaged = false;
                contact.Address2_AddressTypeCode = 1;
                contact.Address3_AddressTypeCode = 1;

                // Temporary data used to assign correct password, password question and password answer
                contact.oems_PendingEmailVerificationFlag = false;
                contact.oems_verificationid = string.Empty;
                contact.Adx_LogonEnabled = true;

                XrmHelper.SetStatus(contact.LogicalName, contact.Id, 1, 0); // set state as active 

                Log.Info("New member login: Status updated");

                context.UpdateObject(contact);
                context.SaveChanges();

                Log.Info("New member login: Contact updated");

                // Validate credentials to save current password
                var validated = membershipProvider.ValidateUser(loginSession.oems_username, decodedPassword);
                if(!validated)
                {
                    Log.Info("New member login: no validated user");
                }

                var question = loginSession.oems_securityquestion != null ? loginSession.oems_securityquestion.Trim() : string.Empty;
                var answer = loginSession.oems_securityanswer != null ? loginSession.oems_securityanswer.Trim() : string.Empty;
                var changePasswordAnswer = membershipProvider.ChangePasswordQuestionAndAnswer(loginSession.oems_username, decodedPassword, question, answer);
                if (!changePasswordAnswer)
                {
                    Log.Info("New member login: problem changing question and answer");

                    RevertNewContactChanges(context, contact, statusCode, stateCode, etfoManaged, userName, emailAddress, logonEnabled);
                    return MembershipCreateStatus.ProviderError;
                }

                Log.Info("New member login: Password and answer changed");

                // Update contact data for verificaton step
                contact.oems_PendingEmailVerificationFlag = true;
                contact.Adx_LogonEnabled = false;
                contact.oems_verificationid = Guid.NewGuid().ToString();

                context.UpdateObject(contact);
                context.SaveChanges();

                Log.Info("Sending new member login email");

                //Send the confirmation email
                RunSendVerificationEmailWorkflowForUser(contact);

                Log.Error("Finalize member login");
            }
            catch (Exception ex)
            {
                Log.Error("Error on Creating New Member Login (Message): " + ex.Message);
                if (ex.InnerException != null)
                {
                    Log.Error("Error on Creating New Member Login (Inner Ex Message): " + ex.InnerException.Message);
                }

                RevertNewContactChanges(context, contact, statusCode, stateCode, etfoManaged, userName, emailAddress, logonEnabled);
                Log.Error("Contact reverted");
                throw ex;
            }

            return MembershipCreateStatus.Success;
		}

        private static PasswordValidationStatus ValidatePassword(string password, CrmContactMembershipProvider membershipProvider)
        {
            if (string.IsNullOrEmpty(password))
                return PasswordValidationStatus.InvalidPasswordIsEmpty;
            if (password.Length < membershipProvider.MinRequiredPasswordLength)
                return PasswordValidationStatus.InvalidMinRequiredPasswordLength;
            if (new Regex("\\W").Matches(password).Count < membershipProvider.MinRequiredNonAlphanumericCharacters)
                return PasswordValidationStatus.InvalidMinRequiredNonAlphanumericCharacters;

            if (!string.IsNullOrEmpty(membershipProvider.PasswordStrengthRegularExpression) && !Regex.IsMatch(password, membershipProvider.PasswordStrengthRegularExpression))
                return PasswordValidationStatus.InvalidPasswordStrengthRegularExpression;
            else
                return PasswordValidationStatus.Success;
        }

        private static void RevertNewContactChanges(XrmServiceContext context, Contact contact, int? statusCode, int? stateCode, bool? etfoManaged, string userName, string emailAddress, bool? logonEnabled)
        {
            if (contact != null)
            {
                contact.StatusCode = statusCode;
                contact.oems_ETFOManaged = etfoManaged;
                contact.Adx_resetpassword = string.Empty;
                contact.oems_PendingEmailVerificationFlag = false;
                contact.oems_verificationid = string.Empty;
                contact.oems_hasemsaccount = false;

                contact.Adx_username = userName;
                contact.EMailAddress1 = emailAddress;
                contact.Adx_LogonEnabled = logonEnabled;

                XrmHelper.SetStatus(contact.LogicalName, contact.Id, statusCode.Value, stateCode.Value); // set state as active 
                context.UpdateObject(contact);
                context.SaveChanges();
            }
        }

		public static void RunSendVerificationEmailWorkflowForUser(Contact contact)
		{
            try
            {
                using (var xrmServiceContext = XrmHelper.GetContext())
                {
                    var fromUser = xrmServiceContext.SystemUserSet.FirstOrDefault(u => u.FullName == XrmHelper.GetSiteSettingValueOrDefault("SystemEmail/Defaults/EmailFrom", "Admin User"));
                    if (fromUser == null)
                    {
                        throw new Exception("Error retrieving system user: " + XrmHelper.GetSiteSettingValueOrDefault("SystemEmail/Defaults/EmailFrom", "Admin User") + " from Site Setting: SystemEmail/Defaults/EmailFrom");
                    }

                    var emailTo = new ActivityParty { AddressUsed = contact.EMailAddress1 };
                    var emailFrom = new ActivityParty { PartyId = fromUser.ToEntityReference() };

                    var htmlBody = new StringBuilder();

                    htmlBody.Append(XrmHelper.GetSnippetValueOrDefault("Account Verification  Email Body Pre",
                        "<p>Thank you for registering as a user of the ETFO Event Management System.</p><p>One final step and you will be ready to fully navigate this system that includes information on all of our exciting events.</p>"));

                    var portalUrl = ConfigurationManager.AppSettings["ExternalPortalUrl"];

                    htmlBody.AppendFormat("<p><a href='{0}/verify-account?id={1}'>Verify My Account</a></p>", portalUrl, contact.oems_verificationid);

                    htmlBody.Append(XrmHelper.GetSnippetValueOrDefault("Account Verification Change Username Email Body Post", ""));

                    var email = new Email
                    {
                        To = new[] { emailTo },
                        From = new[] { emailFrom },
                        Subject = XrmHelper.GetSnippetValueOrDefault("Account Verification Change Username Email Subject",
                                "Activate your ETFO EMS Account"),
                        Description = htmlBody.ToString()
                    };

                    xrmServiceContext.AddObject(email);
                    xrmServiceContext.SaveChanges();

                    var trackingTokenEmailRequest = new GetTrackingTokenEmailRequest();
                    var trackingTokenEmailResponse = (GetTrackingTokenEmailResponse)xrmServiceContext.Execute(trackingTokenEmailRequest);

                    var sendEmailreq = new SendEmailRequest
                    {
                        EmailId = email.Id,
                        TrackingToken = trackingTokenEmailResponse.TrackingToken,
                        IssueSend = true
                    };

                    var sendEmailresp = (SendEmailResponse)xrmServiceContext.Execute(sendEmailreq);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error Sending Mail: " + ex.Message);
                throw ex;
            }
        }

        public static void RunSendSucessSignUpEmailWorkflowForUser(Contact contact)
        {

            using (var xrmServiceContext = XrmHelper.GetContext())
            {
                var systemUser = XrmHelper.GetSiteSettingValueOrDefault("SystemEmail/Defaults/EmailFrom", "Admin User");
                var fromUser = xrmServiceContext.SystemUserSet.FirstOrDefault(u => u.FullName == systemUser);
                if (fromUser == null)
                    throw new Exception("Error retrieving system user: " + XrmHelper.GetSiteSettingValueOrDefault("SystemEmail/Defaults/EmailFrom", "Admin User") + " from Site Setting: SystemEmail/Defaults/EmailFrom");

                var emailTo = new ActivityParty { AddressUsed = contact.EMailAddress1 };
                var emailFrom = new ActivityParty { PartyId = fromUser.ToEntityReference() };

                var htmlBody = new StringBuilder();

                htmlBody.Append(XrmHelper.GetSnippetValueOrDefault("Account Verification  Email Body Pre",
                    "<p>Congratulations! You have successfully registered as a user of the ETFO Event Management System. Visit us often at https://events.etfo.org</p>"));

                var email = new Email
                {
                    To = new[] { emailTo },
                    From = new[] { emailFrom },
                    Subject = XrmHelper.GetSnippetValueOrDefault("Account Verification Change Username Email Subject",
                            "Welcome new ETFO Events Management System User"),
                    Description = htmlBody.ToString()
                };

                xrmServiceContext.AddObject(email);
                xrmServiceContext.SaveChanges();

                var trackingTokenEmailRequest = new GetTrackingTokenEmailRequest();
                var trackingTokenEmailResponse = (GetTrackingTokenEmailResponse)xrmServiceContext.Execute(trackingTokenEmailRequest);

                var sendEmailreq = new SendEmailRequest
                {
                    EmailId = email.Id,
                    TrackingToken = trackingTokenEmailResponse.TrackingToken,
                    IssueSend = true
                };
                
                var sendEmailresp = (SendEmailResponse)xrmServiceContext.Execute(sendEmailreq); 
            }
        }

		public static bool SendResetPasswordLink()
		{
			return true;
		}

		public static bool ConfirmUsernameValid(Adxstudio.Xrm.Web.UI.WebControls.WebFormSavingEventArgs e)
		{
			var username = e.Values["oems_username"] as string;

			var context = XrmHelper.GetContext();
			return context.ContactSet.FirstOrDefault(contact => contact.Adx_username == username) == null;
		}

        public static bool ConfirmUsernameMatch(Adxstudio.Xrm.Web.UI.WebControls.WebFormSavingEventArgs e)
        {
            return (e.Values["oems_username"] as string) == (e.Values["oems_confirmusername"] as string);
        }

        public static bool PasswordSecurityCheck(Adxstudio.Xrm.Web.UI.WebControls.WebFormSavingEventArgs e)
		{
			var password = e.Values["oems_password"] as string;

            if (password.Contains(" "))
            {
                return false;
            }

            var passwordRegex = XrmHelper.GetSiteSettingValueOrDefault("PasswordValidationRegex", @"^(?=.*\d)(?=.*[a-z\\s])(?=.*[A-Z\\s]).{6,}$");
            if (!Regex.Match(password, passwordRegex).Success)
            {
                return false;
            }

			return true;
		}

		public static bool ConfirmPasswordsMatch(Adxstudio.Xrm.Web.UI.WebControls.WebFormSavingEventArgs e)
		{
			return (e.Values["oems_password"] as string) == (e.Values["oems_confirmpassword"] as string);
		}

		public static bool EncodePassword(Adxstudio.Xrm.Web.UI.WebControls.WebFormSavingEventArgs e)
		{
			var password = e.Values["oems_password"] as string;

			var salt = GenerateSalt();
			var encodedPassword = EncodePassword(password, salt);
			//var decodedPassword = DecodePassword(encodedPassword, salt);

			e.Values["oems_encodedpassword"] = encodedPassword;
			e.Values["oems_passwordsalt"] = salt;

			//not saving the password
			e.Values["oems_password"] = string.Empty;
			e.Values["oems_confirmpassword"] = string.Empty;

			return true;
		}

		private static string GenerateSalt()
		{
			var data = new byte[SaltLength];
			new RNGCryptoServiceProvider().GetBytes(data);
			return Convert.ToBase64String(data);
		}

		private static string EncodePassword(string password, string salt)
		{
			var encoding = new UTF8Encoding();

			var saltBytes = Convert.FromBase64String(salt);
			var passwordBytes = Encoding.Unicode.GetBytes(password);
			var saltPasswordBytes = new byte[saltBytes.Length + passwordBytes.Length];
			Buffer.BlockCopy(saltBytes, 0, saltPasswordBytes, 0, saltBytes.Length);
			Buffer.BlockCopy(passwordBytes, 0, saltPasswordBytes, saltBytes.Length, passwordBytes.Length);
			return Convert.ToBase64String(encoding.GetBytes(MachineKey.Encode(saltPasswordBytes, MachineKeyProtection.All)));
		}

		private static string DecodePassword(string encodedPassword, string salt)
		{
			var encoding = new UTF8Encoding();

			var saltBytes = Convert.FromBase64String(salt);
			var encodedValue = encoding.GetString(Convert.FromBase64String(encodedPassword));
			var decodedBytes = MachineKey.Decode(encodedValue, MachineKeyProtection.All);

			var passwordBytes = new byte[decodedBytes.Length - saltBytes.Length];
			Buffer.BlockCopy(decodedBytes, saltBytes.Length, passwordBytes, 0, passwordBytes.Length);

			var password = Encoding.Unicode.GetString(passwordBytes);

			return password;
		}

		public static ContactRecoveryStatus RecoverContact(oems_loginregistrationsession loginSession)
		{
			var context = XrmHelper.GetContext();

			var user = Membership.GetUser(loginSession.oems_username);
			if(user == null)
			{
				return ContactRecoveryStatus.CannotFindUser;
			}

			var workflowName = GetPasswordRecoveryWorkflowName();
			var email = ConvertToEmail(user);

			if (user == null || user.IsLockedOut || string.IsNullOrWhiteSpace(email) || !Membership.EnablePasswordReset)
			{
				return ContactRecoveryStatus.UserIsIneligibleForPasswordReset;
			}

            var password = ResetPassword(user, loginSession.oems_securityanswer);
            if (!string.IsNullOrWhiteSpace(password))
            {
                Guid contactId;
                if (TryConvertToContactId(user, out contactId))
                {
                    context.SendPasswordRecoveryEmail(contactId, password, workflowName);
                }
                else
                {
                    SendPasswordRecoveryEmail(user, password);
                }
            }

            return ContactRecoveryStatus.Success;
		}

		private static string ResetPassword(MembershipUser user, string answer = null)
		{
			try
			{
				return user.ResetPassword(answer);
			}
			catch (ArgumentException)
			{
			}
			catch (MembershipPasswordException)
			{
			}
			catch (ProviderException)
			{
			}

			return null;
		}

		private static string GetPasswordRecoveryWorkflowName()
		{
			var portalContext = PortalCrmConfigurationManager.CreatePortalContext();
			var workflowName = portalContext.ServiceContext.GetSiteSettingValueByName(portalContext.Website, "Account/PasswordRecovery/WorkflowName") ?? _defaultPasswordRecoveryWorkflowName;

			using (var context = XrmHelper.GetContext())
			{
				var workflow = context.GetWorkflowByName(workflowName);

				if (workflow == null)
				{
					throw new InvalidOperationException("Password recovery is not configured.");
				}
			}

			return workflowName;
		}

		/// <summary>
		/// this is a simplified version of what is in the account controller, this does not need to assess if the user is an active directory user..
		/// </summary>
		/// <param name="user"></param>
		/// <returns></returns>
		private static string ConvertToEmail(MembershipUser user)
		{
			// add custom handling here to convert a MembershipUser to a valid email
			if (user == null)
			{
				return null;
			}

			return user.Email;
		}

		/// <summary>
		/// this is a simplified version of what is in the account controller.  This does not need to assess active directory.
		/// </summary>
		/// <param name="user"></param>
		/// <param name="contactId"></param>
		/// <returns></returns>
		private static bool TryConvertToContactId(MembershipUser user, out Guid contactId)
		{
			// add custom handling here to convert a MembershipUser to a valid CRM contact ID
			if (user.ProviderUserKey is Guid)
			{
				contactId = (Guid)user.ProviderUserKey;
				return true;
			}

			contactId = default(Guid);
			return false;
		}

		private static void SendPasswordRecoveryEmail(MembershipUser user, string password)
		{
			if (user == null) throw new ArgumentNullException("user");
			if (string.IsNullOrWhiteSpace(password)) throw new ArgumentNullException("password");

			// for custom MembershipProvider implementations where a contact ID does not exist, the password recovery email cannot be sent by workflow
			throw new NotImplementedException("Send a password recovery email.");
		}

		public static bool VerifyAccount(Contact contact)
		{
			var context = XrmHelper.GetContext();
            if (contact == null || contact.ContactId == null)
            {
                return false;
            }

		    var c = new Contact
		    {
		        Id = contact.ContactId.Value,
		        oems_PendingEmailVerificationFlag = false,
		        oems_verificationid = string.Empty,
		        Adx_LogonEnabled = true
		    };

			context.Update(c);
			return true;
		}

		//non member methods
		public static bool ValidateTeacherSchool(WebFormSavingEventArgs e)
		{
			var context = XrmHelper.GetContext();

			var schoolboardid = e.Values["oems_schoolboardid"] as EntityReference;
			if (schoolboardid == null)
			{
				return false;
			}


			var schoolid = e.Values["oems_schoolid"] as EntityReference;
			if (schoolid == null)
			{
				return false;
			}

			var schoolboard = context.mbr_schoolboardSet.FirstOrDefault(sb => sb.Id == schoolboardid.Id);
			if (schoolboard == null)
			{
				return false;
			}

			var school = context.mbr_schoolSet.FirstOrDefault(s => s.Id == schoolid.Id);
			if (school == null)
			{
				return false;
			}

			if(school.mbr_SchoolBoard.Id != schoolboard.Id)
			{
				return false;
			}

			return true;
		}

        public static MembershipCreateStatus CreateNewNonMemberLogin(oems_loginregistrationsession loginSession)
        {
            var context = XrmHelper.GetContext();

            Contact contact = null;
            MembershipCreateStatus status = MembershipCreateStatus.ProviderError;
            try
            {
                Log.Info("Start New non-member login");

                var membershipProvider = (CrmContactMembershipProvider)Membership.Provider;
                var decodedPassword = DecodePassword(loginSession.oems_encodedpassword, loginSession.oems_passwordsalt);
                var user = membershipProvider.CreateUser(loginSession.oems_username, decodedPassword, loginSession.oems_username,
                                                         loginSession.oems_securityquestion, loginSession.oems_securityanswer, true,
                                                         null, out status);

                if (status != MembershipCreateStatus.Success)
                {
                    RevertNewContactNonMemberChanges(context, contact, loginSession.oems_username);
                    return status;
                }

                Guid? contactid = null;
                if (user != null)
                {
                    contactid = (Guid)user.ProviderUserKey;
                    contact = context.ContactSet.FirstOrDefault(c => c.Id == contactid);

                    // Update the contact
                    Log.Info("Update non member contact");
                    contact.oems_FirstName = loginSession.oems_firstname;
                    contact.oems_LastName = loginSession.oems_lastname;
                    contact.FirstName = loginSession.oems_firstname;
                    contact.LastName = loginSession.oems_lastname;
                    contact.oems_PendingEmailVerificationFlag = true;
                    contact.Adx_LogonEnabled = false;
                    contact.oems_verificationid = Guid.NewGuid().ToString();
                    contact.adx_resetpasswordanswer = loginSession.oems_securityanswer;
                    contact.GenderCode = loginSession.oems_Gender;
                    contact.oems_NonMemberType = loginSession.oems_nonmembertype;
                    contact.mbr_collegeid = loginSession.oems_octnumber;
                    contact.oems_NonMemberOrganization = loginSession.oems_Organization;
                    contact.Address1_Line1 = loginSession.oems_addressline1;
                    contact.Address1_Line2 = loginSession.oems_addressline2;
                    contact.Address1_City = loginSession.oems_city;
                    contact.Address1_StateOrProvince = loginSession.oems_province;
                    contact.Address1_Country = loginSession.oems_country;
                    contact.Address1_PostalCode = loginSession.oems_postalcode;
                    contact.Telephone1 = loginSession.oems_workphone;
                    contact.Telephone2 = loginSession.oems_homephone;
                    contact.Telephone3 = loginSession.oems_cellphone;
                    contact.Address2_AddressTypeCode = 1;
                    contact.Address3_AddressTypeCode = 1;
                    contact.Fax = loginSession.oems_fax;
                    contact.oems_isnon_member = true;
                    contact.oems_StaffId = loginSession.oems_StaffID;
                    contact.EMailAddress1 = loginSession.oems_username;
                    contact.mbr_Active = true;

                    Log.Info("Save non member");

                    context.UpdateObject(contact);
                    context.SaveChanges();

                    Log.Info("Sending new non-member login email");

                    RunSendVerificationEmailWorkflowForUser(contact);

                    Log.Error("Finalize non-member login");

                }
            }
            catch (Exception ex)
            {
                Log.Error("Error on Creating New Non Member Login: " + ex.Message);
                if (ex.InnerException != null)
                {
                    Log.Error("Error on Creating New Non Member Login (Inner Ex Message): " + ex.InnerException.Message);
                }

                RevertNewContactNonMemberChanges(context, contact, loginSession.oems_username);
                Log.Error("Contact reverted");
                throw ex;
            }

            return status;
        }

        private static void RevertNewContactNonMemberChanges(XrmServiceContext context, Contact contact, string username)
        {
            if (contact != null)
            {
                context.Delete(contact.LogicalName, contact.Id);
            }
            else
            {
                contact = context.ContactSet.Where(c => c.Adx_username == username).FirstOrDefault();
                if (contact != null)
                {
                    context.Delete(contact.LogicalName, contact.Id);
                }
            }
        }
	}
}