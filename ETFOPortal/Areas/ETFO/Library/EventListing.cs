﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Xrm;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Site.Helpers;
using Xrm;

namespace Site.Areas.ETFO.Library
{
    public class EventListing
    {
        //1: Event
        //2: Course
        //3: Course Term App
        public int ListingType { get; set; }
        
        public Guid? EventId { get; set; }
        public Guid? EventTypeID { get; set; }
        public string EventTypeName { get; set; }
        public string EventTitle { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? RegistrationDeadline { get; set; }
        public DateTime? RegistrationOpen { get; set; }
        public Guid? LocationId { get; set; }
        public string Location { get; set; }
        public int? EventState { get; set; }
        public int? EventStatus { get; set; }
        public int? RegistrationType { get; set; }
        public string EventUrl { get; set; }
        public Guid? EventRegistrationId { get; set; }
        public string EventRegistrationNumber { get; set; }
        public int? EventRegistrationStatus { get; set; }
        public string EventStatusText { get; set; }
        public string EventRegistrationStatusText { get; set; }
        public bool? DisplayExecutiveStaffOwner { get; set; }
        public bool? DisplaySupportStaffOwner { get; set; }
        public bool? LocalDelegationFlag { get; set; }
        public string AttendingAsJson { get; set; }

        public Site.Helpers.RegistrationHelper.MyEventConfiguration EventConfig { get; set; }

        //Courses Info
        public string CourseTitle { get; set; }
        public Guid? TermId { get; set; }
        public string TermName { get; set; }
        public int? TermCode { get; set; }
        public string TermCodeName { get; set; }
        public string CourseCode { get; set; }
        public string Topic { get; set; }
        public DateTime? InitialOferedDate { get; set; }
        public DateTime? FinalOferedDate { get; set; }
        public string TargetGrades { get; set; }
        public string Presenter { get; set; }
        public string CoPresenter1 { get; set; }
        public string CoPresenter2 { get; set; }
        public string CoPresenter3 { get; set; }
        public string InitialDailyTime { get; set; }
        public string FinalDailyTime { get; set; }
        public string CourseDescription { get; set; }
        public List<string> CoursePrices { get; set; }
        public bool? CoursePackageAvailableFlag { get; set; }
        public Guid? CoursePackageDocument { get; set; }
        public EntityReference CourseDateSlot { get; set; }

        private bool registrationMaximumLoaded = false;
        public int? RegistrationMaximum { get; set; }

        private int? totalRegistrations;
        public int TotalRegistrations
        {
            get {
                if (!totalRegistrations.HasValue)
                {
                    using (var localContext = new XrmServiceContext())
                    {
                        IQueryable<oems_EventRegistration> oemsEventRegistrations = (from eventRegs in localContext.oems_EventRegistrationSet
                                                      join ev in localContext.oems_EventSet on eventRegs.oems_Event.Id equals ev.oems_EventId
                                                      where (eventRegs.statuscode == RegistrationHelper.EventRegistration.Sumbitted || eventRegs.statuscode == RegistrationHelper.EventRegistration.Approved) && eventRegs.oems_Event.Id == EventId
                                                      where eventRegs["oems_registrationtype"] == null || ((OptionSetValue)eventRegs["oems_registrationtype"]).Value == RegistrationHelper.EventRegistrationType.Attendee
                                                      select eventRegs);
                        totalRegistrations = oemsEventRegistrations.ToList().Count();
                    }
                }
                return totalRegistrations.Value;            
            }
        }

        public bool IsFull
        {
            get { return TotalRegistrations >= (RegistrationMaximum ?? Int32.MaxValue); }
        }


        public bool? EventLocalDelegationComponentFlag { get; set; }
        public bool WaitingListComponentFlag { get; set; }
        public bool WaitingListFullFlag { get; set; }
        public bool EventFullFlag { get; set; }

        public int? EventRejectionReason { get; set; }
        public string EventRejectionReasonDetails { get; set; }


        public string EventRejectionReasonText {
            get
            {
                return EntityOptionSet.GetOptionSetLabel("oems_eventregistration", "oems_rejectedreason", EventRejectionReason);
            } 
        }
    }
}