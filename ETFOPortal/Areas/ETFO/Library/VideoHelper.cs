﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace Site.Areas.ETFO.Library
{
    /// <summary>
    /// Class VideoHelper
    /// Provides functionality to manage youtube/vimeo videos
    /// </summary>
    public class VideoHelper
    {
        #region Public properties

        /// <summary>
        /// Validate if the url is from youtube/vimeo
        /// </summary>
        /// <param name="videoUrl">Youtube video url</param>
        /// <returns>Returns true if the video is from Youtube/Vimeo otherwise false</returns>
        public bool ValidateUrl(string videoUrl)
        {
            return this.ValidateYouTubeUrl(videoUrl) || this.ValidateVimeoUrl(videoUrl);
        }

        /// <summary>
        /// Gets the video thumbnail
        /// </summary>
        /// <param name="videoUrl">Video urlr</param>
        /// <returns>Returns the video thumbnail</returns>
        public string GetVideoThumbUrl(string videoUrl)
        {
            if (this.ValidateYouTubeUrl(videoUrl))
            {
                return this.GetYouTubeThumb(this.GetYouTubeVideoId(videoUrl));
            }
            else if (this.ValidateVimeoUrl(videoUrl))
            {
                return this.GetVimeoThumb(this.GetVimeoVideoId(videoUrl));
            }
            return "";
        }

        /// <summary>
        /// Gets the video thumbnail
        /// </summary>
        /// <param name="videoUrl">Video urlr</param>
        /// <returns>Returns the video thumbnail</returns>
        public string GetVideoUrl(string videoUrl)
        {
            if (this.ValidateYouTubeUrl(videoUrl))
            {
                return this.GetYouTubeUrl(this.GetYouTubeVideoId(videoUrl));
            }
            else if (this.ValidateVimeoUrl(videoUrl))
            {
                return this.GetVimeoUrl(this.GetVimeoVideoId(videoUrl));
            }
            return "";
        }

        /// <summary>
        /// Gets the video embed
        /// </summary>
        /// <param name="videoUrl">Video Url</param>
        /// <param name="width">Embed width</param>
        /// <param name="height">Embed height</param>
        /// <returns>Returns the video embed</returns>
        public string GetVideoEmbed(string videoUrl, string width, string height)
        {
            if (this.ValidateYouTubeUrl(videoUrl))
            {
                return this.GetYouTubeVideoEmbed(videoUrl, width, height);
            }
            else if (this.ValidateVimeoUrl(videoUrl))
            {
                return this.GetVimeoVideoEmbed(videoUrl, width, height);
            }
            return "";
        }

        /// <summary>
        /// Gets the video identifier
        /// </summary>
        /// <param name="videoUrl">Video videourl</param>
        /// <returns>Returns the video identifier</returns>
        public string GetVideoId(string videoUrl)
        {
            if (this.ValidateYouTubeUrl(videoUrl))
            {
                return this.GetYouTubeVideoId(videoUrl);
            }
            else if (this.ValidateVimeoUrl(videoUrl))
            {
                return this.GetVimeoVideoId(videoUrl);
            }
            return "";
        }

        #endregion

        #region Other properties

        #region Youtube

        /// <summary>
        /// Validate if the url is from youtube
        /// </summary>
        /// <param name="videoUrl">Youtube video url</param>
        /// <returns>Returns true if the video is from Youtube otherwise false</returns>
        public bool ValidateYouTubeUrl(string videoUrl)
        {
            //var regex = new Regex("^[^v]+v.(.{11}).*", RegexOptions.IgnoreCase);
            //return videoUrl.IndexOf("youtube", StringComparison.InvariantCultureIgnoreCase) > -1 && regex.IsMatch(videoUrl);

            var regex1 = new Regex(@"^(https?:\/\/)?(www\.)?youtube.com\/watch\?v=([A-Za-z0-9._%-]*)(\&\S+)?", RegexOptions.IgnoreCase);
            var regex2 = new Regex(@"^(https?:\/\/)?(www\.)?youtu.be\/([A-Za-z0-9._%-]*)(\&\S+)?", RegexOptions.IgnoreCase);
            if (!String.IsNullOrEmpty(videoUrl))
            {
                return regex1.IsMatch(videoUrl) || regex2.IsMatch(videoUrl);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the Youtube video identifier
        /// </summary>
        /// <param name="videoUrl">Youtube videourl</param>
        /// <returns>Returns the Youtube video identifier</returns>
        private string GetYouTubeVideoId(string videoUrl)
        {
            //var regex = new Regex("^[^v]+v.(.{11}).*", RegexOptions.IgnoreCase);
            //return regex.Replace(videoUrl, "$1");

            var regex1 = new Regex(@"^(https?:\/\/)?(www\.)?youtube.com\/watch\?v=([A-Za-z0-9._%-]*)(\&\S+)?", RegexOptions.IgnoreCase);
            var regex2 = new Regex(@"^(https?:\/\/)?(www\.)?youtu.be\/([A-Za-z0-9._%-]*)(\&\S+)?", RegexOptions.IgnoreCase);

            if (regex1.IsMatch(videoUrl))
            {
                return regex1.Replace(videoUrl, "$3");
            }
            else if (regex2.IsMatch(videoUrl))
            {
                return regex2.Replace(videoUrl, "$3");
            }
            return "";
        }

        /// <summary>
        /// Gets the Youtube video thumbnail
        /// </summary>
        /// <param name="videoId">Youtube video identifier</param>
        /// <returns>Returns the Youtube video thumbnail</returns>
        private string GetYouTubeThumb(string videoId)
        {
            try
            {
                //Default Image
                return "http://img.youtube.com/vi/" + videoId + "/0.jpg";
            }
            catch (Exception ex)
            {
                //Log error
                var exception = new Exception("Cannot get youtube thumb - VideoId: " + videoId, ex);

                throw ex;
            }
        }

        /// <summary>
        /// Gets the youtube embed url
        /// </summary>
        /// <param name="videoId">Video Url</param>
        /// <returns>Returns the youtube url</returns>
        private string GetYouTubeUrl(string videoId)
        {
            var url = "//www.youtube.com/embed/{0}?wmode=transparent&autoplay=1";

            return String.Format(url, videoId);
        }

        /// <summary>
        /// Gets the youtube embed
        /// </summary>
        /// <param name="videoUrl">Video Url</param>
        /// <param name="width">Embed width</param>
        /// <param name="height">Embed height</param>
        /// <returns>Returns the youtube embed</returns>
        private string GetYouTubeVideoEmbed(string videoUrl, string width, string height)
        {
            var embed = "<iframe id=\"{0}\" src=\"//www.youtube.com/embed/{1}?wmode=transparent&autoplay=1\" width=\"{2}\" height=\"{3}\" frameborder=\"0\" allowfullscreen></iframe>";

            return String.Format(embed, Guid.NewGuid(), this.GetYouTubeVideoId(videoUrl), width, height);
        }

        #endregion

        #region Vimeo

        /// <summary>
        /// Validate if the url is from youtube
        /// </summary>
        /// <param name="videoUrl">Youtube video url</param>
        /// <returns>Returns true if the video is from Youtube otherwise false</returns>
        public bool ValidateVimeoUrl(string videoUrl)
        {
            if (!String.IsNullOrEmpty(videoUrl))
            {
                var regex = new Regex(@"^(https?:\/\/)?(www.)?vimeo.com\/(\d+)($|\/)", RegexOptions.IgnoreCase);
                return regex.IsMatch(videoUrl);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the Youtube video identifier
        /// </summary>
        /// <param name="url">Youtube videourl</param>
        /// <returns>Returns the Youtube video identifier</returns>
        private string GetVimeoVideoId(string videoUrl)
        {
            var regex = new Regex(@"^(https?:\/\/)?(www.)?vimeo.com\/(\d+)($|\/)", RegexOptions.IgnoreCase);
            return regex.Replace(videoUrl, "$3");
        }

        /// <summary>
        /// Gets the Youtube video thumbnail
        /// </summary>
        /// <param name="videoId">Youtube video identifier</param>
        /// <returns>Returns the Youtube video thumbnail</returns>
        private string GetVimeoThumb(string videoId)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load("http://vimeo.com/api/v2/video/" + videoId + ".xml");
                XmlElement root = doc.DocumentElement;
                string vimeoThumb = root.FirstChild.SelectSingleNode("thumbnail_large").ChildNodes[0].Value;
                return vimeoThumb;
            }
            catch (Exception ex)
            {
                //Log error
                var exception = new Exception("Cannot get vimeo thumb - VideoId: " + videoId, ex);

                throw ex;
            }
        }

        /// <summary>
        /// Gets the vimeo embed url
        /// </summary>
        /// <param name="videoId">Video Url</param>
        /// <returns>Returns the youtube url</returns>
        private string GetVimeoUrl(string videoId)
        {
            var url = "//player.vimeo.com/video/{0}?title=0&byline=0&portrait=0&wmode=transparent&autoplay=1";

            return String.Format(url, videoId);
        }

        /// <summary>
        /// Gets the vimeo embed
        /// </summary>
        /// <param name="videoUrl">Video Url</param>
        /// <param name="width">Embed width</param>
        /// <param name="height">Embed height</param>
        /// <returns>Returns the vimeo embed</returns>
        private string GetVimeoVideoEmbed(string videoUrl, string width, string height)
        {
            var embed = "<iframe src=\"//player.vimeo.com/video/{0}?title=0&byline=0&portrait=0&wmode=transparent&autoplay=1\" width=\"{1}\" height=\"{2}\" frameborder=\"0\" webkitAllowFullScreen allowFullScreen></iframe>";

            return String.Format(embed, this.GetVimeoVideoId(videoUrl), width, height);
        }

        #endregion

        #endregion
    }
}