﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Dynamic;
using Microsoft.Xrm.Portal;
using Microsoft.Xrm.Client;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Adxstudio.Xrm.Data;
using Lucene.Net.Index;
using Site.Helpers;

using Xrm;
using Microsoft.Crm.Sdk.Messages;


namespace Site.Areas.ETFO.Library
{
    public class CourseListings
    {
        #region Fields

        private readonly Guid? courseTermId;

        private readonly string courseTermName;

        private readonly Guid? contactId;

        private readonly string searchText;

        private readonly IPortalContext portal;

        #endregion

        #region Constructors and Destructors

        public CourseListings(Guid? courseTermId, string courseTermName, Guid? contactId, string serchText, IPortalContext portal)
        {
            this.courseTermId = courseTermId;
            this.courseTermName = courseTermName;
            this.contactId = contactId;
            this.searchText = serchText;
            this.portal = portal;
        }

        #endregion

        #region Public Methods and Operators

        public IEnumerable<EventListing> Select(string sortExpression)
        {
            return Select(sortExpression, 20, 0);
        }

        public IEnumerable<EventListing> MySelect(string sortExpression)
        {
            return MySelect(sortExpression, 10, 0);
        }

        public IEnumerable<EventListing> MyApplicationsTermSelect(string sortExpression)
        {
            return MyApplicationsTermSelect(sortExpression, 10, 0);
        }

        public string ValidateStatusText(int? eventStatusCode, int? registrationStatusCode, Guid? eventid)
        {
            if (eventStatusCode == RegistrationHelper.EventStatus.Completed && registrationStatusCode == RegistrationHelper.EventRegistration.Approved)
            {
                using (var serviceContext = new XrmServiceContext())
                {
                    var result = serviceContext.oems_EventAttendeeSet.SingleOrDefault(q => q.oems_EventRegistration.Id == eventid);

                    if (result != null)
                        return EntityOptionSet.GetOptionSetLabel("oems_eventattendee", "statuscode", result.statuscode);
                }
            }

            return EntityOptionSet.GetOptionSetLabel("oems_event", "statuscode", eventStatusCode);
        }

        public IEnumerable<EventListing> Select(string sortExpression, int maximumRows, int startRowIndex)
        {
            if (string.IsNullOrEmpty(sortExpression))
            {
                sortExpression = "InitialOferedDate desc";
            }

            using (var serviceContext = new XrmServiceContext())
            {
                var securityProvider = Microsoft.Xrm.Portal.Configuration.PortalCrmConfigurationManager.CreateCrmEntitySecurityProvider();                
                var query =
                (
                    from evt in serviceContext.CreateQuery("oems_event")
                    join wp in serviceContext.CreateQuery("adx_webpage") on ((EntityReference)evt["oems_webpage"]).Id equals wp["adx_webpageid"]
                    join top in serviceContext.CreateQuery("oems_coursetopic") on ((EntityReference)evt["oems_coursetopic"]).Id equals top["oems_coursetopicid"]
                    join loc in serviceContext.CreateQuery("oems_courselocation") on ((EntityReference)evt["oems_courselocation"]).Id equals loc["oems_courselocationid"]
                    join ds in serviceContext.CreateQuery("oems_coursedateslots") on ((EntityReference)evt["oems_coursedateslot"]).Id equals ds["oems_coursedateslotsid"]
                    join ts in serviceContext.CreateQuery("oems_coursetimeslot") on ((EntityReference)evt["oems_coursetimeslot"]).Id equals ts["oems_coursetimeslotid"]
                    join tg in serviceContext.CreateQuery("oems_targetgrade") on ((EntityReference)evt["oems_coursetargetgrades"]).Id equals tg["oems_targetgradeid"]  // Clean
                    //join coa in serviceContext.CreateQuery("oems_eventregistration") on ((EntityReference)evt["oems_courseofferingapplication"]).Id equals coa["oems_eventregistrationid"]
                    where evt.GetAttributeValue<EntityReference>("oems_courseterm") != null && evt.GetAttributeValue<EntityReference>("oems_courseterm").Id == this.courseTermId
                    where evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.PublishedApplication
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.OpenApplication
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.CloseApplication
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.PublishedRegistration
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.OpenRegistration
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.CloseRegistration
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.StartEvent
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.EndEvent
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.ExceededCapacity
                    select new
                    {
                        WebPage = wp,
                        //Application = coa,
                        Course = new EventListing()
                        {
                            EventId = evt.GetAttributeValue<Guid>("oems_eventid"),
                            EventTitle = evt.GetAttributeValue<string>("oems_eventname"), // Set for the course title
                            CourseDescription = !string.IsNullOrWhiteSpace(evt.GetAttributeValue<string>("oems_coursedescription")) ? evt.GetAttributeValue<string>("oems_coursedescription").Replace(Environment.NewLine, "<br />").Replace("\n", "<br />") : string.Empty,
                            EventStatus = evt.GetAttributeValue<int?>("statuscode"),
                            EventUrl = GetCourseURL(wp),
                            Topic = top.GetAttributeValue<string>("oems_name"),
                            Location = loc.GetAttributeValue<string>("oems_name"),
                            InitialOferedDate = ds.GetAttributeValue<DateTime>("oems_startdate"),
                            FinalOferedDate = ds.GetAttributeValue<DateTime>("oems_enddate"),
                            CourseCode = evt.GetAttributeValue<string>("oems_coursecode"),
                            TargetGrades = tg.GetAttributeValue<string>("oems_name"), // Clean
                            //Presenter = coa.GetAttributeValue<string>("oems_firstname") + " " + coa.GetAttributeValue<string>("oems_lastname"),
                            CoPresenter1 = string.Empty, //coa.GetAttributeValue<string>("oems_copresenter1firstname") + " " + coa.GetAttributeValue<string>("oems_copresenter1lastname"),
                            CoPresenter2 = string.Empty, //coa.GetAttributeValue<string>("oems_copresenter2firstname") + " " + coa.GetAttributeValue<string>("oems_copresenter2lastname"),
                            CoPresenter3 = string.Empty, //coa.GetAttributeValue<string>("oems_copresenter3firstname") + " " + coa.GetAttributeValue<string>("oems_copresenter3lastname"),
                            InitialDailyTime = ts.GetAttributeValue<string>("oems_starttime"),
                            FinalDailyTime = ts.GetAttributeValue<string>("oems_endtime"),
                            DisplaySupportStaffOwner = evt.GetAttributeValue<bool>("oems_displaysupportstaffowners"),
                            DisplayExecutiveStaffOwner = evt.GetAttributeValue<bool>("oems_displayexecutivestaffowners"),
                            WaitingListComponentFlag = evt.GetAttributeValue<bool>("oems_waitinglistcomponentflag"),
                            WaitingListFullFlag = evt.GetAttributeValue<bool>("oems_waitinglistfullflag"),
                            EventFullFlag = evt.GetAttributeValue<bool>("oems_eventfullflag"),
                            RegistrationMaximum = evt.GetAttributeValue<int>("oems_registrationmaximum"),
                            //EventStatusText = EntityOptionSet.GetOptionSetLabel("oems_event", "statuscode", r.EventLocation.Event.statuscode)
                        }
                    }
                ).ToList();

                if (!string.IsNullOrEmpty(searchText))
                {
                    query = query.Where(courses => courses.Course.EventTitle.ToLower().Contains(searchText.ToLower())).ToList();
                }

                // Clean
                var filteredresults = query.Where(p =>
                {
                    p.WebPage.Id = p.WebPage.GetAttributeValue<Guid>("adx_webpageid");
                    return securityProvider.TryAssert(serviceContext, p.WebPage, Microsoft.Xrm.Client.Security.CrmEntityRight.Read);
                });

                //var filteredresults = query.ToList();
                return (filteredresults.Select(r => r.Course).OrderBy(sortExpression).Skip(startRowIndex).Take(maximumRows));
            }
        }

        public IEnumerable<EventListing> MySelect(string sortExpression, int maximumRows, int startRowIndex)
        {
            if (string.IsNullOrEmpty(sortExpression))
            {
                sortExpression = "InitialOferedDate desc";
            }

            using (var serviceContext = new XrmServiceContext())
            {
                var securityProvider = Microsoft.Xrm.Portal.Configuration.PortalCrmConfigurationManager.CreateCrmEntitySecurityProvider();
                var query =
                    (
                        from reg in serviceContext.CreateQuery("oems_eventregistration")
                        join evt in serviceContext.CreateQuery("oems_event") on ((EntityReference)reg["oems_event"]).Id equals evt["oems_eventid"]
                        join trm in serviceContext.CreateQuery("oems_event") on ((EntityReference)evt["oems_courseterm"]).Id equals trm["oems_eventid"]
                        join con in serviceContext.CreateQuery("contact") on ((EntityReference)reg["oems_account"]).Id equals ((EntityReference)con["parentcustomerid"]).Id
                        join wp in serviceContext.CreateQuery("adx_webpage") on ((EntityReference)evt["oems_webpage"]).Id equals wp["adx_webpageid"]
                        join top in serviceContext.CreateQuery("oems_coursetopic") on ((EntityReference)evt["oems_coursetopic"]).Id equals top["oems_coursetopicid"]
                        join loc in serviceContext.CreateQuery("oems_courselocation") on ((EntityReference)evt["oems_courselocation"]).Id equals loc["oems_courselocationid"]
                        join pre in serviceContext.CreateQuery("oems_eventattendee") on evt["oems_eventid"] equals ((EntityReference)pre["oems_event"]).Id
                        join ds in serviceContext.CreateQuery("oems_coursedateslots") on ((EntityReference)evt["oems_coursedateslot"]).Id equals ds["oems_coursedateslotsid"]
                        join ts in serviceContext.CreateQuery("oems_coursetimeslot") on ((EntityReference)evt["oems_coursetimeslot"]).Id equals ts["oems_coursetimeslotid"]
                        where con.GetAttributeValue<Guid>("contactid") == this.contactId
                        where evt.GetAttributeValue<EntityReference>("oems_courseterm") != null //&& evt.GetAttributeValue<EntityReference>("oems_courseterm").Id == this.courseTermId
                        where !pre.GetAttributeValue<bool>("oems_copresenter")
                        where reg.GetAttributeValue<int>("statuscode") != RegistrationHelper.EventRegistration.Initial
                        select new
                        {
                            WebPage = wp,
                            Course = new EventListing()
                            {
                                EventId = evt.GetAttributeValue<Guid>("oems_eventid"),
                                EventTitle = evt.GetAttributeValue<string>("oems_eventname"),
                                EventStatus = (evt.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                                EventState = (evt.GetAttributeValue<OptionSetValue>("statecode")).Value,
                                TermId = trm.GetAttributeValue<Guid>("oems_eventid"),
                                TermName = trm.GetAttributeValue<string>("oems_eventname"),
                                StartDate = evt.GetAttributeValue<DateTime>("oems_startdate"),
                                EndDate = evt.GetAttributeValue<DateTime>("oems_enddate"),
                                RegistrationType = reg.GetAttributeValue<OptionSetValue>("oems_registrationtype") != null ? (reg.GetAttributeValue<OptionSetValue>("oems_registrationtype")).Value : RegistrationHelper.EventRegistrationType.Attendee,
                                RegistrationOpen = evt.GetAttributeValue<DateTime?>("oems_registrationopening"),
                                RegistrationDeadline = evt.GetAttributeValue<DateTime?>("oems_registrationdeadline"),
                                RegistrationMaximum = evt.GetAttributeValue<int>("oems_registrationmaximum"),
                                EventUrl = GetCourseURL(wp),
                                EventRegistrationNumber = reg.GetAttributeValue<string>("oems_registrationnumber"),
                                EventRegistrationId = reg.GetAttributeValue<Guid>("oems_eventregistrationid"),
                                EventStatusText = ValidateStatusText((evt.GetAttributeValue<OptionSetValue>("statuscode")).Value, (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value, evt.Id),
                                EventRegistrationStatus = (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                                EventRegistrationStatusText = EntityOptionSet.GetOptionSetLabel("oems_eventregistration", "statuscode", (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value),
                                EventConfig = Site.Helpers.RegistrationHelper.MyEventHelper(reg.Id, portal),
                                EventLocalDelegationComponentFlag = evt.GetAttributeValue<bool>("oems_localdelegationcomponentflag"),
                                WaitingListComponentFlag = evt.GetAttributeValue<bool>("oems_waitinglistcomponentflag"),
                                WaitingListFullFlag = evt.GetAttributeValue<bool>("oems_waitinglistfullflag"),
                                EventFullFlag = evt.GetAttributeValue<bool>("oems_eventfullflag"),
                                EventRejectionReason = reg.GetAttributeValue<int>("oems_rejectedreason"),
                                EventRejectionReasonDetails = reg.GetAttributeValue<string>("oems_rejectedreasondetails"),
                                DisplaySupportStaffOwner = evt.GetAttributeValue<bool>("oems_displaysupportstaffowners"),
                                DisplayExecutiveStaffOwner = evt.GetAttributeValue<bool>("oems_displayexecutivestaffowners"),
                                LocalDelegationFlag = evt.GetAttributeValue<bool>("oems_localdelegationcomponentflag"),
                                AttendingAsJson = reg.GetAttributeValue<string>("oems_updateattendingasdata"),
                                // ***** 
                                //Topic = top.GetAttributeValue<string>("oems_name"),
                                //Location = loc.GetAttributeValue<string>("oems_locationdescription"),
                                //InitialOferedDate = ds.GetAttributeValue<DateTime>("oems_startdate"),
                                //FinalOferedDate = ds.GetAttributeValue<DateTime>("oems_enddate"),
                                //CourseCode = evt.GetAttributeValue<string>("oems_courseofferingcode"),
                                //TargetGrades = evt.GetAttributeValue<string>("oems_coursetargetgrades"),
                                //InitialDailyTime = ts.GetAttributeValue<string>("oems_starttime"),
                                //FinalDailyTime = ts.GetAttributeValue<string>("oems_endtime"),
                                CourseDescription = evt.GetAttributeValue<string>("oems_coursedescription"),
                                //Instructor = pre.GetAttributeValue<string>("oems_attendeename")
                            }
                        }).ToList();

                if (courseTermId.HasValue)
                {
                    query = query.Where(courses => courses.Course.TermId == courseTermId.Value).ToList();
                }

                if (!string.IsNullOrEmpty(searchText))
                {
                    query = query.Where(courses => courses.Course.EventTitle.Contains(searchText)).ToList();
                }

                var filteredresults = query.Where(p =>
                {
                    p.WebPage.Id = p.WebPage.GetAttributeValue<Guid>("adx_webpageid");
                    //serviceContext.Attach(p.WebPage);
                    return securityProvider.TryAssert(serviceContext, p.WebPage, Microsoft.Xrm.Client.Security.CrmEntityRight.Read);
                });

                return (filteredresults.Select(r => r.Course).OrderBy(sortExpression).Skip(startRowIndex).Take(maximumRows));
            }
        }

        public IEnumerable<EventListing> MyApplicationsTermSelect(string sortExpression, int maximumRows, int startRowIndex)
        {
            if (string.IsNullOrEmpty(sortExpression))
            {
                sortExpression = "InitialOferedDate desc";
            }

            using (var serviceContext = new XrmServiceContext())
            {
                var securityProvider = Microsoft.Xrm.Portal.Configuration.PortalCrmConfigurationManager.CreateCrmEntitySecurityProvider();
                var query =
                    (
                        from reg in serviceContext.CreateQuery("oems_eventregistration")
                        join evt in serviceContext.CreateQuery("oems_event") on ((EntityReference)reg["oems_event"]).Id equals evt["oems_eventid"]
                        join con in serviceContext.CreateQuery("contact") on ((EntityReference)reg["oems_account"]).Id equals ((EntityReference)con["parentcustomerid"]).Id
                        join wp in serviceContext.CreateQuery("adx_webpage") on ((EntityReference)evt["oems_webpage"]).Id equals wp["adx_webpageid"]
                        where con.GetAttributeValue<Guid>("contactid") == this.contactId
                        where /* reg.GetAttributeValue<int>("statuscode") != RegistrationHelper.EventRegistration.Initial && */ reg.GetAttributeValue<OptionSetValue>("oems_registrationtype").Value == RegistrationHelper.EventRegistrationType.Presenter
                        select new
                        {
                            WebPage = wp,
                            Course = new EventListing()
                            {
                                EventId = evt.GetAttributeValue<Guid>("oems_eventid"),
                                EventTitle = evt.GetAttributeValue<string>("oems_eventname"),
                                TermId = evt.GetAttributeValue<Guid>("oems_eventid"),
                                TermName = evt.GetAttributeValue<string>("oems_eventname"),
                                TermCodeName = evt.GetAttributeValue<OptionSetValue>("oems_coursetermcode") == null ? string.Empty :
                                    EntityOptionSet.GetOptionSetLabel("oems_event", "oems_coursetermcode", (evt.GetAttributeValue<OptionSetValue>("oems_coursetermcode")).Value),
                                EventStatus = (evt.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                                EventState = (evt.GetAttributeValue<OptionSetValue>("statecode")).Value,
                                StartDate = evt.GetAttributeValue<DateTime>("oems_startdate"),
                                EndDate = evt.GetAttributeValue<DateTime>("oems_enddate"),
                                RegistrationType = reg.GetAttributeValue<OptionSetValue>("oems_registrationtype") != null ? (reg.GetAttributeValue<OptionSetValue>("oems_registrationtype")).Value : RegistrationHelper.EventRegistrationType.Attendee,
                                RegistrationOpen = evt.GetAttributeValue<DateTime?>("oems_registrationopening"),
                                RegistrationDeadline = evt.GetAttributeValue<DateTime?>("oems_registrationdeadline"),
                                RegistrationMaximum = evt.GetAttributeValue<int>("oems_registrationmaximum"),
                                EventUrl = GetCourseURL(wp),
                                EventRegistrationNumber = reg.GetAttributeValue<string>("oems_registrationnumber"),
                                EventRegistrationId = reg.GetAttributeValue<Guid>("oems_eventregistrationid"),
                                EventStatusText = ValidateStatusText((evt.GetAttributeValue<OptionSetValue>("statuscode")).Value, (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value, evt.Id),
                                EventRegistrationStatus = (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                                EventRegistrationStatusText = EntityOptionSet.GetOptionSetLabel("oems_eventregistration", "statuscode", (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value),
                                EventConfig = Site.Helpers.RegistrationHelper.MyEventHelper(reg.Id, portal),
                                EventLocalDelegationComponentFlag = evt.GetAttributeValue<bool>("oems_localdelegationcomponentflag"),
                                WaitingListComponentFlag = evt.GetAttributeValue<bool>("oems_waitinglistcomponentflag"),
                                WaitingListFullFlag = evt.GetAttributeValue<bool>("oems_waitinglistfullflag"),
                                EventFullFlag = evt.GetAttributeValue<bool>("oems_eventfullflag"),
                                EventRejectionReason = reg.GetAttributeValue<int>("oems_rejectedreason"),
                                EventRejectionReasonDetails = reg.GetAttributeValue<string>("oems_rejectedreasondetails"),
                                DisplaySupportStaffOwner = evt.GetAttributeValue<bool>("oems_displaysupportstaffowners"),
                                DisplayExecutiveStaffOwner = evt.GetAttributeValue<bool>("oems_displayexecutivestaffowners")
                            }
                        }
                    ).ToList();

                if (courseTermId.HasValue)
                {
                    query = query.Where(courses => courses.Course.TermId == courseTermId.Value).ToList();
                }

                if (!string.IsNullOrEmpty(searchText))
                {
                    query = query.Where(courses => courses.Course.EventTitle.Contains(searchText)).ToList();
                }

                var filteredresults = query.Where(p =>
                {
                    p.WebPage.Id = p.WebPage.GetAttributeValue<Guid>("adx_webpageid");
                    //serviceContext.Attach(p.WebPage);
                    return securityProvider.TryAssert(serviceContext, p.WebPage, Microsoft.Xrm.Client.Security.CrmEntityRight.Read);
                });

                return (filteredresults.Select(r => r.Course).OrderBy(sortExpression).Skip(startRowIndex).Take(maximumRows));
            }
        }

        /// <summary>
        /// The select by class count.
        /// </summary>
        /// <returns>
        /// Returns the number of Events
        /// </returns>
        public int SelectCount()
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var securityProvider = Microsoft.Xrm.Portal.Configuration.PortalCrmConfigurationManager.CreateCrmEntitySecurityProvider();
                var query =
                (
                    from evt in serviceContext.CreateQuery("oems_event")
                    join wp in serviceContext.CreateQuery("adx_webpage") on ((EntityReference)evt["oems_webpage"]).Id equals wp["adx_webpageid"]
                    join top in serviceContext.CreateQuery("oems_coursetopic") on ((EntityReference)evt["oems_coursetopic"]).Id equals top["oems_coursetopicid"]
                    join loc in serviceContext.CreateQuery("oems_courselocation") on ((EntityReference)evt["oems_courselocation"]).Id equals loc["oems_courselocationid"]
                    join ds in serviceContext.CreateQuery("oems_coursedateslots") on ((EntityReference)evt["oems_coursedateslot"]).Id equals ds["oems_coursedateslotsid"]
                    join ts in serviceContext.CreateQuery("oems_coursetimeslot") on ((EntityReference)evt["oems_coursetimeslot"]).Id equals ts["oems_coursetimeslotid"]
                    join tg in serviceContext.CreateQuery("oems_targetgrade") on ((EntityReference)evt["oems_coursetargetgrades"]).Id equals tg["oems_targetgradeid"]  // Clean
                    //join coa in serviceContext.CreateQuery("oems_eventregistration") on ((EntityReference)evt["oems_courseofferingapplication"]).Id equals coa["oems_eventregistrationid"]
                    where evt.GetAttributeValue<EntityReference>("oems_courseterm") != null && evt.GetAttributeValue<EntityReference>("oems_courseterm").Id == this.courseTermId
                    where evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.PublishedApplication
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.OpenApplication
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.CloseApplication
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.PublishedRegistration
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.OpenRegistration
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.CloseRegistration
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.StartEvent
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.EndEvent
                            || evt.GetAttributeValue<int>("statuscode") == RegistrationHelper.EventStatus.ExceededCapacity
                    select new
                    {
                        WebPage = wp,
                        Course = new EventListing()
                        {
                            EventId = evt.Id,
                            EventTitle = evt.GetAttributeValue<string>("oems_eventname")
                        }
                    }
                ).ToList();

                if (!string.IsNullOrEmpty(searchText))
                {
                    query = query.Where(courses => courses.Course.EventTitle.ToLower().Contains(searchText.ToLower())).ToList();
                }

                // Clean
                var filteredresults = query.Where(p =>
                {
                    p.WebPage.Id = p.WebPage.GetAttributeValue<Guid>("adx_webpageid");
                    return securityProvider.TryAssert(serviceContext, p.WebPage, Microsoft.Xrm.Client.Security.CrmEntityRight.Read);
                });

                //var filteredresults = query.ToList();
                return filteredresults.Count();
            }
        }

        /// <summary>
        /// The select by class count.
        /// </summary>
        /// <returns>
        /// Returns the number of Events
        /// </returns>
        public int MySelectCount()
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var securityProvider = Microsoft.Xrm.Portal.Configuration.PortalCrmConfigurationManager.CreateCrmEntitySecurityProvider();
                var query =
                    (
                        from reg in serviceContext.CreateQuery("oems_eventregistration")
                        join evt in serviceContext.CreateQuery("oems_event") on ((EntityReference)reg["oems_event"]).Id equals evt["oems_eventid"]
                        join trm in serviceContext.CreateQuery("oems_event") on ((EntityReference)evt["oems_courseterm"]).Id equals trm["oems_eventid"]
                        join con in serviceContext.CreateQuery("contact") on ((EntityReference)reg["oems_account"]).Id equals ((EntityReference)con["parentcustomerid"]).Id
                        join wp in serviceContext.CreateQuery("adx_webpage") on ((EntityReference)evt["oems_webpage"]).Id equals wp["adx_webpageid"]
                        join top in serviceContext.CreateQuery("oems_coursetopic") on ((EntityReference)evt["oems_coursetopic"]).Id equals top["oems_coursetopicid"]
                        join loc in serviceContext.CreateQuery("oems_courselocation") on ((EntityReference)evt["oems_courselocation"]).Id equals loc["oems_courselocationid"]
                        join pre in serviceContext.CreateQuery("oems_eventattendee") on evt["oems_eventid"] equals ((EntityReference)pre["oems_event"]).Id
                        join ds in serviceContext.CreateQuery("oems_coursedateslots") on ((EntityReference)evt["oems_coursedateslot"]).Id equals ds["oems_coursedateslotsid"]
                        join ts in serviceContext.CreateQuery("oems_coursetimeslot") on ((EntityReference)evt["oems_coursetimeslot"]).Id equals ts["oems_coursetimeslotid"]
                        where con.GetAttributeValue<Guid>("contactid") == this.contactId
                        where evt.GetAttributeValue<EntityReference>("oems_courseterm") != null //&& evt.GetAttributeValue<EntityReference>("oems_courseterm").Id == this.courseTermId
                        where !pre.GetAttributeValue<bool>("oems_copresenter")
                        where reg.GetAttributeValue<int>("statuscode") != RegistrationHelper.EventRegistration.Initial
                        select new
                        {
                            WebPage = wp,
                            Course = new EventListing()
                            {
                                EventId = evt.GetAttributeValue<Guid>("oems_eventid"),
                                EventTitle = evt.GetAttributeValue<string>("oems_eventname"),
                                TermId = trm.GetAttributeValue<Guid>("oems_eventid"),
                                TermName = trm.GetAttributeValue<string>("oems_eventname"),
                            }
                        }).ToList();


                if (courseTermId.HasValue)
                {
                    query = query.Where(courses => courses.Course.TermId == courseTermId.Value).ToList();
                }
                
                if (!string.IsNullOrEmpty(searchText))
                {
                    query = query.Where(courses => courses.Course.EventTitle.Contains(searchText)).ToList();
                }

                var filteredresults = query.Where(p =>
                {
                    p.WebPage.Id = p.WebPage.GetAttributeValue<Guid>("adx_webpageid");
                    //serviceContext.Attach(p.WebPage);
                    return securityProvider.TryAssert(serviceContext, p.WebPage, Microsoft.Xrm.Client.Security.CrmEntityRight.Read);
                });

                return filteredresults.Count();
            }
        }

        /// <summary>
        /// The select by class count.
        /// </summary>
        /// <returns>
        /// Returns the number of Events
        /// </returns>
        public int MyApplicationsTermSelectCount()
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var securityProvider = Microsoft.Xrm.Portal.Configuration.PortalCrmConfigurationManager.CreateCrmEntitySecurityProvider();
                var query =
                    (
                        from reg in serviceContext.CreateQuery("oems_eventregistration")
                        join evt in serviceContext.CreateQuery("oems_event") on ((EntityReference)reg["oems_event"]).Id equals evt["oems_eventid"]
                        join con in serviceContext.CreateQuery("contact") on ((EntityReference)reg["oems_account"]).Id equals ((EntityReference)con["parentcustomerid"]).Id
                        join wp in serviceContext.CreateQuery("adx_webpage") on ((EntityReference)evt["oems_webpage"]).Id equals wp["adx_webpageid"]
                        where con.GetAttributeValue<Guid>("contactid") == this.contactId
                        where /*reg.GetAttributeValue<int>("statuscode") != RegistrationHelper.EventRegistration.Initial && */reg.GetAttributeValue<OptionSetValue>("oems_registrationtype").Value == RegistrationHelper.EventRegistrationType.Presenter
                        select new
                        {
                            WebPage = wp,
                            Course = new EventListing()
                            {
                                EventId = evt.GetAttributeValue<Guid>("oems_eventid"),
                                EventTitle = evt.GetAttributeValue<string>("oems_eventname"),
                                TermId = evt.GetAttributeValue<Guid>("oems_eventid"),
                                TermName = evt.GetAttributeValue<string>("oems_eventname")
                            }
                        }).ToList();


                if (courseTermId.HasValue)
                {
                    query = query.Where(courses => courses.Course.TermId == courseTermId.Value).ToList();
                }

                if (!string.IsNullOrEmpty(searchText))
                {
                    query = query.Where(courses => courses.Course.EventTitle.Contains(searchText)).ToList();
                }

                var filteredresults = query.Where(p =>
                {
                    p.WebPage.Id = p.WebPage.GetAttributeValue<Guid>("adx_webpageid");
                    //serviceContext.Attach(p.WebPage);
                    return securityProvider.TryAssert(serviceContext, p.WebPage, Microsoft.Xrm.Client.Security.CrmEntityRight.Read);
                });

                return filteredresults.Count();
            }
        }

        #endregion

        #region Static Convenience Methods

        public static string GetCourseURL(Entity wp)
        {
            //Adx_webpage wb = null;
            using (var serviceContext = new XrmServiceContext())
            {
                wp.Id = wp.GetAttributeValue<Guid>("adx_webpageid");
                var url = serviceContext.GetUrl(wp);
                return url;
            }
        }

        public static string GetCoursePrices(Guid eventId)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var prices =
                    (
                        from price in serviceContext.CreateQuery("oems_eventpricing")
                        where ((EntityReference)price["oems_event"]).Id == eventId
                        select string.Format
                        (
                            "<tr><td>&nbsp;</td><td>{0}</td><td>{1}</td></tr>",
                            price.GetAttributeValue<string>("oems_name"),
                            "$" + (Math.Round(price.GetAttributeValue<decimal>("oems_price"), 2)).ToString()
                        )
                    ).ToList();

                var html = string.Join("", prices);
                if (!String.IsNullOrEmpty(html))
                {
                    html = @"<div><b>Prices</b></div><table width=""100%"" class=""courselisting-prices"">" + html + "</table>";
                }

                return html;
            }
        }

        public static string GetRegistrationPeriods(Guid eventId, int eventStatusCode, bool includeHeader = false)
        {
            if (eventStatusCode == RegistrationHelper.EventStatus.PublishedRegistration || eventStatusCode == RegistrationHelper.EventStatus.OpenRegistration || eventStatusCode == RegistrationHelper.EventStatus.CloseRegistration)
            { 
                
                
                using (var serviceContext = new XrmServiceContext())
                {
                    int? getTimeZoneCode = RetrieveCurrentUsersSettings(serviceContext);
                    var schedules =
                    (
                        from registrationSchedules in serviceContext.oems_EventRegistrationScheduleSet
                        where registrationSchedules.oems_Event.Id == eventId
                        orderby registrationSchedules.oems_RegistrationOpenDateTime
                        select string.Format
                        (
                            "<tr><td>&nbsp;</td><td>{0}</td><td>{1}</td><td>{2} - {3}</td></tr>",
                            registrationSchedules.oems_RegistrationScheduleName,
                            EntityOptionSet.GetOptionSetLabel("oems_eventregistrationschedule", "oems_registranttype", registrationSchedules.oems_RegistrantType.Value),
                            (RetrieveLocalTimeFromUTCTime((DateTime)registrationSchedules.oems_RegistrationOpenDateTime.Value, getTimeZoneCode,serviceContext)).ToString("f"),
                            (RetrieveLocalTimeFromUTCTime((DateTime)registrationSchedules.oems_RegistrationCloseDateTime.Value, getTimeZoneCode, serviceContext)).ToString("f")                            
                        )
                    ).ToList();

                    var html = string.Join("", schedules);
                    if (!String.IsNullOrEmpty(html))
                    {
                        var header = "";
                        if (includeHeader)
                        {
                            header = "<tr><th>&nbsp;</th><th>Name</th><th>Registrant Type</th><th>Dates</th></tr>";
                        }
                        html = @"<div><b>Registration Periods</b></div><table width=""100%"" class=""registration-schedules"">" + header + html + "</table>";
                    }

                    return html;
                }
            }
            return "";
        }

        public static string GetCourseApplicationSchedules(Guid eventId, int eventStatusCode, bool includeHeader = false)
        {
            if (eventStatusCode == RegistrationHelper.EventStatus.PublishedApplication || eventStatusCode == RegistrationHelper.EventStatus.OpenApplication || eventStatusCode == RegistrationHelper.EventStatus.CloseApplication)
            {
                using (var serviceContext = new XrmServiceContext())
                {
                    var schedules =
                    (
                        from applicationSchedules in serviceContext.CreateQuery("oems_eventapplicationschedule")
                        where ((EntityReference)applicationSchedules["oems_event"]).Id == eventId
                        select string.Format
                        (
                            "<tr><td>&nbsp;</td><td>{0}</td><td>{1}</td><td>{2} - {3}</td></tr>",
                            applicationSchedules["oems_applicationschedulename"],
                            EntityOptionSet.GetOptionSetLabel("oems_eventapplicationschedule", "oems_applicanttype", (applicationSchedules["oems_applicanttype"] as OptionSetValue).Value),
                            ((DateTime)applicationSchedules["oems_applicationopendatetime"]).ToString("MMMM dd, yyyy"),
                            ((DateTime)applicationSchedules["oems_applicationclosedatetime"]).ToString("MMMM dd, yyyy")
                        )
                    ).ToList();

                    var html = string.Join("", schedules);
                    if (!String.IsNullOrEmpty(html))
                    {
                        var header = "";
                        if (includeHeader)
                        {
                            header = "<tr><th>&nbsp;</th><th>Name</th><th>Applicant Type</th><th>Dates</th></tr>";
                        }
                        html = @"<div><b>Application Periods</b></div><table width=""100%"" class=""registration-schedules"">" + header + html + "</table>";
                    }

                    return html;
                }
            }
            return "";
        }

        private static DateTime RetrieveLocalTimeFromUTCTime(DateTime utcTime, int? timeZoneCode, IOrganizationService service)
        {
            if (!timeZoneCode.HasValue)
                return DateTime.Now;

            var request = new LocalTimeFromUtcTimeRequest
            {
                TimeZoneCode = timeZoneCode.Value,
                UtcTime = utcTime.ToUniversalTime()
            };

            var response = (LocalTimeFromUtcTimeResponse)service.Execute(request);
            return response.LocalTime;
        }

        /// <summary>
        /// Retrieves the current users timezone code
        /// </summary>
        /// <param name="service"> IOrganizationService </param>
        /// <returns></returns>
        private static int? RetrieveCurrentUsersSettings(XrmServiceContext service)
        {
            var currentUserSettings = service.RetrieveMultiple(
                new QueryExpression("usersettings")
                {
                    ColumnSet = new ColumnSet("timezonecode"),
                    Criteria = new FilterExpression
                    {
                        Conditions =

                                    {
                                        new ConditionExpression("systemuserid", ConditionOperator.EqualUserId)
                                    }
                    }
                }).Entities[0].ToEntity<Entity>();

            //return time zone code

            return (int?)currentUserSettings.Attributes["timezonecode"];
        }

        #endregion
    }
}