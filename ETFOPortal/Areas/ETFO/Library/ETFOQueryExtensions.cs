﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Xml.Linq;
using Microsoft.Xrm.Client.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Metadata;

namespace Adxstudio.Xrm.Data
{
    public static class ETFOQueryExtensions
    {
        /// <summary>
        /// Filters a query based on whether a selected value is  not equal to any of the values in a given collection.
        /// Derived from Adxstudio.Xrm.Data.EnumerableExtensions WhereIn Method
        /// </summary>
        /// <typeparam name="T">The query type.</typeparam>
        /// <typeparam name="TValue">The type of value to be compared.</typeparam>
        /// <param name="queryable">The query.</param>
        /// <param name="selector">A lamba expression that will select the value to be compared.</param>
        /// <param name="values">The collection of values to be compared against.</param>
        /// <returns>The query, with filter appended.</returns>
        /// <example>
        /// <![CDATA[
        /// var query = serviceContext.CreateQuery("adx_webpage").WhereIn(e => e.GetAttributeValue<Guid>("adx_webpageid"), pageIds);
        /// ]]>
        /// </example>
        public static IQueryable<T> WhereNotIn<T, TValue>(this IQueryable<T> queryable, Expression<Func<T, TValue>> selector, IEnumerable<TValue> values)
        {
            return queryable.Where(NotIn(selector, values));
        }

        private static Expression<Func<T, bool>> NotIn<T, TValue>(Expression<Func<T, TValue>> selector, IEnumerable<TValue> values)
        {
            return Expression.Lambda<Func<T, bool>>(
                NotIn(selector.Body, values),
                selector.Parameters.First());
        }

        private static Expression NotIn<TValue>(Expression selectorBody, IEnumerable<TValue> values)
        {
            return NotIn(
                selectorBody,
                values.Skip(1),
                Expression.Equal(selectorBody, Expression.Constant(values.First())));
        }

        private static Expression NotIn<TValue>(Expression selectorBody, IEnumerable<TValue> values, Expression expression)
        {
            return !values.Any()
                ? NotIn(
                    selectorBody,
                    values.Skip(1),
                    Expression.OrElse(expression, Expression.Equal(selectorBody, Expression.Constant(values.First()))))
                : expression;
        }
    }
}