﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Adxstudio.Xrm.Blogs;

namespace Site.Areas.ETFO.Library
{
	public static class BlogPostHelper
	{
	    public static IQueryable<Xrm.adx_blogpost> GetAllFeaturedPostsByType(Xrm.XrmServiceContext serviceContext, Enums.BlogType type)
	    {
			//var announcementAdapter = new BlogDataAdapter(AnnouncementBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));

			//var baseAnnouncements = from posts in new BlogDataAdapter(AnnouncementBlog, new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
			//var announcements = announcementAdapter.SelectPosts()
		    var query = from post in serviceContext.adx_blogpostSet
		                join blog in serviceContext.adx_blogSet on post.adx_blogid.Id equals blog.adx_blogId
		                where post.adx_published == true && post.oems_Feature == true
		                where blog.oems_type.GetValueOrDefault() == (int)1
						orderby post.adx_date descending
						select post;

			

		    //var test = query.ToList();

			return query;
		}
	}
}