﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Microsoft.Xrm.Portal;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Sdk;
using Site.Helpers;
using Xrm;

namespace Site.Areas.ETFO.Library
{
    public class EventListings
    {
        #region Fields

        private readonly bool? showPastEvents;

        private readonly Guid? eventTypeId;

        private readonly string eventTypeName;

        private readonly Guid? contactId;

        private readonly string searchText;

        private readonly IPortalContext portal;

        private List<Guid> invalidEvents;
        
        private Dictionary<Guid, string> termsCache;

        private Dictionary<Guid, string> eventNames;

        #endregion

        #region Constructors and Destructors

        public EventListings(Guid? eventTypeId, string serchText, bool? showPastEvents, Guid? contactId, IPortalContext portal)
        {
            this.eventTypeId = eventTypeId;
            this.showPastEvents = showPastEvents;
            this.contactId = contactId;
            if (this.eventTypeId.HasValue) this.eventTypeName = getEventTypeName(this.eventTypeId.Value);
            this.searchText = serchText;
            this.portal = portal;

            //Extra variables
            invalidEvents = new List<Guid>();
            termsCache = new Dictionary<Guid, string>();
            eventNames = new Dictionary<Guid, string>();
        }

        #endregion

        #region Public Methods and Operators

        public IEnumerable<EventListing> Select(string sortExpression)
        {
            return Select(sortExpression, 20, 0);
        }

        public IEnumerable<EventListing> Select(string sortExpression, int maximumRows, int startRowIndex)
        {
            if (string.IsNullOrEmpty(sortExpression))
            {
                sortExpression = "StartDate desc";
            }

            using (var serviceContext = new XrmServiceContext())
            {
                var securityProvider = Microsoft.Xrm.Portal.Configuration.PortalCrmConfigurationManager.CreateCrmEntitySecurityProvider();

                var query = serviceContext.oems_EventSet 
                    .Join(serviceContext.oems_LocationSet, ev => ev.oems_EventLocation.Id, el => el.oems_LocationId, (ev, el) => new { Event = ev, Location = el })
                    .Join(serviceContext.Adx_webpageSet, evEl => evEl.Event.oems_WebPage.Id, wp => wp.Adx_webpageId, (evEl, wp) => new { EventLocation = evEl, Webpage = wp })
                    .Where(x => x.EventLocation.Event.statecode == 0)
                    .Where(x => x.EventLocation.Event.GetAttributeValue<EntityReference>("oems_courseterm") == null)
                    .Where(x => !x.EventLocation.Event.GetAttributeValue<bool>("oems_coursetermflag"))
                    .Where(x => !x.EventLocation.Event.GetAttributeValue<bool>("oems_privateevent"));

                if (eventTypeId != null) 
                {
                    query = query.Where(evSub => evSub.EventLocation.Event.oems_Subcategory.Id == eventTypeId);
                }

                if (this.showPastEvents.HasValue && this.showPastEvents.Value)
                {
                    query = query.Where(x => x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.Completed);
                }
                else
                {
                    query = query.Where(x => x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.PublishedApplication
                                          || x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.OpenApplication
                                          || x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.CloseApplication
                                          || x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.PublishedRegistration
                                          || x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.OpenRegistration
                                          || x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.CloseRegistration
                                          || x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.StartEvent
                                          || x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.EndEvent
                                          || x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.ExceededCapacity
                                          );
                }

                if (!string.IsNullOrEmpty(searchText))
                {
                    query = query.Where(evTxt => evTxt.EventLocation.Event.oems_EventName.Contains(searchText));
                }

                var results = query.Select(r => new
                {
                    wp = r.Webpage,
                    ev = new EventListing
                    {
                        EventId = r.EventLocation.Event.Id,
                        EventTitle = r.EventLocation.Event.GetAttributeValue<string>("oems_eventname"),
                        EventStatus = r.EventLocation.Event.GetAttributeValue<int?>("statuscode"),
                        EventState = r.EventLocation.Event.GetAttributeValue<int?>("statecode"),
                        EventTypeID = r.EventLocation.Event.oems_Subcategory.Id,
                        EventTypeName = this.eventTypeName ?? getEventTypeName(r.EventLocation.Event.oems_Subcategory.Id),
                        Location = r.EventLocation.Location.oems_PortalDisplayText,
                        StartDate = r.EventLocation.Event.oems_StartDate,
                        EndDate = r.EventLocation.Event.oems_EndDate,
                        WaitingListComponentFlag = r.EventLocation.Event.GetAttributeValue<bool>("oems_waitinglistcomponentflag"),
                        WaitingListFullFlag = r.EventLocation.Event.GetAttributeValue<bool>("oems_waitinglistfullflag"),
                        EventFullFlag = r.EventLocation.Event.GetAttributeValue<bool>("oems_eventfullflag"),
                        RegistrationMaximum = r.EventLocation.Event.oems_RegistrationMaximum,
                        RegistrationDeadline = r.EventLocation.Event.oems_RegistrationDeadline,
                        EventUrl = getEventURL(r.Webpage),
                        EventStatusText = EntityOptionSet.GetOptionSetLabel("oems_event", "statuscode", r.EventLocation.Event.statuscode),
                        DisplaySupportStaffOwner = r.EventLocation.Event.GetAttributeValue<bool>("oems_displaysupportstaffowners"),
                        DisplayExecutiveStaffOwner = r.EventLocation.Event.GetAttributeValue<bool>("oems_displayexecutivestaffowners")
                    }
                }).ToList();

                var filteredresults = results.Where
                (
                    p =>
                    {
                        p.wp.Id = p.wp.Adx_webpageId.Value;
                        serviceContext.Attach(p.wp);
                        return securityProvider.TryAssert(serviceContext, p.wp, Microsoft.Xrm.Client.Security.CrmEntityRight.Read);
                    }
                );

                return (filteredresults.Select(r => r.ev).OrderBy(sortExpression).Skip(startRowIndex).Take(maximumRows));
            }
        }

        /// <summary>
        /// The select by class count.
        /// </summary>
        /// <returns>
        /// Returns the number of Events
        /// </returns>
        public int SelectCount()
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var securityProvider = Microsoft.Xrm.Portal.Configuration.PortalCrmConfigurationManager.CreateCrmEntitySecurityProvider();

                var query = serviceContext.oems_EventSet
                    .Join(serviceContext.oems_LocationSet, ev => ev.oems_EventLocation.Id, el => el.oems_LocationId, (ev, el) => new { Event = ev, Location = el })
                    .Join(serviceContext.Adx_webpageSet, evEl => evEl.Event.oems_WebPage.Id, wp => wp.Adx_webpageId, (evEl, wp) => new { EventLocation = evEl, Webpage = wp })
                    .Where(x => x.EventLocation.Event.statecode == 0)
                    .Where(x => x.EventLocation.Event.GetAttributeValue<EntityReference>("oems_courseterm") == null)
                    .Where(x => !x.EventLocation.Event.GetAttributeValue<bool>("oems_coursetermflag"))
                    .Where(x => !x.EventLocation.Event.GetAttributeValue<bool>("oems_privateevent"));

                if (eventTypeId != null)
                {
                    query = query.Where(evSub => evSub.EventLocation.Event.oems_Subcategory.Id == eventTypeId);
                }

                if (this.showPastEvents.HasValue && this.showPastEvents.Value)
                {
                    query = query.Where(x => x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.Completed);
                }
                else
                {
                    query = query.Where(x => x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.PublishedApplication
                                          || x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.OpenApplication
                                          || x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.CloseApplication
                                          || x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.PublishedRegistration
                                          || x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.OpenRegistration
                                          || x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.CloseRegistration
                                          || x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.StartEvent
                                          || x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.EndEvent
                                          || x.EventLocation.Event.statuscode == RegistrationHelper.EventStatus.ExceededCapacity);
                }

                if (!string.IsNullOrEmpty(searchText))
                {
                    query = query.Where(evTxt => evTxt.EventLocation.Event.oems_EventName.Contains(searchText));
                }

                var results = query.Select(r => new
                {
                    wp = r.Webpage
                }).ToList();

                var filteredresults = results.Where
                (
                    p => 
                    {
                        p.wp.Id = p.wp.Adx_webpageId.Value;
                        serviceContext.Attach(p.wp);
                        return securityProvider.TryAssert(serviceContext, p.wp, Microsoft.Xrm.Client.Security.CrmEntityRight.Read);
                    }
                );

                return filteredresults.Count();
            }
        }

        public IEnumerable<EventListing> MySelect(string sortExpression)
        {
            return MySelect(sortExpression, 10, 0);
        }

        public IEnumerable<EventListing> MySelect(string sortExpression, int maximumRows, int startRowIndex)
        {
            if (string.IsNullOrEmpty(sortExpression))
            {
                sortExpression = "StartDate desc";
            }

            if (this.contactId == null)
                return null;

            using (var serviceContext = new XrmServiceContext())
            {
                var query =
                    (
                        from reg in serviceContext.CreateQuery("oems_eventregistration")
                        join evt in serviceContext.CreateQuery("oems_event") on ((EntityReference)reg["oems_event"]).Id equals evt["oems_eventid"]
                        join sub in serviceContext.CreateQuery("oems_subcategory") on ((EntityReference)evt["oems_subcategory"]).Id equals sub["oems_subcategoryid"]
                        join wp in serviceContext.CreateQuery("adx_webpage") on ((EntityReference)evt["oems_webpage"]).Id equals wp["adx_webpageid"]
                        where ((EntityReference)reg["oems_contact"]).Id == this.contactId
                        where reg.GetAttributeValue<int>("statuscode") != RegistrationHelper.EventRegistration.Initial
                        select new
                        {
                            WebPage = wp,
                            Evt = evt,
                            Reg = reg,
                            EListing = new EventListing()
                            {
                                EventId = evt.GetAttributeValue<Guid>("oems_eventid"),
                                EventTitle = evt.GetAttributeValue<string>("oems_eventname"),
                                EventStatus = (evt.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                                EventState = (evt.GetAttributeValue<OptionSetValue>("statecode")).Value,
                                EventTypeID = (evt.GetAttributeValue<EntityReference>("oems_subcategory")).Id,
                                EventTypeName = sub.GetAttributeValue<string>("oems_subcategoryname"),
                                StartDate = evt.GetAttributeValue<DateTime>("oems_startdate"),
                                EndDate = evt.GetAttributeValue<DateTime>("oems_enddate"),
                                RegistrationType = reg.GetAttributeValue<OptionSetValue>("oems_registrationtype") != null ? (reg.GetAttributeValue<OptionSetValue>("oems_registrationtype")).Value : RegistrationHelper.EventRegistrationType.Attendee,
                                RegistrationOpen = evt.GetAttributeValue<DateTime?>("oems_registrationopening"),
                                RegistrationDeadline = evt.GetAttributeValue<DateTime?>("oems_registrationdeadline"),
                                RegistrationMaximum = evt.GetAttributeValue<int>("oems_registrationmaximum"),
                                EventUrl = getEventURL(wp),
                                EventRegistrationNumber = reg.GetAttributeValue<string>("oems_registrationnumber"),
                                EventRegistrationId = reg.GetAttributeValue<Guid>("oems_eventregistrationid"),
                                EventStatusText = ValidateStatusText((evt.GetAttributeValue<OptionSetValue>("statuscode")).Value, (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value, evt.Id),
                                EventRegistrationStatus = (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value,
                                EventRegistrationStatusText = EntityOptionSet.GetOptionSetLabel("oems_eventregistration", "statuscode", (reg.GetAttributeValue<OptionSetValue>("statuscode")).Value),
                                EventConfig = Site.Helpers.RegistrationHelper.MyEventHelper(reg.Id, portal),
                                EventLocalDelegationComponentFlag = evt.GetAttributeValue<bool>("oems_localdelegationcomponentflag"),
                                WaitingListComponentFlag = evt.GetAttributeValue<bool>("oems_waitinglistcomponentflag"),
                                WaitingListFullFlag = evt.GetAttributeValue<bool>("oems_waitinglistfullflag"),
                                EventFullFlag = evt.GetAttributeValue<bool>("oems_eventfullflag"),
                                EventRejectionReason = reg.GetAttributeValue<int>("oems_rejectedreason"),
                                EventRejectionReasonDetails = reg.GetAttributeValue<string>("oems_rejectedreasondetails"),
                                DisplaySupportStaffOwner = evt.GetAttributeValue<bool>("oems_displaysupportstaffowners"),
                                DisplayExecutiveStaffOwner = evt.GetAttributeValue<bool>("oems_displayexecutivestaffowners"),
                                LocalDelegationFlag = evt.GetAttributeValue<bool>("oems_localdelegationcomponentflag"),
                                AttendingAsJson = reg.GetAttributeValue<string>("oems_updateattendingasdata"),
                                //Events
                                LocationId = evt.GetAttributeValue<EntityReference>("oems_eventlocation") != null ? (evt.GetAttributeValue<EntityReference>("oems_eventlocation")).Id : (Guid?)null,
                                //Courses
                                CourseCode = evt.GetAttributeValue<string>("oems_coursecode"),
                                CourseTitle = reg.GetAttributeValue<string>("oems_coursetitle"),
                                CourseDescription = !string.IsNullOrWhiteSpace(evt.GetAttributeValue<string>("oems_coursedescription")) ? evt.GetAttributeValue<string>("oems_coursedescription").Replace(Environment.NewLine, "<br />").Replace("\n", "<br />") : string.Empty,
                                TermId = evt.GetAttributeValue<EntityReference>("oems_courseterm") != null ? (evt.GetAttributeValue<EntityReference>("oems_courseterm")).Id : (Guid?)null,
                                TermCodeName = evt.GetAttributeValue<OptionSetValue>("oems_coursetermcode") == null ? string.Empty :
                                    EntityOptionSet.GetOptionSetLabel("oems_event", "oems_coursetermcode", (evt.GetAttributeValue<OptionSetValue>("oems_coursetermcode")).Value),
                                CoursePackageAvailableFlag = evt.GetAttributeValue<bool?>("oems_coursepackageavailableflag"),
                                CourseDateSlot = evt.GetAttributeValue<EntityReference>("oems_coursedateslot")
                            }
                        }
                    ).ToList();

                foreach (var q in query)
                {
                    if (!q.Evt.GetAttributeValue<bool>("oems_coursetermflag") && !q.Evt.GetAttributeValue<bool>("oems_courseflag"))
                    {
                        //Normal Event registrations/applications
                        q.EListing.ListingType = 1;
                    }
                    else if (q.Evt.GetAttributeValue<bool>("oems_courseflag"))
                    {
                        //Course registrations
                        q.EListing.ListingType = 2;

                        if (q.EListing.CourseDateSlot != null)
                        {
                            //Course Dates
                            var courseDates = (from cd in serviceContext.CreateQuery("oems_coursedateslots")
                                               where cd.GetAttributeValue<Guid>("oems_coursedateslotsid") == q.EListing.CourseDateSlot.Id
                                               select cd).FirstOrDefault();

                            q.EListing.StartDate = courseDates.GetAttributeValue<DateTime>("oems_startdate");
                            q.EListing.EndDate = courseDates.GetAttributeValue<DateTime>("oems_enddate");
                        }

                        //Check if it is instructor
                        var presenter = (from evt in serviceContext.CreateQuery("oems_event")
                                         join pre in serviceContext.CreateQuery("oems_eventattendee") on evt["oems_eventid"] equals ((EntityReference)pre["oems_event"]).Id
                                         where evt.GetAttributeValue<Guid>("oems_eventid") == q.EListing.EventId
                                         where !pre.GetAttributeValue<bool>("oems_copresenter")
                                         select pre).FirstOrDefault();


                        //Instructor
                        q.EListing.Presenter = presenter != null ? presenter.GetAttributeValue<string>("oems_attendeename") : string.Empty;
                        //q.EListing.Presenter = q.Evt.GetAttributeValue<string>("oems_presenters") != null ? q.Evt.GetAttributeValue<string>("oems_presenters") : string.Empty;

                        //Term name
                        var termName = "";
                        if (q.EListing.TermId != null)
                        {
                            if (termsCache.ContainsKey(q.EListing.TermId.Value))
                            {
                                termName = termsCache[q.EListing.TermId.Value];
                            }
                            else
                            {
                                var term = (from trm in serviceContext.CreateQuery("oems_event")
                                            where trm.GetAttributeValue<Guid>("oems_eventid") == q.EListing.TermId.Value
                                            select trm).FirstOrDefault();

                                if (term != null)
                                {
                                    termName = term.GetAttributeValue<string>("oems_webtermdescription");
                                }
                            }
                        }

                        //Course title
                        q.EListing.EventTitle = termName + " - " + q.EListing.EventTitle;
                        q.EListing.TermName = termName;
                    }
                    else if (q.Evt.GetAttributeValue<bool>("oems_coursetermflag"))
                    {
                        //Course term applications
                        q.EListing.ListingType = 3;
                        q.EListing.TermName = q.EListing.EventTitle;
                        q.EListing.EventTitle = q.Evt.GetAttributeValue<string>("oems_webtermdescription") + " - " + q.EListing.CourseTitle;
                    }

                    eventNames[q.EListing.EventId.Value] = q.EListing.EventTitle;
                }

                if (!string.IsNullOrEmpty(searchText))
                {
                    query = query.Where(events => events.EListing.EventTitle.ToLower().Contains(searchText.ToLower())).ToList();
                }

                //query.RemoveAll(events => invalidEvents.Contains(events.EListing.EventId.Value));
                return query.Select(r => r.EListing).OrderBy(sortExpression).Skip(startRowIndex).Take(maximumRows);
            }
        }

        /// <summary>
        /// The select count class for My Events.
        /// </summary>
        /// <returns>
        /// Returns the number of Event registrations for a user
        /// </returns>
        public int MySelectCount()
        {
            if (this.contactId == null) return 0;

            using (var serviceContext = new XrmServiceContext())
            {
                var query =
                    (
                        from reg in serviceContext.CreateQuery("oems_eventregistration")
                        join evt in serviceContext.CreateQuery("oems_event") on ((EntityReference)reg["oems_event"]).Id equals evt["oems_eventid"]
                        join sub in serviceContext.CreateQuery("oems_subcategory") on ((EntityReference)evt["oems_subcategory"]).Id equals sub["oems_subcategoryid"]
                        join wp in serviceContext.CreateQuery("adx_webpage") on ((EntityReference)evt["oems_webpage"]).Id equals wp["adx_webpageid"]
                        where ((EntityReference)reg["oems_contact"]).Id == this.contactId
                        where reg.GetAttributeValue<int>("statuscode") != RegistrationHelper.EventRegistration.Initial
                        select new
                        {
                            WebPage = wp,
                            Evt = new EventListing()
                            {
                                EventId = evt.GetAttributeValue<Guid>("oems_eventid"),
                                EventTitle = evt.GetAttributeValue<string>("oems_eventname")
                            }
                        }
                    ).ToList();

                query.ForEach(q =>
                {
                    q.Evt.EventTitle = eventNames[q.Evt.EventId.Value];
                });

                if (!string.IsNullOrEmpty(searchText))
                {
                    query = query.Where(events => events.Evt.EventTitle.ToLower().Contains(searchText.ToLower())).ToList();
                }

                //Remove invalid events
                query.RemoveAll(d => invalidEvents.Contains(d.Evt.EventId.Value));
                return query.Count();
            }
        }

        public string ValidateStatusText(int? eventStatusCode, int? registrationStatusCode, Guid? eventid)
        {
            if (eventStatusCode == RegistrationHelper.EventStatus.Completed && registrationStatusCode == RegistrationHelper.EventRegistration.Approved)
            {
                using (var serviceContext = new XrmServiceContext())
                {
                    var result = serviceContext.oems_EventAttendeeSet.SingleOrDefault(q => q.oems_EventRegistration.Id == eventid);

                    if (result != null)
                        return EntityOptionSet.GetOptionSetLabel("oems_eventattendee", "statuscode", result.statuscode);
                }


            }

            return EntityOptionSet.GetOptionSetLabel("oems_event", "statuscode", eventStatusCode);
        }

        #endregion

        #region General Static Convenience Methods

        public static string getEventURL(Adx_webpage wb)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                wb.Id = wb.Adx_webpageId.Value;
                var url = serviceContext.GetUrl(wb);
                return url;
            }
        }

        public static string getEventURL(Entity wp)
        {
            //Adx_webpage wb = null;
            using (var serviceContext = new XrmServiceContext())
            {
                wp.Id = wp.GetAttributeValue<Guid>("adx_webpageid");
                var url = serviceContext.GetUrl(wp);
                return url;
            }
        }

        public static string getExecutiveStaff(Guid eventID)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var staff = (from u in serviceContext.SystemUserSet
                             join utoe in serviceContext.oems_event_executive_staff_ownersSet
                             on u.Id equals utoe.systemuserid
                             where utoe.oems_eventid.Value == eventID
                             orderby u.LastName
                             select u.FullName).ToList();
                return string.Join("<br/>", staff);
            }
        }

        public static string getSupportStaff(Guid eventID)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var staff = (from u in serviceContext.SystemUserSet
                             join utoe in serviceContext.oems_event_support_staff_ownersSet
                             on u.Id equals utoe.systemuserid
                             where utoe.oems_eventid.Value == eventID
                             orderby u.LastName
                             select u.FullName).ToList();
                return string.Join("<br/>", staff);
            }
        }

        public static string GetRegistrationPeriods(IPortalContext portal, Guid eventId, int eventStatusCode, bool includeHeader = false)
        {
            if (eventStatusCode == RegistrationHelper.EventStatus.PublishedRegistration || eventStatusCode == RegistrationHelper.EventStatus.OpenRegistration || eventStatusCode == RegistrationHelper.EventStatus.CloseRegistration)
            { 
                using (var serviceContext = new XrmServiceContext())
                {
                    var schedules =
                    (
                        from registrationSchedules in serviceContext.oems_EventRegistrationScheduleSet
                        where registrationSchedules.oems_Event.Id == eventId
                        orderby registrationSchedules.oems_RegistrationOpenDateTime
                        select string.Format
                        (
                            "<tr><td>&nbsp;</td><td>{0}</td><td>{1}</td><td>{2} - {3}</td></tr>",
                            registrationSchedules.oems_RegistrationScheduleName,
                            EntityOptionSet.GetOptionSetLabel("oems_eventregistrationschedule", "oems_registranttype", registrationSchedules.oems_RegistrantType.Value),
                            DateHelper.ConvertFromUtc_Time(portal, registrationSchedules.oems_RegistrationOpenDateTime.Value).ToString("MMMM, dd, yyyy"),
                            DateHelper.ConvertFromUtc_Time(portal, registrationSchedules.oems_RegistrationCloseDateTime.Value).ToString("MMMM, dd, yyyy")
                        )
                    ).ToList();

                    var html = string.Join("", schedules);
                    if (!String.IsNullOrEmpty(html))
                    {
                        var header = "";
                        if (includeHeader)
                        {
                            header = "<tr><th>&nbsp;</th><th>Name</th><th>Registrant Type</th><th>Dates</th></tr>";
                        }
                        html = @"<div><b>Registration Periods</b></div><table width=""100%"" class=""registration-schedules"">" + header + html + "</table>";
                    }

                    return html;
                }
            }
            return "";
        }

        public static string GetEventApplicationSchedules(Guid eventId, int eventStatusCode, bool includeHeader = false)
        {
            if (eventStatusCode == RegistrationHelper.EventStatus.PublishedApplication || eventStatusCode == RegistrationHelper.EventStatus.OpenApplication || eventStatusCode == RegistrationHelper.EventStatus.CloseApplication)
            {
                using (var serviceContext = new XrmServiceContext())
                {
                    var schedules =
                    (
                        from applicationSchedules in serviceContext.CreateQuery("oems_eventapplicationschedule")
                        where ((EntityReference)applicationSchedules["oems_event"]).Id == eventId
                        select string.Format
                        (
                            "<tr><td>&nbsp;</td><td>{0}</td><td>{1}</td><td>{2} - {3}</td></tr>",
                            applicationSchedules["oems_applicationschedulename"],
                            EntityOptionSet.GetOptionSetLabel("oems_eventapplicationschedule", "oems_applicanttype", (applicationSchedules["oems_applicanttype"] as OptionSetValue).Value),
                            ((DateTime)applicationSchedules["oems_applicationopendatetime"]).ToString("MMMM dd, yyyy"),
                            ((DateTime)applicationSchedules["oems_applicationclosedatetime"]).ToString("MMMM dd, yyyy")
                        )
                    ).ToList();

                    var html = string.Join("", schedules);
                    if (!String.IsNullOrEmpty(html))
                    {
                        var header = "";
                        if (includeHeader)
                        {
                            header = "<tr><th>&nbsp;</th><th>Name</th><th>Applicant Type</th><th>Dates</th></tr>";
                        }
                        html = @"<div><b>Application Periods</b></div><table width=""100%"" class=""registration-schedules"">" + header + html + "</table>";
                    }

                    return html;
                }
            }
            return "";
        }

        public static string GetCourseRegistrationPeriods(IPortalContext portal, Guid eventId, int eventStatusCode, bool includeHeader = false)
        {
            if (eventStatusCode == RegistrationHelper.EventStatus.PublishedRegistration || eventStatusCode == RegistrationHelper.EventStatus.OpenRegistration || eventStatusCode == RegistrationHelper.EventStatus.CloseRegistration)
            {
                using (var serviceContext = new XrmServiceContext())
                {
                    var schedules =
                    (
                        from registrationSchedules in serviceContext.oems_EventRegistrationScheduleSet
                        where registrationSchedules.oems_Event.Id == eventId
                        orderby registrationSchedules.oems_RegistrationOpenDateTime
                        select string.Format
                        (
                            @"<div class=""details-content long"" style=""width: 100%""><div class=""details-content long"" style=""width: 20%""><b>{0}:</b></div> {1} - {2}</div>",
                            registrationSchedules.oems_RegistrationScheduleName,
                            DateHelper.ConvertFromUtc_Time(portal, registrationSchedules.oems_RegistrationOpenDateTime.Value).ToString("MMM dd yyyy"),
                            DateHelper.ConvertFromUtc_Time(portal, registrationSchedules.oems_RegistrationCloseDateTime.Value).ToString("MMM dd yyyy")
                        )
                    ).ToList();

                    var html = "";
                    var schedulesJoin = string.Join("", schedules);
                    if (!String.IsNullOrEmpty(schedulesJoin))
                    {
                        html = @"<div class=""details-heading"" style=""width: 50%; "">Registration Periods</div><br /><div class=""details-content long registration-periods"" style=""width: 80%; padding-left: 15px"">" + schedulesJoin + "</div>";
                    }

                    return html;
                }
            }
            return "";
        }

        #endregion

        #region My Events Data

        public static string getEventLocationDisplayText(Guid? locationId)
        {
            if (!locationId.HasValue)
                return string.Empty;

            using (var serviceContext = new XrmServiceContext())
            {
                string locationDisplayText = (from loc in serviceContext.CreateQuery("oems_location") 
                                              where loc.GetAttributeValue<Guid>("oems_locationid") == locationId 
                                              select loc.GetAttributeValue<string>("oems_portaldisplaytext")).FirstOrDefault();

                return locationDisplayText;
            }
        }

        #endregion

        #region My Courses Data

        public static string getCourseTopic(Guid eventId)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var topic = (from evt in serviceContext.CreateQuery("oems_event")
                            join top in serviceContext.CreateQuery("oems_coursetopic") on ((EntityReference)evt["oems_coursetopic"]).Id equals top["oems_coursetopicid"]
                            where evt.GetAttributeValue<Guid>("oems_eventid") == eventId
                            select top.GetAttributeValue<string>("oems_name")).FirstOrDefault();

                return topic;
            }
        }

        public static string getCourseTargetGrade(Guid eventId)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var targetGrade = (from evt in serviceContext.CreateQuery("oems_event")
                             join tg in serviceContext.CreateQuery("oems_targetgrade") on ((EntityReference)evt["oems_coursetargetgrades"]).Id equals tg["oems_targetgradeid"]
                             where evt.GetAttributeValue<Guid>("oems_eventid") == eventId
                             select tg.GetAttributeValue<string>("oems_name")).FirstOrDefault();

                return targetGrade;
            }
        }

        public static string getCoursePresenters(Guid eventId)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var presenters = string.Empty;
                var application = (from evt in serviceContext.CreateQuery("oems_event")
                                   join coa in serviceContext.CreateQuery("oems_eventregistration") on ((EntityReference)evt["oems_courseofferingapplication"]).Id equals coa["oems_eventregistrationid"]
                                   where evt.GetAttributeValue<Guid>("oems_eventid") == eventId
                                   select coa).FirstOrDefault();

                if (application == null)
                    return presenters;

                var presenter = application.GetAttributeValue<string>("oems_firstname") + " " + application.GetAttributeValue<string>("oems_lastname");
                var coPresenter1 = application.GetAttributeValue<string>("oems_copresenter1firstname") + " " + application.GetAttributeValue<string>("oems_copresenter1lastname");
                var coPresenter2 = application.GetAttributeValue<string>("oems_copresenter2firstname") + " " + application.GetAttributeValue<string>("oems_copresenter2lastname");
                var coPresenter3 = application.GetAttributeValue<string>("oems_copresenter3firstname") + " " + application.GetAttributeValue<string>("oems_copresenter3lastname");

                if (string.IsNullOrWhiteSpace(presenter) && string.IsNullOrWhiteSpace(coPresenter1) && string.IsNullOrWhiteSpace(coPresenter2) && string.IsNullOrWhiteSpace(coPresenter3))
                    return presenters;

                //Presenter
                if (!string.IsNullOrWhiteSpace(presenter))
                {
                    presenters += @"<div class=""details-content long"">" + presenter + "</div>";
                }

                //coInstructor1
                if (!string.IsNullOrWhiteSpace(coPresenter1))
                {
                    presenters += @"<div class=""details-content long"">" + coPresenter1 + "</div>";
                }

                //coInstructor2
                if (!string.IsNullOrWhiteSpace(coPresenter2))
                {
                    presenters += @"<div class=""details-content long"">" + coPresenter2 + "</div>";
                }

                //coInstructor3
                if (!string.IsNullOrWhiteSpace(coPresenter3))
                {
                    presenters += @"<div class=""details-content long"">" + coPresenter3 + "</div>";
                }

                return string.Join("", presenters);
            }
        }

        public static string getCourseLocation(Guid eventId)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var location = (from evt in serviceContext.CreateQuery("oems_event")
                             join loc in serviceContext.CreateQuery("oems_courselocation") on ((EntityReference)evt["oems_courselocation"]).Id equals loc["oems_courselocationid"]
                             where evt.GetAttributeValue<Guid>("oems_eventid") == eventId
                             select loc.GetAttributeValue<string>("oems_name")).FirstOrDefault();

                return location;
            }
        }

        public static DateTime getCourseInitialDate(Guid eventId)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var initialDate = (from evt in serviceContext.CreateQuery("oems_event")
                             join ds in serviceContext.CreateQuery("oems_coursedateslots") on ((EntityReference)evt["oems_coursedateslot"]).Id equals ds["oems_coursedateslotsid"]
                             where evt.GetAttributeValue<Guid>("oems_eventid") == eventId
                             select ds.GetAttributeValue<DateTime>("oems_startdate")).FirstOrDefault();

                return initialDate;
            }
        }

        public static DateTime getCourseFinalDate(Guid eventId)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var finalDate = (from evt in serviceContext.CreateQuery("oems_event")
                             join ds in serviceContext.CreateQuery("oems_coursedateslots") on ((EntityReference)evt["oems_coursedateslot"]).Id equals ds["oems_coursedateslotsid"]
                             where evt.GetAttributeValue<Guid>("oems_eventid") == eventId
                             select ds.GetAttributeValue<DateTime>("oems_enddate")).FirstOrDefault();

                return finalDate;
            }
        }

        public static string getCourseInitialTime(Guid eventId)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var startTime = (from evt in serviceContext.CreateQuery("oems_event")
                             join ts in serviceContext.CreateQuery("oems_coursetimeslot") on ((EntityReference)evt["oems_coursetimeslot"]).Id equals ts["oems_coursetimeslotid"]
                             where evt.GetAttributeValue<Guid>("oems_eventid") == eventId
                             select ts.GetAttributeValue<string>("oems_starttime")).FirstOrDefault();

                return startTime;
            }
        }

        public static string getCourseFinalTime(Guid eventId)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var endTime = (from evt in serviceContext.CreateQuery("oems_event")
                             join ts in serviceContext.CreateQuery("oems_coursetimeslot") on ((EntityReference)evt["oems_coursetimeslot"]).Id equals ts["oems_coursetimeslotid"]
                             where evt.GetAttributeValue<Guid>("oems_eventid") == eventId
                             select ts.GetAttributeValue<string>("oems_endtime")).FirstOrDefault();

                return endTime;
            }
        }

        #endregion

        #region Methods

        private string getEventTypeName(Guid EventTypeId)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var subcategory = serviceContext.oems_SubcategorySet.FirstOrDefault(sc => sc.oems_SubcategoryId == EventTypeId);

                return subcategory != null ? subcategory.oems_SubcategoryName : String.Empty;
            }
        }

        #endregion
    }
}