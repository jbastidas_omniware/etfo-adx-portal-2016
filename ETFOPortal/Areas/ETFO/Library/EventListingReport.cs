﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Areas.ETFO.Library
{
    /*
    *          Name:           EventListingReport
    *         
    *          Description:    This is the data model class for reports page.
    * 
    *          Author:         Lawrence Zhou
    *          Create Date:    Mar. 04th, 2014
    * */

    public class EventListingReport : EventListing
    {
        public int? AttendeeStatus { get; set; }
        public string AttendeeStatusText { get; set; }

        public int? ChildCareRequestStatus { get; set; }
        public string ChildCareRequestStatusText { get; set; }

        public int? CaucusRequestStatus { get; set; }
        public string CaucusRequestStatusText { get; set; }

        public int? AccommodationRequestStatus { get; set; }
        public string AccommodationRequestStatusText { get; set; }

        public int? ReleaseTimeRequestStatus { get; set; }
        public string ReleaseTimeRequestStatusText { get; set; }
        public string ReleaseTimeRequestScheduleDays { get; set; }
        public string ReleaseTimeRequestExtraDays { get; set; }
        public string ReleaseTimeRequestNotes { get; set; }

        public string LocalDelegationtName { get; set; }
        public int? LocalDelegationtStatus { get; set; }
        public string LocalDelegationtStatusText { get; set; }
        public Guid? LocalDelegationId { get; set; } 

    }
}