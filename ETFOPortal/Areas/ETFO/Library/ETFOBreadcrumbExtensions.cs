﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Microsoft.Xrm.Portal.Web;
using Xrm;

namespace Adxstudio.Xrm.Web.Mvc.Html
{
    public static class ETFOBreadcrumbExtensions
    {
        public static IHtmlString ETFOBreadcrumb(this HtmlHelper html, string divider = "/", string url = null, int? takeLast = null)
        {
            
            return ETFOBreadcrumb(() => url == null ? html.SiteMapPath(takeLast) : html.SiteMapPath(url, takeLast), divider);
        }

        private static IHtmlString ETFOBreadcrumb(Func<IEnumerable<Tuple<SiteMapNode, SiteMapNodeType>>> getSiteMapPath, string divider)
		{
            using (var serviceContext = new XrmServiceContext())
            {

                //var categoryTemplate=serviceContext.Adx_pagetemplateSet
                var path = getSiteMapPath().ToList();

                if (!path.Any())
                {
                    return null;
                }

                var items = new StringBuilder();

                foreach (var node in path)
                {
                    bool includeNode = true;
                    if (node.Item1.ParentNode!=null && node.Item1.ParentNode.Url == node.Item1.RootNode.Url && node.Item1.ParentNode.Title == node.Item1.RootNode.Title)
                    {
                        var crmEntity = node.Item1 as CrmSiteMapNode;
                        Adx_webpage page = crmEntity.Entity as Adx_webpage;
                        var website = page.adx_websiteid.Id;
                        var categoryTemplate = serviceContext.Adx_pagetemplateSet.FirstOrDefault(c => c.Id == page.adx_pagetemplateid.Id && c.adx_websiteid.Id == page.adx_websiteid.Id);

                        includeNode = categoryTemplate != null && categoryTemplate.Adx_name != "Event Category Redirect";
                    }

                    if (includeNode)
                    {
                        var li = new TagBuilder("li");

                        if (node.Item2 == SiteMapNodeType.Current)
                        {
                            li.AddCssClass("active");
                            li.SetInnerText(node.Item1.Title);
                        }
                        else
                        {

                            var a = new TagBuilder("a");

                            a.Attributes["href"] = node.Item1.Url;
                            a.SetInnerText(node.Item1.Title);

                            li.InnerHtml += a.ToString();

                            if (!string.IsNullOrEmpty(divider))
                            {
                                var span = new TagBuilder("span");

                                span.AddCssClass("divider");
                                span.InnerHtml += divider;

                                li.InnerHtml += " " + span;
                            }
                        }
                        items.AppendLine(li.ToString());
                    }
                }

                var ul = new TagBuilder("ul");

                ul.AddCssClass("breadcrumb");
                ul.InnerHtml += items.ToString();

                return new HtmlString(ul.ToString());
            }
		}
    }
}