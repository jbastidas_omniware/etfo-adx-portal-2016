﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Library
{
    public class LocalDelegationUpdate
    {
        public List<AttendingAsList> attendingAsList { get; set; }
        public string sectionMessage { get; set; }
        public bool showAttendingAsComponent { get; set; }
        public bool showSection { get; set; }
        public bool showSectionMessage { get; set; }
    }

    public class AttendingAsList
    {
        public bool isSelected { get; set; }
        public Guid id { get; set; }
        public Selected selected { get; set; }
        public string name { get; set; }
        public bool isVotingRole { get; set; }
        public bool isGuestRole { get; set; }
        public bool isOther { get; set; }
        public string otherInput { get; set; }
        public string colour { get; set; }
    }

    public class Selected
    {
        public Guid Id { get; set; }
        public string LogicalName { get; set; }
        public string Name { get; set; }
    }
}