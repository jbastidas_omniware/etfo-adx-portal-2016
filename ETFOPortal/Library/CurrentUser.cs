﻿using System.Web;

namespace Site.Library
{
    public class CurrentUser
    {
        public static readonly string USER_PROFILE_DOCUMENTS_COUNT_KEY = "UserProfileDocumentsCount";
        public static readonly string USER_PROFILE_DOCUMENTS_SIZE_KEY = "UserProfileDocumentsSize";

        public static int? ProfileDocumentsCount
        {
            get
            {
                return (int?)HttpContext.Current.Session[USER_PROFILE_DOCUMENTS_COUNT_KEY];
            }
            set
            {
                HttpContext.Current.Session[USER_PROFILE_DOCUMENTS_COUNT_KEY] = value;
            }
        }

        public static int? ProfileDocumentsSize
        {
            get
            {
                return (int?)HttpContext.Current.Session[USER_PROFILE_DOCUMENTS_SIZE_KEY];
            }
            set
            {
                HttpContext.Current.Session[USER_PROFILE_DOCUMENTS_SIZE_KEY] = value;
            }
        }
    }
}