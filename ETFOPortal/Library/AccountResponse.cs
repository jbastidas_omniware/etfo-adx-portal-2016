﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Site.Library
{
    public class AccountResponse
    {
        public string result;
        public string name;
        public string hasparrentcontact;
        public string eftomemberid;
        public string email;
        public string contactid;
        public string accountid;
        public string sincheck;
        public string memberonleaveflag;
        public string goodstandingflag;
        public string adx_username;
        public string adx_resetpasswordanswer;
        public string adx_passwordquestion;
        
    }
}