﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Site.Library
{
    public static class StringExtensions
    {
        public static string RemoveDiacritics(string text)
        {
            if (text == null)
            {
                text = String.Empty;
            }

            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public static string ConvetToAlphaNumeric(string text)
        {
            text = RemoveDiacritics(text);
            Regex rgx = new Regex("[^a-zA-Z0-9]");
            text = rgx.Replace(text, "");

            return text;
        }

    }
}