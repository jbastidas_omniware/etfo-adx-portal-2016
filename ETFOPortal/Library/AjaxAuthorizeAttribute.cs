﻿using Microsoft.Xrm.Portal.IdentityModel.Configuration;
using Site.Helpers;
using System.Collections.Specialized;
using System.Web;
using System.Web.Mvc;

namespace Site.Library
{
    /// <summary>
    /// Class AjaxAuthorizeAttribute:
    /// Manage the authorization attribute operations over an Ajax reques
    /// </summary>
    public class AjaxAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Overrides the on authorization method from the authorization attribute
        /// </summary>
        /// <param name="filterContext">Filter context</param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            // Only do something if we are about to give a HttpUnauthorizedResult and we are in AJAX mode.
            if (!(filterContext.Result is HttpUnauthorizedResult) || !filterContext.HttpContext.Request.IsAjaxRequest())
                return;

            var returnUrlKey = FederationCrmConfigurationManager.GetUserRegistrationSettings().ReturnUrlKey ?? "returnurl";
            var returnUrl = filterContext.RequestContext.HttpContext.Request[returnUrlKey] ?? filterContext.RequestContext.HttpContext.Request.Url.PathAndQuery;
            var url = XrmHelper.GetSiteMarkerUrl("Login") + "?returnUrl=" + returnUrl;

            filterContext.HttpContext.Response.StatusCode = 401;
            filterContext.Result = new JsonResult
            {
                Data = new
                {
                    Error = "NotAuthorized",
                    LogOnUrl = url
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            filterContext.HttpContext.Response.End();
        }
    }
}