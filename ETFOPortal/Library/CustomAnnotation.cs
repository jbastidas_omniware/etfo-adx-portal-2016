﻿using System;
using Microsoft.Xrm.Client;

namespace Site.Library
{
    public class CustomAnnotation
    {
        public Guid Id { get; set; }
        public Guid? AnnotationId { get; set; }
        public string DocumentBody { get; set; }
        public string FileName { get; set; }
        public int? FileSize { get; set; }
        public bool? IsDocument { get; set; }
        public string MimeType { get; set; }
        public string NoteText { get; set; }
        public string Subject { get; set; }
        public virtual CrmEntityReference ObjectId { get; set; }
        public string ObjectTypeCode { get; set; }
    }
}