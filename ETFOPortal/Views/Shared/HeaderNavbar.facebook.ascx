﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<div class="navbar">
	<div class="navbar-inner">
		<div class="container">
			<div class="pull-left">
				<%: Html.HtmlSnippet("Facebook/Navbar/Left") %>
			</div>
			<%: Html.WebLinksNavBar("Facebook Primary Navigation", "weblinks pull-left", "active", "active") %>
			<% if (Request.IsAuthenticated) { %>
				<% var profileNavigation = Html.WebLinkSet("Facebook Profile Navigation"); %>
				<% if (profileNavigation != null && profileNavigation.WebLinks.Any()) { %>
					<ul class="nav pull-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa-user"></i> <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<% foreach (var webLink in profileNavigation.WebLinks) { %>
									<%: Html.WebLinkListItem(webLink, false, false) %>
								<% } %>
							</ul>
						</li>
					</ul>
				<% } %>
			<% } else { %>
				<ul class="nav pull-right">
					<li>
						<% Html.RenderPartial("SignInLink"); %>
					</li>
				</ul>
			<% } %>
			<div class="pull-right">
				<%: Html.HtmlSnippet("Facebook/Navbar/Right") %>
			</div>
		</div>
	</div>
</div>
