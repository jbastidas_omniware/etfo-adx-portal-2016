﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<footer>
    <div class="socialbar">
        <div class="container">
            <p class="pull-right"  style="margin-right:35px">
                <%: Html.TextSnippet("Footer Social Bar", true, "span") %>
                <a href="<%: Html.SiteMarkerUrl("Contact Us") %>" class="social-link"><i class="fa fa-envelope"></i></a>
                <a href="<%: Html.SiteMarkerUrl("Contact Us") %>" class="social-link"><i class="fa fa-phone"></i></a>
            </p>
        </div>
    </div>
    <div class="container">
	    <%: Html.HtmlSnippet("Footer") %>
    </div>
</footer>