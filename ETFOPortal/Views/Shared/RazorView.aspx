﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Mvc.master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
 
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial((string) ViewBag._ViewName); %>
</asp:Content>
