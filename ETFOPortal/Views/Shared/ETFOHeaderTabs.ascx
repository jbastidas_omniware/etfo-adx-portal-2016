﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<%@ Import Namespace="Microsoft.Xrm.Portal" %>
<%@ Import Namespace="Microsoft.Xrm.Portal.Configuration" %>
<%@ Import Namespace="Xrm" %>
<%@ Import Namespace="Site.Areas.Payment.Library" %>

<%--Check for what navigation to load as primary navigation for page--%>
<% var portal = new Lazy<IPortalContext>(() => PortalCrmConfigurationManager.CreatePortalContext()); %>
<% var underMyEtfo = new SiteMarkerHelper().EqualOrUnderSiteMarker(System.Web.SiteMap.CurrentNode, "My ETFO"); %>
<% var navigationName = underMyEtfo ? "My ETFO Navigation" : "Primary Navigation"; %>
<% var ancestorCSS = underMyEtfo ? "" : "active"; %>
<% var sc = PaymentUtil.ShoppingCart; %>

<script language="c#" runat="server">
	public void Page_Load(object sender, EventArgs e)
    {
		var context = new Xrm.XrmServiceContext();
		var page = context.Adx_webpageSet.FirstOrDefault(p => p.Adx_webpageId == PortalContext.Current.Entity.Id);
		if (page != null)
		{
			CurrentEntity.DataItem = page;
		}
	}
</script>


<crm:CrmEntityDataSource ID="CurrentEntity" runat="server" />

<header class="<%: Html.AttributeLiteral("adx_imageurl") %> main-menu">
    <div class="navbar navbar-static-top hidden-md hidden-lg" role="navigation">
        <nav class="navbar-inner navbar-default">
		    <div class="container navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navHeaderCollapse" aria-expanded="false">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
			    <div class="pull-left hidden-md hidden-lg">
				    <%: Html.HtmlSnippet("Mobile Header") %>
			    </div>
            </div>
            <div class="collapse navbar-collapse navHeaderCollapse">
				<div class="pull-left">
					<%: Html.HtmlSnippet("Navbar Left") %>
				</div>
                <%: Html.WebLinksNavBar(navigationName, "hidden-md hidden-lg weblinks", currentSiteMapNodeCssClass: "active", ancestorSiteMapNodeCssClass: "active") %>
				<div class="hidden-md hidden-lg weblinks">
                    <ul class="nav navbar-nav pull-left">
					    <% if (Request.IsAuthenticated) { %>
						    <li class="dropdown">
							    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
								    <i class="fa fa-user"></i> <%: Html.AttributeLiteral(Html.PortalUser(), "fullname") %> <span class="caret"></span>
							    </a>
							    <ul class="dropdown-menu">
								    <% if (Html.BooleanSetting("Header/ShowAllProfileNavigationLinks").GetValueOrDefault(true)) { %>
									    <% var profileNavigation = Html.WebLinkSet("Profile Navigation"); %>
									    <% if (profileNavigation != null) { %>
										    <% foreach (var webLink in profileNavigation.WebLinks) { %>
											    <%: Html.WebLinkListItem(webLink, false, false) %>
										    <% } %>
									    <% } %>
								    <% } else { %>
									    <li><a href="<%: Html.SiteMarkerUrl("Profile") %>"><%: Html.SnippetLiteral("Profile Link Text", "Profile") %></a></li>
								    <% } %>
								    <li class="divider"></li>
								    <li><a href="<%: Url.Action("SignOut", "Account", new { area = "Account" }) %>"><%: Html.SnippetLiteral("links/logout", "Sign Out") %></a></li>
							    </ul>
						    </li>
					    <% } else { %>
						     <li>
							    <% Html.RenderPartial("SignInLink"); %>
						    </li>
					    <% } %>
				    </ul>
                </div>
				<ul class="nav navbar-nav pull-right">
					<% var relatedWebsites = Html.RelatedWebsites(linkTitleSiteSettingName:"Site Name"); %>
					<% if (relatedWebsites.Any) { %>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-globe"></i> <%: relatedWebsites.Current.Title %> <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<% foreach (var relatedWebsiteLink in relatedWebsites.Links) { %>
									<li><a href="<%: relatedWebsiteLink.Url %>"><%: relatedWebsiteLink.Title %></a></li>
								<% } %>
							</ul>
						</li>
					<% } %>
                    <%
                    
                    if (sc != null && sc.Details.Any())
                    {%>
					    <li class="shopping-cart-status" data-href="<%: Url.Action("Status", "ShoppingCart", new {area = "Commerce", __portalScopeId__ = Html.Website().EntityReference.Id}) %>">
						    <a href="<%: Html.SiteMarkerUrl("Shopping Cart") %>">
							    <i class="fa fa-shopping-cart"></i>
							    <%: Html.SnippetLiteral("Shopping Cart Status Link Text", "Cart") %>
							    <span class="count">(<span class="value"></span>)</span>
						    </a>
					    </li>
                    <%}%>
				</ul>
			</div>
        </nav>
    </div>
    <div class="masthead tabs visible-md visible-lg">
	    <div class="container">
	        <a href="http://www.etfo.ca/" class="etfo-brand"></a>
            <div class="page-image <%: underMyEtfo ? "myetfo-home" : "events-home" %>"></div>
			<h1 class="banner-title"><crm:Property runat="server" PropertyName="oems_bannertitle" EditType="html" Editable="True" DataSourceID="CurrentEntity"/></h1>
            <div class="top-nav-container navbar">
                <ul class="nav navbar-nav">
					<% if (Request.IsAuthenticated) { %>
						<li class="dropdown">
							<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-user"></i> <%: Html.AttributeLiteral(Html.PortalUser(), "fullname") %> <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<% if (Html.BooleanSetting("Header/ShowAllProfileNavigationLinks").GetValueOrDefault(true)) { %>
									<% var profileNavigation = Html.WebLinkSet("Profile Navigation"); %>
									<% if (profileNavigation != null) { %>
										<% foreach (var webLink in profileNavigation.WebLinks) { %>
											<%: Html.WebLinkListItem(webLink, false, false) %>
										<% } %>
									<% } %>
								<% } else { %>
									<li><a href="<%: Html.SiteMarkerUrl("Profile") %>"><%: Html.SnippetLiteral("Profile Link Text", "Profile") %></a></li>
								<% } %>
								<li class="divider"></li>
								<li><a href="<%: Url.Action("SignOut", "Account", new { area = "Account" }) %>"><%: Html.SnippetLiteral("links/logout", "Sign Out") %></a></li>
							</ul>
						</li>
					<% } else { %>
						<li>
							<% Html.RenderPartial("SignInLink"); %>
						</li>
					<% } %>
                    <li>
                        <a class="etfo-my-events <%: underMyEtfo ? "hidden-md hidden-lg hidden-sm hidden-xs" : string.Empty %>" href="<%: Html.SiteMarkerUrl("My ETFO") %>"><%: Html.SnippetLiteral("My ETFO Link Text", "My ETFO") %></a>
                    </li>
                    <li>
                        <a class="icon-etfo-link icon-etfo-contact" href="<%: Html.SiteMarkerUrl("Contact Us") %>">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <%
                            var helpText = "";
                            var path = this.Page.Request.Path.ToLower();
                            path = "Help/" + path.Trim(new[] {'/'});
                            if (!String.IsNullOrEmpty(path))
                            {
                                helpText = "";// XrmHelper.GetSnippetValueOrDefault(path);

                                if (String.IsNullOrEmpty(helpText))
                                {
                                    var more = true;
                                    do
                                    {
                                        var idx = path.LastIndexOf("/", System.StringComparison.Ordinal);
                                        if (idx != -1)
                                        { 
                                            var npath = path.Substring(0, idx);

                                            helpText = "";// XrmHelper.GetSnippetValueOrDefault(path);
                                            if (!String.IsNullOrEmpty(helpText))
                                            {
                                                more = false;
                                            }
                                            path = npath;
                                        }
                                        else
                                        {
                                            more = false;
                                        }
                                    }
                                    while (more);
                                }
                            }

                            if (String.IsNullOrEmpty(helpText))
                            {
                                helpText = "Help";
                            }
                        %>
                        <a class="icon-etfo-link icon-etfo-help" href="javascript:void(0);">
                            <i class="fa fa-question" aria-hidden="true"></i>
                        </a>
                        
                        <!-- Modal -->
                        <div id="helpModal" class="modal fade" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Help</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p><%= Html.Raw(helpText) %></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <script type="text/javascript">
                            $(function () {
                                $('#helpModal').modal
                                ({
                                    keyboard: false,
                                    backdrop: true,
                                    show: false
                                });

                                $(".icon-etfo-help").click
                                (
                                    function ()
                                    {
                                        $('#helpModal').modal('show');
                                    }
                                );
                            });
                        </script>
                    </li>
					<li class=" ">
						<a class="icon-etfo-accessibility" role="button" data-target="#" href="#" id="dAccessibility">
							<%--<img class="icon-wheelchair" src="../../img/wheelchair.png"/>--%>
						</a>
						<ul class="accessibility-controls dropdown-menu" role="menu" aria-labelledby="dAccessibility">
							<li>
								<div class="font-resizer">
									<a class="decrease-font" href="#">-A</a>
									<div class="font-slider"></div>
									<a class="increase-font" href="#">+A</a>
								</div>
								<div class="accessibility-close">
									<i class="fa fa-remove"></i>
								</div>
							</li>
						</ul>
                    </li>
                    <%
                    if (sc != null && sc.Details.Any())
                    {
                    %>
                        <li>
                            <a class="icon-etfo-link icon-etfo-shoppingcart" href="/payment/shoppingcart">
                                <i class='fa fa-shopping-cart'></i>
							    <span class="shoppingcart-count"><%= sc.Details.Count().ToString()%></span>
                            </a>
                        </li>
                    <%
                    } 
                    %>
				</ul>
            </div>
			
			<div class="nav-container">
                <%: Html.WebLinksDropdowns(navigationName, "weblinks", "nav nav-tabs", currentSiteMapNodeCssClass: "active", ancestorSiteMapNodeCssClass: ancestorCSS) %>
            </div>
	    </div>
    </div>
</header>
