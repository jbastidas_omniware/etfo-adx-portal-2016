﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<a href="<%: Html.SignInUrl() %>">
	<i class="fa fa-lock" aria-hidden="true"></i>
	<%: Html.SnippetLiteral("links/login", "Sign In") %>
</a>