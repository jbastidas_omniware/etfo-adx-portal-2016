﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Profile;
using System.Web.Routing;
using System.Web.Security;
using System.Web.WebPages;
using Adxstudio.Xrm.Web;
using Adxstudio.Xrm.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Xrm.Portal.Configuration;
using Site.Areas.Account.Models;
using log4net;
using System.Security.Principal;
using log4net.Config;
using System.IO;
using System.Web.Helpers;
using System.Web.Http;
using Microsoft.IdentityModel.Claims;
using Site.Library;
using System.Net;
using Site.Areas.Payment.Library;

namespace Site
{
	public class Global : HttpApplication
	{
	    private static bool _setupRunning;

        private ILog Log = LogManager.GetLogger(typeof(Global));

		void Application_Start(object sender, EventArgs e)
		{
		    _setupRunning = SetupConfig.ApplicationStart();
            
			if (_setupRunning)
                return;

            SetupLog4Net(Server.MapPath("\\"));

            this.ConfigurePortal();

			var areaRegistrationState = new PortalAreaRegistrationState();
			Application[typeof (IPortalAreaRegistrationState).FullName] = areaRegistrationState;

			AreaRegistration.RegisterAllAreas(areaRegistrationState);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Customize Json provider
            //find the default JsonVAlueProviderFactory
            JsonValueProviderFactory jsonValueProviderFactory = null;
            foreach (var factory in ValueProviderFactories.Factories)
            {
                if (factory is JsonValueProviderFactory)
                {
                    jsonValueProviderFactory = factory as JsonValueProviderFactory;
                }
            }

            //remove the default JsonVAlueProviderFactory
		    if (jsonValueProviderFactory != null)
		    {
		        ValueProviderFactories.Factories.Remove(jsonValueProviderFactory);
		    }

		    //add the custom one
            ValueProviderFactories.Factories.Add(new CustomJsonValueProviderFactory());

            // fixes error when navigating back to login page in login state  would cause AntifogeryToken to generate exception
            //http://stack247.wordpress.com/2013/02/22/antiforgerytoken-a-claim-of-type-nameidentifier-or-identityprovider-was-not-present-on-provided-claimsidentity/
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Name;

            RouteTable.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = System.Web.Http.RouteParameter.Optional }
            );


            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

            Log.Info("Application Startup completed ");
        }

        protected void Session_Start(Object sender, EventArgs e)
		{
			Session["init"] = 0;
		}

        protected void Session_End(Object sender, EventArgs E)
        {
            PaymentUtil.Clean();
        }

        protected void Application_PostAuthenticateRequest()
		{
		    if (_setupRunning) return;

            this.OnPostAuthenticateRequest();
		}

        public static void SetupLog4Net(string appRootDirectory)
        {
            //todo add Release/Prod config to run outside
            GlobalContext.Properties["LogDir"] = appRootDirectory + @"App_Data\Logs";

            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            string appPoolName = identity != null
                                     ? identity.Name.Replace("IIS APPPOOL", "").Replace("\\", "").Replace(" ", "_")
                                     : string.Empty;

            GlobalContext.Properties["Identity"] = appPoolName;

            XmlConfigurator.Configure(new FileInfo(appRootDirectory + @"log4net.config"));
        }

        private void Application_Error(object sender, EventArgs e)
        {
            Exception error = Server.GetLastError();
            Log.Error("Glocal Unhandled exception: " + error.Message, error);
            if (error.InnerException != null)
            {
                Log.Error("Glocal Unhandled exception (Inner Ex Message): " + error.InnerException.Message);
            }

        }

        public override string GetVaryByCustomString(HttpContext context, string arg)
		{
			switch (arg)
			{
				case "roles":
					return GetVaryByRolesString(context);
				case "roles;website":
					return GetVaryByRolesAndWebsiteString(context);
				case "user":
					return GetVaryByUserString(context);
				case "user;website":
					return GetVaryByUserAndWebsiteString(context);
				case "website":
					return GetVaryByWebsiteString(context);
			}

			return base.GetVaryByCustomString(context, arg);
		}

		public void Profile_MigrateAnonymous(object sender, ProfileMigrateEventArgs e)
		{
			var portalAreaRegistrationState = Application[typeof (IPortalAreaRegistrationState).FullName] as IPortalAreaRegistrationState;

			if (portalAreaRegistrationState != null)
			{
				portalAreaRegistrationState.OnProfile_MigrateAnonymous(sender, e);
			}
		}

		void Application_EndRequest(object sender, EventArgs e)
		{
			this.RedirectOnStatusCode();
		}

		private static string GetVaryByDisplayModesString(HttpContext context)
		{
			var availableDisplayModeIds = DisplayModeProvider.Instance
				.GetAvailableDisplayModesForContext(context.Request.RequestContext.HttpContext, null)
				.Select(mode => mode.DisplayModeId);

			return string.Join(",", availableDisplayModeIds);
		}

		private static string GetVaryByRolesString(HttpContext context)
		{
			// If the role system is not enabled, fall back to varying cache by user.
			if (!Roles.Enabled)
			{
				return GetVaryByUserString(context);
			}

			var roles = context.User != null && context.User.Identity != null && context.User.Identity.IsAuthenticated
				? Roles.GetRolesForUser()
					.OrderBy(role => role)
					.Aggregate(new StringBuilder(), (sb, role) => sb.AppendFormat("[{0}]", role))
					.ToString()
				: string.Empty;

			return string.Format("IsAuthenticated={0},Roles={1},DisplayModes={2}",
				context.Request.IsAuthenticated,
				roles.GetHashCode(),
				GetVaryByDisplayModesString(context).GetHashCode());
		}

		private static string GetVaryByRolesAndWebsiteString(HttpContext context)
		{
			return string.Format("{0},{1}", GetVaryByRolesString(context), GetVaryByWebsiteString(context));
		}

		private static string GetVaryByUserString(HttpContext context)
		{
			var identity = context.User != null ? context.User.Identity.Name : string.Empty;

			return string.Format("IsAuthenticated={0},Identity={1},DisplayModes={2}",
				context.Request.IsAuthenticated,
				identity,
				GetVaryByDisplayModesString(context).GetHashCode());
		}

		private static string GetVaryByUserAndWebsiteString(HttpContext context)
		{
			return string.Format("{0},{1}", GetVaryByUserString(context), GetVaryByWebsiteString(context));
		}

		private static string GetVaryByWebsiteString(HttpContext context)
		{
			var websiteId = GetWebsiteIdFromOwinContext(context) ?? GetWebsiteIdFromPortalContext(context);

			return websiteId == null ? "Website=" : string.Format("Website={0}", websiteId.GetHashCode());
		}

		private static string GetWebsiteIdFromOwinContext(HttpContext context)
		{
			var owinContext = context.GetOwinContext();

			if (owinContext == null)
			{
				return null;
			}

			var website = owinContext.Get<ApplicationWebsite>();

			return website == null ? null : website.Id;
		}

		private static string GetWebsiteIdFromPortalContext(HttpContext context)
		{
			var portalContext = PortalCrmConfigurationManager.CreatePortalContext(request: context.Request.RequestContext);

			if (portalContext == null)
			{
				return null;
			}

			return portalContext.Website == null ? null : portalContext.Website.Id.ToString();
		}
	}
}
