﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Profile.master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="Site.Pages.ChangePassword" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
    <asp:UpdatePanel ID="ChangeCredentialsUpdateResultPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="ChangeCredentialsResultPanel" runat="server" CssClass="alert" Visible="false">
                <asp:Literal ID="ChangeCredentialsResultLiteral" runat="server"></asp:Literal>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UsernameUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
	        <asp:Panel ID="ChangePasswordPanel" runat="server">
				<div class="form-horizontal">
					<fieldset>
						<div class="form-group">
							<label class="col-sm-4 control-label required" for="CurrentPassword"><adx:Snippet runat="server" SnippetName="Profile Change Password Current Password Label" DefaultText="Current Password"/></label>
							<div class="col-sm-8">
                                <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password" CssClass="input-xlarge"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword" ValidationGroup="ChangePassword" Display="Dynamic" 
                                    ErrorMessage="Current Password is a required field." Text="" CssClass="help-inline error"></asp:RequiredFieldValidator>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label required" for="NewPassword"><adx:Snippet runat="server" SnippetName="Profile Change Password New Password Label" DefaultText="New Password"/></label>
							<div class="col-sm-8">
                                <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" CssClass="input-xlarge"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword" ValidationGroup="ChangePassword" Display="Dynamic" 
                                    ErrorMessage="New Password is a required field." Text="" CssClass="help-inline error"></asp:RequiredFieldValidator>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label required" for="ConfirmNewPassword"><adx:Snippet runat="server" SnippetName="Profile Change Password Confirm New Password Label" DefaultText="Confirm New Password"/></label>
							<div class="col-sm-8">
                                <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password" CssClass="input-xlarge"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword" ValidationGroup="ChangePassword" 
                                    Display="Dynamic" ErrorMessage="Confirm New Password is a required field." Text="" CssClass="help-inline error"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" ValidationGroup="ChangePassword"
                                    Display="Dynamic" ErrorMessage="The New Password and Confirm New Password must match." Text="" CssClass="help-inline error" />
							</div>
						</div>
                        <div class="form-actions">
                            <div class="col-sm-9"></div>
                            <div>
                                <asp:Button ID="ChangePasswordButton" runat="server" CssClass="btn btn-primary" Text="<%$ Snippet: My Account Change Security Question Button, Update %>"
                                    ValidationGroup="ChangePassword" CausesValidation="true" OnClick="ChangePasswordButton_OnClick" />
                            </div>
						</div>
					</fieldset>
				</div>
	        </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/css/assets/img/ajax-modal-loading.gif" AlternateText="Loading ..." ToolTip="Loading ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
	<asp:Panel ID="FederatedIdentityMessage" Visible="False" CssClass="alert alert-block alert-info" runat="server">
		<% var federatedIdentityMessageSnippet = Html.HtmlSnippet("Profile Change Password Federated Identity Message"); %>
		<% if (federatedIdentityMessageSnippet != null) { %>
			<%: federatedIdentityMessageSnippet %>
		<% } else { %>
            <%: Html.HtmlSnippet("Profile Change Password Federated Identity Message", defaultValue: "<p>You are signed in using an identity provider, rather than a local account. Please update your password through your identity provider, if applicable.</p>") %>
		<% } %>
	</asp:Panel>
</asp:Content>
