﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Collections.Generic;
using Adxstudio.Xrm.Configuration;
using Adxstudio.Xrm.Web.Mvc;
using Adxstudio.Xrm.Web.Mvc.Html;
using Microsoft.IdentityModel.Web;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Portal;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Portal.IdentityModel.Configuration;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Portal.Web.Security;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Xrm;

namespace Site.Pages
{
    public class PortalPage : PortalViewPage
    {
        public const string WebAnnotationPrefix = "*WEB*";

        private readonly Lazy<XrmServiceContext> _xrmContext;

        public PortalPage()
        {
            _xrmContext = new Lazy<XrmServiceContext>(() => CreateXrmServiceContext());
        }

        /// <summary>
        /// A general use <see cref="OrganizationServiceContext"/> for managing entities on the page.
        /// </summary>
        public XrmServiceContext XrmContext
        {
            get { return _xrmContext.Value; }
        }

        /// <summary>
        /// The current <see cref="IPortalContext"/> instance.
        /// </summary>
        public IPortalContext Portal
        {
            get { return PortalCrmConfigurationManager.CreatePortalContext(PortalName); }
        }

        /// <summary>
        /// The <see cref="OrganizationServiceContext"/> that is associated with the current <see cref="IPortalContext"/> and used to manage its entities.
        /// </summary>
        /// <remarks>
        /// This <see cref="OrganizationServiceContext"/> instance should be used when querying against the Website, User, or Entity properties.
        /// </remarks>
        public XrmServiceContext ServiceContext
        {
            get { return Portal.ServiceContext as XrmServiceContext; }
        }

        /// <summary>
        /// The current <see cref="Adx_website"/>.
        /// </summary>
        public Adx_website Website
        {
            get { return Portal.Website as Adx_website; }
        }

        /// <summary>
        /// The current <see cref="Contact"/>.
        /// </summary>
        public Contact Contact
        {
            get { return Portal.User as Contact; }
        }

        /// <summary>
        /// The <see cref="Entity"/> representing the current page.
        /// </summary>
        public Entity Entity
        {
            get { return Portal.Entity; }
        }

        protected XrmServiceContext CreateXrmServiceContext(MergeOption? mergeOption = null)
        {
            var context = PortalCrmConfigurationManager.CreateServiceContext(PortalName) as XrmServiceContext;
            if (context != null && mergeOption != null) context.MergeOption = mergeOption.Value;
            return context;
        }

        private readonly IDictionary<int, string> _campaignStatusLabelCache = new Dictionary<int, string>();

        protected string GetCampaignStatusLabel(object dataItem)
        {
            var campaign = dataItem as Campaign;

            if (campaign == null || campaign.StatusCode == null)
            {
                return string.Empty;
            }

            string cachedLabel;

            if (_campaignStatusLabelCache.TryGetValue(campaign.StatusCode.Value, out cachedLabel))
            {
                return cachedLabel;
            }

            var response = (RetrieveAttributeResponse)ServiceContext.Execute(new RetrieveAttributeRequest
            {
                EntityLogicalName = campaign.LogicalName,
                LogicalName = "statuscode"
            });

            var statusCodeMetadata = response.AttributeMetadata as StatusAttributeMetadata;

            if (statusCodeMetadata == null)
            {
                return string.Empty;
            }

            var option = statusCodeMetadata.OptionSet.Options.FirstOrDefault(o => o.Value == campaign.StatusCode);

            if (option == null)
            {
                return string.Empty;
            }

            var label = option.Label.UserLocalizedLabel.Label;

            if (option.Value.HasValue)
            {
                _campaignStatusLabelCache[option.Value.Value] = label;
            }

            return label;
        }

        protected UrlBuilder GetUrlForRequiredSiteMarker(string siteMarkerName)
        {
            var page = ServiceContext.GetPageBySiteMarkerName(Website, siteMarkerName);

            if (page == null)
            {
                throw new Exception("Please contact your System Administrator. Required Site Marker '{0}' is missing.".FormatWith(siteMarkerName));
            }

            var path = ServiceContext.GetUrl(page);

            if (path == null)
            {
                throw new Exception("Please contact your System Administrator. Unable to build URL for Site Marker '{0}'.".FormatWith(siteMarkerName));
            }

            return new UrlBuilder(path);
        }

        protected virtual void LinqDataSourceSelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Arguments.RetrieveTotalRowCount = false;
        }

        protected void RedirectToLoginIfAnonymous()
        {
            if (!Request.IsAuthenticated)
            {
                var loginPage = ServiceContext.GetPageBySiteMarkerName(Website, "Login");

                var settings = FederationCrmConfigurationManager.GetUserRegistrationSettings();
                var returnUrlKey = settings.ReturnUrlKey ?? "returnurl";
                Response.Redirect(ServiceContext.GetUrl(loginPage).AppendQueryString(returnUrlKey, Request.Url.PathAndQuery));
            }
        }

        protected void RedirectToLoginIfNecessary()
        {
            if (Request.IsAuthenticated)
                return;

            var portal = PortalCrmConfigurationManager.CreatePortalContext();

            var portalPage = portal.Entity as Adx_webpage;

            if (portalPage == null)
            {
                return;
            }

            var webPage = ServiceContext.Adx_webpageSet.FirstOrDefault(w => w.Adx_webpageId == portalPage.Id);

            if (webPage == null || webPage.adx_webpage_sitemarker.Any(s => s.Adx_name == "Login" || s.Adx_name == "Page Not Found"))
            {
                return;
            }

            var loginPage = portal.ServiceContext.GetPageBySiteMarkerName(portal.Website, "Login");

            var settings = FederationCrmConfigurationManager.GetUserRegistrationSettings();
            var returnUrlKey = settings.ReturnUrlKey ?? "returnurl";
            Response.Redirect(portal.ServiceContext.GetUrl(loginPage).AppendQueryString(returnUrlKey, Request.Path));
        }

        protected void ShowModalInfo(string scriptName, string title, string description, string goTotext, string goToUrl)
        {
            var script = String.Format("$(function() {{ showInfoModal('{0}', '{1}', '{2}', '{3}'); }});", title, description, goTotext, goToUrl);
            ClientScript.RegisterStartupScript(this.GetType(), scriptName, script, true);
        }
    }
}