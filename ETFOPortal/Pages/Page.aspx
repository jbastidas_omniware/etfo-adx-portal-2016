<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="True" CodeBehind="Page.aspx.cs" Inherits="Site.Pages.Page" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Register src="~/Controls/Comments.ascx" tagname="Comments" tagprefix="site" %>
<%@ Register src="~/Controls/MultiRatingControl.ascx" tagname="MultiRatingControl" tagprefix="site" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<div class="page-metadata clearfix">
		<div class="page-last-updated">
			<crm:Snippet ID="Snippet1" SnippetName="Page Modified On Prefix" DefaultText="This page was last updated" EditType="text" runat="server"/>
			<abbr class="timeago">
				<%: string.Format("{0:r}", Html.AttributeLiteral("modifiedon")) %>
			</abbr>.
			<% var displayDate = Html.AttributeLiteral("adx_displaydate"); %>
			<% if (displayDate != null) { %>
				<crm:Snippet ID="Snippet2" SnippetName="Page Published On Prefix" DefaultText="It was published" EditType="text" runat="server"/>
				<abbr class="timeago">
					<%: string.Format("{0:r}", displayDate) %>
				</abbr>.
			<% } %>
		</div>
		<crm:Snippet ID="Snippet3" SnippetName="Social Share Widget Code Page Bottom" EditType="text" DefaultText="" runat="server"/>
	</div>
	<site:Comments RatingType="vote" EnableRatings="False" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="SidebarBottom" runat="server">
	<site:MultiRatingControl ID="MultiRatingControl" RatingType="rating" IsPanel="True" runat="server" />
<%--	<% Html.RenderAction("PollPlacement", "Poll", new {Area = "Cms", id = "Sidebar", __portalScopeId__ = Website.Id}); %>
	<% Html.RenderAction("AdPlacement", "Ad", new { Area = "Cms", id = "Sidebar Bottom", __portalScopeId__ = Website.Id }); %>--%>
</asp:Content>