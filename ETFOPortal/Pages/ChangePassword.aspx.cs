﻿using System;
using System.Text.RegularExpressions;
using System.Web.Security;
using Adxstudio.Xrm.IdentityModel;
using Adxstudio.Xrm.Web.Security;
using Microsoft.IdentityModel.Claims;
using Site.Helpers;

namespace Site.Pages
{
	public partial class ChangePassword : PortalPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfAnonymous();

			IClaimsIdentity identity;

			var isFederatedIdentity = Context.User.TryGetFederatedIdentity(out identity);

			ChangePasswordPanel.Visible = !isFederatedIdentity;
			FederatedIdentityMessage.Visible = isFederatedIdentity;
		}

        protected void ChangePasswordButton_OnClick(object sender, EventArgs e)
        {
            try
            {
                ChangeCredentialsResultPanel.Visible = false;

                var newPassword = NewPassword.Text;
                if (newPassword.Contains(" "))
                {
                    ShowMessage(XrmHelper.GetSnippetValueOrDefault("My Account Change Password Regex Error Message",
                        "Your password does not meet the minimum requirements. Passwords must not have spaces, must have at least 1 digit, 1 lower case, 1 upper case character and be at least 6 characters long."), "warning");

                    return;
                }

                var passwordRegex = XrmHelper.GetSiteSettingValueOrDefault("PasswordValidationRegex", @"^(?=.*\d)(?=.*[a-z\\s])(?=.*[A-Z\\s]).{6,}$");
                if (!Regex.Match(newPassword, passwordRegex).Success)
                {
                    ShowMessage(XrmHelper.GetSnippetValueOrDefault("My Account Change Password Regex Error Message",
                        "Your password does not meet the minimum requirements. Passwords must not have spaces, must have at least 1 digit, 1 lower case, 1 upper case character and be at least 6 characters long."), "warning");

                    return;
                }

                var membershipProvider = (CrmContactMembershipProvider)Membership.Provider;
                var oldPassword = CurrentPassword.Text.Trim();
                var changeResult = membershipProvider.ChangePassword(Contact.Adx_username, oldPassword, newPassword);

                if (changeResult)
                {
                    ShowMessage(XrmHelper.GetSnippetValueOrDefault("My Account Change Password Success Message",
                      "Your password has been changed successfully.."), "success");
                }
                else
                {
                    ShowMessage(XrmHelper.GetSnippetValueOrDefault("My Account Change Password Invalid Message",
                      "Your current password doesn't match. Please try again"), "warning");
                }
            }
            catch (Exception x)
            {
                ShowMessage(XrmHelper.GetSnippetValueOrDefault("My Account Change Passworrd Update Request Error Message",
                    "There was a problem updating your password."), "warning");
            }
        }


        private void ShowMessage(string msg, string type)
        {
            ChangeCredentialsResultPanel.Visible = true;
            ChangeCredentialsResultLiteral.Text = msg;
            ChangeCredentialsResultPanel.CssClass = "alert " + string.Format(" alert-{0}", type);

            ChangeCredentialsUpdateResultPanel.Update();
        }

    }
}