﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Site.Library;
using Xrm;
using Microsoft.Xrm.Client.Services;

namespace Site.Helpers
{
    public static class AnnotationsHelper
    {
        public static void CreateAnnotation<T>(Annotation annotation, T entity, XrmServiceContext currentContext = null) where T : Entity
        {
            annotation.ObjectId = entity.ToEntityReference();
            annotation.ObjectTypeCode = entity.LogicalName;

            if (string.IsNullOrWhiteSpace(annotation.MimeType))
            {
                var extension = Path.GetExtension(annotation.FileName);
                annotation.MimeType = MimeTypeHelper.GetMimeTypeFromExtension(extension);
            }

            var context = currentContext != null ? currentContext : XrmHelper.GetContext(); 
            annotation.Id = context.Create(annotation);
            annotation.AnnotationId = annotation.Id;
        }

        public static void DeleteAnnotation(Guid id)
        {
            var context = XrmHelper.GetContext();
            context.Delete("annotation", id);
        }

        public static List<CustomAnnotation> GetAnnotations(Guid objectId, bool? isDocument)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var query = serviceContext.AnnotationSet.Where(a => a.ObjectId.Id == objectId);
                if (isDocument != null)
                {
                    query = query.Where(a => a.IsDocument == true);
                }

                var annotations =
                (
                    query.Select
                    (
                        a => new CustomAnnotation()
                        {
                            Id = a.Id,
                            AnnotationId = a.AnnotationId,
                            IsDocument = a.IsDocument,
                            FileName = a.FileName,
                            FileSize = a.FileSize,
                            MimeType = a.MimeType,
                            Subject = a.Subject
                        }
                    )
                ).ToList();

                return annotations;
            }
        }

        public static int GetAnnotationFileSize(Guid id)
        {
            using (var serviceContext = new XrmServiceContext())
            {
                var query = serviceContext.AnnotationSet.Where(a => a.Id == id);

                var annotation =
                (
                    query.Select
                    (
                        a => new CustomAnnotation()
                        {
                            FileSize = a.FileSize
                        }
                    )
                ).ToList().FirstOrDefault();

                return annotation != null ? (annotation.FileSize ?? 0) : 0;
            }
        }

        public static byte[] GetAnnotationDocumentBody(Guid id, out string fileName, out string mimetype)
        {
            fileName = "";
            mimetype = "";

            using (var serviceContext = new XrmServiceContext())
            {
                var query = serviceContext.AnnotationSet.Where(a => a.Id == id);

                var annotation =
                (
                    query.Select
                    (
                        a => new CustomAnnotation()
                        {
                            DocumentBody = a.DocumentBody,
                            FileName = a.FileName,
                            MimeType = a.MimeType
                        }
                    )
                ).ToList().FirstOrDefault();

                if (annotation != null)
                {

                    fileName = annotation.FileName;
                    mimetype = annotation.MimeType;

                    return !String.IsNullOrEmpty(annotation.DocumentBody) ? Convert.FromBase64String(annotation.DocumentBody) : null;
                }
            }

            return null;
        }

        public static byte[] GetAnnotationDocumentBodyByObjectId(Guid id, out string fileName, out string mimetype)
        {
            fileName = "";
            mimetype = "";

            using (var serviceContext = new XrmServiceContext())
            {
                var query = serviceContext.AnnotationSet.Where(a => a.ObjectId.Id == id);

                var annotation =
                (
                    query.Select
                    (
                        a => new CustomAnnotation()
                        {
                            DocumentBody = a.DocumentBody,
                            FileName = a.FileName,
                            MimeType = a.MimeType
                        }
                    )
                ).ToList().FirstOrDefault();

                if (annotation != null)
                {
                    fileName = annotation.FileName;
                    mimetype = annotation.MimeType;

                    return !String.IsNullOrEmpty(annotation.DocumentBody) ? Convert.FromBase64String(annotation.DocumentBody) : null;
                }
            }

            return null;
        }

    }
}
