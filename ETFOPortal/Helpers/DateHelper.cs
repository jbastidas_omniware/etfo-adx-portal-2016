﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Xrm.Portal;

namespace Site.Helpers
{
    public static class DateHelper
    {
        /// <summary>
        /// x - crm UTC time
        /// y - time to compare with
        /// Portal - from Portal Page
        /// Settings timezone/id and tiezone/name should be defined in the portal!
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="Portal"></param>
        /// <returns></returns>
        public static int timeComparer(DateTime x, DateTime y, IPortalContext Portal)
        {
            DateTime current = ConvertFromUtc_Time(Portal, x);
            y = ConvertFromUtc_Time(Portal, y);

            if (current > y) return 1;
            if (current < y) return -1;

            return 0;

        }


        public static DateTime ConvertFromUtc_Time(IPortalContext portal, DateTime d)
        {
            try
            {
                //var context = portal.ServiceContext;
                //var website = portal.Website;

                //var timezoneid = XrmHelper.GetSiteSettingValueOrDefault("timezone/id");
                //var timezonename = XrmHelper.GetSiteSettingValueOrDefault("timezone/name");

                //var tz = TimeZoneInfo.FindSystemTimeZoneById(timezoneid);
                //var nt = TimeZoneInfo.ConvertTimeFromUtc(d, tz);
                //return nt;

                if (d.Kind == DateTimeKind.Utc)
                {
                    TimeZoneInfo timeZone = portal.GetTimeZone();
                    d = TimeZoneInfo.ConvertTimeFromUtc(d, timeZone);
                }

                return d;
            }
            catch
            {
                return d;
            }
        }


        public static string ConvertFromUtc(IPortalContext portal, DateTime d, bool addTimeName)
        {
            try
            {
                //var context = portal.ServiceContext;
                //var website = portal.Website;

                //var timezoneid = XrmHelper.GetSiteSettingValueOrDefault("timezone/id");
                //var timezonename = XrmHelper.GetSiteSettingValueOrDefault("timezone/name");

                //var tz = TimeZoneInfo.FindSystemTimeZoneById(timezoneid);
                //var nt = TimeZoneInfo.ConvertTimeFromUtc(d, tz);
                //return nt + (addTimeName ? (" " + timezonename) : string.Empty);

                if (d.Kind == DateTimeKind.Utc)
                {
                    TimeZoneInfo timeZone = portal.GetTimeZone();
                    string timeZoneLabel = timeZone.DisplayName;
                    d = TimeZoneInfo.ConvertTimeFromUtc(d, timeZone);

                    return d + (addTimeName ? (" " + timeZoneLabel) : string.Empty);
                }

                return d.ToString();

            }
            catch
            {
                return d.ToString();
            }
        }


        public static string ConvertFromUtc(IPortalContext portal, DateTime? d, bool addTimeName = true)
        {
            return d.HasValue ? ConvertFromUtc(portal, d.Value, addTimeName) : "";
        }


    }
}