﻿using System;
using Microsoft.Xrm.Portal;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Xrm;

namespace Site.Helpers
{
    /// <summary>
    /// The portal class.
    /// </summary>
    public class PortalClass
    {
        #region Fields

        /// <summary>
        /// The XRM context.
        /// </summary>
        private readonly Lazy<XrmServiceContext> xrmContext;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PortalClass"/> class.
        /// </summary>
        public PortalClass()
        {
            this.xrmContext = new Lazy<XrmServiceContext>(() => this.CreateXrmServiceContext());
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the current <see cref="Contact"/>.
        /// </summary>
        public Contact Contact
        {
            get
            {
                return this.Portal.User as Contact;
            }
        }

        /// <summary>
        /// Gets The <see cref="Entity"/> representing the current page.
        /// </summary>
        public Entity Entity
        {
            get
            {
                return this.Portal.Entity;
            }
        }

        /// <summary>
        /// Gets the current <see cref="IPortalContext"/> instance.
        /// </summary>
        public IPortalContext Portal
        {
            get
            {
                return PortalCrmConfigurationManager.CreatePortalContext(this.PortalName);
            }
        }

        /// <summary>
        /// Gets or sets the selected portal configuration Name.
        /// </summary>
        public string PortalName { get; set; }

        /// <summary>
        /// Gets the <see cref="OrganizationServiceContext"/> that is associated with the current <see cref="IPortalContext"/> and used to manage its entities.
        /// </summary>
        /// <remarks>
        /// This <see cref="OrganizationServiceContext"/> instance should be used when querying against the Website, User, or Entity properties.
        /// </remarks>
        public XrmServiceContext ServiceContext
        {
            get
            {
                return this.Portal.ServiceContext as XrmServiceContext;
            }
        }

        /// <summary>
        /// Gets the current <see cref="Adx_website"/>.
        /// </summary>
        public Adx_website Website
        {
            get
            {
                return this.Portal.Website as Adx_website;
            }
        }

        /// <summary>
        /// Gets a general use <see cref="OrganizationServiceContext"/> for managing entities on the page.
        /// </summary>
        public XrmServiceContext XrmContext
        {
            get
            {
                return this.xrmContext.Value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The create XRM service context.
        /// </summary>
        /// <param name="mergeOption">
        /// The merge option.
        /// </param>
        /// <returns>
        /// Returns the service context
        /// </returns>
        protected XrmServiceContext CreateXrmServiceContext(MergeOption? mergeOption = null)
        {
            var context = PortalCrmConfigurationManager.CreateServiceContext(this.PortalName) as XrmServiceContext;
            if (context != null && mergeOption != null)
            {
                context.MergeOption = mergeOption.Value;
            }

            return context;
        }

        #endregion
    }
}