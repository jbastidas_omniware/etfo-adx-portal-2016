﻿// <copyright file="SiteMarkerHelper.cs" company="HP Enterprise Services">
//   (C) Copyright 2012 HP Enterprise Services, Inc. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Web;
using Microsoft.Xrm.Client;

namespace Site.Helpers
{
    using System.Linq;
    using Microsoft.Xrm.Portal.Cms;
    using Microsoft.Xrm.Sdk;
    using Xrm;
    using Microsoft.Xrm.Portal.Web;

    /// <summary>
    /// The site marker helper.
    /// </summary>
    public class SiteMarkerHelper : PortalClass
    {
        /// <summary>
        /// The is site marker.
        /// </summary>
        /// <param name="webpage">
        /// The adx webpage entity
        /// </param>
        /// <param name="siteMarker">
        /// The site marker.
        /// </param>
        /// <returns>
        /// Test if this is site marker.
        /// </returns>
        public bool IsSiteMarker(Entity webpage, string siteMarker)
        {
            var siteMarkerPage = ServiceContext.GetPageBySiteMarkerName(Website, siteMarker);
            return webpage != null && webpage.Id == siteMarkerPage.Id;
        }

        /// <summary>
        /// The in site marker.
        /// </summary>
        /// <param name="page">
        /// The page.
        /// </param>
        /// <param name="siteMarker">
        /// The site marker.
        /// </param>
        /// <returns>
        /// Test if the page is in site marker.
        /// </returns>
        public bool InSiteMarker(Adx_webpage page, string siteMarker)
        {
            return this.IsSiteMarker(page, siteMarker) || this.UnderSiteMarker(page, siteMarker);
        }

        /// <summary>
        /// The under site marker.
        /// </summary>
        /// <param name="page">
        /// The page.
        /// </param>
        /// <param name="siteMarker">
        /// The site marker.
        /// </param>
        /// <returns>
        /// Test if the page is under site marker.
        /// </returns>
        public bool UnderSiteMarker(Adx_webpage page, string siteMarker)
        {
            var siteMarkerPage = ServiceContext.GetPageBySiteMarkerName(Website, siteMarker);

            var isSiteMarkerNodeFound = false;

            page = this.GetParentPage(page);
            while (!isSiteMarkerNodeFound && page != null)
            {
                if (page.Id == siteMarkerPage.Id)
                {
                    isSiteMarkerNodeFound = true;
                }

                page = this.GetParentPage(page);
            }

            return isSiteMarkerNodeFound;
        }

        /// <summary>
        /// The under site marker.
        /// </summary>
        /// <param name="page">
        /// The page.
        /// </param>
        /// <param name="siteMarker">
        /// The site marker.
        /// </param>
        /// <returns>
        /// Test if the page is equal to or under site marker.
        /// </returns>
        public bool EqualOrUnderSiteMarker(Adx_webpage page, string siteMarker)
        {
            var siteMarkerPage = ServiceContext.GetPageBySiteMarkerName(Website, siteMarker);

            if (siteMarkerPage.Id == page.Id)
                return true;

            var isSiteMarkerNodeFound = false;

            page = this.GetParentPage(page);
            while (!isSiteMarkerNodeFound && page != null)
            {
                if (page.Id == siteMarkerPage.Id)
                {
                    isSiteMarkerNodeFound = true;
                }

                page = this.GetParentPage(page);
            }

            return isSiteMarkerNodeFound;
        }

        /// <summary>
        /// Test if the current node is equal or below the site markers site map node
        /// </summary>
        /// <param name="currentNode"></param>
        /// <param name="siteMarker"></param>
        /// <returns></returns>
        public bool EqualOrUnderSiteMarker(SiteMapNode currentNode, string siteMarker)
        {
            var siteMarkerPage = ServiceContext.GetPageBySiteMarkerName(Website, siteMarker);

            var siteMarkerNode = System.Web.SiteMap.Provider.FindSiteMapNodeFromKey(ServiceContext.GetUrl(siteMarkerPage));

            var isSiteMarkerNodeFound = false;

            while (!isSiteMarkerNodeFound && currentNode != null)
            {
                if (Equals(currentNode, siteMarkerNode))
                    isSiteMarkerNodeFound = true;

                currentNode = currentNode.ParentNode;
            }

            return isSiteMarkerNodeFound;
        }

        /// <summary>
        /// The under site marker.
        /// </summary>
        public string SiteMarkerUrl(string siteMarker)
        {
            var siteMarkerPage = ServiceContext.GetPageBySiteMarkerName(Website, siteMarker);
            if (siteMarkerPage == null)
            {
                throw new Exception("Please contact your System Administrator. Required Site Marker '{0}' is missing.".FormatWith(siteMarker));
            }

            var path = ServiceContext.GetUrl(siteMarkerPage);

            if (path == null)
            {
                throw new Exception("Please contact your System Administrator. Unable to build URL for Site Marker '{0}'.".FormatWith(siteMarker));
            }

            var url = new UrlBuilder(path);
            return url;
        }

        /// <summary>
        /// The get parent page.
        /// </summary>
        /// <param name="page">
        /// The page.
        /// </param>
        /// <returns>
        /// Gets the parent page of the given page
        /// </returns>
        private Adx_webpage GetParentPage(Adx_webpage page)
        {
            return page.adx_parentpageid != null ? this.ServiceContext.Adx_webpageSet.FirstOrDefault(p => p.Id == page.adx_parentpageid.Id) : null;
        }
    }
}