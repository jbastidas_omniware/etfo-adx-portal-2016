﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Runtime.Caching;
using System.Web;
using System.Web.UI.WebControls;
using Adxstudio.Xrm;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Caching;
using Microsoft.Xrm.Client.Messages;
using Microsoft.Xrm.Portal.Cms;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Xrm;
using Microsoft.Crm.Sdk.Messages;

namespace Site.Helpers
{
	public class XrmHelper
	{
		private static readonly string _organizationUri = "/XRMServices/2011/Organization.svc";

		private static XrmServiceContext _xrmContext;

		public static XrmServiceContext GetContext()
		{
			return PortalCrmConfigurationManager.CreateServiceContext() as XrmServiceContext;
		}

        #region Crm Helpers

        public static bool  SetStatus(string entityName, Guid entityid, int _status ,int _state = 0){

            var context = GetContext();

            OptionSetValue state = new OptionSetValue();
            OptionSetValue status = new OptionSetValue();
            state.Value = _state;
            status.Value = _status;

            EntityReference moniker = new EntityReference(entityName, entityid);
            OrganizationRequest request = new OrganizationRequest() { RequestName = "SetState" };
            request["EntityMoniker"] = moniker;
            request["State"] = state;
            request["Status"] = status;
            try
            {
                context.Execute(request);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool SendEmail( Email email)
        {
           
             var context = GetContext();

            try
            {
                GetTrackingTokenEmailRequest trackingTokenEmailRequest = new GetTrackingTokenEmailRequest();
                GetTrackingTokenEmailResponse trackingTokenEmailResponse = null;
                trackingTokenEmailResponse = (GetTrackingTokenEmailResponse)context.Execute(trackingTokenEmailRequest);

                SendEmailRequest request = new SendEmailRequest();
                request.EmailId = email.ActivityId.Value;
                request.TrackingToken = trackingTokenEmailResponse.TrackingToken;
                request.IssueSend = true;

                SendEmailResponse response = (SendEmailResponse)context.Execute(request);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static Email GetEmail(SystemUser sender, Contact to, string Subject, string Description)
        {
            Email email = new Email();
            email.Description = Description;
            email.Subject = Subject;

            ActivityParty from = new ActivityParty();
            from.PartyId = new EntityReference(sender.LogicalName, sender.SystemUserId.Value);

            email.From = new ActivityParty[] { from };

            ActivityParty To = new ActivityParty();
            To.PartyId = new EntityReference(to.LogicalName, to.Id);

            email.To = new ActivityParty[] { To };

            return email;
        }


        #endregion Crm Helpers

        #region Portal Helpers

        public static string GetSnippetValueOrDefault(string snippetName, string defaultString = "")
		{
            HttpContext.Current.Trace.Write(string.Format("{0}:{1}", MethodInfo.GetCurrentMethod().Name, snippetName), "Start");
			var context = XrmHelper.GetContext();
			var website = context.Adx_websiteSet.First(ws => ws.Adx_websiteId == PortalContext.Current.Website.Id);
			var snippet = (Adx_contentsnippet)context.GetSnippetByName(website, snippetName);
			
			var value = snippet == null ? defaultString : snippet.Adx_Value;
			HttpContext.Current.Trace.Write(string.Format("{0}:{1}", MethodInfo.GetCurrentMethod().Name, snippetName), "End");
			return value;

		}

		public static string GetSiteSettingValueOrDefault(string siteSettingName, string defaultString = "")
		{
			var context = XrmHelper.GetContext();
			var website = context.Adx_websiteSet.First(ws => ws.Adx_websiteId == PortalContext.Current.Website.Id);
			var siteSetting = (Adx_sitesetting)context.GetSiteSettingByName(website, siteSettingName);

			return siteSetting == null ? defaultString : siteSetting.Adx_Value;
		}

		public static Adx_sitemarker GetSiteMarker(string sitemarkerName, XrmServiceContext context = null)
		{
			if (context == null)
			{
				context = XrmHelper.GetContext();
			}
			var website = context.Adx_websiteSet.First(ws => ws.Adx_websiteId == PortalContext.Current.Website.Id);
			var sitemarker = (Adx_sitemarker) context.GetSiteMarkerByName(website, sitemarkerName);

			return sitemarker;
		}

		public static string GetSiteMarkerUrl(string sitemarkerName)
		{
			var context = XrmHelper.GetContext();
			var sitemarker = GetSiteMarker(sitemarkerName, context);

			if ((sitemarker == null) || (sitemarker.adx_webpage_sitemarker == null))
			{
				return string.Empty;
			}

			return context.GetUrl(sitemarker.adx_webpage_sitemarker);
		}

		public static List<ListItem> CreateOptionSetListItems(Entity entity, string attributeLogicalName, string cacheKey = null)
		{
			var context = XrmHelper.GetContext();
			var picklistMetadata = context.RetrieveAttribute(entity.LogicalName, attributeLogicalName) as PicklistAttributeMetadata;

			if (picklistMetadata == null)
			{
				HttpContext.Current.Trace.Warn("GetOptionSetLabelByValue", string.Format("Attribute '{0}' is not a picklist attribute.", attributeLogicalName));
				return null;
			}
			
			var cache = ObjectCacheManager.GetInstance();

			if (!string.IsNullOrEmpty(cacheKey))
			{
				if (cache.Get(cacheKey) != null)
				{
					return cache.Get(cacheKey) as List<ListItem>;
				}
			}
			
			var list = new List<ListItem>();

			foreach (OptionMetadata metadata in picklistMetadata.OptionSet.Options)
			{
				var listItem = new ListItem();
				listItem.Text = metadata.Label.UserLocalizedLabel.Label;
				listItem.Value = metadata.Value.ToString();
				list.Add(listItem);
			}

            cache.Insert(cacheKey, list, new CacheItemPolicy()); 


			return list;
		}

		public static string GetOptionSetLabelByValue(Entity entity, string attributeLogicalName, int? value)
		{
			HttpContext.Current.Trace.Write(MethodInfo.GetCurrentMethod().Name, "Start");
			var context = XrmHelper.GetContext();
			//var entityMetadata = context.RetrieveEntity(entity.LogicalName, EntityFilters.Attributes);

			//if(entityMetadata == null)
			//{
			//    HttpContext.Current.Trace.Warn("GetOptionSetLabelByValue", "Unable to retrieve entity metadata");
			//    return null;
			//}

			//var picklistMetadata = entityMetadata.Attributes.FirstOrDefault(attr => attr.LogicalName == attributeLogicalName) as PicklistAttributeMetadata;

			//var picklistMetadata = context.RetrieveAttribute(entity.LogicalName, attributeLogicalName) as PicklistAttributeMetadata;
			var request = new RetrieveAttributeRequest();
			request.EntityLogicalName = entity.LogicalName;
			request.LogicalName = attributeLogicalName;

			var response = context.Execute(request);

			var picklistMetadata = response.Results.First().Value as PicklistAttributeMetadata;

			if(picklistMetadata == null)
			{
				HttpContext.Current.Trace.Warn("GetOptionSetLabelByValue", string.Format("Attribute '{0}' is not a picklist attribute.", attributeLogicalName));
				return null;
			}

			var option = picklistMetadata.OptionSet.Options.FirstOrDefault(opt => opt.Value == value);

			if(option == null)
			{
				HttpContext.Current.Trace.Warn("GetOptionSetLabelByValue", string.Format("Unable to find option with value '{0}' in picklist attribute.", value));
				return null;
			}

			var label = option.Label.UserLocalizedLabel.Label;
			HttpContext.Current.Trace.Write(MethodInfo.GetCurrentMethod().Name, "End");
			return label;
		}

		public static int? GetOptionSetValueByLabel(Entity entity, string attributeLogicalName, string label)
		{
			HttpContext.Current.Trace.Write(MethodInfo.GetCurrentMethod().Name, "Start");
			var context = XrmHelper.GetContext();
			//var entityMetadata = context.RetrieveEntity(entity.LogicalName, EntityFilters.Attributes);

			//if (entityMetadata == null)
			//{
			//    HttpContext.Current.Trace.Warn("GetOptionSetValueByLabel", "Unable to retrieve entity metadata");
			//    return null;
			//}

			//var picklistMetadata = entityMetadata.Attributes.FirstOrDefault(attr => attr.LogicalName == attributeLogicalName) as PicklistAttributeMetadata;

			//var picklistMetadata = context.RetrieveAttribute(entity.LogicalName, attributeLogicalName) as PicklistAttributeMetadata;
			var request = new RetrieveAttributeRequest();
			request.EntityLogicalName = entity.LogicalName;
			request.LogicalName = attributeLogicalName;

			var response = context.Execute(request);

			var picklistMetadata = response.Results.First().Value as PicklistAttributeMetadata;


			if (picklistMetadata == null)
			{
				HttpContext.Current.Trace.Warn("GetOptionSetValueByLabel", string.Format("Attribute '{0}' is not a picklist attribute.", attributeLogicalName));
				return null;
			}

			var option = picklistMetadata.OptionSet.Options.FirstOrDefault(opt => opt.Label.UserLocalizedLabel.Label.ToLower() == label.ToLower());

			if (option == null)
			{
				HttpContext.Current.Trace.Warn("GetOptionSetValueByLabel", string.Format("Unable to find option with label '{0}' in picklist attribute.", label));
				return null;
			}
			HttpContext.Current.Trace.Write(MethodInfo.GetCurrentMethod().Name, "End");
			return option.Value;
		}

		public static OrganizationServiceProxy GetOrganizationServiceProxy(CrmConnection connection)
		{
			HttpContext.Current.Trace.Write(MethodInfo.GetCurrentMethod().Name, "Start");
			if(connection == null)
			{
				throw new ArgumentException("ToOrganizationServiceProxy: The connection was null");
			}

			if (connection.ServiceUri == null) throw new ConfigurationErrorsException("The connection's 'ServiceUri' must be specified.");

			var clientCredentials = connection.ClientCredentials;

			OrganizationServiceProxy service;

			// retrieve the ServiceConfiguration from cache

			var config = CreateServiceConfiguration(connection);

			service = new OrganizationServiceProxy(config, clientCredentials);

			service.CallerId = connection.CallerId ?? Guid.Empty;

			if (connection.Timeout != null) service.Timeout = connection.Timeout.Value;
			HttpContext.Current.Trace.Write(MethodInfo.GetCurrentMethod().Name, "End");
			return service;
		}
		
		private static IServiceConfiguration<IOrganizationService> CreateServiceConfiguration(CrmConnection connection)
		{
			var uri = connection.ServiceUri;
			var fullServiceUri = uri.AbsolutePath.EndsWith(_organizationUri, StringComparison.OrdinalIgnoreCase)
				? uri
				: new Uri(uri, uri.AbsolutePath.TrimEnd('/') + _organizationUri);

			var proxyTypesEnabled = connection.ProxyTypesEnabled;
			var proxyTypesAssembly = connection.ProxyTypesAssembly;

			var config = ServiceConfigurationFactory.CreateConfiguration<IOrganizationService>(fullServiceUri);

			// the ideal is to call OrganizationServiceConfiguration.EnableProxyTypes() but the class is internal
			// also the IServiceConfiguration does not have an EnableProxyTypes method
			// so manually attach the proxy assembly

			if (proxyTypesEnabled) config.CurrentServiceEndpoint.Behaviors.Add(new ProxyTypesBehavior(proxyTypesAssembly));

			return config;
		}

		public static string GetCurrentPageUrl()
		{
			var context = GetContext();
			var page = context.Adx_webpageSet.FirstOrDefault(wp => wp.Adx_webpageId.Value == PortalContext.Current.Entity.Id);

			return context.GetUrl(page);
		}

		public static void InvalidateCacheForCurrentPage()
		{
			var context = GetContext();

			context.TryRemoveFromCache(PortalContext.Current.Entity.LogicalName, PortalContext.Current.Entity.Id);
		}

		public static void InvalidateCacheForEntity(Entity entity)
		{
			var context = GetContext();

			context.TryRemoveFromCache(entity.LogicalName, entity.Id);
		}

		public static string GetPrimaryKeyLogicalName(Entity entity, string cacheKey)
		{
			var context = GetContext();

			var cache = ObjectCacheManager.GetInstance();
			var metadata = cache.Get(cacheKey) as EntityMetadata;

			if (metadata == null)
			{
				metadata = context.RetrieveEntity(entity.LogicalName);
			}

			return metadata.PrimaryIdAttribute;
        }

        #endregion Portal Helpers
    }
}