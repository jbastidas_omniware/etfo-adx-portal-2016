﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Xrm;
using System.Web.Script.Serialization;
using Site.Helpers;
using System.Web.Security;

using Adxstudio.Xrm.Partner;
using Adxstudio.Xrm.Web.Security;
using System.Configuration;
using System.Collections.Specialized;
using System.Text;


namespace Site.Helpers
{
    public static class ProfileHelper
    {
        public static bool UpdateProfileContact(Guid contactid, string FirstName, string LastName, string EMailAddress1)
        {
            var context = XrmHelper.GetContext();
            var contact = new Contact()
            {
                Id = contactid,
                Adx_LogonEnabled = true
            };

            contact.FirstName = FirstName;
            contact.LastName = LastName;
            try
            {
                context.Attach(contact);
                context.UpdateObject(contact);
                context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool UpdateProfileContact(Guid contactid, string FirstName, string LastName, string EMailAddress1, string Telephone1_business, 
            string Telephone2_home, string MobilePhone, string Fax, string Address1_Line1, string Address1_Line2, string Address1_City,
            string Address1_StateOrProvince, string Address1_PostalCode, string Address1_Country, string oems_NonMemberTypeId, string sequrityQuestion, 
            string sequrityAnswer, int oems_NonMemberTypeCode, string mbr_collegeid, string oems_SchoolBoardId, string oems_SchoolId)
        {

            var context = XrmHelper.GetContext();
            
            var contact = new Contact()
            {
                Id = contactid,
                Adx_LogonEnabled = true
            };
                
            contact.FirstName = FirstName;
            contact.LastName = LastName;
            contact.Address1_Line1 = Address1_Line1;
            contact.Address1_Line2 = Address1_Line2;
            contact.Address1_City = Address1_City;
            contact.Address1_StateOrProvince = Address1_StateOrProvince;
            contact.Address1_PostalCode = Address1_PostalCode;
            contact.Address1_Country = Address1_Country;
            contact.Telephone1 = Telephone1_business;
            contact.Telephone2 = Telephone2_home;
            contact.MobilePhone = MobilePhone;
            contact.Fax = Fax;
            contact.EMailAddress1 = EMailAddress1;
            //contact.BirthDate = oems_Birthday;
            contact.oems_NonMemberType = oems_NonMemberTypeCode;

            if (!string.IsNullOrEmpty(mbr_collegeid) && !string.IsNullOrWhiteSpace(mbr_collegeid))
                contact.mbr_collegeid = mbr_collegeid;

            try
            {
                context.Attach(contact);
                context.UpdateObject(contact);
                context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool CreateProfileSecurity(string EMailAddress1, string password, string sequrityQuestion, string sequrityAnswer) 
        {
            CrmContactMembershipProvider user = new CrmContactMembershipProvider();
            NameValueCollection settings = new NameValueCollection();
            settings.Add("enablePasswordReset", "true");
            settings.Add("enablePasswordRetrieval", "true");
            settings.Add("requiresQuestionAndAnswer", "true");
            settings.Add("passwordFormat", "Encrypted");
            settings.Add("requiresUniqueEmail", "false");
            settings.Add("maxInvalidPasswordAttempts", "5");
            settings.Add("minRequiredPasswordLength", "6");
            settings.Add("minRequiredNonAlphanumericCharacters", "0");
            settings.Add("passwordAttemptWindow", "10");
            user.Initialize("Xrm", settings);
            
            MembershipCreateStatus status;
            MembershipUser member = user.CreateUser(EMailAddress1,password,EMailAddress1,sequrityQuestion, sequrityAnswer, true, null, out status);
            
            return  !ReferenceEquals(member,null);
        }

        public static bool UpdateProfileSecurityChalengeAnswer(Guid contactid, string securityAnswer, string EMailAddress1)
        {
            CrmContactMembershipProvider user = new CrmContactMembershipProvider();
            NameValueCollection settings = new NameValueCollection();
            settings.Add("enablePasswordReset", "true");
            settings.Add("enablePasswordRetrieval", "true");
            settings.Add("requiresQuestionAndAnswer", "true");
            settings.Add("passwordFormat", "Encrypted");
            settings.Add("requiresUniqueEmail", "false");
            settings.Add("maxInvalidPasswordAttempts", "5");
            settings.Add("minRequiredPasswordLength", "6");
            settings.Add("minRequiredNonAlphanumericCharacters", "0");
            settings.Add("passwordAttemptWindow", "10");
            user.Initialize("Xrm", settings);

            var password = user.ResetPassword(EMailAddress1, securityAnswer);

            if (!string.IsNullOrWhiteSpace(password))
            {
                return true;
            }
            return false;
        }

        public static string getSchoolHtml(Guid schoolboardid)
        {
            var context = XrmHelper.GetContext();

            var Schools = (from s in context.CreateQuery<mbr_school>()
                           where s.statecode == 0 && s.mbr_SchoolBoard.Id == schoolboardid
                           select new { Name = s.mbr_name, GID = s.Id.ToString() });

            StringBuilder html = new StringBuilder(" <option value=''></option>");

            foreach (var item in Schools)
            {
                html.Append("<option value=");
                html.Append(item.GID);
                html.Append(">");
                html.Append(item.Name);
                html.Append("</option>");
            }

            return html.ToString();
        }

    }
}