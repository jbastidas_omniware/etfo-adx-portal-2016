﻿namespace Site.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.Xrm.Sdk.Messages;
    using Microsoft.Xrm.Sdk.Metadata;

    /// <summary>
    /// The entity option set.
    /// </summary>
    public class EntityOptionSet : PortalClass
    {
        #region Fields

        /// <summary>
        /// The _option set description cache.
        /// </summary>
        private readonly IDictionary<int, string> optionSetDescriptionCache = new Dictionary<int, string>();

        /// <summary>
        /// The _option set label cache.
        /// </summary>
        private readonly IDictionary<int, string> optionSetLabelCache = new Dictionary<int, string>();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get option set label.
        /// </summary>
        /// <param name="entityname">
        /// The entity name.
        /// </param>
        /// <param name="attributename">
        /// The attribute name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// Returns the option set label.
        /// </returns>
        public static string GetOptionSetLabel(string entityname, string attributename, int? value)
        {
            if (value.HasValue)
            {
                return new EntityOptionSet().GetOptionSetLabelByValue(entityname, attributename, value.Value);
            }

            return string.Empty;
        }

        /// <summary>
        /// The get option set value.
        /// </summary>
        /// <param name="entityname">
        /// The entity name.
        /// </param>
        /// <param name="attributename">
        /// The attribute name.
        /// </param>
        /// <param name="label">
        /// The label.
        /// </param>
        /// <returns>
        /// The value
        /// </returns>
        public static int? GetOptionSetValue(string entityname, string attributename, string label)
        {
            if (!string.IsNullOrWhiteSpace(label))
            {
                return new EntityOptionSet().GetOptionSetValueByLabel(entityname, attributename, label);
            }

            return null;
        }

        /// <summary>
        /// The get option set description by value.
        /// </summary>
        /// <param name="entityLogicalName">
        /// The entity logical name.
        /// </param>
        /// <param name="attributeLogicalName">
        /// The attribute logical name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetOptionSetDescriptionByValue(string entityLogicalName, string attributeLogicalName, int value)
        {
            if (string.IsNullOrEmpty(entityLogicalName) || string.IsNullOrEmpty(attributeLogicalName))
            {
                return string.Empty;
            }

            string cachedDescription;

            if (this.optionSetDescriptionCache.TryGetValue(value, out cachedDescription))
            {
                return cachedDescription;
            }

            var retrieveAttributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entityLogicalName,
                LogicalName = attributeLogicalName
            };

            var retrieveAttributeResponse =
                (RetrieveAttributeResponse)this.ServiceContext.Execute(retrieveAttributeRequest);

            var retrievedPicklistAttributeMetadata = (EnumAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;

            OptionMetadata option =
                retrievedPicklistAttributeMetadata.OptionSet.Options.FirstOrDefault(o => o.Value == value);

            if (option == null)
            {
                return string.Empty;
            }

            string label = option.Description.UserLocalizedLabel.Label;

            if (option.Value.HasValue)
            {
                this.optionSetDescriptionCache[option.Value.Value] = label;
            }

            return label;
        }

        /// <summary>
        /// The get option set label by value.
        /// </summary>
        /// <param name="entityLogicalName">
        /// The entity logical Name.
        /// </param>
        /// <param name="attributeLogicalName">
        /// The attribute logical Name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// Returns the get option set label by value.
        /// </returns>
        public string GetOptionSetLabelByValue(string entityLogicalName, string attributeLogicalName, int value)
        {
            if (string.IsNullOrEmpty(entityLogicalName) || string.IsNullOrEmpty(attributeLogicalName))
            {
                return string.Empty;
            }

            string cachedLabel;

            if (this.optionSetLabelCache.TryGetValue(value, out cachedLabel))
            {
                return cachedLabel;
            }

            var retrieveAttributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entityLogicalName,
                LogicalName = attributeLogicalName
            };

            var retrieveAttributeResponse =
                (RetrieveAttributeResponse)this.ServiceContext.Execute(retrieveAttributeRequest);

            var retrievedPicklistAttributeMetadata = (EnumAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;

            OptionMetadata option =
                retrievedPicklistAttributeMetadata.OptionSet.Options.FirstOrDefault(o => o.Value == value);

            if (option == null)
            {
                return string.Empty;
            }

            string label = option.Label.UserLocalizedLabel.Label;

            if (option.Value.HasValue)
            {
                this.optionSetLabelCache[option.Value.Value] = label;
            }

            return label;
        }

       

        /// <summary>
        /// The get option set value by description.
        /// </summary>
        /// <param name="entityLogicalName">
        /// The entity logical name.
        /// </param>
        /// <param name="attributeLogicalName">
        /// The attribute logical name.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <returns>
        /// The description
        /// </returns>
        public int? GetOptionSetValueByDescription(
            string entityLogicalName, string attributeLogicalName, string description)
        {
            // Check to see all the parameters are available
            if (string.IsNullOrEmpty(entityLogicalName) || string.IsNullOrEmpty(attributeLogicalName)
                || string.IsNullOrEmpty(description))
            {
                return null;
            }

            // Initialize return value as -1; Such that if no value is found, it will return an impossible value
            int? optionSetValue = null;

            var retrieveAttributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entityLogicalName,
                LogicalName = attributeLogicalName
            };

            var retrieveAttributeResponse =
                (RetrieveAttributeResponse)this.ServiceContext.Execute(retrieveAttributeRequest);

            var retrievedPicklistAttributeMetadata = (EnumAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;

            // Obtain the corresponding Option Set and iterate through the set to look for matching value
            OptionMetadata[] optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();

            foreach (OptionMetadata optionMetaData in optionList)
            {
                // To avoid an error where the Label is null for whatever reason
                if (optionMetaData.Description != null && optionMetaData.Description.UserLocalizedLabel != null)
                {
                    if (string.Compare(
                        optionMetaData.Description.UserLocalizedLabel.Label,
                        description,
                        StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        optionSetValue = optionMetaData.Value;
                        break;
                    }
                }
            }

            // Return the value
            return optionSetValue;
        }

        /// <summary>
        /// The get option set value by label.
        /// </summary>
        /// <param name="entityLogicalName">
        /// The entity logical Name.
        /// </param>
        /// <param name="attributeLogicalName">
        /// The attribute logical Name.
        /// </param>
        /// <param name="labelText">
        /// The label text.
        /// </param>
        /// <returns>
        /// Returns the OptionSet value
        /// </returns>
        public int? GetOptionSetValueByLabel(string entityLogicalName, string attributeLogicalName, string labelText)
        {
            // Check to see all the parameters are available
            if (string.IsNullOrEmpty(entityLogicalName) || string.IsNullOrEmpty(attributeLogicalName)
                || string.IsNullOrEmpty(labelText))
            {
                return null;
            }

            // Initialize return value as -1; Such that if no value is found, it will return an impossible value
            int? optionSetValue = null;

            var retrieveAttributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entityLogicalName,
                LogicalName = attributeLogicalName
            };

            var retrieveAttributeResponse =
                (RetrieveAttributeResponse)this.ServiceContext.Execute(retrieveAttributeRequest);

            var retrievedPicklistAttributeMetadata = (EnumAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;

            // Obtain the corresponding Option Set and iterate through the set to look for matching value
            OptionMetadata[] optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();

            foreach (OptionMetadata optionMetaData in optionList)
            {
                // To avoid an error where the Label is null for whatever reason
                if (optionMetaData.Label != null && optionMetaData.Label.UserLocalizedLabel != null)
                {
                    if (string.Compare(
                        optionMetaData.Label.UserLocalizedLabel.Label, labelText, StringComparison.OrdinalIgnoreCase)
                        == 0)
                    {
                        optionSetValue = optionMetaData.Value;
                        break;
                    }
                }
            }

            // Return the value
            return optionSetValue;
        }

        #endregion
    }
}