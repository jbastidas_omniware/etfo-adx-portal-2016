﻿using Microsoft.Xrm.Sdk;
using System;
using System.Linq;
using Microsoft.Xrm.Portal;
using Newtonsoft.Json.Linq;
using Site.Library;
using Xrm;
using Site.Areas.Payment.Library;


namespace Site.Helpers
{
    public static class RegistrationHelper
    {
        public static class RegistrationType
        {
            public const int Member = 1;
            public const int NonMember = 2;
            public const int Staff = 3;
            public const int InvitedGuest = 4;
            public const int AllOther = 5;
        }

        public static class ApplicationType
        {
            public const int Member = 1;
            public const int NonMember = 2;
            public const int AllOther = 1000000;
        }

        public static class NonMemberType
        {
            public const int Teacher = 1;
            public const int NonEducator = 2;
            public const int Staff = 3;
        }

        public static class PrivilegeType
        {
            public const int RegisrationExtended = 1;
            public const int DelegationExtended = 2;
            public const int ApplicationExtended = 100000000;

        }

        public static class State
        {
            public const int Active = 0;
            public const int Inactive = 1;
        }

        public static class EventStatus
        {
            public const int Creating = 1;
            public const int Cancelled = 347780010;//2
            public const int PublishedApplication = 347780001;
            public const int OpenApplication = 347780002;
            public const int CloseApplication = 347780003;
            public const int PublishedRegistration = 347780004;
            public const int OpenRegistration = 347780005;
            public const int CloseRegistration = 347780006;
            public const int StartEvent = 347780007;
            public const int EndEvent = 347780008;
            public const int Completed = 347780009;
            public const int ExceededCapacity = 347780011;
        }

        public static class EventRegistration
        {
            public const int Initial = 347780004;
            public const int Created = 1;
            public const int Cancelled = 347780007;//2;
            public const int Withdrawn = 347780008;//347780000;
            public const int Sumbitted = 347780001;
            public const int Denied = 347780006;//347780002;
            public const int Approved = 347780003;
        }

        public static class EventRegistrationType
        {
            public const int Attendee = 347780000;
            public const int Presenter = 347780001;
        }

        public static class LocalDelegation
        {
            public const int Sumbitted = 347780002;
            public const int Creating = 1;
            public const int Approved = 347780000;
            public const int Revisit = 347780001;
        }

        public static class WaitingListStatus
        {
            public static int Accepted = 1;
            public static int Declined = 347780000;
            public static int Expired = 347780001;
            public static int Offered = 347780002;
            public static int Removed = 347780003;
            public static int Waiting = 347780004;
            public static int Cancelled = 347780005;
        }

        public static class AccountRole
        {
            public const int LocalPresident = 347780000;
            public const int LocalPresidentDesignate = 347780001;
        }

        public static class CourseTermCode
        {
            public const int Annual = 347780000;
            public const int Fall = 347780001;
            public const int Winter = 347780002;
            public const int Spring = 347780003;
            public const int Summer = 347780004;
        }

        public static class AttachmentTypes
        {
            public const string CourseOutiline = "course-outline";
            public const string Resume = "resume";
            public const string LetterReference1 = "letter-reference1";
            public const string LetterReference2 = "letter-reference2";
            public const string IndividualCourseParticipationLetter = "individual-course-participation-letter";
            public const string InstructorCourseAgreementForm = "instructor-course-agreement-form";
            public const string InstructorPayrollEnrollmentForm = "instructor-payroll-enrollment-form";
        }

        public class MyEventConfiguration
        {
            public bool showManageRegistrationButton { get; set; }
            public bool showManageApplicationButton { get; set; }
            public bool showCancelRegistrationButton { get; set; }
            public bool showCancelApplicationButton { get; set; }
            public bool showWithdrawRegistrationButton { get; set; }
            public bool showWithdrawApplicationButton { get; set; }
            public bool showManageDelegationButton { get; set; }
            public bool showMyItineraryButton { get; set; }
            public bool showMyCourseCertificateButton { get; set; }
        }

        public static MyEventConfiguration MyEventHelper(Guid oems_EventRegistrationId, IPortalContext Portal)
        {
            var context = XrmHelper.GetContext();

            MyEventConfiguration myEventConfig = new MyEventConfiguration();

            // get event registration
            oems_EventRegistration oemsEventRegistration = context.oems_EventRegistrationSet.Where(a => a.oems_EventRegistrationId == oems_EventRegistrationId).Single();
            oems_Event oemsEvent = context.oems_EventSet.Where(a => a.oems_EventId == oemsEventRegistration.oems_Event.Id).Single();
            Contact contact = context.ContactSet.Where(a => a.ContactId == oemsEventRegistration.oems_Contact.Id).Single();

            var data = new
            {
                eventRegistrationType = oemsEventRegistration.Contains("oems_registrationtype") ? ((OptionSetValue)oemsEventRegistration["oems_registrationtype"]).Value : EventRegistrationType.Attendee,
                eventRegistrationState = oemsEventRegistration.statecode,
                eventRegistrationStatus = oemsEventRegistration.statuscode,
                eventState = oemsEvent.statecode,
                eventStatus = oemsEvent.statuscode,
                contactId = (Guid)contact.ContactId,
                eventId = (Guid)oemsEvent.oems_EventId,
                localDelegationOpenDate = oemsEvent.oems_LocalDelegationOpenDateTime,
                localDelegationCloseDate = oemsEvent.oems_LocalDelegationCloseDateTime,
                //presidentFlag = contact.AccountRoleCode.HasValue && (contact.AccountRoleCode.Value == AccountRole.LocalPresident || contact.AccountRoleCode.Value == AccountRole.LocalPresidentDesignate) ? true : false
            };

            //Manage
            if ((data.eventRegistrationStatus == EventRegistration.Created || data.eventRegistrationStatus == EventRegistration.Sumbitted)//|| data.eventRegistrationStatus == EventRegistration.Rejected)
                && RegistrationOpen(data.contactId, data.eventId, Portal)
                && data.eventRegistrationType == EventRegistrationType.Attendee)
                myEventConfig.showManageRegistrationButton = true;
            else
                myEventConfig.showManageRegistrationButton = false;

            if ((data.eventRegistrationStatus == EventRegistration.Created || data.eventRegistrationStatus == EventRegistration.Sumbitted)//|| data.eventRegistrationStatus == EventRegistration.Rejected)
                && ApplicationOpen(data.contactId, data.eventId, Portal)
                && data.eventRegistrationType == EventRegistrationType.Presenter)
                myEventConfig.showManageApplicationButton = true;
            else
                myEventConfig.showManageApplicationButton = false;

            //Cancel
            if ((data.eventRegistrationStatus == EventRegistration.Created || data.eventRegistrationStatus == EventRegistration.Sumbitted)
                && (data.eventStatus == EventStatus.OpenRegistration || data.eventStatus == EventStatus.CloseRegistration)
                && data.eventRegistrationType == EventRegistrationType.Attendee)
                myEventConfig.showCancelRegistrationButton = true;
            else
                myEventConfig.showCancelRegistrationButton = false;

            if ((data.eventRegistrationStatus == EventRegistration.Created || data.eventRegistrationStatus == EventRegistration.Sumbitted)
                && (data.eventStatus == EventStatus.OpenApplication || data.eventStatus == EventStatus.CloseApplication)
                && data.eventRegistrationType == EventRegistrationType.Presenter)
                myEventConfig.showCancelApplicationButton = true;
            else
                myEventConfig.showCancelApplicationButton = false;

            //Withdraw
            if ((data.eventRegistrationStatus == EventRegistration.Approved)
                && (data.eventStatus == EventStatus.OpenRegistration || data.eventStatus == EventStatus.CloseRegistration)
                && data.eventRegistrationType == EventRegistrationType.Attendee)
                myEventConfig.showWithdrawRegistrationButton = true;
            else
                myEventConfig.showWithdrawRegistrationButton = false;

            if ((data.eventRegistrationStatus == EventRegistration.Approved)
                && (data.eventStatus == EventStatus.OpenApplication || data.eventStatus == EventStatus.CloseApplication)
                && data.eventRegistrationType == EventRegistrationType.Presenter)
                myEventConfig.showWithdrawApplicationButton = true;
            else
                myEventConfig.showWithdrawApplicationButton = false;

            //Itinerary
            if ((data.eventRegistrationStatus == EventRegistration.Approved)
                && (data.eventStatus == EventStatus.CloseRegistration || data.eventStatus == EventStatus.StartEvent))
                myEventConfig.showMyItineraryButton = true;
            else
                myEventConfig.showMyItineraryButton = false;

            //Local delegation
            if (oemsEventRegistration.oems_Local != null)
            {
                var now = DateTime.Now.Date;
                var orgContact =
                (
                    from a in context.mbr_organizationcontactSet
                    where a.mbr_ContactId != null && a.mbr_ContactId.Id == contact.Id
                    && a.mbr_LocalId != null && a.mbr_LocalId.Id == oemsEventRegistration.oems_Local.Id
                    && a.mbr_EffectiveStartDate != null && a.mbr_EffectiveStartDate.Value <= now
                    && a.mbr_EffectiveEndDate != null && a.mbr_EffectiveEndDate.Value >= now
                    && a.mbr_Role != null && a.statecode == 0
                    select a
                ).FirstOrDefault();

                if (orgContact != null && orgContact.mbr_Role.Name.ToLower().Trim() == "president")// && local.mbr_CurrentPresident.Id == contact.Id)
                {
                    if (isUserOverrideSet(data.contactId, data.eventId, PrivilegeType.DelegationExtended, Portal))
                    {
                        myEventConfig.showManageDelegationButton = true;
                    }
                    else
                    {
                        // check schedule
                        if (data.localDelegationOpenDate != null && data.localDelegationCloseDate != null
                            && DateHelper.timeComparer((DateTime)data.localDelegationOpenDate, DateTime.UtcNow, Portal) == -1
                            && DateHelper.timeComparer((DateTime)data.localDelegationCloseDate, DateTime.UtcNow, Portal) == 1)
                            myEventConfig.showManageDelegationButton = true;
                        else
                            myEventConfig.showManageDelegationButton = false;
                    }
                }
                else
                {
                    myEventConfig.showManageDelegationButton = false;
                }
            }
            else
            {
                myEventConfig.showManageDelegationButton = false;
            }

            //My Course Certificate
            if (data.eventRegistrationStatus == EventRegistration.Approved && data.eventStatus == EventStatus.Completed)
                myEventConfig.showMyCourseCertificateButton = true;
            else
                myEventConfig.showMyCourseCertificateButton = false;


            return myEventConfig;
        }

        public static bool IsUserRegisteredToEvent(Guid? contactId, Guid eventId, out Guid registrationId)
        {
            registrationId = Guid.Empty;
            if (contactId == null)
            {
                return false;
            }

            var context = XrmHelper.GetContext();
            var er = context.oems_EventRegistrationSet.Where(
                r =>
                    r.oems_Event.Id == eventId &&
                    r.oems_Contact.Id == contactId.Value &&
                    r.statuscode != EventRegistration.Initial &&
                    r.statuscode != EventRegistration.Cancelled &&
                    (r["oems_registrationtype"] == null || ((OptionSetValue)r["oems_registrationtype"]).Value == RegistrationHelper.EventRegistrationType.Attendee)
                ).ToList();

            var total = er.Count();

            if (total > 0)
            {
                registrationId = er.First().oems_EventRegistrationId ?? Guid.Empty;
            }

            return total > 0;
        }

        public static bool IsUserAppliedToEvent(Guid? contactId, Guid eventId, out Guid registrationId)
        {
            registrationId = Guid.Empty;
            if (contactId == null)
            {
                return false;
            }

            var context = XrmHelper.GetContext();
            var er = context.oems_EventRegistrationSet.Where(
                r =>
                    r.oems_Event.Id == eventId &&
                    r.oems_Contact.Id == contactId.Value &&
                    r.statuscode != EventRegistration.Initial &&
                    r.statuscode != EventRegistration.Cancelled &&
                    r["oems_registrationtype"] != null &&
                    ((OptionSetValue)r["oems_registrationtype"]).Value == RegistrationHelper.EventRegistrationType.Presenter
                ).ToList();

            var total = er.Count();

            if (total > 0)
            {
                registrationId = er.First().oems_EventRegistrationId ?? Guid.Empty;
            }

            return total > 0;
        }

        public static int? GetRegistrationStatus(Guid registrationId)
        {
            var context = XrmHelper.GetContext();
            var er = context.oems_EventRegistrationSet.Where(r => r.Id == registrationId).FirstOrDefault();

            return er != null ? er.statuscode : null;
        }

        public static bool CanViewRegistration(int? eventStatus, int? registrationStatus)
        {
            if (eventStatus == null || registrationStatus == null)
            {
                return false;
            }
            var yesNo = false;
            switch (eventStatus.Value)
            {
                case EventStatus.OpenRegistration:
                case EventStatus.OpenApplication:
                    switch (registrationStatus)
                    {
                        case EventRegistration.Cancelled:
                        case EventRegistration.Withdrawn:
                        case EventRegistration.Approved:
                        case EventRegistration.Denied:
                            yesNo = true;
                            break;
                    }
                    break;
                case EventStatus.CloseRegistration:
                case EventStatus.CloseApplication:
                    switch (registrationStatus)
                    {
                        case EventRegistration.Created:
                        case EventRegistration.Sumbitted:
                        case EventRegistration.Cancelled:
                        case EventRegistration.Withdrawn:
                        case EventRegistration.Approved:
                        case EventRegistration.Denied:
                            yesNo = true;
                            break;
                    }
                    break;
                case EventStatus.StartEvent:
                case EventStatus.EndEvent:
                case EventStatus.Completed:
                    yesNo = true;
                    break;
            }

            return yesNo;
        }

        public static bool IsPaymentPendingOrPayed(Guid registrationId)
        {
            var paymentInfo = BillingService.GetPaymentInformation(registrationId);

            return paymentInfo != null &&
                (paymentInfo.PaymentStatus == (int)BillingService.PaymentStatus.Paid
                || paymentInfo.PaymentStatus == (int)BillingService.PaymentStatus.ChequePending
                || paymentInfo.PaymentStatus == (int)BillingService.PaymentStatus.Partial
                || paymentInfo.PaymentStatus == (int)BillingService.PaymentStatus.PayLater
                || paymentInfo.PaymentStatus == (int)BillingService.PaymentStatus.RefundFailed);
        }

        public static bool RegistrationOpen(Guid? contactId, Guid oems_EventId, IPortalContext Portal, bool ignoreEventStatus = false)
        {
            var context = XrmHelper.GetContext();
            oems_Event OEvent = (from e in context.CreateQuery<oems_Event>() where e.oems_EventId == oems_EventId select e).FirstOrDefault();

            if (contactId == null)
            {
                if (!OEvent.oems_RegistrationOpening.HasValue || !OEvent.oems_RegistrationDeadline.HasValue)
                    return false;

                if (!ignoreEventStatus)
                {
                    if (DateHelper.timeComparer(OEvent.oems_RegistrationOpening.Value, DateTime.UtcNow, Portal) == -1
                        && DateHelper.timeComparer(OEvent.oems_RegistrationDeadline.Value, DateTime.UtcNow, Portal) == 1
                        && ((OptionSetValue)OEvent.Attributes["statuscode"]).Value == EventStatus.OpenRegistration)
                        return true;
                    else
                        return false;
                }
                else
                {
                    if (DateHelper.timeComparer(OEvent.oems_RegistrationOpening.Value, DateTime.UtcNow, Portal) == -1
                        && DateHelper.timeComparer(OEvent.oems_RegistrationDeadline.Value, DateTime.UtcNow, Portal) == 1)
                        return true;
                    else
                        return false;
                }
            }

            if (isUserOverrideSet(contactId.Value, (Guid)OEvent.oems_EventId, PrivilegeType.RegisrationExtended, Portal))
            {
                return true;
            }

            if (isUserInvited(contactId.Value, OEvent))
            {
                switch (CheckScheduleIsOkay(OEvent, RegistrationType.InvitedGuest, Portal, ignoreEventStatus))
                {
                    case 1:
                        return true;
                    case 2:
                        return false;
                }
            }

            var contact = (from a in context.CreateQuery<Contact>() where a.ContactId == contactId select a).FirstOrDefault();
            if (contact != null)
            {
                if (!contact.oems_isnon_member.HasValue || !contact.oems_isnon_member.Value)
                {
                    switch (CheckScheduleIsOkay(OEvent, RegistrationType.Member, Portal, ignoreEventStatus))
                    {
                        case 1:
                            return true;
                        case 2:
                            return false;
                    }
                }
                else if (contact.oems_NonMemberType.HasValue)
                {
                    if (contact.oems_NonMemberType.Value == NonMemberType.Teacher || contact.oems_NonMemberType.Value == NonMemberType.NonEducator)
                    {
                        switch (CheckScheduleIsOkay(OEvent, RegistrationType.NonMember, Portal, ignoreEventStatus))
                        {
                            case 1:
                                return true;
                            case 2:
                                return false;
                        }
                    }
                    else if (contact.oems_NonMemberType.Value == NonMemberType.Staff)
                    {
                        switch (CheckScheduleIsOkay(OEvent, RegistrationType.Staff, Portal, ignoreEventStatus))
                        {
                            case 1:
                                return true;
                            case 2:
                                return false;
                        }
                    }
                }
            }

            switch (CheckScheduleIsOkay(OEvent, RegistrationType.AllOther, Portal, ignoreEventStatus))
            {
                case 1:
                    return true;
                default:
                    return false;
            }
        }

        public static bool ApplicationOpen(Guid? contactId, Guid oems_EventId, IPortalContext Portal)
        {

            var context = XrmHelper.GetContext();
            oems_Event OEvent = (from e in context.CreateQuery<oems_Event>() where e.oems_EventId == oems_EventId select e).FirstOrDefault();

            if (contactId == null)
            {
                if (!OEvent.Contains("oems_applicationopening") || !OEvent.Contains("oems_applicationdeadline"))
                    return false;

                if (
                    DateHelper.timeComparer((DateTime)OEvent["oems_applicationopening"], DateTime.UtcNow, Portal) == -1
                    && DateHelper.timeComparer((DateTime)OEvent["oems_applicationdeadline"], DateTime.UtcNow, Portal) == 1
                    && ((OptionSetValue)OEvent.Attributes["statuscode"]).Value == EventStatus.OpenApplication)
                    return true;
                else
                    return false;
            }

            if (isUserOverrideSet(contactId.Value, (Guid)OEvent.oems_EventId, PrivilegeType.ApplicationExtended, Portal))
                return true;

            bool isApplicationOpen = false;
            var contact = (from a in context.CreateQuery<Contact>() where a.ContactId == contactId select a).FirstOrDefault();
            if (contact != null)
            {
                if (contact.oems_isnon_member.HasValue && contact.oems_isnon_member.Value)
                {
                    isApplicationOpen = CheckApplicationScheduleIsOkay(OEvent, ApplicationType.NonMember, Portal) == 1;
                }
                else
                {
                    isApplicationOpen = CheckApplicationScheduleIsOkay(OEvent, ApplicationType.Member, Portal) == 1;
                }
            }

            if (!isApplicationOpen)
            {
                isApplicationOpen = CheckApplicationScheduleIsOkay(OEvent, ApplicationType.AllOther, Portal) == 1;
            }

            return isApplicationOpen;
        }

        private static bool isUserOverrideSet(Guid contactId, Guid eventId, int privilege, IPortalContext Portal)
        {
            var context = XrmHelper.GetContext();
            var x = (from p in context.CreateQuery<oems_EventUserPrivilege>()
                     where p.oems_User.Id == contactId && p.oems_Event.Id == eventId && p.oems_Privilege.Value == privilege
                     select p).FirstOrDefault();

            //&& p.oems_ExpiryDate.Value > DateTime.UtcNow
            //dont forget to set time setings in crm portal settings
            if (!ReferenceEquals(x, null) && x.oems_ExpiryDate.HasValue)
            {
                if (DateHelper.timeComparer(x.oems_ExpiryDate.Value, DateTime.UtcNow, Portal) == 1)
                    return true;
            }

            return false;
        }

        private static bool isUserInvited(Guid contactId, oems_Event OEvent)
        {
            var context = XrmHelper.GetContext();

            var x = (from p in context.CreateQuery<oems_EventInvitedGuest>()
                     where p.oems_Invitee.Id == contactId && p.oems_Event.Id == OEvent.Id
                     select p).FirstOrDefault();

            return (!ReferenceEquals(x, null));
        }

        private static int CheckScheduleIsOkay(oems_Event OEvent, int RegistrantType, IPortalContext Portal, bool ignoreEventStatus = false)
        {
            var context = XrmHelper.GetContext();

            var y = (from p in context.CreateQuery<oems_EventRegistrationSchedule>()
                     where p.oems_RegistrantType.Value == RegistrantType
                     && p.oems_Event.Id == OEvent.Id
                     select p).FirstOrDefault();

            if (ReferenceEquals(y, null) || !y.oems_RegistrationOpenDateTime.HasValue || !y.oems_RegistrationCloseDateTime.HasValue) return 0;

            if (!ignoreEventStatus)
            {
                if (DateHelper.timeComparer(y.oems_RegistrationOpenDateTime.Value, DateTime.UtcNow, Portal) == -1
                    && DateHelper.timeComparer(y.oems_RegistrationCloseDateTime.Value, DateTime.UtcNow, Portal) == 1
                    && ((OptionSetValue)OEvent.Attributes["statuscode"]).Value == EventStatus.OpenRegistration)
                {
                    return 1;
                }
            }
            else
            {
                if (DateHelper.timeComparer(y.oems_RegistrationOpenDateTime.Value, DateTime.UtcNow, Portal) == -1
                && DateHelper.timeComparer(y.oems_RegistrationCloseDateTime.Value, DateTime.UtcNow, Portal) == 1)
                {
                    return 1;
                }
            }

            return 2;
        }

        private static int CheckApplicationScheduleIsOkay(oems_Event OEvent, int RegistrantType, IPortalContext Portal)
        {
            var context = XrmHelper.GetContext();

            var y = (from applicationSchedules in context.CreateQuery("oems_eventapplicationschedule")
                     where ((EntityReference)applicationSchedules["oems_event"]).Id == OEvent.Id
                     where (applicationSchedules["oems_applicanttype"] as OptionSetValue).Value == RegistrantType
                     select applicationSchedules).FirstOrDefault();

            if (ReferenceEquals(y, null) || !((DateTime?)y["oems_applicationopendatetime"]).HasValue || !((DateTime?)y["oems_applicationclosedatetime"]).HasValue)
            {
                return 0;
            }

            if (DateHelper.timeComparer(((DateTime?)y["oems_applicationopendatetime"]).Value, DateTime.UtcNow, Portal) == -1
                && DateHelper.timeComparer(((DateTime?)y["oems_applicationclosedatetime"]).Value, DateTime.UtcNow, Portal) == 1
                && ((OptionSetValue)OEvent.Attributes["statuscode"]).Value == EventStatus.OpenApplication)
            {
                return 1;
            }

            return 2;
        }

        public static EventJasonResponce CreateInvitation(Guid contactId, Guid oems_EventId, Guid invitedGuestId, int statusCode)
        {
            var context = XrmHelper.GetContext();

            oems_EventRegistration reg = new oems_EventRegistration
            {
                oems_Event = new EntityReference("oems_event", oems_EventId),
                oems_Contact = new EntityReference("contact", contactId)
            };

            try
            {
                Guid regid = context.Create(reg);

                XrmHelper.SetStatus("oems_eventregistration", regid, 1);

                oems_EventRegistration newReg = (from r in context.CreateQuery<oems_EventRegistration>()
                                                 where r.oems_EventRegistrationId == regid
                                                 select r).FirstOrDefault();

                //retrieve Json here and return it.
                if (updateInvitedGuest(invitedGuestId, statusCode)
                    && !ReferenceEquals(newReg, null)) return new EventJasonResponce
                    {
                        oems_EventRegistrationId = regid.ToString(),
                        result = "true"
                    };

                return new EventJasonResponce
                {
                    result = "false"
                };
            }
            catch (Exception)
            {

                return new EventJasonResponce
                {
                    result = "false"
                };
            }


        }

        public static bool updateInvitedGuest(Guid invitedGuestId, int statusCode)
        {
            var context = XrmHelper.GetContext();

            oems_EventInvitedGuest current = (from i in context.CreateQuery<oems_EventInvitedGuest>()
                                              where i.oems_EventInvitedGuestId == invitedGuestId
                                              && i.statecode == 0
                                              select i).FirstOrDefault();

            if (ReferenceEquals(current, null)) return false;


            try
            {

                XrmHelper.SetStatus("oems_eventinvitedguest", invitedGuestId, statusCode);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //public static bool updateRegistrationForm(Guid oems_EventRegistrationId, string oems_UpdateEventRegistrationData, string oems_UpdatePersonalAccommodationData, int statusCode)
        public static int updateDynamicRegistrationForm(Guid oems_EventRegistrationId, string oems_UpdateEventRegistrationData, string documents, int statusCode)
        {
            var context = XrmHelper.GetContext();

            oems_EventRegistration current = new oems_EventRegistration
            {
                oems_EventRegistrationId = oems_EventRegistrationId,
                oems_dynamicregformdata = oems_UpdateEventRegistrationData

            };

            if (statusCode != 0)
            {
                current.statuscode = statusCode;
            }

            try
            {
                if (!context.IsAttached(current))
                {
                    context.Attach(current);
                }
                context.UpdateObject(current);

                //Save documents
                if (!String.IsNullOrEmpty(documents))
                {
                    dynamic odocuments = JArray.Parse(documents);
                    foreach (var d in odocuments)
                    {
                        var annotation = new Annotation()
                        {
                            FileName = d.fileName,
                            //FileSize = d.fileSize,
                            IsDocument = true,
                            DocumentBody = d.body,
                            MimeType = d.mimeType,
                            Subject = d.subject
                        };
                        AnnotationsHelper.CreateAnnotation(annotation, current, context);
                    }
                }

                context.SaveChanges();

                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static bool IsRegistrationWithWaitingListWaiting(Guid eventId, Guid registrationId)
        {
            var ctx = XrmHelper.GetContext();
            var entities =
            (
                from request in ctx.CreateQuery("oems_waitinglistrequest")
                where
                    request.GetAttributeValue<EntityReference>("oems_event").Id == eventId &&
                    request.GetAttributeValue<EntityReference>("oems_eventregistration").Id == registrationId &&
                    (request.GetAttributeValue<int>("statuscode") == WaitingListStatus.Offered || request.GetAttributeValue<int>("statuscode") == WaitingListStatus.Waiting)
                select request.GetAttributeValue<Guid>("oems_waitinglistrequestid")
            ).ToList();

            return entities.Count > 0;
        }

        public static Entity GetRegistrationWithWaitingListRequest(Guid eventId, Guid registrationId, XrmServiceContext context = null)
        {
            var ctx = context ?? XrmHelper.GetContext();
            var entity =
            (
                from request in ctx.CreateQuery("oems_waitinglistrequest")
                where
                    request.GetAttributeValue<EntityReference>("oems_event").Id == eventId &&
                    request.GetAttributeValue<EntityReference>("oems_eventregistration").Id == registrationId
                select request
            ).FirstOrDefault();

            return entity;
        }

        public static oems_Event GetEvent(Guid id)
        {
            var context = XrmHelper.GetContext();

            var evt =
            (
                from e in context.CreateQuery<oems_Event>()
                where e.oems_EventId == id
                select e
            ).FirstOrDefault();

            return evt;
        }

        public static oems_Event GetEventFromRegistration(Guid id)
        {
            var context = XrmHelper.GetContext();

            var evt =
            (
                from e in context.CreateQuery<oems_Event>()
                join r in context.CreateQuery<oems_EventRegistration>() on e.oems_EventId equals r.oems_Event.Id
                where r.oems_EventRegistrationId == id
                select e
            ).FirstOrDefault();

            return evt;
        }
    }
}