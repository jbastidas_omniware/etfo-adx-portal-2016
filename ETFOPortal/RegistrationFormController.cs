﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Site.Areas.ETFO.RegistrationForm;
using Xrm;
using Microsoft.Xrm.Sdk;
using Site.Helpers;
using WebApi.OutputCache.Core.Time;
using WebApi.OutputCache.V2;
using LinqKit;
using System.Collections.Generic;
using Microsoft.Xrm.Client;

namespace Site
{
    public class CorsCacheOutputAttribute : CacheOutputAttribute
    {
        protected override void ApplyCacheHeaders(HttpResponseMessage response, CacheTime cacheTime)
        {
            base.ApplyCacheHeaders(response, cacheTime);
            response.Headers.Add("Access-Control-Allow-Origin", "*");
        }
    }

    public class RegistrationFormController : ApiController
    {
        public static class BillingStatus
        {
            public static int None = 347780000;
            public static int RefundFailed = 347780001;
        }

        public HttpResponseMessage GetControlList()
        {
            var json = JsonConvert.SerializeObject(FormControlHandler.GetControlList());

            return Request.CreateResponse(HttpStatusCode.OK, json);
        }

        public HttpResponseMessage GetDefaultFormTemplate()
        {
            var json = JsonConvert.SerializeObject(FormControlHandler.GetDefaultFormTemplate());

            return Request.CreateResponse(HttpStatusCode.OK, json);
        }

        public HttpResponseMessage GetRandomGUID()
        {
            return Request.CreateResponse(HttpStatusCode.OK, Guid.NewGuid().ToString());
        }

        [HttpGet]
        public HttpResponseMessage BindSchoolBoardList()
        {
            using (var ServiceContext = new XrmServiceContext())
            {
                var list = (from a in ServiceContext.mbr_schoolboardSet
                            where a.statecode == 0
                            select new Tuple<string, string>(a.mbr_schoolboardId.ToString(), a.mbr_name)
                            ).OrderBy(s => s.Item2).ToList();

                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
        }

        [HttpGet]
        public HttpResponseMessage BindSchoolList(Guid id)
        {
            using (var ServiceContext = new XrmServiceContext())
            {
                var list = (from a in ServiceContext.mbr_schoolSet
                            where a.mbr_SchoolBoard.Id == id
                            select new Tuple<string, string, string, string, string>(
                                a.mbr_schoolId.ToString(), 
                                a.mbr_name, 
                                a.mbr_City, 
                                a.mbr_MainPhone, 
                                a.mbr_Fax)
                            ).OrderBy(s => s.Item2).ToList();

                return Request.CreateResponse(HttpStatusCode.OK, list);
            }         
        }

        [HttpGet]
        public HttpResponseMessage BindLocal(Guid id)
        {
            using (var ServiceContext = new XrmServiceContext())
            {
                var list = (from a in ServiceContext.mbr_localSet
                            //where a.oems_SchoolBoard.Id == id
                            select new Tuple<string, string>(a.mbr_localId.ToString(), a.mbr_name)).ToList();

                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
        }

        [HttpPost]
        public HttpResponseMessage SaveFormTemplate(object data)
        {
            var dd = JsonConvert.DeserializeObject<FormTemplate>(data.ToString());
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        //[CorsCacheOutput(ClientTimeSpan = 7200, ServerTimeSpan = 7200)]
        [HttpGet]
        public HttpResponseMessage SchoolList(Guid contactid)
        {
            using (var ServiceContext = new XrmServiceContext())
            {
                var schoolList = (from ms in ServiceContext.mbr_memberschoolSet
                                  join sch in ServiceContext.mbr_schoolSet on ms.mbr_School.Id equals sch.mbr_schoolId
                                  where ms.mbr_Member.Id == contactid && ms.mbr_Active == true
                                  select new 
                                  {
                                      id = sch.mbr_schoolId.ToString().Replace("{", "").Replace("}", ""),
                                      name = sch.mbr_name,
                                      parent = (sch.GetAttributeValue<EntityReference>("mbr_schoolboard")).Id.ToString().Replace("{", "").Replace("}", ""),
                                      city = sch.mbr_City,
                                      email = sch.mbr_Email,
                                      fax = sch.mbr_Fax,
                                      phone = sch.mbr_MainPhone,
                                      principalName = sch.mbr_PrincipalName,
                                      principalEmail = sch.mbr_PrincipalEmail,
                                      memberTypeId = (ms.GetAttributeValue<EntityReference>("mbr_membertype")).Id,
                                      memberTypeName = (ms.GetAttributeValue<EntityReference>("mbr_membertype")).Name
                                  }).ToList();

                var response = Request.CreateResponse(HttpStatusCode.OK, schoolList, Configuration.Formatters.JsonFormatter);
                return response;
            }
        }

        //[CorsCacheOutput(ClientTimeSpan = 7200, ServerTimeSpan = 7200, ExcludeQueryStringFromCacheKey = false)]
        [HttpGet]
        public HttpResponseMessage LocalList(Guid eventid, Guid contactid)
        {
            using (var ServiceContext = new XrmServiceContext())
            {
                // Get member schools
                var memberSchools = (from ms in ServiceContext.mbr_memberschoolSet
                                     join s in ServiceContext.mbr_schoolSet on ms.mbr_School.Id equals s.mbr_schoolId
                                     where ms.mbr_Member.Id == contactid && ms.mbr_Active == true
                                     select new
                                     {
                                         MemberTypeId = ms.mbr_MemberType.Id,
                                         SchoolBoardId = s.mbr_SchoolBoard.Id
                                     }).ToList();

                var predicate = PredicateBuilder.New<Entity>();
                if (memberSchools.Count() > 0)
                {
                    foreach (var key in memberSchools)
                    {
                        predicate = predicate.Or
                        (
                            sbl => ((EntityReference)sbl["mbr_membertypeid"]).Id == key.MemberTypeId &&
                                    ((EntityReference)sbl["mbr_schoolboardid"]).Id == key.SchoolBoardId
                        );
                    }

                    //Gets the locals
                    var locals = (from sbl in ServiceContext.CreateQuery("mbr_schoolboardlocal").AsExpandable()
                                    .Where(s => s.GetAttributeValue<int>("statecode") == 0)
                                    .Where(predicate)
                                  select new
                                  {
                                      id = (sbl.GetAttributeValue<EntityReference>("mbr_localid")).Id,
                                      name = (sbl.GetAttributeValue<EntityReference>("mbr_localid")).Name,
                                      memberTypeId = (sbl.GetAttributeValue<EntityReference>("mbr_membertypeid")).Id,
                                      memberTypeName = (sbl.GetAttributeValue<EntityReference>("mbr_membertypeid")).Name,
                                      schoolBoardId = (sbl.GetAttributeValue<EntityReference>("mbr_schoolboardid")).Id,
                                      schoolBoardName = (sbl.GetAttributeValue<EntityReference>("mbr_schoolboardid")).Name,
                                      delegationLimit = ""
                                  }).ToList();


                    // Local delegation & tikets validation
                    var oemsEvent = (from a in ServiceContext.oems_EventSet where a.oems_EventId == eventid select a).Single();
                    if (oemsEvent.oems_LocalDelegationComponentFlag.HasValue && oemsEvent.oems_LocalDelegationComponentFlag.Value &&
                        oemsEvent.oems_TicketComponentFlag.HasValue && oemsEvent.oems_TicketComponentFlag.Value)
                    {
                        predicate = PredicateBuilder.New<Entity>();
                        foreach (var local in locals)
                        {
                            predicate = predicate.Or
                            (
                                evtLoc => ((EntityReference)evtLoc["oems_event"]).Id == eventid &&
                                            ((EntityReference)evtLoc["oems_local"]).Id == local.id &&
                                            (int?)evtLoc["oems_localdelegationlimit"] != null &&
                                            (int)evtLoc["statecode"] == 0
                            );
                        }

                        //Gets the locals
                        var eventLocals = (from evtLoc in ServiceContext.CreateQuery("oems_eventlocal").AsExpandable().Where(predicate)
                        select new
                        {
                            localId = ((EntityReference)evtLoc["oems_local"]).Id,
                            delegationLimit = (int)evtLoc["oems_localdelegationlimit"]
                        }).ToList();

                        //Assign local Delegation limits
                        var finalList = new List<dynamic>();
                        foreach (var local in locals)
                        {
                            var evtLoc = eventLocals.Where(l => l.localId == local.id).FirstOrDefault();
                            if(evtLoc != null)
                            {
                                finalList.Add(new
                                {
                                    id = local.id,
                                    name = local.name,
                                    memberTypeId = local.memberTypeId,
                                    memberTypeName = local.memberTypeName,
                                    schoolBoardId = local.schoolBoardId,
                                    schoolBoardName = local.schoolBoardName,
                                    delegationLimit = evtLoc.delegationLimit
                                });
                            }
                            else
                            {
                                finalList.Add(local);
                            }
                        }

                        var responseLocals = Request.CreateResponse(HttpStatusCode.OK, finalList, Configuration.Formatters.JsonFormatter);
                        return responseLocals;
                    }
                    else
                    {
                        var responseLocals = Request.CreateResponse(HttpStatusCode.OK, locals, Configuration.Formatters.JsonFormatter);
                        return responseLocals;
                    }
                }

                var response = Request.CreateResponse(HttpStatusCode.OK, new List<dynamic>(), Configuration.Formatters.JsonFormatter);
                return response;
            }
        }

        [HttpPost]
        public HttpResponseMessage CancelOrWithdrawnRegistration(Guid eventRegistrationId, bool withdrawn)
        {
            using (var ServiceContext = new XrmServiceContext())
            {
                // set status to cancelled
                var cancelType = withdrawn ? RegistrationHelper.EventRegistration.Withdrawn : RegistrationHelper.EventRegistration.Cancelled;
                bool result = XrmHelper.SetStatus(oems_EventRegistration.EntityLogicalName, eventRegistrationId, cancelType, RegistrationHelper.State.Active);
                if (!result)
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }


                var eventRegistration = (from er in ServiceContext.CreateQuery("oems_eventregistration")
                                         where er.GetAttributeValue<Guid>("oems_eventregistrationid") == eventRegistrationId
                                         select er).Single();

                result = eventRegistration.GetAttributeValue<OptionSetValue>("oems_billingstatus").Value == BillingStatus.None;
                if (result)
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    var errorMessage = withdrawn
                        ? XrmHelper.GetSnippetValueOrDefault("EventRegistration/WithdrawError", "Your registration has been withdrawn but there was an error refunding you the money. Please contact ETFO  Web support.")
                        : XrmHelper.GetSnippetValueOrDefault("EventRegistration/CancelError", "Your registration has been cancelled but there was an error refunding you the money. Please contact ETFO  Web support.");
                    return Request.CreateResponse(HttpStatusCode.OK, errorMessage);
                }
            }
        }

        [HttpPost]
        public HttpResponseMessage RejectWaitingListOffer(Guid eventId, Guid registrationId)
        {
            using (var ServiceContext = new XrmServiceContext())
            {
                var request = RegistrationHelper.GetRegistrationWithWaitingListRequest(eventId, registrationId, ServiceContext);
                request.SetAttributeValue("oems_declineddate", DateTime.UtcNow);
                ServiceContext.UpdateObject(request);

                bool result = XrmHelper.SetStatus
                (
                    "oems_waitinglistrequest",
                    request.GetAttributeValue<Guid>("oems_waitinglistrequestid"),
                    RegistrationHelper.WaitingListStatus.Declined,
                    RegistrationHelper.State.Active
                );

                if (result)
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }

        }
    }

    public class SchoolBoard
    {
        public string m_Item1 { get; set; }
        public string m_Item2 { get; set; }
       
    }

    public class School
    {
        public String id { get; set; }
        public String parent { get; set; }
        public String name { get; set; }
        public String city { get; set; }
        public String phone { get; set; }
        public String fax { get; set; }
        public String principalName { get; set; }
        public String principalEmail { get; set; }
    }

    public class Local
    {
        public String id { get; set; }
        public String parent { get; set; }
        public String name { get; set; }
    }

    public class MemberSchool
    {
        public String id { get; set; }
        public String schoolid { get; set; }
        public String membertypeid { get; set; }
        public String memberid { get; set; }
    }

    public class MemberType
    {
        public String id { get; set; }
        public String membertype { get; set; }
    }

    public class TeacherType
    {
        public String id { get; set; }
        public String teachertype { get; set; }
    }
    
}